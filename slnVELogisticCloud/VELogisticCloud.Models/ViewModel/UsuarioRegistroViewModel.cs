﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VELogisticCloud.Models;

namespace VELogisticCloud.App.Web.Models.Seguridad
{
    public class UsuarioRegistroViewModel
    {
        [Required]        
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "El {0} debe tener al menos {2} y un maximo de {1} characters.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar password")]
        [Compare("Password", ErrorMessage = "El password y la confirmacion del password no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Apellidos { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Nombres { get; set; }

        public string TelefonoMovil { get; set; }

        [Required(ErrorMessage = "Seleccione un Estado valido.")]        
        public int IdEstado { get; set; }

        public List<Estado> Estados { get; set; }

        public string FechaRegistro { get; set; }
    }
}
