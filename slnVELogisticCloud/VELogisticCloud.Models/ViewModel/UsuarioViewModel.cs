﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.Models;

namespace VELogisticCloud.Models.ViewModel
{
    public class UsuarioViewModel
    {
        public ApplicationUser Entidad { get; set; }
        public List<SelectListItem> Estados { get; set; }
    }
}
