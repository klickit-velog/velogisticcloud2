﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.ViewModel
{
    public class ReservaViewModel
    {
        public string CodigoNaviera { get; set; }
        public string NombreNaviera { get; set; }
        public string CodigoExpeditor { get; set; }
        public string NombreExpeditor { get; set; }
        public string CodigoConsignatario { get; set; }
        public string NombreConsignatario { get; set; }
        public string CodigoPuertoEmbarque { get; set; }
        public string NombrePuertoEmbarque { get; set; }
        public string CodigoPuertoDescarga { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string NombreViaje { get; set; }
        public string NaveViaje { get; set; }
        public string NumeroContrato { get; set; }

        public List<SelectListItem> ListaNavieras { get; set; }
        public List<SelectListItem> ListaExpeditor { get; set; }
        public List<SelectListItem> ListaTipoContenedor { get; set; }
        public List<SelectListItem> ListaCommodity { get; set; }
        public List<SelectListItem> ListaCampania { get; set; }
        public List<SelectListItem> ListaAlmacenDeVacio { get; set; }
        public List<SelectListItem> ListaClientes { get; set; }
    }
}
