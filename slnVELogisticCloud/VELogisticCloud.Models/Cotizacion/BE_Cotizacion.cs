﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Cotizacion
{
    public class BE_Cotizacion
    {

        #region Maritimo
        public class BE_Maritimo
        {
            public int Cantidad_Contenedor { get; set; }
            public string Codigo_Tipo_Contenedor { get; set; }
            public string Tipo_Contenedor { get; set; }
            public string Codigo_Cotizacion { get; set; }
            public string Cliente { get; set; }
            public string Fecha_Envio { get; set; }
            public string Fecha_Final { get; set; }
            public string Campania { get; set; }
            public string Codigo_Campania { get; set; }
            public string Nota { get; set; }
            public string Observacion { get; set; }
            public string Codigo_Usuario { get; set; }
            public string IdUsuarioCreacion { get; set; }
            public string IdUsuarioModificacion { get; set; }
            public int Estado { get; set; }
        }
        public class BE_Det_Maritimo
        {
            public decimal Id_Cotizacion_Maritimo { get; set; }
            public string Region { get; set; }
            public string Codigo_Linea_Naviera { get; set; }
            public string Linea_Naviera { get; set; }
            public string Codigo_Puerto_Embarque { get; set; }
            public string Puerto_Embarque { get; set; }
            public string Codigo_Puerto_Descarga { get; set; }
            public string Puerto_Descarga { get; set; }
            public string Codigo_Commodity { get; set; }
            public string Commodity { get; set; }
            public string Tamanio { get; set; }
            public string TT { get; set; }
            public string Valido_Desde { get; set; }
            public string Valido_Hasta { get; set; }

            public string Codigo_Pais_Embarque { get; set; }
            public string Pais_Puerto_Embarque { get; set; }
            public string Codigo_Pais_Descarga { get; set; }
            public string Pais_Puerto_Descarga { get; set; }

            public int Id_Puerto_Carga { get; set; }
            public int Id_Puerto_Descarga { get; set; }
            public int Id_Naviera { get; set; }
            



        }
        public class Parametros_Lista_Maritimo
        {
            public string Codigo_Usuario { get; set; }
            public string cod_coti { get; set; }
            public string Campana { get; set; }
            public string cod_line_navi { get; set; }
            public string cod_puerto_destino { get; set; }
            public string cod_puerto_salida { get; set; }
            public decimal id_estado { get; set; }
            public int TipoCotizacion { get; set; }
            public int IdNaviera { get; set; }
            public List<string> ListadoUsuarios { get; set; }
        }
        public class C_Usuario
        {
            public string Usuario { get; set; }
        }
        public class Lista_Maritimo
        {
            public int Codigo_coti { get; set; }
            public int id_detalle { get; set; }
            public int Cantidad_Contenedor { get; set; }
            public string Region { get; set; }
            public string Linea_naviera { get; set; }
            public string POL { get; set; }
            public string POD { get; set; }
            public string Campania { get; set; }
            public decimal NETO { get; set; }
            public decimal REBATE { get; set; }
            public decimal FLETE { get; set; }
            public string TT { get; set; }
            public string Valido_Desde { get; set; }
            public string Valido_Hasta { get; set; }
            public string FECHA_APROBACION { get; set; }
            public string COMENTARIOS { get; set; }
            public string NumeroContrato { get; set; }
            public int IdEstado { get; set; }
            public string Estado { get; set; }
            public int IdOddoService { get; set; }
            public int cantPuertoCarga { get; set; }
            public int cantPuertoDescarga { get; set; }
            


        }
        public class DetalleMaritimo
        {
            public int Id_Cotizacion_Maritimo { get; set; }
            public string Codigo { get; set; }
            public string Cliente { get; set; }
            public string Fecha_Envio { get; set; }
            public string Fecha_Final { get; set; }
            public string Puerto_EmbarqueCabecera { get; set; }
            public string Puerto_DescargaCabecera { get; set; }
            public string CommodityCabecera { get; set; }
            public string Campania { get; set; }
            public string CodigoCampania { get; set; }
            public string Codigo_Tipo_Contenedor { get; set; }
            public string TipoContenedor { get; set; }
            public int Cantidad_Contenedor { get; set; }
            public string Linea_NavieraCabecera { get; set; }
            //detalle 
            public string Region { get; set; }
            public string Linea_Naviera { get; set; }
            public string Puerto_Embarque { get; set; }
            public string Puerto_Descarga { get; set; }
            public string Codigo_Commodity { get; set; }
            public string Commodity { get; set; }
            public string Tamanio { get; set; }
            public decimal NETO { get; set; }
            public decimal REBATE { get; set; }
            public decimal FLETE { get; set; }
            public string TT { get; set; }
            public string Valido_Desde { get; set; }
            public string Valido_Hasta { get; set; }
            public string Codigo_Pais_Embarque { get; set; }
            public string Codigo_Pais_Descarga { get; set; }
            public string Pais_Puerto_Descarga { get; set; }
            public string Pais_Puerto_Embarque { get; set; }

            public string Codigo_Puerto_Embarque { get; set; }
            public string Codigo_Puerto_Descarga { get; set; }

            public int Id_Puerto_Carga { get; set; }
            public int Id_Puerto_Descarga { get; set; }

            public string Codigo_Linea_Naviera { get; set; }

            public int Id_detalle_cotizacion_maritimo {get;set;}

            public string Nota { get; set; }
            public string Codigo_Usuario { get; set; }
            public string Ruc { get; set; }

        }
        public class DatosEditarMaritimo
        {
            public int Id_Cotizacion_Maritimo { get; set; }
            public string RUC { get; set; }
            public string Cliente { get; set; }
            public string Fecha_Envio { get; set; }
            public string Fecha_Final { get; set; }
            public string CodCampania { get; set; }
            public string Campania { get; set; }
            public string Codigo_Tipo_Contenedor { get; set; }
            public int Cantidad_Contenedor { get; set; }
            public string Nota { get; set; }
            public string Observacion { get; set; }
            //detalle
            public int Id_detalle_cotizacion_maritimo { get; set; }
            public string Region { get; set; }
            public string Linea_Naviera { get; set; }
            public string Puerto_Embarque { get; set; }
            public string Puerto_Descarga { get; set; }
            public string Commodity { get; set; }
            public string Tamanio { get; set; }
        }

        public class ListaCotizacionesLogisticasAprobadas
        {
            public string FechaAprobacion { get; set; }
            public int IdCotizacionLogistica { get; set; }
            public int IdCotizacionDetalleOdoo { get; set; }
            public string NombreNaviera { get; set; }
            public string NombrePuertoCarga { get; set; }
            public string NombreServicio { get; set; }
            public string NombreTerminalRetiroVacio { get; set; }
            public string NombreAlmacenPacking { get; set; }
            public string NombreCommodity { get; set; }
            public decimal Flete { get; set; }
            public string NombreCampania { get; set; }
            public int IdTerminalDepositoVacio { get; set; }
            public int IdNaviera { get; set; }
            public int IdPuertoCarga { get; set; }
            public string NombreTerminalDepositoVacio { get; set; }
            
        }

        #endregion


        #region Logistica

        public class TablaDetalleLogistica
        {
            public string NombreNaviera { get; set; }
            public string NombrePuertoCarga { get; set; }
            public string NombreServicio { get; set; }
            public string NombreTerminalRetiroVacio { get; set; }
            public string NombreAlmacenPacking { get; set; }
            public string NombreCommodity { get; set; }
            public decimal Flete { get; set; }
        }

        public class BE_Logistica
        {
            public string Codigo_Puerto_Embarque { get; set; }
            public string Puerto_Embarque { get; set; }
            public string Codigo_Commodity { get; set; }
            public string Commodity { get; set; }
            public int Servicio_Embarque { get; set; }
            public int Retiro_Contenedor_Vacio { get; set; }
            public string Codigo_Almacen_Retiro { get; set; }
            public string Almacen_Retiro { get; set; }
            public string Fecha_Cita { get; set; }
            public int Retiro_Transporte { get; set; }
            public string Codigo_Almacen_Packing { get; set; }
            public string Almacen_Packing { get; set; }
            public int Servicio_Agenciamiento_Aduana { get; set; }
            public int Certificado_Origen { get; set; }
            public int Conexion_Electrica { get; set; }
            public int Pago_CPB { get; set; }
            public int Agenciamiento_Maritimo { get; set; }
            public int Movilizaciones_Contenedor { get; set; }
            public int Generador { get; set; }
            public int Aforo_Fisico { get; set; }
            public int Courier { get; set; }
            public int Senasa { get; set; }
            public int Filtro { get; set; }
            public int Termografo { get; set; }
            public string Tipo_Termografo { get; set; }


            public string Codigo_Pais_Embarque { get; set; }
            public string Codigo_Pais_Descarga { get; set; }
            public string Pais_Puerto_Descarga { get; set; }
            public string Pais_Puerto_Embarque { get; set; }



            public string IdUsuarioCreacion { get; set; }
            public string IdUsuarioModificacion { get; set; }
            public int Estado { get; set; }
            public int Id_Puerto_Carga { get; set; }

            public int Chk_Transporte               {get;set;}
            public int Chk_GateOut                  {get;set;}
            public int Chk_AgenciamientoAduana      {get;set;}
            public int Chk_DerechoEmbarque          {get;set;}
            public int Chk_VistoBueno               {get;set;}
            public int Chk_EnvioDocumentos          {get;set;}
            public int Chk_Movilidad                {get;set;}
            public int Chk_TermoRegistros           {get;set;}
            public int Chk_FiltrosEtileno           {get;set;}
            public int Chk_Certificado              { get;set; }
            public int IdCampania                   {get;set;}
            public string Desc_Campania              {get;set;}
            public string TipoTermo                  {get;set;}
            public string TipoFiltro                 { get; set; }
            public string Comentarios                { get; set; }
            public int CantidadTermo                { get; set; }
            public int CantidadFiltro { get; set; }
            public int IdFiltro { get; set; }
            public int IdTermo { get; set; }



        }
        public class Parametros_Lista_Logistica
        {
            public string numero_contenedor { get; set; }
            public string codigo_bl { get; set; }
            public string numero_dam { get; set; }
            public string numero_viaje { get; set; }
            public string numero_booking { get; set; }
            public string commodity { get; set; }
        }
        public class Lista_Logistica
        {
            public int IdCotizacionLogistica { get; set; }
            public int IdOperacionLogistica { get; set; }
            public string Nro_Contenedor { get; set; }
            public string Campania { get; set; }
            public string NombreCommodity { get; set; }
            public string Booking { get; set; }
            public string Nro_Viaje { get; set; }
            public string Canal { get; set; }
            public string Linea { get; set; }
            public string Deposito { get; set; }
            public string Packing { get; set; }

            public string Lugar1 {get;set;}
            public string Lugar2 {get;set;}
            public string Lugar3 {get;set;}
            public string Fecha1 {get;set;}
            public string Fecha2 {get;set;}
            public string Fecha3 {get;set;}
            public string Hora1  {get;set;}
            public string Hora2  {get;set;}
            public string Hora3 { get; set; }
            public string OrdenServicio { get; set; }
            public string Cliente { get; set; }
            
        }
        public class Mis_Solicitudes_Logisticas
        {
            public int IdCotizacionLogistica { get; set; }
            public string Campania { get; set; }
            public string NroCotizacion { get; set; }
            public string NombrePuertoCarga { get; set; }
            public string NombreTerminalRetiroVacio { get; set; }
            public string NroServicio { get; set; }
            public int IdEstadoOddo { get; set; }
            public string EstadoOddo { get; set; }
            public int IdEstado { get; set; }
            public string Estado { get; set; }
            public int IdCotizacionOdoo { get; set; }
            public string NombreAlmacenPacking { get; set; }





        }
        public class DetalleLogistica
        {
            public int IdCotizacionLogistica { get; set; }
            public string NroContenedor { get; set; }
            public string NombrePuertoCarga { get; set; }
            public string NombreCommodity { get; set; }
            public string NombreAlmacenPacking { get; set; }
            public string Fecha_Cita { get; set; }
            public string NombrePaisCarga { get; set; }
            public string UsuarioCreacion { get; set; }

            public int IdPuertoCarga { get; set; }
            public int IdCommodity { get; set; }
            public int IdAlmacenPacking { get; set; }
            public string Notas { get; set; }
            public string Cliente { get; set; }
            public string NombreCampania { get; set; }
            
            //public string Courier { get; set; }
            //public string Filtro { get; set; }
            //public string CertificadoOrigen { get; set; }
            //public string PagoCPB { get; set; }
            //public string MovilizacionesContenedor { get; set; }
            //public string AforoFisico { get; set; }
            //public string Senasa { get; set; }
            //public string Termografo { get; set; }
            //public string Pais_Puerto_Descarga { get; set; }
            //public string Pais_Puerto_Embarque { get; set; }

            //public int Id_Puerto_Carga { get; set; }
            //public int Codigo_Commodity { get; set; }
            //public int Codigo_Almacen_Packing { get; set; }


            //public string Chk_Transporte { get; set; }
            //public string Chk_GateOut { get; set; }
            //public string Chk_AgenciamientoAduana { get; set; }
            //public string Chk_DerechoEmbarque { get; set; }
            //public string Chk_VistoBueno { get; set; }

            //public string Chk_EnvioDocumentos { get; set; }
            //public string Chk_Movilidad { get; set; }
            //public string Chk_TermoRegistros { get; set; }
            //public string Chk_FiltrosEtileno { get; set; }
            //public string Chk_Certificado { get; set; }

            //public string Comentarios { get; set; }


        }
        #endregion

        #region Editar Logistica
        public class DatosEditarLogistica
        {
            public int Id_Cotizacion_Logistica { get; set; }
            public string Codigo_Puerto_Embarque { get; set; }
            public string Codigo_Commodity { get; set; }
            public int Servicio_Embarque { get; set; }
            public int Retiro_Contenedor_Vacio { get; set; }
            public string Codigo_Almacen_Retiro { get; set; }
            public string Fecha_Cita { get; set; }
            public int Retiro_Transporte { get; set; }
            public string Codigo_Almacen_Packing { get; set; }
            public int Servicio_Agenciamiento_Aduana { get; set; }
            public int Certificado_Origen { get; set; }
            public int Conexion_Electrica { get; set; }
            public int Pago_CPB { get; set; }
            public int Agenciamiento_Maritimo { get; set; }
            public int Movilizaciones_Contenedor { get; set; }
            public int Generador { get; set; }
            public int Aforo_Fisico { get; set; }
            public int Courier { get; set; }
            public int Senasa { get; set; }
            public int Filtro { get; set; }
            public int Termografo { get; set; }
            public string Tipo_Termografo { get; set; }

        }
        #endregion
        public class BE_Transporte
        {
            public string Booking { get; set; }
            public string Dimensiones { get; set; }
            public string BL { get; set; }
            public string Codigo_Contenedor { get; set; }
            public string Tipo_Contenedor { get; set; }
            public string Nro_Orden { get; set; }
            public decimal Cant_Contenedor { get; set; }
            public string Cod_Commodity { get; set; }
            public string Commodity { get; set; }
            public string Peso { get; set; }
            public string Almacen_Retiro { get; set; }
            public string Fecha_Retiro { get; set; }
            public string Almacen_Devolucion { get; set; }
            public string Estado_Transporte { get; set; }
            public string IdUsuarioCreacion { get; set; }
            public string IdUsuarioModificacion { get; set; }
            public int Estado { get; set; }
        }

        public class DetalleCotizacionAPI
        {
            public int Id_Cotizacion_Maritimo { get; set; }
            public string Region { get; set; }
            public string Codigo_Linea_Naviera { get; set; }
            public string Linea_Naviera { get; set; }
            public string Codigo_Puerto_Embarque { get; set; }
            public string Puerto_Embarque { get; set; }
            public string Codigo_Puerto_Descarga { get; set; }
            public string Puerto_Descarga { get; set; }
            public string Id_Commodity { get; set; }
            public string Commodity { get; set; }
            public string Tamanio { get; set; }
            public string TT { get; set; }
            public DateTime? Valido_Desde { get; set; }
            public DateTime? Valido_Hasta { get; set; }
            public decimal Neto { get; set; }
            public decimal Rebate { get; set; }
            public decimal Flete { get; set; }
            public string Fecha_Aprobacion { get; set; }
            public string Comentarios { get; set; }
            public string IdUsuarioCreacion { get; set; }
            public string FechaCreacion { get; set; }
            public string IdUsuarioModificacion { get; set; }
            public string FechaModificacion { get; set; }
            public int Estado { get; set; }
            public string NumeroCotizacion { get; set; }
            public string NumeroContrato { get; set; }
            public string CodigoCampania { get; set; }
            public string Campania { get; set; }
            public string Codigo_Pais_Embarque { get; set; }
            public string Codigo_Pais_Descarga { get; set; }
            public string Pais_Puerto_Descarga { get; set; }
            public string Pais_Puerto_Embarque { get; set; }
            public int Id_Puerto_Embarque { get; set; }
            public int Id_Puerto_Descarga { get; set; }
            public int Id_Naviera { get; set; }
            public string codigoEstado { get; set; }
        }
        
       public class BE_DetalleLogisticaParaOdoo
        {
            public int IdNaviera { get; set; }
            public int IdPuertoCarga { get; set; }
            public int IdTerminalRetiroVacio { get; set; }
            public int IdTerminalDepositoVacio { get; set; }
            public int IdAlmacenPacking { get; set; }
            public int IdCotizacionLogisticaDetalle { get; set; }
            public int IdServicio { get; set; }
        }
       
        
        

    }
}
