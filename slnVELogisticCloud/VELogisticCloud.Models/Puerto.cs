﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VELogisticCloud.Models
{
    [NotMapped]
    public class Puerto
    {
        [Key]
        public int IdPuerto { get; set; }

        [Required(ErrorMessage = "Ingrese un nombre para el puerto.")]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        [Required(ErrorMessage = "Ingrese un código para el puerto.")]
        public string Codigo { get; set; }
    }
}
