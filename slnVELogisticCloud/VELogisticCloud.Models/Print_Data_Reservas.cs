﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models
{
    public class Ent_Reserva
    {
        public int Id_Reserva { get; set; }
        public int Id_Cotizacion_Maritimo_Detalle { get; set; }
        public string   Naviera             { get; set; }
        public string Nro_contrato { get; set; }
        public string   Expeditor           { get; set; }
        public string   Consignatario       { get; set; }
        public string Puerto_Embarque { get; set; }
        public string Puerto_Descarga { get; set; }
        public string   ETD                 { get; set; }
        public string   ETA                 { get; set; }
        public string Nombre_Nave { get; set; }
        public string Nave_Viaje { get; set; }
        public string Cantidad { get; set; }
        public string   Tipo_Contenedor      { get; set; }
        public string Codigo_Commodity { get; set; }
        public string Commodity { get; set; }
        public string Descripcion_Carga { get; set; }
        public string   Temperatura             { get; set; }
        public string   Ventilacion                 { get; set; }
        public string   Humedad             { get; set; }
        public string   Peso                { get; set; }
        public string   Volumen             { get; set; }
        public string   Flete               { get; set; }
        public string   Emision_BL          { get; set; }
        public int Id_Estado_Reserva { get; set; }
        public string Estado_desc           { get; set; }
        public string Nro_Booking { get; set; }
        

        public string Id_Naviera { get; set; }
        public string Id_Contenedor { get; set; }
        public string Codigo_Nave { get; set; }
        public string CallSign_Nave { get; set; }
        public string Codigo_Puerto_Embarque { get; set; }
        public string Codigo_Pais_Embarque { get; set; }
        public string Codigo_Puerto_Descarga { get; set; }
        public string Codigo_Pais_Descarga { get; set; }
        public string Codigo_Consignatario { get; set; }
        public string Direccion_Consignatario { get; set; }

        public string Codigo_Expeditor { get; set; }
        public string Direccion_Expeditor { get; set; }
        public string Pais_Puerto_Descarga { get; set; }
        public string Pais_Puerto_Embarque { get; set; }

        public string IdUsuarioCreacion { get; set; }
        public string IdUsuarioModificacion { get; set; }


        
        public string Tipo							    {get;set;}
        public string Condicion						    {get;set;}
        public string ColdTreatment					    {get;set;}
        public string TipoTemperatura				    {get;set;}
        public string TipoVentilacion				    {get;set;}
        public string TipoHumedad					    {get;set;}
        public string Atmosfera						    {get;set;}
        public string CO2							    {get;set;}
        public string O2								{get;set;}
        public string ID_IMO							{get;set;}
        public string IMO							    {get;set;}
        public string UN1							    {get;set;}
        public string UN2							    {get;set;}
        public string NombrePagador					    {get;set;}
        public string DireccionPagador                  { get; set; }

        public int Id_Puerto_Carga               { get; set; }
        public int Id_Puerto_Descarga                { get; set; }

        public string Codigo_Usuario { get; set; }
        public string Nota { get; set; }




    }

    public class Resumen
    {
        public string Nro_booking { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string Cantidad { get; set; }
        public string Tipo_Contenedor { get; set; }
        public string Commodity { get; set; }
        public string Temperatura { get; set; }
        public string Ventilacion { get; set; }
        public string Humedad { get; set; }
        public string Peso { get; set; }
        public string Flete { get; set; }
        public string Emision { get; set; }
        public string Pais_Puerto_Embarque { get; set; }
        public string Pais_Puerto_Descarga { get; set; }
        
    }


    public class Ent_DetalleMaritimo
    {
        public int Id_Cotizacion_Maritimo { get; set; }
        public int Id_detalle_cotizacion_maritimo { get; set; }
        public string Codigo_Linea_Naviera { get; set; }
        public string NumeroContrato { get; set; }
        public int Cantidad_Contenedor { get; set; }
        public string Codigo_Tipo_Contenedor { get; set; }
        public string Codigo_Commodity { get; set; }
        public string Codigo_Puerto_Embarque { get; set; }
        public string Codigo_Puerto_Descarga { get; set; }
        public string Pais_Puerto_Descarga { get; set; }
        public string Pais_Puerto_Embarque { get; set; }
        public string Codigo_Pais_Embarque { get; set; }
        public string Codigo_Pais_Descarga { get; set; }
        public string Puerto_Embarque { get; set; }
        public string Puerto_Descarga { get; set; }
        public string Commodity { get; set; }
        public int Id_Puerto_Carga { get; set; }
        public int Id_Puerto_Descarga { get; set; }

        
    }

    public class Ent_Navieras_Oddo_VE
    {
        public int Id_VE_Naviera { get; set; }
        public int Id_Oddo { get; set; }
        public string NombreNaviera { get; set; }
        public string IMO { get; set; }
    }
}
