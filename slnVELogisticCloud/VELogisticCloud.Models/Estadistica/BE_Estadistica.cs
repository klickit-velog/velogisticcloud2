﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Estadistica
{

    #region Estadistica Maritimo
    public class BE_Estadistica_Cotizacion
    {
        public int total { get; set; }
        public int solicitado { get; set; }
        public int aceptado { get; set; }
        public int confirmada { get; set; }
        public int cancelada { get; set; }
        public int observada { get; set; }
    }
    public class BE_RESUMEN_BOOKING
    {
        public int total { get; set; }
        public int pendiente { get; set; }
        public int aprobados { get; set; }
        public int rechazado { get; set; }

        public int total_opl { get; set; }
        public int pendiente_opl { get; set; }
        public int aprobados_opl { get; set; }
        
    }

    public class BE_Estadistica_Reservas
    {
        public int id_estado_reserva { get; set; }
        public int cantidad { get; set; }
    }

    public class BE_Estadistica_Reservas_Inttra
    {
        public int puerto { get; set; }
        public int nave { get; set; }
        public int puertoDestino { get; set; }
        public int total { get; set; }
    }
    #endregion

    public class BE_Estadistica_Cotizacion_Logistica
    {
        public int total { get; set; }
        public int pendiente { get; set; }
        public int aceptado { get; set; }
        public int confirmado { get; set; }
        public int observada { get; set; }
        public int cancelada { get; set; }
        
    }

    public class BE_Estadistica_Opl
    {
        public int id_estado_opl { get; set; }
        public int cantidad { get; set; }
    }

    public class BE_Estadistica_OPL_Oddo
    {
        public int contenedorAsignado { get; set; }
        public int ingresoallenarCarga { get; set; }
        public int ingresoalPuerto { get; set; }
        public int llegadaadepositodevacio { get; set; }
        public int llegadaalCliente { get; set; }
        public int llegadaalPuerto { get; set; }
        public int enCamino { get; set; }
        public int salidadelCliente { get; set; }
        public int salidadelPuerto { get; set; }
        public int llegadaaBase { get; set; }
        public int salidadedepositodevacio { get; set; }
        public int revisionSINI { get; set; }
        public int total { get; set; }
    }
}
