﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Alertas
{
    public class BE_Alertas_Contrato
    {
        public int Id_Alerta     {get;set;}
        public string NroContrato   {get;set;}
        public string Descripcion   {get;set;}
        public string Tipo          {get;set;}
        public string Servicio      {get;set;}
        public string Area          {get;set;}
        public string Proceso       {get;set;}
        public string Linea         {get;set;}
        public string Estado { get;set; }
        public int IdEstado      { get; set; }

        public int IdContrato { get; set; }
        public int bTipo { get; set; }
        public DateTime FechaIni { get; set; }
        public DateTime FechaFin { get; set; }
        public int DiasVencimiento { get; set; }
    }


}
