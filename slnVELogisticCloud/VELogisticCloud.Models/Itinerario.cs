﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models
{
    public class Itinerario
    {
        public string scac { get; set; }
        public string carrierName { get; set; }
        public string serviceName { get; set; }
        public string vesselName { get; set; }
        public string voyageNumber { get; set; }
        public string imoNumber { get; set; }
        public string originUnloc { get; set; }
        public string originCountry { get; set; }
        public string originCityName { get; set; }
        public string originSubdivision { get; set; }
        public string originTerminal { get; set; }
        public string destinationUnloc { get; set; }
        public string destinationCountry { get; set; }
        public string destinationSubdivision { get; set; }
        public string destinationCityName { get; set; }
        public string destinationTerminal { get; set; }
        public string originDepartureDate { get; set; }
        public string destinationArrivalDate { get; set; }
        public string estimatedTerminalCutoff { get; set; }
        public string terminalCutoff { get; set; }
        public string bkCutoff { get; set; }
        public string siCutoff { get; set; }
        public string hazBkCutoff { get; set; }
        public string vgmCutoff { get; set; }
        public string reeferCutoff { get; set; }
        public string totalDuration { get; set; }
        public string scheduleType { get; set; }
        public string legs { get; set; }

    }
}
