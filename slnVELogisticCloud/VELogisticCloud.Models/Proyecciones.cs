﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models
{
    public class Proyecciones
    {
        public class Cabecera_Proyecciones
        {
            public decimal Id_Proyeccion { get; set; }
            public string Campania { get; set; }
            public string Codigo_Commodity { get; set; }
            public string Commodity { get; set; }
            public string Codigo_Origen { get; set; }
            public string Origen { get; set; }
            public string Codigo_Destino { get; set; }
            public string Destino { get; set; }
            public string Codigo_Tipo { get; set; }
            public string Tipo { get; set; }
            public string IdUsuarioCreacion { get; set; }
            public string IdUsuarioModificacion { get; set; }
            public int Estado { get; set; }
        }

        public class Detalle_Proyecciones
        {
            public decimal Id_Detalle_Proyeccion { get; set; }
            public decimal Id_Proyeccion { get; set; }
            public decimal S1 { get; set; }
            public decimal S2 { get; set; }
            public decimal S3 { get; set; }
            public decimal S4 { get; set; }
            public decimal S5 { get; set; }
            public decimal S6 { get; set; }
            public decimal S7 { get; set; }
            public decimal S8 { get; set; }
            public decimal S9 { get; set; }
            public decimal S10 { get; set; }
            public decimal S11 { get; set; }
            public decimal S12 { get; set; }
            public decimal S13 { get; set; }
            public decimal S14 { get; set; }
            public decimal S15 { get; set; }
            public decimal S16 { get; set; }
            public decimal S17 { get; set; }
            public decimal S18 { get; set; }
            public decimal S19 { get; set; }
            public decimal S20 { get; set; }
            public decimal S21 { get; set; }
            public decimal S22 { get; set; }
            public decimal S23 { get; set; }
            public decimal S24 { get; set; }
            public decimal S25 { get; set; }
            public decimal S26 { get; set; }
            public decimal S27 { get; set; }
            public decimal S28 { get; set; }
            public decimal S29 { get; set; }
            public decimal S30 { get; set; }
            public decimal S31 { get; set; }
            public decimal S32 { get; set; }
            public decimal S33 { get; set; }
            public decimal S34 { get; set; }
            public decimal S35 { get; set; }
            public decimal S36 { get; set; }
        }

        public class Lista_Proyecciones
        {
            public int Id_Proyeccion { get; set; }
            public string Campania { get; set; }
            public string Tipo { get; set; }
            public string Commodity { get; set; }
            public decimal Cantidad { get; set; }
            public string Origen { get; set; }
            public string Destino { get; set; }
        }

        public class PROYECCION_DETALLE
        {
            public string Campania { get; set; }
            public string Commodity { get; set; }
            public string Origen { get; set; }
            public string Destino { get; set; }
            public string Tipo { get; set; }
            public decimal S1 { get; set; }
            public decimal S2 { get; set; }
            public decimal S3 { get; set; }
            public decimal S4 { get; set; }
            public decimal S5 { get; set; }
            public decimal S6 { get; set; }
            public decimal S7 { get; set; }
            public decimal S8 { get; set; }
            public decimal S9 { get; set; }
            public decimal S10 { get; set; }
            public decimal S11 { get; set; }
            public decimal S12 { get; set; }
            public decimal S13 { get; set; }
            public decimal S14 { get; set; }
            public decimal S15 { get; set; }
            public decimal S16 { get; set; }
            public decimal S17 { get; set; }
            public decimal S18 { get; set; }
            public decimal S19 { get; set; }
            public decimal S20 { get; set; }
            public decimal S21 { get; set; }
            public decimal S22 { get; set; }
            public decimal S23 { get; set; }
            public decimal S24 { get; set; }
            public decimal S25 { get; set; }
            public decimal S26 { get; set; }
            public decimal S27 { get; set; }
            public decimal S28 { get; set; }
            public decimal S29 { get; set; }
            public decimal S30 { get; set; }
            public decimal S31 { get; set; }
            public decimal S32 { get; set; }
            public decimal S33 { get; set; }
            public decimal S34 { get; set; }
            public decimal S35 { get; set; }
            public decimal S36 { get; set; }
        }
    }
}