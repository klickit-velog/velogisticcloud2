﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.Models
{
    public class ItinerarioViewModel
    {
        public ItinerarioViewModel()
        {
            ListaItinerarios = new List<ItinerarioResponseDTO>();
        }
        public string NroContrato { get; set; }
        public int idOrigin { get; set; }
        public string Origin { get; set; }
        public string OriginCode { get; set; }
        public string CodigoPaisOrigen { get; set; }
        public string NombrePaisOrigen { get; set; }
        public int idDestination { get; set; }
        public string Destination { get; set; }
        public string DestinationCode { get; set; }
        public string CodigoPaisDestino { get; set; }
        public string NombrePaisDestino { get; set; }
        public string Sacs { get; set; }
        public int WeeksOut { get; set; }
        public DateTime SearchDate { get; set; }
        public string SearchDateType { get; set; }
        public bool includeNearbyDestinationPort { get; set; }
        public bool includeNearbyOriginPorts { get; set; }

        public bool Directo { get; set; }

        public List<ItinerarioResponseDTO> ListaItinerarios { get; set; }
        public List<SelectListItem> ListaNavieras { get; set; }
    }
}
