﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Seguimiento
{
    public class SeguimientoReserva
    {
        public int IdSeguimiento { get; set; }
        public int IdReserva { get; set; }
        public string CodEstado { get; set; }
        public string DescEstado { get; set; }
        public string FechaIni { get; set; }
        public string FechaFin { get; set; }
    }

    public class SeguimientoOPL
    {
        public int IdSeguimiento { get; set; }
        public int IdOperacionLogistica { get; set; }
        public string CodEstado { get; set; }
        public string DescEstado { get; set; }
        public string FechaIni { get; set; }
        public string FechaFin { get; set; }
    }

    public class DatosCorreoOPL
    {
        public string Codigo_Usuario { get; set; }
        public string OrdenServicio { get; set; }
        public string CanalSini { get; set; }
    }
}
