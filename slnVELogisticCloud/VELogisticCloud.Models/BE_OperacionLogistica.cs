﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models
{
    public class BE_OperacionLogistica
    {
        public int IdCotizacionLogistica { get; set; }
        public int IdOperacionLogistica { get; set; }
        public int IdCotizacionDetalleOdoo { get; set; }

        public string NroCotizacion { get; set; }
        public string NroContenedor { get; set; }
        public string NroBooking { get; set; }
        public string CodigoCommodity { get; set; }
        public string Commodity { get; set; }
        public string CodigoCliente { get; set; }
        public string Cliente { get; set; }
        public string OrdenServicio { get; set; }

        public string Canal { get; set; }
        public string Linea { get; set; }
        public string PuertoDestino { get; set; }
        public string Deposito { get; set; }


        public string Producto { get; set; }
        public string TipoEmbarque { get; set; }
        public string FechaServicio { get; set; }
        public string NroBL { get; set; }
        public string NroViaje { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string Conductor { get; set; }
        public string Nave { get; set; }
        public string ConceptosOPL { get; set; }
        public string Lugar1 { get; set; }
        public string Fecha1 { get; set; }
        public string Hora1 { get; set; }
        public string Lugar2 { get; set; }
        public string Fecha2 { get; set; }
        public string Hora2 { get; set; }

        public string Lugar3 { get; set; }
        public string Fecha3 { get; set; }
        public string Hora3 { get; set; }
        public string EstadoOddo { get; set; }
        public string IdUsuarioCreacion { get; set; }
        public string IdUsuarioModificacion { get; set; }
        public int Estado { get; set; }
    }

    public class Autocomplete
    {
        public int IdCotizacionLogisticaDetalle { get; set; }
        public int IdCotizacionDetalleOdoo { get; set; }
        public string name { get; set; }
    }

    public class ParametrosBusqueda
    {
        public int IdOperacionLogistica { get; set; }
        public string NroContenedor { get; set; }
        public string CodigoBL { get; set; }
        public string NroDAM { get; set; }
        public string NroViaje { get; set; }
        public string NroBooking { get; set; }
        public string Commodity { get; set; }
    }                 		

    public class Busqueda
    {
        public int IdOperacionLogistica { get; set; }
        public string NumeroContenedor { get; set; }
        public int CodigoCommodity { get; set; }
        public string NombreCommodity { get; set; }
        public string Cliente { get; set; }
        public string ConceptosOPL { get; set; }
        public string NroBooking { get; set; }
        public string EstadoOddo { get; set; }
        public string NroViaje { get; set; }
        public string Canal { get; set; }
        public string Linea { get; set; }
        public string Deposito { get; set; }
        public string Packing { get; set; }
        public string OperadorLogistico { get; set; }
        public string NroBL { get; set; }
        public string Nave { get; set; }
        public string PuertoDestino { get; set; }
        public string OrdenServicio { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string AlmacenRetiro { get; set; }
        public string AlmacenIngreo { get; set; }
        //public string Senasa { get; set; }
        //public string Aduana { get; set; }
        //public string Embarque { get; set; }
        //public int val_senasa { get; set; }
        //public int val_aduana { get; set; }
        //public int val_embarque { get; set; }
        public int IdEstadoOddo { get; set; }
        public string NombreServicio { get; set; }


    }

    public class ParametroUpdate
    {
        public int IdOperacionLogistica         {get;set;}
        public string NroContenedor             {get;set;}
        public string NroBooking                {get;set;}
        public string OrdenServicio             {get;set;}
        public string CodigoCommodity           {get;set;}
        public string Commodity                 {get;set;}
        public int MovSenasa                 {get;set;}
        public int MovAduana                 {get;set;}
        public int ServicioEmbarque              {get;set;}
        public string ConceptosOPL              {get;set;}
        public int IdEstadoOddo              {get;set;}
        public string EstadoOddo                {get;set;}
        public string IdUsuarioModificacion { get; set; }
    }
}
