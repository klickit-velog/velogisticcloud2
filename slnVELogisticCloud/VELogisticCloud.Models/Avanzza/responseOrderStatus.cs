﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace VELogisticCloud.Models.EntidadJSON
{
    public class responseOrderStatus
    {
        public payload payload { get; set; }

    }

    public class trip_interval
    {
        public List<orders> orders { get; set; }
    }

    public class orders
    {
        public string code { get; set; }
    }

    public class payload
    {
        public int id { get; set; }
        public string status_code { get; set; }
        public string status { get; set; }
        public trip_interval trip_interval { get; set; }
        public string status_id { get; set; }
        public string datetime { get; set; }
    }
}
