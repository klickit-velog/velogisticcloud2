﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models
{
    public class Estado
    {
        public int IdEstado { get; set; }
        public string Nombre { get; set; }
    }
}
