﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.General
{
    public class BE_ItinerarioManual
    {
        public int IdItinerarioManual { get; set; }
        public string scac { get; set; }
        public string carrierName { get; set; }
        public string terminalCutoff { get; set; }
        public string vesselName { get; set; }
        public string voyageNumber { get; set; }
        public string imoNumber { get; set; }
        public string originUnloc { get; set; }
        public string originCityName { get; set; }
        public string originSubdivision { get; set; }
        public string originCountry { get; set; }
        public string originDepartureDate { get; set; }
        public string destinationUnloc { get; set; }
        public string destinationCityName { get; set; }
        public string destinationCountry { get; set; }
        public string destinationArrivalDate { get; set; }
        public string totalDuration { get; set; }
        public string legs { get; set; }
        public string sequence { get; set; }
        public string serviceName { get; set; }
        public string transportName  { get; set; }
        public string conveyanceNumber { get; set; }
        public string departureCityName { get; set; }
        public string departureCountry { get; set; }
        public string departureDate { get; set; }
        public string arrivalCityName { get; set; }
        public string arrivalCountry { get; set; }
        public string arrivalDate { get; set; }
        public string transitDuration { get; set; }
        

    }
}
