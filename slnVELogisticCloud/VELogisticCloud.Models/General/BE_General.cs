﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.General
{
   public class BE_Valores
    {
        public int ID { get; set; }
        public string Cod { get; set; }
        public string Descripcion { get; set; }
    }

    public class BE_Variables
    {
        public string Variable { get; set; }
        public int Activado { get; set; }
    }

}
