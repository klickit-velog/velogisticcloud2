﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Booking
{
    public class BookingTOoddo
    {
        public string Codigo_Usuario { get; set; }
        public int Codigo_Commodity { get; set; }
        public int Id_Puerto_Carga { get; set; }
        public int Id_Puerto_Descarga { get; set; }
        public int Cantidad { get; set; }
        public string ETA { get; set; }
        public string ETD { get; set; }
        public string ETA_Destino { get; set; }
        public string Tipo_Contenedor { get; set; }
        public string Nota { get; set; }
        public int Id_Cotizacion_Maritimo_Detalle { get; set; }
        public int Id_Cotizacion_Maritimo { get; set; }
        public string Nombre_Nave { get; set; }
        public string TipoTemperatura { get; set; }
        public string Temperatura { get; set; }
        public string Humedad { get; set; }
        public string TipoHumedad { get; set; }
        public string CO2 { get; set; }
        public string O2 { get; set; }
        public string ColdTreatment { get; set; }
        public decimal Peso { get; set; }
        public string ID_IMO { get; set; }
        public string IMO { get; set; }
        public string UN1 { get; set; }
        public string UN2 { get; set; }
        public int NumeroCotizacion { get; set; }

        public string CodigoNaviera { get; set; }
        public string NumeroViaje { get; set; }
        public string Consignatario { get; set; }
        public string Descripcion_Carga { get; set; }
        public string Condicion { get; set; }
        public string Ventilacion { get; set; }
        public string TipoVentilacion { get; set; }
        public string Atmosfera { get; set; }
        public string Nro_Booking { get; set; }
    }
}
