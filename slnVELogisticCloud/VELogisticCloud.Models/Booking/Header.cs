﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Booking
{
    public class Header
    {
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public string RequestDateTimeStamp { get; set; }
        public string RequestMessageVersion { get; set; }
        public string TransactionType { get; set; }
        public string TransactionVersion { get; set; }
        public string DocumentIdentifier { get; set; }
        public string TransactionStatus { get; set; }
        public string TransactionSplitIndicator { get; set; }
    }

    public class Cabecera
    {
        public string title { get; set; }
        public string detalle { get; set; }
    }
}
