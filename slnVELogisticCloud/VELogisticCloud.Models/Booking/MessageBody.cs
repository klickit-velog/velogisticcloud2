﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Booking
{
    public class MessageBody
    {
        public class MessageProperties
        {
            public string ShipmentID { get; set; }
            public class ConfirmedWith
            {
                public string Type { get; set; }
                public string Name { get; set; }
                public class CommunicationDetails
                {
                    public string Phone { get; set; }
                    public string Email { get; set; }
                }
            }

            public string Datetime { get; set; }
            public string DateType { get; set; }
            public string MovementType { get; set; }
            public class ReferenceInformation
            {
                public string Type { get; set; }
                public string Value { get; set; }

            }

            public class TransportationDetails
            {
                public string TransportStage { get; set; }
                public string TransportMode { get; set; }

                public class ConveyanceInformation
                {
                    public class Identifier
                    {
                        public string Type { get; set; }
                        public string Value { get; set; }
                    }
                }

                public class Location
                {
                    public string Type { get; set; }
                    public string IdentifierType { get; set; }
                    public string IdentifierValue { get; set; }
                    public string Name { get; set; }
                    public string Subdivision { get; set; }
                    public string CountryName { get; set; }
                    public string CountryCode { get; set; }
                    public string DateType { get; set; }
                    public string DateTimeValue { get; set; }
                }

            }

            public class Party
            {
                public string Role { get; set; }
                public string Name { get; set; }
                public string IdentifierType { get; set; }
                public string IdentifierValue { get; set; }
                public class Address
                {
                    public string StreetAddress { get; set; }
                    public string CityName { get; set; }
                    public string PostalCode { get; set; }
                    public string CountryCode { get; set; }
                    public string CountryName { get; set; }
                }
                public class Contacts
                {
                    public string Type { get; set; }
                    public string Name { get; set; }
                    public class ComunicationDetails
                    {
                        public string Phone { get; set; }
                        public string Email { get; set; }
                    }
                }
            }

        }
        public class MessageDetails
        {
            public class EquipmentDetails
            {
                public string EquipmentSupplier { get; set; }
                public string NumberOfEquipment { get; set; }
                public class EquipmentType
                {
                    public string EquipmentTypeCode { get; set; }
                    public string EquipmentDescription { get; set; }
                }
                public class ImportExportHaulage
                {
                    public string CargoMovementType { get; set; }
                    public string HaulageArrangements { get; set; }
                }
            }
        }

    }
}
