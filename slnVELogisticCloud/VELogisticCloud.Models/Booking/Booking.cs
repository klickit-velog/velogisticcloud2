﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Booking
{
    public class InsertBooking
    {

        public int IdBooking { get; set; }
        public string ExportadorNombre { get; set; }
        public string ExportadorDomicilio { get; set; }
        public string ExportadorRuc         { get; set; }
        public string ExportadorContacto { get; set; }
        public string ExportadorTelefono { get; set; }
        public string ExportadorFax { get; set; }
        public string ExportadorCelular	          {get;set;}

        public int IdConsignatario       {get;set;}
        public int IdNotify              {get;set;}
        public int IdLinea               {get;set;}
        public string CodLinea              {get;set;}
        public string Linea                 {get;set;}
        public string NombreNave { get;set;}
        public string NroViaje { get;set;}
        public string NroContrato { get;set; }
        public string CodigoPaisEmbarque    {get;set;}
        public string PaisEmbarque          {get;set;}
        public int IdPuertoEmbarque      {get;set;}
        public string CodigoPuertoEmbarque  {get;set;}
        public string PuertoEmbarque        {get;set;}
        public string CodigoPaisDescarga    {get;set;}
        public string PaisDescarga          {get;set;}
        public int IdPuertoDescarga      {get;set;}
        public string CodigoPuertoDescarga  {get;set;}
        public string PuertoDescarga        {get;set;}
        public DateTime ETA                   {get;set;}
        public DateTime ETD                   {get;set;}
        public string ClaseContenedor       {get;set;}
        public string TipoContenedor        {get;set;}
        public int AtmosferaControlada   {get;set;}
        public decimal CO2                   {get;set;}
        public decimal O2                    {get;set;}
        public int ColdTreatment         {get;set;}
        public string TipoTemperatura       {get;set;}
        public decimal Temperatura           {get;set;}
        public string TipoVentilacion       {get;set;}
        public decimal Ventilacion           {get;set;}
        public int TipoHumedad           {get;set;}
        public decimal Humedad               {get;set;}
        public int IdImo                 {get;set;}
        public string IMO                   {get;set;}
        public string UN1                   {get;set;}
        public string UN2                   {get;set;}
        public int IdCommodity           {get;set;}
        public string CodigoCommodity       {get;set;}
        public string Commodity             {get;set;}
        public string Variedad              {get;set;}
        public string PartidaArancelaria	{get;set;}		
        public string TipoBulto             {get;set;}
        public decimal CantidadBulto         {get;set;}
        public decimal PesoBruto             {get;set;}
        public int IdCondicionPago       {get;set;}
        public string CondicionPago         {get;set;}
        public int IdEmisionBL           {get;set;}
        public string EmisionBL             {get;set;}
        public string NombrePagador         {get;set;}
        public string DireccionPagador      {get;set;}
        public string OperadorLogistico     {get;set;}
        public string RucOperador           {get;set;}
        public string Nota                  {get;set;}
        public int EstadoVE { get; set; }
        public string EstadoInttra { get; set; }
        public string Cliente { get;set;}
        public string UsuarioCreacion       {get;set;}
        public DateTime FechaCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public string ConsignatarioNombre { get; set; }
        public string ConsignatarioDireccion { get; set; }
        public string ConsignatarioFax { get; set; }
        public string ConsignatarioTelefono { get; set; }
        public string ConsignatarioContacto { get; set; }
        public string NotifyNombre { get; set; }
        public string NotifyDireccion { get; set; }
        public string NotifyFax { get; set; }
        public string NotifyTelefono { get; set; }
        public string NotifyContacto { get; set; }
        public string NroBooking { get; set; }
        public string EstadoVE_Desc { get; set; }
        public int IdDetalleCotizacionOdoo { get; set; }
        public int IdTipoContenedor { get; set; }
        public string ImoInttra { get; set; }
        public string CantidadBooking { get; set; }
        public string NumeroReferenciaInttra { get; set; }
        public int IdContactOwner { get; set; }
        public string ContactOwner { get; set; }
        public string NombreCustomer { get; set; }
        public string tipoContrato { get; set; }
        public string Tecnologia { get; set; }
        public string Valido_Desde { get; set; }
        public string Valido_Hasta { get; set; }

    }

    public class InsertConsignatario
    {
        public int      IdConsignatario { get; set; }
        public string   Nombre          { get; set; }
        public string   Direccion           { get; set; }
        public string   Fax             { get; set; }
        public string   Telefono        { get; set; }
        public string   PersonaContacto { get; set; }
    }

    public class InsertNotify
    {
        public int IdNotify { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Fax { get; set; }
        public string Telefono { get; set; }
        public string PersonaContacto { get; set; }
    }

    public class DatosCotizacionMaritima
    {
        public int Id_Cotizacion_Maritimo { get; set; }
        public int Id_detalle_cotizacion_maritimo { get; set; }
        public string Codigo_Linea_Naviera { get; set; }
        public string NumeroContrato { get; set; }
        public int Cantidad_Contenedor { get; set; }
        public string Codigo_Tipo_Contenedor { get; set; }
        public string Codigo_Commodity { get; set; }
        public string Codigo_Puerto_Embarque { get; set; }
        public string Codigo_Puerto_Descarga { get; set; }
        public string Pais_Puerto_Descarga { get; set; }
        public string Pais_Puerto_Embarque { get; set; }
        public string Codigo_Pais_Embarque { get; set; }
        public string Codigo_Pais_Descarga { get; set; }
        public string Puerto_Embarque { get; set; }
        public string Puerto_Descarga { get; set; }
        public string Commodity { get; set; }
        public int Id_Puerto_Carga { get; set; }
        public int Id_Puerto_Descarga { get; set; }
        public int IdOddoService { get; set; }
        public string Valido_Desde { get; set; }
        public string Valido_Hasta { get; set; }



    }

    public class ExcelBooking
    {
        public int IdBooking { get; set; }
        public string ExportadorNombre { get; set; }
        public string ExportadorDomicilio { get; set; }
        public string ExportadorRuc { get; set; }
        public string ExportadorContacto { get; set; }
        public string ExportadorTelefono { get; set; }
        public string ExportadorFax { get; set; }
        public string ExportadorCelular { get; set; }
        public string NombreConsignatario { get; set; }
        public string DireccionConsignatario { get; set; }
        public string FaxConsignatario { get; set; }
        public string TelefonoConsignatario { get; set; }
        public string PersonaContactoConsignatario { get; set; }
        public string NombreNotify { get; set; }
        public string DireccionNotify { get; set; }
        public string FaxNotify { get; set; }
        public string TelefonoNotify { get; set; }
        public string PersonaContactoNotify { get; set; }
        public string CodLinea { get; set; }
        public string NombreNave { get; set; }
        public string NroViaje { get; set; }
        public string NroContrato { get; set; }
        public string PaisEmbarque { get; set; }
        public string PuertoEmbarque { get; set; }
        public string PaisDescarga { get; set; }
        public string PuertoDescarga { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string ClaseContenedor { get; set; }
        public string TipoContenedor { get; set; }
        public string CO2 { get; set; }
        public string O2 { get; set; }
        public string TipoTemperatura { get; set; }
        public string Temperatura { get; set; }
        public string TipoVentilacion { get; set; }
        public string Ventilacion { get; set; }
        public string Humedad { get; set; }
        public string UN1 { get; set; }
        public string UN2 { get; set; }
        public string Commodity { get; set; }
        public string Variedad { get; set; }
        public string PartidaArancelaria { get; set; }
        public string TipoBulto { get; set; }
        public string CantidadBulto { get; set; }
        public string PesoBruto { get; set; }
        public string CondicionPago { get; set; }
        public string EmisionBL { get; set; }
        public string OperadorLogistico { get; set; }
        public string RucOperador { get; set; }
        public string Nota { get; set; }
        public string Estado { get; set; }
        public string Cliente { get; set; }
        public string UsuarioCreacion { get; set; }
        public string NroBooking { get; set; }
        public string IdOdoo { get; set; }
        public string ImoInttra { get; set; }
        public string CantidadBooking { get; set; }
    }

    public class NuevaListaBooking
    {
        public int IdBooking { get; set; }
        public string ContactOwner { get; set; }
        public string ExportadorNombre { get; set; }
        public string Linea { get; set; }
        public string NroContrato { get; set; }
        public string NroBooking { get; set; }
        public string Estado { get; set; }
        public string NombreNave { get; set; }
        public string NroViaje { get; set; }
        public string PuertoEmbarque { get; set; }
        public string PuertoDescarga { get; set; }
        public string Commodity { get; set; }
        public string Cliente { get; set; }
        public string ETD { get; set; }
        public string NumeroBL { get; set; }
        public string FechaSolicitud { get; set; }
        public string NombreConsignatario { get; set; }
        public string NombreNotify { get; set; }
        public string OperadorLogistico { get; set; }
        public string TiempoTransito { get; set; }
        public string ETA { get; set; }
        public string TipoContenedor { get; set; }
        public string ClaseContenedor { get; set; }
        public string ColdTreatment { get; set; }
        public int CantidadContenedor { get; set; }
        public string AtmosferaControlada { get; set; }
        public string PaisDescarga { get; set; }
        public string CO2 { get; set; }
        public string O2 { get; set; }
        public string TipoTemperatura { get; set; }
        public decimal Temperatura { get; set; }
        public string TipoVentilacion { get; set; }
        public decimal Ventilacion { get; set; }
        public decimal Humedad { get; set; }
        public string UN1 { get; set; }
        public string UN2 { get; set; }
        public string ExportadorDomicilio { get; set; }
        public string ExportadorRuc { get; set; }
        public string ExportadorContacto { get; set; }
        public string ExportadorTelefono { get; set; }
        public string ExportadorFax { get; set; }
        public string ExportadorCelular { get; set; }
        public string DireccionConsignatario { get; set; }
        public string FaxConsignatario { get; set; }
        public string TelefonoConsignatario { get; set; }
        public string PersonaContactoConsignatario { get; set; }
        public string DireccionNotify { get; set; }
        public string FaxNotify { get; set; }
        public string TelefonoNotify { get; set; }
        public string PersonaContactoNotify { get; set; }
        public string PaisEmbarque { get; set; }
        public string Variedad { get; set; }
        public string PartidaArancelaria { get; set; }
        public string TipoBulto { get; set; }
        public decimal CantidadBulto { get; set; }
        public decimal PesoBruto { get; set; }
        public string CondicionPago { get; set; }
        public string EmisionBL { get; set; }
        public string RucOperador { get; set; }
        public string Nota { get; set; }
        public string UsuarioCreacion { get; set; }
        public string ImoInttra { get; set; }
        public string CantidadBooking { get; set; }
        public string NombreCustomer { get; set; }
        public string TipoContrato { get; set; }

        public int IdCommodity { get; set; }
        public int IdPuertoEmbarque { get; set; }
        public int IdPuertoDescarga { get; set; }
        public int IdEstado { get; set; }
        public int IdContactOwner { get; set; }
        public string DateETD { get; set; }


    }

    public class ParametrosBusquedaNuevo
    {
        public int IdBooking { get; set; }
        public string NroBooking { get; set; }
        public int IdContactOwner { get; set; }
        public int IdLinea { get; set; }
        public string Linea { get; set; }
        public string NombreNave { get; set; }
        public string NroViaje { get; set; }
        public int IdEmbarque { get; set; }
        public int IdDescarga { get; set; }
        public int IdComodity { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int EstadoId { get; set; }

    }
}
