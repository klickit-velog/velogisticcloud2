﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.OperacionLogistica
{
    public class BE_CotizacionDetalleOdoo
    {
        public string NombreTerminalDepositoVacio { get; set; }
        public string NombreTerminalRetiroVacio { get; set; }
    }
    public class BE_OperacionLogistica2
    {
        public int IdOperacionLogistica {get;set;}
        public string NroBooking { get; set; }
        public int IdLinea { get; set; }
        public string CodLinea { get; set; }
        public string Linea { get; set; }
        public string NombreNave { get; set; }
        public DateTime ETA { get; set; }
        public string CodigoPaisEmbarque { get; set; }
        public string PaisEmbarque { get; set; }
        public int IdPuertoEmbarque { get; set; }
        public string CodigoPuertoEmbarque { get; set; }
        public string PuertoEmbarque { get; set; }
        public string CodigoPaisDescarga { get; set; }
        public string PaisDescarga { get; set; }
        public int IdPuertoDescarga { get; set; }
        public string CodigoPuertoDescarga { get; set; }
        public string PuertoDescarga { get; set; }
        public string MarcasProducto { get; set; }
        public decimal PesoCaja { get; set; }
        public string CantidadPaletas { get; set; }
        public string CondicionPago { get; set; }
        public decimal PesoNeto { get; set; }
        public string ValorFob { get; set; }
        public int IdIncoterm { get; set; }
        public string CodIncoterm { get; set; }
        public string Incoterm { get; set; }
        public decimal CantidadTermoregistros { get; set; }
        public decimal CantidadFiltrosEtileno { get; set; }
        public string Observaciones { get; set; }
        public int IdOrigenProducto { get; set; }
        public string OrigenProducto { get; set; }
        public DateTime CitaPacking { get; set; }
        public DateTime? HoraInspeccionSenasa { get; set; } 
        public string RazonSocialPacking { get; set; }
        public string RucPacking { get; set; }
        public string DireccionPacking { get; set; }
        public int Drawback { get; set; }
        public int IdDepositoVacio { get; set; } 
        public string DepositoVacio { get; set; }
        public string CodigoPaisEmbarqueInttra { get; set; }
        public string PaisEmbarqueInttra { get; set; }
        public int IdPuertoEmbarqueInttra { get; set; }
        public string CodigoPuertoEmbarqueInttra { get; set; }
        public string PuertoEmbarqueInttra { get; set; }
        public string OrdenServicio         { get; set; }
        public int IdEstadoVE            { get; set; }
        public string UsuarioCreacion       { get; set; }
        public DateTime FechaCreacion         { get; set; }
        public string UsuarioModificacion   { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string EstadoVE { get; set; }
        public int IdCotizacionOddo { get; set; }
        public string Placa { get; set; }
        public string Chofer { get; set; }
        public string Codigo_Usuario { get; set; }
        public decimal Flete { get; set; }
        public string CanalSini { get; set; }
        public string Transporte { get; set; }
        public int IdTransporte { get; set; }
        public int IdCondicionPago { get; set; }
        public string Nombre { get; set; }
        public int IdConsigne { get; set; }
        public string TelefonoConsigne { get; set; }
        public string Tecnologia { get; set; }

        public string ExportadorNombre { get; set; }
        public string Commodity { get; set; }
        public string NroViaje { get; set; }
        public string OperadorLogistico { get; set; }
        public string TipoContenedor { get; set; }

    }

    public class BE_DatosPedido
    {
        public int IdOperacionLogistica { get; set; }
        public string ExportadorRuc { get; set; }
        public string ExportadorNombre { get; set; }
        public string Producto { get; set; }
        public string RazonSocialPacking { get; set; }
        public string DireccionPacking { get; set; }
        public string OperadorLogistico { get; set; }
        public string NroBooking { get; set; }
        public string TerminalPortuario { get; set; }
        public string DepositoVacio { get; set; }
        public string FechaPacking { get; set; }
        public string HoraPacking { get; set; }
        public string FechaSenasa { get; set; }
        public string HoraSenasa { get; set; }
        public string NombreNave { get; set; }
        public string NroViaje { get; set; }
        public string Linea { get; set; }
        public decimal CantidadTermoregistros { get; set; }
        public decimal CantidadFiltrosEtileno { get; set; }
        public string NombreContactoPacking { get; set; }
        public string TelefonoContacto { get; set; }
        public string TipoContrato { get; set; }
        public decimal CantidadContenedores { get; set; }
        public string PuertoDestino { get; set; }
        public string ETD { get; set; }
        public string TipoEmbarque { get; set; }
        public string PuertoEmbarque { get; set; }
        public string Observaciones { get; set; }
        public string DireccionCliente { get; set; }
        public string CorreoCliente { get; set; }
        
    }
}
