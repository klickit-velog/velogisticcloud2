﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VELogisticCloud.Models
{
    public class ItinerarioSolicitud
    {
        [Key]
        public int Id_Reserva { get; set; }

        [Required(ErrorMessage = "Ingrese un nombre para la naviera.")]
        public string Naviera { get; set; }
        public string Nro_contrato { get; set; }
        public string Expeditor { get; set; }
        public string Consignatario { get; set; }
        public string Puerto_Embarque { get; set; }
        public string Puerto_Descarga { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public string Nombre_Nave { get; set; }
        public string Nave_Viaje { get; set; }
        public string Cantidad { get; set; }
        public string Tipo_Contenedor { get; set; }
        public string Codigo_Commodity { get; set; }
        public string Comodity { get; set; }
        public string Descripcion_Carga { get; set; }
        public string Temperatura { get; set; }
        public string Ventilacion { get; set; }
        public string Humedad { get; set; }
        public string Peso { get; set; }
        public string Volumen { get; set; }
        public string Flete { get; set; }
        public string Emision_BL { get; set; }
    }
}
