﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.ApiAvanzza
{

    public class PedidosResponse
    {
        public int id { get; set; }
        public string code { get; set; }
        public object description { get; set; }
        public object location_start_id { get; set; }
        public object start_lat { get; set; }
        public object start_lng { get; set; }
        public object start_address { get; set; }
        public object client_id { get; set; }
        public string client_name { get; set; }
        public object client_document { get; set; }
        public string client_email { get; set; }
        public string client_phone { get; set; }
        public object time_window { get; set; }
        public int service_time { get; set; }
        public int is_overtime_notify { get; set; }
        public object notes { get; set; }
        public string price { get; set; }
        public string weight { get; set; }
        public string volume { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public object location_id { get; set; }
        public string client_address { get; set; }
        public string client_lat { get; set; }
        public string client_lng { get; set; }
        public object user_responsible_id { get; set; }
        public string state { get; set; }
        public Extra_Data extra_data { get; set; }
        public int attempts { get; set; }
        public object order_clone_id { get; set; }
        public int is_visible { get; set; }
        public object deleted_at { get; set; }
        public object closing_code { get; set; }
        public string[] tags_array { get; set; }
        public Tag[] tags { get; set; }
    }

    public class Extra_Data
    {
        public string custom_1 { get; set; }
        public string custom_2 { get; set; }
        public string custom_3 { get; set; }
        public string custom_4 { get; set; }
        public string custom_12 { get; set; }
        public string custom_18 { get; set; }
        public DateTime custom_22 { get; set; }
        public string custom_26 { get; set; }
        public string custom_27 { get; set; }
        public string custom_28 { get; set; }
        public string custom_29 { get; set; }
        public string custom_30 { get; set; }
        public string custom_31 { get; set; }
        public string custom_32 { get; set; }
        public string custom_33 { get; set; }
        public string custom_36 { get; set; }
        public string custom_37 { get; set; }
    }

    public class Tag
    {
        public int id { get; set; }
        public Name name { get; set; }
        public Slug slug { get; set; }
        public string type { get; set; }
        public int order_column { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public Pivot pivot { get; set; }
    }

    public class Name
    {
        public string esPE { get; set; }
    }

    public class Slug
    {
        public string esPE { get; set; }
    }

    public class Pivot
    {
        public int taggable_id { get; set; }
        public int tag_id { get; set; }
        public string taggable_type { get; set; }
    }
}
