﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.ApiAvanzza
{
    public class Vehiculos
    {
        public class Rootobject
        {
            public int current_page { get; set; }
            public Datum[] data { get; set; }
            public string first_page_url { get; set; }
            public int from { get; set; }
            public int last_page { get; set; }
            public string last_page_url { get; set; }
            public Link[] links { get; set; }
            public string next_page_url { get; set; }
            public string path { get; set; }
            public int per_page { get; set; }
            public object prev_page_url { get; set; }
            public int to { get; set; }
            public int total { get; set; }
        }

        public class Datum
        {
            public int id { get; set; }
            public int? provider_id { get; set; }
            public string plate { get; set; }
            public int has_motor { get; set; }
            public int is_load_type { get; set; }
            public int? axes_number { get; set; }
            public int max_volume { get; set; }
            public int max_weight { get; set; }
            public int? tires_number { get; set; }
            public string status { get; set; }
            public string gps_imei { get; set; }
            public bool cellphone_gps { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public object driver_id { get; set; }
            public object extra_data { get; set; }
            public object vehicle_type { get; set; }
            public Acl acl { get; set; }
            public object[] tags_array { get; set; }
            public Custom_Properties custom_properties { get; set; }
            public object[] zone_ids { get; set; }
            public Provider provider { get; set; }
            public object driver_default { get; set; }
            public object[] tags { get; set; }
            public object[] zones { get; set; }
        }

        public class Acl
        {
            public bool update { get; set; }
            public bool delete { get; set; }
        }

        public class Custom_Properties
        {
            public object custom_13 { get; set; }
            public string custom_14 { get; set; }
            public string custom_15 { get; set; }
            public object custom_16 { get; set; }
            public object custom_17 { get; set; }
            public object custom_18 { get; set; }
            public object custom_19 { get; set; }
        }

        public class Provider
        {
            public int id { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public string document_type_code { get; set; }
            public string document_number { get; set; }
            public object[] extra_data { get; set; }
            public object address { get; set; }
            public object phone { get; set; }
            public object email { get; set; }
            public object commercial_representative { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
        }

        public class Link
        {
            public string url { get; set; }
            public string label { get; set; }
            public bool active { get; set; }
        }

    }

}

