﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VELogisticCloud.Model.Identity
{
    public class ApplicationUser:IdentityUser
    {
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "La Razón Social es requerida.")]
        public string RazonSocial { get; set; }

        [Required(ErrorMessage = "El RUC es requerido")]
        public string RUC { get; set; }

        [Required(ErrorMessage = "El número de contacto es requerido")]
        public string NumeroContacto { get; set; }

        [Required(ErrorMessage = "El nombre de contacto es requerido")]
        public string NombreContacto { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? IdUsuarioActualiza { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public int IdEstado { get; set; }
    }
}
