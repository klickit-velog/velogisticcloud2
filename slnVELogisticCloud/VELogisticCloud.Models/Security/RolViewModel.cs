﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VELogisticCloud.Models.Security
{
    public class RolViewModel
    {
        [Required(ErrorMessage ="Nombre es obligatorio")]
        public string Nombre { get; set; }
    }
}
