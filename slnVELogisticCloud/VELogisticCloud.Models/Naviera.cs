﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VELogisticCloud.Models
{
    [NotMapped]
    public class Naviera
    {
        [Key]
        public int IdNaviera { get; set; }

        [Required(ErrorMessage = "Ingrese un nombre para la naviera..")]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        [Required(ErrorMessage = "Ingrese un código para el puerto.")]
        public string Codigo { get; set; }
    }
}
