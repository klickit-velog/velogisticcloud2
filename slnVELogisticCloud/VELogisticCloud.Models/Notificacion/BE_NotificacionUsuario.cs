﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Models.Notificacion
{
    public class BE_NotificacionUsuario
    {
       public int IdNotificacion     {get;set;}
       public int IdUsuario          {get;set;}
       public string CodUsuario         {get;set;}
       public string CorreoUsuario      {get;set;}
       public string NombreUsuario      {get;set;}
       public int IdTipoNotificacion {get;set;}
       public string NombreNotificacion {get;set;}
       public string UsuarioCreacion    {get;set;}
       public int Activo             {get;set;}
       public DateTime FechaCreacion      {get;set;}
       public string UsuarioModificacion{get;set;}
       public DateTime FechaModificacion { get; set; }

    }
}
