﻿using System;
using System.IO;
using System.Net;
//using WebJobs.Repositories;

namespace WebJobs
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var url = $"https://vecloudapi.azurewebsites.net/api/Inttra/OnInttra";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";

                try
                {
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream strReader = response.GetResponseStream())
                        {
                            if (strReader == null) return;
                            using (StreamReader objReader = new StreamReader(strReader))
                            {
                                string responseBody = objReader.ReadToEnd();
                                Console.WriteLine(responseBody);
                            }
                        }
                    }
                }
                catch (WebException ex)
                {
                    // Handle error
                }
                //BL.listFiles();
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, "Error InttraController/OnInttra");
            }

        }
    }
}
