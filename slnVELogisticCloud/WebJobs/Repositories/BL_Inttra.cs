﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using WebJobs.Data;
using static WebJobs.Models.BE_Inttra;
using VLOG = WebJobs.Models.VLOG;

namespace WebJobs.Repositories
{
    public class BL_Inttra
    {
        private static void LeerXML(string ruta)
        {

            #region Inicializar Variables

            Header header = new Header();
            //MessageBody messageBody = new MessageBody();
            MessageBody.MessageProperties messageProperties = new MessageBody.MessageProperties();
            MessageBody.MessageProperties.ConfirmedWith confirmedWith = new MessageBody.MessageProperties.ConfirmedWith();
            MessageBody.MessageProperties.ConfirmedWith.CommunicationDetails communicationDetails = new MessageBody.MessageProperties.ConfirmedWith.CommunicationDetails();
            MessageBody.MessageProperties.ReferenceInformation referenceInformation = new MessageBody.MessageProperties.ReferenceInformation();
            List<MessageBody.MessageProperties.ReferenceInformation> ListareferenceInformation = new List<MessageBody.MessageProperties.ReferenceInformation>();
            MessageBody.MessageProperties.TransportationDetails.ConveyanceInformation.Identifier identifier = new MessageBody.MessageProperties.TransportationDetails.ConveyanceInformation.Identifier();
            List<MessageBody.MessageProperties.TransportationDetails.ConveyanceInformation.Identifier> listaidentifier = new List<MessageBody.MessageProperties.TransportationDetails.ConveyanceInformation.Identifier>();
            MessageBody.MessageProperties.TransportationDetails.Location location = new MessageBody.MessageProperties.TransportationDetails.Location();
            List<MessageBody.MessageProperties.TransportationDetails.Location> listalocation = new List<MessageBody.MessageProperties.TransportationDetails.Location>();
            //XmlTextReader reader2 = new XmlTextReader(@"E:\Trabajo\Klickit\Archivos\IFTMBC2_9289771823_20210222170025.xml");
            MessageBody.MessageProperties.Party party = new MessageBody.MessageProperties.Party();
            List<MessageBody.MessageProperties.Party> listaparty = new List<MessageBody.MessageProperties.Party>();
            MessageBody.MessageProperties.Party.Address address = new MessageBody.MessageProperties.Party.Address();
            MessageBody.MessageProperties.Party.Contacts contacts = new MessageBody.MessageProperties.Party.Contacts();
            MessageBody.MessageProperties.Party.Contacts.ComunicationDetails comunicationDetailsParty = new MessageBody.MessageProperties.Party.Contacts.ComunicationDetails();
            MessageBody.MessageDetails.EquipmentDetails equipmentDetails = new MessageBody.MessageDetails.EquipmentDetails();
            MessageBody.MessageDetails.EquipmentDetails.EquipmentType equipmentType = new MessageBody.MessageDetails.EquipmentDetails.EquipmentType();
            MessageBody.MessageDetails.EquipmentDetails.ImportExportHaulage importExportHaulage = new MessageBody.MessageDetails.EquipmentDetails.ImportExportHaulage();

            #endregion

            var RutaDocumento = ruta;

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            try
            {
                doc.Load(RutaDocumento);

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }
            //XmlNode node = doc.ReadNode(reader2);


            using (XmlReader reader = XmlReader.Create(@RutaDocumento))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag  
                        switch (reader.Name.ToString())
                        {
                            #region Header
                            case "SenderId":
                                header.SenderId = reader.ReadString();
                                break;
                            case "ReceiverId":
                                header.ReceiverId = reader.ReadString();
                                break;
                            case "RequestDateTimeStamp":
                                header.RequestDateTimeStamp = reader.ReadString();
                                break;
                            case "RequestMessageVersion":
                                header.RequestMessageVersion = reader.ReadString();
                                break;
                            case "TransactionType":
                                header.TransactionType = reader.ReadString();
                                break;
                            case "TransactionVersion":
                                header.TransactionVersion = reader.ReadString();
                                break;
                            case "DocumentIdentifier":
                                header.DocumentIdentifier = reader.ReadString();
                                break;
                            case "TransactionStatus":
                                header.TransactionStatus = reader.ReadString();
                                break;
                            case "TransactionSplitIndicator":
                                header.TransactionSplitIndicator = reader.ReadString();
                                break;
                                #endregion
                        }


                    }

                }
            }

            #region Body

            XmlNodeList elemList1 = doc.GetElementsByTagName("ShipmentID");
            for (int i = 0; i < elemList1.Count; i++)
            {
                messageProperties.ShipmentID = elemList1[i].InnerText;
            }

            XmlNodeList elemList2 = doc.GetElementsByTagName("ConfirmedWith");
            for (int i = 0; i < elemList2.Count; i++)
            {
                XmlNodeList detlist = elemList2[i].ChildNodes;
                for (int j = 0; j < detlist.Count; j++)
                {
                    if (detlist[j].Name == "CommunicationDetails")
                    {
                        XmlNodeList detdetlist = detlist[j].ChildNodes;
                        for (int k = 0; k < detdetlist.Count; k++)
                        {
                            if (detdetlist[k].Name == "Phone")
                            {
                                communicationDetails.Phone = detdetlist[k].InnerText;
                            }
                            else
                            {
                                communicationDetails.Email = detdetlist[k].InnerText;
                            }
                        }
                    }
                    else
                    {
                        if (detlist[j].Name == "Type")
                            confirmedWith.Type = detlist[j].InnerText;
                        else
                            confirmedWith.Name = detlist[j].InnerText;
                    }

                }
            }

            XmlNodeList elemList = doc.GetElementsByTagName("DateTime");
            for (int i = 0; i < elemList.Count; i++)
            {
                messageProperties.DateType = elemList[i].Attributes["DateType"].Value;
                messageProperties.Datetime = elemList[i].InnerText;
            }

            XmlNodeList elemList3 = doc.GetElementsByTagName("MovementType");
            for (int i = 0; i < elemList3.Count; i++)
            {
                messageProperties.MovementType = elemList3[i].InnerText;
            }

            XmlNodeList elemList4 = doc.GetElementsByTagName("ReferenceInformation");
            for (int i = 0; i < elemList4.Count; i++)
            {
                referenceInformation = new MessageBody.MessageProperties.ReferenceInformation();
                referenceInformation.Type = elemList4[i].Attributes["Type"].Value;
                referenceInformation.Value = elemList4[i].InnerText;
                ListareferenceInformation.Add(referenceInformation);
            }

            XmlNodeList elemList5 = doc.GetElementsByTagName("ConveyanceInformation");
            for (int i = 0; i < elemList5.Count; i++)
            {
                XmlNodeList detlist5 = elemList5[i].ChildNodes;
                for (int j = 0; j < detlist5.Count; j++)
                {
                    identifier = new MessageBody.MessageProperties.TransportationDetails.ConveyanceInformation.Identifier();
                    bool ExisteImo = false;
                    bool validar = detlist5[j].Attributes["Type"] != null ? true : false;
                    if (validar)
                    {
                        ExisteImo = detlist5[j].Attributes["Type"].Value == "LloydsCode" ? true : false;
                    }
                    if (ExisteImo)
                    {
                        identifier.Type = detlist5[j].Attributes["Type"].Value;
                        identifier.Value = detlist5[j].InnerText;
                        listaidentifier.Add(identifier);

                    }


                }

            }

            XmlNodeList elemList6 = doc.GetElementsByTagName("Location");
            for (int i = 0; i < elemList6.Count; i++)
            {
                XmlNodeList detlist6 = elemList6[i].ChildNodes;
                for (int j = 0; j < detlist6.Count; j++)
                {
                    if (detlist6[j].Name == "Type")
                    {
                        location = new MessageBody.MessageProperties.TransportationDetails.Location();
                        location.Type = detlist6[j].InnerText;
                    }
                    if (detlist6[j].Name == "Identifier")
                    {
                        location.IdentifierType = detlist6[j].Attributes["Type"].Value;
                        location.IdentifierValue = detlist6[j].InnerText;
                    }
                    if (detlist6[j].Name == "Name")
                    {
                        location.Name = detlist6[j].InnerText;
                    }
                    if (detlist6[j].Name == "Subdivision")
                    {
                        location.Subdivision = detlist6[j].InnerText;
                    }
                    if (detlist6[j].Name == "CountryName")
                    {
                        location.CountryName = detlist6[j].InnerText;
                    }
                    if (detlist6[j].Name == "CountryCode")
                    {
                        location.CountryCode = detlist6[j].InnerText;
                    }
                    if (detlist6[j].Name == "DateTime")
                    {
                        location.DateType = detlist6[j].Attributes["DateType"].Value;
                        location.DateTimeValue = detlist6[j].InnerText;
                        listalocation.Add(location);
                    }

                    //identifier = new MessageBody.MessageProperties.TransportationDetails.ConveyanceInformation.Identifier();
                    //identifier.Type = detlist6[j].Attributes["Type"].Value;
                    //identifier.Value = detlist6[j].InnerText;
                    //listaidentifier.Add(identifier);

                }

            }

            XmlNodeList elemList7 = doc.GetElementsByTagName("Party");
            for (int i = 0; i < elemList7.Count; i++)
            {
                party = new MessageBody.MessageProperties.Party();
                XmlNodeList detlist7 = elemList7[i].ChildNodes;
                for (int j = 0; j < detlist7.Count; j++)
                {
                    var a = detlist7[j].Name == "Role" ? party.Role = detlist7[j].InnerText : "";

                    var b = detlist7[j].Name == "Name" ? party.Name = detlist7[j].InnerText : "";

                    if (detlist7[j].Name == "Identifier")
                    {
                        party.IdentifierType = detlist7[j].Attributes["Type"].Value;
                        party.IdentifierValue = detlist7[j].InnerText;
                    }

                    if (detlist7[j].Name == "Address")
                    {
                        XmlNodeList detdetlist7 = detlist7[j].ChildNodes;
                        for (int k = 0; k < detdetlist7.Count; k++)
                        {

                            var aa = detdetlist7[k].Name == "StreetAddress" ? address.StreetAddress = detdetlist7[k].InnerText : "";
                            var bb = detdetlist7[k].Name == "CityName" ? address.CityName = detdetlist7[k].InnerText : "";
                            var cc = detdetlist7[k].Name == "PostalCode" ? address.PostalCode = detdetlist7[k].InnerText : "";
                            var dd = detdetlist7[k].Name == "CountryCode" ? address.CountryCode = detdetlist7[k].InnerText : "";
                            var ee = detdetlist7[k].Name == "CountryName" ? address.CountryName = detdetlist7[k].InnerText : "";

                        }
                    }
                    if (detlist7[j].Name == "Contacts")
                    {
                        XmlNodeList detdetlist7 = detlist7[j].ChildNodes;
                        for (int k = 0; k < detdetlist7.Count; k++)
                        {

                            var aa = detdetlist7[k].Name == "Type" ? contacts.Type = detdetlist7[k].InnerText : "";
                            var bb = detdetlist7[k].Name == "Name" ? contacts.Name = detdetlist7[k].InnerText : "";

                            if (detdetlist7[k].Name == "CommunicationDetails")
                            {
                                XmlNodeList dedetdetlist = detdetlist7[k].ChildNodes;
                                for (int n = 0; n < dedetdetlist.Count; n++)
                                {
                                    var ff = dedetdetlist[n].Name == "Phone" ? comunicationDetailsParty.Phone = dedetdetlist[n].InnerText : "";
                                    var gg = dedetdetlist[n].Name == "Email" ? comunicationDetailsParty.Email = dedetdetlist[n].InnerText : "";
                                }

                            }

                        }
                    }
                }
                listaparty.Add(party);
            }


            XmlNodeList elemList8 = doc.GetElementsByTagName("EquipmentDetails");
            for (int i = 0; i < elemList8.Count; i++)
            {
                if (elemList8[i].Name == "EquipmentDetails")
                {
                    equipmentDetails.EquipmentSupplier = elemList8[i].Attributes["EquipmentSupplier"].Value;

                    XmlNodeList det = elemList8[i].ChildNodes;
                    for (int k = 0; k < det.Count; k++)
                    {
                        if (det[k].Name == "EquipmentType")
                        {
                            XmlNodeList detdd = det[k].ChildNodes;
                            for (int q = 0; q < detdd.Count; q++)
                            {

                                var aa = detdd[q].Name == "EquipmentTypeCode" ? equipmentType.EquipmentTypeCode = detdd[q].InnerText : "";
                                var bb = detdd[q].Name == "EquipmentDescription" ? equipmentType.EquipmentDescription = detdd[q].InnerText : "";

                            }
                        }

                        if (det[k].Name == "ImportExportHaulage")
                        {
                            XmlNodeList detd = det[k].ChildNodes;
                            for (int f = 0; f < detd.Count; f++)
                            {

                                var aa = detd[f].Name == "HaulageArrangements" ? importExportHaulage.HaulageArrangements = detd[f].InnerText : "";
                                var bb = detd[f].Name == "CargoMovementType" ? importExportHaulage.CargoMovementType = detd[f].InnerText : "";

                            }
                        }
                        if (det[k].Name == "NumberOfEquipment")
                        {
                            equipmentDetails.NumberOfEquipment = det[k].InnerText;

                        }

                    }
                }





            }


            #endregion


            #region Ver Datos

            var lista0 = header;
            var lista1 = messageProperties;
            var lista2 = confirmedWith;
            var lista3 = communicationDetails;
            var lista4 = ListareferenceInformation;
            var jj = equipmentDetails;
            var ll = listaidentifier;

            DA_Inttra da = new DA_Inttra();
            Be_Archivo be = new Be_Archivo();

            be.NAME_ARCHIVO = Path.GetFileName(ruta);
            be.codigoDoc = messageProperties.ShipmentID;
            be.estado = header.TransactionStatus;
            //BookingNumber
            be.booking = referenceInformation.Value;
            if (listaidentifier.Count > 0)
            {
                be.imo = listaidentifier[0].Value;
            }
            //be.booking = referenceInformation.Type;
            da.InsertArchivo(be);

            #endregion
        }

        //Leer xml Documento Aceptado
        private static void LeerXML_2(string ruta)
        {
            //string ruta = @"C:\Users\JhonChayguaque\source\VLOG_CONTRLX2_20210312225120_18328477_1000496225.xml";
            #region Inicializar Variables

            VLOG.Message.Header header = new VLOG.Message.Header();
            VLOG.Message.Header.MessageType messageType = new VLOG.Message.Header.MessageType();
            VLOG.Message.Header.Parties.PartnerInformation partnerInformation = new VLOG.Message.Header.Parties.PartnerInformation();
            VLOG.Message.Header.Parties.PartnerInformation.PartnerIdentifier partnerIdentifier = new VLOG.Message.Header.Parties.PartnerInformation.PartnerIdentifier();

            VLOG.Message.MessageBody.MessageProperties.ShipmentID.ShipmentIdentifier shipmentIdentifier = new VLOG.Message.MessageBody.MessageProperties.ShipmentID.ShipmentIdentifier();
            VLOG.Message.MessageBody.MessageProperties.DateTime dateTime = new VLOG.Message.MessageBody.MessageProperties.DateTime();
            VLOG.Message.MessageBody.MessageProperties.Instructions.ShipmentComments shipmentComments = new VLOG.Message.MessageBody.MessageProperties.Instructions.ShipmentComments();

            #endregion

            var RutaDocumento = ruta;

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            try
            {
                doc.Load(RutaDocumento);

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }
            //XmlNode node = doc.ReadNode(reader2);


            using (XmlReader reader = XmlReader.Create(@RutaDocumento))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag  
                        switch (reader.Name.ToString())
                        {
                            #region Header
                            case "MessageType":
                                messageType.value = reader.ReadString();
                                break;
                            case "DocumentIdentifier":
                                header.DocumentIdentifier = reader.ReadString();
                                break;
                            case "DateTime":
                                dateTime.Value = reader.ReadString();
                                break;
                            case "RequestMessageVersion":
                                partnerIdentifier.value = reader.ReadString();
                                break;
                                #endregion
                        }


                    }

                }
            }

            #region Body

            XmlNodeList elemList1 = doc.GetElementsByTagName("ShipmentIdentifier");
            for (int i = 0; i < elemList1.Count; i++)
            {
                shipmentIdentifier.Acknowledgment = elemList1[i].Attributes["Acknowledgment"].Value;
                shipmentIdentifier.MessageStatus = elemList1[i].Attributes["MessageStatus"].Value;
                shipmentIdentifier.Value = elemList1[i].InnerText;
            }


            XmlNodeList elemList2 = doc.GetElementsByTagName("DateTime");
            for (int i = 0; i < elemList2.Count; i++)
            {
                dateTime.DateType = elemList2[i].Attributes["DateType"].Value;
                dateTime.Value = elemList2[i].InnerText;
            }

            XmlNodeList elemList3 = doc.GetElementsByTagName("ShipmentComments");
            for (int i = 0; i < elemList3.Count; i++)
            {
                shipmentComments.CommentType = elemList3[i].Attributes["CommentType"].Value;
                shipmentComments.Value = elemList3[i].InnerText;
            }
            #endregion


            DA_Inttra da = new DA_Inttra();
            Be_Archivo be = new Be_Archivo();
            be.NAME_ARCHIVO = Path.GetFileName(ruta);
            be.codigoDoc = header.DocumentIdentifier;
            be.estado = shipmentComments.Value;
            be.booking = "";
            //be.
            da.InsertArchivo(be);

        }

        private static void LeerXML_3(string ruta)
        {
            //string ruta = @"C:\Users\JhonChayguaque\source\VLOG_CONTRLX2_20210312225120_18328477_1000496225.xml";
            #region Inicializar Variables

            VLOG.Message.Header header = new VLOG.Message.Header();
            VLOG.Message.Header.MessageType messageType = new VLOG.Message.Header.MessageType();
            VLOG.Message.Header.Parties.PartnerInformation partnerInformation = new VLOG.Message.Header.Parties.PartnerInformation();
            VLOG.Message.Header.Parties.PartnerInformation.PartnerIdentifier partnerIdentifier = new VLOG.Message.Header.Parties.PartnerInformation.PartnerIdentifier();

            VLOG.Message.MessageBody.MessageProperties.ShipmentID.ShipmentIdentifier shipmentIdentifier = new VLOG.Message.MessageBody.MessageProperties.ShipmentID.ShipmentIdentifier();
            VLOG.Message.MessageBody.MessageProperties.DateTime dateTime = new VLOG.Message.MessageBody.MessageProperties.DateTime();
            VLOG.Message.MessageBody.MessageProperties.Instructions.ShipmentComments shipmentComments = new VLOG.Message.MessageBody.MessageProperties.Instructions.ShipmentComments();
            VLOG.Message.MessageBody.MessageProperties.EventCode eventCode = new VLOG.Message.MessageBody.MessageProperties.EventCode();

            #endregion

            var RutaDocumento = ruta;

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            try
            {
                doc.Load(RutaDocumento);

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }
            //XmlNode node = doc.ReadNode(reader2);


            using (XmlReader reader = XmlReader.Create(@RutaDocumento))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag  
                        switch (reader.Name.ToString())
                        {
                            #region Header
                            case "MessageType":
                                messageType.value = reader.ReadString();
                                break;
                            case "DocumentIdentifier":
                                header.DocumentIdentifier = reader.ReadString();
                                break;
                            case "DateTime":
                                dateTime.Value = reader.ReadString();
                                break;
                                //case "RequestMessageVersion":
                                //    partnerIdentifier.value = reader.ReadString();
                                //    break;
                                #endregion
                        }


                    }

                }
            }

            #region Body

            XmlNodeList elemList1 = doc.GetElementsByTagName("EventCode");
            for (int i = 0; i < elemList1.Count; i++)
            {
                eventCode.value = elemList1[i].InnerText;
            }



            #endregion


            DA_Inttra da = new DA_Inttra();
            //Be_Archivo be = new Be_Archivo();
            //be.NAME_ARCHIVO = Path.GetFileName(ruta);
            //be.codigoDoc = header.DocumentIdentifier;
            //be.estado = shipmentComments.Value;
            //be.booking = "";
            da.Seguimiento_OPL(header.DocumentIdentifier, eventCode.value);
            DA_Inttra daa = new DA_Inttra();
            Be_Archivo be = new Be_Archivo();
            be.NAME_ARCHIVO = Path.GetFileName(ruta);
            //be.codigoDoc = header.DocumentIdentifier.ToString();
            be.estado = eventCode.value;
            //BookingNumber
            be.booking = "";
            //be.booking = referenceInformation.Type;
            daa.InsertArchivo(be);
        }

        public void listFiles()
        {

            string host = "ftp.inttraworks.inttra.com";
            string username = "v0649108";
            string password = "Q3oh4Z66";
            string remoteDirectory = "/outbound";
            int port = 22;
            DA_Inttra da = new DA_Inttra();

            var res = da.LISTA();
            List<Be_Archivo> lista = (List<Be_Archivo>)res;



            //string remoteDirectory = "/some/example/directory";
            string url = Path.Combine(Environment.CurrentDirectory);
            //string pathLocalFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "download_sftp_file.txt");

            using (SftpClient sftp = new SftpClient(host, port, username, password))
            {
                try
                {
                    sftp.Connect();

                    var files = sftp.ListDirectory(remoteDirectory);

                    foreach (var file in files)
                    {

                        if (lista.Exists(x => x.NAME_ARCHIVO == file.Name))
                        {
                            if (lista.Exists(x => file.Name.Contains("IFTMBC2")))
                            {
                                string name = file.Name;
                            }

                        }
                        else
                        {


                            if (Path.GetExtension(file.Name) == ".xml")
                            {

                                var valor = file.Name.Split('_');

                                if (valor[0] == "VLOG" && valor[1] == "CONTRLX2")
                                {
                                    string Pathlocal = Path.Combine(url, file.Name);
                                    using (Stream fileStream = File.OpenWrite(Pathlocal))
                                    {
                                        string path = remoteDirectory + "/" + file.Name;
                                        sftp.DownloadFile(path, fileStream);
                                    }
                                    LeerXML_2(Pathlocal);

                                }
                                else if (valor[0] == "IFTMBC2")
                                {
                                    string Pathlocal = Path.Combine(url, file.Name);
                                    using (Stream fileStream = File.OpenWrite(Pathlocal))
                                    {
                                        string path = remoteDirectory + "/" + file.Name;
                                        sftp.DownloadFile(path, fileStream);
                                    }

                                    LeerXML(Pathlocal);
                                }
                                else if (valor[0] == "VLOG" && valor[1] == "IFTSTA")
                                {
                                    string Pathlocal = Path.Combine(url, file.Name);
                                    using (Stream fileStream = File.OpenWrite(Pathlocal))
                                    {
                                        string path = remoteDirectory + "/" + file.Name;
                                        sftp.DownloadFile(path, fileStream);
                                    }

                                    LeerXML_3(Pathlocal);
                                }
                                //Console.WriteLine(file.Name);

                            }
                        }
                    }

                    sftp.Disconnect();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Se ha detectado una excepción " + e.ToString());
                }
            }
        }


        public void GetDatosInttra()
        {
            var url = $"https://vecloudapi.azurewebsites.net/api/Inttra/OnInttra";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            // Do something with responseBody
                            Console.WriteLine(responseBody);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
            }
        }

    
        private static void PostItem(string data)
        {
            var url = $"http://localhost:8080/items";
            var request = (HttpWebRequest)WebRequest.Create(url);
            string json = $"{{\"data\":\"{data}\"}}";
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            // Do something with responseBody
                            Console.WriteLine(responseBody);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                // Handle error
            }
        }

 
    }
}
