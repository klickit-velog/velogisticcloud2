﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebJobs.Models.VLOG
{
    public class Message
    {
        public class Header
        {
            public class MessageType
            {
                public string MessageVersion { get; set; }
                public string value { get; set; }
            }
            public string DocumentIdentifier { get; set; }
            public class DateTime
            {
                public string DateType { get; set; }
                public string value { get; set; }
            }

            public class Parties
            {
                public class PartnerInformation
                {
                    public string PartnerRole { get; set; }
                    public class PartnerIdentifier
                    {
                        public string Agency { get; set; }
                        public string value { get; set; }
                    }
                }
            }
        }

        public class MessageBody
        {
            public class MessageProperties
            {
                public class ShipmentID
                {
                    public class ShipmentIdentifier
                    {
                        public string Acknowledgment { get; set; }
                        public string MessageStatus { get; set; }
                        public string Value { get; set; }
                    }
                }

                public class DateTime
                {
                    public string DateType { get; set; }
                    public string Value { get; set; }
                }
                public class Instructions
                {
                    public class ShipmentComments
                    {
                        public string CommentType { get; set; }
                        public string Value { get; set; }
                    }
                }

                //propiedades xml seguimiento tracking
                public class EventCode
                {
                    public string value { get; set; }
                }
            }
        }
    }
}
