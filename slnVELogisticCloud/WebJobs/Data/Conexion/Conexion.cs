﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace WebJobs.Data.Conexion
{
    public class Conexion
    {

        #region Variables
        private String oSqlConnIN;
        private readonly SqlTransaction sqlTransaction = null;
        #endregion



        #region Conexion

        public Conexion(Int32 idDatabase)
        {
            if (idDatabase == 1)
            {
                SqlConnectionStringBuilder sql = new SqlConnectionStringBuilder()
                {
                    DataSource = "winsrvdesadb.database.windows.net",
                    InitialCatalog = "BDVECloud",
                    //UserID = "lucky_srv_master",
                    UserID = "Admindb",
                    //Password = "0qWeRtY1",
                    Password = "Klickitdb$1",
                };
                oSqlConnIN = sql.ToString();
                //oSqlConnIN = "Server=tcp:winsrvdesadb.database.windows.net,1433;Initial Catalog=BDVECloud;Persist Security Info=False;User ID=Admindb;Password=Klickitdb$1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            }
            else
            {

            }
        }

        public SqlConnection GetConnection()
        {
            SqlConnection cn = new SqlConnection(oSqlConnIN);
            try
            {
                cn.Open();
                return cn;
            }
            catch (SqlException ex)
            {
                _ = ex.ErrorCode;
                return null;
            }

        }
        #endregion

        #region EjecutarEscalar
        /// <summary>
        /// </summary>
        /// <param name="vProcedure"></param>
        /// <param name="vValores"></param>
        /// <example>Insertar|Actualizar|Eliminar</example>
        /// <returns></returns>
        public String EjecutarEscalar(String sProcedure, params object[] valores)
        {
            SqlParameter[] arParms = new SqlParameter[valores.Length];

            //DataSet ds=ejecutarQuery("SELECT * FROM sysobjects WHERE xtype = 'P' AND name = '"+ sProcedure+"'");
            //if (ds.Tables[0].Rows.Count == 0)
            //    throw new Exception("El store procedure " + sProcedure + " no existe o no tiene permisos para ejecutarlo.");

            //ds=ejecutarQuery("sp_help " + sProcedure);

            //Obtiene los parámetros del procecimiento
            DataSet ds = ObtenerParametros(sProcedure);

            if (ds.Tables.Count == 0)
                return null;
            else if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0]; //Estructura del Stored
                //if(dt.Rows.Count != valores.Length)
                //{	
                //    return null;
                //}
                Int32 i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    //Omite el parámetro de retorno del procedimiento
                    if (!dr["Parameter_name"].Equals("@RETURN_VALUE"))
                    {
                        arParms[i] = new SqlParameter(dr["Parameter_name"].ToString(), f_obtenerSQLType(dr["Type_name"].ToString()));
                        arParms[i].Value = valores[i];
                        i++;
                    }
                }
                if (i != valores.Length)
                    throw new Exception("La cantidad de parámetros ingresados no coincide con las del procedimiento.");
            }

            //Se verifica si existe una Transaccion de BD activa
            String sValor;
            if (sqlTransaction != null)
            {
                sValor = SqlHelper.ExecuteScalar
                    (
                    sqlTransaction,
                    CommandType.StoredProcedure,
                    sProcedure,
                    arParms
                    ).ToString();
            }
            else
            {
                sValor = SqlHelper.ExecuteScalar
                    (
                    this.oSqlConnIN,
                    CommandType.StoredProcedure,
                    sProcedure,
                    arParms
                    ).ToString();
            }
            return sValor;
        }
        #endregion

        #region EjecutarConRetorno
        public string ejecutarConRetorno(String sProcedure, params object[] valores)
        {
            string retorno = String.Empty;
            int Cant;
            SqlParameter[] arParms = new SqlParameter[valores.Length];

            //DataSet ds=ejecutarQuery("SELECT * FROM sysobjects WHERE xtype = 'P' AND name = '"+ sProcedure+"'");
            //if (ds.Tables[0].Rows.Count == 0)
            //    throw new Exception("El store procedure " + sProcedure + " no existe o no tiene permisos para ejecutarlo.");

            //ds=ejecutarQuery("sp_help " + sProcedure);

            //Obtiene los parámetros del procecimiento
            DataSet ds = ObtenerParametros(sProcedure);
            if (ds.Tables.Count == 0)
                //return 0;
                retorno = "Verifique el sp";
            else if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0]; //Estructura del Stored
                //if(dt.Rows.Count != valores.Length)
                //{	
                //    return;
                //}
                Int32 i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    //Omite el parámetro de retorno del procedimiento
                    if (!dr["Parameter_name"].Equals("@RETURN_VALUE"))
                    {
                        arParms[i] = new SqlParameter(dr["Parameter_name"].ToString(), f_obtenerSQLType(dr["Type_name"].ToString()));
                        arParms[i].Value = valores[i];
                        i++;
                    }
                }
                if (i != valores.Length)
                    throw new Exception("La cantidad de parámetros ingresados no coincide con las del procedimiento.");
            }

            //Se verifica si existe una Transaccion de BD activa
            if (sqlTransaction != null)
            {
                Cant = SqlHelper.ExecuteNonQuery
                    (
                    this.sqlTransaction,
                    CommandType.StoredProcedure,
                    sProcedure,
                    arParms
                    );
            }
            else
            {
                Cant = SqlHelper.ExecuteNonQuery
                    (
                    this.oSqlConnIN,
                    CommandType.StoredProcedure,
                    sProcedure,
                    arParms
                    );
            }

            if (!string.IsNullOrEmpty(retorno))
            {
                retorno = Convert.ToString(Cant);
            }

            return retorno;
        }
        #endregion

        #region EjecutarDataReader
        /// <summary>
        /// </summary>
        /// <param name="sProcedure"></param>
        /// <param name="valores"></param>
        /// <returns></returns>
        public SqlDataReader ejecutarDataReader(String sProcedure, params object[] valores)
        {
            try
            {
                return SqlHelper.ExecuteReader(this.oSqlConnIN, sProcedure, valores);
            }
            catch (Exception ex)
            {
                Console.Write("Error: " + ex.Message);
                return null;
            }
        }
        #endregion

        #region EjecutarDataSet
        /// <summary>
        /// </summary>
        /// <param name="vProcedure"></param>
        /// <param name="vValores"></param>
        /// <returns></returns>
        public DataSet ejecutarDataSet(String vProcedure, params object[] vValores)
        {
            SqlParameter[] sqlParameters = new SqlParameter[vValores.Length];

            //Obtiene los parámetros del procecimiento
            DataSet ds = ObtenerParametros(vProcedure);

            if (ds.Tables.Count == 0)
            {
                return null;
            }
            else if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                Int32 i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!dr["Parameter_name"].Equals("@RETURN_VALUE"))
                    {
                        sqlParameters[i] = new SqlParameter(dr["Parameter_name"].ToString(), f_obtenerSQLType(dr["Type_name"].ToString()));
                        sqlParameters[i].Value = vValores[i];
                        i++;
                    }
                }
                if (i != vValores.Length)
                {
                    throw new Exception("La cantidad de parámetros ingresados no coincide con las del procedimiento.");
                }
            }
            //Se verifica si existe una Transaccion de BD activa
            ds = null;
            if (sqlTransaction != null)
            {
                ds = SqlHelper.ExecuteDataset
                    (
                    sqlTransaction,
                    CommandType.StoredProcedure,
                    vProcedure,
                    sqlParameters
                    );
            }
            else
            {
                ds = SqlHelper.ExecuteDataset
                    (
                    oSqlConnIN,
                    CommandType.StoredProcedure,
                    vProcedure,
                    sqlParameters
                    );
            }

            return ds;
        }
        #endregion

        #region EjecutarDataTable
        /// <summary
        /// </summary>
        /// <param name="vProcedure"></param>
        /// <param name="vValores"></param>
        /// <returns></returns>
        public DataTable ejecutarDataTable(String vProcedure, params object[] vValores)
        {
            SqlParameter[] sqlParameters = new SqlParameter[vValores.Length];

            DataTable dt = null;

            //Obtiene los parámetros del procecimiento
            DataSet ds = ObtenerParametros(vProcedure);

            if (ds.Tables.Count == 0)
            {
                return null;
            }
            else if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
                Int32 i = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    if (!dr["Parameter_name"].Equals("@RETURN_VALUE"))
                    {
                        sqlParameters[i] = new SqlParameter(dr["Parameter_name"].ToString(), f_obtenerSQLType(dr["Type_name"].ToString()));
                        sqlParameters[i].Value = vValores[i];
                        i++;
                    }
                }
                if (i != vValores.Length)
                {
                    throw new Exception("La cantidad de parámetros ingresados no coincide con las del procedimiento.");
                }
            }
            //Se verifica si existe una Transaccion de BD activa
            ds = null;
            dt = null;
            DataSet dsResult;
            if (sqlTransaction != null)
            {
                dsResult = SqlHelper.ExecuteDataset(sqlTransaction, CommandType.StoredProcedure, vProcedure, sqlParameters);
            }
            else
            {
                dsResult = SqlHelper.ExecuteDataset(oSqlConnIN, CommandType.StoredProcedure, vProcedure, sqlParameters);
            }

            if (sqlTransaction != null) sqlTransaction.Dispose();
            if (dsResult.Tables.Count > 0) dt = dsResult.Tables[0]; else dt = null;
            dsResult = null;
            if (dt != null) return dt.Copy(); else return null;
        }
        #endregion

        #region Metodos Privados
        private DataSet ObtenerParametros(string vProcedure)
        {
            object vSquema = DBNull.Value;
            if (vProcedure.IndexOf('.') > 0)
            {
                vSquema = vProcedure.Substring(0, vProcedure.IndexOf('.'));
                vProcedure = vProcedure.Substring(vProcedure.IndexOf('.') + 1); ;
            }

            SqlParameter[] sqlParameters = { new SqlParameter("@procedure_name", SqlDbType.NChar, 256),
                                      new SqlParameter("@group_number", SqlDbType.Int, 4),
                                      new SqlParameter("@procedure_schema", SqlDbType.NChar, 256),
                                      new SqlParameter("@parameter_name", SqlDbType.NChar, 256) };

            sqlParameters[0].Value = vProcedure;
            sqlParameters[1].Value = DBNull.Value;
            sqlParameters[2].Value = vSquema;
            sqlParameters[3].Value = DBNull.Value;

            DataSet ds;



            if (sqlTransaction != null)
            {


                ds = SqlHelper.ExecuteDataset(
                    sqlTransaction,
                    CommandType.StoredProcedure,
                    "sp_procedure_params_rowset",
                    sqlParameters
                    );

            }
            else
            {
                ds = SqlHelper.ExecuteDataset
                   (
                   oSqlConnIN,
                   CommandType.StoredProcedure,
                   "sp_procedure_params_rowset",
                   sqlParameters
                   );
            }
            return ds;

        }

        private SqlDbType f_obtenerSQLType(string sNombreTipo)
        {
            SqlDbType tTipo;

            switch (sNombreTipo)
            {
                case "bit":
                    tTipo = SqlDbType.Bit;
                    break;

                case "char":
                    tTipo = SqlDbType.Char;
                    break;

                case "varchar":
                    tTipo = SqlDbType.VarChar;
                    break;

                case "decimal":
                    tTipo = SqlDbType.Decimal;
                    break;

                case "float":
                    tTipo = SqlDbType.Float;
                    break;

                case "int":
                    tTipo = SqlDbType.Int;
                    break;

                case "smallint":
                    tTipo = SqlDbType.SmallInt;
                    break;

                case "tinyint":
                    tTipo = SqlDbType.TinyInt;
                    break;

                case "datetime":
                    tTipo = SqlDbType.DateTime;
                    break;

                case "smalldatetime":
                    tTipo = SqlDbType.SmallDateTime;
                    break;

                case "nvarchar":
                    tTipo = SqlDbType.NVarChar;
                    break;

                case "image":
                    tTipo = SqlDbType.Image;
                    break;

                case "xml":
                    tTipo = SqlDbType.Xml;
                    break;

                case "text":
                    tTipo = SqlDbType.Text;
                    break;

                case "ntext":
                    tTipo = SqlDbType.NText;
                    break;

                case "bigint":
                    tTipo = SqlDbType.BigInt;
                    break;

                default:
                    throw (new Exception("Tipo de dato SQL no soportado:" + sNombreTipo));
            }

            return tTipo;
        }
        #endregion



        //public List<string> ListaSolicitud()
        //{

        //    DataTable dt = new DataTable();
        //    string proc = "Lista";

        //    SqlConnection cnn = new SqlConnection(@"Data Source=DESKTOP-TM50QGU\MSSQLSERVER01; Initial Catalog=BDVELogisticsCloud; User ID=sa ; Password=197346825;");
        //    SqlDataAdapter da = new SqlDataAdapter(proc, cnn);
        //    cnn.Open();

        //    da.SelectCommand.CommandType = CommandType.StoredProcedure;
        //    List<string> listaPersonal = new List<string>();
        //    da.Fill(dt);
        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            string a = Convert.ToString(row["codigo"]);
        //        listaPersonal.Add(a);
        //        }
        //    }
        //    return listaPersonal;

        //}


    }
}
