﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static WebJobs.Models.BE_Inttra;

namespace WebJobs.Data
{
    public class DA_Inttra
    {
        #region Conexion
        private Conexion.Conexion oCon;
        public DA_Inttra()
        {
            oCon = new Conexion.Conexion(1);
        }
        #endregion
        public object InsertArchivo(Be_Archivo ent)
        {
            try
            {
                return Convert.ToString(oCon.EjecutarEscalar("Usp_GuardarArchivo", ent.NAME_ARCHIVO, ent.codigoDoc, ent.estado, ent.booking, ent.imo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object LISTA()
        {
            try
            {
                //switch (request.pOpcion)
                //{
                //case 2:
                List<Be_Archivo> lista = new List<Be_Archivo>();
                using (IDataReader dr = oCon.ejecutarDataReader("LISTA_ARCHIVOS"))
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            Be_Archivo be_Archivo = new Be_Archivo();

                            Type _type = be_Archivo.GetType();
                            System.Reflection.PropertyInfo[] lProperty = _type.GetProperties();

                            foreach (System.Reflection.PropertyInfo propiedad in lProperty)
                            {
                                String pType = propiedad.PropertyType.Name;
                                String pName = propiedad.Name;

                                if (dr[pName].GetType().Name != "DBNull")
                                {
                                    propiedad.SetValue(be_Archivo, dr[pName]);
                                }
                            }
                            lista.Add(be_Archivo);
                        }
                    }
                }
                return lista;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object Seguimiento_OPL(string DocumentIdentifier, string EventCode)
        {
            try
            {

                var IdReserva = Convert.ToUInt64(DocumentIdentifier);
                int idestado = 0;
                string estado = "";
                #region Asginar estado
                if (EventCode == "EE")
                {
                    idestado = 1;
                    estado = "Depósito de vacíos";
                }
                if (EventCode == "I")
                {
                    idestado = 2;
                    estado = "Gated in";
                }
                if (EventCode == "AE")
                {
                    idestado = 3;
                    estado = "Carga del buque";
                }
                if (EventCode == "VD")
                {
                    idestado = 4;
                    estado = "Salida del buque";
                }
                if (EventCode == "VA")
                {
                    idestado = 5;
                    estado = "Llegada del buque";
                }
                if (EventCode == "UV")
                {
                    idestado = 6;
                    estado = "Descarga del buque";
                }
                if (EventCode == "QA")
                {
                    idestado = 7;
                    estado = "Gated out";
                }
                if (EventCode == "RD")
                {
                    idestado = 8;
                    estado = "Regreso";
                }
                #endregion
                //"EE" "Depósito de vacíos"
                //"I" "Gated in"
                //"AE" "Carga del buque"
                //"VD" "Salida del buque"
                //"VA" "Llegada del buque"
                //"UV" "Descarga del buque"
                //"OA" "Gated out"
                //"RD" "Regreso"
                return Convert.ToString(oCon.EjecutarEscalar("USP_SEGUIMIENTO_RESERVA", IdReserva, EventCode, estado));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
