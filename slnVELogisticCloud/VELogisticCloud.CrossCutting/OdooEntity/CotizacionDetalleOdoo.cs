﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class CotizacionDetalleOdoo
    {
        public CotizacionDetalleOdoo()
        {
            ListaServicios = new List<Servicio>();
        }

        public int IdNaviera { get; set; }
        public int IdPuertoCarga { get; set; }
        public int IdPuertoDescarga { get; set; }
        public int IdTerminalRetiroVacio { get; set; }
        public int IdTerminalDepositoVacio { get; set; }
        public int IdAlmacenPacking { get; set; }
        public int IdCotizacionVeCloud { get; set; }
        public int IdProduct { get; set; }
        public List<Servicio> ListaServicios { get; set; }
    }
}
