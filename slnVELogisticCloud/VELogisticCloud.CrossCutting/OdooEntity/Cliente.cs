﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Cliente
    {

        public Cliente()
        {
            Contactos = new List<Contacto>();
        }

        private string nombre;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public string Correo { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Fax { get; set; }
        public string Celular { get; set; }
        public List<Contacto> Contactos { get; set; }
    }
}