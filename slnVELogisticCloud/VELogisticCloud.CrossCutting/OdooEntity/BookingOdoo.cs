﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.CrossCutting.Comun;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class BookingOdoo
    {
        private string correoCliente;
        private string notas;
        private string nombreNave;
        private string consignatario;
        private string numeroViaje;
        private string codigoNaviera;
        private string uN1;
        private string uN2;
        private bool? coldTreatment;
        private double? temperatura;
        private double? humedad;
        private bool? tieneHumedad;
        private bool? atmosferaControlada;
        private double? cO2;
        private double? o2;
        private string numeroContedor;
        private string codigoBL;
        private string contractOwnerId;
        private string tipoContrato;
        private string tecnologia;

        public string NumeroBooking { get; set; }

        public string CorreoCliente { get => correoCliente ?? ""; set => correoCliente = value; }
        public int IdCommodity { get; set; }
        public int IdPuertoCarga { get; set; }
        public int IdPuertoDescarga { get; set; }
        public int CantidadContenedor { get; set; }
        public DateTime ETA_Origen { get; set; }
        public DateTime ETD_Origen { get; set; }
        public DateTime ETA_Destino { get; set; }
        public int IdTipoContenedor { get; set; }
        public string Notas { get => notas ?? ""; set => notas = value; }
        public int IdTerminaDepositoVacio { get; set; }
        public string NombreNave { get => nombreNave ?? ""; set => nombreNave = value; }
        public int IdDetalleCotizacionOdoo { get; set; }
        public double Peso { get; set; }
        public string Consignatario { get => consignatario ?? ""; set => consignatario = value; }
        public string NumeroViaje { get => numeroViaje ?? ""; set => numeroViaje = value; }
        public string CodigoNaviera { get => codigoNaviera ?? ""; set => codigoNaviera = value; }


        //CAMPOS REEFER
        public TipoCondicion? Condicion { get; set; }
        public bool? ColdTreatment { get => coldTreatment ?? false; set => coldTreatment = value; }
        public double? Temperatura { get => temperatura ?? 0; set => temperatura = value; }
        public TipoTemperatura? TipoTemperatura { get; set; }
        public double? Ventilacion { get; set; }
        public TipoVentilacion? TipoVentilacion { get; set; }
        public double? Humedad { get => humedad ?? 0; set => humedad = value; }
        public bool? TieneHumedad { get => tieneHumedad ?? false; set => tieneHumedad = value; }
        public TipoHumedad? TipoHumedad { get; set; }
        public bool? AtmosferaControlada { get => atmosferaControlada ?? false; set => atmosferaControlada = value; }
        public double? CO2 { get => cO2 ?? 0; set => cO2 = value; }
        public double? O2 { get => o2 ?? 0; set => o2 = value; }

        //CAMPOS DRY
        public IMO? Imo { get; set; }
        public string UN1 { get => uN1 ?? ""; set => uN1 = value; }
        public string UN2 { get => uN2 ?? ""; set => uN2 = value; }

        //CAMPOS NUEVOS
        public string NumeroContenedor { get => numeroContedor ?? ""; set => numeroContedor = value; }
        public string CodigoBL { get => codigoBL ?? ""; set => codigoBL = value; }
        public string ContractOwnerId { get => contractOwnerId ?? ""; set => contractOwnerId = value; }
        public string TipoContrato { get => tipoContrato ?? ""; set => tipoContrato = value; }
        public string Tecnologia { get => tecnologia ?? ""; set => tecnologia = value; }

    }
}
