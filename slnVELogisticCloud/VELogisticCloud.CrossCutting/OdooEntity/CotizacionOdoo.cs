﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.CrossCutting.Comun;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class CotizacionOdoo
    {
        private string notas;
        private string correoCliente;

        public CotizacionOdoo()
        {
            this.ListaCotizacionDetalle = new List<CotizacionDetalleOdoo>();
        }

        public string CorreoCliente { get => correoCliente ?? ""; set => correoCliente = value; }
        public int IdCommodity { get; set; }
        public int IdCampania { get; set; }
        public int IdTipoContenedor { get; set; }
        public int CantidadContenedor { get; set; }
        public int IdCotizacionVeCloud { get; set; }
        public TipoCotizacion2 TipoCotizacion { get; set; }
        public string Notas { get => notas ?? ""; set => notas = value; }

        public List<CotizacionDetalleOdoo> ListaCotizacionDetalle { get; set; }

    }
}
