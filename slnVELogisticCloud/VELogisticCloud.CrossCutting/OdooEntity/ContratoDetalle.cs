﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class ContratoDetalle
    {
        private string _nombrePuertoCarga;
        private string _nombrePuertoDescarga;
        private string _TT;
        private string _nombreNaviera;
        private string _nombreCampania;

        public int IdContrato { get; set; }
        public int IdCotizacionDetalleOdoo { get; set; }
        public int IdPuertoCarga { get; set; }
        public string NombrePuertoCarga { get => _nombrePuertoCarga ?? ""; set => _nombrePuertoCarga = value; }
        public string CodigoPuertoCarga { get; set; }
        public int IdPuertoDescarga { get; set; }
        public string NombrePuertoDescarga { get => _nombrePuertoDescarga ?? ""; set => _nombrePuertoDescarga = value; }
        public string CodigoPuertoDescarga { get; set; }
        public string TT { get => _TT ?? ""; set => _TT = value; }
        public double FleteBL { get; set; }
        public double Rebate { get; set; }
        public double Neto { get; set; }

        public string NumeroContrato { get; set; }
        public int IdNaviera { get; set; }
        public string NombreNaviera { get => _nombreNaviera ?? ""; set => _nombreNaviera = value; }
        public DateTime? ValidoDesde { get; set; }
        public DateTime? ValidoHasta { get; set; }

        public int IdCampania { get; set; }
        public string NombreCampania { get => _nombreCampania ?? ""; set => _nombreCampania = value; }
    }
}
