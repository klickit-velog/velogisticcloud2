﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Deposito
    {
        private string nombre;

        public int Id { get; set; }
        public int IdNaviera { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public bool EsDepositoTemporal { get; set; }

    }
}