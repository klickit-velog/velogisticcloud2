﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Company
    {
        public int IdFleteMaritimo { get; set; }
        public int IdOperacionLogistica_Directo { get; set; }
        public int IdOperacionLogistica_DepositoTemporal { get; set; }
    }
}
