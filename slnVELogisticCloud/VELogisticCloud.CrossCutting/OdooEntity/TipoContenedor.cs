﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class TipoContenedor
    {
        private string nombre;
        private string codigoInternacional;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public bool EsPrincipal { get; set; }
        public string CodigoInternacional { get => codigoInternacional ?? ""; set => codigoInternacional = value; }

    }
}