﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Puerto
    {
        private string nombre;
        private string codigo;
        private string nombreRegion;
        private string nombrePais;
        private string codigoPais;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public string Codigo { get => codigo ?? ""; set => codigo = value; }
        public string NombreRegion { get => nombreRegion ?? ""; set => nombreRegion = value; }
        public string NombrePais { get => nombrePais ?? ""; set => nombrePais = value; }
        public string CodigoPais { get => codigoPais ?? ""; set => codigoPais = value; }
    }
}
