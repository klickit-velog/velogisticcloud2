﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class CustomerService
    {
        private string nombre;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
    }
}