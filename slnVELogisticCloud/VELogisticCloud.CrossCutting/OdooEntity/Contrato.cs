﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Contrato
    {
        private string nombreContrato;
        private string numeroContrato;
        private string nombreCampania;
        private string nombreNaviera;
        private string estado;
        private string nombrePuertoOrigen;
        private string nombrePuertoDescarga;

        public Contrato()
        {
            ListaContratoDetalle = new List<ContratoDetalle>();
        }

        public int IdContrato { get; set; }
        public string NombreContrato { get => nombreContrato ?? ""; set => nombreContrato = value; }
        public string NumeroContrato { get => numeroContrato ?? ""; set => numeroContrato = value; }
        public int IdCampania { get; set; }
        public string NombreCampania { get => nombreCampania ?? ""; set => nombreCampania = value; }
        //public int IdNaviera { get; set; }
        //public string NombreNaviera { get => nombreNaviera ?? ""; set => nombreNaviera = value; }
        public string CodigoNaviera { get; set; }
        public string Estado { get => estado ?? ""; set => estado = value; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public List<ContratoDetalle> ListaContratoDetalle { get; set; }
    }
}
