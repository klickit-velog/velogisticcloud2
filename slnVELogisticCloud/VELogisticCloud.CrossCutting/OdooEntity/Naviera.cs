﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Naviera
    {
        private string nombre;
        private string codigo;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }

        public string Codigo { get => codigo ?? ""; set => codigo = value; }
        public bool VaPorInttra { get; set; }

    }
}
