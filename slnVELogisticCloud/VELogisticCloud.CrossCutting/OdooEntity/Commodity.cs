﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Commodity
    {
        private string nombre;
        private string partidaArancelaria;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public string PartidaArancelaria { get => partidaArancelaria ?? ""; set => partidaArancelaria = value; }

    }
}
