﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class BookingOL
    {
        public int IdBooking { get; set; }
        public string Nombre { get; set; }
        public string NumeroBooking { get; set; }
        public string NumeroViaje { get; set; }
        public string NombreNave { get; set; }
        public string ETA_Origen { get; set; }
        public string ETD_Origen { get; set; }
        public string ETA_Destino { get; set; }
        public string NombreCliente { get; set; }
        public int IdCliente { get; set; }
    }
}
