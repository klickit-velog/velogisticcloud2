﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class User
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
    }
}
