﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Servicio
    {
        private string nombre;
        private string descripcion;
        private int cantidad;

        public Servicio()
        {
            Nombre = string.Empty;
            Descripcion = string.Empty;
            Cantidad = 1;
        }

        public int IdServicio { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public string Descripcion { get => descripcion ?? ""; set => descripcion = value; }

        public int Cantidad { get => cantidad == 0 ? 1 : cantidad; set => cantidad = value; }
    }
}
