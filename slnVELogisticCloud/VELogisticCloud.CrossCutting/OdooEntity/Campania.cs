﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.OdooEntity
{
    public class Campania
    {
        private string nombre;
        private string descripcion;

        public int Id { get; set; }
        public string Nombre { get => nombre ?? ""; set => nombre = value; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string Descripcion { get => descripcion ?? ""; set => descripcion = value; }
    }
}
