﻿using System;

namespace VELogisticCloud.CrossCutting.DTO
{
    public class UsuarioDTO 
    {
        public string IdUsuario { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string RazonSocial { get; set; }
        public string RUC { get; set; }
        public string NombreContacto { get; set; }
        public string NumeroContacto { get; set; }
        public int IdEstado { get; set; }        
        public string NombreEstado { get; set; }
        public DateTime? FechaRegistro { get; set; }        
        public int? IdUsuarioActualiza { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string NombreRol { get; set; }
    }
}
