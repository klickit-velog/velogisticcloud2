﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.DTO.WebApi
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Credential { get; set; }
    }
}
