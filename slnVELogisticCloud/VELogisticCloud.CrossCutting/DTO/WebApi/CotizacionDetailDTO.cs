﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.DTO.WebApi
{
    public class CotizacionDetailDTO
    {
        public DateTime? FechaAprobacion { get; set; }
        public string NumeroCotizacion { get; set; }
        public string CodigoContrato { get; set; }
        public string NumeroReferenciaVE { get; set; }
        public string CodigoEstado { get; set; }
        public string TipoCotizacion { get; set; }
        public string Region { get; set; }
        public string CodigoLinea { get; set; }
        public string NombreLinea { get; set; }
        public string CodigoPOL { get; set; }
        public string POL { get; set; }
        public string CodigoPOD { get; set; }
        public string POD { get; set; }
        public string CodigoCommodity { get; set; }
        public string NombreCommodity { get; set; }
        public string CodigoCampania { get; set; }
        public string NombreCampania { get; set; }
        public decimal Neto { get; set; }
        public decimal Rebate { get; set; }
        public decimal Flete { get; set; }
        public string TT { get; set; }
        public DateTime? ValidoDesde { get; set; }
        public DateTime? ValidoHasta { get; set; }

        public int IdCotizacion { get; set; }
        public string Tamanio { get; set; }
        public int IdLinea { get; set; }
        public int IdPOL { get; set; }
        public string CodigoPaisPOL { get; set; }
        public string NombrePaisPOL { get; set; }
        public int IdPOD { get; set; }
        public string CodigoPaisPOD { get; set; }
        public string NombrePaisPOD { get; set; }

    }
}
