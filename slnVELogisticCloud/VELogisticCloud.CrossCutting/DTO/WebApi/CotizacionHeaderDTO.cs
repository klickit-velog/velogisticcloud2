﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.DTO.WebApi
{
    public class CotizacionHeaderDTO
    {
        public DateTime? FechaAprobacion { get; set; }
        public string NumeroCotizacion { get; set; }
        public string CodigoContrato { get; set; }
        public string NumeroReferenciaVE { get; set; }
        public string TipoCotizacion { get; set; }
        public string CodigoEstado { get; set; }
        public string RutaArchivo { get; set; }

    }
}
