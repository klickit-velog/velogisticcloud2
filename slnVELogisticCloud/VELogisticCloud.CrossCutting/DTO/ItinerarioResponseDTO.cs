﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.CrossCutting.Comun;

namespace VELogisticCloud.CrossCutting.DTO
{
    public class ItinerarioResponseDTO
    {
        public ItinerarioResponseDTO()
        {
            legs = new List<LegDTO>();
        }
        public int idItinerarioManual { get; set; }
        public string scac { get; set; }
        public string carrierName { get; set; }
        public string serviceName { get; set; }
        public string vesselName { get; set; }
        public string voyageNumber { get; set; }
        public string imoNumber { get; set; }
        public string originUnloc { get; set; }
        public string originCountry { get; set; }
        public string originCityName { get; set; }
        public string originSubdivision { get; set; }
        public string originTerminal { get; set; }
        public string destinationUnloc { get; set; }
        public string destinationCountry { get; set; }
        public string destinationSubdivision { get; set; }
        public string destinationCityName { get; set; }
        public string destinationTerminal { get; set; }

        private DateTime? _originDepartureDate;
        public DateTime? originDepartureDate
        {
            get { return _originDepartureDate; }
            set
            {
                _originDepartureDate = value;
                if (_originDepartureDate.HasValue)
                {
                    var cultura = System.Globalization.CultureInfo.CreateSpecificCulture("es-PE");
                    diaSalida = _originDepartureDate.Value.ToString("dddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    diaSalidaAbreviado = _originDepartureDate.Value.ToString("ddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    numeroDiaSemana = (int)_originDepartureDate.Value.DayOfWeek;
                    originDepartureDateString = _originDepartureDate.Value.ToString(Formato.Fecha);
                    originDepartureDateStringUniversal = _originDepartureDate.Value.ToString(Formato.FechaAnioMesDia);
                }
                else
                {
                    diaSalida = string.Empty;
                    diaSalidaAbreviado = string.Empty;
                    numeroDiaSemana = 0;
                    originDepartureDateString = "";
                    originDepartureDateStringUniversal = "";
                }
            }
        }
        //public DateTime? originDepartureDate { get; set; }
        private DateTime? _destinationArrivalDate;
        public DateTime? destinationArrivalDate
        {
            get { return _destinationArrivalDate; }
            set 
            { 
                _destinationArrivalDate = value;

                if (_destinationArrivalDate.HasValue)
                {
                    var cultura = System.Globalization.CultureInfo.CreateSpecificCulture("es-PE");
                    diaLlegada = _destinationArrivalDate.Value.ToString("dddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    diaLlegadaAbreviado = _destinationArrivalDate.Value.ToString("ddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS

                    destinationArrivalDateString = _destinationArrivalDate.Value.ToString(Formato.Fecha);
                    destinationArrivalDateStringUniversal = _destinationArrivalDate.Value.ToString(Formato.FechaAnioMesDia);
                }
                else
                {
                    diaLlegada = "";
                    diaLlegadaAbreviado = "";
                    destinationArrivalDateString = "";
                    destinationArrivalDateStringUniversal = "";
                }

            }
        }
        private DateTime? _estimatedTerminalCutoff;
        public DateTime? estimatedTerminalCutoff
        {
            get { return _estimatedTerminalCutoff; }
            set 
            { 
                _estimatedTerminalCutoff = value;
                estimatedTerminalCutoffString = _estimatedTerminalCutoff.HasValue ? _estimatedTerminalCutoff.Value.ToString(Formato.Fecha) : "";
            }
        }
        private DateTime? _terminalCutoff;
        public DateTime? terminalCutoff
        {
            get { return _terminalCutoff; }
            set 
            { 
                _terminalCutoff = value;
                terminalCutoffString = _terminalCutoff.HasValue ? _terminalCutoff.Value.ToString(Formato.Fecha) : "";

            }
        }
        private DateTime? _bkCutoff;
        public DateTime? bkCutoff
        {
            get { return _bkCutoff; }
            set 
            { 
                _bkCutoff = value;
                bkCutoffString = _bkCutoff.HasValue ? _bkCutoff.Value.ToString(Formato.Fecha) : "";
            }
        }
        private DateTime? _siCutoff;
        public DateTime? siCutoff
        {
            get { return _siCutoff; }
            set 
            { 
                _siCutoff = value;
                siCutoffString = _siCutoff.HasValue ? _siCutoff.Value.ToString(Formato.Fecha) : "";
            }
        }
        private DateTime? _hazBkCutoff;
        public DateTime? hazBkCutoff
        {
            get { return _hazBkCutoff; }
            set 
            { 
                _hazBkCutoff = value;
                hazBkCutoffString = _hazBkCutoff.HasValue ? _hazBkCutoff.Value.ToString(Formato.Fecha) : "";
            }
        }
        private DateTime? _vgmCutoff;
        public DateTime? vgmCutoff
        {
            get { return _vgmCutoff; }
            set 
            { 
                _vgmCutoff = value;
                vgmCutoffString = _vgmCutoff.HasValue ? _vgmCutoff.Value.ToString(Formato.Fecha) : "";
            }
        }
        private DateTime? _reeferCutoff;
        public DateTime? reeferCutoff
        {
            get { return _reeferCutoff; }
            set 
            { 
                _reeferCutoff = value; 
                reeferCutoffString = _reeferCutoff.HasValue ? _reeferCutoff.Value.ToString(Formato.Fecha) : "";
            }
        }
        public int totalDuration { get; set; }
        public string scheduleType { get; set; }

        private List<LegDTO> _legs;
        public List<LegDTO> legs {
            get { return _legs; }
            set { 
                _legs = value;
                if (_legs != null && _legs.Count > 0)
                {
                    transbordo = "Si";
                }
                else
                {
                    transbordo = "No";
                }
            }
        }


        //PROPIEDADES PERSONALIZADAS

        public string diaSalida { get; set; }
        public string diaSalidaAbreviado { get; set; }
        public string diaLlegada { get; set; }
        public string diaLlegadaAbreviado { get; set; }
        public int numeroDiaSemana { get; set; }
        public string transbordo { get; set; }
        public string originDepartureDateString { get; set; }
        public string originDepartureDateStringUniversal { get; set; }
        public string destinationArrivalDateString { get; set; }
        public string destinationArrivalDateStringUniversal { get; set; }
        public string estimatedTerminalCutoffString { get; set; }
        public string terminalCutoffString { get; set; }
        public string bkCutoffString { get; set; }
        public string siCutoffString { get; set; }
        public string hazBkCutoffString { get; set; }
        public string vgmCutoffString { get; set; }
        public string reeferCutoffString { get; set; }


        //PROPIEDADES AGREGADAS
        public string puertoEmbarque { get; set; }
        public string puertoDestino { get; set; }
        public string FechaContratoExpiracion { get; set; }
        public int IdContrato { get; set; }
        public double Flete { get; set; }
        public double Neto { get; set; }
        public double Rebate { get; set; }

    }
}
