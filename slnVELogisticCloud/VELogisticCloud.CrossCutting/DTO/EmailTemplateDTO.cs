﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.DTO
{
    public class EmailTemplateDTO
    {
        public string NombreCliente { get; set; }
        public string NroCotizacion { get; set; }
        public string NroBooking { get; set; }
        public string NroSolicitud { get; set; }
        public string Canal { get; set; }
        public string FechaHora { get; set; }
        public string OrdenServicio { get; set; }
    }
}
