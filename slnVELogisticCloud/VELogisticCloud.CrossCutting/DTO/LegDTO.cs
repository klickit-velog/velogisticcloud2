﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.CrossCutting.Comun;

namespace VELogisticCloud.CrossCutting.DTO
{
    public class LegDTO
    {
        public int sequence { get; set; }
        public string transportID { get; set; }
        public string serviceName { get; set; }
        public string transportType { get; set; }
        public string transportName { get; set; }
        public string conveyanceNumber { get; set; }
        public string departureUnloc { get; set; }
        public string departureCityName { get; set; }
        public string departureCountry { get; set; }
        public string departureSubdivision { get; set; }
        public string departureTerminal { get; set; }
        private DateTime? _departureDate;
        public DateTime? departureDate
        {
            get { return _departureDate; }
            set 
            { 
                _departureDate = value;
                if (_departureDate.HasValue)
                {
                    var cultura = System.Globalization.CultureInfo.CreateSpecificCulture("es-PE");
                    departureDateStringDateName = _departureDate.Value.ToString("dddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    departureDateStringDateNameShort = _departureDate.Value.ToString("ddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    departureDateString = _departureDate.Value.ToString(Formato.Fecha);
                }
            }
        }
        private DateTime? _arrivalDate;
        public DateTime? arrivalDate
        {
            get { return _arrivalDate; }
            set 
            { 
                _arrivalDate = value;
                if (_arrivalDate.HasValue)
                {
                    var cultura = System.Globalization.CultureInfo.CreateSpecificCulture("es-PE");
                    arrivalDateStringDateName = _arrivalDate.Value.ToString("dddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    arrivalDateStringDateNameShort = _arrivalDate.Value.ToString("ddd", cultura); //TODO: CAMBIAR POR USO DE IDIOMAS
                    arrivalDateString = _arrivalDate.Value.ToString(Formato.Fecha);
                }
            }
        }
        public string arrivalUnloc { get; set; }
        public string arrivalCityName { get; set; }
        public string arrivalCountry { get; set; }
        public string arrivalSubdivision { get; set; }
        public string arrivalTerminal { get; set; }
        public bool transshipmentIndicator { get; set; }
        public int transitDuration { get; set; }

        //PROPIEDADES PERSONALIZADAS
        public string departureDateString { get; set; }
        public string departureDateStringDateName { get; set; }
        public string departureDateStringDateNameShort { get; set; }
        public string arrivalDateString { get; set; }
        public string arrivalDateStringDateName { get; set; }
        public string arrivalDateStringDateNameShort { get; set; }
    }
}
