﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.DTO
{
    public class ItinerarioRequestDTO
    {
        public string originPort { get; set; }
        public string destinationPort { get; set; }
        public DateTime searchDate { get; set; }
        public string searchDateType { get; set; }
        public int weeksOut { get; set; }
        public string sacs { get; set; }
        public bool directOnly { get; set; }
        public bool includeNearbyOriginPorts { get; set; }
        public bool includeNearbyDestinationPort { get; set; }
    }
}
