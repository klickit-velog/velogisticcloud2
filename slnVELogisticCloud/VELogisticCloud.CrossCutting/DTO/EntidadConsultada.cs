﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.DTO
{
    public class EntidadConsultada<T>
    {
        public List<T> Lista { get; set; }

        public int TotalRegistros { get; set; }
    }
}
