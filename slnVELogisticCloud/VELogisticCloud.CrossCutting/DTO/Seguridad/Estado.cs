﻿using System;

namespace VELogisticCloud.CrossCutting.Entidad.Seguridad
{
    public class Estado
    {
        public int IdEstado { get; set; }
        public int IdEstadoPadre { get; set; }
        public string Nombre { get; set; }
        public Boolean Activo { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdUsuarioActualiza { get; set; }
        public DateTime FechaActualizacion { get; set; }
    }
}
