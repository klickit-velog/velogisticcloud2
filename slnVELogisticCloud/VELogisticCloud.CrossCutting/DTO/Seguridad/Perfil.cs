﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.Entidad.Seguridad
{
    public class Perfil
    {
        public int IdPerfil { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int IdEstado { get; set; }

        public int IdUsuarioRegistro { get; set; }

        public DateTime FechaRegistro { get; set; }

        public int IdUsuarioActualiza { get; set; }

        public string FechaActualizacion { get; set; }

    }
}
