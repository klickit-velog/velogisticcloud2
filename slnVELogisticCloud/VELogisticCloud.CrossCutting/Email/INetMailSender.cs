﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.DTO;

namespace VELogisticCloud.CrossCutting.Mail
{
    public interface INetMailSender
    {
        Task sendMailAsync(string emailCliente, EmailTemplateDTO emailTemplate, TipoEmail tipo, string emailCustomer="");
    }
}
