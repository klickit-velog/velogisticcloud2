﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.DTO;

namespace VELogisticCloud.CrossCutting.Mail
{
    public class NetMailSender : INetMailSender
    {
        private IConfiguration configuration;

        public NetMailSender(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        //Envios de correos para Cotizacion Maritimo
        public async Task sendMailAsync(string emailCliente, EmailTemplateDTO emailTemplate, TipoEmail tipo, string emailCustomer="")
        {
            if (bool.Parse(configuration.GetSection("EnvioMail:EnvioCorreoActivo").Value))
            {
                MailMessage mail = new MailMessage();
                using (SmtpClient SmtpServer = new SmtpClient(configuration.GetSection("EnvioMail:Host").Value))
                {
                    string correo = ObtenerCuerpoCorreo(emailTemplate, tipo);

                    mail.IsBodyHtml = true;
                    mail.From = new MailAddress(
                                    configuration.GetSection("EnvioMail:DireccionRemitente").Value,
                                    configuration.GetSection("EnvioMail:NombreRemitente").Value
                                );

                    if (bool.Parse(configuration.GetSection("EnvioMail:ToActivo").Value))
                    {
                        mail.To.Add(emailCliente);
                    }

                    if (bool.Parse(configuration.GetSection("EnvioMail:CcActivo").Value))
                    {
                        mail.CC.Add(configuration.GetSection("EnvioMail:CcEmail").Value);
                    }

                    mail.Subject = ObtenerAsunto(emailTemplate,tipo);
                    mail.SubjectEncoding = Encoding.UTF8;
                    mail.Body = correo;
                    mail.BodyEncoding = Encoding.UTF8;

                    SmtpServer.Port = Convert.ToInt32(configuration.GetSection("EnvioMail:Puerto").Value);
                    SmtpServer.Credentials = new System.Net.NetworkCredential(
                                                configuration.GetSection("EnvioMail:Usuario").Value,
                                                configuration.GetSection("EnvioMail:Clave").Value
                                                );
                    SmtpServer.EnableSsl = true;
                    await SmtpServer.SendMailAsync(mail);
                }
            }
        }

        private string ObtenerCuerpoCorreo(EmailTemplateDTO emailTemplate, TipoEmail tipo)
        {
            string fullPath = Directory.GetCurrentDirectory().Replace("VELogisticCloud.App.Web", "VELogisticCloud.CrossCutting");
            string filePath = string.Concat(fullPath, @"\Email\Template\", tipo.ToString(), ".html");
            string correo = ObtenerBodyHTML(filePath);

            switch (tipo)
            {
                //COTIZACION MARITIMA
                case TipoEmail.COTIZACION_MARITIMA_1_RECIBIDO:
                case TipoEmail.COTIZACION_MARITIMA_2_ATENDIDO:
                case TipoEmail.COTIZACION_MARITIMA_3_ACEPTADO:
                    correo = correo.Replace("{NombreCliente}", emailTemplate.NombreCliente);
                    correo = correo.Replace("{NroCotizacion}", emailTemplate.NroCotizacion);
                    break;
                
                //COTIZACION LOGISTICA
                case TipoEmail.COTIZACION_LOGISTICA_1_RECIBIDO:
                case TipoEmail.COTIZACION_LOGISTICA_2_ATENDIDO:
                case TipoEmail.COTIZACION_LOGISTICA_3_ACEPTADO:
                    correo = correo.Replace("{NombreCliente}", emailTemplate.NombreCliente);
                    correo = correo.Replace("{NroCotizacion}", emailTemplate.NroCotizacion);
                    break;

                //BOOKING
                case TipoEmail.BOOKING_1_RECIBIDO:
                case TipoEmail.BOOKING_2_ACEPTADO:
                case TipoEmail.BOOKING_3_GENERADO: //TODO: CONFIRMAR SI SE ESTA USUANDO BIEN ESTA PLANTILLA DE CORREO
                case TipoEmail.BOOKING_4_INTTRA_VESSEL_LOAD:
                case TipoEmail.BOOKING_5_INTTRA_VESSEL_DEPARTED:
                case TipoEmail.BOOKING_6_INTTRA_VESSEL_ARRIVAL:
                case TipoEmail.BOOKING_7_INTTRA_VESSEL_DISCHARGE:
                case TipoEmail.BOOKING_8_INTTRA_GATED_OUR:
                case TipoEmail.BOOKING_9_INTTRA_RETURNED:
                //case TipoEmail.BOOKING_10_INTTRA_LIQUIDADO:
                    correo = correo.Replace("{NombreCliente}", emailTemplate.NombreCliente);
                    correo = correo.Replace("{NroSolicitud}", emailTemplate.NroSolicitud);
                    correo = correo.Replace("{NroBooking}", emailTemplate.NroBooking);
                    break;

                //OPL
                case TipoEmail.OPL_1_RECIBIDO:
                case TipoEmail.OPL_2_ACEPTADO:
                case TipoEmail.OPL_3_CANAL:
                case TipoEmail.OPL_4_AVANZZA_EN_CAMINO_DEPOSITO_VACIO:
                case TipoEmail.OPL_5_AVANZZA_LLEGO_DEPOSITO_VACIO:
                //case TipoEmail.OPL_6_AVANZZA_CONTENEDOR_ASIGNADO:
                case TipoEmail.OPL_7_AVANZZA_EN_CAMINO_PACKING:
                case TipoEmail.OPL_8_AVANZZA_LLEGO_AL_PACKING:
                //case TipoEmail.OPL_9_AVANZZA_INGRESO_LLENAR_CARGA:
                case TipoEmail.OPL_10_AVANZZA_SALIENDO_DEL_PACKING:
                case TipoEmail.OPL_11_AVANZZA_INSPECCION_SINI:
                case TipoEmail.OPL_12_AVANZZA_CAMINIO_AL_PUERTO:
                case TipoEmail.OPL_13_AVANZZA_INGRESANDO_AL_PUERTO:
                case TipoEmail.OPL_14_AVANZZA_CONTENEDOR_EN_PUERTO:
                    correo = correo.Replace("{NombreCliente}", emailTemplate.NombreCliente);
                    correo = correo.Replace("{NroBooking}", emailTemplate.NroBooking);
                    correo = correo.Replace("{OrdenServicio}", emailTemplate.OrdenServicio);
                    correo = correo.Replace("{canal}", emailTemplate.Canal);
                    correo = correo.Replace("#Fechayhora", emailTemplate.FechaHora);
                    break;
                default:
                    break;
            }           

            return correo;
        }

        private string ObtenerAsunto(EmailTemplateDTO emailTemplate , TipoEmail tipo)
        {
            string asunto = string.Empty;

            switch (tipo)
            {
                case TipoEmail.COTIZACION_MARITIMA_1_RECIBIDO:
                    asunto = string.Format("Hemos recibido su solicitud cotización de Flete Naviero #{0}", emailTemplate.NroCotizacion);
                    break;
                case TipoEmail.COTIZACION_MARITIMA_2_ATENDIDO:
                    asunto = string.Format("Hemos atendido su Cotizacion de Flete Naviero #{0}", emailTemplate.NroCotizacion);
                    break;
                case TipoEmail.COTIZACION_MARITIMA_3_ACEPTADO:
                    asunto = string.Format("Cotizacion aceptada de Flete Naviero #{0}", emailTemplate.NroCotizacion);
                    break;
                case TipoEmail.COTIZACION_LOGISTICA_1_RECIBIDO:
                    asunto = string.Format("Hemos recibido su Cotizacion de Operación Logística #{0}", emailTemplate.NroCotizacion);
                    break;
                case TipoEmail.COTIZACION_LOGISTICA_2_ATENDIDO:
                    asunto = string.Format("Hemos atendido su Cotizacion de Operación Logística #{0}", emailTemplate.NroCotizacion);
                    break;
                case TipoEmail.COTIZACION_LOGISTICA_3_ACEPTADO:
                    asunto = string.Format("Cotizacion aceptada de Operación Logística #{0}", emailTemplate.NroCotizacion);
                    break;
                case TipoEmail.BOOKING_1_RECIBIDO:
                    asunto = string.Format("Hemos recibido su solicitud de Booking #{0}", emailTemplate.NroSolicitud);
                    break;
                case TipoEmail.BOOKING_2_ACEPTADO:
                    asunto = string.Format("Hemos aceptado su Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.BOOKING_3_GENERADO:
                    asunto = string.Format("Hemos generado su Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.BOOKING_4_INTTRA_VESSEL_LOAD:
                    asunto = "Su contenedor fue cargado a nave";
                    break;
                case TipoEmail.BOOKING_5_INTTRA_VESSEL_DEPARTED:
                    asunto = "Su contenedor acaba de zarpar del puerto";
                    break;
                case TipoEmail.BOOKING_6_INTTRA_VESSEL_ARRIVAL:
                    asunto = "Su contenedor arribó al puerto";
                    break;
                case TipoEmail.BOOKING_7_INTTRA_VESSEL_DISCHARGE:
                    asunto = "Su contenedor se descargó de la nave";
                    break;
                case TipoEmail.BOOKING_8_INTTRA_GATED_OUR:
                    asunto = "Su contenedor salió del puerto destino";
                    break;
                case TipoEmail.BOOKING_9_INTTRA_RETURNED:
                    asunto = "Su carga fué entregada al destinatario";
                    break;
                //case TipoEmail.BOOKING_10_INTTRA_LIQUIDADO:
                //    break;
                case TipoEmail.OPL_1_RECIBIDO:
                    asunto = string.Format("Hemos recibido su solicitud de Operación Logística, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_2_ACEPTADO:
                    asunto = string.Format("Hemos aceptado el servicio de Operación Logística, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_3_CANAL:
                    asunto = string.Format("Su carga se actualizó, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_4_AVANZZA_EN_CAMINO_DEPOSITO_VACIO:
                    asunto = string.Format("El camión está en camino al depósito de vacío, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_5_AVANZZA_LLEGO_DEPOSITO_VACIO:
                    asunto = string.Format("El camión llegó al depósito de vacío, Booking #{0}", emailTemplate.NroBooking);
                    break;
                //case TipoEmail.OPL_6_AVANZZA_CONTENEDOR_ASIGNADO:
                //    break;
                case TipoEmail.OPL_7_AVANZZA_EN_CAMINO_PACKING:
                    asunto = string.Format("El camión está camino al packing, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_8_AVANZZA_LLEGO_AL_PACKING:
                    asunto = string.Format("El camión llegó al packing, Booking #{0}", emailTemplate.NroBooking);
                    break;
                //case TipoEmail.OPL_9_AVANZZA_INGRESO_LLENAR_CARGA:
                //    break;
                case TipoEmail.OPL_10_AVANZZA_SALIENDO_DEL_PACKING:
                    asunto = string.Format("El camión está saliendo del packing, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_11_AVANZZA_INSPECCION_SINI:
                    asunto = string.Format("El camión llegó a la inspección SINI, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_12_AVANZZA_CAMINIO_AL_PUERTO:
                    asunto = string.Format("El camión está llegando al puerto, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_13_AVANZZA_INGRESANDO_AL_PUERTO:
                    asunto = string.Format("El camión está ingresando al puerto, Booking #{0}", emailTemplate.NroBooking);
                    break;
                case TipoEmail.OPL_14_AVANZZA_CONTENEDOR_EN_PUERTO:
                    asunto = string.Format("El camión dejó el contenedor en el puerto, Booking #{0}", emailTemplate.NroBooking);
                    break;
                default:
                    break;
            }

            return asunto;
        }

        private string ObtenerBodyHTML(string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                return reader.ReadToEnd();
            }
        }

        private Boolean email_bien_escrito(string email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

    }
}
