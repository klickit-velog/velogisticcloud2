﻿using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.Comun
{
    public interface IEmailService
    {
        void Send(string to, string subject, string html);
    }

    public class EmailService : IEmailService
    {
        private readonly AppSettings _appSettings;

        public EmailService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public void Send(string to, string subject, string html)
        {
            // create message
            var email = new MimeMessage();
            //email.From.Add(MailboxAddress.Parse(_appSettings.EmailFrom));
            //email.To.Add(MailboxAddress.Parse("jaldave@klickit.io"));//MailboxAddress.Parse(to));

            email.From.Add(new MailboxAddress("Notificaciones",_appSettings.EmailFrom));
            email.To.Add(new MailboxAddress("Jonathan","jaldave@klickit.io"));

            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = html };

            // send email
            using var smtp = new SmtpClient(new ProtocolLogger("D:\\TMP\\correo.log"));
            //smtp.ServerCertificateValidationCallback = (s, c, h, e) => true;
            //smtp.Connect(_appSettings.SmtpHost, _appSettings.SmtpPort, SecureSocketOptions.StartTls);
            smtp.Connect(_appSettings.SmtpHost, _appSettings.SmtpPort);
            smtp.AuthenticationMechanisms.Remove("XOAUTH2");
            smtp.Authenticate(_appSettings.SmtpUser, _appSettings.SmtpPass);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
