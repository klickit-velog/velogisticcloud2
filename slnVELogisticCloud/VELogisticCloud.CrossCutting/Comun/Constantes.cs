﻿namespace VELogisticCloud.CrossCutting.Comun
{
    public static class Constantes
    {
        // Codigos Padre de las tablas de Seguridad
        //public const int SEG_Usuario = 1;
        //public const int SEG_Opcion = 4;
        //public const int SEG_OpcionPerfil = 11;
        //public const int SEG_Perfil = 14;
        //public const int SEG_TipoOpcion = 17;
    }

    public static class EstadoUsuario
    {
        public const string ACTIVO = "Activo";
        public const string INACTIVO = "Inactivo";

    }

    public static class Rol
    {
        public const string ADMINISTRADOR = "Administrador";
        public const string CUSTOMER_SERVICE = "Customer Service";
        public const string CLIENTE = "Cliente";

    }

    public static class TipoCotizacion
    {
        public const int FLETE_MARITIMO = 1;
        public const int OPERACION_LOGISTICA = 2;
        public const int TRANSPORTE = 3;
    }

    public static class Formato
    {
        public const string Fecha = "dd-MM-yyyy";
        public const string FechaAnioMesDia = "yyyy-MM-dd";
        public const string FechaGeneral = "dd/MM/yyyy";
    }

    public static class ClaimVECloud
    {
        public const string IdCliente = "ID_CLIENTE";
        public const string NomCliente = "NOM_CLIENTE";
    }
    public static class ErroresBooking
    {
        public const string NO_SE_PUDO_GRABAR_BOOKING = "No se pudo grabar el booking.";
        public const string NO_SE_GRABA_CON_USUARIO_CUSTOMER = "No se puede grabar con usuario Customer.";
        public const string NO_SE_OBTUVO_PARTIDA_ARANCELARIA = "No se pudo Obtener partida arancelaria de Odoo.";
        public const string CODIGO_RESERVA_NO_EXISTE = "Codigo de reserva no existe.";
        public const string NO_SE_PUDO_TRAER_RESUMEN = "No se pudo traer datos de resumen.";
        public const string NO_SE_PUDO_TRAER_DATOS_RESERVA = "No se pudo cargar datos de la reserva.";
        public const string NO_SE_PUDO_ACTUALIZAR_IMO_NAVE = "No se pudo actualizar el imo-nave.";
        public const string NO_SE_PUDO_ACTUALIZAR = "No se pudo actualizar los datos.";
        public const string NO_SE_PUDO_CANCELAR_BOOKING = "No se pudo cancelar el booking.";
        public const string ENVIO_ODOO_ERROR = "No se pudo enviar datos del booking correctamente.";


    }
    public static class InformativoBooking
    {
        public const string FALTA_INGRESAR_CONTRACT_OWNER= "Falta ingresar correctamente Contract Owner.";
        public const string FALTA_INGRESAR_CANTIDAD_BOOKING= "Falta ingresar cantidad de booking.";

    }
    public static class CorrectoBooking
    {
        public const string CREACION_CORRECTA = "Se registro la reserva correctamente.";
        public const string ACTUALIZACION_CORRECTA = "Se actualizo la reserva correctamente.";
        public const string FALTA_INGRESAR_CANTIDAD_BOOKING = "Falta ingresar cantidad de booking.";
        public const string CANCELAR_RESERVA_CORRECTA = "Se cancelo la reserva correctamente.";
        public const string ENVIO_ODOO_CORRECTO = "Booking enviado correctamente.";

    }

    public static class CotizacionMaritimaCorrecto
    {
        public const string ENVIO_ODOO_CORRECTA = "Se registro la cotizacion correctamente.";
    }

    public static class CotizacionLogisticaCorrecto
    {
        public const string CREACION_CORRECTA = "Se registro correctamente.";
    }

    public static class CotizacionMaritimaError
    {
        public const string ENVIO_ODOO_ERROR = "No se pudo enviar a odoo.";

    }

    public static class CotizacionLogisticaError
    {
        public const string ENVIO_LOGISTICA_ERROR = "No se pudo enviar a odoo.";
        public const string COTIZACION_DETALLE_VACIA = "Lista de cotizacion detalle vacia.";
        
    }
}
