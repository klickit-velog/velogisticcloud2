﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace VELogisticCloud.CrossCutting.Comun
{
    public class Funciones
    {
        public byte[] GetExcel(DataTable dtData, string rutaPlantilla)
        {
            byte[] resultado = default;
            int filaInicio = 3;

            DataRow fila = dtData.Rows[0];
            //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(new System.IO.FileInfo(rutaPlantilla)))
            {
                var worksheet = package.Workbook.Worksheets[0];

                //EXPORTADOR
                worksheet.Cells[$"C{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorNombre");
                worksheet.Cells[$"C{filaInicio + 1}"].Value = fila.FieldOrDefault<string>("ExportadorDomicilio");
                worksheet.Cells[$"C{filaInicio + 2}"].Value = fila.FieldOrDefault<string>("ExportadorRuc");
                worksheet.Cells[$"G{filaInicio + 2}"].Value = fila.FieldOrDefault<string>("ExportadorTelefono");
                worksheet.Cells[$"I{filaInicio + 2}"].Value = fila.FieldOrDefault<string>("ExportadorFax");
                worksheet.Cells[$"C{filaInicio + 3}"].Value = fila.FieldOrDefault<string>("ExportadorContacto");
                worksheet.Cells[$"I{filaInicio + 3}"].Value = fila.FieldOrDefault<string>("ExportadorCelular");
                //CONSIGNATARIO
                worksheet.Cells[$"C{filaInicio + 6}"].Value = fila.FieldOrDefault<string>("ConsignatarioNombre");
                worksheet.Cells[$"C{filaInicio + 7}"].Value = fila.FieldOrDefault<string>("ConsignatarioDireccion");
                worksheet.Cells[$"I{filaInicio + 7}"].Value = fila.FieldOrDefault<string>("ConsignatarioTelefono");
                worksheet.Cells[$"C{filaInicio + 8}"].Value = fila.FieldOrDefault<string>("ConsignatarioContacto");
                worksheet.Cells[$"I{filaInicio + 8}"].Value = fila.FieldOrDefault<string>("ConsignatarioFax");
                //NOTIFY
                worksheet.Cells[$"C{filaInicio + 11}"].Value = fila.FieldOrDefault<string>("NotifyNombre");
                worksheet.Cells[$"C{filaInicio + 12}"].Value = fila.FieldOrDefault<string>("NotifyDireccion");
                worksheet.Cells[$"I{filaInicio + 12}"].Value = fila.FieldOrDefault<string>("NotifyTelefono");
                worksheet.Cells[$"C{filaInicio + 13}"].Value = fila.FieldOrDefault<string>("NotifyContacto");
                worksheet.Cells[$"I{filaInicio + 13}"].Value = fila.FieldOrDefault<string>("NotifyFax");
                //BLOQUE BOOKING
                worksheet.Cells[$"C{filaInicio + 15}"].Value = fila.FieldOrDefault<string>("NroBooking");
                worksheet.Cells[$"C{filaInicio + 16}"].Value = fila.FieldOrDefault<string>("Linea");
                worksheet.Cells[$"C{filaInicio + 17}"].Value = fila.FieldOrDefault<string>("NombreNave");
                worksheet.Cells[$"C{filaInicio + 18}"].Value = fila.FieldOrDefault<string>("PuertoEmbarque");
                worksheet.Cells[$"C{filaInicio + 19}"].Value = fila.FieldOrDefault<string>("PuertoDescarga");
                worksheet.Cells[$"C{filaInicio + 20}"].Value = fila.FieldOrDefault<DateTime>("ETD");
                worksheet.Cells[$"C{filaInicio + 21}"].Value = fila.FieldOrDefault<DateTime>("ETA");
                worksheet.Cells[$"C{filaInicio + 23}"].Value = fila.FieldOrDefault<string>("TipoContenedor");

                //VALORES POR TIPO DE CONTENEDOR
                var claseContenedor  = fila.FieldOrDefault<string>("ClaseContenedor");

                if (claseContenedor.ToUpper() == ClaseContenedor.REEFER.ToString())
                {
                    worksheet.Cells[$"C{filaInicio + 25}"].Value = fila.FieldOrDefault<bool>("AtmosferaControlada");
                    worksheet.Cells[$"F{filaInicio + 25}"].Value = fila.FieldOrDefault<decimal>("CO2");
                    worksheet.Cells[$"I{filaInicio + 25}"].Value = fila.FieldOrDefault<decimal>("O2");
                    worksheet.Cells[$"C{filaInicio + 26}"].Value = fila.FieldOrDefault<bool>("ColdTreatment");
                    worksheet.Cells[$"C{filaInicio + 27}"].Value = fila.FieldOrDefault<decimal>("Temperatura");
                    worksheet.Cells[$"F{filaInicio + 27}"].Value = fila.FieldOrDefault<string>("TipoTemperatura");
                    worksheet.Cells[$"C{filaInicio + 28}"].Value = fila.FieldOrDefault<decimal>("Ventilacion");
                    worksheet.Cells[$"F{filaInicio + 28}"].Value = fila.FieldOrDefault<string>("TipoVentilacion");
                    worksheet.Cells[$"C{filaInicio + 29}"].Value = fila.FieldOrDefault<decimal>("Humedad");
                    worksheet.Cells[$"F{filaInicio + 29}"].Value = fila.FieldOrDefault<bool>("TipoHumedad");

                    //OCULTA DRY
                    worksheet.Row(31).Hidden = true;
                    worksheet.Row(32).Hidden = true;
                }
                else
                {
                    worksheet.Cells[$"C{filaInicio + 31}"].Value = fila.FieldOrDefault<string>("IMO");
                    worksheet.Cells[$"F{filaInicio + 31}"].Value = fila.FieldOrDefault<string>("UN1");
                    worksheet.Cells[$"I{filaInicio + 31}"].Value = fila.FieldOrDefault<string>("UN2");

                    //OCULTAR REEFER
                    worksheet.Row(25).Hidden = true;
                    worksheet.Row(26).Hidden = true;
                    worksheet.Row(27).Hidden = true;
                    worksheet.Row(28).Hidden = true;
                    worksheet.Row(29).Hidden = true;
                    worksheet.Row(30).Hidden = true;
                }

                //BLOQUE OPERACION LOGISTICA
                worksheet.Cells[$"C{filaInicio + 33}"].Value = fila.FieldOrDefault<string>("Commodity");
                worksheet.Cells[$"C{filaInicio + 34}"].Value = fila.FieldOrDefault<string>("Variedad");
                worksheet.Cells[$"C{filaInicio + 35}"].Value = fila.FieldOrDefault<string>("PartidaArancelaria");
                worksheet.Cells[$"C{filaInicio + 36}"].Value = fila.FieldOrDefault<string>("TipoBulto");
                worksheet.Cells[$"C{filaInicio + 37}"].Value = fila.FieldOrDefault<decimal>("CantidadBulto");
                worksheet.Cells[$"C{filaInicio + 38}"].Value = fila.FieldOrDefault<decimal>("PesoBruto");
                worksheet.Cells[$"C{filaInicio + 39}"].Value = fila.FieldOrDefault<string>("MarcasProducto");
                worksheet.Cells[$"C{filaInicio + 40}"].Value = fila.FieldOrDefault<decimal>("PesoCaja");
                worksheet.Cells[$"C{filaInicio + 41}"].Value = fila.FieldOrDefault<string>("CantidadPaletas");
                worksheet.Cells[$"C{filaInicio + 42}"].Value = fila.FieldOrDefault<string>("CondicionPagoCommodity");
                worksheet.Cells[$"C{filaInicio + 43}"].Value = fila.FieldOrDefault<decimal>("PesoNeto");


                //CONDICION DE PAGO
                if (fila.FieldOrDefault<string>("CondicionPago").ToUpper() == "ELSEWHERE")
                {
                    worksheet.Cells[$"C{filaInicio + 48}"].Value = fila.FieldOrDefault<string>("NombrePagador");
                    worksheet.Cells[$"C{filaInicio + 49}"].Value = fila.FieldOrDefault<string>("DireccionPagador");
                }
                else
                {
                    worksheet.Row(48).Hidden = true;
                    worksheet.Row(49).Hidden = true;
                }
                worksheet.Cells[$"C{filaInicio + 47}"].Value = fila.FieldOrDefault<string>("CondicionPago");                
                worksheet.Cells[$"C{filaInicio + 50}"].Value = fila.FieldOrDefault<string>("EmisionBL");

                worksheet.Cells[$"C{filaInicio + 52}"].Value = fila.FieldOrDefault<string>("OperadorLogistico");
                worksheet.Cells[$"H{filaInicio + 52}"].Value = fila.FieldOrDefault<string>("RucOperador");

                worksheet.Cells[$"C{filaInicio + 54}"].Value = fila.FieldOrDefault<string>("ValorFob");
                worksheet.Cells[$"C{filaInicio + 55}"].Value = fila.FieldOrDefault<string>("Incoterm");
                worksheet.Cells[$"C{filaInicio + 56}"].Value = fila.FieldOrDefault<decimal>("CantidadTermoregistros");
                worksheet.Cells[$"C{filaInicio + 57}"].Value = fila.FieldOrDefault<decimal>("CantidadFiltrosEtileno");
                worksheet.Cells[$"C{filaInicio + 58}"].Value = fila.FieldOrDefault<string>("Observaciones");

                worksheet.Cells[$"C{filaInicio + 60}"].Value = fila.FieldOrDefault<string>("OrigenProducto");
                worksheet.Cells[$"C{filaInicio + 61}"].Value = fila.FieldOrDefault<DateTime>("CitaPacking");
                worksheet.Cells[$"C{filaInicio + 62}"].Value = fila.FieldOrDefault<DateTime>("HoraInspeccionSenasa");
                worksheet.Cells[$"C{filaInicio + 63}"].Value = fila.FieldOrDefault<string>("RazonSocialPacking");
                worksheet.Cells[$"C{filaInicio + 64}"].Value = fila.FieldOrDefault<string>("RucPacking");
                worksheet.Cells[$"C{filaInicio + 65}"].Value = fila.FieldOrDefault<string>("DireccionPacking");
                worksheet.Cells[$"C{filaInicio + 66}"].Value = fila.FieldOrDefault<bool>("Drawback");
                worksheet.Cells[$"C{filaInicio + 67}"].Value = fila.FieldOrDefault<string>("DepositoVacio");
                worksheet.Cells[$"C{filaInicio + 68}"].Value = fila.FieldOrDefault<string>("PuertoEmbarqueInttra");

                resultado = package.GetAsByteArray();
            }

            return resultado;
        }
        public byte[] GetExcelBooking(DataTable dtData, string rutaPlantilla)
        {
            byte[] resultado = default;

            try
            {
                //int filaInicio = 2;
                //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var package = new ExcelPackage(new System.IO.FileInfo(rutaPlantilla)))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    DataRow fila = dtData.Rows[0];
                    int filaDt = 0;
                    int cantidad = dtData.Rows.Count + 1;
                    for (int filaInicio = 2; filaInicio <= cantidad; filaInicio++)
                    {
                        fila = dtData.Rows[filaDt];
                        worksheet.Cells[$"A{filaInicio}"].Value = fila.FieldOrDefault<int>("IdBooking");
                        worksheet.Cells[$"B{filaInicio}"].Value = fila.FieldOrDefault<string>("LineaNaviera");
                        worksheet.Cells[$"C{filaInicio}"].Value = fila.FieldOrDefault<string>("CustomerService");
                        worksheet.Cells[$"D{filaInicio}"].Value = fila.FieldOrDefault<string>("NroContrato");
                        worksheet.Cells[$"E{filaInicio}"].Value = fila.FieldOrDefault<string>("NumeroBL");
                        worksheet.Cells[$"F{filaInicio}"].Value = fila.FieldOrDefault<string>("NroBooking");
                        worksheet.Cells[$"G{filaInicio}"].Value = fila.FieldOrDefault<string>("FechaSolicitud");
                        worksheet.Cells[$"H{filaInicio}"].Value = fila.FieldOrDefault<string>("Estado");
                        worksheet.Cells[$"I{filaInicio}"].Value = fila.FieldOrDefault<string>("NombreNave");
                        worksheet.Cells[$"J{filaInicio}"].Value = fila.FieldOrDefault<string>("NroViaje");
                        worksheet.Cells[$"K{filaInicio}"].Value = fila.FieldOrDefault<string>("PuertoEmbarque");
                        worksheet.Cells[$"L{filaInicio}"].Value = fila.FieldOrDefault<string>("PuertoDescarga");
                        worksheet.Cells[$"M{filaInicio}"].Value = fila.FieldOrDefault<string>("Commodity");
                        worksheet.Cells[$"N{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorNombre");
                        worksheet.Cells[$"O{filaInicio}"].Value = fila.FieldOrDefault<string>("NombreConsignatario");
                        worksheet.Cells[$"P{filaInicio}"].Value = fila.FieldOrDefault<string>("NombreNotify");
                        worksheet.Cells[$"Q{filaInicio}"].Value = fila.FieldOrDefault<string>("OperadorLogistico");
                        worksheet.Cells[$"R{filaInicio}"].Value = fila.FieldOrDefault<string>("CutOff");
                        worksheet.Cells[$"S{filaInicio}"].Value = fila.FieldOrDefault<string>("ETD");
                        worksheet.Cells[$"T{filaInicio}"].Value = fila.FieldOrDefault<string>("TiempoTransito");
                        worksheet.Cells[$"U{filaInicio}"].Value = fila.FieldOrDefault<string>("ETA");
                        worksheet.Cells[$"V{filaInicio}"].Value = fila.FieldOrDefault<string>("TipoContenedor");
                        worksheet.Cells[$"W{filaInicio}"].Value = fila.FieldOrDefault<string>("ClaseContenedor");
                        worksheet.Cells[$"X{filaInicio}"].Value = fila.FieldOrDefault<string>("Campania");
                        worksheet.Cells[$"Y{filaInicio}"].Value = fila.FieldOrDefault<string>("ColdTreatment");
                        worksheet.Cells[$"Z{filaInicio}"].Value = fila.FieldOrDefault<string>("CantidadContenedor");
                        worksheet.Cells[$"AA{filaInicio}"].Value = fila.FieldOrDefault<string>("AtmosferaControlada");
                        worksheet.Cells[$"AB{filaInicio}"].Value = fila.FieldOrDefault<string>("PaisDescarga");
                        worksheet.Cells[$"AC{filaInicio}"].Value = fila.FieldOrDefault<string>("CO2");
                        worksheet.Cells[$"AD{filaInicio}"].Value = fila.FieldOrDefault<string>("O2");
                        worksheet.Cells[$"AE{filaInicio}"].Value = fila.FieldOrDefault<string>("TipoTemperatura");
                        worksheet.Cells[$"AF{filaInicio}"].Value = fila.FieldOrDefault<string>("Temperatura");
                        worksheet.Cells[$"AG{filaInicio}"].Value = fila.FieldOrDefault<string>("TipoVentilacion");
                        worksheet.Cells[$"AH{filaInicio}"].Value = fila.FieldOrDefault<string>("Ventilacion");
                        worksheet.Cells[$"AI{filaInicio}"].Value = fila.FieldOrDefault<string>("Humedad");
                        worksheet.Cells[$"AJ{filaInicio}"].Value = fila.FieldOrDefault<string>("UN1");
                        worksheet.Cells[$"AK{filaInicio}"].Value = fila.FieldOrDefault<string>("UN2");
                        worksheet.Cells[$"AL{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorDomicilio");
                        worksheet.Cells[$"AM{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorRuc");
                        worksheet.Cells[$"AN{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorContacto");
                        worksheet.Cells[$"AO{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorTelefono");
                        worksheet.Cells[$"AP{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorFax");
                        worksheet.Cells[$"AQ{filaInicio}"].Value = fila.FieldOrDefault<string>("ExportadorCelular");
                        worksheet.Cells[$"AR{filaInicio}"].Value = fila.FieldOrDefault<string>("DireccionConsignatario");
                        worksheet.Cells[$"AS{filaInicio}"].Value = fila.FieldOrDefault<string>("FaxConsignatario");
                        worksheet.Cells[$"AT{filaInicio}"].Value = fila.FieldOrDefault<string>("TelefonoConsignatario");
                        worksheet.Cells[$"AU{filaInicio}"].Value = fila.FieldOrDefault<string>("PersonaContactoConsignatario");
                        worksheet.Cells[$"AV{filaInicio}"].Value = fila.FieldOrDefault<string>("DireccionNotify");
                        worksheet.Cells[$"AW{filaInicio}"].Value = fila.FieldOrDefault<string>("FaxNotify");
                        worksheet.Cells[$"AX{filaInicio}"].Value = fila.FieldOrDefault<string>("TelefonoNotify");
                        worksheet.Cells[$"AY{filaInicio}"].Value = fila.FieldOrDefault<string>("PersonaContactoNotify");
                        worksheet.Cells[$"AZ{filaInicio}"].Value = fila.FieldOrDefault<string>("CodLinea");
                        worksheet.Cells[$"BA{filaInicio}"].Value = fila.FieldOrDefault<string>("PaisEmbarque");
                        worksheet.Cells[$"BB{filaInicio}"].Value = fila.FieldOrDefault<string>("Variedad");
                        worksheet.Cells[$"BC{filaInicio}"].Value = fila.FieldOrDefault<string>("PartidaArancelaria");
                        worksheet.Cells[$"BD{filaInicio}"].Value = fila.FieldOrDefault<string>("TipoBulto");
                        worksheet.Cells[$"BE{filaInicio}"].Value = fila.FieldOrDefault<string>("CantidadBulto");
                        worksheet.Cells[$"BF{filaInicio}"].Value = fila.FieldOrDefault<string>("PesoBruto");
                        worksheet.Cells[$"BG{filaInicio}"].Value = fila.FieldOrDefault<string>("CondicionPago");
                        worksheet.Cells[$"BH{filaInicio}"].Value = fila.FieldOrDefault<string>("EmisionBL");
                        worksheet.Cells[$"BI{filaInicio}"].Value = fila.FieldOrDefault<string>("RucOperador");
                        worksheet.Cells[$"BJ{filaInicio}"].Value = fila.FieldOrDefault<string>("Nota");
                        worksheet.Cells[$"BK{filaInicio}"].Value = fila.FieldOrDefault<string>("Cliente");
                        worksheet.Cells[$"BL{filaInicio}"].Value = fila.FieldOrDefault<string>("UsuarioCreacion");
                        worksheet.Cells[$"BM{filaInicio}"].Value = fila.FieldOrDefault<string>("IdOdoo");
                        worksheet.Cells[$"BN{filaInicio}"].Value = fila.FieldOrDefault<string>("ImoInttra");
                        worksheet.Cells[$"BO{filaInicio}"].Value = fila.FieldOrDefault<string>("CantidadBooking");
                        worksheet.Cells[$"BP{filaInicio}"].Value = fila.FieldOrDefault<string>("ContactOwner");
                        worksheet.Cells[$"BQ{filaInicio}"].Value = fila.FieldOrDefault<string>("NombreCustomer");
                        worksheet.Cells[$"BR{filaInicio}"].Value = fila.FieldOrDefault<string>("TipoContrato");
                        filaDt++;
                    }
                    resultado = package.GetAsByteArray();
                }

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }
            
            return resultado;
        }
        public byte[] GetExcelOpl(DataTable dtData, string rutaPlantilla)
        {
            byte[] resultado = default;

            try
            {
                //int filaInicio = 2;
                //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var package = new ExcelPackage(new System.IO.FileInfo(rutaPlantilla)))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    DataRow fila = dtData.Rows[0];
                    int cantRegistros = dtData.Rows.Count + 1;
                    int filaDt = 0;
                    for (int filaInicio = 2; filaInicio <= cantRegistros; filaInicio++)
                    {
                        fila = dtData.Rows[filaDt];
                        worksheet.Cells[$"A{filaInicio}"].Value = fila.FieldOrDefault<int>("IdOperacionLogistica");
                        worksheet.Cells[$"B{filaInicio}"].Value = fila.FieldOrDefault<string>("NroBooking");
                        worksheet.Cells[$"C{filaInicio}"].Value = fila.FieldOrDefault<string>("CodLinea");
                        worksheet.Cells[$"D{filaInicio}"].Value = fila.FieldOrDefault<string>("Linea");
                        worksheet.Cells[$"E{filaInicio}"].Value = fila.FieldOrDefault<string>("NombreNave");
                        worksheet.Cells[$"F{filaInicio}"].Value = fila.FieldOrDefault<string>("ETA");
                        worksheet.Cells[$"G{filaInicio}"].Value = fila.FieldOrDefault<string>("CodigoPaisEmbarque");
                        worksheet.Cells[$"H{filaInicio}"].Value = fila.FieldOrDefault<string>("PaisEmbarque");
                        worksheet.Cells[$"I{filaInicio}"].Value = fila.FieldOrDefault<int>("IdPuertoEmbarque");
                        worksheet.Cells[$"J{filaInicio}"].Value = fila.FieldOrDefault<string>("CodigoPuertoEmbarque");
                        worksheet.Cells[$"K{filaInicio}"].Value = fila.FieldOrDefault<string>("PuertoEmbarque");
                        worksheet.Cells[$"L{filaInicio}"].Value = fila.FieldOrDefault<string>("CodigoPaisDescarga");
                        worksheet.Cells[$"M{filaInicio}"].Value = fila.FieldOrDefault<string>("PaisDescarga");
                        worksheet.Cells[$"N{filaInicio}"].Value = fila.FieldOrDefault<int>("IdPuertoDescarga");
                        worksheet.Cells[$"O{filaInicio}"].Value = fila.FieldOrDefault<string>("CodigoPuertoDescarga");
                        worksheet.Cells[$"P{filaInicio}"].Value = fila.FieldOrDefault<string>("PuertoDescarga");
                        worksheet.Cells[$"Q{filaInicio}"].Value = fila.FieldOrDefault<string>("MarcasProducto");
                        worksheet.Cells[$"R{filaInicio}"].Value = fila.FieldOrDefault<decimal>("PesoCaja");
                        worksheet.Cells[$"S{filaInicio}"].Value = fila.FieldOrDefault<string>("CantidadPaletas");
                        worksheet.Cells[$"T{filaInicio}"].Value = fila.FieldOrDefault<string>("CondicionPago");
                        worksheet.Cells[$"U{filaInicio}"].Value = fila.FieldOrDefault<decimal>("PesoNeto");
                        worksheet.Cells[$"V{filaInicio}"].Value = fila.FieldOrDefault<string>("ValorFob");
                        worksheet.Cells[$"W{filaInicio}"].Value = fila.FieldOrDefault<string>("CodIncoterm");
                        worksheet.Cells[$"X{filaInicio}"].Value = fila.FieldOrDefault<decimal>("CantidadTermoregistros");
                        worksheet.Cells[$"Y{filaInicio}"].Value = fila.FieldOrDefault<decimal>("CantidadFiltrosEtileno");
                        worksheet.Cells[$"Z{filaInicio}"].Value = fila.FieldOrDefault<string>("Observaciones");
                        worksheet.Cells[$"AA{filaInicio}"].Value = fila.FieldOrDefault<string>("OrigenProducto");
                        worksheet.Cells[$"AB{filaInicio}"].Value = fila.FieldOrDefault<DateTime>("CitaPacking");
                        worksheet.Cells[$"AC{filaInicio}"].Value = fila.FieldOrDefault<DateTime>("HoraInspeccionSenasa");
                        worksheet.Cells[$"AD{filaInicio}"].Value = fila.FieldOrDefault<string>("RazonSocialPacking");
                        worksheet.Cells[$"AE{filaInicio}"].Value = fila.FieldOrDefault<string>("RucPacking");
                        worksheet.Cells[$"AF{filaInicio}"].Value = fila.FieldOrDefault<string>("DireccionPacking");
                        worksheet.Cells[$"AG{filaInicio}"].Value = fila.FieldOrDefault<Boolean>("Drawback");
                        worksheet.Cells[$"AH{filaInicio}"].Value = fila.FieldOrDefault<string>("DepositoVacio");
                        worksheet.Cells[$"AI{filaInicio}"].Value = fila.FieldOrDefault<string>("OrdenServicio");
                        worksheet.Cells[$"AJ{filaInicio}"].Value = fila.FieldOrDefault<string>("UsuarioCreacion");
                        worksheet.Cells[$"AK{filaInicio}"].Value = fila.FieldOrDefault<int>("IdCotizacionOddo");
                        worksheet.Cells[$"AL{filaInicio}"].Value = fila.FieldOrDefault<string>("Placa");
                        worksheet.Cells[$"AM{filaInicio}"].Value = fila.FieldOrDefault<string>("Chofer");
                        worksheet.Cells[$"AN{filaInicio}"].Value = fila.FieldOrDefault<string>("CanalSini");
                        worksheet.Cells[$"AO{filaInicio}"].Value = fila.FieldOrDefault<string>("Transporte");
                        worksheet.Cells[$"AP{filaInicio}"].Value = fila.FieldOrDefault<string>("Nombre");
                        worksheet.Cells[$"AQ{filaInicio}"].Value = fila.FieldOrDefault<string>("TelefonoConsigne");
                        filaDt++;
                    }
                    resultado = package.GetAsByteArray();
                }

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }

            return resultado;
        }

        public static string QuitarComillasSimples(string cadena)
        {
            return cadena.Replace("'", @"\'").Replace(System.Environment.NewLine, "").Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
        }
    }
}
