﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace VELogisticCloud.CrossCutting.Comun
{
    public static class DataRowExtensions
    {
        public static T FieldOrDefault<T>(this DataRow row, string columnName)
        {
            return row.IsNull(columnName) ? default(T) : row.Field<T>(columnName);
        }
    }
}
