﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.CrossCutting.Comun
{
    public enum TipoCotizacion2
    {
        FleteMaritimo,
        OperacionLogistica
    }

    public enum TipoTemperatura
    {
        Celsius,
        fahrenheit
    }

    public enum TipoVentilacion
    {
        cubic_meters,
        other
    }

    public enum TipoHumedad
    {
        off,
        on
    }

    public enum TipoCondicion
    {
        fresh, 
        frozen
    }

    public enum IMO
    { 
        imo1=1,
        imo2=2,
        imo3=3,
        imo4=4,
        imo5=5,
        imo6=6,
        imo7=7,
        imo8=8,
        imo9=9
    }

    public enum ClaseContenedor
    { 
        REEFER,
        DRY
    }

    public enum TipoChildCliente
    {
        CONTACTO,
        DIRECCION_PACKING
    }

    public enum TipoServicioOPL
    {
        FILTRO,
        TERMO
    }

    public enum TipoEmail
    { 
        COTIZACION_MARITIMA_1_RECIBIDO,
        COTIZACION_MARITIMA_2_ATENDIDO,
        COTIZACION_MARITIMA_3_ACEPTADO,
        COTIZACION_LOGISTICA_1_RECIBIDO,
        COTIZACION_LOGISTICA_2_ATENDIDO,
        COTIZACION_LOGISTICA_3_ACEPTADO,
        BOOKING_1_RECIBIDO,
        BOOKING_2_ACEPTADO,
        BOOKING_3_GENERADO,
        BOOKING_4_INTTRA_VESSEL_LOAD,
        BOOKING_5_INTTRA_VESSEL_DEPARTED,
        BOOKING_6_INTTRA_VESSEL_ARRIVAL,
        BOOKING_7_INTTRA_VESSEL_DISCHARGE,
        BOOKING_8_INTTRA_GATED_OUR,
        BOOKING_9_INTTRA_RETURNED,
        BOOKING_10_INTTRA_LIQUIDADO,
        OPL_1_RECIBIDO,
        OPL_2_ACEPTADO,
        OPL_3_CANAL,
        OPL_4_AVANZZA_EN_CAMINO_DEPOSITO_VACIO,
        OPL_5_AVANZZA_LLEGO_DEPOSITO_VACIO,
        OPL_6_AVANZZA_CONTENEDOR_ASIGNADO,
        OPL_7_AVANZZA_EN_CAMINO_PACKING,
        OPL_8_AVANZZA_LLEGO_AL_PACKING,
        OPL_9_AVANZZA_INGRESO_LLENAR_CARGA,
        OPL_10_AVANZZA_SALIENDO_DEL_PACKING,
        OPL_11_AVANZZA_INSPECCION_SINI,
        OPL_12_AVANZZA_CAMINIO_AL_PUERTO,
        OPL_13_AVANZZA_INGRESANDO_AL_PUERTO,
        OPL_14_AVANZZA_CONTENEDOR_EN_PUERTO
    }

    public enum CacheTipo { 
        NAVIERA,
        CAMPANIA,
        COMMODITY,
        PUERTO,
        TIPO_CONTENEDOR,
        TERMINAL
    }
}
