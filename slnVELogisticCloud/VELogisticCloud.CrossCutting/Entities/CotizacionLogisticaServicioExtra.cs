﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VELogisticCloud.CrossCutting.Entities
{
    public class CotizacionLogisticaServicioExtra
    {
        [Key]
        public int IdServicioExtra { get; set; }
        public int? IdCotizacionLogisticaDetalle { get; set; }
        public int? IdServicio { get; set; }
        public string NombreServicio { get; set; }
        public string DescripcionServicio { get; set; }
        public int? IdEstado { get; set; }
        public int? Cantidad { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public CotizacionLogisticaDetalle CotizacionLogisticaDetalle { get; set; }
    }
}
