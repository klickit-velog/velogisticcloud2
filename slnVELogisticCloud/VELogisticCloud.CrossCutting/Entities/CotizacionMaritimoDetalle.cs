﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VELogisticCloud.CrossCutting.Entities
{
    public class CotizacionMaritimoDetalle
    {
        [Key]
        public int Id_detalle_cotizacion_maritimo { get; set; }
        public int? Id_Cotizacion_Maritimo { get; set; }
        public string Region { get; set; }
        public string Codigo_Linea_Naviera { get; set; }
        public string Linea_Naviera { get; set; }
        public string Codigo_Puerto_Embarque { get; set; }
        public string Puerto_Embarque { get; set; }
        public string Codigo_Puerto_Descarga { get; set; }
        public string Puerto_Descarga { get; set; }
        public string Codigo_Commodity { get; set; }
        public string Commodity { get; set; }
        public string Tamanio { get; set; }
        public string TT { get; set; }
        public DateTime? Valido_Desde { get; set; }
        public DateTime? Valido_Hasta { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Neto { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Rebate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Flete { get; set; }
        public DateTime? Fecha_Aprobacion { get; set; }
        public string Comentarios { get; set; }
        public string IdUsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string IdUsuarioModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? Estado { get; set; }
        public string NumeroCotizacion { get; set; }
        public string NumeroContrato { get; set; }
        public string Campania { get; set; }
        public string CodigoCampania { get; set; }

        public string Codigo_Pais_Embarque { get; set; }
        public string Codigo_Pais_Descarga { get; set; }
        public string Pais_Puerto_Descarga { get; set; }
        public string Pais_Puerto_Embarque { get; set; }
        public int? Id_Puerto_Carga { get; set; }
        public int? Id_Puerto_Descarga { get; set; }
        public int? Id_Naviera { get; set; }

    }
}
