﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VELogisticCloud.CrossCutting.Entities
{
    public class CotizacionLogistica
    {
        [Key]
        public int IdCotizacionLogistica { get; set; }
        public string Cliente { get; set; }
        public int? IdCommodity { get; set; }
        public string NombreCommodity { get; set; }
        public int? IdCampania { get; set; }
        public string NombreCampania { get; set; }
        public int? IdTipoContenedor { get; set; }
        public string NombreTipoContenedor { get; set; }
        public string Notas { get; set; }
        public int? IdCotizacionOdoo { get; set; }
        public string NumeroCotizacionOdoo { get; set; }
        public string CodigoContrato { get; set; }
        public int? IdEstado { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public string Codigo_Usuario { get; set; }
    }
}
