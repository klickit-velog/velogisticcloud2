﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VELogisticCloud.CrossCutting.Entities
{
    public class CotizacionMaritimo
    {
        [Key]
        public int Id_Cotizacion_Maritimo { get; set; }
        public int? Cantidad_Contenedor { get; set; }
        public string Codigo_Tipo_Contenedor { get; set; }
        public string Tipo_Contenedor { get; set; }
        public string Codigo_Cotizacion { get; set; }
        public string Cliente { get; set; }
        public DateTime? Fecha_Envio { get; set; }
        public DateTime? Fecha_Final { get; set; }
        public string Campania { get; set; }
        public string Nota { get; set; }
        public string Observacion { get; set; }
        public string Codigo_Usuario { get; set; }
        public string IdUsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string IdUsuarioModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? Estado { get; set; }
        public DateTime? FechaAprobacion { get; set; }
        public string CodigoContrato { get; set; }
        public string RutaArchivo { get; set; }
    }
}
