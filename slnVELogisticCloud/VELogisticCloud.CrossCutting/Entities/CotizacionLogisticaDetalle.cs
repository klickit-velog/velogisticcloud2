﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VELogisticCloud.CrossCutting.Entities
{
    public class CotizacionLogisticaDetalle
    {
        [Key]
        public int? IdCotizacionLogisticaDetalle { get; set; }
        public int IdCotizacionLogistica { get; set; }
        public int? IdNaviera { get; set; }
        public string NombreNaviera { get; set; }
        public int? IdPuertoCarga { get; set; }
        public string NombrePuertoCarga { get; set; }
        public string CodigoPaisCarga { get; set; }
        public string NombrePaisCarga { get; set; }
        public int? IdServicio { get; set; }
        public string NombreServicio { get; set; }
        public int? IdTerminalRetiroVacio { get; set; }
        public string NombreTerminalRetiroVacio { get; set; }
        public int? IdAlmacenPacking { get; set; }
        public string NombreAlmacenPacking { get; set; }
        public DateTime? FechaAprobacion { get; set; }
        public int? IdCotizacionDetalleOdoo { get; set; }
        public string CodigoContrato { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Flete { get; set; }
        public DateTime? FechaValidoDesde { get; set; }
        public DateTime? FechaValidoHasta { get; set; }
        public int? IdEstado { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public int IdTerminalDepositoVacio { get; set; }
        public string NombreTerminalDepositoVacio { get; set; }
        public ICollection<CotizacionLogisticaServicioExtra> ServiciosExtra { get; set; }
    }
}
