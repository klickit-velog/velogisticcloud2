﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using VELogisticCloud.Models.EntidadJSON;
using VELogisticCloud.Servicio.Business.Cotizacion;
using VELogisticCloud.Servicio.Data;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CotizacionDetalleController : Controller
    {
        private readonly VECloudDBContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private IConfiguration _configuration;

        BL_Cotizacion BL;


        public CotizacionDetalleController(VECloudDBContext context, IMapper mapper, ILogger<CotizacionDetalleController> logger, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _configuration = configuration;
            BL = new BL_Cotizacion(configuration);
        }
        
        //[HttpPost("InsertCotizacionDetail")]
        //public async Task<ActionResult> CreateDetalleCotizacion([FromBody] DetalleCotizacionAPI ent)
        //{
        //    var result = 0;
        //    try
        //    {
        //        ent.Estado = ConvertirEstado(ent.codigoEstado);
        //       var objeto =  BL.BL_InsertDetalleCotizacionAPI(ent);
        //        result = Convert.ToInt32(objeto);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "Error CotizacionDetalleController/CreateDetalleCotizacion");
        //    }
        //    return Ok(result);
        //}

        //private int ConvertirEstado(string estado)
        //{
        //    int respuesta = 0;
        //    switch (estado.ToUpper())
        //    {
        //        case "DRAFT": respuesta = 1; break;
        //        case "SENT": respuesta = 2; break;
        //        case "SALE": respuesta = 3; break;
        //        case "DONE": respuesta = 4; break;
        //        case "CANCEL": respuesta = 5; break;
        //        default: respuesta = 9; break;
        //    }

        //    return respuesta;
        //}


}
}