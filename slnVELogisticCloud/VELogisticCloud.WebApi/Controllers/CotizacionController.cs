﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.CrossCutting.DTO.WebApi;
using VELogisticCloud.CrossCutting.Mail;
using VELogisticCloud.Servicio.Business.Cotizacion;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Middleware;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CotizacionController : Controller
    {
        private readonly VECloudDBContext _context;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly INetMailSender netMailSender;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public CotizacionController(VECloudDBContext context, IMapper mapper, 
            ILogger<CotizacionController> logger, INetMailSender _netMailSender, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            netMailSender = _netMailSender;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        [HttpPut("CotizacionHeader")]
        public async Task<ActionResult> PutCotizacionHeader([FromBody] CotizacionHeaderDTO cotizacionHeaderDTO)
        {
            try
            {
                int id;
                if (string.IsNullOrEmpty(cotizacionHeaderDTO.NumeroReferenciaVE) || !int.TryParse(cotizacionHeaderDTO.NumeroReferenciaVE, out id))
                {
                    return BadRequest("El NumeroReferenciaVE no es un numero, es un formato incorrecto. ");
                }

                if (cotizacionHeaderDTO.TipoCotizacion == TipoCotizacion2.FleteMaritimo.ToString())
                {
                    var existe = await _context.CotizacionMaritimo.AnyAsync(a => a.Id_Cotizacion_Maritimo == id);

                    if (!existe)
                    {
                        return NotFound("No se encontró el registro con el NumeroReferenciaVE enviado.");
                    }

                    var entity = _context.CotizacionMaritimo.First(a => a.Id_Cotizacion_Maritimo == id);
                    entity.FechaAprobacion = cotizacionHeaderDTO.FechaAprobacion;
                    entity.Codigo_Cotizacion = cotizacionHeaderDTO.NumeroCotizacion;
                    entity.CodigoContrato = cotizacionHeaderDTO.CodigoContrato;
                    entity.Estado = ConvertirEstado(cotizacionHeaderDTO.CodigoEstado);
                    entity.RutaArchivo = cotizacionHeaderDTO.RutaArchivo;

                    await _context.SaveChangesAsync();

                    string correoCliente = _context.CotizacionMaritimo.FirstOrDefault(a => a.Id_Cotizacion_Maritimo == entity.Id_Cotizacion_Maritimo).Cliente;
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(correoCliente);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroCotizacion = entity.Id_Cotizacion_Maritimo.ToString()
                    };

                    await netMailSender.sendMailAsync(correoCliente, emailTemplate, TipoEmail.COTIZACION_MARITIMA_2_ATENDIDO);
                }
                else if (cotizacionHeaderDTO.TipoCotizacion == TipoCotizacion2.OperacionLogistica.ToString())
                {
                    var existe = await _context.CotizacionLogistica.AnyAsync(a => a.IdCotizacionLogistica == id);

                    if (!existe)
                    {
                        return NotFound("No se encontró el registro con el NumeroReferenciaVE enviado.");
                    }

                    var entity = _context.CotizacionLogistica.First(a => a.IdCotizacionLogistica == id);
                    //entity.FechaAprobacion = cotizacionHeaderDTO.FechaAprobacion;
                    entity.NumeroCotizacionOdoo = cotizacionHeaderDTO.NumeroCotizacion;
                    entity.CodigoContrato = cotizacionHeaderDTO.CodigoContrato;
                    entity.IdEstado = ConvertirEstado(cotizacionHeaderDTO.CodigoEstado);
                    //entity.RutaArchivo = cotizacionHeaderDTO.RutaArchivo;

                    await _context.SaveChangesAsync();

                    string correoCliente = _context.CotizacionLogistica.FirstOrDefault(a => a.IdCotizacionLogistica == entity.IdCotizacionLogistica).Cliente;
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(correoCliente);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroCotizacion = entity.IdCotizacionLogistica.ToString()
                    };

                    await netMailSender.sendMailAsync(correoCliente, emailTemplate, TipoEmail.COTIZACION_LOGISTICA_2_ATENDIDO);
                }
                else
                {
                    return BadRequest("El TipoCotizacion enviado es incorrecto.");
                }

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CotizacionController/PutCotizacionHeader");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("CotizacionDetail")]
        public async Task<ActionResult> PutCotizacionDetail([FromBody] CotizacionDetailDTO cotizacionDetailDTO)
        {
            try
            {
                int id;
                downloadJson(cotizacionDetailDTO, "PUT");

                if (string.IsNullOrEmpty(cotizacionDetailDTO.NumeroReferenciaVE) || !int.TryParse(cotizacionDetailDTO.NumeroReferenciaVE, out id))
                {
                    return BadRequest("El NumeroReferenciaVE no es un numero, es un formato incorrecto. ");
                }

                if (cotizacionDetailDTO.TipoCotizacion == TipoCotizacion2.FleteMaritimo.ToString())
                {
                    var existe = await _context.CotizacionMaritimoDetalle.AnyAsync(a => a.Id_detalle_cotizacion_maritimo == id);

                    if (!existe)
                    {
                        return NotFound("No se encontró el registro con el NumeroReferenciaVE enviado.");
                    }

                    var entity = _context.CotizacionMaritimoDetalle.FirstOrDefault(a => a.Id_detalle_cotizacion_maritimo == id);
                    entity.Fecha_Aprobacion = cotizacionDetailDTO.FechaAprobacion;
                    entity.NumeroCotizacion = cotizacionDetailDTO.NumeroCotizacion;
                    entity.NumeroContrato = cotizacionDetailDTO.CodigoContrato;
                    entity.Estado = ConvertirEstado(cotizacionDetailDTO.CodigoEstado);
                    entity.Region = cotizacionDetailDTO.Region;
                    entity.Codigo_Linea_Naviera = cotizacionDetailDTO.CodigoLinea;
                    entity.Linea_Naviera = cotizacionDetailDTO.NombreLinea;
                    entity.Codigo_Puerto_Embarque = cotizacionDetailDTO.CodigoPOL;
                    entity.Puerto_Embarque = cotizacionDetailDTO.POL;
                    entity.Codigo_Puerto_Descarga = cotizacionDetailDTO.CodigoPOD;
                    entity.Puerto_Descarga = cotizacionDetailDTO.POD;
                    entity.Codigo_Commodity = cotizacionDetailDTO.CodigoCommodity;
                    entity.Commodity = cotizacionDetailDTO.NombreCommodity;
                    entity.Campania = cotizacionDetailDTO.NombreCampania;
                    entity.CodigoCampania = cotizacionDetailDTO.CodigoCampania;
                    entity.Neto = cotizacionDetailDTO.Neto;
                    entity.Rebate = cotizacionDetailDTO.Rebate;
                    entity.Flete = cotizacionDetailDTO.Flete;
                    entity.TT = cotizacionDetailDTO.TT; //TODO: Corregir tipo de dato.
                    entity.Valido_Desde = cotizacionDetailDTO.ValidoDesde;
                    entity.Valido_Hasta = cotizacionDetailDTO.ValidoHasta;


                    await _context.SaveChangesAsync();
                }
                else if (cotizacionDetailDTO.TipoCotizacion == TipoCotizacion2.OperacionLogistica.ToString())
                {
                    var existe = await _context.CotizacionLogisticaDetalle.AnyAsync(a => a.IdCotizacionLogisticaDetalle == id);

                    if (!existe)
                    {
                        return NotFound("No se encontró el registro con el NumeroReferenciaVE enviado.");
                    }

                    var entity = _context.CotizacionLogisticaDetalle.First(a => a.IdCotizacionLogisticaDetalle == id);
                    entity.FechaAprobacion = cotizacionDetailDTO.FechaAprobacion;
                    entity.IdCotizacionDetalleOdoo = Convert.ToInt32(cotizacionDetailDTO.NumeroCotizacion.Trim());
                    entity.CodigoContrato = cotizacionDetailDTO.CodigoContrato;
                    entity.IdEstado = ConvertirEstado(cotizacionDetailDTO.CodigoEstado);
                    //entity.Region = cotizacionDetailDTO.Region;
                    //entity.Codigo_Linea_Naviera = cotizacionDetailDTO.CodigoLinea;
                    //entity.Linea_Naviera = cotizacionDetailDTO.NombreLinea;
                    //entity.Codigo_Puerto_Embarque = cotizacionDetailDTO.CodigoPOL;
                    //entity.Puerto_Embarque = cotizacionDetailDTO.POL;
                    //entity.Codigo_Puerto_Descarga = cotizacionDetailDTO.CodigoPOD;
                    //entity.Puerto_Descarga = cotizacionDetailDTO.POD;
                    //entity.Codigo_Commodity = cotizacionDetailDTO.CodigoCommodity;
                    //entity.Commodity = cotizacionDetailDTO.NombreCommodity;
                    //entity.Campania = cotizacionDetailDTO.NombreCampania;
                    //entity.CodigoCampania = cotizacionDetailDTO.CodigoCampania;
                    //entity.Neto = cotizacionDetailDTO.Neto;
                    //entity.Rebate = cotizacionDetailDTO.Rebate;
                    entity.Flete = cotizacionDetailDTO.Flete;
                    //entity.TT = cotizacionDetailDTO.TT.ToString(); //TODO: Corregir tipo de dato.
                    entity.FechaValidoDesde = cotizacionDetailDTO.ValidoDesde;
                    entity.FechaValidoHasta = cotizacionDetailDTO.ValidoHasta;

                    await _context.SaveChangesAsync();

                }
                else
                {
                    return BadRequest("El TipoCotizacion enviado es incorrecto.");
                }
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CotizacionController/PutCotizacionDetail");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }

        [HttpPost("CotizacionDetail")]
        public async Task<ActionResult> PostCotizacionDetail([FromBody] CotizacionDetailDTO cotizacionDetailDTO)
        {
            try
            {
                int id;

                downloadJson(cotizacionDetailDTO, "POST");

                if (cotizacionDetailDTO.TipoCotizacion == TipoCotizacion2.FleteMaritimo.ToString())
                {
                    var existe = await _context.CotizacionMaritimo.AnyAsync(a => a.Id_Cotizacion_Maritimo == cotizacionDetailDTO.IdCotizacion);

                    if (!existe)
                    {
                        return NotFound("No se encontró el registro con el IdCotizacion enviado.");
                    }

                    var entity = new CrossCutting.Entities.CotizacionMaritimoDetalle();
                    entity.Fecha_Aprobacion = cotizacionDetailDTO.FechaAprobacion;
                    entity.NumeroCotizacion = cotizacionDetailDTO.NumeroCotizacion;
                    entity.NumeroContrato = cotizacionDetailDTO.CodigoContrato;
                    entity.Estado = ConvertirEstado(cotizacionDetailDTO.CodigoEstado);
                    entity.Region = cotizacionDetailDTO.Region;
                    entity.Codigo_Linea_Naviera = cotizacionDetailDTO.CodigoLinea;
                    entity.Linea_Naviera = cotizacionDetailDTO.NombreLinea;
                    entity.Codigo_Puerto_Embarque = cotizacionDetailDTO.CodigoPOL;
                    entity.Puerto_Embarque = cotizacionDetailDTO.POL;
                    entity.Codigo_Puerto_Descarga = cotizacionDetailDTO.CodigoPOD;
                    entity.Puerto_Descarga = cotizacionDetailDTO.POD;
                    entity.Codigo_Commodity = cotizacionDetailDTO.CodigoCommodity;
                    entity.Commodity = cotizacionDetailDTO.NombreCommodity;
                    entity.Campania = cotizacionDetailDTO.NombreCampania;
                    entity.CodigoCampania = cotizacionDetailDTO.CodigoCampania;
                    entity.Neto = cotizacionDetailDTO.Neto;
                    entity.Rebate = cotizacionDetailDTO.Rebate;
                    entity.Flete = cotizacionDetailDTO.Flete;
                    entity.TT = cotizacionDetailDTO.TT;
                    entity.Valido_Desde = cotizacionDetailDTO.ValidoDesde;
                    entity.Valido_Hasta = cotizacionDetailDTO.ValidoHasta;

                    entity.FechaCreacion = DateTime.Now;
                    entity.Id_Cotizacion_Maritimo = cotizacionDetailDTO.IdCotizacion;
                    entity.Tamanio = cotizacionDetailDTO.Tamanio;
                    entity.Id_Naviera = cotizacionDetailDTO.IdLinea;
                    entity.Id_Puerto_Carga = cotizacionDetailDTO.IdPOL;
                    entity.Codigo_Pais_Embarque = cotizacionDetailDTO.CodigoPaisPOL;
                    entity.Pais_Puerto_Embarque = cotizacionDetailDTO.NombrePaisPOL;
                    entity.Id_Puerto_Descarga = cotizacionDetailDTO.IdPOD;
                    entity.Codigo_Pais_Descarga = cotizacionDetailDTO.CodigoPaisPOD;
                    entity.Pais_Puerto_Descarga = cotizacionDetailDTO.NombrePaisPOD;

                    _context.CotizacionMaritimoDetalle.Add(entity);
                    await _context.SaveChangesAsync();

                    id = entity.Id_detalle_cotizacion_maritimo;
                }
                else if (cotizacionDetailDTO.TipoCotizacion == TipoCotizacion2.OperacionLogistica.ToString())
                {
                    var existe = await _context.CotizacionLogistica.AnyAsync(a => a.IdCotizacionLogistica == cotizacionDetailDTO.IdCotizacion);

                    if (!existe)
                    {
                        return NotFound("No se encontró el registro con el IdCotizacion enviado.");
                    }

                    var entity = new CrossCutting.Entities.CotizacionLogisticaDetalle();
                    entity.FechaAprobacion = cotizacionDetailDTO.FechaAprobacion;
                    entity.IdCotizacionDetalleOdoo = Convert.ToInt32(cotizacionDetailDTO.NumeroCotizacion.Trim());
                    entity.CodigoContrato = cotizacionDetailDTO.CodigoContrato;
                    entity.IdEstado = ConvertirEstado(cotizacionDetailDTO.CodigoEstado);
                    //entity.Region = cotizacionDetailDTO.Region;
                    //entity.Codigo_Linea_Naviera = cotizacionDetailDTO.CodigoLinea;
                    //entity.Linea_Naviera = cotizacionDetailDTO.NombreLinea;
                    //entity.Codigo_Puerto_Embarque = cotizacionDetailDTO.CodigoPOL;
                    //entity.Puerto_Embarque = cotizacionDetailDTO.POL;
                    //entity.Codigo_Puerto_Descarga = cotizacionDetailDTO.CodigoPOD;
                    //entity.Puerto_Descarga = cotizacionDetailDTO.POD;
                    //entity.Codigo_Commodity = cotizacionDetailDTO.CodigoCommodity;
                    //entity.Commodity = cotizacionDetailDTO.NombreCommodity;
                    //entity.Campania = cotizacionDetailDTO.NombreCampania;
                    //entity.CodigoCampania = cotizacionDetailDTO.CodigoCampania;
                    //entity.Neto = cotizacionDetailDTO.Neto;
                    //entity.Rebate = cotizacionDetailDTO.Rebate;
                    entity.Flete = cotizacionDetailDTO.Flete;
                    //entity.TT = cotizacionDetailDTO.TT.ToString(); //TODO: Corregir tipo de dato.
                    entity.FechaValidoDesde = cotizacionDetailDTO.ValidoDesde;
                    entity.FechaValidoHasta = cotizacionDetailDTO.ValidoHasta;
                    entity.IdCotizacionLogistica = cotizacionDetailDTO.IdCotizacion;


                    _context.CotizacionLogisticaDetalle.Add(entity);
                    await _context.SaveChangesAsync();

                    id = entity.IdCotizacionLogisticaDetalle.Value;
                }
                else
                {
                    return BadRequest("El TipoCotizacion enviado es incorrecto.");
                }
                return Ok(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CotizacionController/PostCotizacionDetail");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            

        }

        private void downloadJson(CotizacionDetailDTO cotizacionDetailDTO, string metodo)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;

            var json = JsonConvert.SerializeObject(cotizacionDetailDTO, settings);

            string pathRoot = string.Concat(Directory.GetCurrentDirectory(), "/TramasInput/");
            string fileName = string.Concat(cotizacionDetailDTO.NumeroCotizacion,"_",metodo,DateTime.Now.ToString("_ddMMyyyy_hhmmss_"),Guid.NewGuid().ToString(),".txt");

            if (!Directory.Exists(pathRoot)) Directory.CreateDirectory(pathRoot);

            using (StreamWriter sw = new StreamWriter(string.Concat(pathRoot,fileName)))
            {
                sw.WriteLine(json);
            }
        }


        private int ConvertirEstado(string estado)
        {
            int respuesta = 0;
            switch (estado.ToUpper())
            {
                case "DRAFT": respuesta = 1; break;
                case "SENT": respuesta = 2; break;
                case "SALE": respuesta = 3; break;
                case "DONE": respuesta = 4; break;
                case "CANCEL": respuesta = 5; break;
                default: respuesta = 9; break;
            }

            return respuesta;
        }

        [HttpGet("GetCotizacionHeader/{numeroReferenciaVE}")]
        public async Task<ActionResult<CotizacionHeaderDTO>> GetCotizacionHeader(string numeroReferenciaVE)
        {
            //int id = Convert.ToInt32(numeroReferenciaVE.Substring(1, numeroReferenciaVE.Length - 1));
            int id = Convert.ToInt32(numeroReferenciaVE);
            //if (!int.TryParse(numeroReferenciaVE.Substring(1, numeroReferenciaVE.Length - 1), out id))
            //{
            //    return NotFound();
            //}

            var entity = await _context.CotizacionMaritimo.FirstOrDefaultAsync(a => a.Id_Cotizacion_Maritimo == id);

            if (entity == null)
            {
                return NotFound();
            }

            //var dto = _mapper.Map<CotizacionHeaderDTO>(entity);
            var dto = new CotizacionHeaderDTO();
            dto.FechaAprobacion = entity.FechaAprobacion;
            dto.NumeroCotizacion = entity.Codigo_Cotizacion;
            dto.CodigoContrato = entity.CodigoContrato;
            dto.NumeroReferenciaVE = entity.Id_Cotizacion_Maritimo.ToString();
            dto.CodigoEstado = entity.Estado.ToString();
            dto.RutaArchivo = entity.RutaArchivo;

            return dto;
        }

        [HttpGet("GetCotizacionDetail/{numeroReferenciaVE}")]
        public async Task<ActionResult<CotizacionDetailDTO>> GetCotizacionDetail(string numeroReferenciaVE)
        {
            //int id = Convert.ToInt32(numeroReferenciaVE.Substring(1, numeroReferenciaVE.Length - 1));
            int id = Convert.ToInt32(numeroReferenciaVE);
            //if (!int.TryParse(numeroReferenciaVE.Substring(1, numeroReferenciaVE.Length - 1), out id))
            //{
            //    return NotFound();
            //}

            var entity = await _context.CotizacionMaritimoDetalle.FirstOrDefaultAsync(a => a.Id_detalle_cotizacion_maritimo == id);

            if (entity == null)
            {
                return NotFound();
            }

            //var dto = _mapper.Map<CotizacionHeaderDTO>(entity);
            CotizacionDetailDTO cotizacionDetailDTO = new CotizacionDetailDTO();

            cotizacionDetailDTO.FechaAprobacion = entity.Fecha_Aprobacion;
            cotizacionDetailDTO.NumeroCotizacion = entity.NumeroCotizacion;
            cotizacionDetailDTO.CodigoContrato = entity.NumeroContrato;
            cotizacionDetailDTO.CodigoEstado = entity.Estado.ToString();
            cotizacionDetailDTO.NumeroReferenciaVE = entity.Id_detalle_cotizacion_maritimo.ToString();
            cotizacionDetailDTO.Region = entity.Region;
            cotizacionDetailDTO.CodigoLinea = entity.Codigo_Linea_Naviera;
            cotizacionDetailDTO.NombreLinea = entity.Linea_Naviera;
            cotizacionDetailDTO.CodigoPOL = entity.Codigo_Puerto_Embarque;
            cotizacionDetailDTO.POL = entity.Puerto_Embarque;
            cotizacionDetailDTO.CodigoPOD = entity.Codigo_Puerto_Descarga;
            cotizacionDetailDTO.POD = entity.Puerto_Descarga;
            cotizacionDetailDTO.CodigoCommodity = entity.Codigo_Commodity;
            cotizacionDetailDTO.NombreCommodity = entity.Commodity;
            cotizacionDetailDTO.NombreCampania = entity.Campania;
            cotizacionDetailDTO.CodigoCampania = entity.CodigoCampania;
            cotizacionDetailDTO.Neto = entity.Neto.HasValue ? entity.Neto.Value : 0;
            cotizacionDetailDTO.Rebate = entity.Rebate.HasValue ? entity.Rebate.Value : 0;
            cotizacionDetailDTO.Flete = entity.Flete.HasValue ? entity.Flete.Value : 0;
            cotizacionDetailDTO.TT = entity.TT;
            cotizacionDetailDTO.ValidoDesde = entity.Valido_Desde;
            cotizacionDetailDTO.ValidoHasta = entity.Valido_Hasta;


            return cotizacionDetailDTO;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
