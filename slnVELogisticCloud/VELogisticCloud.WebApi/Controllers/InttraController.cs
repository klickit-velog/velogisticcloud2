﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Models.General;
using VELogisticCloud.Servicio.Business.General;
using VELogisticCloud.Servicio.Business.Inttra;
using VELogisticCloud.Servicio.Data;

namespace VELogisticCloud.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InttraController : Controller
    {
       
        private IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly VECloudDBContext _context;
        private readonly IMapper _mapper;
        private BL_Variable bl_Variable;
        public InttraController(VECloudDBContext context, IMapper mapper, ILogger<InttraController> logger, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _configuration = configuration;
        }
        //BL_Inttra BL = new BL_Inttra(_configuration);
        [HttpPost("OnInttra")]
        public async Task<ActionResult> OnInttra()
        {
            try
            {
                BL_Inttra BL = new BL_Inttra(_configuration);
                string CodigoVariable = "F0001";
                bl_Variable = new BL_Variable(_configuration);
                var datoVariable = bl_Variable.BL_ListaVariables(CodigoVariable);
                var Variable = (List<BE_Variables>)datoVariable;
                if (Variable[0].Activado == 1)
                {
                    ////  Lista de inttra
                    BL.listFiles();
                    //var inttra = BL.envioXML(id, user);
                }
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error InttraController/OnInttra");
            }
            return Ok();
        }
    }
}