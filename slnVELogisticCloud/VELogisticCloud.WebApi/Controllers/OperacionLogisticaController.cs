﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.CrossCutting.Mail;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Models.EntidadJSON;
using VELogisticCloud.Models.Seguimiento;
using VELogisticCloud.Servicio.Business.Booking;
using VELogisticCloud.Servicio.Business.OperacionLogistica;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperacionLogisticaController : Controller
    {

        private readonly VECloudDBContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly INetMailSender netMailSender;
        BL_OperacionLogistica2 BL;
        private readonly IMemoryCache _memoryCache;

        public OperacionLogisticaController(VECloudDBContext context, IMapper mapper, ILogger<OperacionLogisticaController> logger, 
            IConfiguration configuration, INetMailSender _netMailSender, IMemoryCache memoryCache)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _configuration = configuration;
            netMailSender = _netMailSender;
            BL = new BL_OperacionLogistica2(_configuration);
            _memoryCache = memoryCache;
        }


        [HttpPost("UpdateStatus")]
        public async Task<ActionResult> PutStatusOPL([FromBody]  responseOrderStatus json)
        {
            int id = json.payload.id;
            string codEstado = json.payload.status_code;
            string descEstado = json.payload.status;
            string code = json.payload.trip_interval.orders[0].code;
            string status_id = json.payload.status_id;
            string fechaIni = DateTime.Parse(json.payload.datetime).ToString("yyyy-MM-dd HH:mm:ss");
            
            try
            {
                var res1 = BL.DatosParaCorreoOPL(code);
                var Datos1 = (List<DatosCorreoOPL>)res1;
                var Datos = Datos1[0];
                new BL_OperacionLogistica2(_configuration).BL_ApiUpdateOPL(id, codEstado, descEstado, fechaIni, code, status_id);
                if(Datos.OrdenServicio != null)
                {
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(Datos.Codigo_Usuario);
                    
                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        OrdenServicio = Datos.OrdenServicio,
                        Canal = Datos.CanalSini,
                        FechaHora = fechaIni
                    };

                    await EnvioCorreo(Datos.Codigo_Usuario, codEstado, emailTemplate); 
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "OperacionLogisticaController/PutStatusOPL");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return Ok();
        }

        public async Task EnvioCorreo(string usuario,string code,EmailTemplateDTO emailTemplate)
        {
            switch (code)
            {
                case "on-my-way":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_4_AVANZZA_EN_CAMINO_DEPOSITO_VACIO);
                    break;
                case "LLEGADA_DV":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_5_AVANZZA_LLEGO_DEPOSITO_VACIO);
                    break;
                case "IN_DV":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_6_AVANZZA_CONTENEDOR_ASIGNADO);
                    break;
                case "Sali":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_7_AVANZZA_EN_CAMINO_PACKING);
                    break;
                case "LLEGADA_P":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_8_AVANZZA_LLEGO_AL_PACKING);
                    break;
                case "IN_P":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_9_AVANZZA_INGRESO_LLENAR_CARGA);
                    break;
                case "OUT_P":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_10_AVANZZA_SALIENDO_DEL_PACKING);
                    break;
                case "SINI":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_11_AVANZZA_INSPECCION_SINI);
                    break;
                case "LLEGADA_PO":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_12_AVANZZA_CAMINIO_AL_PUERTO);
                    break;

                case "IN_POL":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_13_AVANZZA_INGRESANDO_AL_PUERTO);
                    break;

                case "delivered":
                    await netMailSender.sendMailAsync(usuario, emailTemplate, TipoEmail.OPL_14_AVANZZA_CONTENEDOR_EN_PUERTO);
                    break;
            }
        }
    }
}