﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using VELogisticCloud.CrossCutting.DTO.WebApi;

namespace VELogisticCloud.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        [HttpPost]
        public ActionResult<UserToken> Post([FromBody] UserInfo userInfo)
        {
            if (userInfo.Id == "odoo@vecloud.com" && userInfo.Credential == @"*g_h7{-=Bxd<SG]<D5:{}mty[5m7M")
            {
                return BuildToken(userInfo.Id, userInfo.Credential);
            }
            else
            {
                return BadRequest("User or password invalid.");
            }
        }

        private UserToken BuildToken(string user, string password)
        {
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.UniqueName, user),
                new Claim("algunvalor","cualquiervalor"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("8edb8414-b85c-4cfd-8ee5-ca0f07b6cd88"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //Tiempo de expiracion del token
            var expiration = DateTime.UtcNow.AddHours(24);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: expiration,
                signingCredentials: creds
                );

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
    }
}
