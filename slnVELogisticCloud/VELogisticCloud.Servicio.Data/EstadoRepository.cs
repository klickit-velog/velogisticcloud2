﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Data
{
    public class EstadoRepository
    {
        private readonly VECloudDBContext _db;
        //public EstadoRepository(VECloudDBContext context)
        //{
        //    _db = context;
        //}
        public IEnumerable<SelectListItem> GetListaEstado()
        {
            var listaEstado = new List<SelectListItem>()
            {
                new SelectListItem(){
                    Text = "Seleccione",
                    Value = "-1"
                },
                new SelectListItem(){ 
                    Text = "Activo",
                    Value = "1"
                },
                new SelectListItem(){
                    Text = "Inactivo",
                    Value = "0"
                }
            };

            return listaEstado;
        }

    }
}
