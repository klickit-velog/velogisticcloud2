﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.Servicio.Data
{
    public class ContenedorNaviera : IContenedorNaviera
    {
        private readonly VECloudDBContext _context;
        public INavieraRepository Naviera { get; private set; }
        public ContenedorNaviera(VECloudDBContext context)
        {
            _context = context;
            Naviera = new NavieraRepository(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
