﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.Servicio.Data
{
    public class PerfilRepository : Repository<Perfil>, IPerfilRepository
    {
        private readonly VECloudDBContext _db;

        public PerfilRepository(VECloudDBContext context) : base(context)
        {
            _db = context;
        }

        public IEnumerable<SelectListItem> GetListaPerfil()
        {
            return _db.Perfil.Select(a => new SelectListItem()
            {
                Text = a.Nombre,
                Value = a.IdPerfil.ToString()
            });
        }

        public void Update(Perfil entidad)
        {
            var objDesdeDb = _db.Perfil.FirstOrDefault(a => a.IdPerfil == entidad.IdPerfil);
            objDesdeDb.Nombre = entidad.Nombre;
            objDesdeDb.Descripcion = entidad.Descripcion;
            objDesdeDb.IdEstado = entidad.IdEstado;

            _db.SaveChanges();
        }
    }
}
