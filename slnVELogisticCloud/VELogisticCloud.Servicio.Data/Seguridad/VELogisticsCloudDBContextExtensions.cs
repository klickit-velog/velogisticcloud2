﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Entidad.Seguridad;

namespace VELogisticCloud.Servicio.Data.Seguridad
{
    public static class VELogisticsCloudDBContextExtensions
    {

        public static async Task<List<Estado>> EstadosListar(this VELogisticsCloudDBContext dbContext, int idEstadoPadre)
        {

            var result = from estado in dbContext.Estados.AsNoTracking()
                         where estado.IdEstadoPadre == idEstadoPadre &&
                               estado.Activo == true
                         orderby estado.Nombre
                         select new Estado { 
                                IdEstado = estado.IdEstado,
                                Nombre = estado.Nombre
                            };

            return await result.ToListAsync<Estado>();
        }

        public static async Task<EntidadConsultada<UsuarioDTO>> UsuariosConsultar(this VELogisticsCloudDBContext dbContext, UsuarioDTO usuario, int skip, int pageSize)
        {
            EntidadConsultada<UsuarioDTO> response = new EntidadConsultada<UsuarioDTO>();
            try
            {
                var result = from usuarios in dbContext.Usuarios.AsNoTracking()
                             join estados in dbContext.Estados.AsNoTracking() on
                                new
                                {
                                    Key1 = usuarios.IdEstado
                                } equals
                                new
                                {
                                    Key1 = estados.IdEstado
                                }
                             orderby usuarios.Login
                             select new UsuarioDTO
                             {
                                 IdUsuario = usuarios.IdUsuario,
                                 UserName = usuarios.UserName,
                                 Login = usuarios.Login,
                                 Email = usuarios.Email,
                                 //PasswordHash = usuarios.PasswordHash
                                 //SecurityStamp = usuarios
                                 //ConcurrencyStamp = usuarios
                                 //Discriminator = usuarios
                                 Nombres = usuarios.Nombres,
                                 Apellidos = usuarios.Apellidos,
                                 TelefonoMovil = usuarios.TelefonoMovil,
                                 IdEstado = usuarios.IdEstado,
                                 NombreEstado = estados.Nombre,
                                 FechaRegistro = usuarios.FechaRegistro,
                                 IdUsuarioActualiza = usuarios.IdUsuarioActualiza,
                                 FechaActualizacion = usuarios.FechaActualizacion
                             };

                if (!string.IsNullOrWhiteSpace(usuario.Login))
                    result = result.Where(p => EF.Functions.Like(p.Login.Trim(), "%" + usuario.Login + "%"));

                if (!string.IsNullOrWhiteSpace(usuario.Nombres))
                    result = result.Where(p => EF.Functions.Like(p.Nombres.Trim(), "%" + usuario.Nombres + "%"));

                if (!string.IsNullOrWhiteSpace(usuario.Apellidos))
                    result = result.Where(p => EF.Functions.Like(p.Apellidos.Trim(), "%" + usuario.Apellidos + "%"));

                if (usuario.IdEstado > 0)
                    result = result.Where(p => p.IdEstado == usuario.IdEstado);

                response.TotalRegistros = result.Count();
                response.Lista = await result.Skip(skip).Take(pageSize).ToListAsync<UsuarioDTO>();
            }
            catch (Exception ex)
            {
                throw ex;               
            }
            
            return response;
        }
    }
}
