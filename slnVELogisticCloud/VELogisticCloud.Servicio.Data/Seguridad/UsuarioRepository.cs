﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.DTO;
using System.Linq;

namespace VELogisticCloud.Servicio.Data.Seguridad
{
    public class UsuarioRepository
    {
        private VECloudDBContext dbContext;

        public UsuarioRepository(VECloudDBContext context)
        {
            dbContext = context;
        }

        public async Task<EntidadConsultada<UsuarioDTO>> ListarUsuarios(UsuarioDTO pUsuario, int skip, int pageSize)
        {
            EntidadConsultada<UsuarioDTO> response = new EntidadConsultada<UsuarioDTO>();
            try
            {
                var result = from usuario in dbContext.ApplicationUser.AsNoTracking()
                             orderby usuario.Email
                             select new UsuarioDTO
                             {
                                 IdUsuario = usuario.Id,
                                 Email = usuario.Email,
                                 Nombre = usuario.Nombre,
                                 RazonSocial = usuario.RazonSocial,
                                 RUC = usuario .RUC,
                                 NombreContacto = usuario.NombreContacto,
                                 NumeroContacto = usuario.NumeroContacto,
                                 IdEstado = usuario.IdEstado,
                                 //TODO: AGREGAR EL NOMBRE DE ESTADO.
                                 FechaRegistro = usuario.FechaRegistro,
                                 IdUsuarioActualiza = usuario.IdUsuarioActualiza,
                                 FechaActualizacion = usuario.FechaActualizacion
                             };

                if (!string.IsNullOrWhiteSpace(pUsuario.Nombre))
                    result = result.Where(p => EF.Functions.Like(p.Nombre.Trim(), "%" + pUsuario.Nombre + "%"));

                if (!string.IsNullOrWhiteSpace(pUsuario.RazonSocial))
                    result = result.Where(p => EF.Functions.Like(p.RazonSocial.Trim(), "%" + pUsuario.RazonSocial+ "%"));

                if (!string.IsNullOrWhiteSpace(pUsuario.Email))
                    result = result.Where(p => EF.Functions.Like(p.Email.Trim(), "%" + pUsuario.Email + "%"));

                if (pUsuario.IdEstado > 0)
                    result = result.Where(p => p.IdEstado == pUsuario.IdEstado);              

                response.TotalRegistros = result.Count();
                //response.Lista = await result.Skip(skip).Take(pageSize).ToListAsync<UsuarioDTO>();

                var listaUsuarios = await result.Skip(skip).Take(pageSize).ToListAsync();

                var usuariosRol = await dbContext.UserRoles.Where(a => listaUsuarios.Select(u => u.IdUsuario).Contains(a.UserId)).ToListAsync();
                var roles = await dbContext.Roles.ToListAsync();

                foreach (var usuario in listaUsuarios)
                {
                    var idRol = usuariosRol.Where(a => a.UserId == usuario.IdUsuario).First().RoleId;
                    var rol = roles.Where(a => a.Id == idRol).First();
                    usuario.NombreRol = rol.Name;
                }

                response.Lista = listaUsuarios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

    }
}
