﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VELogisticCloud.CrossCutting.Entities;
using VELogisticCloud.Model.Identity;
//using VELogisticCloud.CrossCutting.Entidad.Seguridad;
using VELogisticCloud.Models;
//using VELogisticCloud.Servicio.Data.Configuracion.Seguridad;

namespace VELogisticCloud.Servicio.Data
{
    public class VECloudDBContext : IdentityDbContext<ApplicationUser>
    {
        public VECloudDBContext(DbContextOptions<VECloudDBContext> options) : base(options)
        {

        }

        //public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Puerto> Puerto { get; set; }
        public DbSet<Models.Perfil> Perfil { get; set; }
        public DbSet<Naviera> Naviera { get; set; }
        public DbSet<ItinerarioSolicitud> ItinerarioSolicitud { get; set; }
        public DbSet<ApplicationUser> ApplicationUser { get; set; }
        //public DbSet<Estado> Estados { get; set; }
        public DbSet<CotizacionMaritimo> CotizacionMaritimo { get; set; }

        public DbSet<CotizacionMaritimoDetalle> CotizacionMaritimoDetalle { get; set; }
        public DbSet<CotizacionLogistica> CotizacionLogistica { get; set; }
        public DbSet<CotizacionLogisticaDetalle> CotizacionLogisticaDetalle { get; set; }
        public DbSet<CotizacionLogisticaServicioExtra> CotizacionLogisticaServicioExtra { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //modelBuilder
            //    .ApplyConfiguration(new UsuarioConfiguracion())
            //    .ApplyConfiguration(new EstadoConfiguracion());
            modelBuilder.Entity<CotizacionLogisticaDetalle>()
                .HasMany(s => s.ServiciosExtra)
                .WithOne(e => e.CotizacionLogisticaDetalle);


            base.OnModelCreating(modelBuilder);
        }
    }
}
