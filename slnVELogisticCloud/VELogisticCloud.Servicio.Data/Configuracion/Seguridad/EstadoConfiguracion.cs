﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VELogisticCloud.CrossCutting.Entidad.Seguridad;

namespace VELogisticCloud.Servicio.Data.Configuracion.Seguridad
{
    public class EstadoConfiguracion : IEntityTypeConfiguration<Estado>
    {
        public void Configure(EntityTypeBuilder<Estado> builder)
        {
            // Mapping for table
            builder.ToTable("SEG_Estado", "dbo");

            // Set key for entity
            builder.HasKey(p => p.IdEstado);

            // Set identity for entity (auto increment)
            builder.Property(p => p.IdEstado).UseIdentityColumn();

            // Set mapping for columns
            builder.Property(p => p.IdEstado).HasColumnType("int").IsRequired();
            builder.Property(p => p.IdEstadoPadre).HasColumnType("int");
            builder.Property(p => p.Nombre).HasColumnType("varchar(100)").IsRequired();
            builder.Property(p => p.Activo).HasColumnType("bit").IsRequired();
            builder.Property(p => p.IdUsuarioRegistro).HasColumnType("int");
            builder.Property(p => p.FechaRegistro).HasColumnType("datetime");
            builder.Property(p => p.IdUsuarioActualiza).HasColumnType("int");
            builder.Property(p => p.FechaActualizacion).HasColumnType("datetime");
        }
    }
}
