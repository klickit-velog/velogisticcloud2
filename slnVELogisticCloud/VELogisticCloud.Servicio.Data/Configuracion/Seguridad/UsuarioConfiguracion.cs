﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VELogisticCloud.CrossCutting.Entidad.Seguridad;

namespace VELogisticCloud.Servicio.Data.Configuracion.Seguridad
{
    public class UsuarioConfiguracion : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            // Mapping for table
            builder.ToTable("SEG_Usuario", "dbo");

            // Set key for entity
            builder.HasKey(p => p.IdUsuario);

            // Set identity for entity (auto increment)
            builder.Property(p => p.IdUsuario).UseIdentityColumn();

            // Set mapping for columns
            builder.Property(p => p.IdUsuario).HasColumnType("int").IsRequired();
            builder.Property(p => p.UserName).HasColumnType("varchar(100)");
            builder.Property(p => p.Login).HasColumnType("varchar(100)").IsRequired();
            builder.Property(p => p.Email).HasColumnType("varchar(100)").IsRequired();
            builder.Property(p => p.PasswordHash).HasColumnType("nvarchar(max)");
            builder.Property(p => p.SecurityStamp).HasColumnType("nvarchar(max)");
            builder.Property(p => p.ConcurrencyStamp).HasColumnType("nvarchar(max)");
            builder.Property(p => p.Discriminator).HasColumnType("nvarchar(max)").IsRequired();
            builder.Property(p => p.Nombres).HasColumnType("varchar(100)");
            builder.Property(p => p.Apellidos).HasColumnType("varchar(100)");
            builder.Property(p => p.TelefonoMovil).HasColumnType("varchar(20)");
            builder.Property(p => p.IdEstado).HasColumnType("int");
            builder.Property(p => p.FechaRegistro).HasColumnType("datetime");
            builder.Property(p => p.IdUsuarioActualiza).HasColumnType("int");
            builder.Property(p => p.FechaActualizacion).HasColumnType("datetime");
        }
    }
}
