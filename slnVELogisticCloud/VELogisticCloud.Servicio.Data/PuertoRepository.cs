﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.Servicio.Data
{
    public class PuertoRepository : Repository<Puerto>, IPuertoRepository
    {
        private readonly VECloudDBContext _db;
        public PuertoRepository(VECloudDBContext db):base(db)
        {
            _db = db;
        }
        public IEnumerable<SelectListItem> GetListaPuerto()
        {
            return _db.Puerto.Select(a => new SelectListItem()
            {
                Text = a.Nombre,
                Value = a.IdPuerto.ToString()
            });
        }

        public void Update(Puerto entidad)
        {
            var objDesdeDb = _db.Puerto.FirstOrDefault(a => a.IdPuerto == entidad.IdPuerto);
            objDesdeDb.Nombre = entidad.Nombre;
            objDesdeDb.Descripcion = entidad.Descripcion;
            objDesdeDb.Codigo = entidad.Codigo;

            _db.SaveChanges();
        }
    }
}
