﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Models;

namespace VELogisticCloud.Servicio.Data.Repository
{
    public interface IPuertoRepository:IRepository<Puerto>
    {
        IEnumerable<SelectListItem> GetListaPuerto();
        void Update(Puerto puerto);
    }
}
