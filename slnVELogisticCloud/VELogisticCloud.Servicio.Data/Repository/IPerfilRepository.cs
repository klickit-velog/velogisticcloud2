﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Models;

namespace VELogisticCloud.Servicio.Data.Repository
{
    public interface IPerfilRepository : IRepository<Perfil>
    {
        IEnumerable<SelectListItem> GetListaPerfil();
        void Update(Perfil entidad);

    }
}
