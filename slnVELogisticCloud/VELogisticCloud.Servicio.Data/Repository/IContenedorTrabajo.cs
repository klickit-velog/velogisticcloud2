﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.Seguridad;

namespace VELogisticCloud.Servicio.Data.Repository
{
    public interface IContenedorTrabajo:IDisposable
    {
        IPuertoRepository Puerto { get; }
        IPerfilRepository Perfil { get; }
        UsuarioRepository Usuario { get; }
        void Save();
    }
}
