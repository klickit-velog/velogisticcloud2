﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Models;

namespace VELogisticCloud.Servicio.Data.Repository
{
    public interface INavieraRepository : IRepository<Naviera>
    {
        IEnumerable<SelectListItem> GetListNaviera();
        void Update(Naviera naviera);
    }
}

