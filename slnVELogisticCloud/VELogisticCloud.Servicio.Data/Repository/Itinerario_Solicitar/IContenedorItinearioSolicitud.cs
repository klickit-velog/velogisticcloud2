﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Data.Repository.Itinerario_Solicitar
{
    public interface IContenedorItinearioSolicitud : IDisposable
    {
        IItinerarioSolicitudRepository ItinerarioSolicitudRepository { get; }
        void Save();
    }
}
