﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Models;

namespace VELogisticCloud.Servicio.Data.Repository.Itinerario_Solicitar
{
    public interface IItinerarioSolicitudRepository : IRepository<ItinerarioSolicitud>
    {
        IEnumerable<SelectListItem> GetListItinerario();
        void Update(ItinerarioSolicitud itinerarioSolicitud);
    }
}
