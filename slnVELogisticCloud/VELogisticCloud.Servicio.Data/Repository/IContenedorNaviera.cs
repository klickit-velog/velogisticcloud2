﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Data.Repository
{
    public interface IContenedorNaviera:IDisposable
    {
        INavieraRepository Naviera { get; }
        void Save();
    }
}
