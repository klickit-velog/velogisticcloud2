﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository.Itinerario_Solicitar;

namespace VELogisticCloud.Servicio.Data.ClasesRepository
{
    public class ItinerarioSolicitudRepository : Repository<ItinerarioSolicitud>, IItinerarioSolicitudRepository
    {
        private readonly VECloudDBContext _db;
        public ItinerarioSolicitudRepository(VECloudDBContext db) : base(db)
        {
            _db = db;
        }
        public IEnumerable<SelectListItem> GetListItinerario()
        {
            return _db.ItinerarioSolicitud.Select(a => new SelectListItem()
            {
                Text = a.Naviera,
                Value = a.Id_Reserva.ToString()
            });
        }

        public void Update(ItinerarioSolicitud itinerario)
        {
            var objDesdeDb = _db.ItinerarioSolicitud.FirstOrDefault(a => a.Id_Reserva == itinerario.Id_Reserva);
            objDesdeDb.Naviera = itinerario.Naviera;
            objDesdeDb.Nro_contrato = itinerario.Nro_contrato;
            objDesdeDb.Expeditor = itinerario.Expeditor;
            objDesdeDb.Consignatario = itinerario.Consignatario;
            objDesdeDb.Puerto_Embarque = itinerario.Puerto_Embarque;
            objDesdeDb.Puerto_Descarga = itinerario.Puerto_Descarga;
            objDesdeDb.ETD = itinerario.ETD;
            objDesdeDb.ETA = itinerario.ETA;
            objDesdeDb.Nombre_Nave = itinerario.Nombre_Nave;
            objDesdeDb.Nave_Viaje = itinerario.Nave_Viaje;
            objDesdeDb.Cantidad = itinerario.Cantidad;
            objDesdeDb.Tipo_Contenedor = itinerario.Tipo_Contenedor;
            objDesdeDb.Comodity = itinerario.Comodity;
            objDesdeDb.Temperatura = itinerario.Temperatura;
            objDesdeDb.Ventilacion = itinerario.Ventilacion;
            objDesdeDb.Humedad = itinerario.Humedad;
            objDesdeDb.Peso = itinerario.Peso;
            objDesdeDb.Volumen = itinerario.Volumen;
            objDesdeDb.Flete = itinerario.Flete;
            objDesdeDb.Emision_BL = itinerario.Emision_BL;

            _db.SaveChanges();
        }
    }
}
