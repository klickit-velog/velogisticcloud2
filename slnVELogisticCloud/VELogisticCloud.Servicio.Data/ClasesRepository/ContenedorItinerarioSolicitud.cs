﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.Repository.Itinerario_Solicitar;

namespace VELogisticCloud.Servicio.Data.ClasesRepository
{
    public class ContenedorItinerarioSolicitud : IContenedorItinearioSolicitud
    {
        private readonly VECloudDBContext _context;
        public IItinerarioSolicitudRepository ItinerarioSolicitudRepository { get; private set; }
        public ContenedorItinerarioSolicitud(VECloudDBContext context)
        {
            _context = context;
            ItinerarioSolicitudRepository = new ItinerarioSolicitudRepository(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}


