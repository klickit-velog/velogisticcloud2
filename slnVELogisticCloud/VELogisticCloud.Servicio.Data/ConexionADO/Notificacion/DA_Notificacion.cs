﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.Notificacion;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Notificacion
{
    public class DA_Notificacion : BaseRepository
    {
        public DA_Notificacion(IConfiguration configuration)
        : base(configuration)
        { }

        public async Task<object> InsertNotificacion(BE_NotificacionUsuario ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_InsertarNotificacion");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                  IdUsuario= ent.IdUsuario
                , CorreoUsuario= ent.CorreoUsuario
                , NombreUsuario= ent.NombreUsuario
                , IdTipoNotificacion= ent.IdTipoNotificacion
                , NombreNotificacion= ent.NombreNotificacion
                , UsuarioCreacion = ent.UsuarioCreacion
                
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();       
        }
        public async Task<object> UpdateNotificacion(BE_NotificacionUsuario ent)
        {
             int result;
            string storeProcedure = String.Format("{0}", "USP_UpdateNotificacion");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                 IdUsuario = ent.IdUsuario
                ,IdTipoNotificacion = ent.IdTipoNotificacion
                ,Activo = ent.Activo
                ,UsuarioModificacion = ent.UsuarioModificacion

                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();   
        }

        public async Task<object> ListaNotificacionUsuario()
        {
            string nombreStoreProcedure = "USP_ListaNotificacion";
            IEnumerable<BE_NotificacionUsuario> list = new List<BE_NotificacionUsuario>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_NotificacionUsuario>(nombreStoreProcedure, new
                {
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

    }
}
