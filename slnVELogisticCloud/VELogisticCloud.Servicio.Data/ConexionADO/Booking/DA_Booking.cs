﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Models.Seguimiento;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Booking
{
    public class DA_Booking: BaseRepository
    {
        #region Conexion
        public DA_Booking(IConfiguration configuration)
         : base(configuration)
        { }
        #endregion
        //InsertBooking

        public async Task<object> InsertBooking(InsertBooking ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "Usp_InsertBooking");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ExportadorNombre = ent.ExportadorNombre,
                    ExportadorDomicilio = ent.ExportadorDomicilio,
                    ExportadorRuc = ent.ExportadorRuc,
                    ExportadorContacto = ent.ExportadorContacto,
                    ExportadorTelefono = ent.ExportadorTelefono,
                    ExportadorFax = ent.ExportadorFax,
                    ExportadorCelular = ent.ExportadorCelular,
                    IdConsignatario = ent.IdConsignatario,
                    IdNotify = ent.IdNotify,
                    IdLinea = ent.IdLinea,
                    CodLinea = ent.CodLinea,
                    Linea = ent.Linea,
                    NombreNave = ent.NombreNave,
                    NroViaje = ent.NroViaje,
                    NroContrato = ent.NroContrato,
                    CodigoPaisEmbarque = ent.CodigoPaisEmbarque,
                    PaisEmbarque = ent.PaisEmbarque,
                    IdPuertoEmbarque = ent.IdPuertoEmbarque,
                    CodigoPuertoEmbarque = ent.CodigoPuertoEmbarque,
                    PuertoEmbarque = ent.PuertoEmbarque,
                    CodigoPaisDescarga = ent.CodigoPaisDescarga,
                    PaisDescarga = ent.PaisDescarga,
                    IdPuertoDescarga = ent.IdPuertoDescarga,
                    CodigoPuertoDescarga = ent.CodigoPuertoDescarga,
                    PuertoDescarga = ent.PuertoDescarga,
                    ETA = ent.ETA,
                    ETD = ent.ETD,
                    ClaseContenedor = ent.ClaseContenedor,
                    TipoContenedor = ent.TipoContenedor,
                    AtmosferaControlada = ent.AtmosferaControlada,
                    CO2 = ent.CO2,
                    O2 = ent.O2,
                    ColdTreatment = ent.ColdTreatment,
                    TipoTemperatura = ent.TipoTemperatura,
                    Temperatura = ent.Temperatura,
                    TipoVentilacion = ent.TipoVentilacion,
                    Ventilacion = ent.Ventilacion,
                    TipoHumedad = ent.TipoHumedad,
                    Humedad = ent.Humedad,
                    IdImo = ent.IdImo,
                    IMO = ent.IMO,
                    UN1 = ent.UN1,
                    UN2 = ent.UN2,
                    IdCommodity = ent.IdCommodity,
                    CodigoCommodity = ent.CodigoCommodity,
                    Commodity = ent.Commodity,
                    Variedad = ent.Variedad,
                    PartidaArancelaria = ent.PartidaArancelaria,
                    TipoBulto = ent.TipoBulto,
                    CantidadBulto = ent.CantidadBulto,
                    PesoBruto = ent.PesoBruto,
                    IdCondicionPago = ent.IdCondicionPago,
                    CondicionPago = ent.CondicionPago,
                    IdEmisionBL = ent.IdEmisionBL,
                    EmisionBL = ent.EmisionBL,
                    NombrePagador = ent.NombrePagador,
                    DireccionPagador = ent.DireccionPagador,
                    OperadorLogistico = ent.OperadorLogistico,
                    RucOperador = ent.RucOperador,
                    Nota = ent.Nota,
                    Usuario = ent.Cliente,
                    UsuarioCreacion = ent.UsuarioCreacion,
                    IdDetalleCotizacionOdoo = ent.IdDetalleCotizacionOdoo,
                    IdTipoContenedor = ent.IdTipoContenedor,
                    CantidadBooking = ent.CantidadBooking,
                    IdContactOwner = ent.IdContactOwner,
                    ContactOwner = ent.ContactOwner,
                    NombreCustomer = ent.NombreCustomer,
                    tipoContrato = ent.tipoContrato,
                    Tecnologia = ent.Tecnologia,
                    ImoInttra = ent.ImoInttra
                },
                    commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> UpdateBooking(InsertBooking ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "Usp_UpdateBooking");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    IdBooking = ent.IdBooking,
                    ExportadorNombre = ent.ExportadorNombre,
                    ExportadorDomicilio = ent.ExportadorDomicilio,
                    ExportadorRuc = ent.ExportadorRuc,
                    ExportadorContacto = ent.ExportadorContacto,
                    ExportadorTelefono = ent.ExportadorTelefono,
                    ExportadorFax = ent.ExportadorFax,
                    ExportadorCelular = ent.ExportadorCelular,
                    IdConsignatario = ent.IdConsignatario,
                    IdNotify = ent.IdNotify,
                    IdLinea = ent.IdLinea,
                    CodLinea = ent.CodLinea,
                    Linea = ent.Linea,
                    NombreNave = ent.NombreNave,
                    NroViaje = ent.NroViaje,
                    NroContrato = ent.NroContrato,
                    CodigoPaisEmbarque = ent.CodigoPaisEmbarque,
                    PaisEmbarque = ent.PaisEmbarque,
                    IdPuertoEmbarque = ent.IdPuertoEmbarque,
                    CodigoPuertoEmbarque = ent.CodigoPuertoEmbarque,
                    PuertoEmbarque = ent.PuertoEmbarque,
                    CodigoPaisDescarga = ent.CodigoPaisDescarga,
                    PaisDescarga = ent.PaisDescarga,
                    IdPuertoDescarga = ent.IdPuertoDescarga,
                    CodigoPuertoDescarga = ent.CodigoPuertoDescarga,
                    PuertoDescarga = ent.PuertoDescarga,
                    ETA = ent.ETA,
                    ETD = ent.ETD,
                    ClaseContenedor = ent.ClaseContenedor,
                    TipoContenedor = ent.TipoContenedor,
                    AtmosferaControlada = ent.AtmosferaControlada,
                    CO2 = ent.CO2,
                    O2 = ent.O2,
                    ColdTreatment = ent.ColdTreatment,
                    TipoTemperatura = ent.TipoTemperatura,
                    Temperatura = ent.Temperatura,
                    TipoVentilacion = ent.TipoVentilacion,
                    Ventilacion = ent.Ventilacion,
                    TipoHumedad = ent.TipoHumedad,
                    Humedad = ent.Humedad,
                    IdImo = ent.IdImo,
                    IMO = ent.IMO,
                    UN1 = ent.UN1,
                    UN2 = ent.UN2,
                    IdCommodity = ent.IdCommodity,
                    CodigoCommodity = ent.CodigoCommodity,
                    Commodity = ent.Commodity,
                    Variedad = ent.Variedad,
                    PartidaArancelaria = ent.PartidaArancelaria,
                    TipoBulto = ent.TipoBulto,
                    CantidadBulto = ent.CantidadBulto,
                    PesoBruto = ent.PesoBruto,
                    IdCondicionPago = ent.IdCondicionPago,
                    CondicionPago = ent.CondicionPago,
                    IdEmisionBL = ent.IdEmisionBL,
                    EmisionBL = ent.EmisionBL,
                    NombrePagador = ent.NombrePagador,
                    DireccionPagador = ent.DireccionPagador,
                    OperadorLogistico = ent.OperadorLogistico,
                    RucOperador = ent.RucOperador,
                    Nota = ent.Nota,
                    Cliente = ent.Cliente,
                    UsuarioModificacion = ent.UsuarioModificacion,
                    ContactOwner = ent.ContactOwner,
                    IdContactOwner = ent.IdContactOwner,
                    TipoContrato = ent.tipoContrato,
                    Tecnologia = ent.Tecnologia
                },
                    commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> UpdateNumeroBL(int id, string NumeroBL)
        {
            int result;
            string storeProcedure = String.Format("{0}", "UpdateNumeroBL");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                    NumeroBL = NumeroBL
                },
                    commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> UpdateImo(int id,string imo)
        {
            int result;
            string storeProcedure = String.Format("{0}", "Usp_UpdateImo");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                    IMO = imo,
                
                },
                   commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> InsertConsignatario(InsertConsignatario ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "Usp_InsertConsignatario");
            using (var connection = CreateConnection())
            {
                 result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Nombre = ent.Nombre,
                    Direccion = ent.Direccion,
                    Fax = ent.Fax,
                    Telefono = ent.Telefono,
                    PersonaContacto = ent.PersonaContacto,

                },
                   commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> InsertNotify(InsertNotify ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "Usp_InsertNotify");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Nombre = ent.Nombre,
                    Direccion = ent.Direccion,
                    Fax = ent.Fax,
                    Telefono = ent.Telefono,
                    PersonaContacto = ent.PersonaContacto,

                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> ListaConsignatario()
        {
            IEnumerable<InsertConsignatario> list = new List<InsertConsignatario>();
            string storeProcedure = String.Format("{0}", "Usp_ListaConsignatario");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<InsertConsignatario>(storeProcedure, new
                {
                },
                    commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> ListaNotify()
        {
            IEnumerable<InsertNotify> list = new List<InsertNotify>();
            string storeProcedure = String.Format("{0}", "Usp_ListaNotify");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<InsertNotify>(storeProcedure, new
                {
                },
                    commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> ListaBooking(int id,string Codigo_Usuario)
        {
            IEnumerable<InsertBooking> list = new List<InsertBooking>();
            string storeProcedure = String.Format("{0}", "Usp_ListaBooking");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<InsertBooking>(storeProcedure, new
                {
                    id=id,
                    Codigo_Usuario = Codigo_Usuario,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> ListaBookingXNroBooking(int id, string NroBooking)
        {
            IEnumerable<InsertBooking> list = new List<InsertBooking>();
            string storeProcedure = String.Format("{0}", "Usp_ListaNroBooking");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<InsertBooking>(storeProcedure, new
                {
                    id = id,
                    NroBooking = NroBooking,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<InsertBooking> ObtenerByNumeroBooking(string nroBooking)
        {
            IEnumerable<InsertBooking> list = new List<InsertBooking>();
            string storeProcedure = String.Format("{0}", "USP_BOOKING_OBTENER_ByNroBooking");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<InsertBooking>(storeProcedure, new
                {
                    nroBooking = nroBooking
                },
                commandType: CommandType.StoredProcedure);
            }
            return list.FirstOrDefault();
        }

        public DataTable DatosExcelBooking()
        {
            DataTable resultado = new DataTable();

            using (var cn = new SqlConnection(CreateConnection().ConnectionString) )
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "USP_DatosReporteExcel";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(resultado);
                    }
                }
            }

            return resultado;
        }
        public async Task<object> SeguimientoBooking(int id)
        {
            IEnumerable<SeguimientoReserva> list = new List<SeguimientoReserva>();
            string storeProcedure = String.Format("{0}", "USP_SeguimientoBooking");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<SeguimientoReserva>(storeProcedure, new
                {
                    id = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> CancelarBooking(int id,string user)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_CancelarBooking");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    id = id,
                    user = user
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> ConfirmarBooking(int id, string user)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_ConfirmarBooking");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                    User = user
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> ConfirmarBookingManual(int id,string booking,string imo, string user)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_ConfirmarBookingManual");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Booking = booking.Trim(),
                    IMO = imo.Trim(),
                    IdBooking = id,
                    User = user.Trim()
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> EstadoEnvioOddobooking(int id,string user)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_EnvioOdooBooking");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                    user = user
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> DatosCotizacionMaritima(int id)
        {
            IEnumerable<DatosCotizacionMaritima> list = new List<DatosCotizacionMaritima>();
            string storeProcedure = String.Format("{0}", "USP_DatosCotizacionMaritima");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<DatosCotizacionMaritima>(storeProcedure, new
                {
                    ID = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> InsertBookingItinerarioManual(int IdBooking,int IdItinerarioManual)
        {
            int result;
            string storeProcedure = String.Format("{0}", "Usp_InsertBookingItinerarioManual");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    IdBooking = IdBooking,
                    IdItinerarioManual = IdItinerarioManual
                },
                    commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }

        public async Task<object> NuevaListaBooking(int id, string Codigo_Usuario)
        {
            IEnumerable<NuevaListaBooking> list = new List<NuevaListaBooking>();
            string storeProcedure = String.Format("{0}", "USP_NuevaListaBooking");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<NuevaListaBooking>(storeProcedure, new
                {
                    id = id,
                    Codigo_Usuario = Codigo_Usuario,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public IEnumerable<NuevaListaBooking> ListarBookingDelCustomer(List<string> listaClienteCorreo,int IdBooking, string NroBooking, int IdContactOwner, string NombreLinea, 
            string NombreNave, string NroViaje, int IdEmbarque, int IdDescarga, int IdCommodity, string FechaDesde, string FechaHasta,int EstadoId)
        {
            IEnumerable<NuevaListaBooking> list = new List<NuevaListaBooking>();
            string storeProcedure = "USP_BOOKING_CUSTOMER_LISTAR";
            StringBuilder sbCadenaListaClienteCorreo = new StringBuilder();
            listaClienteCorreo.ForEach(a => sbCadenaListaClienteCorreo.Append(string.Concat("'",a,"',")));
            var cadenaClienteCorreo = sbCadenaListaClienteCorreo.ToString().Substring(0, sbCadenaListaClienteCorreo.Length - 1);
            using (var connection = CreateConnection())
            {
                list = connection.Query<NuevaListaBooking>(storeProcedure, new
                {
                    ListaClientes = cadenaClienteCorreo,
                    IdBooking,
                    NroBooking, 
                    IdContactOwner,
                    NombreLinea, 
                    NombreNave, 
                    NroViaje, 
                    IdEmbarque, 
                    IdDescarga, 
                    IdCommodity, 
                    FechaDesde, 
                    FechaHasta,
                    EstadoId
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

    }
}
