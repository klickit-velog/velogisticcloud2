﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using static VELogisticCloud.Models.Inttra.BE_Inttra;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Inttra
{
    public class DA_Inttra : BaseRepository
    {
        public DA_Inttra(IConfiguration configuration)
                : base(configuration)
        { }

        public async Task<object> InsertArchivo(Be_Archivo ent)
        {
            try
            {
                int result;
                string storeProcedure = String.Format("{0}", "Usp_GuardarArchivo");
                using (var connection = CreateConnection())
                {
                    result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                    {
                        ent.NAME_ARCHIVO,
                        ent.codigoDoc,
                        ent.estado,
                        ent.booking,
                        ent.imo,
                        ent.NumeroReferenciaInttra
                    },
                      commandType: CommandType.StoredProcedure);
                }
                return result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<object> LISTA()
        {
            try
            {
                string nombreStoreProcedure = "LISTA_ARCHIVOS";
                IEnumerable<Be_Archivo> list = new List<Be_Archivo>();
                using (var connection = CreateConnection())
                {
                    list = await connection.QueryAsync<Be_Archivo>(nombreStoreProcedure, new
                    {
                    },
                    commandType: CommandType.StoredProcedure);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<object> Seguimiento_OPL(string DocumentIdentifier, string EventCode)
        {
            try
            {
                var IdReserva = Convert.ToUInt64(DocumentIdentifier);
                int idestado = 0;
                string estado = "";
                #region Asginar estado
                if (EventCode == "EE")
                {
                    idestado = 1;
                    estado = "Depósito de vacíos";
                }
                if (EventCode == "I")
                {
                    idestado = 2;
                    estado = "Gated in";
                }
                if (EventCode == "AE")
                {
                    idestado = 3;
                    estado = "Carga del buque";
                }
                if (EventCode == "VD")
                {
                    idestado = 4;
                    estado = "Salida del buque";
                }
                if (EventCode == "VA")
                {
                    idestado = 5;
                    estado = "Llegada del buque";
                }
                if (EventCode == "UV")
                {
                    idestado = 6;
                    estado = "Descarga del buque";
                }
                if (EventCode == "QA")
                {
                    idestado = 7;
                    estado = "Gated out";
                }
                if (EventCode == "RD")
                {
                    idestado = 8;
                    estado = "Regreso";
                }
                #endregion
                //"EE" "Depósito de vacíos"
                //"I" "Gated in"
                //"AE" "Carga del buque"
                //"VD" "Salida del buque"
                //"VA" "Llegada del buque"
                //"UV" "Descarga del buque"
                //"OA" "Gated out"
                //"RD" "Regreso"
                int result;
                string storeProcedure = String.Format("{0}", "USP_SEGUIMIENTO_RESERVA");
                using (var connection = CreateConnection())
                {
                    result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                    {
                        ID = IdReserva,
                        Code = EventCode,
                        Estado = estado,

                    },
                      commandType: CommandType.StoredProcedure);
                }
                return result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
