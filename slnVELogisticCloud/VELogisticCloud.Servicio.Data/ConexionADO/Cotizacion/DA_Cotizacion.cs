﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.Entities;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Cotizacion
{
    public class DA_Cotizacion : BaseRepository
    {
        #region Conexion
        public DA_Cotizacion(IConfiguration configuration)
           : base(configuration)
        { }
        #endregion

        #region Flete Maritimo
        public async Task<object> DetalleMaritimo(int id)
        {
            IEnumerable<DetalleMaritimo> list = new List<DetalleMaritimo>();
            string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<DetalleMaritimo>(storeProcedure, new
                {
                    ID = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> Lista_Cotizaciones(Parametros_Lista_Maritimo param)
        {
            string nombreStoreProcedure = string.Empty;
            switch (param.TipoCotizacion)
            {
                case TipoCotizacion.FLETE_MARITIMO: nombreStoreProcedure = "USP_LISTA_COTIZACION_MARITIMO"; break;
                case TipoCotizacion.OPERACION_LOGISTICA: nombreStoreProcedure = "USP_LISTAR_COTIZACION_LOGISTICA"; break;
                case TipoCotizacion.TRANSPORTE: nombreStoreProcedure = "USP_LISTAR_COTIZACION_TRANSPORTE"; break;
            }

            IEnumerable<Lista_Maritimo> list = new List<Lista_Maritimo>();

            var dt = new DataTable();
            dt.Columns.Add("usuario", typeof(string));

            if (param.ListadoUsuarios != null)
            {
                foreach (var usuario in param.ListadoUsuarios)
                {
                    dt.Rows.Add(usuario);
                }
            }

            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Lista_Maritimo>(nombreStoreProcedure, new
                {
                    cod_coti = param.cod_coti,
                    Campana = param.Campana,
                    cod_line_navi = param.cod_line_navi,
                    cod_puerto_destino = param.cod_puerto_destino,
                    cod_puerto_salida = param.cod_puerto_salida,
                    id_estado = param.id_estado,
                    Codigo_Usuario = param.Codigo_Usuario,
                    ListadoUsuarios = dt
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> Lista_CotizacionesAprobadas(Parametros_Lista_Maritimo param)
        {
            string nombreStoreProcedure = string.Empty;

            switch (param.TipoCotizacion)
            {
                case TipoCotizacion.FLETE_MARITIMO: nombreStoreProcedure = "USP_LISTA_COTIZACION_MARITIMO_APROBADOS"; break;
                case TipoCotizacion.OPERACION_LOGISTICA: nombreStoreProcedure = "USP_LISTAR_COTIZACION_LOGISTICA"; break;
                case TipoCotizacion.TRANSPORTE: nombreStoreProcedure = "USP_LISTAR_COTIZACION_TRANSPORTE"; break;
            }

            IEnumerable<Lista_Maritimo> list = new List<Lista_Maritimo>();
            var dt = new DataTable();
            dt.Columns.Add("usuario", typeof(string));

            if(param.ListadoUsuarios != null)
            {
                foreach (var usuario in param.ListadoUsuarios)
                {
                    dt.Rows.Add(usuario);
                }
            }
           
            
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Lista_Maritimo>(nombreStoreProcedure, new
                {
                    cod_coti = param.cod_coti,
                    Campana = param.Campana,
                    cod_line_navi = param.cod_line_navi,
                    cod_puerto_destino = param.cod_puerto_destino,
                    cod_puerto_salida = param.cod_puerto_salida,
                    id_estado = param.id_estado,
                    Codigo_Usuario = param.Codigo_Usuario,
                    ListadoUsuarios = dt
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> MisSolicitudesMaritimo(Parametros_Lista_Maritimo param)
        {
            string nombreStoreProcedure = "USP_LISTA_SOLICITUDES_MARITIMO";

            IEnumerable<Lista_Maritimo> list = new List<Lista_Maritimo>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Lista_Maritimo>(nombreStoreProcedure, new
                {
                    cod_coti = param.cod_coti,
                    Campana = param.Campana,
                    cod_line_navi = param.cod_line_navi,
                    cod_puerto_destino = param.cod_puerto_destino,
                    cod_puerto_salida = param.cod_puerto_salida,
                    id_estado = param.id_estado,
                    Codigo_Usuario = param.Codigo_Usuario,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> Lista_Logistica(Parametros_Lista_Logistica param)
        {
            string nombreStoreProcedure = "USP_LISTAR_COTIZACION_LOGISTICA_2";
            IEnumerable<Lista_Logistica> list = new List<Lista_Logistica>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Lista_Logistica>(nombreStoreProcedure, new
                {
                    numero_contenedor = param.numero_contenedor,
                    codigo_bl = param.codigo_bl,
                    numero_dam = param.numero_dam,
                    numero_viaje = param.numero_viaje,
                    numero_booking = param.numero_booking,
                    commodity = param.commodity,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> InsertMaritimoCabecera(BE_Maritimo ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_INSERTAR_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Cant_contenedor = ent.Cantidad_Contenedor,
                    Cod_tipocontenedor = ent.Codigo_Tipo_Contenedor,
                    Tipo_contenedor = ent.Tipo_Contenedor,
                    Codigo_coti = ent.Codigo_Cotizacion,
                    Cliente = ent.Cliente,
                    Fec_Envio = ent.Fecha_Envio,
                    Fec_Final = ent.Fecha_Final,
                    Campana = ent.Campania,
                    CodigoCampania = ent.Codigo_Campania,
                    Nota = ent.Nota,
                    Observacion = ent.Observacion,
                    IdUsuarioCreacion = ent.IdUsuarioCreacion,
                    Codigo_Usuario = ent.Codigo_Usuario,

                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }

        public async Task<object> InsertIdOddo_Maritimo(int idcotizacionmaritimo, int idoddo)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_AGREGAR_ID_ODDO_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID_COTIZACION_MARITIMO = idcotizacionmaritimo,
                    IdOddoService = idoddo,
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> InsertIdOddo_Logistica(int idcotizacionlogistica, int idoddo)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_AGREGAR_ID_ODDO_COTIZACION_LOGISTICA");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID_COTIZACION_LOGISTICA = idcotizacionlogistica,
                    IdOddoService = idoddo,
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> InsertMaritimoDetalle(BE_Det_Maritimo ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_INSERTAR_COTIZACION_MARITIMO_DETALLE");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Id_flete_mar = ent.Id_Cotizacion_Maritimo,
                    Region = ent.Region,
                    Cod_line_navi = ent.Codigo_Linea_Naviera,
                    Linea_naviera = ent.Linea_Naviera,
                    Cod_puertoemba = ent.Codigo_Puerto_Embarque,
                    Carga_puerto = ent.Puerto_Embarque,
                    Cod_puertodes = ent.Codigo_Puerto_Descarga,
                    desc_puerto = ent.Puerto_Descarga,
                    Cod_comodity = ent.Codigo_Commodity,
                    Commodity = ent.Commodity,
                    Tamano = ent.Tamanio,
                    TT = ent.TT,
                    Codigo_Pais_Embarque = ent.Codigo_Pais_Embarque,
                    Pais_Puerto_Embarque = ent.Pais_Puerto_Embarque,
                    Codigo_Pais_Descarga = ent.Codigo_Pais_Descarga,
                    Pais_Puerto_Descarga = ent.Pais_Puerto_Descarga,
                    Id_Puerto_Carga = ent.Id_Puerto_Carga,
                    Id_Puerto_Descarga = ent.Id_Puerto_Descarga,
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> DeleteMaritimoDetalle(int id)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_DELETE_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }

        public async Task<object> UpdateMaritimoCabecera(int id ,BE_Maritimo ent,string UserUpdate)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_EDITAR_CABECERA_COTI_MARITIMO");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                    Cantidad_Contenedor = ent.Cantidad_Contenedor,
                    Codigo_Tipo_Contenedor = ent.Codigo_Tipo_Contenedor,
                    Tipo_Contenedor = ent.Tipo_Contenedor,
                    Fecha_Envio = ent.Fecha_Envio,
                    Fecha_Final = ent.Fecha_Final,
                    Campania = ent.Campania,
                    Nota = ent.Nota,
                    Observacion = ent.Observacion,
                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }

        public async Task<object> Confirmar_RechazarCotizacionMaritimo(int id, int estado)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_CONFIRMAR_RECHAZAR_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ESTADO = estado,
                    ID_COTIZACION_MARITIMO = id,
                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        #endregion

        #region Editar Maritimo
        public async Task<object> DatosEditarMaritimo(int id)
        {
            string nombreStoreProcedure = "USP_DATOS_COT_MARITIMO_CS_CLIENTE";
            IEnumerable<DatosEditarMaritimo> list = new List<DatosEditarMaritimo>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<DatosEditarMaritimo>(nombreStoreProcedure, new
                {
                    ID = id,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        #endregion

        #region Editar Logistica
        public async Task<object> DatosEditarLogistica(int id)
        {
            string nombreStoreProcedure = "USP_DATOS_COTIZACION_LOGISTICA";
            IEnumerable<DatosEditarLogistica> list = new List<DatosEditarLogistica>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<DatosEditarLogistica>(nombreStoreProcedure, new
                {
                    ID = id,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> UpdateCotizacionLogistica(int id ,BE_Logistica ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_UPDATE_COTIZACION_CLIENTE_LOGISTICA");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ID = id,
                    Cod_puerto_embar = ent.Codigo_Puerto_Embarque,
                    Puerto_embar = ent.Puerto_Embarque,
                    Cod_commodity = ent.Codigo_Commodity,
                    Commodity = ent.Commodity,
                    Servicio_embar = ent.Servicio_Embarque,
                    Retiro_cont_vacio = ent.Retiro_Contenedor_Vacio,
                    Cod_almacen_retiro = ent.Codigo_Almacen_Retiro,
                    Almacen_retiro = ent.Almacen_Retiro,
                    Fec_cita = ent.Fecha_Cita,
                    Retiro_trans = ent.Retiro_Transporte,
                    Cod_almacen_pack = ent.Codigo_Almacen_Packing,
                    Almacen_pack = ent.Almacen_Packing,
                    Ser_Agen_Aduana = ent.Servicio_Agenciamiento_Aduana,
                    Cert_origen = ent.Certificado_Origen,
                    Conex_elect = ent.Conexion_Electrica,
                    Pago_cpb = ent.Pago_CPB,
                    Agenciamiento_Maritimo = ent.Agenciamiento_Maritimo,
                    Movilizaciones_Contenedor = ent.Movilizaciones_Contenedor,
                    Generador = ent.Generador,
                    Aforo_Fisico = ent.Aforo_Fisico,
                    Courier = ent.Courier,
                    Senasa = ent.Senasa,
                    Filtro = ent.Filtro,
                    Termografo = ent.Termografo,
                    Tipo_Termografo = ent.Tipo_Termografo,
                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        #endregion

        #region Operacion Logistica

        
        public async Task<object> TablaDetalleLogistica(int id)
        {
            string nombreStoreProcedure = "USP_TABLA_DETALLE_COTIZACION_LOGISTICA";
            IEnumerable<TablaDetalleLogistica> list = new List<TablaDetalleLogistica>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<TablaDetalleLogistica>(nombreStoreProcedure, new
                {
                    ID = id,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> MisSolicitudes_Logistica(Parametros_Lista_Maritimo param)
        {
            string nombreStoreProcedure = "USP_MIS_SOLICITUDES_LOGISTICA";
            IEnumerable<Mis_Solicitudes_Logisticas> list = new List<Mis_Solicitudes_Logisticas>();

            var dt = new DataTable();
            dt.Columns.Add("usuario", typeof(string));

            if (param.ListadoUsuarios != null)
            {
                foreach (var usuario in param.ListadoUsuarios)
                {
                    dt.Rows.Add(usuario);
                }
            }

            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Mis_Solicitudes_Logisticas>(nombreStoreProcedure, new
                {
                    Id = param.cod_coti,
                    Campana = param.Campana,
                    cod_line_navi = param.cod_line_navi,
                    cod_puerto_destino = param.cod_puerto_destino,
                    cod_puerto_salida = param.cod_puerto_salida,
                    id_estado = param.id_estado,
                    Codigo_Usuario = param.Codigo_Usuario,
                    ListadoUsuarios = dt
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> InsertOpeLogi(BE_Logistica ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_INSERTAR_COTIZACION_LOGISTICA");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Cod_puerto_embar = ent.Codigo_Puerto_Embarque,
                    Puerto_embar = ent.Puerto_Embarque,
                    Cod_commodity = ent.Codigo_Commodity,
                    Commodity = ent.Commodity,
                    Servicio_embar = ent.Servicio_Embarque,
                    Retiro_cont_vacio = ent.Retiro_Contenedor_Vacio,
                    Cod_almacen_retiro = ent.Codigo_Almacen_Retiro,
                    Almacen_retiro = ent.Almacen_Retiro,
                    Fec_cita = ent.Fecha_Cita,
                    Retiro_trans = ent.Retiro_Transporte,
                    Cod_almacen_pack = ent.Codigo_Almacen_Packing,
                    Almacen_pack = ent.Almacen_Packing,
                    Ser_Agen_Aduana = ent.Servicio_Agenciamiento_Aduana,
                    Cert_origen = ent.Certificado_Origen,
                    Conex_elect = ent.Conexion_Electrica,
                    Pago_cpb = ent.Pago_CPB,
                    Agenciamiento_Maritimo = ent.Agenciamiento_Maritimo,
                    Movilizaciones_Contenedor = ent.Movilizaciones_Contenedor,
                    Generador = ent.Generador,
                    Aforo_Fisico = ent.Aforo_Fisico,
                    Courier = ent.Courier,
                    Senasa = ent.Senasa,
                    Filtro = ent.Filtro,
                    Termografo = ent.Termografo,
                    Tipo_Termografo = ent.Tipo_Termografo,
                    IdUsuarioCreacion = ent.IdUsuarioCreacion,
                    Codigo_Pais_Embarque = ent.Codigo_Pais_Embarque,
                    Codigo_Pais_Descarga = ent.Codigo_Pais_Descarga,
                    Pais_Puerto_Embarque = ent.Pais_Puerto_Embarque,
                    Pais_Puerto_Descarga = ent.Pais_Puerto_Descarga,
                    Id_Puerto_Carga = ent.Id_Puerto_Carga,
                    Chk_Transporte = ent.Chk_Transporte,
                    Chk_GateOut = ent.Chk_GateOut,
                    Chk_AgenciamientoAduana = ent.Chk_AgenciamientoAduana,
                    Chk_DerechoEmbarque = ent.Chk_DerechoEmbarque,
                    Chk_VistoBueno = ent.Chk_VistoBueno,
                    Chk_EnvioDocumentos = ent.Chk_EnvioDocumentos,
                    Chk_Movilidad = ent.Chk_Movilidad,
                    Chk_TermoRegistros = ent.Chk_TermoRegistros,
                    Chk_FiltrosEtileno = ent.Chk_FiltrosEtileno,
                    Chk_Certificado = ent.Chk_Certificado,
                    IdCampania = ent.IdCampania,
                    Desc_Campania = ent.Desc_Campania,
                    TipoTermo = ent.TipoTermo,
                    TipoFiltro = ent.TipoFiltro,
                    Comentarios = ent.Comentarios,
                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> DetalleLogistica(int id)
        {
            string nombreStoreProcedure = "USP_DETALLE_COTIZACION_LOGISTICA";
            IEnumerable<DetalleLogistica> list = new List<DetalleLogistica>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<DetalleLogistica>(nombreStoreProcedure, new
                {
                    ID = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> Confirmar_RechazarCotizacionLogistica(int id,int estado)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_CONFIRMAR_RECHAZAR_COTIZACION_LOGISTICA");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    ESTADO = estado,
                    ID_COTIZACIONLOGISTICA = id

                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> ListaCotizacionAprobadasLogistica(Parametros_Lista_Maritimo param)
        {
            string nombreStoreProcedure = "USP_COTIZACION_LOGISTICA_APROBADA2";
            IEnumerable<ListaCotizacionesLogisticasAprobadas> list = new List<ListaCotizacionesLogisticasAprobadas>();
            var dt = new DataTable();
            dt.Columns.Add("usuario", typeof(string));

            if (param.ListadoUsuarios != null)
            {
                foreach (var usuario in param.ListadoUsuarios)
                {
                    dt.Rows.Add(usuario);
                }
            }

            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<ListaCotizacionesLogisticasAprobadas>(nombreStoreProcedure, new
                {
                    cod_coti = param.cod_coti,
                    Campana = param.Campana,
                    cod_puerto_destino = param.cod_puerto_destino,
                    cod_puerto_salida = param.cod_puerto_salida,
                    id_estado = param.id_estado,
                    Codigo_Usuario = param.Codigo_Usuario,
                    IdNaviera = param.IdNaviera,
                    ListadoUsuarios = dt
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        #endregion

        #region Crear detalle cotizacion API

        public async Task<object> InsertDetalleCotizacionAPI(DetalleCotizacionAPI ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_InsertarCotizacionMaritimoDetalleApi");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Id_Cotizacion_Maritimo = ent.Id_Cotizacion_Maritimo,
                    Region = ent.Region,
                    Codigo_Linea_Naviera = ent.Codigo_Linea_Naviera,
                    Linea_Naviera =  ent.Linea_Naviera,
                    Codigo_Puerto_Embarque = ent.Codigo_Puerto_Embarque,
                    Puerto_Embarque = ent.Puerto_Embarque,
                    Codigo_Puerto_Descarga = ent.Codigo_Puerto_Descarga,
                    Puerto_Descarga = ent.Puerto_Descarga,
                    Codigo_Commodity = ent.Id_Commodity,
                    Commodity = ent.Commodity,
                    Tamanio = ent.Tamanio,
                    TT =  ent.TT,
                    Valido_Desde = ent.Valido_Desde,
                    Valido_Hasta = ent.Valido_Hasta,
                    Neto = ent.Neto,
                    Rebate = ent.Rebate,
                    Flete = ent.Flete,
                    Fecha_Aprobacion = ent.Fecha_Aprobacion,
                    Comentarios = ent.Comentarios,
                    IdUsuarioCreacion = ent.IdUsuarioCreacion,
                    FechaCreacion = ent.FechaCreacion,
                    IdUsuarioModificacion = ent.IdUsuarioModificacion,
                    FechaModificacion = ent.FechaModificacion,
                    Estado = ent.Estado,
                    NumeroCotizacion = ent.NumeroCotizacion,
                    NumeroContrato = ent.NumeroContrato,
                    CodigoCampania = ent.CodigoCampania,
                    Campania = ent.Campania,
                    Codigo_Pais_Embarque = ent.Codigo_Pais_Embarque,
                    Codigo_Pais_Descarga = ent.Codigo_Pais_Descarga,
                    Pais_Puerto_Descarga = ent.Pais_Puerto_Descarga,
                    Pais_Puerto_Embarque = ent.Pais_Puerto_Embarque,
                    Id_Puerto_Carga = ent.Id_Puerto_Embarque,
                    Id_Puerto_Descarga = ent.Id_Puerto_Descarga,
                    Id_Naviera = ent.Id_Naviera,
                },
                commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        #endregion

        #region Lista Detalle Cotizacion para Odoo
        public String oSqlConnIN;
        public async Task<object> DetalleCotizacionLogistica(int id)
        {
            string nombreStoreProcedure = "USP_DETALLE_LOGISTICA_ODOO";
            IEnumerable<BE_DetalleLogisticaParaOdoo> list = new List<BE_DetalleLogisticaParaOdoo>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_DetalleLogisticaParaOdoo>(nombreStoreProcedure, new
                {
                    ID = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        
        public void BulkInsertCotizacionDetalle(List<CotizacionLogisticaDetalle> ent)
        {
            CotizacionLogisticaDetalle be = new CotizacionLogisticaDetalle();
            DataTable table = new DataTable();
            table.TableName = "CotizacionLogisticaDetalle";
            
            table.Columns.Add("IdCotizacionLogisticaDetalle", typeof(int));
            table.Columns.Add("IdCotizacionLogistica", typeof(int));
            table.Columns.Add("IdNaviera", typeof(int));
            table.Columns.Add("NombreNaviera", typeof(string));
            table.Columns.Add("IdPuertoCarga", typeof(int));
            table.Columns.Add("NombrePuertoCarga", typeof(string));
            table.Columns.Add("CodigoPaisCarga", typeof(string));
            table.Columns.Add("NombrePaisCarga", typeof(string));
            table.Columns.Add("IdServicio", typeof(int));
            table.Columns.Add("NombreServicio", typeof(string));
            table.Columns.Add("IdTerminalRetiroVacio", typeof(int));
            table.Columns.Add("NombreTerminalRetiroVacio", typeof(string));
            table.Columns.Add("IdAlmacenPacking", typeof(int));
            table.Columns.Add("NombreAlmacenPacking", typeof(string));

            table.Columns.Add("FechaAprobacion", typeof(DateTime));
            table.Columns.Add("IdCotizacionDetalleOdoo", typeof(int)); 
            table.Columns.Add("CodigoContrato", typeof(int)); 

            table.Columns.Add("Flete", typeof(decimal)); 

            table.Columns.Add("FechaValidoDesde", typeof(DateTime)); 
            table.Columns.Add("FechaValidoHasta", typeof(DateTime)); 

            table.Columns.Add("IdEstado", typeof(int));
            table.Columns.Add("FechaCreacion", typeof(DateTime));
            table.Columns.Add("UsuarioCreacion", typeof(string));

            table.Columns.Add("FechaActualizacion", typeof(DateTime));
            table.Columns.Add("UsuarioActualizacion", typeof(string));

            table.Columns.Add("IdTerminalDepositoVacio", typeof(int));
            table.Columns.Add("NombreTerminalDepositoVacio", typeof(string));
            

            foreach (var person in ent)
            {
                var row = table.NewRow();
                
                row[nameof(be.IdCotizacionLogisticaDetalle)] = 0;
                row[nameof(be.IdCotizacionLogistica)] = person.IdCotizacionLogistica;
                row[nameof(be.IdNaviera)] = person.IdNaviera;
                row[nameof(be.NombreNaviera)] = person.NombreNaviera;
                row[nameof(be.IdPuertoCarga)] = person.IdPuertoCarga;
                row[nameof(be.NombrePuertoCarga)] = person.NombrePuertoCarga;
                row[nameof(be.CodigoPaisCarga)] = person.CodigoPaisCarga;
                row[nameof(be.NombrePaisCarga)] = person.NombrePaisCarga;
                row[nameof(be.IdServicio)] = person.IdServicio;
                row[nameof(be.NombreServicio)] = person.NombreServicio;
                row[nameof(be.IdTerminalRetiroVacio)] = person.IdTerminalRetiroVacio;
                row[nameof(be.NombreTerminalRetiroVacio)] = person.NombreTerminalRetiroVacio;
                row[nameof(be.IdAlmacenPacking)] = person.IdAlmacenPacking;
                row[nameof(be.NombreAlmacenPacking)] = person.NombreAlmacenPacking;
                row[nameof(be.FechaAprobacion)] = DBNull.Value;
                row[nameof(be.IdCotizacionDetalleOdoo)] = DBNull.Value;
                row[nameof(be.CodigoContrato)] = DBNull.Value;
                row[nameof(be.Flete)] = person.Flete;
                row[nameof(be.FechaValidoDesde)] = DBNull.Value;
                row[nameof(be.FechaValidoHasta)] = DBNull.Value;
                row[nameof(be.Flete)] = person.Flete;
                row[nameof(be.IdEstado)] = person.IdEstado;
                row[nameof(be.FechaCreacion)] = person.FechaCreacion;
                row[nameof(be.UsuarioCreacion)] = person.UsuarioCreacion;
                row[nameof(be.FechaActualizacion)] = DBNull.Value;
                row[nameof(be.UsuarioActualizacion)] = DBNull.Value;
                row[nameof(be.IdTerminalDepositoVacio)] = person.IdTerminalDepositoVacio;
                row[nameof(be.NombreTerminalDepositoVacio)] = person.NombreTerminalDepositoVacio;
                table.Rows.Add(row);
            }
       
            using (var bulkInsert = new SqlBulkCopy(CreateConnection().ConnectionString))
            {
                bulkInsert.DestinationTableName = table.TableName;
                bulkInsert.WriteToServer(table);
            }
        }

        #endregion

    }



}


// 3cm  18cm x 30 214 cm broca 1/4 , fierro liso de 1async Task<object>/4 o construccion 1/8 


