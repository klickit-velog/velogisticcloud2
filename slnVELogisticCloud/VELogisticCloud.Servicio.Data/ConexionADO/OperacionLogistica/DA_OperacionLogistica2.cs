﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.OperacionLogistica;
using VELogisticCloud.Models.Seguimiento;

namespace VELogisticCloud.Servicio.Data.ConexionADO.OperacionLogistica
{
    public class DA_OperacionLogistica2 : BaseRepository
    {
        public DA_OperacionLogistica2(IConfiguration configuration)
           : base(configuration)
        { }

        public async Task<object> DA_InsertarOperacionLogistica(BE_OperacionLogistica2 ent)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_InsertarOperacionLogistica2");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    NroBooking=  ent.NroBooking.Trim()
                   ,IdLinea=  ent.IdLinea
                   ,CodLinea=  ent.CodLinea
                   ,Linea=  ent.Linea
                   ,NombreNave=  ent.NombreNave
                   ,ETA=  ent.ETA
                   ,CodigoPaisEmbarque=  ent.CodigoPaisEmbarque
                   ,PaisEmbarque=  ent.PaisEmbarque
                   ,IdPuertoEmbarque=  ent.IdPuertoEmbarque
                   ,CodigoPuertoEmbarque=  ent.CodigoPuertoEmbarque
                   ,PuertoEmbarque=  ent.PuertoEmbarque
                   ,CodigoPaisDescarga=  ent.CodigoPaisDescarga
                   ,PaisDescarga=  ent.PaisDescarga
                   ,IdPuertoDescarga=  ent.IdPuertoDescarga
                   ,CodigoPuertoDescarga=  ent.CodigoPuertoDescarga
                   ,PuertoDescarga=  ent.PuertoDescarga
                   ,MarcasProducto=  ent.MarcasProducto
                   ,PesoCaja=  ent.PesoCaja
                   ,CantidadPaletas=  ent.CantidadPaletas
                   ,CondicionPago=  ent.CondicionPago
                   ,PesoNeto=  ent.PesoNeto
                   ,ValorFob=  ent.ValorFob
                   ,IdIncoterm=  ent.IdIncoterm
                   ,CodIncoterm=  ent.CodIncoterm
                   ,Incoterm=  ent.Incoterm
                   ,CantidadTermoregistros=  ent.CantidadTermoregistros
                   ,CantidadFiltrosEtileno=  ent.CantidadFiltrosEtileno
                   ,Observaciones=  ent.Observaciones
                   ,IdOrigenProducto=  ent.IdOrigenProducto
                   ,OrigenProducto=  ent.OrigenProducto
                   ,CitaPacking=  ent.CitaPacking
                   ,HoraInspeccionSenasa=  ent.HoraInspeccionSenasa
                   ,RazonSocialPacking=  ent.RazonSocialPacking
                   ,RucPacking=  ent.RucPacking
                   ,DireccionPacking=  ent.DireccionPacking
                   ,Drawback=  ent.Drawback
                   ,IdDepositoVacio=  ent.IdDepositoVacio
                   ,DepositoVacio=  ent.DepositoVacio
                   ,CodigoPaisEmbarqueInttra=  ent.CodigoPaisEmbarqueInttra
                   ,PaisEmbarqueInttra=  ent.PaisEmbarqueInttra
                   ,IdPuertoEmbarqueInttra=  ent.IdPuertoEmbarqueInttra
                   ,CodigoPuertoEmbarqueInttra=  ent.CodigoPuertoEmbarqueInttra
                   ,PuertoEmbarqueInttra=  ent.PuertoEmbarqueInttra
                   ,UsuarioCreacion=  ent.UsuarioCreacion
                   ,IdCotizacionOddo=  ent.IdCotizacionOddo
                   ,Codigo_Usuario=  ent.Codigo_Usuario
                   ,CanalSini = ent.CanalSini
                   ,Transporte = ent.Transporte
                   ,IdTransporte = ent.IdTransporte
                   ,IdCondicionPago = ent.IdCondicionPago
                   ,Nombre = ent.Nombre
                   ,IdConsigne = ent.IdConsigne
                   ,TelefonoConsigne = ent.TelefonoConsigne
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();       
        }

        public async Task<object> DA_UpdateOperacionLogistica(BE_OperacionLogistica2 ent)
        {
             int result;
            string storeProcedure = String.Format("{0}", "USP_UpdateOperacionLogistica");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                  IdOperacionLogistica = ent.IdOperacionLogistica
                , NroBooking				= ent.NroBooking.Trim()				
                , IdLinea					= ent.IdLinea					
                , CodLinea					= ent.CodLinea					
                , Linea						= ent.Linea						
                , NombreNave				= ent.NombreNave					
                , ETA						= ent.ETA						
                , CodigoPaisEmbarque		= ent.CodigoPaisEmbarque			
                 ,PaisEmbarque				= ent.PaisEmbarque				
                , IdPuertoEmbarque			= ent.IdPuertoEmbarque			
                , CodigoPuertoEmbarque		= ent.CodigoPuertoEmbarque		
                , PuertoEmbarque			= ent.PuertoEmbarque				
                , CodigoPaisDescarga		= ent.CodigoPaisDescarga			
                , PaisDescarga				= ent.PaisDescarga				
                , IdPuertoDescarga			= ent.IdPuertoDescarga			
                , CodigoPuertoDescarga		= ent.CodigoPuertoDescarga		
                , PuertoDescarga			= ent.PuertoDescarga				
                , MarcasProducto			= ent.MarcasProducto				
                , PesoCaja					= ent.PesoCaja					
                , CantidadPaletas			= ent.CantidadPaletas			
                , CondicionPago				= ent.CondicionPago				
                , PesoNeto					= ent.PesoNeto					
                , ValorFob					= ent.ValorFob					
                , IdIncoterm				= ent.IdIncoterm					
                , CodIncoterm				= ent.CodIncoterm				
                , Incoterm					= ent.Incoterm					
                , CantidadTermoregistros	= ent.CantidadTermoregistros		
                , CantidadFiltrosEtileno	= ent.CantidadFiltrosEtileno		
                , Observaciones				= ent.Observaciones				
                , IdOrigenProducto			= ent.IdOrigenProducto			
                , OrigenProducto			= ent.OrigenProducto				
                , CitaPacking				= ent.CitaPacking				
                , HoraInspeccionSenasa		= ent.HoraInspeccionSenasa		
                , RazonSocialPacking		= ent.RazonSocialPacking			
                , RucPacking				= ent.RucPacking					
                , DireccionPacking			= ent.DireccionPacking			
                , Drawback					= ent.Drawback					
                , IdDepositoVacio			= ent.IdDepositoVacio			
                , DepositoVacio				= ent.DepositoVacio				
                , CodigoPaisEmbarqueInttra	= ent.CodigoPaisEmbarqueInttra	
                , PaisEmbarqueInttra		= ent.PaisEmbarqueInttra			
                , IdPuertoEmbarqueInttra	= ent.IdPuertoEmbarqueInttra		
                , CodigoPuertoEmbarqueInttra= ent.CodigoPuertoEmbarqueInttra	
                , PuertoEmbarqueInttra		= ent.PuertoEmbarqueInttra		
                , OrdenServicio				= ent.OrdenServicio				
                , UsuarioModificacion	    = ent.UsuarioModificacion	
                , Placa                     = ent.Placa
                , Chofer                    = ent.Chofer
                , CanalSini                 = ent.CanalSini
                , Transporte                = ent.Transporte
                , IdTransporte              = ent.IdTransporte
                , IdCondicionPago           = ent.IdCondicionPago
                , Nombre                    = ent.Nombre
                , TelefonoConsigne          = ent.TelefonoConsigne
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();   
        }
        public async Task<object> DA_ListaOperacionLogistica(int id, string Codigo_Usuario,List<string> ListadoUsuarios)
        {
            string nombreStoreProcedure = "USP_ListaOperacionLogistica2";
            IEnumerable<BE_OperacionLogistica2> list = new List<BE_OperacionLogistica2>();

            var dt = new DataTable();
            dt.Columns.Add("usuario", typeof(string));

            if (ListadoUsuarios != null)
            {
                foreach (var usuario in ListadoUsuarios)
                {
                    dt.Rows.Add(usuario);
                }
            }
                //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_OperacionLogistica2>(nombreStoreProcedure, new
                {
                    Id = id,
                    Codigo_Usuario = Codigo_Usuario,
                    ListadoUsuarios = dt
                },
                commandType: CommandType.StoredProcedure);
            }

            return list;
        }

        public async Task<object> DA_DatosEnvioPedido(int IdOperacionLogistica)
        {
            string nombreStoreProcedure = "DatosEnvioPedido";
            IEnumerable<BE_DatosPedido> list = new List<BE_DatosPedido>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_DatosPedido>(nombreStoreProcedure, new
                {
                    IdOperacionLogistica = IdOperacionLogistica
                },
                commandType: CommandType.StoredProcedure);
            }

            return list;
        }

        

        public async Task<object> DA_DetalleCotizacion(int id)
        {
            string nombreStoreProcedure = "USP_DatosCotizacionDetalle";
            IEnumerable<BE_CotizacionDetalleOdoo> list = new List<BE_CotizacionDetalleOdoo>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_CotizacionDetalleOdoo>(nombreStoreProcedure, new
                {
                    Id = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list; 
        }
        public async Task<object> DA_CancelarOperacionLogistica(int id, string user)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_CancelarOperacionLogistica");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    id = id,
                    user = user
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();   
        }
        public async Task<object> DA_ConfirmarOperacionLogistica(int id, string user)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_ConfirmarOperacionLogistica");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    id = id,
                    user = user
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> SeguimientoOPL(int id)
        {
            string nombreStoreProcedure = "USP_SeguimientoOperacionLogistica2";
            IEnumerable<SeguimientoOPL> list = new List<SeguimientoOPL>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<SeguimientoOPL>(nombreStoreProcedure, new
                {
                    id = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public DataTable DA_ListarExcelAduanero(int idOperacionLogistica)
        {
            DataTable resultado = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(CreateConnection().ConnectionString))
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "USP_GENERAR_EXCEL_ADUANERO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@ID_OPERACION_LOGISTICA",
                        Value = idOperacionLogistica,
                        SqlDbType = SqlDbType.Int
                    });

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(resultado);
                    }
                }
            }

            return resultado;
        }

        public async Task<object> DA_ApiUpdateOPL(int id, string codEstado, string descEstado, string fechaIni, string code, string status_id)
        {
              int result;
            string storeProcedure = String.Format("{0}", "USP_API_UPDATE_STATUS_OPL");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    id = id,
                    Cod_Estado = codEstado,
                    Desc_Estado = descEstado,
                    FechaIni = fechaIni,
                    code = code,
                    status_id = status_id
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();     
            //return Convert.ToString(oCon.EjecutarEscalar("USP_API_UPDATE_STATUS_OPL", id, codEstado, descEstado, fechaIni, code, status_id));
        }
        public DataTable DatosExcelOpl()
        {
            DataTable resultado = new DataTable();

            using (var cn = new SqlConnection(CreateConnection().ConnectionString))
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "USP_DATOS_EXCEL_OPL";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        da.Fill(resultado);
                    }
                }
            }

            return resultado;
        }

        public object DA_UpdateOplDatosObligatorios(string OrdenServicio,string Transporte,string CanalSini, int id)
        {
             int result;
            string storeProcedure = String.Format("{0}", "UpdateOplDatosObligatorios");
            using (var connection = CreateConnection())
            {
                result = connection.QueryFirstOrDefault<int>(storeProcedure, new
                {
                  IdOperacionLogistica = id
                , OrdenServicio				= OrdenServicio
                , Transporte			    = Transporte
                , CanalSini					= CanalSini
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();   
        }

        public int DA_Confirmar_OPL(int idOperacionLogistica)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_CONFIRMAR_OPL");
            using (var connection = CreateConnection())
            {
                result = connection.Execute(storeProcedure, param : new
                {
                    IdOperacionLogistica = idOperacionLogistica
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result;
        }

        public async Task<object> DatosParaCorreoOPL(string code)
        {
            string nombreStoreProcedure = "USP_DatosParaCorreoOPL";
            IEnumerable<DatosCorreoOPL> list = new List<DatosCorreoOPL>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<DatosCorreoOPL>(nombreStoreProcedure, new
                {
                    code = code
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

    }
}