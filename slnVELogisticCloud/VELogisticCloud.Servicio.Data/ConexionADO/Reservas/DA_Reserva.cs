﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Booking;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Reservas
{
    public class DA_Reserva : BaseRepository
    {
        #region Conexion
        public DA_Reserva(IConfiguration configuration)
          : base(configuration)
        { }
        #endregion

        public async Task<object> Delete_Reserva(decimal id)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_ELIMINAR_RESERVA");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    id = id
             
                },
                    commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        //public object Delete_Reserva(decimal id)
        //{
        //   return Convert.ToString(oCon.EjecutarEscalar("USP_ELIMINAR_RESERVA", id));
        //}

        //public object ReservexID(decimal id, string commodity, string naviera, string destino, string puertoSalida,decimal idestado,string Nro_Booking)
        //{
        //        List<Ent_Reserva> lcVacaciones = new List<Ent_Reserva>();
        //        using (IDataReader dr = oCon.ejecutarDataReader("USP_LISTA_RESERVAS", id, commodity, naviera, destino.Trim(), puertoSalida.Trim(), Nro_Booking.Trim(), idestado))
        //        {
        //            if (dr != null)
        //            {
        //                while (dr.Read())
        //                {
        //                    Ent_Reserva citinerario = new Ent_Reserva();

        //                    Type _type = citinerario.GetType();
        //                    System.Reflection.PropertyInfo[] lProperty = _type.GetProperties();

        //                    foreach (System.Reflection.PropertyInfo propiedad in lProperty)
        //                    {
        //                        String pType = propiedad.PropertyType.Name;
        //                        String pName = propiedad.Name;

        //                        if (dr[pName].GetType().Name != "DBNull")
        //                        {
        //                            propiedad.SetValue(citinerario, dr[pName]);
        //                        }
        //                    }
        //                    lcVacaciones.Add(citinerario);
        //                }
        //            }
        //        }
        //        return lcVacaciones;
        //}


        //public object Resumen(decimal id)
        //{
        //        List<Resumen> lsta = new List<Resumen>();
        //        using (IDataReader dr = oCon.ejecutarDataReader("USP_RESUMEN_RESERVA", id))
        //        {
        //            if (dr != null)
        //            {
        //                while (dr.Read())
        //                {
        //                    Resumen ent = new Resumen();

        //                    Type _type = ent.GetType();
        //                    System.Reflection.PropertyInfo[] lProperty = _type.GetProperties();

        //                    foreach (System.Reflection.PropertyInfo propiedad in lProperty)
        //                    {
        //                        String pType = propiedad.PropertyType.Name;
        //                        String pName = propiedad.Name;

        //                        if (dr[pName].GetType().Name != "DBNull")
        //                        {
        //                            propiedad.SetValue(ent, dr[pName]);
        //                        }
        //                    }
        //                    lsta.Add(ent);
        //                }
        //            }
        //        }
        //        return lsta;
        //}


        //public object InsertReserva(Ent_Reserva ent)
        //{
        //        return Convert.ToString(oCon.EjecutarEscalar("USP_INSERTAR_RESERVA", ent.Naviera
        //            ,ent.Nro_contrato
        //            ,ent.Expeditor
        //            ,ent.Consignatario
        //            ,ent.Puerto_Embarque
        //            ,ent.Puerto_Descarga
        //            ,ent.ETD
        //            ,ent.ETA
        //            ,ent.Nombre_Nave
        //            ,ent.Nave_Viaje
        //            ,ent.Cantidad
        //            ,ent.Tipo_Contenedor
        //            , ent.Codigo_Commodity
        //            , ent.Commodity
        //            ,ent.Descripcion_Carga
        //            ,ent.Temperatura
        //            ,ent.Ventilacion
        //            ,ent.Humedad
        //            ,ent.Peso
        //            ,ent.Volumen
        //            ,ent.Flete
        //            ,ent.Emision_BL

        //            ,ent.Id_Naviera
        //            ,ent.Id_Contenedor
        //            ,ent.Codigo_Nave
        //            ,ent.CallSign_Nave
        //            , ent.Codigo_Puerto_Embarque
        //            , ent.Codigo_Pais_Embarque
        //            , ent.Codigo_Puerto_Descarga
        //            , ent.Codigo_Pais_Descarga
        //            , ent.Codigo_Expeditor
        //            , ent.Direccion_Expeditor
        //            , ent.Codigo_Consignatario
        //            , ent.Direccion_Consignatario
        //            , ent.Pais_Puerto_Descarga
        //            , ent.Pais_Puerto_Embarque
        //            , ent.IdUsuarioCreacion

        //            , ent.Tipo
        //            , ent.Condicion
        //            , ent.ColdTreatment
        //            , ent.TipoTemperatura
        //            , ent.TipoVentilacion
        //            , ent.TipoHumedad
        //            , ent.Atmosfera
        //            , ent.CO2
        //            , ent.O2
        //            , ent.ID_IMO
        //            , ent.IMO
        //            , ent.UN1
        //            , ent.UN2
        //            , ent.NombrePagador
        //            , ent.DireccionPagador
        //            ,ent.Id_Puerto_Carga
        //            ,ent.Id_Puerto_Descarga
        //            ,ent.Codigo_Usuario
        //            ,ent.Id_Cotizacion_Maritimo_Detalle
        //            ,ent.Nota   
        //            ));
        //}

        public object UpdateReserva(int id,Ent_Reserva ent)
        {
            return null;
            //        return Convert.ToString(oCon.EjecutarEscalar("USP_ACTUALIZAR_RESERVA", id
            //            , ent.Naviera
            //            , ent.Nro_contrato
            //            , ent.Expeditor
            //            , ent.Consignatario
            //            , ent.Puerto_Embarque
            //            , ent.Puerto_Descarga
            //            , ent.ETD
            //            , ent.ETA
            //            , ent.Nombre_Nave
            //            , ent.Nave_Viaje
            //            , ent.Cantidad
            //            , ent.Tipo_Contenedor
            //            , ent.Codigo_Commodity
            //            , ent.Commodity
            //            , ent.Descripcion_Carga
            //            , ent.Temperatura
            //            , ent.Ventilacion
            //            , ent.Humedad
            //            , ent.Peso
            //            , ent.Volumen
            //            , ent.Flete
            //            , ent.Emision_BL

            //            , ent.Id_Naviera
            //            , ent.Id_Contenedor
            //            , ent.Codigo_Nave
            //            , ent.CallSign_Nave
            //            , ent.Codigo_Puerto_Embarque
            //            , ent.Codigo_Pais_Embarque
            //            , ent.Codigo_Puerto_Descarga
            //            , ent.Codigo_Pais_Descarga
            //            , ent.Codigo_Expeditor
            //            , ent.Direccion_Expeditor
            //            , ent.Codigo_Consignatario
            //            , ent.Direccion_Consignatario
            //            , ent.Pais_Puerto_Descarga
            //            , ent.Pais_Puerto_Embarque
            //            , ent.IdUsuarioCreacion

            //            ,ent.Tipo
            //            ,ent.Condicion
            //            ,ent.ColdTreatment
            //            ,ent.TipoTemperatura
            //            ,ent.TipoVentilacion
            //            ,ent.TipoHumedad
            //            ,ent.Atmosfera
            //            ,ent.CO2
            //            ,ent.O2
            //            ,ent.ID_IMO
            //            ,ent.IMO
            //            ,ent.UN1
            //            ,ent.UN2
            //            ,ent.NombrePagador
            //            ,ent.DireccionPagador
            //            ,ent.Id_Puerto_Carga
            //            ,ent.Id_Puerto_Descarga
            //            ));
        }

       
        //public object BookingTOoddo(int id)
        //{
        //        List<BookingTOoddo> lsta = new List<BookingTOoddo>();
        //        using (IDataReader dr = oCon.ejecutarDataReader("USP_DatosParaBooking", id ))
        //        {
        //            if (dr != null)
        //            {
        //                while (dr.Read())
        //                {
        //                    BookingTOoddo ent = new BookingTOoddo();

        //                    Type _type = ent.GetType();
        //                    System.Reflection.PropertyInfo[] lProperty = _type.GetProperties();

        //                    foreach (System.Reflection.PropertyInfo propiedad in lProperty)
        //                    {
        //                        String pType = propiedad.PropertyType.Name;
        //                        String pName = propiedad.Name;

        //                        if (dr[pName].GetType().Name != "DBNull")
        //                        {
        //                            propiedad.SetValue(ent, dr[pName]);
        //                        }
        //                    }
        //                    lsta.Add(ent);
        //                }
        //            }
        //        }
        //        return lsta;
        //}


        #region CustomerService

        //public object Confirmar_booking(decimal id)
        //{
        //        return Convert.ToString(oCon.EjecutarEscalar("USP_CONFIRMAR_RESERVA", id));
        //}

        //public object Estado_Envio_Oddo_booking(decimal id)
        //{
        //        return Convert.ToString(oCon.EjecutarEscalar("USP_ENVIO_ODOO_RESERVA", id));
        //}
        
        //public object rechazar_booking(decimal id)
        //{
        //        return Convert.ToString(oCon.EjecutarEscalar("USP_RECHAZAR_BOOKING", id));
        //}
        #endregion

        public async Task<object> ListaNavierasOdooVE()
        {
            IEnumerable<Ent_Navieras_Oddo_VE> list = new List<Ent_Navieras_Oddo_VE>();
            string storeProcedure = String.Format("{0}", "USP_Lista_Navieras_ODOO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Ent_Navieras_Oddo_VE>(storeProcedure, new
                {
                },
                    commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        //public object ListaNavierasOdooVE()
        //{
        //        List<Ent_Navieras_Oddo_VE> lsta = new List<Ent_Navieras_Oddo_VE>();
        //        using (IDataReader dr = oCon.ejecutarDataReader("USP_Lista_Navieras_ODOO"))
        //        {
        //            if (dr != null)
        //            {
        //                while (dr.Read())
        //                {
        //                    Ent_Navieras_Oddo_VE ent = new Ent_Navieras_Oddo_VE();

        //                    Type _type = ent.GetType();
        //                    System.Reflection.PropertyInfo[] lProperty = _type.GetProperties();

        //                    foreach (System.Reflection.PropertyInfo propiedad in lProperty)
        //                    {
        //                        String pType = propiedad.PropertyType.Name;
        //                        String pName = propiedad.Name;

        //                        if (dr[pName].GetType().Name != "DBNull")
        //                        {
        //                            propiedad.SetValue(ent, dr[pName]);
        //                        }
        //                    }
        //                    lsta.Add(ent);
        //                }
        //            }
        //        }
        //        return lsta;
        //}

    }
}
