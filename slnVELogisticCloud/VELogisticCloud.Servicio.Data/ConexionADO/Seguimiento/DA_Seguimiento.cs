﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Seguimiento;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Seguimiento
{
    public class DA_Seguimiento : BaseRepository
    {
        public DA_Seguimiento(IConfiguration configuration)
       : base(configuration)
        { }
        public async Task<object> SeguimientoReserva(int id)
        {
            string nombreStoreProcedure = "USP_SeguimientoReserva";
            IEnumerable<SeguimientoReserva> list = new List<SeguimientoReserva>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<SeguimientoReserva>(nombreStoreProcedure, new
                {
                    id = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> SeguimientoOPL(int id)
        {
            string nombreStoreProcedure = "USP_SeguimientoOperacionLogistica";
            IEnumerable<SeguimientoOPL> list = new List<SeguimientoOPL>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<SeguimientoOPL>(nombreStoreProcedure, new
                {
                    id = id
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
    }
}
