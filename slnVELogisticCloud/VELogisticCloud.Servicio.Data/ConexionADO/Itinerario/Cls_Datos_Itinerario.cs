﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models;
using VELogisticCloud.Models.General;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Itinerario
{
    public class Cls_Datos_Itinerario : BaseRepository
    {
        public Cls_Datos_Itinerario(IConfiguration configuration)
         : base(configuration)
        { }
        public async Task<object>  ListaItinerarioManual()
        {
            string nombreStoreProcedure = "USP_ItinerarioManual";
            IEnumerable<BE_ItinerarioManual> list = new List<BE_ItinerarioManual>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_ItinerarioManual>(nombreStoreProcedure, new
                {
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        
        public  void BulkInsert(List<BE_ItinerarioManual> ent)
        {
            BE_ItinerarioManual be = new BE_ItinerarioManual();
            DataTable table = new DataTable();
            table.TableName = "ItinerarioManual";

            //table.Columns.Add(nameof(be.IdItinerarioManual), typeof(int));
            table.Columns.Add(nameof(be.scac), typeof(string));
            table.Columns.Add(nameof(be.carrierName), typeof(string));
            table.Columns.Add(nameof(be.terminalCutoff), typeof(string));
            table.Columns.Add(nameof(be.vesselName), typeof(string));
            table.Columns.Add(nameof(be.voyageNumber), typeof(string));
            table.Columns.Add(nameof(be.imoNumber), typeof(string));
            table.Columns.Add(nameof(be.originUnloc), typeof(string));
            table.Columns.Add(nameof(be.originCityName), typeof(string));
            table.Columns.Add(nameof(be.originSubdivision), typeof(string));
            table.Columns.Add(nameof(be.originCountry), typeof(string));
            table.Columns.Add(nameof(be.originDepartureDate), typeof(string));
            table.Columns.Add(nameof(be.destinationUnloc), typeof(string));
            table.Columns.Add(nameof(be.destinationCityName), typeof(string));
            table.Columns.Add(nameof(be.destinationCountry), typeof(string));
            table.Columns.Add(nameof(be.destinationArrivalDate), typeof(string));
            table.Columns.Add(nameof(be.totalDuration), typeof(string));
            table.Columns.Add(nameof(be.legs), typeof(string));
            table.Columns.Add(nameof(be.sequence), typeof(string));
            table.Columns.Add(nameof(be.serviceName), typeof(string));
            table.Columns.Add(nameof(be.transportName), typeof(string));
            table.Columns.Add(nameof(be.conveyanceNumber), typeof(string));
            table.Columns.Add(nameof(be.departureCityName), typeof(string));
            table.Columns.Add(nameof(be.departureCountry), typeof(string));
            table.Columns.Add(nameof(be.departureDate), typeof(string));
            table.Columns.Add(nameof(be.arrivalCityName), typeof(string));
            table.Columns.Add(nameof(be.arrivalCountry), typeof(string));
            table.Columns.Add(nameof(be.arrivalDate), typeof(string));
            table.Columns.Add(nameof(be.transitDuration), typeof(string));

            foreach (var person in ent)
            {
                var row = table.NewRow();
                
                //row[nameof(be.IdItinerarioManual)] = 0;
                row[nameof(be.scac)] = person.scac.ToString();
                row[nameof(be.carrierName)] = person.carrierName;
                row[nameof(be.terminalCutoff)] = person.terminalCutoff;
                row[nameof(be.vesselName)] = person.vesselName;
                row[nameof(be.voyageNumber)] = person.voyageNumber;
                row[nameof(be.imoNumber)] = person.imoNumber;
                row[nameof(be.originUnloc)] = person.originUnloc;
                row[nameof(be.originCityName)] = person.originCityName;
                row[nameof(be.originSubdivision)] = person.originSubdivision;
                row[nameof(be.originCountry)] = person.originCountry;
                row[nameof(be.originDepartureDate)] = person.originDepartureDate;
                row[nameof(be.destinationUnloc)] = person.destinationUnloc;
                row[nameof(be.destinationCityName)] = person.destinationCityName;
                row[nameof(be.destinationCountry)] = person.destinationCountry;
                row[nameof(be.destinationArrivalDate)] = person.destinationArrivalDate;
                row[nameof(be.totalDuration)] = person.totalDuration;
                row[nameof(be.legs)] = person.legs;
                row[nameof(be.sequence)] = person.sequence;
                row[nameof(be.serviceName)] = person.serviceName;
                row[nameof(be.transportName)] = person.transportName;
                row[nameof(be.conveyanceNumber)] = person.conveyanceNumber;
                row[nameof(be.departureCityName)] = person.departureCityName;
                row[nameof(be.departureCountry)] = person.departureCountry;
                row[nameof(be.departureDate)] = person.departureDate;
                row[nameof(be.arrivalCityName)] = person.arrivalCityName;
                row[nameof(be.arrivalCountry)] = person.arrivalCountry;
                row[nameof(be.arrivalDate)] = person.arrivalDate;
                row[nameof(be.transitDuration)] = person.transitDuration;
                table.Rows.Add(row);
            }

            using(SqlConnection sqlConnection = new SqlConnection(CreateConnection().ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand("DELETE ItinerarioManual", sqlConnection);
                int result = command.ExecuteNonQuery();
            }

            //SqlConnection sqlConnection = new SqlConnection(CreateConnection().ConnectionString);
            //SqlCommand command = new SqlCommand("DELETE ItinerarioManual", sqlConnection);
            //int result = command.ExecuteNonQuery();
            using (var bulkInsert = new SqlBulkCopy(CreateConnection().ConnectionString))
            {
                bulkInsert.DestinationTableName = table.TableName;
                bulkInsert.WriteToServer(table);
            }
        } 
    }
}
