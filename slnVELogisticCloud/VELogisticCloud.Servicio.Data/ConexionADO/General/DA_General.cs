﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.General;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.Servicio.Data.ConexionADO.General
{
    public class DA_General :BaseRepository
    {
        public DA_General(IConfiguration configuration)
        : base(configuration)
        { }
        public async Task<object> ListaValores(string grupo)
        {
            string nombreStoreProcedure = "USP_ListaValores";
            IEnumerable<BE_Valores> list = new List<BE_Valores>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Valores>(nombreStoreProcedure, new
                {
                    Grupo = grupo

                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }


    }
}
