﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using static VELogisticCloud.Models.Proyecciones;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Proyecciones
{
    public class Da_Proyecciones:BaseRepository
    {
        public Da_Proyecciones(IConfiguration configuration)
        : base(configuration)
        { }

        public async Task<object> InsertCabecera(Cabecera_Proyecciones ent,string UserCreacion)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_INSERTAR_PROYECCION");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Campana = ent.Campania,
                    codigoCommodity = ent.Codigo_Commodity,
                    Commodity = ent.Commodity,
                    codigoOrigen  = ent.Codigo_Origen,
                    Origen=  ent.Origen,
                    codigoDestino = ent.Codigo_Destino,
                    Destino = ent.Destino ,
                    codigoTipo = ent.Codigo_Tipo,
                    Tipo = ent.Tipo,
                    UserCreacion = UserCreacion
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> InsertDetalle(Detalle_Proyecciones ent, string UserCreacion)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_INSERTAR_PROYECCION_DETALLE");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    S1 = ent.S1,
                    S2 = ent.S2,
                    S3 = ent.S3,
                    S4 = ent.S4,
                    S5 = ent.S5,
                    S6 = ent.S6,
                    S7 = ent.S7,
                    S8 = ent.S8,
                    S9 = ent.S9,
                    S10 = ent.S10,
                    S11 = ent.S11,
                    S12 = ent.S12,
                    S13 = ent.S13,
                    S14 = ent.S14,
                    S15 = ent.S15,
                    S16 = ent.S16,
                    S17 = ent.S17,
                    S18 = ent.S18,
                    S19 = ent.S19,
                    S20 = ent.S20,
                    S21 = ent.S21,
                    S22 = ent.S22,
                    S23 = ent.S23,
                    S24 = ent.S24,
                    S25 = ent.S25,
                    S26 = ent.S26,
                    S27 = ent.S27,
                    S28 = ent.S28,
                    S29 = ent.S29,
                    S30 = ent.S30,
                    S31 = ent.S31,
                    S32 = ent.S32,
                    S33 = ent.S33,
                    S34 = ent.S34,
                    S35 = ent.S35,
                    S36 = ent.S36,
                    UserCreacion = UserCreacion
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
           
        }
        public async Task<object> UpdateDetalle(Detalle_Proyecciones ent, string UserUpdate)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_UPDATE_PROYECCION_DETALLE");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Id_Proyeccion = ent.Id_Proyeccion,
                    S1 = ent.S1,
                    S2 = ent.S2,
                    S3 = ent.S3,
                    S4 = ent.S4,
                    S5 = ent.S5,
                    S6 = ent.S6,
                    S7 = ent.S7,
                    S8 = ent.S8,
                    S9 = ent.S9,
                    S10 = ent.S10,
                    S11 = ent.S11,
                    S12 = ent.S12,
                    S13 = ent.S13,
                    S14 = ent.S14,
                    S15 = ent.S15,
                    S16 = ent.S16,
                    S17 = ent.S17,
                    S18 = ent.S18,
                    S19 = ent.S19,
                    S20 = ent.S20,
                    S21 = ent.S21,
                    S22 = ent.S22,
                    S23 = ent.S23,
                    S24 = ent.S24,
                    S25 = ent.S25,
                    S26 = ent.S26,
                    S27 = ent.S27,
                    S28 = ent.S28,
                    S29 = ent.S29,
                    S30 = ent.S30,
                    S31 = ent.S31,
                    S32 = ent.S32,
                    S33 = ent.S33,
                    S34 = ent.S34,
                    S35 = ent.S35,
                    S36 = ent.S36,
                    UserCreacion = UserUpdate
                },
                  commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }
        public async Task<object> Lista_Proyecciones(string fecha, string  Cod_commodity, string cod_destino)
        {
            string nombreStoreProcedure = "USP_LISTA_PROYECCIONES";
            IEnumerable<Lista_Proyecciones> list = new List<Lista_Proyecciones>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<Lista_Proyecciones>(nombreStoreProcedure, new
                {
                    FECHA_CREATE = fecha,
                    COD_COMIDITY = Cod_commodity,
                    COD_DESTINO = cod_destino,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
        public async Task<object> ViewDatos(decimal id)
        {
            string nombreStoreProcedure = "USP_LISTAR_PROYECCION_DETALLE";
            IEnumerable<PROYECCION_DETALLE> list = new List<PROYECCION_DETALLE>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<PROYECCION_DETALLE>(nombreStoreProcedure, new
                {
                    ID = id,
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }


    }
}
