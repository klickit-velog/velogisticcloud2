﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace VELogisticCloud.Servicio.Data.ConexionADO
{
    public class BaseRepository
    {
        private readonly IConfiguration configuration;

        protected BaseRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected IDbConnection CreateConnection()
        {
            return new SqlConnection(configuration.GetConnectionString("AppDbContextConnection"));
        }
    }
}
