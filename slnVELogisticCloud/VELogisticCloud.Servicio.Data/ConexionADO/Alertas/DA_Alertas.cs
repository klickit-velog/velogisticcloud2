﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.Alertas;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Alertas
{
    public class DA_Alertas : BaseRepository
    {
        //#region Conexion
        //private Conexion.Conexion oCon;
        //public DA_Alertas()
        //{
        //    oCon = new Conexion.Conexion(1);
        //}
        //#endregion

        public DA_Alertas(IConfiguration configuration)
          : base(configuration)
        { }

        public async Task<object> Lista_Alertas()
        {
            IEnumerable<BE_Alertas_Contrato> list = new List<BE_Alertas_Contrato>();
            string procedure = "USP_LISTA_ALERTAS";
            try
            {
                using (var connection = CreateConnection())
                {
                    list = await connection.QueryAsync<BE_Alertas_Contrato>
                                (procedure, new {  }, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return list;
        }

        public async Task<object> MarcarLeido(int id)
        {
            IEnumerable<int> list = new List<int>();
            string storeProcedure = String.Format("{0}", "USP_MARCAR_LEIDO_ALERTAS");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<int>(storeProcedure, new
                {
                    ID = id
                },
                    commandType: CommandType.StoredProcedure);
            }
            return 1;
            //return Convert.ToString(oCon.EjecutarEscalar("USP_MARCAR_LEIDO_ALERTAS", id));
        }

        public async Task<object> InsertarDataAlertas(BE_Alertas_Contrato ent)
        {
            IEnumerable<int> list = new List<int>();
            string storeProcedure = String.Format("{0}", "USP_MARCAR_LEIDO_ALERTAS");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<int>(storeProcedure, new
                {
                    NroContrato = ent.NroContrato,
                    Tipo = ent.Tipo,
                    Servicio = ent.Servicio,
                    bTipo = ent.bTipo,
                    IdContrato = ent.IdContrato,
                    FechaIni = ent.FechaIni,
                    FechaFin = ent.FechaFin,
                    Dias = ent.DiasVencimiento,
                    Linea = ent.Linea,

                },
                    commandType: CommandType.StoredProcedure);
            }
            return 1;
        }
    }
}
