﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.General;

namespace VELogisticCloud.Servicio.Data.ConexionADO
{
    public class Da_Variables:BaseRepository
    {
        public Da_Variables(IConfiguration configuration)
        : base(configuration)
        { }
        public async Task<object> UpdateVariable(int Activado,string Usuario, string Codigo)
        {
            int result;
            string storeProcedure = String.Format("{0}", "USP_CrudVariables;3");
            using (var connection = CreateConnection())
            {
                result = await connection.QueryFirstOrDefaultAsync<int>(storeProcedure, new
                {
                    Activado = Activado,
                    Usuario = Usuario,
                    Codigo = Codigo
                },
                    commandType: CommandType.StoredProcedure);
            }
            return result.ToString();
        }

        public async Task<object> ListaVariables(string Codigo)
        {
            IEnumerable<BE_Variables> list = new List<BE_Variables>();
            string storeProcedure = String.Format("{0}", "USP_CrudVariables;2");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Variables>(storeProcedure, new
                {
                    Codigo = Codigo
                },
                    commandType: CommandType.StoredProcedure);
            }
            return list;
        }
    }
}
