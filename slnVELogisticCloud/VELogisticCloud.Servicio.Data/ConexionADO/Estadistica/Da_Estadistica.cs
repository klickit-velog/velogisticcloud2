﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Models.Estadistica;

namespace VELogisticCloud.Servicio.Data.ConexionADO.Estadistica
{
    public class Da_Estadistica : BaseRepository
    {
        public Da_Estadistica(IConfiguration configuration)
          : base(configuration)
        { }
        public async Task<object> DatosEstadisticaCotizacion(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_ESTADISTICA_COTIZACION";
            IEnumerable<BE_Estadistica_Cotizacion> list = new List<BE_Estadistica_Cotizacion>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Estadistica_Cotizacion>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario
                  
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
            
        }

        public async Task<object> DatosEstadisticaReserva(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_ESTADISTICA_RESERVA";
            IEnumerable<BE_Estadistica_Reservas> list = new List<BE_Estadistica_Reservas>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Estadistica_Reservas>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario

                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> DatosEstadisticaReservaInttra(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_ESTADISTICA_RESERVA_INTTRA";
            IEnumerable<BE_Estadistica_Reservas_Inttra> list = new List<BE_Estadistica_Reservas_Inttra>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Estadistica_Reservas_Inttra>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario

                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> DatosEstadisticaCotizacionLogistica(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_ESTADISTICA_COTIZACION_LOGISTICA";
            IEnumerable<BE_Estadistica_Cotizacion_Logistica> list = new List<BE_Estadistica_Cotizacion_Logistica>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Estadistica_Cotizacion_Logistica>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> DatosEstadisticaOperacionLogistica(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_ESTADISTICA_OPL";
            IEnumerable<BE_Estadistica_Opl> list = new List<BE_Estadistica_Opl>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Estadistica_Opl>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }

        public async Task<object> DatosEstadisticaOperacionLogisticaOddo(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_ESTADISTICA_OPL_ODDO";
            IEnumerable<BE_Estadistica_OPL_Oddo> list = new List<BE_Estadistica_OPL_Oddo>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_Estadistica_OPL_Oddo>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;

        }

        public async Task<object> ResumenBooking(string IdUsuario)
        {
            string nombreStoreProcedure = "USP_RESUMEN_BOOKING";
            IEnumerable<BE_RESUMEN_BOOKING> list = new List<BE_RESUMEN_BOOKING>();
            //string storeProcedure = String.Format("{0}", "USP_DETALLE_COTIZACION_MARITIMO");
            using (var connection = CreateConnection())
            {
                list = await connection.QueryAsync<BE_RESUMEN_BOOKING>(nombreStoreProcedure, new
                {
                    IdUsuario = IdUsuario
                },
                commandType: CommandType.StoredProcedure);
            }
            return list;
        }
    }
}

