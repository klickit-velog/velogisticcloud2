﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.Servicio.Data
{
    public class NavieraRepository : Repository<Naviera>, INavieraRepository
    {
        private readonly VECloudDBContext _db;
        public NavieraRepository(VECloudDBContext db) : base(db)
        {
            _db = db;
        }
        public IEnumerable<SelectListItem> GetListNaviera()
        {
            return _db.Naviera.Select(a => new SelectListItem()
            {
                Text = a.Nombre,
                Value = a.IdNaviera.ToString()
            });
        }

        public void Update(Naviera naviera)
        {
            var objDesdeDb = _db.Naviera.FirstOrDefault(a => a.IdNaviera == naviera.IdNaviera);
            objDesdeDb.Nombre = naviera.Nombre;
            objDesdeDb.Descripcion = naviera.Descripcion;

            _db.SaveChanges();
        }
    }
}