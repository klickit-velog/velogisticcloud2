﻿using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.Repository;
using VELogisticCloud.Servicio.Data.Seguridad;

namespace VELogisticCloud.Servicio.Data
{
    public class ContenedorTrabajo : IContenedorTrabajo
    {
        private readonly VECloudDBContext _context;
        public IPuertoRepository Puerto { get; private set; }
        public IPerfilRepository Perfil { get; private set; }

        public UsuarioRepository Usuario { get; private set; }

        public ContenedorTrabajo(VECloudDBContext context)
        {
            _context = context;
            Puerto = new PuertoRepository(_context);
            Perfil = new PerfilRepository(_context);
            Usuario = new UsuarioRepository(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
