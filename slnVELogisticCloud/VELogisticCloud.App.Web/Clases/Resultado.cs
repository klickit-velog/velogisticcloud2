﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VELogisticCloud.App.Web.Clases
{
    public class Resultado
    {
        public int result { get; set; }
        /// <summary>
        /// Propiedad para personalizar el mensaje a enviar al usuario desde la controladora.
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// Lista de objetos serializados para su manipulacion con Jquery
        /// </summary>
        public JsonResult data { get; set; }

        /// <summary>
        /// Valor para cualquier dato de retorno tipo scalar.
        /// </summary>
        public int count { get; set; }
        public string valor { get; set; }

        public string valor2 { get; set; }

        public string valor3 { get; set; }
        public string valor4 { get; set; }

        public string valor5 { get; set; }
        public string valor6 { get; set; }
        public string valor7 { get; set; }
        public string valor8 { get; set; }

        public string valor9 { get; set; }
        public string valor10 { get; set; }
        public string valor11 { get; set; }
        public string valor12 { get; set; }
        public string valor13 { get; set; }
        public string valor14 { get; set; }
        public string valor15 { get; set; }
        public object objeto { get; set; }
        public object objeto2 { get; set; }
        public object objeto3 { get; set; }

        /// <summary>
        /// Propiedad agregada para la Integracion con SIS SEGURIDAD
        /// isRedirect: indicador si se debe redireccionar al login la pagina.
        /// </summary>
        public bool isRedirect { get; set; }
        /// <summary>
        /// Propiedad agregada para la Integracion con SIS SEGURIDAD
        /// redirectUrl: Url a donde se redireccionara la pagina.
        /// </summary>
        public string redirectUrl { get; set; }
        public int nTipoMensaje { get; set; }
        public string sMensaje { get; set; }
        public int nValorEntero { get; set; }
        public string nValorString { get; set; }
    }

    public enum TipoMensaje
    {
        Error,
        Correcto,
        Informativo
    }

}
