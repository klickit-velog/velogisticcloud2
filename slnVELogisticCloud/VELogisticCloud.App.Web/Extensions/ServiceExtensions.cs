﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VELogisticCloud.CrossCutting.Mail;
//using VELogisticCloud.Servicio.Business.Seguridad;
using VELogisticCloud.Servicio.Data;

namespace VELogisticCloud.App.Web.Extensions
{
    //TODO: REVISAR SI SE USA ESTA CLASE SINO SE ELIMINA.
    public static class ServiceExtensions
    {
        public static void ConfigureMailSender(this IServiceCollection services)
        {
            services.AddTransient<INetMailSender, NetMailSender>();
        }
        //public static void ConfigureRepository(this IServiceCollection services)
        //{            
        //    services.Add(new ServiceDescriptor(typeof(IEstadoService), typeof(EstadoService), ServiceLifetime.Transient));
        //    services.Add(new ServiceDescriptor(typeof(IUsuarioService), typeof(UsuarioService), ServiceLifetime.Transient));            
        //}

        //public static void ConfigureEF(this IServiceCollection services, IConfiguration configuration)
        //{
        //    services.AddDbContext<VELogisticsCloudDBContext>(builder =>
        //    {
        //        builder.UseSqlServer(configuration["ConnectionStrings:AppDbContextConnection"]);
        //    });
        //}
    }
}
