﻿$(document).ready(function () {
    hideSpinner();

    $('#btnConsultar').click(function () {
        Consultar();
    });

    document.getElementById("link_Usuarios").style.color = "black";
    document.getElementById("link_Usuarios").style.fontWeight = "bold";
    
});

function Consultar() {

    var DTO = {
        "email": $("#email").val(),
        "nombre": $("#nombre").val(),
        "razonSocial": $("#razonSocial").val(),
        "idEstado": $("#idEstado").val() === '-1' ? '' : $("#idEstado").val()
    };
    $("#usuario-datatable").DataTable({
        responsive: false,
        destroy: true,
        "processing": true,
        "serverSide": true,
        "filter": false,
        "ordering": false,
        "language": {
            "processing": '<div class="spinner-border"></div> Procesando...',
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "ajax": {
            "url": "/Usuario/ListarUsuario",
            "type": "POST",
            "data": DTO,
            "datatype": "json"
        },
        "columns": [
            { "data": "nombre", "name": "Nombre", "autoWidth": true },
            { "data": "razonSocial", "name": "RazonSocial", "autoWidth": true },
            { "data": "ruc", "name": "RUC", "autoWidth": true },
            { "data": "email", "name": "Email", "autoWidth": true },
            { "data": "nombreRol", "name": "Rol", "autoWidth": true },
            {
                "render": function (data, type, full, meta) {
                    return '<a href="Identity/Account/Admin?Id=' + full.idUsuario + '" class="btn btn-block btn-outline-warning btn-sm"><i class="fas fa-edit"></i></a >';
                }, "name": "Editar", "autoWidth": true
            }
        ]
    });

}