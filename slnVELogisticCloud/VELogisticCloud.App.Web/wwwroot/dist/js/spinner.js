﻿
const UsuarioMenu = localStorage.getItem('UsuarioMenu');
const correoUsuario = localStorage.getItem('correoUsuario');
const sumLocal = localStorage.getItem('sum');

document.getElementById('span_UsuarioMenu').innerHTML = UsuarioMenu;
document.getElementById('span_correoUsuario').innerHTML = correoUsuario;
document.getElementById('txt_alertas').innerHTML = sumLocal;

function showSpinner() {
    showSpinner2();
}

async function hideSpinner() {
    hideSpinner2();
}

async function ReiniciarClaims() {
    const result = await $.ajax({
        url: '../CustomerCliente/ReiniciarClaims',
        type: 'POST',
        async: false,
        //data: { id: id },
        beforeSend: function () { },
        success: function (response) {
        }
    });
    return result;
}


function showSpinner2() {
    console.log('spinner should be visible 2');
    
    const ob = document.getElementById("contenedor_spinner2");
    //ob.classList.add("loader");

    ob.classList.add("loading");
    ob.classList.add("show"); 
    //ob.classList.add("spin");
}

 function hideSpinner2() {
    console.log('spinner should disappear 2');
    var ob = document.getElementById("contenedor_spinner2");

     //ob.classList.remove("loader");

     ob.classList.remove("loading");
     ob.classList.remove("show");
     //ob.classList.remove("spin");

}

