﻿
$(document).ready(function () {
    MisAlertas();
    hideSpinner2();

});


function MisAlertas() {

    showSpinner2();

    $.ajax({
        url: '../Alertas/ListaAlertas',
        type: 'POST',
        async: false,
        //data: { param: param },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato.htmlGrid);
            document.getElementById('lbl_opl_packing').innerHTML = dato.verde;
            document.getElementById('lbl_opl_transito_packing').innerHTML = dato.amarillo;
            document.getElementById('lbl_opl_vacios').innerHTML = dato.rojo;
            document.getElementById('lbl_opl_espera').innerHTML = dato.gris;
            document.getElementById('lbl_status2').innerHTML = dato.gris + dato.rojo + dato.amarillo + dato.verde;

            hideSpinner2();

        }
    });
}


function BusquedaAlertas() {
    showSpinner2();

    const idestado = $("#cbo_estados option:selected").val();
    const tipo = $("#cbo_Tipo option:selected").val();
    $.ajax({
        url: '../Alertas/BusquedaAlertas',
        type: 'POST',
        async: true,
        data: { idestado: idestado, tipo: tipo},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato.htmlGrid);
            //document.getElementById('lbl_opl_packing').innerHTML = dato.verde;
            //document.getElementById('lbl_opl_transito_packing').innerHTML = dato.amarillo;
            //document.getElementById('lbl_opl_vacios').innerHTML = dato.rojo;
            //document.getElementById('lbl_opl_espera').innerHTML = dato.gris;
            //document.getElementById('lbl_status2').innerHTML = dato.gris + dato.rojo + dato.amarillo + dato.verde;

            hideSpinner2();

        }
    });

}


function Marcar(id) {
    showSpinner2();

    $.ajax({
        url: '../Alertas/MarcarLeido',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            MisAlertas();

        }
    });


}

function VerFlete(id) {
    alert(id);
}



