﻿
var id = getParameterByName('id');
CargarDatos();

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function Volver() {
    url2 = "../Cotizacion/MisSolicitudes";
    window.open(url2, '_self');
}

function CargarDatos() {
    showSpinner2();

    $.ajax({
        url: '../Cotizacion/DetalleMaritimo',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('pais_inicio').innerHTML = dato.puerto_EmbarqueCabecera; 
            document.getElementById('pais_fin').innerHTML = dato.puerto_DescargaCabecera; 
            document.getElementById('lbl_tipoContenedor2').innerHTML = dato.tipoContenedor;


            document.getElementById('lbl_id_cotizacion').innerHTML = dato.id_Cotizacion_Maritimo; 
            document.getElementById('lbl_id_cotizacion_2').innerHTML = dato.id_Cotizacion_Maritimo; 
            document.getElementById('lbl_Cantidad').innerHTML = dato.cantidad_Contenedor; 
            

            document.getElementById('lbl_codigo').innerHTML = dato.id_Cotizacion_Maritimo;
            document.getElementById('lbl_cliente').innerHTML = dato.cliente;


            document.getElementById('lbl_cargapuerto').innerHTML = dato.puerto_EmbarqueCabecera;
            document.getElementById('lbl_descargapuerto').innerHTML = dato.puerto_DescargaCabecera;
            document.getElementById('lbl_commodity').innerHTML = dato.commodityCabecera;
            document.getElementById('lbl_campania').innerHTML = dato.campania;
            document.getElementById('lbl_tipocontenedor').innerHTML = dato.tipoContenedor;
            document.getElementById('lbl_cantidadcontenedor').innerHTML = dato.cantidad_Contenedor;

            document.getElementById('lbl_ruc').innerHTML = dato.ruc;
            document.getElementById('lbl_correo').innerHTML = dato.codigo_Usuario;
            
            


            TablaDetalle();
            hideSpinner2();
        }
    });
}


function TablaDetalle() {
    $.ajax({
        url: '../Cotizacion/TablaDetalleMaritimo',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
        }
    });
}

