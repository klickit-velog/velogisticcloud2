﻿
var id = getParameterByName('id');
var almacen = getParameterByName('almacen');
DatosParaEditar();

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function Volver() {
    url2 = "../Cotizacion/MisSolicitudesLogistica";
    window.open(url2, '_self');
}

function DatosParaEditar() {
    showSpinner2();
    $.ajax({
        url: '../Cotizacion/DetalleLogistica',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;

            document.getElementById('lbl_pais_inicio').innerHTML = dato.nombrePaisCarga;
            document.getElementById('lbl_pais_fin').innerHTML = almacen;
             
            
            document.getElementById('lbl_nroCotizacion').innerHTML = dato.idCotizacionLogistica;
            document.getElementById('lbl_nroCotizacion2').innerHTML = dato.idCotizacionLogistica;
            document.getElementById('lbl_Cantidad').innerHTML = dato.nroContenedor;
            
            document.getElementById('lbl_embarque').innerHTML = dato.nombrePuertoCarga;
            document.getElementById('lbl_commodity').innerHTML = dato.nombreCommodity;
            //document.getElementById('lbl_servicioEmbarque').innerHTML = dato.servicioEmbarque;
            //document.getElementById('lbl_almacenRetiro').innerHTML = dato.almacen_Retiro;
            //document.getElementById('lbl_fechaCita').innerHTML = dato.fecha_Cita;
            document.getElementById('lbl_almacenPacking').innerHTML = almacen;
            document.getElementById('lbl_campania').innerHTML = dato.nombreCampania;

            
            //document.getElementById('lbl_aduana').innerHTML = dato.servicioAduana;
            //document.getElementById('lbl_conexElectrica').innerHTML = dato.conexionElectrica;
            //document.getElementById('lbl_maritimo').innerHTML = dato.agenciamientoMaritimo;
            //document.getElementById('lbl_generador').innerHTML = dato.generador;
            //document.getElementById('lbl_courier').innerHTML = dato.courier;
            //document.getElementById('lbl_filtro').innerHTML = dato.filtro;
            //document.getElementById('lbl_certificado').innerHTML = dato.certificadoOrigen;
            //document.getElementById('lbl_pago').innerHTML = dato.pagoCPB;
            //document.getElementById('lbl_movili').innerHTML = dato.movilizacionesContenedor;
            //document.getElementById('lbl_aforo').innerHTML = dato.aforoFisico;
            //document.getElementById('lbl_senasa').innerHTML = dato.senasa;
            //document.getElementById('lbl_termografo').innerHTML = dato.termografo;

            //document.getElementById('lbl_transporte').innerHTML = dato.chk_Transporte;
            //document.getElementById('lbl_gateOut').innerHTML = dato.chk_GateOut;
            //document.getElementById('lbl_agenciamientoAduanas').innerHTML = dato.chk_AgenciamientoAduana;
            //document.getElementById('lbl_derechoEmbarque').innerHTML = dato.chk_DerechoEmbarque;
            //document.getElementById('lbl_vistosBuenos').innerHTML = dato.chk_VistoBueno;

            //document.getElementById('lbl_envio').innerHTML = dato.chk_EnvioDocumentos;
            //document.getElementById('lbl_certificado').innerHTML = dato.chk_Certificado;
            //document.getElementById('lbl_movilidad').innerHTML = dato.chk_Movilidad;
            //document.getElementById('lbl_registros').innerHTML = dato.chk_TermoRegistros;
            //document.getElementById('lbl_filtros').innerHTML = dato.chk_FiltrosEtileno;

            //document.getElementById("cbo_Commodity").value = dato.codigoCommodity;
            //document.getElementById("cbo_estado").value = dato.idEstadoOddo;
            //$("#txt_conpectosOp").val(dato.conceptosOPL);
            //$("#txt_canal").val(dato.canal);
            TablaDetalle();
            hideSpinner2();
        }
    });
}


function TablaDetalle() {
    $.ajax({
        url: '../Cotizacion/TablaDetalleLogistica',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
        }
    });
}
