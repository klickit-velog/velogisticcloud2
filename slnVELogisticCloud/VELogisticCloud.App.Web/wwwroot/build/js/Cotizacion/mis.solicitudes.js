﻿
$(document).ready(function () {

    $("#txtPuertoSalida").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDestino").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });

    document.getElementById("link_Solic_Cotizacion").style.color = "black";
    document.getElementById("link_Solic_Cotizacion").style.fontWeight = "bold";

    MisSolicitudes();
    hideSpinner2();

});



function MisSolicitudes() {
    showSpinner2();

    let textoEmbarque = $("#txtPuertoSalida").val();
    let textoDescarga = $("#txtPuertoDestino").val();


    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    //var ValPuertoDestino = $("#txtPuertoDestino option:selected").val();
    //var codigoDestino = ValPuertoDestino.split('-');
    //var ValPuertoSalida = $("#txtPuertoSalida option:selected").val();
    //var codigoOrigen = ValPuertoSalida.split('-');

    var param = {
        cod_coti: $("#txtCodigoCoti").val(),
        Campana: "",
        cod_line_navi: $("#txtNaviera option:selected").val(),
        cod_puerto_destino: $("#hddEmbarqueCodigoPuerto").val(), 
        cod_puerto_salida: $("#hddDescargaCodigoPuerto").val(),
        id_estado: $("#cbo_estado option:selected").val(),
        TipoCotizacion: $("#cboTipoServicio option:selected").val()
    }

    var tipousuario = $("#tipousuario").val();

    $.ajax({
        url: '../Cotizacion/MisSolicitudes',
        type: 'POST',
        async: false,
        data: { param: param, tipousuario: tipousuario },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
            hideSpinner();
        }
    });
    hideSpinner2();

}


$("#cboTipoServicio").change(function () {

    var opcion = $("#cboTipoServicio option:selected").val();
    if (opcion == '1') {
        cod = 1;
        $("#cboTipoServicio option:selected").val('1');
        var url = "../Cotizacion/MisSolicitudes";
        window.open(url, '_self');
    } else if (opcion == '2') {
        cod = 2;
        $("#cboTipoServicio option:selected").val('2');
        var url = "../Cotizacion/MisSolicitudesLogistica";
        window.open(url, '_self');
    } 
    //url2 = "../Itinerario_Solicitar/Resumen";
    //window.open(url2, '_self');

});

function VerDetalle(Codigo_coti) {
    //var url = '@Url.Action("Editar","OperacionLogistica", new {id="' + IdOperacionLogistica+'"})';
    url2 = "../Cotizacion/VerDetalleMaritimo?id=" + Codigo_coti;
    window.open(url2, '_self');
}


function Confirmar_Rechazar_Maritimo(id,estado) {


    var texto = "";
    if (estado === 3) {
        texto = "Confirmar";
    } else {
        texto = "Rechazar";
    }
    Swal.fire({
        title: '¿Esta Seguro de ' + texto + ' la solicitud.?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../Cotizacion/Confirmar_Rechazar_Maritimo',
                type: 'POST',
                async: true,
                data: { id: id, estado: estado },
                beforeSend: function () { },
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Actualizacion Correcta',
                        text: 'Se actualizo correctamente!!',
                    });
                    MisSolicitudes();
                }
            });
           
        } else { }
    })

   

}


function DescargarPDFCotizacion(id) {

    $.ajax({
        url: '../Cotizacion/PDF_CotizacionMaritimo',
        type: 'POST',
        async: true,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato == "") {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Archivo PDF no encontrado!',
                });
            } else {
                let pdfWindow = window.open("")
                pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(dato) + "'></iframe>")

            }

        }
    });
}