﻿contenedor = new Array();

$(document).ready(function () {
    IniciarDatos();
    //$("#cboTipoServicio option:selected").val(1);
    $("#div_termo").css({ "display": "none" });
    $("#div_filtro").css({ "display": "none" });
    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });


    if ($("#txtPuertoDescarga").length)
    {
        $("#txtPuertoDescarga").autocomplete({
            source: "../Maestros/AutoCompletePuertosPais",
            minLength: 2,
            select: function (event, ui) {
                $("#hddDescargaId").val(ui.item.id);
                $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
                $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
                $("#hddDescargaNombrePais").val(ui.item.nombrePais);
                $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
            },
        });
    }
    document.getElementById("link_CotizacionCreate2").style.color = "black";
    document.getElementById("link_CotizacionCreate2").style.fontWeight = "bold";

    //document.getElementById("cbo_TermoRegistro").disabled = true;
    document.getElementById("cbo_Cantidad").disabled = true;
    document.getElementById("cbo_TermoRegistro").disabled = true;
    document.getElementById("cbo_FiltroEtileno").disabled = true;
    document.getElementById("cbo_Cantidad2").disabled = true;

  
    //ListaVehiculos();
});


$("#chk_n_termo").change(function () {

    if ($('#chk_n_termo').is(':checked')) {
        $("#div_termo").css({ "display": "block" });
        document.getElementById("cbo_TermoRegistro").disabled = false;
        document.getElementById("cbo_Cantidad").disabled = false;
    } else {
        $("#div_termo").css({ "display": "none" });
        document.getElementById("cbo_TermoRegistro").disabled = true;
        document.getElementById("cbo_Cantidad").disabled = true;
    }
});


$("#chk_n_filtro").change(function () {

    if ($('#chk_n_filtro').is(':checked')) {
        $("#div_filtro").css({ "display": "block" });
        document.getElementById("cbo_FiltroEtileno").disabled = false;
        document.getElementById("cbo_Cantidad2").disabled = false;

    } else {
        $("#div_filtro").css({ "display": "none" });
        document.getElementById("cbo_FiltroEtileno").disabled = true;
        document.getElementById("cbo_Cantidad2").disabled = true;
    }
});

$("#cboTipoServicio").change(function () {

    var opcion = $("#cboTipoServicio option:selected").val();
    if (opcion == '1') {
        cod = 1;
        $("#cboTipoServicio option:selected").val('1');
        var url = "../Cotizacion/Create";
        window.open(url, '_self');
    } else if (opcion == '2') {
        cod = 2;
        $("#cboTipoServicio option:selected").val('2');
        var url = "../Cotizacion/CotLogistica";
        window.open(url, '_self');
    } else if (opcion == '3') {
        cod = 3;
        $("#cboTipoServicio option:selected").val(3);
        var url = "../Cotizacion/CotTransporte";
        window.open(url, '_self');
    }
    //url2 = "../Itinerario_Solicitar/Resumen";
    //window.open(url2, '_self');

});



function IniciarDatos() {

    //var d = new Date();
    //var dia = d.getDate();
    //var mes = d.getMonth()+1;
    //var anio = d.getFullYear();
    //var hora = d.getHours();
    //var min = d.getMinutes();
    //var seg = d.getSeconds();

    //var codigo = dia.toString() + mes.toString() + anio.toString() + hora.toString() + min.toString() + seg.toString();

 
    $.ajax({
        url: '../Cotizacion/IniciarDatos',
        type: 'POST',
        async: false,
        //data: { ent: ent, contenedor: contenedor },
        beforeSend: function () { },
        success: function (response) {
            var dato = response.split(',');
            var datoname = dato[0];
            var Ruc = dato[1];
            $("#txtCliente").val(datoname);
            $("#txtCodigo").val(Ruc);
            if (Ruc === 'Customer') {
                $('#btnSolicitar').prop('disabled', true);
                $('#AlertaUsuarioCustomer').show();
                $('#btnRegistroLogistica').prop('disabled', true);
            }
            hideSpinner2();
        }
    });

}

function ValidarPuertoEmbarqueYSalida(puerto1, puerto2) {
    if (puerto1 == puerto2) {
            Swal.fire({
                icon: 'info',
                title: 'Oops...',
                text: 'Seleccione diferentes puertos!',
            });
            return '0';

        } else {
            return '1';
        }
    }
function añadirContenedor() {

        //var Embarquecodpais = $("#txtPuertoEmbarque option:selected").attr('codpais');
        //var Embarquepais = $("#txtPuertoEmbarque option:selected").attr('pais');
        var Embarquecodpais = $("#hddEmbarqueCodigoPais").val();
        var Embarquepais = $("#hddEmbarqueNombrePais").val();
        var Region = $("#hddDescargaNombreRegion").val();

        //var Descargacodpais = $("#txtPuertoDescarga option:selected").attr('codpais');
        //var Descargapais = $("#txtPuertoDescarga option:selected").attr('pais');
        var Descargacodpais = $("#hddDescargaCodigoPais").val();
        var Descargapais = $("#hddDescargaNombrePais").val();

        var Cod_line_navi = $("#txtNaviera option:selected").val();
        var Linea_naviera = $("#txtNaviera option:selected").text();

        var Cod_puertoemba = $("#hddEmbarqueCodigoPuerto").val();
        var Carga_puerto = $("#txtPuertoEmbarque").val()

        var Cod_puertodes = $("#hddDescargaCodigoPuerto").val();
        var desc_puerto = $("#txtPuertoDescarga").val()

        var Commodity = $("#txtComodity option:selected").text();
        var Cod_comodity = document.getElementById("txtComodity").value;

        var Id_Puerto_Carga = $("#hddEmbarqueId").val();
        var Id_Puerto_Descarga = $("#hddDescargaId").val();
        var Id_Naviera = $("#hddDescargaId").val();
    

        var TT = "0";
        var Tamano = $("#txtCantidad").val();

        
        //var Val_desde = $("#txtValidoDesde").val();
        //var Val_hasta = $("#txtValidoHasta").val();
    if (Cod_puertoemba === "") {
        Swal.fire({
            icon: 'info',
            title: 'Seleccione Puerto Embarque',
            text: 'Seleccione correctamente el puerto de Embarque.',
        });
        return;
    }
    if (Cod_puertodes === "") {
        Swal.fire({
            icon: 'info',
            title: 'Seleccione Puerto Descarga',
            text: 'Seleccione correctamente el puerto de Descarga.',
        });
        return;
    }

    let result = 0;
    let result2;
    let result3;

    result2 = contenedor.filter(x => x.Codigo_Linea_Naviera === '0' &&
        //x.Linea_Naviera === Linea_naviera && 
        x.Codigo_Puerto_Embarque === Cod_puertoemba &&
        //x.Puerto_Embarque === Carga_puerto &&
        x.Codigo_Puerto_Descarga === Cod_puertodes &&
        //x.Puerto_Descarga === desc_puerto &&
        x.Codigo_Pais_Embarque === Embarquecodpais &&
        //x.Pais_Puerto_Embarque === Embarquepais &&
        x.Codigo_Pais_Descarga === Descargacodpais &&
        //x.Pais_Puerto_Descarga === Descargapais &&
        x.Id_Puerto_Carga === Id_Puerto_Carga &&
        x.Id_Puerto_Descarga === Id_Puerto_Descarga
    );

    if (Cod_line_navi === '0') {
        result3 = contenedor.filter(x => 
            //x.Linea_Naviera === Linea_naviera && 
            x.Codigo_Puerto_Embarque === Cod_puertoemba &&
            //x.Puerto_Embarque === Carga_puerto &&
            x.Codigo_Puerto_Descarga === Cod_puertodes &&
            //x.Puerto_Descarga === desc_puerto &&
            x.Codigo_Pais_Embarque === Embarquecodpais &&
            //x.Pais_Puerto_Embarque === Embarquepais &&
            x.Codigo_Pais_Descarga === Descargacodpais &&
            //x.Pais_Puerto_Descarga === Descargapais &&
            x.Id_Puerto_Carga === Id_Puerto_Carga &&
            x.Id_Puerto_Descarga === Id_Puerto_Descarga
        );

        if (result3.length > 0) {
            Swal.fire({
                icon: 'info',
                title: 'Datos existentes o dentro de un grupo',
                text: 'Naviera:' + result3[0].Linea_Naviera + ' ,Puerto Embarque:' + result3[0].Puerto_Embarque + ' ,Puerto Descarga:' + result3[0].Puerto_Descarga,
            });
            return contenedor;
        }
    }
    

    if (result2.length > 0) {
        Swal.fire({
            icon: 'info',
            title: 'Datos existentes o dentro de un grupo',
            text: 'Naviera:' + result2[0].Linea_Naviera + ' ,Puerto Embarque:' + result2[0].Puerto_Embarque + ' ,Puerto Descarga:' + result2[0].Puerto_Descarga,
        });
        return contenedor;
    }

    if (contenedor != undefined) {
        result = contenedor.filter(x => x.Codigo_Linea_Naviera === Cod_line_navi &&
            //x.Linea_Naviera === Linea_naviera && 
            x.Codigo_Puerto_Embarque === Cod_puertoemba &&
            //x.Puerto_Embarque === Carga_puerto &&
            x.Codigo_Puerto_Descarga === Cod_puertodes &&
            //x.Puerto_Descarga === desc_puerto &&
            //x.Commodity === Commodity &&
            x.Codigo_Commodity === Cod_comodity &&
            x.TT === TT &&
            x.Tamanio === Tamano &&
            x.Codigo_Pais_Embarque === Embarquecodpais &&
            //x.Pais_Puerto_Embarque === Embarquepais &&
            x.Codigo_Pais_Descarga === Descargacodpais &&
            //x.Pais_Puerto_Descarga === Descargapais &&
            x.Id_Puerto_Carga === Id_Puerto_Carga &&
            x.Id_Puerto_Descarga === Id_Puerto_Descarga &&
            x.Id_Naviera === Id_Naviera
        );
    } else {
        contenedor = new Array();
    }
    

    console.log(result);
    if (result.length > 0) {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'Datos existentes!',
        });

        return contenedor;

    } else {
        contenedor.push({
            Codigo_Linea_Naviera: Cod_line_navi,
            Linea_Naviera: Linea_naviera,
            Codigo_Puerto_Embarque: Cod_puertoemba,
            Puerto_Embarque: Carga_puerto,
            Codigo_Puerto_Descarga: Cod_puertodes,
            Puerto_Descarga: desc_puerto,
            Commodity: Commodity,
            Codigo_Commodity: Cod_comodity,
            TT: TT,
            Tamanio: Tamano,
            Region: Region,

            Codigo_Pais_Embarque: Embarquecodpais,
            Pais_Puerto_Embarque: Embarquepais,
            Codigo_Pais_Descarga: Descargacodpais,
            Pais_Puerto_Descarga: Descargapais,
            Id_Puerto_Carga: Id_Puerto_Carga,
            Id_Puerto_Descarga: Id_Puerto_Descarga,
            Id_Naviera: Id_Naviera
        });

        return contenedor;
    }
        //if (Cantidad != "" && Temperatura != "" && TipoContenedor != "" && Ventilacion != "" && Comodity != "" && Humedad != "" && Descrip_Carga != "" && Peso != "" && Volumen != "") {
       
    } 

function generar_tabla(estado) {

    var Cod_comodity = document.getElementById("txtComodity").value;
    if (Cod_comodity === '') {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'Seleccione Commodity',
        });
        return '0';
    }

    var valor1 = $("#txtPuertoDescarga").val();
    var valor2 = $("#txtPuertoEmbarque").val();
    var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);
    if (val1 == '1') {

        if (estado == 1) {
            contenedor = añadirContenedor();
        }
        if (contenedor !== undefined) {
            if (contenedor.length >= 0) {
                contador = 0;
                let myTable = "<hr/>";
                myTable += "<div class='col-12'>";
                myTable += "<div class='card'>";
                myTable += "<div class='card-body table-responsive p-0'>";
                myTable += "<table class='table table-hover text-nowrap'>";
                myTable += "<thead>";

                myTable += "<tr>";
                myTable += "<th>Region</th>";
                myTable += "<th>Linea</th>";
                myTable += "<th>POL</th>";
                myTable += "<th>POD</th>";
                myTable += "<th>Commodity</th>";
                myTable += "<th>CANT. CNTR.</th>";
                //myTable += "<th>TT</th>";
                //myTable += "<th>Valida hasta</th>";
                //myTable += "<th>Valida desde</th>";
                myTable += "</tr>";
                myTable += "</thead>";
                myTable += "<tbody>";


                for (let i = 0; i < contenedor.length; i++) {
                    myTable += "<tr>";
                    myTable += "<td> " + contenedor[i].Region + "</td>";
                    myTable += "<td> " + contenedor[i].Linea_Naviera + "</td>";
                    myTable += "<td> " + contenedor[i].Puerto_Embarque + "</td>";
                    myTable += "<td> " + contenedor[i].Puerto_Descarga + "</td>";
                    myTable += "<td> " + contenedor[i].Commodity + "</td>";
                    myTable += "<td> " + contenedor[i].Tamanio + "</td>";
                    //myTable += "<td> " + contenedor[i].TT + "</td>";
                    //myTable += "<td> " + contenedor[i].Valido_Desde + "</td>";
                    //myTable += "<td> " + contenedor[i].Valido_Hasta + "</td>";
                    myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <a href=\"#\" onclick='QuitarLista(" + contador + "); return false'> <i class=\"fa fa-times \" style=\"color: red\"></i>  </a> </td>";
                    myTable += "</tr>";
                    contador++;
                    //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].prod + "</td>";
                    //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].cant + "</td>";
                    //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].pr + "</td>";
                    //myTable += "</tr>";
                }
                myTable += "</tbody>";
                myTable += "</table>";
                myTable += "</div>";
                myTable += "</div>";
                myTable += "</div>";

                $("#gridLista").html(myTable);
            }
        } else {
            return;
        }
     

    } else {
        return false;

    }

}

function QuitarLista(id) {
    contenedor.splice(id, 1);

    generar_tabla(2);

    if (id == 0) {
        $('#btn_agregar').attr("disabled", false);

    }

}
 
function InsertMaritimo() {

    if ($("#form").valid()) {
        var data = new FormData();
        if (contenedor.length > 0) {
           showSpinner2();
            var ent = {
                Cantidad_Contenedor: $("#txtCantidad").val(),
                Codigo_Tipo_Contenedor: $("#txtTipoContenedor option:selected").val(),
                Tipo_Contenedor: $("#txtTipoContenedor option:selected").text(),
                Codigo_Cotizacion: $("#txtCodigo").val(),
                Cliente: $("#txtCliente").val(),
                Fecha_Envio: $("#txtFechaEnvio").val(),
                Fecha_Final: $("#txtFechaFinal").val(),
                Campania: $("#txtCampana option:selected").text(),
                Codigo_Campania: $("#txtCampana option:selected").val(),
                Nota: $("#txtNota").val(),
                Observacion: $("#txtObservacion").val()
            }
            var entJson = JSON.stringify(ent);
            var contenedorJson = JSON.stringify(contenedor);
            data.append("ent", entJson);
            data.append("contenedor", contenedorJson);

            $.ajax({
                url: '../Cotizacion/InsertMaritimo',
                type: 'POST',
                async: true,
                contentType: false,
                processData: false,
                data: data,
                beforeSend: function () { },
                success: function (response) {
                    var dato = response
                    hideSpinner2();
                    switch (response.nTipoMensaje) {
                        case 0:
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.sMensaje,
                            });
                            break;
                        case 1: /// Correcto
                            Swal.fire({
                                title: 'Registro Correcto.',
                                text: response.sMensaje,
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Ok!'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    url2 = "../Cotizacion/MisSolicitudes";
                                    window.open(url2, '_self');
                                }
                            });
                            break;
                        case 2:
                            Swal.fire({
                                icon: 'info',
                                title: 'Datos Repetidos',
                                text: response.sMensaje,
                            });
                            break;
                        default:
                    }

                    //if (dato == "1") {
                    //    Swal.fire({
                    //        title: 'Registro Correcto.',
                    //        text: "Se registro correctamente.",
                    //        icon: 'success',
                    //        showCancelButton: false,
                    //        confirmButtonColor: '#3085d6',
                    //        confirmButtonText: 'Ok!'
                    //    }).then((result) => {
                    //        if (result.isConfirmed) {
                    //            url2 = "../Cotizacion/MisSolicitudes";
                    //            window.open(url2, '_self');
                    //        }
                    //    })
                    //} else {
                    //    var spl = dato.split('-')
                    //    if (spl.length > 1) {

                    //        Swal.fire({
                    //            icon: 'info',
                    //            title: 'Datos Repetidos',
                    //            text: 'Naviera:' + spl[1] + ' ,Puerto Embarque:' + spl[2] + ' ,Puerto Descarga:' + spl[3],
                    //        });

                    //    } else {
                    //        var mensajeError = dato.split('-');
                    //        const Error = mensajeError[1];
                    //        Swal.fire({
                    //            icon: 'error',
                    //            title: 'Oops...',
                    //            text: 'Error: - ' + Error + ' Contacte al administrador.',
                    //        });
                    //    }
                       
                    //}
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Debe Agregar mínimo una Linea Marítima!',
            });
        }

    }
}


function InsertCotLogic() {

    showSpinner2();

    var Servicio_embar = 0;
    var Retiro_cont_vacio = 0;
    var Retiro_trans = 0;
    if ($('#chk_embarque').is(':checked')) {
        Servicio_embar = 1;
    } else {
        Servicio_embar = 0;
    }
    if ($('#chk_retiro').is(':checked')) {
        Retiro_cont_vacio = 1;
    } else {
        Retiro_cont_vacio = 0;
    }
    if ($('#chk_transporte').is(':checked')) {
        Retiro_trans = 1;
    } else {
        Retiro_trans = 0;
    }

    var Ser_Agen_Aduana = 0;
    var Cert_origen = 0;
    var Conex_elect = 0;
    var Pago_cpb = 0;

    if ($('#chk_Servicio_Aduana').is(':checked')) {
        Ser_Agen_Aduana = 1;
    } else {
        Ser_Agen_Aduana = 0;
    }
    if ($('#chk_Origen').is(':checked')) {
        Cert_origen = 1;
    } else {
        Cert_origen = 0;
    }
    if ($('#chk_conexion').is(':checked')) {
        Conex_elect = 1;
    } else {
        Conex_elect = 0;
    }
    if ($('#chk_Pago_cpb').is(':checked')) {
        Pago_cpb = 1;
    } else {
        Pago_cpb = 0;
    }


    var chk_agenciamiento_maritimo = 0;
    var chk_movilizaciones_contenedor = 0;
    var chk_generador = 0;
    var chk_aforo_fisico = 0;

    var chk_courier = 0;
    var chk_senasa = 0;
    var chk_filtro = 0;
    var chk_termografo = 0;

    if ($('#chk_agenciamiento_maritimo').is(':checked')) {
        chk_agenciamiento_maritimo = 1;
    } else {
        chk_agenciamiento_maritimo = 0;
    }
    if ($('#chk_movilizaciones_contenedor').is(':checked')) {
        chk_movilizaciones_contenedor = 1;
    } else {
        chk_movilizaciones_contenedor = 0;
    }
    if ($('#chk_generador').is(':checked')) {
        chk_generador = 1;
    } else {
        chk_generador = 0;
    }
    if ($('#chk_aforo_fisico').is(':checked')) {
        chk_aforo_fisico = 1;
    } else {
        chk_aforo_fisico = 0;
    }

    if ($('#chk_courier').is(':checked')) {
        chk_courier = 1;
    } else {
        chk_courier = 0;
    }
    if ($('#chk_senasa').is(':checked')) {
        chk_senasa = 1;
    } else {
        chk_senasa = 0;
    }
    if ($('#chk_filtro').is(':checked')) {
        chk_filtro = 1;
    } else {
        chk_filtro = 0;
    }
    if ($('#chk_termografo').is(':checked')) {
        Chk_TermoRegistros = 1;
    } else {
        Chk_TermoRegistros = 0;
    }


    //var Embarquecodpais = $("#cbo_embarque option:selected").attr('codpais');
    //var Embarquepais = $("#cbo_embarque option:selected").attr('pais');
    var Id_Puerto_Carga = $("#hddEmbarqueId").val();
    var Embarquecodpais = $("#hddEmbarqueCodigoPais").val();
    var Embarquepais = $("#hddEmbarqueNombrePais").val();

    var Codigo_Puerto_Embarque = $("#hddEmbarqueCodigoPuerto").val();
    var Puerto_Embarque = $("#txtPuertoEmbarque").val();

    var Descargacodpais = ''; //$("#cbo_AlmacenRetiro option:selected").attr('codpais');
    var Descargapais ='';// = $("#cbo_AlmacenRetiro option:selected").attr('pais');

    var Codigo_Commodity = $("#cbo_Commodity option:selected").val();
    var Commodity = $("#cbo_Commodity option:selected").text();

    var Codigo_Almacen_Retiro = $("#cbo_AlmacenRetiro option:selected").val();
    var Almacen_Retiro = $("#cbo_AlmacenRetiro option:selected").text();

    var Fecha_Cita = $("#txt_fecha").val();
    var Codigo_Almacen_Packing = $("#cbo_Almacen_Packing option:selected").val();
    var Almacen_Packing = $("#cbo_Almacen_Packing option:selected").text();

    var TipoTermografo = $("#txt_area_termografo").text();


    var Chk_Transporte = 0;
    var Chk_GateOut = 0;
    var Chk_AgenciamientoAduana = 0;
    var Chk_DerechoEmbarque = 0;
    var Chk_VistoBueno = 0;

    var Chk_EnvioDocumentos = 0;
    var Chk_Movilidad = 0;
    var Chk_TermoRegistros = 0;
    var Chk_FiltrosEtileno = 0;
    var Chk_Certificado = 0;

    //#region Checks Nuevos
    if ($('#chk_n_transporte').is(':checked')) {
        Chk_Transporte = 1;
    } else {
        Chk_Transporte = 0;
    }
    if ($('#chk_n_gate').is(':checked')) {
        Chk_GateOut = 1;
    } else {
        Chk_GateOut = 0;
    }
    if ($('#chk_n_agenciamiento').is(':checked')) {
        Chk_AgenciamientoAduana = 1;
    } else {
        Chk_AgenciamientoAduana = 0;
    }
    if ($('#chk_n_derecho').is(':checked')) {
        Chk_DerechoEmbarque = 1;
    } else {
        Chk_DerechoEmbarque = 0;
    }

    if ($('#chk_n_vistos').is(':checked')) {
        Chk_VistoBueno = 1;
    } else {
        Chk_VistoBueno = 0;
    }
    if ($('#chk_n_documentos').is(':checked')) {
        Chk_EnvioDocumentos = 1;
    } else {
        Chk_EnvioDocumentos = 0;
    }
    if ($('#chk_n_movilidad').is(':checked')) {
        Chk_Movilidad = 1;
    } else {
        Chk_Movilidad = 0;
    }
    if ($('#chk_n_termo').is(':checked')) {
        Chk_TermoRegistros = 1;
    } else {
        Chk_TermoRegistros = 0;
    }
    if ($('#chk_n_filtro').is(':checked')) {
        Chk_FiltrosEtileno = 1;
    } else {
        Chk_FiltrosEtileno = 0;
    }
    if ($('#chk_n_certificado').is(':checked')) {
        Chk_Certificado = 1;
    } else {
        Chk_Certificado = 0;
    }

    //#endregion

    var ent = {
        Codigo_Puerto_Embarque: Codigo_Puerto_Embarque,
        Puerto_Embarque: Puerto_Embarque,
        Codigo_Commodity: Codigo_Commodity,
        Commodity: Commodity,
        Servicio_Embarque: Servicio_embar,
        Retiro_Contenedor_Vacio: Retiro_cont_vacio,
        Codigo_Almacen_Retiro: Codigo_Almacen_Retiro,
        Almacen_Retiro: Almacen_Retiro,
        Fecha_Cita: Fecha_Cita,
        Retiro_Transporte: Retiro_trans,
        Codigo_Almacen_Packing: Codigo_Almacen_Packing,
        Almacen_Packing: Almacen_Packing,   
        Servicio_Agenciamiento_Aduana: Ser_Agen_Aduana,
        Certificado_Origen: Cert_origen,
        Conexion_Electrica: Conex_elect,
        Pago_CPB: Pago_cpb,

        Agenciamiento_Maritimo: chk_agenciamiento_maritimo,
        Movilizaciones_Contenedor: chk_movilizaciones_contenedor,
        Generador: chk_generador,
        Aforo_Fisico: chk_aforo_fisico,
        Courier: chk_courier,
        Senasa: chk_senasa,
        Filtro: chk_filtro,
        Termografo: chk_termografo,
        Tipo_Termografo: TipoTermografo,

        Codigo_Pais_Embarque: Embarquecodpais,
        Codigo_Pais_Descarga: Descargacodpais,
        Pais_Puerto_Descarga: Descargapais,
        Pais_Puerto_Embarque: Embarquepais,
        
        Id_Puerto_Carga: Id_Puerto_Carga,

        Chk_Transporte           : Chk_Transporte,
        Chk_GateOut              :    Chk_GateOut,
        Chk_AgenciamientoAduana  :    Chk_AgenciamientoAduana,
        Chk_DerechoEmbarque      :    Chk_DerechoEmbarque,
        Chk_VistoBueno           :    Chk_VistoBueno,
        Chk_EnvioDocumentos      :    Chk_EnvioDocumentos,
        Chk_Movilidad            :    Chk_Movilidad,
        Chk_TermoRegistros       :    Chk_TermoRegistros,
        Chk_FiltrosEtileno       :    Chk_FiltrosEtileno,
        Chk_Certificado: Chk_Certificado,

        IdCampania: $("#cbo_Campania option:selected").val(),
        Desc_Campania: $("#cbo_Campania option:selected").text(),
        TipoTermo: $("#cbo_TermoRegistro option:selected").text(),
        TipoFiltro: $("#cbo_FiltroEtileno option:selected").text(),
        CantidadTermo: $("#cbo_Cantidad option:selected").text(),
        CantidadFiltro: $("#cbo_Cantidad2 option:selected").text(),
        Comentarios: $("#txt_Comentario").val(),
        IdFiltro: $("#cbo_FiltroEtileno option:selected").val(),
        IdTermo: $("#cbo_TermoRegistro option:selected").val()
    }


    $.ajax({
        url: '../Cotizacion/InsertLogi',
        type: 'POST',
        async: true,
        data: { ent: ent},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;

            hideSpinner2();

            switch (response.nTipoMensaje) {
                case 0:
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                case 1: /// Correcto
                    Swal.fire({
                        title: 'Registro Correcto.',
                        text: response.sMensaje,
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'

                    }).then((result) => {
                        if (result.isConfirmed) {
                            url2 = "../Cotizacion/MisSolicitudesLogistica";
                            window.open(url2, '_self');
                        }
                    })
                    break;
                case 2:
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                default:
            }

            //if (dato === '1') {
            //    Swal.fire({
            //        title: 'Registro Correcto.',
            //        text: "Se registro correctamente.",
            //        icon: 'success',
            //        showCancelButton: false,
            //        confirmButtonColor: '#3085d6',
            //        confirmButtonText: 'Ok!'

            //    }).then((result) => {
            //        if (result.isConfirmed) {
            //            url2 = "../Cotizacion/MisSolicitudesLogistica";
            //            window.open(url2, '_self');
            //        }
            //    })
            //} else {
            //    var mensajeError = dato.split('-');
            //    const Error = mensajeError[1];
            //    Swal.fire({
            //        icon: 'error',
            //        title: 'Oops...',
            //        text: 'Error: - ' + Error + ' Contacte al administrador.',
            //    });
            //}
        }
    });

}

function InsertTransporte() {

    var ent = {
        Booking: $("#txtBooking").val(),
        Dimensiones: $("#txtDimensiones").val(),
        BL: $("#txtDimensiones").val(),
        Codigo_Contenedor: $("#cboTipoContenedor option:selected").val(),
        Tipo_Contenedor: $("#cboTipoContenedor option:selected").text(),
        Nro_Orden: $("#txtNroOrden").val(),
        Cant_Contenedor: $("#txtCantidadContenedores").val(),
        Cod_Commodity: $("#cboComodity option:selected").val(),
        Commodity: $("#cboComodity option:selected").text(),
        Peso: $("#txtPeso").val(),
        Almacen_Retiro: $("#txtAlmacenRetiro").val(),
        Fecha_Retiro: $("#txtFechaHoraRetiro").val(),
        Almacen_Devolucion: $("#txtAlmacenDevolucion").val()
    }

    $.ajax({
        url: '../Cotizacion/InsertTransporte',
        type: 'POST',
        async: false,
        data: { ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
           
            ;
        }
    });

}


