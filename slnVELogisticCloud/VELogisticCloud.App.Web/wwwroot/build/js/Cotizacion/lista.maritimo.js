﻿
$(document).ready(function () {
    Lista_Maritimo();

    $("#txtPuertoSalida").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });


    if ($("#txtPuertoDestino").length) {
        $("#txtPuertoDestino").autocomplete({
            source: "../Maestros/AutoCompletePuertosPais",
            minLength: 2,
            select: function (event, ui) {
                $("#hddDescargaId").val(ui.item.id);
                $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
                $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
                $("#hddDescargaNombrePais").val(ui.item.nombrePais);
                $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
            },
        });
    }

    document.getElementById("link_Cotizacion").style.color = "black";
    document.getElementById("link_Cotizacion").style.fontWeight = "bold";
    hideSpinner2();
});

function Lista_Maritimo() {
    showSpinner2();

    //var ValPuertoDestino = $("#txtPuertoDestino option:selected").val();
    //var codigoDestino = ValPuertoDestino.split('-');
    //var ValPuertoSalida = $("#txtPuertoSalida option:selected").val();
    //var codigoOrigen = ValPuertoSalida.split('-');


    let textoEmbarque = $("#txtPuertoSalida").val();
    let textoDescarga = $("#txtPuertoDestino").val();

    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var param = {
        cod_coti: $("#txtCodigoCoti").val(),
        Campana: "",
        cod_line_navi: $("#txtNaviera option:selected").val(),
        cod_puerto_destino: $("#hddDescargaCodigoPuerto").val(),
        cod_puerto_salida: $("#hddEmbarqueCodigoPuerto").val(),
        id_estado: $("#cbo_estado option:selected").val(),
        TipoCotizacion: $("#cboTipoServicio option:selected").val()
    }

    //showSpinner2();
    var tipousuario = $("#tipousuario").val();
    $.ajax({
        url: '../Cotizacion/ListaCotizacion',
        type: 'POST',
        async: false,
        data: { param: param, tipousuario: tipousuario},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);

            hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
        }
    });

}

$("#cboTipoServicio").change(function () {

    var opcion = $("#cboTipoServicio option:selected").val();
    if (opcion == '1') {
        cod = 1;
        $("#cboTipoServicio option:selected").val('1');
        var url = "../Cotizacion/Index";
        window.open(url, '_self');
    } else if (opcion == '2') {
        cod = 2;
        $("#cboTipoServicio option:selected").val('2');
        var url = "../Cotizacion/CotizacionLogisticaAprobadas";
        window.open(url, '_self');
    } 
    //url2 = "../Itinerario_Solicitar/Resumen";
    //window.open(url2, '_self');

});

function RealizarBooking(id) {
    url2 = "../Booking/Create?id=" + id + "&Tipo=CotizacionMaritima";
    window.open(url2, '_self');
}

function holamundo() {
    alert('Hola mundo')
}
