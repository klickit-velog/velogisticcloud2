﻿$(document).ready(function () {
    ListaLogisticaAprobadas();
});



function ListaLogisticaAprobadas() {
    showSpinner2();
    //var ValPuertoDestino = $("#txtPuertoDestino option:selected").val();
    //var codigoDestino = ValPuertoDestino.split('-');
    //var ValPuertoSalida = $("#txtPuertoSalida option:selected").val();
    //var codigoOrigen = ValPuertoSalida.split('-');

    const textoEmbarque = $("#txtPuertoSalida").val();
    const textoDescarga = $("#txtPuertoDestino").val();

    if(textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if(textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var param = {
        cod_coti: $("#txtCodigoCoti").val(),
        Campana: "",
        cod_line_navi: $("#txtNaviera option:selected").text(),
        cod_puerto_destino: $("#hddDescargaCodigoPuerto").val(),
        cod_puerto_salida: $("#hddEmbarqueCodigoPuerto").val(),
        id_estado: $("#cbo_estado option:selected").val(),
        TipoCotizacion: $("#cboTipoServicio option:selected").val(),
        IdNaviera: $("#txtNaviera option:selected").val()
    }

    var tipousuario = $("#tipousuario").val();
    //showSpinner2();
    $.ajax({
        url: '../Cotizacion/ListaAprobadosLogistica',
        type: 'POST',
        async: false,
        data: { param: param, tipousuario: tipousuario},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);

            hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
        }
    });

}


$("#cboTipoServicio").change(function () {

    var opcion = $("#cboTipoServicio option:selected").val();
    if (opcion == '1') {
        cod = 1;
        $("#cboTipoServicio option:selected").val('1');
        var url = "../Cotizacion/Index";
        window.open(url, '_self');
    } else if (opcion == '2') {
        cod = 2;
        $("#cboTipoServicio option:selected").val('2');
        var url = "../Cotizacion/CotizacionLogisticaAprobadas";
        window.open(url, '_self');
    }
    //url2 = "../Itinerario_Solicitar/Resumen";
    //window.open(url2, '_self');

});


function VistaAgregarOP(id) {
    url2 = "../OperacionLogistica2/Create?id="+id;
    window.open(url2, '_self');
}