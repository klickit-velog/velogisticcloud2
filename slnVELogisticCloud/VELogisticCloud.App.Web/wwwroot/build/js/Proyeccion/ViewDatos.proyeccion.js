﻿

var id = getParameterByName('id');

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

ViewDatos();

$(document).ready(function () {

    ViewDatos();
});

function ViewDatos() {
    $.ajax({
        url: '../Proyecciones/ViewDatosStore/',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#txtCampana").val(dato.campania); 
            $("#txtComodity").val(dato.commodity);
            $("#txtOrigen").val(dato.origen);
            $("#txtDestino").val(dato.destino);
            $("#txtTipo").val(dato.tipo); 

            $("#txt_s1").val(dato.s1);
            $("#txt_s2").val(dato.s2);
            $("#txt_s3").val(dato.s3);
            $("#txt_s4").val(dato.s4);
            $("#txt_s5").val(dato.s5);
            $("#txt_s6").val(dato.s6);
            $("#txt_s7").val(dato.s7);
            $("#txt_s8").val(dato.s8);
            $("#txt_s9").val(dato.s9);
            $("#txt_s10").val(dato.s10);
            $("#txt_s11").val(dato.s11);
            $("#txt_s12").val(dato.s12);
            $("#txt_s13").val(dato.s13);
            $("#txt_s14").val(dato.s14);
            $("#txt_s15").val(dato.s15);
            $("#txt_s16").val(dato.s16);
            $("#txt_s17").val(dato.s17);
            $("#txt_s18").val(dato.s18);
            $("#txt_s19").val(dato.s19);
            $("#txt_s20").val(dato.s22);
            $("#txt_s21").val(dato.s21);
            $("#txt_s22").val(dato.s22);
            $("#txt_s23").val(dato.s23);
            $("#txt_s24").val(dato.s24);
            $("#txt_s25").val(dato.s25);
            $("#txt_s26").val(dato.s26);
            $("#txt_s27").val(dato.s27);
            $("#txt_s28").val(dato.s28);
            $("#txt_s29").val(dato.s29);
            $("#txt_s30").val(dato.s30);
            $("#txt_s31").val(dato.s31);
            $("#txt_s32").val(dato.s32);
            $("#txt_s33").val(dato.s33);
            $("#txt_s34").val(dato.s34);
            $("#txt_s35").val(dato.s35);
            $("#txt_s36").val(dato.s36);

            //$("#gridLista").html(dato);
            /* validarRedirect(dato);*/ /*add sysseg*/
            ;
        }
    });

}

function UpdateProyeccion() {

    var detalle = {
        Id_Proyeccion : id,
        S1: $("#txt_s1").val(),
        S2: $("#txt_s2").val(),
        S3: $("#txt_s3").val(),
        S4: $("#txt_s4").val(),
        S5: $("#txt_s5").val(),
        S6: $("#txt_s6").val(),
        S7: $("#txt_s7").val(),
        S8: $("#txt_s8").val(),
        S9: $("#txt_s9").val(),
        S10: $("#txt_s10").val(),
        S11: $("#txt_s11").val(),
        S12: $("#txt_s12").val(),
        S13: $("#txt_s13").val(),
        S14: $("#txt_s14").val(),
        S15: $("#txt_s15").val(),
        S16: $("#txt_s16").val(),
        S17: $("#txt_s17").val(),
        S18: $("#txt_s18").val(),
        S19: $("#txt_s19").val(),
        S20: $("#txt_s20").val(),
        S21: $("#txt_s21").val(),
        S22: $("#txt_s22").val(),
        S23: $("#txt_s23").val(),
        S24: $("#txt_s24").val(),
        S25: $("#txt_s25").val(),
        S26: $("#txt_s26").val(),
        S27: $("#txt_s27").val(),
        S28: $("#txt_s28").val(),
        S29: $("#txt_s29").val(),
        S30: $("#txt_s30").val(),
        S31: $("#txt_s31").val(),
        S32: $("#txt_s32").val(),
        S33: $("#txt_s33").val(),
        S34: $("#txt_s34").val(),
        S35: $("#txt_s35").val(),
        S36: $("#txt_s36").val(),
    }

    $.ajax({
        url: '../Proyecciones/UpdateProyeccion',
        type: 'POST',
        async: false,
        data: { detalle: detalle },
        beforeSend: function () { },
        success: function (response) {

            Swal.fire({
                title: 'Actualización Correcta.',
                text: "Se Actualizó Correctamente.",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.isConfirmed) {
                    url2 = "../Proyecciones/Index";
                    window.open(url2, '_self');
                }
            })
        }
    });
}

