﻿
$(document).ready(function () {
    $("#dllorigen").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    if ($("#dllDestino").length) {
        $("#dllDestino").autocomplete({
            source: "../Maestros/AutoCompletePuertosPais",
            minLength: 2,
            select: function (event, ui) {
                $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
                $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
                $("#hddDescargaNombrePais").val(ui.item.nombrePais);
                $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
            },
        });
    }
    $("#form").valid();

});

function InsertProyeccion() {

    if ($("#form").valid()) {
    var cabecera = {
        Campania: $("#dllCampana option:selected").text(),
        Codigo_Commodity: $("#dllComodity option:selected").val(),
        Commodity: $("#dllComodity option:selected").text(),
        Codigo_Origen: $("#hddEmbarqueCodigoPuerto").val(), 
        Origen: $("#dllorigen").val(),
        Codigo_Destino: $("#hddDescargaCodigoPuerto").val(), 
        Destino: $("#dllDestino").val(),
        Codigo_Tipo: $("#dllTipo option:selected").val(), 
        Tipo: $("#dllTipo option:selected").text()
    }

    var detalle = {
        S1: $("#txt_s1").val(),
        S2: $("#txt_s2").val(),
        S3: $("#txt_s3").val(),
        S4: $("#txt_s4").val(),
        S5: $("#txt_s5").val(),
        S6: $("#txt_s6").val(),
        S7: $("#txt_s7").val(),
        S8: $("#txt_s8").val(),
        S9: $("#txt_s9").val(),
        S10: $("#txt_s10").val(),
        S11: $("#txt_s11").val(),
        S12: $("#txt_s12").val(),
        S13: $("#txt_s13").val(),
        S14: $("#txt_s14").val(),
        S15: $("#txt_s15").val(),
        S16: $("#txt_s16").val(),
        S17: $("#txt_s17").val(),
        S18: $("#txt_s18").val(),
        S19: $("#txt_s19").val(),
        S20: $("#txt_s20").val(),
        S21: $("#txt_s21").val(),
        S22: $("#txt_s22").val(),
        S23: $("#txt_s23").val(),
        S24: $("#txt_s24").val(),
        S25: $("#txt_s25").val(),
        S26: $("#txt_s26").val(),
        S27: $("#txt_s27").val(),
        S28: $("#txt_s28").val(),
        S29: $("#txt_s29").val(),
        S30: $("#txt_s30").val(),
        S31: $("#txt_s31").val(),
        S32: $("#txt_s32").val(),
        S33: $("#txt_s33").val(),
        S34: $("#txt_s34").val(),
        S35: $("#txt_s35").val(),
        S36: $("#txt_s36").val(),
    }

    $.ajax({
        url: '../Proyecciones/InsertProyeccion',
        type: 'POST',
        async: false,
        data: { cabecera: cabecera, detalle: detalle },
        beforeSend: function () { },
        success: function (response) {

            Swal.fire({
                title: 'Registro Correcto.',
                text: "Se registro correctamente.",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.isConfirmed) {
                    url2 = "../Proyecciones/Index";
                    window.open(url2, '_self');
                }
            })
        }
    });

    }
}

