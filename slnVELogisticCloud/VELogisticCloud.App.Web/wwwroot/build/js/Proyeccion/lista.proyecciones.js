﻿

$(document).ready(function () {

    Lista();

    $("#txtPuertoDestino").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDestinoCodigoPuerto").val(ui.item.codigo);
            $("#hddDestinoCodigoPais").val(ui.item.codigoPais);
            $("#hddDestinoNombrePais").val(ui.item.nombrePais);
            $("#hddDestinoNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtCommodity").autocomplete({
        source: "../Maestros/AutoCompleteCommodity",
        minLength: 2,
        select: function (event, ui) {
            $("#hddCommodity").val(ui.item.id);
        },
    });


    document.getElementById("link_Proyecciones").style.color = "black";
    document.getElementById("link_Proyecciones").style.fontWeight = "bold";
});

function Lista(){

    var fecha = $("#txtFecha").val();
    var codComodity = $("#hddCommodity").val();
    var codDestino = $("#hddDestinoCodigoPuerto").val();

    if ($("#txtCommodity").val() == '') codComodity = '0';
    if ($("#txtPuertoDestino").val() == '') codDestino = '0';

    $.ajax({
        url: '../Proyecciones/ListaProyeccion/',
        type: 'POST',
        async: false,
        data: { fecha: fecha, codComodity: codComodity, codDestino: codDestino },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
        }
    });
    hideSpinner();
}

//function GuardarId(id) {
//    url2 = "../Proyecciones/ViewDatos?id=" + id ;
//    window.open(url2, '_self');
//}

function EnviarARegistro() {
    url2 = "../Proyecciones/Create";
    window.open(url2, '_self');
}


function ViewDatos(id) {
    url2 = "../Proyecciones/ViewDatos?id=" + id;
    window.open(url2, '_self');
}
