﻿
var id = getParameterByName('id');
Iniciar();
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function Iniciar() {
    if (id == 1) {
        showSpinner2();

        Contenedor1();
        $("#cont_1").addClass("bg-info");
        hideSpinner();

    } else {
        showSpinner2();

        Contenedor2();
        hideSpinner();

    }
}


function Contenedor1() {

    document.getElementById("div_maritimo").style.display = "block";
    document.getElementById("div_logistica").style.display = "none"; 

    $("#cont_1").addClass("bg-info");
    $("#cont_1").removeClass("bg-light");
    $("#cont_2").removeClass("bg-light");
    $("#cont_2").removeClass("bg-info");

    document.getElementById('lbl_Reserva').innerHTML = 0;
    document.getElementById('lbl_status').innerHTML = 0;
    document.getElementById('lbl_coti').innerHTML = 0; 
    DatosCotizacion();
    DatosReserva();
    DatosReservaInttra();

}

function Contenedor2() {

  
    document.getElementById("div_maritimo").style.display = "none";
    document.getElementById("div_logistica").style.display = "block"; 

    $("#cont_2").addClass("bg-info");
    $("#cont_2").removeClass("bg-light");
    $("#cont_1").removeClass("bg-info");
    $("#cont_1").removeClass("bg-light");

    //document.getElementById('lbl_Reserva2').innerHTML = 40;
    //document.getElementById('lbl_status2').innerHTML = 20;
    //document.getElementById('lbl_coti2').innerHTML = 15; 
    DatosCotiLogistica(); 
    DatosOPL();
    DatosOPLOddo();
}

function DatosCotizacion() {
        $.ajax({
            url: '../Estadistica/DatosCotizacion',
            type: 'POST',
            async: false,
            //data: { id: id, ent: ent },
            beforeSend: function () { },
            success: function (response) {
                var dato = response;
                document.getElementById('lbl_coti').innerHTML = dato[0].total;
                document.getElementById('lbl_coti_solicitada').innerHTML = dato[0].solicitado;
                document.getElementById('lbl_coti_confirmada').innerHTML = dato[0].aceptado + dato[0].confirmada;
                document.getElementById('lbl_coti_cancelada').innerHTML = dato[0].cancelada;
                //document.getElementById('lbl_coti_observada').innerHTML = dato[0].cancelada;

                
            }
        });
}

function DatosReserva() {
    $.ajax({
        url: '../Estadistica/ResumenBooking/',
        type: 'POST',
        async: false,
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('lbl_Reserva').innerHTML = dato[0].total;
            document.getElementById('lbl_reserva_solicitada').innerHTML = dato[0].pendiente;
            document.getElementById('lbl_reserva_confirmada').innerHTML = dato[0].aprobados;
            document.getElementById('lbl_reserva_observada').innerHTML = 0;
            document.getElementById('lbl_reserva_cancelada').innerHTML = dato[0].rechazado;
        }
    });

    //$.ajax({
    //    url: '../Estadistica/DatosReserva',
    //    type: 'POST',
    //    async: true,
    //    //data: { id: id, ent: ent },
    //    beforeSend: function () { },
    //    success: function (response) {
    //        var dato = response;
    //        document.getElementById('lbl_reserva_solicitada').innerHTML = dato[0].cantidad;
    //        document.getElementById('lbl_reserva_confirmada').innerHTML = dato[2].cantidad;
    //        document.getElementById('lbl_reserva_observada').innerHTML = 0;
    //        document.getElementById('lbl_reserva_cancelada').innerHTML = dato[3].cantidad;
    //        document.getElementById('lbl_Reserva').innerHTML = dato[3].cantidad + dato[2].cantidad + dato[0].cantidad;

    //    }
    //});
}

function DatosReservaInttra() {
    $.ajax({
        url: '../Estadistica/DatosReservaInttra',
        type: 'POST',
        async: false,
        //data: { id: id, ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('lbl_status').innerHTML = dato[0].total;
            document.getElementById('lbl_reserva_puerto').innerHTML = dato[0].puerto;
            document.getElementById('lbl_reserva_nave').innerHTML = dato[0].nave;
            document.getElementById('lbl_reserva_destino').innerHTML = dato[0].puertoDestino;

        }
    });
}


function DatosCotiLogistica() {
    $.ajax({
        url: '../Estadistica/DatosCotLogistica',
        type: 'POST',
        async: false,
        //data: { id: id, ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('lbl_logistica_solicitada').innerHTML = dato[0].pendiente;
            document.getElementById('lbl_logistica_observada').innerHTML = dato[0].observada;
            document.getElementById('lbl_logistica_confirmada').innerHTML = dato[0].confirmado + dato[0].aceptado;
            document.getElementById('lbl_logistica_cancelada').innerHTML = dato[0].cancelada;
            document.getElementById('lbl_coti2').innerHTML = dato[0].total;

        }
    });
}


function DatosOPL() {
    $.ajax({
        url: '../Estadistica/DatosOperacionLogistica',
        type: 'POST',
        async: false,
        //data: { id: id, ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('lbl_reserva_solicitada').innerHTML = dato[0].cantidad;
            document.getElementById('lbl_reserva_observada').innerHTML = 0;
            document.getElementById('lbl_reserva_confirmada').innerHTML = dato[2].cantidad;
            document.getElementById('lbl_reserva_cancelada').innerHTML = dato[3].cantidad;
            document.getElementById('lbl_Reserva2').innerHTML = dato[2].cantidad + dato[3].cantidad + dato[0].cantidad;

        }
    });
}

function DatosOPLOddo() {
    $.ajax({
        url: '../Estadistica/DatosOperacionLogisticaOddo',
        type: 'POST',
        async: false,
        //data: { id: id, ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('lbl_opl_contenedorAsignado').innerHTML = dato[0].contenedorAsignado;
            document.getElementById('lbl_opl_ingresoallenarCarga').innerHTML = dato[0].ingresoallenarCarga
            document.getElementById('lbl_opl_ingresoalPuerto').innerHTML = dato[0].ingresoalPuerto;
            document.getElementById('lbl_opl_llegadaadepositodevacio').innerHTML = dato[0].llegadaadepositodevacio;
            document.getElementById('lbl_opl_llegadaalCliente').innerHTML = dato[0].llegadaalCliente;
            document.getElementById('lbl_opl_llegadaalPuerto').innerHTML = dato[0].llegadaalPuerto;
            document.getElementById('lbl_opl_enCamino').innerHTML = dato[0].enCamino;
            document.getElementById('lbl_opl_SalidadelCliente').innerHTML = dato[0].salidadelCliente;
            document.getElementById('lbl_opl_salidadelPuerto').innerHTML = dato[0].salidadelPuerto;
            document.getElementById('lbl_opl_llegadaaBase').innerHTML = dato[0].llegadaaBase;
            document.getElementById('lbl_opl_Salidadedepositodevacio').innerHTML = dato[0].salidadedepositodevacio;
            document.getElementById('lbl_opl_revisionSINI').innerHTML = dato[0].revisionSINI;
            document.getElementById('lbl_status2').innerHTML = dato[0].total;

        }
    });
}
