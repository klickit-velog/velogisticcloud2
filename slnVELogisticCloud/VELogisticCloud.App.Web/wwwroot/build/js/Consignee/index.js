﻿
$(document).ready(function () {
    //Lista_Maritimo();
    $("#txtPuertoSalida").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });


    if ($("#txtPuertoDestino").length) {
        $("#txtPuertoDestino").autocomplete({
            source: "../Maestros/AutoCompletePuertosPais",
            minLength: 2,
            select: function (event, ui) {
                $("#hddDescargaId").val(ui.item.id);
                $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
                $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
                $("#hddDescargaNombrePais").val(ui.item.nombrePais);
                $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
            },
        });
    }

    $("#txtContactOwner").autocomplete({
        source: "../Maestros/AutoCompleteContactOwner",
        minLength: 2,
        select: function (event, ui) {
            $("#hddContactId").val(ui.item.id);
            $("#hddCorreo").val(ui.item.correo);
        },
    });

    document.getElementById("link_ContactOwner").style.color = "black";
    document.getElementById("link_ContactOwner").style.fontWeight = "bold";
    hideSpinner();

});

function Lista_Maritimo() {
    showSpinner2();

    //var ValPuertoDestino = $("#txtPuertoDestino option:selected").val();
    //var codigoDestino = ValPuertoDestino.split('-');
    //var ValPuertoSalida = $("#txtPuertoSalida option:selected").val();
    //var codigoOrigen = ValPuertoSalida.split('-');


    let textoEmbarque = $("#txtPuertoSalida").val();
    let textoDescarga = $("#txtPuertoDestino").val();

    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var param = {
        cod_coti: $("#txtCodigoCoti").val(),
        Campana: "",
        cod_line_navi: $("#txtNaviera option:selected").val(),
        cod_puerto_destino: $("#hddDescargaCodigoPuerto").val(),
        cod_puerto_salida: $("#hddEmbarqueCodigoPuerto").val(),
        id_estado: $("#cbo_estado option:selected").val(),
        TipoCotizacion: $("#cboTipoServicio option:selected").val()
    }
    var correo = $("#hddCorreo").val();
    //showSpinner2();
    var tipousuario = $("#tipousuario").val();
    $.ajax({
        url: '../Consignee/ListaCotizacion',
        type: 'POST',
        async: false,
        data: { param: param, tipousuario: tipousuario, correo: correo },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);

            hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
        }
    });

}


function RealizarBooking(id) {
    const idContractOwner = $("#hddContactId").val();
    const ContractOwner = $("#txtContactOwner").val();
    url2 = "../Booking/Create?id=" + id + "&Tipo=Consignee" + "&idContractOwner=" + idContractOwner + "&ContractOwner=" + ContractOwner ;
    window.open(url2, '_self');
}

function holamundo() {
    alert('Hola mundo')
}
