﻿
$(document).ready(function () {

    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });

    document.getElementById("link_Reservas").style.color = "black";
    document.getElementById("link_Reservas").style.fontWeight = "bold";

    ListaActual();
});

function ListaActual() {
    showSpinner2();

    let textoEmbarque = $("#txtPuertoEmbarque").val();
    let textoDescarga = $("#txtPuertoDescarga").val();


    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var ent = {
        NroBooking: $("#txt_nroBooking").val(), 
        IdBooking: $("#txt_idBooking").val(),
        IdPuertoEmbarque: $("#hddEmbarqueId").val(),
        IdPuertoDescarga: $("#hddDescargaId").val(),
        EstadoVE: $("#id_cbEstado option:selected").val()
    }

    var tipousuario = $("#tipousuario").val();

    $.ajax({
        url: '../Booking/ListarBooking/',
        type: 'POST',
        async: false,
        data: { ent: ent, tipousuario: tipousuario},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridLista").html(dato);
            /* validarRedirect(dato);*/ /*add sysseg*/
            ;
        }
    });
    hideSpinner2();
}


function CancelarBooking(id) {

    Swal.fire({
        title: 'Cancelar Reserva.',
        text: "¿Esta seguro de cancelar la reserva?.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../Booking/CancelarBooking',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {

                    switch (response.nTipoMensaje) {
                        case 0:
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.sMensaje,
                            });
                            break;
                        case 1: /// Correcto
                            Swal.fire({
                                title: 'Cancelacion Exitosa.',
                                text: response.sMensaje,
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Ok!'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    ListaActual();
                                }
                            });
                            break;
                        case 2:
                            Swal.fire({
                                icon: 'info',
                                title: 'Oops...',
                                text: response.sMensaje,
                            });
                            break;
                        default:
                    }
                }
            });
        };
    })

       
    

}


function EditReserva(id) {
    showSpinner2();
    url2 = "../Booking/Create?id=" + id + "&Tipo=Editar";
    window.open(url2, '_self');
    hideSpinner2();

}

function UpdateImo(id) {
        Swal.fire({
            title: 'Registrar imo manual.',
            html:
                '<input id="swal-input" placeholder="Ingrese # Imo" class="swal2-input">',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Grabar!',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                nroImo = document.getElementById('swal-input').value;
                GuardarImoAjax(id, nroImo);
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          
        })
}

function GuardarImoAjax(id, imo) {
    $.ajax({
        url: '../Booking/UpdateImo',
        type: 'POST',
        async: false,
        data: { id: id, imo: imo },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            Swal.fire({
                title: 'Se grabo el Imo correctamente.',
                text: "Actualización correcta.",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.isConfirmed) {
                    VerMapa(imo);
                    ListaActual();
                }
            });
        }
    });
}

function VerMapa(id) {

    if (id != "0") {
        url2 = "../Alertas/Mapa?imo=" + id;

        //Swal.fire({
        //    url: 'https://localhost:44350/Alertas/Mapa?imo=' + id,
        //    imageHeight: 1500,
        //    imageAlt: 'A tall image'
        //})
        window.open(url2, '_blank');
    } else {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'No se encontro el codigo la Nave!',
        });
    }

}



function PlantillaReserva(id) {
    showSpinner2();

    url2 = "../Booking/Create?id=" + id + "&Tipo=Plantilla";
    window.open(url2, '_self');

    hideSpinner2();

}
