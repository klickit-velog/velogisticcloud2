﻿var id = getParameterByName('id');
var Tipo = getParameterByName('Tipo');
var J_user = getParameterByName('J_user');
var ImoNumber = 0;
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function () {
    showSpinner2();
    $("#txtConsignatario").autocomplete({
        source: "../Booking/AutoCompleteConsignatario",
        minLength: 2,

        select: function (event, ui) {
            $("#HidCodConsignatario").val(ui.item.id);
            $("#txtConsignatarioDireccion").val(ui.item.direccion);
            $("#txtConsignatarioContacto").val(ui.item.contacto);
            $("#txtConsignatarioTelefono").val(ui.item.telefono);
            $("#txtConsignatarioFax").val(ui.item.fax);

            //$('#txtConsignatarioDireccion').prop('disabled', true);
            //$('#txtConsignatarioContacto').prop('disabled', true);
            //$('#txtConsignatarioTelefono').prop('disabled', true);
            //$('#txtConsignatarioFax').prop('disabled', true); 
        },
        focus: function (event, ui) {
            //$("#HidCodConsignatario").val(ui.item.id);
            $("#txtConsignatarioDireccion").val(ui.item.direccion);
            $("#txtConsignatarioContacto").val(ui.item.contacto);
            $("#txtConsignatarioTelefono").val(ui.item.telefono);
            $("#txtConsignatarioFax").val(ui.item.fax);
        },
    });

    $("#txtNotify").autocomplete({
        source: "../Booking/AutoCompleteNotify",
        minLength: 2,
        select: function (event, ui) {
            $("#HidCodNotify").val(ui.item.id);
            $("#txtNotifyDireccion").val(ui.item.direccion);
            $("#txtNotifyContacto").val(ui.item.contacto);
            $("#txtNotifyTelefono").val(ui.item.telefono); 
            $("#txtNotifyFax").val(ui.item.fax);
            //$('#txtNotifyDireccion').prop('disabled', true);
            //$('#txtNotifyContacto').prop('disabled', true);
            //$('#txtNotifyTelefono').prop('disabled', true);
            //$('#txtNotifyFax').prop('disabled', true); 
        },
        focus: function (event, ui) {
            //$("#HidCodNotify").val(ui.item.id);
            $("#txtNotifyDireccion").val(ui.item.direccion);
            $("#txtNotifyContacto").val(ui.item.contacto);
            $("#txtNotifyTelefono").val(ui.item.telefono);
            $("#txtNotifyFax").val(ui.item.fax);
        },
      
        
    });

    //$("#txtContactOwner").autocomplete({
    //    source: "../Maestros/AutoCompleteContactOwner",
    //    minLength: 2,
    //    select: function (event, ui) {
    //        $("#hddContactId").val(ui.item.id);
    //    },
    //});

    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });
    
    CargarDatos();
    ActivarDesactivar();

    $("#cboTecnologia").select2();
    $("#txtContactOwner").select2();
    hideSpinner2();
});

function toggleCheckbox(element) {
    //alert(element.checked)
    if (element.checked) {
        const dato0 = $("#txtConsignatario").val();
        const dato1 = $("#txtConsignatarioDireccion").val();
        const dato2 = $("#txtConsignatarioContacto").val();
        const dato3 = $("#txtConsignatarioTelefono").val();
        const dato4 = $("#txtConsignatarioFax").val();
        $("#txtNotify").val(dato0);
        $("#txtNotifyDireccion").val(dato1);
        $("#txtNotifyContacto").val(dato2);
        $("#txtNotifyTelefono").val(dato3);
        $("#txtNotifyFax").val(dato4);
    } else {
        $("#txtNotify").val('');
        $("#txtNotifyDireccion").val('');
        $("#txtNotifyContacto").val('');
        $("#txtNotifyTelefono").val('');
        $("#txtNotifyFax").val('');
    }
    //element.checked = !element.checked;
}

$("#cbo_atmosfera").change(function () {
    ActivarDesactivar();
});

$("#cbo_tipo_humedad").change(function () {
    ActivarDesactivar();
});

$("#txtComodity").change(function () {
    var id = $("#txtComodity option:selected").val();
    if (id > 0) {
        ObtenerPartidaArancelaria(id);
    }
});

function ObtenerPartidaArancelaria(id) {
    $.ajax({
        url: '../Booking/ObtenerPartidaArancelaria',
        type: 'POST',
        async: false,
        data: {  id: id },
        beforeSend: function () { },
        success: function (response) {

            switch (response.nTipoMensaje) {
                case 0:
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                case 1: /// Correcto
                    var dato = response.nValorString;
                    $("#txt_partida").val(dato);
                    break;
                case 2:
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                default:
            }
        },
    });
}

function ActivarDesactivar() {
    var opcion = $("#cbo_atmosfera option:selected").val();
    if (opcion == 0) {
        $("#txt_co2").prop('disabled', true);
        $("#txt_o2").prop('disabled', true);
    } else {
        $("#txt_co2").prop('disabled', false);
        $("#txt_o2").prop('disabled', false);
    }

    var opcion2 = $("#cbo_tipo_humedad option:selected").val();
    if (opcion2 == 0) {
        $("#txt_humedad").prop('disabled', true);
    } else {
        $("#txt_humedad").prop('disabled', false);
    }

}

function CambiarContenedor() {
    const tipo = $("#txtTipoContenedor option:selected").text();
    if (tipo.trim().toLocaleLowerCase().includes("reefer")) {
        document.getElementById("div_reefer").style.display = "block";
        document.getElementById("div_dry").style.display = "none";

    } else if (tipo.trim().toLocaleLowerCase().includes("dry")) {
        document.getElementById("div_dry").style.display = "block";
        document.getElementById("div_reefer").style.display = "none";

    } else {
        document.getElementById("div_dry").style.display = "none";
        document.getElementById("div_reefer").style.display = "none";

    }
}

function CambiarPago() {
    const pago = $("#cbo_pago option:selected").val();
    if (pago == "1") {
        $("#txt_nombre_pagador").prop('disabled', false);
        $("#txt_direccion_pagador").prop('disabled', false);
    } else if (pago == "2") {
        $("#txt_direccion_pagador").prop('disabled', true);
        $("#txt_nombre_pagador").prop('disabled', true);
    } else if (pago == "3") {
        $("#txt_direccion_pagador").prop('disabled', true);
        $("#txt_nombre_pagador").prop('disabled', true);
    }
}

function formatoFecha(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
}

function formatoFechaAnioMesDia(texto) { 
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$1/$2/$3');
}

function ValidarPuertoEmbarqueYSalida(puerto1, puerto2) {
    if (puerto1 == puerto2) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Seleccione diferentes puertos!',
        });
        return '0';

    } else {
        return '1';
    }
}

function CargarDatosCotizacionMaritima() { 
    $.ajax({
        url: '../Booking/CargarDatosCoti',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#hddIdOdooService").val(dato.idOddoService);

            document.getElementById("txtLineaNaviera").value = dato.codigo_Linea_Naviera;
            $("#txtNroContrato").val(dato.numeroContrato);

            document.getElementById("txtPuertoEmbarque").value = dato.puerto_Embarque
            $("#hddEmbarqueId").val(dato.id_Puerto_Carga);
            $("#hddEmbarqueCodigoPuerto").val(dato.codigo_Puerto_Embarque);
            $("#hddEmbarqueCodigoPais").val(dato.codigo_Pais_Embarque);
            $("#hddEmbarqueNombrePais").val(dato.pais_Puerto_Embarque);

            document.getElementById("txtPuertoDescarga").value = dato.puerto_Descarga;
            $("#hddDescargaId").val(dato.id_Puerto_Descarga);
            $("#hddDescargaCodigoPuerto").val(dato.codigo_Puerto_Descarga);
            $("#hddDescargaCodigoPais").val(dato.codigo_Pais_Descarga);
            $("#hddDescargaNombrePais").val(dato.pais_Puerto_Descarga);
            //$("#txtComodity").val(dato.commodity);
            $("#hddCommodityId").val(dato.codigo_Commodity);
            document.getElementById("txtComodity").value = dato.codigo_Commodity;



            $("#txtCantidad").val(1);
            $("#hddValidoHasta").val(dato.valido_Hasta);
            $("#hddValidoDesde").val(dato.valido_Desde);
            document.getElementById("txtTipoContenedor").value = dato.codigo_Tipo_Contenedor;
            //document.getElementById("txtComodity").value = dato.codigo_Commodity; 
            CambiarPago();
            CambiarContenedor();
            ObtenerPartidaArancelaria(dato.codigo_Commodity);

        }
    });


    //Disabled
    //$("#txtLineaNaviera").prop('disabled', true);
    //$("#txtNroContrato").prop('disabled', true);
    $("#txtExpeditor").prop('disabled', true);
    $("#txtPuertoEmbarque").prop('disabled', true);
    $("#txtPuertoDescarga").prop('disabled', true);
    //$("#txtETD").prop('disabled', true);
    //$("#txtETA").prop('disabled', true);
    //$("#txtNombreNave").prop('disabled', true);
    //$("#txtNaveViaje").prop('disabled', true);
    $("#txtTipoContenedor").prop('disabled', true);
    $("#txtComodity").prop('disabled', true);
}

//function ValidarContrato() {
//    var contrato = $("#txtNroContrato").val();
//    if (contrato.trim() === null || contrato.trim() === "") {
//        //document.getElementById('span_correcto').innerHTML = "# Contrato Erróneo.";
//        //document.getElementById('span_correcto').style = "color:red";
//        document.getElementById('span_correcto').innerHTML = "Sin # Contrato.";
//        document.getElementById('span_correcto').style = "color:yellow";
//        $('#btnInsert').prop('disabled', false);
//        $('#btnUpdate').prop('disabled', false);
//    }
//    else {
//        $.ajax({
//            url: '../Booking/ValidarContrato',
//            type: 'POST',
//            async: false,
//            data: { contrato: contrato, idReserva: id },
//            beforeSend: function () { },
//            success: function (response) {
//                var dato = response;

//                if (dato === 0) {
//                    document.getElementById('span_correcto').innerHTML = "# Contrato Erróneo.";
//                    document.getElementById('span_correcto').style = "color:red";
//                    $('#btnInsert').prop('disabled', true);
//                    $('#btnUpdate').prop('disabled', true);
//                } else {
//                    document.getElementById('span_correcto').innerHTML = "# Contrato Correcto.";
//                    document.getElementById('span_correcto').style = "color:green";
//                    //IdCotizacionDetalleOdoo = dato;
//                    $('#btnInsert').prop('disabled', false);
//                    $('#btnUpdate').prop('disabled', false);
//                    //$('#btn_update').prop('disabled', false);
//                }
//            },

//        })
//    };
//}

//$("#txtNroContrato").blur(function () {

//    //if (J_user === 'customer') {
//        showSpinner2();
//        ValidarContrato();
//        hideSpinner();

//    //} else {
//    //}
//});

function CargarDatosItinerario() {
    var OriginCode = getParameterByName('OriginCode');
    var DestinationCode = getParameterByName('DestinationCode');
    var puertoEmbarque = getParameterByName('puertoEmbarque');
    var puertoDescarga = getParameterByName('puertoDescarga');
    var scac = getParameterByName('scac');
    var NroContrato = getParameterByName('NroContrato');
    var IdEmbarqueIti = getParameterByName('IdEmbarque');
    var IdDescargaIti = getParameterByName('IdDescarga');
    var CodigoPaisOrigen = getParameterByName('CodigoPaisOrigen');
    var NombrePaisOrigen = getParameterByName('NombrePaisOrigen');
    var CodigoPaisDestino = getParameterByName('CodigoPaisDestino');
    var NombrePaisDestino = getParameterByName('NombrePaisDestino');
    var etd = getParameterByName('etd');
    var eta = getParameterByName('eta');
    var nombreViaje = getParameterByName('nombreViaje');
    var naveViaje = getParameterByName('naveViaje');
    ImoNumber = getParameterByName('ImoNumber');

    document.getElementById("div_dry").style.display = "none";
    document.getElementById("div_reefer").style.display = "none";
    document.getElementById("txtLineaNaviera").value = scac.trim();
    document.getElementById("txtComodity").value = '0';

    $("#txtPuertoEmbarque").val(puertoEmbarque);
    $("#txtPuertoDescarga").val(puertoDescarga);


    $("#hddEmbarqueId").val(IdEmbarqueIti);
    $("#hddEmbarqueCodigoPuerto").val(OriginCode);
    $("#hddEmbarqueCodigoPais").val(CodigoPaisOrigen);
    $("#hddEmbarqueNombrePais").val(NombrePaisOrigen);

    $("#hddDescargaId").val(IdDescargaIti);
    $("#hddDescargaCodigoPuerto").val(DestinationCode);
    $("#hddDescargaCodigoPais").val(CodigoPaisDestino);
    $("#hddDescargaNombrePais").val(NombrePaisDestino);

    $("#txtNroContrato").val(NroContrato);

    $("#txtETD").val(etd);
    $("#txtETA").val(eta);
    $("#txtNombreNave").val(nombreViaje);
    $("#txtNaveViaje").val(naveViaje);




    //Disabled
    $("#txtLineaNaviera").prop('disabled', true);
    $("#txtExpeditor").prop('disabled', true);
    $("#txtPuertoEmbarque").prop('disabled', true);
    $("#txtPuertoDescarga").prop('disabled', true);
    $("#txtETD").prop('disabled', true);
    $("#txtETA").prop('disabled', true);
    $("#txtNombreNave").prop('disabled', true);
    $("#txtNaveViaje").prop('disabled', true);

    CambiarPago();
    CambiarContenedor();
}

function CargarDatosPlantilla() {
    $.ajax({
        url: '../Booking/CargarDatosReserva',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {

            switch (response.nTipoMensaje) {
                case 0:
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                case 1: /// Correcto
                    var dato = response.objeto;
                    $("#txtExportadorNombre").val(dato.exportadorNombre);
                    $("#txtExportadorDomicilio").val(dato.exportadorDomicilio);
                    $("#txtExportadorRuc").val(dato.exportadorRuc);
                    $("#txtExportadorContacto").val(dato.exportadorContacto);
                    $("#txtExportadorTelefono").val(dato.exportadorTelefono);
                    $("#txtExportadorFax").val(dato.exportadorFax);
                    $("#txtExportadorCelular").val(dato.exportadorCelular);

                    //Datos Consignatario
                    $("#txtConsignatario").val(dato.consignatarioNombre);
                    $("#HidCodConsignatario").val(dato.idConsignatario);
                    $("#txtConsignatarioDireccion").val(dato.consignatarioDireccion);
                    $("#txtConsignatarioContacto").val(dato.consignatarioContacto);
                    $("#txtConsignatarioTelefono").val(dato.consignatarioTelefono);
                    $("#txtConsignatarioFax").val(dato.consignatarioFax);

                    //Datos Notify
                    $("#txtNotify").val(dato.notifyNombre);
                    $("#HidCodNotify").val(dato.idNotify);
                    $("#txtNotifyDireccion").val(dato.notifyDireccion);
                    $("#txtNotifyContacto").val(dato.notifyContacto);
                    $("#txtNotifyTelefono").val(dato.notifyTelefono);
                    $("#txtNotifyFax").val(dato.notifyFax);

                    //Datos Naviera 


                    document.getElementById("txtLineaNaviera").value = dato.codLinea;

                    $("#txtNombreNave").val(dato.nombreNave);
                    $("#txtNaveViaje").val(dato.nroViaje);
                    $("#txtNroContrato").val(dato.nroContrato);
                    $("#hddEmbarqueCodigoPais").val(dato.codigoPaisEmbarque);
                    $("#hddEmbarqueNombrePais").val(dato.paisEmbarque);
                    $("#hddEmbarqueId").val(dato.idPuertoEmbarque);
                    $("#hddEmbarqueCodigoPuerto").val(dato.codigoPuertoEmbarque);
                    $("#txtPuertoEmbarque").val(dato.puertoEmbarque);
                    $("#hddDescargaCodigoPais").val(dato.codigoPaisDescarga);
                    $("#hddDescargaNombrePais").val(dato.paisDescarga);
                    $("#hddDescargaId").val(dato.idPuertoDescarga);
                    $("#hddDescargaCodigoPuerto").val(dato.codigoPuertoDescarga);
                    $("#txtPuertoDescarga").val(dato.puertoDescarga);
                    var fechavistaETA = formatoFechaAnioMesDia(dato.eta);
                    var fechavistaETD = formatoFechaAnioMesDia(dato.etd);
                    $("#txtETA").val(fechavistaETA.replace("T00:00:00", ""));
                    $("#txtETD").val(fechavistaETD.replace("T00:00:00", ""));

                    document.getElementById("txtTipoContenedor").value = dato.idTipoContenedor;
                    document.getElementById("cbo_atmosfera").value = dato.atmosferaControlada;
                    document.getElementById("cboTecnologia").value = dato.tecnologia;

                    $("#txt_co2").val(dato.cO2);
                    $("#txt_o2").val(dato.o2);
                    document.getElementById("cbo_Cold").value = dato.coldTreatment;
                    document.getElementById("cbo_grados").value = dato.tipoTemperatura;
                    $("#txt_temperatura").val(dato.temperatura);
                    document.getElementById("cbo_tipo_ventilacion").value = dato.tipoVentilacion;
                    $("#txt_ventilacion").val(dato.ventilacion);
                    document.getElementById("cbo_tipo_humedad").value = dato.tipoHumedad;
                    $("#txt_humedad").val(dato.humedad);
                    document.getElementById("cbo_imo").value = dato.idImo;

                    $("#txt_un1").val(dato.uN1);
                    $("#txt_un2").val(dato.uN2);

                    //Datos del Producto
                    document.getElementById("cbo_imo").value = dato.idCommodity;

                    $("#txtVariedad").val(dato.variedad);
                    $("#txt_partida").val(dato.partidaArancelaria);
                    document.getElementById("txtTipoBulto").value = dato.tipoBulto;
                    $("#txtCantidad").val(dato.cantidadBulto);
                    $("#txtPeso").val(dato.pesoBruto);

                    //Condiciones de Pago 
                    document.getElementById("cbo_pago").value = dato.idCondicionPago;
                    document.getElementById("cbo_emision").value = dato.idEmisionBL;
                    $("#txt_nombre_pagador").val(dato.nombrePagador);
                    $("#txt_direccion_pagador").val(dato.direccionPagador);
                    $("#txtOperadorLogistico").val(dato.operadorLogistico);
                    $("#txtRucOperador").val(dato.rucOperador);
                    $("#txt_cant_booking").val(dato.cantidadBooking);

                    $("#hddCommodityId").val(dato.codigoCommodity);
                    $("#hddIdOdooService").val(dato.idDetalleCotizacionOdoo);
                    $("#hddValidoHasta").val(dato.valido_Hasta);
                    $("#hddValidoDesde").val(dato.valido_Desde);

                    $("#txtContactOwner").val(dato.contactOwner);
                    $("#hddContactId").val(dato.idContactOwner);
                    document.getElementById("ddlTipoContrato").value = dato.tipoContrato;
                    document.getElementById("txtComodity").value = dato.idCommodity;
                    CambiarPago();
                    CambiarContenedor();
                    break;
                case 2:
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                default:
            }

        }
    });

}

function CargarDatosConsignee() {
    var idContractOwner = getParameterByName('idContractOwner');
    var ContractOwner = getParameterByName('ContractOwner');
    $("#hddContactId").val(idContractOwner);
    $("#txtContactOwner").val(ContractOwner);
    $.ajax({
        url: '../Booking/CargarDatosCoti',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato === '0') {
                var mensajeError = dato.split('-');
                const Error = mensajeError[1];
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error: - ' + Error + ' Contacte al administrador.',
                });
            } else {
                $("#hddIdOdooService").val(dato.idOddoService);

                document.getElementById("txtLineaNaviera").value = dato.codigo_Linea_Naviera;
                $("#txtNroContrato").val(dato.numeroContrato);

                document.getElementById("txtPuertoEmbarque").value = dato.puerto_Embarque
                $("#hddEmbarqueId").val(dato.id_Puerto_Carga);
                $("#hddEmbarqueCodigoPuerto").val(dato.codigo_Puerto_Embarque);
                $("#hddEmbarqueCodigoPais").val(dato.codigo_Pais_Embarque);
                $("#hddEmbarqueNombrePais").val(dato.pais_Puerto_Embarque);

                document.getElementById("txtPuertoDescarga").value = dato.puerto_Descarga;
                $("#hddDescargaId").val(dato.id_Puerto_Descarga);
                $("#hddDescargaCodigoPuerto").val(dato.codigo_Puerto_Descarga);
                $("#hddDescargaCodigoPais").val(dato.codigo_Pais_Descarga);
                $("#hddDescargaNombrePais").val(dato.pais_Puerto_Descarga);
                $("#hddCommodityId").val(dato.codigo_Commodity);
                document.getElementById("txtComodity").value = dato.codigo_Commodity;



                $("#txtCantidad").val(1);
                $("#hddValidoHasta").val(dato.valido_Hasta);
                $("#hddValidoDesde").val(dato.valido_Desde);
                document.getElementById("txtTipoContenedor").value = dato.codigo_Tipo_Contenedor;
                CambiarPago();
                CambiarContenedor();
                ObtenerPartidaArancelaria(dato.codigo_Commodity);
            }

        }
    });


    //Disabled
    //$("#txtLineaNaviera").prop('disabled', true);
    //$("#txtNroContrato").prop('disabled', true);
    $("#txtExpeditor").prop('disabled', true);
    $("#txtPuertoEmbarque").prop('disabled', true);
    $("#txtPuertoDescarga").prop('disabled', true);

    $("#txtTipoContenedor").prop('disabled', true);
    $("#txtComodity").prop('disabled', true);
}

function CargarDatos() {

    showSpinner2();

    if (Tipo.trim() === "CotizacionMaritima") {
        CargarDatosCotizacionMaritima();
        document.getElementById("div_btn_insert").style.display = "block";
        document.getElementById("div_btn_update").style.display = "none";
        //ValidarContrato();
    }
    if (Tipo.trim() === "Itinerario") {
        CargarDatosItinerario();
        document.getElementById("div_btn_insert").style.display = "block";
        document.getElementById("div_btn_update").style.display = "none";
    }
    if (Tipo.trim() === "Plantilla") {
        CargarDatosPlantilla();
        document.getElementById("div_btn_insert").style.display = "block";
        document.getElementById("div_btn_update").style.display = "none";
    }
    if (Tipo.trim() === "Editar") {
        CargarDatosPlantilla();
        document.getElementById("div_btn_insert").style.display = "none";
        document.getElementById("div_btn_update").style.display = "block";
        document.getElementById("div_cant_booking").style.display = "none";
    }
    if (Tipo.trim() === "Consignee") {
        CargarDatosConsignee();
        document.getElementById("div_btn_insert").style.display = "block";
        document.getElementById("div_btn_update").style.display = "none";
        //ValidarContrato();
    }
    
    hideSpinner2();
}

function InsertReserva() {
    const txtNotify = $("#txtNotify").val();
    const ContractId = $("#hddContactId").val();
    const IdTipoContenedor = $("#txtTipoContenedor option:selected").val();
    const IdTipoCommodity = $("#txtComodity option:selected").val();


    if (txtNotify.trim() !== '' && IdTipoContenedor !== '0' && IdTipoCommodity !== '0' && ContractId !== '0' ) {
        if ($("#form").valid()) {

            Swal.fire({
                title: 'Registrar Reserva.',
                text: "¿Esta seguro de registrar la reserva?",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            }).then(( result) => {
                if (result.isConfirmed) {
                    //Swal.fire('Please wait')
                    //Swal.showLoading()
                     showSpinner2();

                    var etd_valida = $("#txtETD").val()
                    var eta_valida = $("#txtETA").val()

                    if (etd_valida.trim() != "" && eta_valida.trim() != "") {

                        //Datos Exportador
                        var ExportadorNombre = $("#txtExportadorNombre").val();
                        var ExportadorDomicilio = $("#txtExportadorDomicilio").val();
                        var ExportadorRuc = $("#txtExportadorRuc").val();
                        var ExportadorContacto = $("#txtExportadorContacto").val();
                        var ExportadorTelefono = $("#txtExportadorTelefono").val();
                        var ExportadorFax = $("#txtExportadorFax").val();
                        var ExportadorCelular = $("#txtExportadorCelular").val();

                        //Datos Consignatario
                        var ConsignatarioNombre = $("#txtConsignatario").val();
                        var ConsignatarioId = $("#HidCodConsignatario").val();
                        var ConsignatarioDireccion = $("#txtConsignatarioDireccion").val();
                        var ConsignatarioContacto = $("#txtConsignatarioContacto").val();
                        var ConsignatarioTelefono = $("#txtConsignatarioTelefono").val();
                        var ConsignatarioFax = $("#txtConsignatarioFax").val();

                        //Datos Notify
                        var NotifyNombre = $("#txtNotify").val();
                        var NotifyId = $("#HidCodNotify").val();
                        var NotifyDireccion = $("#txtNotifyDireccion").val();
                        var NotifyContacto = $("#txtNotifyContacto").val();
                        var NotifyTelefono = $("#txtNotifyTelefono").val();
                        var NotifyFax = $("#txtNotifyFax").val();

                        //Datos Naviera 

                        var IdLinea = $("#txtLineaNaviera option:selected").val();
                        var CodLinea = $("#txtLineaNaviera option:selected").val();
                        var Linea = $("#txtLineaNaviera option:selected").text();
                        var NombreNave = $("#txtNombreNave").val();
                        var NroViaje = $("#txtNaveViaje").val();
                        var NroContrato = $("#txtNroContrato").val();
                        var CodigoPaisEmbarque = $("#hddEmbarqueCodigoPais").val();
                        var PaisEmbarque = $("#hddEmbarqueNombrePais").val();
                        var IdPuertoEmbarque = $("#hddEmbarqueId").val();
                        var CodigoPuertoEmbarque = $("#hddEmbarqueCodigoPuerto").val();
                        var PuertoEmbarque = $("#txtPuertoEmbarque").val();
                        var CodigoPaisDescarga = $("#hddDescargaCodigoPais").val();
                        var PaisDescarga = $("#hddDescargaNombrePais").val();
                        var IdPuertoDescarga = $("#hddDescargaId").val();
                        var CodigoPuertoDescarga = $("#hddDescargaCodigoPuerto").val();
                        var PuertoDescarga = $("#txtPuertoDescarga").val();
                        //var ETA = formatoFecha($("#txtETA").val());
                        //var ETD = formatoFecha($("#txtETD").val());
                        var IdContactOwner = $("#hddContactId").val();
                        var ContactOwner = $("#txtContactOwner").val();

                        var ETA = $("#txtETA").val();
                        var ETD = $("#txtETD").val();

                        //Datos Contenedor
                        var a = $("#txtTipoContenedor option:selected").text();
                        var Clase = "Dry";
                        if (a.trim().toLocaleLowerCase().includes("reefer")) {
                            Clase = "Reefer"
                        }
                        var ClaseContenedor = Clase;
                        var IdTipoContenedor = $("#txtTipoContenedor option:selected").val();
                        var TipoContenedor = $("#txtTipoContenedor option:selected").text();
                        var AtmosferaControlada = $("#cbo_atmosfera option:selected").val();
                        var CO2 = $("#txt_co2").val();
                        var O2 = $("#txt_o2").val();
                        var ColdTreatment = $("#cbo_Cold option:selected").val();
                        var TipoTemperatura = $("#cbo_grados option:selected").val();
                        var Temperatura = $("#txt_temperatura").val();
                        var TipoVentilacion = $("#cbo_tipo_ventilacion option:selected").val();
                        var Ventilacion = $("#txt_ventilacion").val();
                        var TipoHumedad = $("#cbo_tipo_humedad option:selected").val();
                        var Humedad = $("#txt_humedad").val();
                        var IdImo = $("#cbo_imo option:selected").val();
                        var IMO = $("#cbo_imo option:selected").text();
                        var UN1 = $("#txt_un1").val();
                        var UN2 = $("#txt_un2").val();

                        //Datos del Producto

                        var IdCommodity = $("#txtComodity option:selected").val();
                        var CodigoCommodity = $("#txtComodity option:selected").text();
                        var Commodity = $("#txtComodity option:selected").text();
                        var Variedad = $("#txtVariedad").val();
                        var PartidaArancelaria = $("#txt_partida").val();
                        var TipoBulto = $("#txtTipoBulto option:selected").text();
                        var CantidadBulto = $("#txtCantidad").val();
                        var PesoBruto = $("#txtPeso").val();

                        //Condiciones de Pago 
                        var IdCondicionPago = $("#cbo_pago option:selected").val();
                        var CondicionPago = $("#cbo_pago option:selected").text();
                        var IdEmisionBL = $("#cbo_emision option:selected").val();
                        var EmisionBL = $("#cbo_emision option:selected").text();
                        var NombrePagador = $("#txt_nombre_pagador").val();
                        var DireccionPagador = $("#txt_direccion_pagador").val();
                        var OperadorLogistico = $("#txtOperadorLogistico").val();
                        var RucOperador = $("#txtRucOperador").val();
                        var CantidadBooking = $("#txt_cant_booking").val();
                        var Nota = "";
                        var IdDetalleCotizacionOdoo = $("#hddIdOdooService").val();
                        var Tecnologia = $("#cboTecnologia option:selected").val();

                        const IdItinerarioManual = $('#IdItinerarioManual').val();

                        var ent = {

                            ExportadorNombre: ExportadorNombre,
                            ExportadorDomicilio: ExportadorDomicilio,
                            ExportadorRuc: ExportadorRuc,
                            ExportadorContacto: ExportadorContacto,
                            ExportadorTelefono: ExportadorTelefono,
                            ExportadorFax: ExportadorFax,
                            ExportadorCelular: ExportadorCelular,

                            IdLinea: IdLinea,
                            CodLinea: CodLinea,
                            Linea: Linea,
                            NombreNave: NombreNave,
                            NroViaje: NroViaje,
                            NroContrato: NroContrato,
                            CodigoPaisEmbarque: CodigoPaisEmbarque,
                            PaisEmbarque: PaisEmbarque,
                            IdPuertoEmbarque: IdPuertoEmbarque,
                            CodigoPuertoEmbarque: CodigoPuertoEmbarque,
                            PuertoEmbarque: PuertoEmbarque,
                            CodigoPaisDescarga: CodigoPaisDescarga,
                            PaisDescarga: PaisDescarga,
                            IdPuertoDescarga: IdPuertoDescarga,
                            CodigoPuertoDescarga: CodigoPuertoDescarga,
                            PuertoDescarga: PuertoDescarga,
                            ETA: ETA,
                            ETD: ETD,


                            ClaseContenedor: ClaseContenedor,
                            TipoContenedor: TipoContenedor,
                            AtmosferaControlada: AtmosferaControlada,
                            CO2: CO2,
                            O2: O2,
                            ColdTreatment: ColdTreatment,
                            TipoTemperatura: TipoTemperatura,
                            Temperatura: Temperatura,
                            TipoVentilacion: TipoVentilacion,
                            Ventilacion: Ventilacion,
                            TipoHumedad: TipoHumedad,
                            Humedad: Humedad,
                            IdImo: IdImo,
                            IMO: IMO,
                            UN1: UN1,
                            UN2: UN2,

                            IdCommodity: IdCommodity,
                            CodigoCommodity: CodigoCommodity,
                            Commodity: Commodity,
                            Variedad: Variedad,
                            PartidaArancelaria: PartidaArancelaria,
                            TipoBulto: TipoBulto,
                            CantidadBulto: CantidadBulto,
                            PesoBruto: PesoBruto,
                            IdCondicionPago: IdCondicionPago,
                            CondicionPago: CondicionPago,
                            IdEmisionBL: IdEmisionBL,
                            EmisionBL: EmisionBL,
                            NombrePagador: NombrePagador,
                            DireccionPagador: DireccionPagador,
                            OperadorLogistico: OperadorLogistico,
                            RucOperador: RucOperador,
                            IdDetalleCotizacionOdoo: IdDetalleCotizacionOdoo,
                            IdTipoContenedor: IdTipoContenedor,
                            CantidadBooking: CantidadBooking,
                            IdContactOwner: IdContactOwner,
                            ContactOwner: ContactOwner,
                            Tecnologia: Tecnologia,
                            ImoInttra: ImoNumber
                        }

                        var consignatario = {
                            IdConsignatario: ConsignatarioId,
                            Nombre: ConsignatarioNombre,
                            Direccion: ConsignatarioDireccion,
                            Fax: ConsignatarioFax,
                            Telefono: ConsignatarioTelefono,
                            PersonaContacto: ConsignatarioContacto
                        }

                        var notify = {
                            IdNotify: NotifyId,
                            Nombre: NotifyNombre,
                            Direccion: NotifyDireccion,
                            Fax: NotifyFax,
                            Telefono: NotifyTelefono,
                            PersonaContacto: NotifyContacto
                        }

                        var valor1 = $("#hddEmbarqueCodigoPuerto").val();
                        var valor2 = $("#hddDescargaCodigoPuerto").val();
                        var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);
                        var tipoContrato = $("#ddlTipoContrato option:selected").val();
                        if (val1 == '1') {

                            $.ajax({
                                url: '../Booking/InsertarDatos',
                                type: 'POST', 
                                async: true, 
                                data: {
                                    ent: ent, consignatario: consignatario, notify: notify, tipousuario: J_user,
                                    tipoContrato: tipoContrato, IdItinerarioManual: IdItinerarioManual
                                },
                                beforeSend: function () { },
                                success: function (response) {
                                    hideSpinner2();

                                    switch (response.nTipoMensaje) {
                                        case 0:
                                            hideSpinner2();

                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: response.sMensaje,
                                            });

                                            break;
                                        case 1:
                                            hideSpinner2();

                                            Swal.fire({
                                                title: 'Registro Correcto.',
                                                text: response.sMensaje,
                                                icon: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                confirmButtonText: 'Ok!'
                                            }).then((result) => {
                                                if (result.isConfirmed) {
                                                    url2 = "../Booking/Resumen?id=" + response.nValorEntero + '&J_user=' + J_user;
                                                    window.open(url2, '_self');
                                                } else {

                                                }
                                            })
                                            break;
                                        case 2:
                                            hideSpinner2();
                                            Swal.fire({
                                                icon: 'info',
                                                title: 'Oops...',
                                                text: response.sMensaje,
                                            });

                                            break;
                                        default:
                                    }

                                }
                            });

                        } else {
                            hideSpinner2();

                            Swal.fire({
                                icon: 'info',
                                title: 'Oops...',
                                text: 'Error Puertos!',
                            })
                        }
                    } else {
                        hideSpinner2();

                        Swal.fire({
                            icon: 'info',
                            title: 'Oops...',
                            text: 'No puede generar una reserva sin un ETD o ETA!',
                        })
                    }
                }
            });
        }
    } else {
        Swal.fire({
            icon: 'info',
            title: 'Verifique los campos.',
            text: 'Recuerde llenar los campos de Notify , Contenedor,Comodity o Contract Owner!',
        })
    }

    hideSpinner2();

}

function UpdateReserva() {
    Swal.fire({
        title: 'Actualizar Reserva.',
        text: "¿Esta seguro de actualizar la reserva?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {

            var etd_valida = $("#txtETD").val();
            var eta_valida = $("#txtETA").val();
            const ContractId = $("#hddContactId").val();


            if (etd_valida.trim() != "" && eta_valida.trim() != "" && ContractId !== '0') {

                //Datos Exportador
                var ExportadorNombre = $("#txtExportadorNombre").val();
                var ExportadorDomicilio = $("#txtExportadorDomicilio").val();
                var ExportadorRuc = $("#txtExportadorRuc").val();
                var ExportadorContacto = $("#txtExportadorContacto").val();
                var ExportadorTelefono = $("#txtExportadorTelefono").val();
                var ExportadorFax = $("#txtExportadorFax").val();
                var ExportadorCelular = $("#txtExportadorCelular").val();

                //Datos Consignatario
                var ConsignatarioNombre = $("#txtConsignatario").val();
                var ConsignatarioId = $("#HidCodConsignatario").val();
                var ConsignatarioDireccion = $("#txtConsignatarioDireccion").val();
                var ConsignatarioContacto = $("#txtConsignatarioContacto").val();
                var ConsignatarioTelefono = $("#txtConsignatarioTelefono").val();
                var ConsignatarioFax = $("#txtConsignatarioFax").val();

                //Datos Notify
                var NotifyNombre = $("#txtNotify").val();
                var NotifyId = $("#HidCodNotify").val();
                var NotifyDireccion = $("#txtNotifyDireccion").val();
                var NotifyContacto = $("#txtNotifyContacto").val();
                var NotifyTelefono = $("#txtNotifyTelefono").val();
                var NotifyFax = $("#txtNotifyFax").val();

                //Datos Naviera 

                var IdLinea = $("#txtLineaNaviera option:selected").val();
                var CodLinea = $("#txtLineaNaviera option:selected").val();
                var Linea = $("#txtLineaNaviera option:selected").text();
                var NombreNave = $("#txtNombreNave").val();
                var NroViaje = $("#txtNaveViaje").val();
                var NroContrato = $("#txtNroContrato").val();
                var CodigoPaisEmbarque = $("#hddEmbarqueCodigoPais").val();
                var PaisEmbarque = $("#hddEmbarqueNombrePais").val();
                var IdPuertoEmbarque = $("#hddEmbarqueId").val();
                var CodigoPuertoEmbarque = $("#hddEmbarqueCodigoPuerto").val();
                var PuertoEmbarque = $("#txtPuertoEmbarque").val();
                var CodigoPaisDescarga = $("#hddDescargaCodigoPais").val();
                var PaisDescarga = $("#hddDescargaNombrePais").val();
                var IdPuertoDescarga = $("#hddDescargaId").val();
                var CodigoPuertoDescarga = $("#hddDescargaCodigoPuerto").val();
                var PuertoDescarga = $("#txtPuertoDescarga").val();
                var ETA = $("#txtETA").val();
                var ETD = $("#txtETD").val();
                var IdContactOwner = $("#hddContactId").val();
                var ContactOwner = $("#txtContactOwner").val();


                //Datos Contenedor
                var a = $("#txtTipoContenedor option:selected").text();
                var Clase = "Dry";
                if (a.trim().toLocaleLowerCase().includes("reefer")) {
                    Clase = "Reefer"
                }
                var ClaseContenedor = Clase;
                var IdTipoContenedor = $("#txtTipoContenedor option:selected").val();
                var TipoContenedor = $("#txtTipoContenedor option:selected").text();
                var AtmosferaControlada = $("#cbo_atmosfera option:selected").val();
                var CO2 = $("#txt_co2").val();
                var O2 = $("#txt_o2").val();
                var ColdTreatment = $("#cbo_Cold option:selected").val();
                var TipoTemperatura = $("#cbo_grados option:selected").val();
                var Temperatura = $("#txt_temperatura").val();
                var TipoVentilacion = $("#cbo_tipo_ventilacion option:selected").val();
                var Ventilacion = $("#txt_ventilacion").val();
                var TipoHumedad = $("#cbo_tipo_humedad option:selected").val();
                var Humedad = $("#txt_humedad").val();
                var IdImo = $("#cbo_imo option:selected").val();
                var IMO = $("#cbo_imo option:selected").text();
                var UN1 = $("#txt_un1").val();
                var UN2 = $("#txt_un2").val();

                //Datos del Producto

                var IdCommodity = $("#txtComodity option:selected").val();
                var CodigoCommodity = $("#txtComodity option:selected").text();
                var Commodity = $("#txtComodity option:selected").text();
                var Variedad = $("#txtVariedad").val();
                var PartidaArancelaria = $("#txt_partida").val();
                var TipoBulto = $("#txtTipoBulto option:selected").text();
                var CantidadBulto = $("#txtCantidad").val();
                var PesoBruto = $("#txtPeso").val();

                //Condiciones de Pago 
                var IdCondicionPago = $("#cbo_pago option:selected").val();
                var CondicionPago = $("#cbo_pago option:selected").text();
                var IdEmisionBL = $("#cbo_emision option:selected").val();
                var EmisionBL = $("#cbo_emision option:selected").text();
                var NombrePagador = $("#txt_nombre_pagador").val();
                var DireccionPagador = $("#txt_direccion_pagador").val();
                var OperadorLogistico = $("#txtOperadorLogistico").val();
                var RucOperador = $("#txtRucOperador").val();
                var Nota = "";
                var Tecnologia = $("#cboTecnologia option:selected").val();



                var ent = {

                    ExportadorNombre: ExportadorNombre,
                    ExportadorDomicilio: ExportadorDomicilio,
                    ExportadorRuc: ExportadorRuc,
                    ExportadorContacto: ExportadorContacto,
                    ExportadorTelefono: ExportadorTelefono,
                    ExportadorFax: ExportadorFax,
                    ExportadorCelular: ExportadorCelular,

                    IdLinea: IdLinea,
                    CodLinea: CodLinea,
                    Linea: Linea,
                    NombreNave: NombreNave,
                    NroViaje: NroViaje,
                    NroContrato: NroContrato,
                    CodigoPaisEmbarque: CodigoPaisEmbarque,
                    PaisEmbarque: PaisEmbarque,
                    IdPuertoEmbarque: IdPuertoEmbarque,
                    CodigoPuertoEmbarque: CodigoPuertoEmbarque,
                    PuertoEmbarque: PuertoEmbarque,
                    CodigoPaisDescarga: CodigoPaisDescarga,
                    PaisDescarga: PaisDescarga,
                    IdPuertoDescarga: IdPuertoDescarga,
                    CodigoPuertoDescarga: CodigoPuertoDescarga,
                    PuertoDescarga: PuertoDescarga,
                    ETA: ETA,
                    ETD: ETD,


                    ClaseContenedor: ClaseContenedor,
                    TipoContenedor: TipoContenedor,
                    AtmosferaControlada: AtmosferaControlada,
                    CO2: CO2,
                    O2: O2,
                    ColdTreatment: ColdTreatment,
                    TipoTemperatura: TipoTemperatura,
                    Temperatura: Temperatura,
                    TipoVentilacion: TipoVentilacion,
                    Ventilacion: Ventilacion,
                    TipoHumedad: TipoHumedad,
                    Humedad: Humedad,
                    IdImo: IdImo,
                    IMO: IMO,
                    UN1: UN1,
                    UN2: UN2,

                    IdCommodity: IdCommodity,
                    CodigoCommodity: CodigoCommodity,
                    Commodity: Commodity,
                    Variedad: Variedad,
                    PartidaArancelaria: PartidaArancelaria,
                    TipoBulto: TipoBulto,
                    CantidadBulto: CantidadBulto,
                    PesoBruto: PesoBruto,
                    IdCondicionPago: IdCondicionPago,
                    CondicionPago: CondicionPago,
                    IdEmisionBL: IdEmisionBL,
                    EmisionBL: EmisionBL,
                    NombrePagador: NombrePagador,
                    DireccionPagador: DireccionPagador,
                    OperadorLogistico: OperadorLogistico,
                    RucOperador: RucOperador,
                    IdTipoContenedor: IdTipoContenedor,
                    IdContactOwner: IdContactOwner,
                    ContactOwner: ContactOwner,
                    Tecnologia: Tecnologia
                }


                var consignatario = {
                    IdConsignatario: ConsignatarioId,
                    Nombre: ConsignatarioNombre,
                    Direccion: ConsignatarioDireccion,
                    Fax: ConsignatarioFax,
                    Telefono: ConsignatarioTelefono,
                    PersonaContacto: ConsignatarioContacto
                }

                var notify = {
                    IdNotify: NotifyId,
                    Nombre: NotifyNombre,
                    Direccion: NotifyDireccion,
                    Fax: NotifyFax,
                    Telefono: NotifyTelefono,
                    PersonaContacto: NotifyContacto
                }

                var valor1 = $("#hddEmbarqueCodigoPuerto").val();
                var valor2 = $("#hddDescargaCodigoPuerto").val();
                var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);

                var tipoContrato = $("#ddlTipoContrato option:selected").val();
                if (val1 == '1') {
                    showSpinner2();
                    $.ajax({
                        url: '../Booking/UpdateDatos',
                        type: 'POST',
                        async: false,
                        data: { id: id, ent: ent, consignatario: consignatario, notify: notify, tipoContrato: tipoContrato },
                        beforeSend: function () { },
                        success: function (response) {

                            hideSpinner2();

                            switch (response.nTipoMensaje) {
                                case 0:
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: response.sMensaje,
                                    });
                                    break;
                                case 1:
                                    Swal.fire({
                                        title: 'Registro Correcto.',
                                        text: response.sMensaje,
                                        icon: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Ok!'
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            url2 = "../Booking/Resumen?id=" + response.nValorEntero + '&J_user=' + J_user;
                                            window.open(url2, '_self');
                                        } else {

                                        }
                                    })
                                    break;
                                case 2:
                                    Swal.fire({
                                        icon: 'info',
                                        title: 'Oops...',
                                        text: response.sMensaje,
                                    });
                                    break;
                                default:
                            }

                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: 'Error Puertos!',
                    })
                }
            } else {
                Swal.fire({
                    icon: 'info',
                    title: 'Oops...',
                    text: 'No puede generar una reserva sin un ETD, ETA o Contract Owner!',
                })
            }
        }
    });
}

$("#txt_co2").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value) {
            return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});

$("#txt_o2").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value) {
            return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});

$("#txt_ventilacion").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value) {
            return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});

$("#txt_humedad").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value) {
            return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});


function soloNumeros(e) {
    var textoNumero = $("#txt_temperatura").val();
    var TieneNegativo = textoNumero.includes("-");
    var TienePunto = textoNumero.includes(".");
    var LengTexto = textoNumero.length;
    //if (LengTexto === 0) {
    //    var key = e.keyCode || e.which,
    //        tecla = String.fromCharCode(key).toLowerCase(),
    //        letras = "-123456789",
    //        especiales = [],
    //        tecla_especial = false;

    //    for (var i in especiales) {
    //        if (key == especiales[i]) {
    //            tecla_especial = true;
    //            break;
    //        }
    //    }
    //}

    if (LengTexto === 0) {
        var key = e.keyCode || e.which,
            tecla = String.fromCharCode(key).toLowerCase(),
            letras = "-0123456789",
            especiales = [],
            tecla_especial = false;

        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    } 

    if (TienePunto || !TieneNegativo && LengTexto > 0) {
        var key = e.keyCode || e.which,
            tecla = String.fromCharCode(key).toLowerCase(),
            letras = "0123456789",
            especiales = [],
            tecla_especial = false;

        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    }

    if (TieneNegativo || !TienePunto && LengTexto > 0 ) {
        var key = e.keyCode || e.which,
            tecla = String.fromCharCode(key).toLowerCase(),
            letras = "0123456789.",
            especiales = [],
            tecla_especial = false;

        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    }

    if (TieneNegativo && TienePunto && LengTexto > 0) {
        var key = e.keyCode || e.which,
            tecla = String.fromCharCode(key).toLowerCase(),
            letras = "0123456789",
            especiales = [],
            tecla_especial = false;

        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
    //else {
    //    var key = e.keyCode || e.which,
    //        tecla = String.fromCharCode(key).toLowerCase(),
    //        letras = "-123456789.",
    //        especiales = [],
    //        tecla_especial = false;

    //    for (var i in especiales) {
    //        if (key == especiales[i]) {
    //            tecla_especial = true;
    //            break;
    //        }
    //    }

    //}
   
}

//$("#txt_un1").on({
//    "focus": function (event) {
//        $(event.target).select();
//    },
//    "keyup": function (event) {
//        $(event.target).val(function (index, value) {
//            return value.replace(/\D/g, "")
//                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
//                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
//        });
//    }
//});

//$("#txt_un2").on({
//    "focus": function (event) {
//        $(event.target).select();
//    },
//    "keyup": function (event) {
//        $(event.target).val(function (index, value) {
//            return value.replace(/\D/g, "")
//                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
//                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
//        });
//    }
//});

$("#txtPeso").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value) {
            return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});