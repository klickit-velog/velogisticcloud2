﻿
var id = getParameterByName('id');
var J_user = getParameterByName('J_user');

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function () {
    Inicializar();
});


function Inicializar() {
    $.ajax({
        url: '../Booking/CargarResumen',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {

            switch (response.nTipoMensaje) {
                case 0:
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                case 1: /// Correcto
                    var dato = response.objeto;
                    document.getElementById('lbl_nroBooking').innerHTML = dato.idBooking;
                    document.getElementById('lbl_Cantidad').innerHTML = dato.cantidadBulto;
                    document.getElementById('lbl_etd').innerHTML = dato.valido_Hasta;
                    document.getElementById('lbl_eta').innerHTML = dato.valido_Desde;
                    document.getElementById('lbl_tipoContenedor').innerHTML = dato.tipoContenedor;
                    document.getElementById('lbl_CantSolicitados').innerHTML = dato.cantidadBulto;

                    document.getElementById('lbl_TipoCont').innerHTML = dato.tipoContenedor;
                    document.getElementById('lbl_Comodity').innerHTML = dato.commodity;
                    document.getElementById('lbl_Linea').innerHTML = dato.linea;
                    document.getElementById('lbl_NroContrato').innerHTML = dato.nroContrato;
                    document.getElementById('lbl_PuertoEmbarque').innerHTML = dato.puertoEmbarque;
                    document.getElementById('lbl_PuertoDescarga').innerHTML = dato.puertoDescarga;
                    document.getElementById('lbl_NombreNave').innerHTML = dato.nombreNave;
                    document.getElementById('lbl_NroViaje').innerHTML = dato.nroViaje;


                    document.getElementById('lbl_emision').innerHTML = dato.emisionBL;
                    //document.getElementById('lbl_flete').innerHTML = dato.flete; 

                    document.getElementById('lbl_pais_inicio').innerHTML = dato.puertoEmbarque + ', ' + dato.paisEmbarque;
                    document.getElementById('lbl_tico_contenedor2').innerHTML = dato.tipoContenedor;
                    document.getElementById('lbl_pais_fin').innerHTML = dato.puertoDescarga + ', ' + dato.paisDescarga;

                    if (dato.claseContenedor.trim().toLowerCase() === "reefer") {
                        document.getElementById("div_reefer").style.display = "block";
                        document.getElementById("div_dry").style.display = "none";

                        document.getElementById('lbl_temperatura').innerHTML = dato.temperatura;
                        document.getElementById('lbl_ventilacion').innerHTML = dato.ventilacion;
                        document.getElementById('lbl_humedad').innerHTML = dato.humedad;
                        document.getElementById('lbl_peso').innerHTML = dato.pesoBruto;

                    } else {
                        document.getElementById("div_reefer").style.display = "none";
                        document.getElementById("div_dry").style.display = "block";
                        document.getElementById('lbl_imo').innerHTML = dato.imo;
                        document.getElementById('lbl_un1').innerHTML = dato.uN1;
                        document.getElementById('lbl_un2').innerHTML = dato.uN2;
                        document.getElementById('lbl_peso2').innerHTML = dato.pesoBruto;


                    }

                    // Datos del Exportrador
                    document.getElementById('lbl_ExportadorNombre').innerHTML = dato.exportadorNombre;
                    document.getElementById('lbl_ExportadorDomicilio').innerHTML = dato.exportadorDomicilio;
                    document.getElementById('lbl_ExportadorRuc').innerHTML = dato.exportadorRuc;
                    document.getElementById('lbl_ExportadorContacto').innerHTML = dato.exportadorContacto;
                    document.getElementById('lbl_ExportadorTelefono').innerHTML = dato.exportadorTelefono;
                    document.getElementById('lbl_ExportadorFax').innerHTML = dato.exportadorFax;
                    document.getElementById('lbl_ExportadorCelular').innerHTML = dato.exportadorCelular;
                    document.getElementById('lbl_ExportadorContractOwner').innerHTML = dato.contactOwner;
                    document.getElementById('lbl_ExportadorTipoContrato').innerHTML = dato.tipoContrato;


                    // Datos Consignatario
                    document.getElementById('lbl_ConsignatarioEmpresa').innerHTML = dato.consignatarioNombre;
                    document.getElementById('lbl_ConsignatarioDireccion').innerHTML = dato.consignatarioDireccion;
                    document.getElementById('lbl_ConsignatarioContacto').innerHTML = dato.consignatarioContacto;
                    document.getElementById('lbl_ConsignatarioTelefono').innerHTML = dato.consignatarioTelefono;
                    document.getElementById('lbl_ConsignatarioFax').innerHTML = dato.consignatarioFax;

                    // Datos Notify
                    document.getElementById('lbl_NotifyNombre').innerHTML = dato.notifyNombre;
                    document.getElementById('lbl_NotifyDireccion').innerHTML = dato.notifyDireccion;
                    document.getElementById('lbl_NotifyContacto').innerHTML = dato.notifyContacto;
                    document.getElementById('lbl_NotifyTelefono').innerHTML = dato.notifyTelefono;
                    document.getElementById('lbl_NotifyFax').innerHTML = dato.notifyFax;
                    break;
                case 2:
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                default:
            }

            hideSpinner();
        }
    });
}


function VerReservas() {
    url2 = "../Booking/ListaBooking";
    window.open(url2, '_self');
}

function VerReservasCustomer() {
    url2 = "../Booking/ListaBookingCustomer";
    window.open(url2, '_self');
}
