﻿

$(document).ready(function () {


    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtContactOwner").autocomplete({
        source: "../Maestros/AutoCompleteContactOwner",
        minLength: 2,
        select: function (event, ui) {
            $("#hddContactId").val(ui.item.id);
        },
    });

    //var today = new Date();
    //document.getElementById("dateTo").valueAsDate = sumarDias(today, +10);
    //document.getElementById("dateFrom").valueAsDate = new Date();

    $("#btnBusqueda").click(function () {
        ListaActual();
    });

    //$("#btnSpinner").click(function () {
       
    //});

    Inicializar();

    ListaActual();

    document.getElementById("link_ReservasCustomer").style.color = "black";
    document.getElementById("link_ReservasCustomer").style.fontWeight = "bold";

    hideSpinner2();

});

function Inicializar() {
    var today = new Date();
    //document.getElementById("dateTo").valueAsDate = sumarDias(today, +30);
    document.getElementById("dateFrom").valueAsDate = sumarDias(today, -90); //se retrocede 30 dias de los agregado + 30 dias antes del dia de hoy.
}

function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
}

async function ListaActual() {
    showSpinner2();

    const IdBooking = $("#txt_code").val();
    const NroBooking = $("#txt_nroBooking").val();
    const IdContactOwner = $("#txtContactOwner").val().trim() === "" ? 0 : $("#hddContactId").val();
    const Linea = $("#txtLineaNaviera option:selected").text() === "Selecciona Naviera" ? "" : $("#txtLineaNaviera option:selected").text();
    const NombreNave = $("#txtNombreNave").val();
    const NroViaje = $("#txtNroViaje").val();
    const IdEmbarque = $("#txtPuertoEmbarque").val().trim() === "" ? 0 : $("#hddEmbarqueId").val();
    const IdDescarga = $("#txtPuertoDescarga").val().trim() === "" ? 0 : $("#hddDescargaId").val();
    const IdComodity = $("#txtComodity option:selected").val();
    const DateFrom = $("#dateFrom").val();
    const DateTo = $("#dateTo").val();
    const EstadoId = $("#Cbo_Estado option:selected").val();

    var filtros = {
        IdBooking: IdBooking,
        NroBooking: NroBooking,
        IdContactOwner: IdContactOwner,
        IdLinea: 0,
        Linea: Linea,
        NombreNave: NombreNave,
        NroViaje: NroViaje,
        IdEmbarque: IdEmbarque,
        IdDescarga: IdDescarga,
        IdComodity: IdComodity,
        DateFrom: DateFrom,
        DateTo: DateTo,
        EstadoId: EstadoId
    }
    var tipousuario = $("#tipousuario").val();


    const rest = await
        $.ajax({
        url: '../Booking/ListarBookingCustomer/',
        type: 'POST',
            data: { filtros: filtros, tipousuario: tipousuario },
        success: async function (response) {            
            $("#gridLista").html(response.data);
            document.querySelector('#lblTotal').innerText = 'Total de registros: ' + (response.cantidad);
            Paginacion();
            await hideSpinner();
        }
    });
}

function Paginacion() {
    $('#nav').remove();
    $('#data').after('<nav id="nav" aria-label="..."><ul id="ul" class="pagination pagination-sm"> </ul></nav>');
    var rowsShown = 30;
    var rowsTotal = $('#data tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    var numEntero = Math.trunc(numPages);
    if (numPages > numEntero) {
        numEntero++;
    }

    $('#ul').append('<li id="li_' + (0) + '"  class="page-item"> <a id="' + (0) + '"class="page-link" href="#" rel="' + 0 + '">' + "<span aria-hidden=\"true\">&laquo;</span>" + '</a></li>');
    for (i = 0; i < numEntero; i++) {
        var pageNum = i + 1;
        if (pageNum <= 11) {
            $('#ul').append('<li id="li_' + pageNum + '"  class="page-item"> <a id="' + pageNum + '"class="page-link" href="#" rel="' + i + '">' + pageNum + '</a></li>');
        } else {
            $('#ul').append('<li id="li_' + pageNum + '"  class="page-item"> <a id="' + pageNum + '"class="page-link" href="#" rel="' + i + '">' + pageNum + '</a></li>');
            document.getElementById("li_" + pageNum ).style.display = "none";
        }
    }

    $('#ul').append('<li id="li_' + (numEntero + 1) + '"  class="page-item"> <a id="' + (numEntero + 1) + '"class="page-link" href="#" rel="' + i + '">' + "<span aria-hidden=\"true\">&raquo;</span>" + '</a></li>');

    $('#data tbody tr').hide();
    $('#data tbody tr').slice(0, rowsShown).show();
    $('#li_' + 1).addClass('active');
    //$('#ul li:first').addClass('active');
    $('#ul li a').bind('click', function () {
        $('#ul li').removeClass('active');
        //$(this).find('li').addClass('active');
        var id = $(this).attr('id');
        let inicio;
        inicio = parseInt(id, 10);
        let currPage;
        currPage = $(this).attr('rel');
        //var numEntero2 = Math.trunc(numPages);
        if (inicio === 0) {
            inicio = 1;
            $('#li_' + 1).addClass('active');
            currPage = (1 - 1).toString();
        }else if (inicio === (numEntero + 1)) {
            inicio = numEntero;
            $('#li_' + numEntero).addClass('active');
            currPage = (numEntero - 1).toString();

        } else {
            $('#li_' + id).addClass('active');
        }

        let inicioPag;
        inicioPag = inicio - 5;
        let  finPag;
        finPag = inicio + 5;
        if (finPag > numEntero) {
            finPag = numEntero
            inicioPag = (numEntero - 10)
        }
        if (inicioPag <= 0) {
            inicio = 1;
            inicioPag = 1;
            finPag = 11;
        }
        //if (inicio === 6) {
        //    finPag = inicio + 6;
        //} else {
        //    finPag = inicio + 5;
        //}

        if ( finPag <= (numEntero) ) {
            for (i = 0; i < numEntero; i++) {
                var pageNum = i + 1;
                if (pageNum >= inicioPag && pageNum <= finPag) {
                    document.getElementById("li_" + pageNum).style.display = "block";
                } else {
                    document.getElementById("li_" + pageNum).style.display = "none";
                }
            }
        } else {

        }
    

        var startItem = currPage * rowsShown;
        var endItem = parseInt(startItem, 10) + parseInt(rowsShown, 10);
        $('#data tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function ConfirmarReserva(id) {

    Swal.fire({
        title: '¿Esta Seguro de Confirmar la Reserva.?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            this.showSpinner2()

            $.ajax({
                url: '../Reservas/ConfirmarReserva/',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    hideSpinner2();

                    Swal.fire({
                        icon: 'success',
                        title: 'Actualizacion Correcta',
                        text: 'Se actualizo correctamente!!',
                    });

                    ListaActual();
                }
            });

        } else { }
    })




}

function CancelarBooking(id) {

    Swal.fire({
        title: 'Cancelar Reserva.',
        text: "¿Esta seguro de cancelar la reserva?.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../Booking/CancelarBooking',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {

                    switch (response.nTipoMensaje) {
                        case 0:
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.sMensaje,
                            });
                            break;
                        case 1: /// Correcto
                            Swal.fire({
                                title: 'Cancelacion Exitosa.',
                                text: response.sMensaje ,
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Ok!'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    ListaActual();
                                }
                            });
                            break;
                        case 2:
                            Swal.fire({
                                icon: 'info',
                                title: 'Oops...',
                                text: response.sMensaje,
                            });
                            break;
                        default:
                    }
                }
            });
        } else {

        }
    })
}

async function ConfirmarBookingManual(id, booking, imo) {
    const res = await
        $.ajax({
        url: '../Booking/ConfirmarBookingManual',
        type: 'POST',
        data: { id: id, booking: booking, imo: imo },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
        }
        });
    return res;
}

function ValidarBookingManual(booking) {
    $.ajax({
        url: '../Booking/ValidarBookingManual',
        type: 'POST',
        async: false,
        data: {booking: booking},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato !== '1') {
                Swal.fire({
                    title: 'Booking repetido!',
                    text: "El booking ya se encuentra registrado.",
                    icon: 'info',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        return;
                    } else {
                    }
                })
            }
          
        }
    });
}

async function ConfirmarBooking(id) {
    
    Swal.fire({
        title: 'Confirmar Reserva.',
        text: "¿Esta seguro de confirmar la reserva?.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then(async (result) => {
        if (result.isConfirmed) {

            showSpinner();

            const result2 = await
            $.ajax({
                url: '../Booking/ConfirmarBooking',
                type: 'POST',
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    var dato = response;
                    (async () => {
                        console.log('before start');
                        await hideSpinner();
                        console.log('after start');
                    })();

                    if (dato.result == "1") {
                        Swal.fire({
                            title: 'Confirmación Exitosa.',
                            text: "Se confirmo la reserva correctamente.",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                ListaActual();
                            }
                        });
                    } else if (dato.result == "2") {
                        let imo = "";
                        let booking = "";
                        Swal.fire({
                            title: 'Registrar datos manuales.',
                            html:
                                '<label>Ingrese Imo Nave</label> <input id="swal-input1" placeholder="Ingrese Imo Nave" class="swal2-input" value="'+dato.imo+'">' +
                                '<label>Ingrese Nro de Booking</label> <input id="swal-input2" placeholder="Ingrese Nro de Booking" class="swal2-input">',
                            inputAttributes: {
                                autocapitalize: 'off'
                            },
                            showCancelButton: true,
                            confirmButtonText: 'Grabar!',
                            showLoaderOnConfirm: true,
                            preConfirm: (login) => {
                                imo = document.getElementById('swal-input1').value;
                                nroBooking = document.getElementById('swal-input2').value;
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then(async (result) => {
                            if (result.isConfirmed) {
                                showSpinner();
                                const result3 = await
                                    $.ajax({
                                    url: '../Booking/ValidarBookingManual',
                                    type: 'POST',
                                        data: { nroBooking: nroBooking },
                                    beforeSend: function () { },
                                    success: function (response) {
                                        (async () => {
                                            console.log('before start');
                                            await hideSpinner();
                                            console.log('after start');
                                        })();
                                        var dato = response;
                                        if (dato !== '1') {
                                            Swal.fire({
                                                title: 'Booking repetido!',
                                                text: "El booking ya se encuentra registrado.",
                                                icon: 'info',
                                                allowOutsideClick: false
                                            }).then((result) => {
                                                if (result.isConfirmed) {
                                                } 
                                            })
                                        } else {
                                            Swal.fire({
                                                title: 'Confirmación!',
                                                text: "Se confirmo la reserva correctamente.",
                                                icon: 'success',
                                                allowOutsideClick: false
                                            }).then(async (result) => {
                                                if (result.isConfirmed) {
                                                    showSpinner();
                                                    (async () => {
                                                        console.log('before start');
                                                        await ConfirmarBookingManual(id, nroBooking, imo);
                                                        console.log('after start');
                                                    })();
                                                    ListaActual();
                                                } 
                                            })
                                        }

                                    }
                                });
                            
                            } else {
                            }
                        })
                    }
                }
            });
        } else {

        }
    })
}

function EnvioOdoo(id) {
    Swal.fire({
        title: '¿Esta Seguro de Enviar a Odoo la Reserva con Nro Solicitud : ' + id + ' ?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                title: 'Registrar datos manuales.',
                html:
                    '<label>Ingrese Número Contenedor</label><input id="swal-input3" placeholder="Ingrese Número Contenedor" class="swal2-input">' +
                    '<label>Ingrese Número BL</label><input id="swal-input4" placeholder="Ingrese Número BL" class="swal2-input">',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Grabar!',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    nroContenedor = document.getElementById('swal-input3').value;
                    codigoBL = document.getElementById('swal-input4').value;
                    EnvioOdooAjax(id, nroContenedor, codigoBL);
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                //        if (result.isConfirmed) {
                //            Swal.fire({
                //                title: 'Confirmación!',
                //                text: "Se confirmo la reserva correctamente.",
                //                icon: 'success',
                //                allowOutsideClick: false
                //            }).then((result) => {
                //                if (result.isConfirmed) {
                //                    ListaActual();
                //                } else {
                //                }
                //            })
                //} else { }
            })
        }
    });
}


function UpdateImo(id) {
    Swal.fire({
        title: 'Registrar imo manual.',
        html:
            '<input id="swal-input" placeholder="Ingrese # Imo" class="swal2-input">',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Grabar!',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
            nroImo = document.getElementById('swal-input').value;
            GuardarImoAjax(id, nroImo);
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {

    })
}

function GuardarImoAjax(id, imo) {
    $.ajax({
        url: '../Booking/UpdateImo',
        type: 'POST',
        async: false,
        data: { id: id, imo: imo },
        beforeSend: function () { },
        success: function (response) {

            switch (response.nTipoMensaje) {
                case 0:
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                case 1: /// Correcto
                    var dato = response.objeto;
                    Swal.fire({
                        title: 'Se grabo el Imo correctamente.',
                        text: response.sMensaje,
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            VerMapa(imo);
                            ListaActual();
                        }
                    });
                    break;
                case 2:
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                default:
            }


            var dato = response;
            if (dato === '0') {
                var mensajeError = dato.split('-');
                const Error = mensajeError[1];
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error: - ' + Error + ' Contacte al administrador.',
                });
            } else {
               
            }
         
        }
    });
}


function VerMapa(id) {

    if (id != "0") {
        url2 = "../Alertas/Mapa?imo=" + id;

        //Swal.fire({
        //    url: 'https://localhost:44350/Alertas/Mapa?imo=' + id,
        //    imageHeight: 1500,
        //    imageAlt: 'A tall image'
        //})
        window.open(url2, '_blank');
    } else {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'No se encontro el codigo la Nave!',
        });
    }

}

function EnvioOdooAjax(id, nroContenedor, codigoBL) {
    $.ajax({
        url: '../Booking/EnvioOdoo/',
        type: 'POST',
        async: false,
        data: { id: id, nroContenedor: nroContenedor, codigoBL: codigoBL },
        beforeSend: function () { },
        success: function (response) {

            hideSpinner2();

            switch (response.nTipoMensaje) {
                case 0:
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                case 1: /// Correcto
                    Swal.fire({
                        title: 'Envio Correcto.',
                        text: response.sMensaje,
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            ListaActual();
                        }
                    });
                    break;
                case 2:
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: response.sMensaje,
                    });
                    break;
                default:
            }

        }
    });
}

function PlantillaReserva(id) {
    showSpinner2();

    url2 = "../Booking/Create?id=" + id + "&Tipo=Plantilla";
    window.open(url2, '_self');

    hideSpinner2();

}


function EditReserva(id) {

    url2 = "../Booking/Create?id=" + id + "&Tipo=Editar" + '&J_user=customer';
    window.open(url2, '_self');
}


