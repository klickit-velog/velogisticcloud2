﻿
$(document).ready(function () {
    hideSpinner2();
    
    document.getElementById("link_Configuracion").style.color = "black";
    document.getElementById("link_Configuracion").style.fontWeight = "bold";
});

function ReiniciarCache() {
    Swal.fire({
        title: 'Reiniciar Temporales Odoo',
        text: "¿Esta seguro de reiniciar los temporales con Odoo?.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../Configuracion/CacheRestart',
                type: 'POST',
                async: false,
                beforeSend: function () { },
                success: function (response) {
                    Swal.fire({
                        title: 'Reinicio exitoso.',
                        text: "Se reinició los temporales correctamente.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'
                    });
                }
            });
        } else {

        }
    })

}