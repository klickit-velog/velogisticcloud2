﻿$(document).ready(function () {
    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });
    //ListaVehiculos();
    ListaActual();

    document.getElementById("link_Opl").style.color = "black";
    document.getElementById("link_Opl").style.fontWeight = "bold";

});


function ListaVehiculos() {


    $.ajax({
        url: '../Cotizacion/ListaVehiculos',
        type: 'POST',
        async: false,
        //data: { ent: ent, contenedor: contenedor },
        beforeSend: function () { },
        success: function (response) {

            hideSpinner2();
        }
    });

}

function ListaActual() {
    showSpinner();

    let textoEmbarque = $("#txtPuertoEmbarque").val();
    let textoDescarga = $("#txtPuertoDescarga").val();


    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var param = {
        NroBooking: $("#txt_nroBooking").val(),
        IdPuertoEmbarque: $("#hddEmbarqueId").val(),
        IdPuertoDescarga: $("#hddDescargaId").val(),
        IdEstadoVE: $("#id_cbEstado option:selected").val()
    }
    var tipousuario = $("#tipousuario").val();
    $.ajax({
        url: '../OperacionLogistica2/ListaOperacionLogistica/',
        type: 'POST',
        data: { param: param, tipousuario: tipousuario },
        success: function (response) {
            var dato = response;
            hideSpinner();
            $("#gridLista").html(dato);
            /* validarRedirect(dato);*/ /*add sysseg*/
        }
    });
}


function loadTransporte() {
    const control = 'swal-input2';
    const valSel = 1;
    $('#' + control + ' option').remove();
    $('#' + control).append($("<option />", { value: 0, text: '-- Seleccione --' }));
    $.ajax({
        url: '../OperacionLogistica2/ListarTransporte',
        type: 'POST',
        async: false,
        data: { },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato.result == 1) {
                var datos = dato.data.value;
                datos.forEach(
                    x =>
                        $('#' + control).append($("<option />", { value: x.value, text: x.text }))
                )
            } else {
                alert(dato.message);
            }
        }
    });
}


function ConfirmarOpl(id) {

    Swal.fire({
        title: '¿Esta Seguro de Confirmar la Operacion Logistica.?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {

            ////<input id="swal-input2" placeholder="Ingrese Transporte" class="swal2-input">
            //let OrdenServicio = "";
            //let IdTransporte;
            //let Transporte = ""; 
            //let CanalSini = "";
            //Swal.fire({
            //    title: 'Registrar datos Obligatorios.',
            //    html:
            //        '<input id="swal-input1" placeholder="Ingrese Orden de Servicio" class="swal2-input">' +
            //        '<select id="swal-input2" class="swal2-input"></select> '+
            //    '<select id="swal-input3" placeholder="Ingrese Canal Sini" class="swal2-input form-select"><option value="Verde">Verde</option><option value="Rojo">Rojo</option></select>',
            //    inputAttributes: {
            //        autocapitalize: 'off',
                 
            //    },
            //    showCancelButton: true,
            //    confirmButtonText: 'Grabar!',
            //    showLoaderOnConfirm: true,
            //    preConfirm: (login) => {
            //        OrdenServicio = document.getElementById('swal-input1').value;
            //        IdTransporte = $("#swal-input2 option:selected").val();
            //        Transporte = $("#swal-input2 option:selected").text();
            //        CanalSini = document.getElementById('swal-input3').value;


            //    },
            //    allowOutsideClick: () => !Swal.isLoading()
            //}).then((result) => {
            //    if (result.isConfirmed) {

            $.ajax({
                url: '../OperacionLogistica2/ConfirmarOperacionLogistica/',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {

                    hideSpinner2();

                    Swal.fire({
                        icon: 'success',
                        title: 'Actualizacion Correcta',
                        text: 'Se Confirmo la Operacion Logistica correctamente!!',
                    });
                    ListaActual();
                }
            });

                    //$.ajax({
                    //    url: '../OperacionLogistica2/UpdateDatosObligatorios',
                    //    type: 'POST',
                    //    async: false,
                    //    data: { OrdenServicio: OrdenServicio, Transporte: Transporte, CanalSini: CanalSini, IdTransporte: IdTransporte, id: id },
                    //    beforeSend: function () { },
                    //    success: function (response) {
                    //        var dato = response;
                    //        if (dato == '1') {

                    //        } else {

                    //            Swal.fire({
                    //                title: 'Error!',
                    //                text: "Contacte con el administrador",
                    //                icon: 'error',
                    //                allowOutsideClick: false
                    //            }).then((result) => {
                    //                if (result.isConfirmed) {
                    //                    ListaActual();
                    //                }
                    //            })
                    //        }

                    //    }
                    //});






            //    }
            //});
            //loadTransporte();
            $("#swal-input1").val(id);;
        }
    });
}

function CancelarOpl(id) {

    Swal.fire({
        title: 'Cancelar Reserva.',
        text: "¿Esta seguro de cancelar la Operacion Logistica?.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../OperacionLogistica2/CancelarOperacionLogistica',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    var dato = response;
                    Swal.fire({
                        title: 'Cancelacion Exitosa.',
                        text: "Se cancelo la Operacion Logistica correctamente.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            ListaActual();
                        }
                    });
                }
            });
        } else {

        }
    })
}

function VistaAgregarOP() {
    url2 = "../OperacionLogistica2/Create";
    window.open(url2, '_self');
}

function EditarOpl(id) {
    url2 = "../OperacionLogistica2/Editar?id="+id + "&J_User=Customer";
    window.open(url2, '_self');
}

function VerMapa(OrdenServicio) {
    //P5509
    url2 = "https://velogistics.oss.avanzza.io/trackear-pedido?order=" + OrdenServicio;
    window.open(url2, '_blank');
}

function EnvioOdoo(id) {
    Swal.fire({
        title: '¿Esta seguro de enviar a Odoo el registro : ' + id + ' ?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            let OrdenServicio = "";
            let Transporte = "";
            let CanalSini = "";
            Swal.fire({
                title: 'Registrar datos Obligatorios.',
                html:
                    '<div class="float-left font-weight-bold">Orden de Servicio</div>' +
                    '<input id="swal-input1" placeholder="Ingrese Orden de Servicio" class="swal2-input form-control" readonly>' +
                    '<div class="float-left font-weight-bold">Nombre de Transporte</div>' +
                    '<input id="swal-input2" placeholder="No se encontró un transporte asignado." class="swal2-input form-control" readonly>' +
                    '<div class="float-left font-weight-bold">Canal Sini</div>' +
                    '<select id="swal-input3" placeholder="Ingrese Canal Sini" class="swal2-input form-select"><option value="green">Verde</option><option value="red">Rojo</option></select>',
                inputAttributes: {
                    autocapitalize: 'off',

                },
                showCancelButton: true,
                confirmButtonText: 'Grabar!',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    OrdenServicio = document.getElementById('swal-input1').value;
                    Transporte = document.getElementById('swal-input2').value;
                    CanalSini = document.getElementById('swal-input3').value;
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        url: '../OperacionLogistica2/UpdateDatosObligatorios',
                        type: 'POST',
                        async: false,
                        data: { OrdenServicio: OrdenServicio, Transporte: Transporte, CanalSini: CanalSini, id: id },
                        beforeSend: function () { },
                        success: function (response) {
                            var dato = response;
                            if (dato == '1') {
                                hideSpinner2();

                                Swal.fire({
                                    title: 'Actualizacion Correcta',
                                    text: "Se Confirmo la Operacion Logistica correctamente!!",
                                    icon: 'success',
                                    allowOutsideClick: false
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        ListaActual();
                                    }
                                })
                            } else if (dato == '0') {
                                hideSpinner2();

                                Swal.fire({
                                    title: 'Error!',
                                    text: "Contacte con el administrador",
                                    icon: 'error',
                                    allowOutsideClick: false
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        ListaActual();
                                    }
                                })
                            } else {
                                hideSpinner2();

                                Swal.fire({
                                    title: 'Validacion!',
                                    text: dato,
                                    icon: 'error',
                                    allowOutsideClick: false
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        ListaActual();
                                    }
                                })
                        }

                        }
                    });
                }
            });
            GetTransporte(id);
            $("#swal-input1").val(id);;
        }
    });
}

function GetTransporte(ordenServicio) {
    
    $.ajax({
        url: '../OperacionLogistica2/GetTransporte',
        type: 'GET',
        async: false,
        data: { ordenServicio: ordenServicio},
        beforeSend: function () { },
        success: function (response) {
            if (response.result == 1) {
                $('#swal-input2').val(response.data.value);
                if (response.data.value == "") {
                    $(".swal2-confirm").prop('disabled', true);
                }
                else {
                    $(".swal2-confirm").prop('disabled', false);
                }
            } else {
                alert("Ocurrio un error, contactese con el Administrador");
            }
        }
    });
}