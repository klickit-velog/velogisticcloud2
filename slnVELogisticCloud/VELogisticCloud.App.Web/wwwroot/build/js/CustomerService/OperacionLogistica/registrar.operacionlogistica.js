﻿//$(document).ready(function () {
//    $("#txt_cliente").autocomplete({
//        source: function (request, response) {
//            $.ajax({
//                url: '/OperacionLogistica/Languages',
//                type: 'GET',
//                cache: false,
//                data: request,
//                dataType: 'json',
//                success: function (data) {
//                    response($.map(data, function (item) {
//                        return {
//                            label: item,
//                            value: item + "kk"
//                        }
//                    }))
//                }
//            });
//        },
//        minLength: 2,
//        select: function (event, ui) {
//            alert('you have selected ' + ui.item.label + ' ID: ' + ui.item.value);
//            $('#txtSearch').val(ui.item.label);
//            return false;
//        }
//    });
//});

var dataBooking;

$(document).ready(function () {
    $("#txt_nro_cotizacion").autocomplete({
        source: "../OperacionLogistica/AutoCompleteCotizaciones",
        minLength: 1,

        select: function (event, ui) {

            $("#HidCodCoti").val(ui.item.id);
        },

    });
});
$('#btnInsert').prop('disabled', true);

//function initAutoCompletarCotizacion(control, controlHidden) {
//    $("#" + control).autocomplete({
//        source: "../OperacionLogistica/Languages",
//        minLength: 1,

//        select: function (event, ui) {

//            $("#" + controlHidden).val(ui.item.Id);
//        },

//    });
//}

$("#txt_nro_booking").blur(function () {

    var booking = $("#txt_nro_booking").val();
    $.ajax({
        url: '../OperacionLogistica/ValidarBooking',
        type: 'POST',
        async: false,
        data: { booking: booking },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato === 0) {
                document.getElementById('span_correcto').innerHTML = "Nro Booking Erróneo.";
                document.getElementById('span_correcto').style = "color:red";
                $('#btnInsert').prop('disabled', true);
            } else {
                dataBooking = dato;
                document.getElementById('span_correcto').innerHTML = "Nro Booking Correcto.";
                document.getElementById('span_correcto').style = "color:green";
                $('#btnInsert').prop('disabled', false);

            }
        },

    });
    // tu codigo ajax va dentro de esta function...
});


function formatoFecha(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
}


function InsertarOPL() {

    var ent = {
        IdCotizacionLogistica: $("#HidCodCoti").val(),
        NroCotizacion: $("#txt_nro_cotizacion").val(),
        NroContenedor: $("#txt_nro_contenedor").val(),
        NroBooking: $("#txt_nro_booking").val(),
        CodigoCliente: dataBooking.idCliente,
        Cliente: dataBooking.nombreCliente,
        OrdenServicio: $("#txt_orden_servicio").val(),

        Canal: $("#txt_canal").val(),
        Linea: $("#txt_linea").val(),
        PuertoDestino: $("#txt_puerto_destino").val(),
        Deposito: $("#txt_deposito").val(),

        Producto: $("#txt_producto").val(),
        TipoEmbarque: $("#txt_tipo_embarque").val(),
        FechaServicio: $("#txt_fecha_servicio").val(),
        NroBL: $("#txt_nro_bl").val(),
        NroViaje: dataBooking.numeroViaje,
        ETD: formatoFecha(dataBooking.etD_Origen),
        ETA: formatoFecha(dataBooking.etA_Destino),
        Conductor: $("#txt_conductor").val(),
        Nave: dataBooking.nombreNave,
        ConceptosOPL: $("#txt_concepto_opl").val(),
        Lugar1: $("#txt_lugar1").val(),
        Fecha1: $("#txt_fecha1").val(),
        Hora1: $("#txt_hora1").val(),
        Lugar2: $("#txt_lugar2").val(),
        Fecha2: $("#txt_fecha2").val(),
        Hora2: $("#txt_hora2").val(),
        Lugar3: $("#txt_lugar3").val(),
        Fecha3: $("#txt_fecha3").val(),
        Hora3: $("#txt_hora3").val(),

    }

    showSpinner2();

    $.ajax({ 
        url: '../OperacionLogistica/InsertarOperacionLogistica',
        type: 'POST',
        async: false,
        data: { ent: ent },
        beforeSend: function () { },
        success: function (response) {
            hideSpinner();

            Swal.fire({
                title: 'Registro Correcto.',
                text: "Se registro correctamente.",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    url2 = "../OperacionLogistica/Busqueda";
                    window.open(url2, '_self');
                }
            })
        },
        error: function(response) {
            hideSpinner();
            alert('Ocurrio un error');

        },

    });

}