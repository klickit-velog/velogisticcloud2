﻿$(document).ready(function () {
    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });
    ListaActual();

    document.getElementById("link_Opl_Cliente").style.color = "black";
    document.getElementById("link_Opl_Cliente").style.fontWeight = "bold";
});


function ListaActual() {
    showSpinner2();

    let textoEmbarque = $("#txtPuertoEmbarque").val();
    let textoDescarga = $("#txtPuertoDescarga").val();


    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var param = {
        NroBooking: $("#txt_nroBooking").val(),
        IdPuertoEmbarque: $("#hddEmbarqueId").val(),
        IdPuertoDescarga: $("#hddDescargaId").val(),
        IdEstadoVE: $("#id_cbEstado option:selected").val()
    }
    var tipousuario = $("#tipousuario").val();

    $.ajax({
        url: '../OperacionLogistica2/ListaOperacionLogisticaCliente/',
        type: 'POST',
        async: false,
        data: { param: param, tipousuario: tipousuario },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridLista").html(dato);
            /* validarRedirect(dato);*/ /*add sysseg*/
            ;
        }
    });

    hideSpinner2();
}

function ConfirmarOpl(id) {

    Swal.fire({
        title: '¿Esta Seguro de Confirmar la Operacion Logistica.?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            showSpinner2();

            $.ajax({
                url: '../OperacionLogistica2/ConfirmarOperacionLogistica/',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {

                    hideSpinner2();

                    Swal.fire({
                        icon: 'success',
                        title: 'Actualizacion Correcta',
                        text: 'Se Confirmo la Operacion Logistica correctamente!!',
                    });
                    ListaActual();
                }
            });

        } else { }
    })




}

function CancelarOpl(id) {

    Swal.fire({
        title: 'Cancelar Operación Logística.',
        text: "¿Esta seguro de cancelar la Operación Logística?.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../OperacionLogistica2/CancelarOperacionLogistica',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    var dato = response;
                    Swal.fire({
                        title: 'Cancelacion Exitosa.',
                        text: "Se cancelo la Operacion Logistica correctamente.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            ListaActual();
                        }
                    });
                }
            });
        } else {

        }
    })
}

function VistaAgregarOP() {
    url2 = "../OperacionLogistica2/Create";
    window.open(url2, '_self');
}

function EditReserva(id) {
    url2 = "../OperacionLogistica2/Editar?id="+id;
    window.open(url2, '_self');
}

function VerMapa(OrdenServicio) {
    //P5509
    url2 = "https://velogistics.oss.avanzza.io/trackear-pedido?order=" + OrdenServicio;
    window.open(url2, '_blank');
}
