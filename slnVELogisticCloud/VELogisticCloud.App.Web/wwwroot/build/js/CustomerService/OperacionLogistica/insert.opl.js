﻿
var id = getParameterByName('id');
var J_User = $("#tipousuario").val();
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function () {

    DetalleCotizacion();
    $("#txtPesoCaja").on({
        "focus": function (event) {
            $(event.target).select();
        },
        "keyup": function (event) {
            $(event.target).val(function (index, value) {
                return value.replace(/\D/g, "")
                    .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            });
        }
    });

    $("#txtPesoNeto").on({
        "focus": function (event) {
            $(event.target).select();
        },
        "keyup": function (event) {
            $(event.target).val(function (index, value) {
                return value.replace(/\D/g, "")
                    .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            });
        }
    });

    $("#txtCantidadTermoregistros").on({
        "focus": function (event) {
            $(event.target).select();
        },
        "keyup": function (event) {
            $(event.target).val(function (index, value) {
                return value.replace(/\D/g, "")
                    .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            });
        }
    });

    $("#txtCantidadFiltrosEtileno").on({
        "focus": function (event) {
            $(event.target).select();
        },
        "keyup": function (event) {
            $(event.target).val(function (index, value) {
                return value.replace(/\D/g, "")
                    .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            });
        }
    });

    $("#hddIdOdooService").val(id);

    //$("#txtPuertoEmbarqueInttra").autocomplete({
    //    source: "../Maestros/AutoCompletePuertosPais",
    //    minLength: 2,
    //    select: function (event, ui) {
    //        $("#hddEmbarqueIdInttra").val(ui.item.id);
    //        $("#hddEmbarqueCodigoPuertoInttra").val(ui.item.codigo);
    //        $("#hddEmbarqueCodigoPaisInttra").val(ui.item.codigoPais);
    //        $("#hddEmbarqueNombrePaisInttra").val(ui.item.nombrePais);
    //    },
    //});
});

//loadProveedor('ddlProveedor', '0', 'C');
//loadBoxes('','','')
LoadValores('ddlIncoterm', '0', 'Incoterm')

function LoadValores(control, valSel, grupo) {

    $('#' + control + ' option').remove();
    $('#' + control).append($("<option />", { value: 0, text: '---Seleccione---' }));
    $.ajax({
        url: '../OperacionLogistica2/ListaValores',
        type: 'POST',
        async: false,
        data: { grupo: grupo },
        beforeSend: function (idDependencia) { },
        success: function (response) {
            var dato = response;
            if (dato.length > 0) {
                var datos = dato;
                $.each(datos, function (indice, lista) {
                    if (lista.Value == valSel)
                        $('#' + control).append($("<option />", { value: lista.cod, text: lista.descripcion, selected: true }));
                    else
                        $('#' + control).append($("<option />", { value: lista.cod, text: lista.descripcion }));
                });
            } else {
                alert(dato.message);
            }
        }
    });
    hideSpinner();
}

function formatoFechaAnioMesDia(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$1/$2/$3');
}


function DetalleCotizacion() {
    $.ajax({
        url: '../OperacionLogistica2/DetalleCotizacion',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#txtPuertoEmbarqueInttra").val(dato[0].nombreTerminalRetiroVacio);
            $("#txtDepositoVacios").val(dato[0].nombreTerminalDepositoVacio);
        },
    });
}

function ValidarBooking() {
    var booking = $("#txtBooking").val();
    $.ajax({
        url: '../OperacionLogistica2/ValidarBooking',
        type: 'POST',
        async: false,
        data: { booking: booking},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;

            if (dato.idBooking) {
                
                $("#txtPuertoEmbarque").val(dato.puertoEmbarque);
                $("#hddEmbarqueId").val(dato.idPuertoEmbarque);
                $("#hddEmbarqueCodigoPuerto").val(dato.codigoPuertoEmbarque);
                $("#hddEmbarqueCodigoPais").val(dato.codigoPaisEmbarque);
                $("#hddEmbarqueNombrePais").val(dato.paisEmbarque);

                $("#txtPuertoDescarga").val(dato.puertoDescarga);
                $("#hddDescargaId").val(dato.idPuertoDescarga); 
                $("#hddDescargaCodigoPuerto").val(dato.codigoPuertoDescarga);
                $("#hddDescargaCodigoPais").val(dato.codigoPaisDescarga);
                $("#hddDescargaNombrePais").val(dato.paisDescarga);
                document.getElementById("txtLineaNaviera").value = dato.codLinea;
                var fechavistaETD = formatoFechaAnioMesDia(dato.etd);
                $("#txtETD").val(fechavistaETD.replace("T00:00:00", ""));
                $("#txtNombreNave").val(dato.nombreNave);
                document.getElementById('span_correcto').innerHTML = "# Booking Correcto.";
                document.getElementById('span_correcto').style = "color:green";

            } else {
                $("#txtPuertoEmbarque").val("");
                $("#hddEmbarqueId").val("");
                $("#hddEmbarqueCodigoPuerto").val("");
                $("#hddEmbarqueCodigoPais").val("");
                $("#hddEmbarqueNombrePais").val("");

                $("#txtPuertoDescarga").val("");
                $("#hddDescargaId").val("");
                $("#hddDescargaCodigoPuerto").val("");
                $("#hddDescargaCodigoPais").val("");
                $("#hddDescargaNombrePais").val("");
                document.getElementById("txtLineaNaviera").value = "0";
                //var fechavistaETA = formatoFechaAnioMesDia(dato.eta);
                $("#txtETD").val("");
                $("#txtNombreNave").val("");
                document.getElementById('span_correcto').innerHTML = "# Booking Incorrecto.";
                document.getElementById('span_correcto').style = "color:red";


            }

              
        },

    });
}

$("#txtBooking").blur(function () {

    //if (J_user === 'customer') {
    showSpinner2();
    ValidarBooking();
    hideSpinner();

    //} else {
    //}
});

function formatoFecha(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
}

function ValidarPuertoEmbarqueYSalida(puerto1, puerto2) {
    if (puerto1 == puerto2) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Seleccione diferentes puertos!',
        });
        return '0';

    } else {
        return '1';
    }
}

function InsertOperacionLogistica() {

    Swal.fire({
        title: 'Registrar Operacion Logistica.',
        text: "¿Esta seguro de registrar la Op?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
    }).then((result) => {
        if (result.isConfirmed) {
            //var etd_valida = $("#txtETD").val()
            var eta_valida = $("#txtETD").val();
             
            if (eta_valida.trim() != "") {

                //Datos Exportador
                var NroBooking = $("#txtBooking").val();

                var IdLinea = $("#txtLineaNaviera option:selected").val();
                var CodLinea = $("#txtLineaNaviera option:selected").val();
                var Linea = $("#txtLineaNaviera option:selected").text();
                var NombreNave = $("#txtNombreNave").val();
                //var ETA = formatoFecha($("#txtETD").val());
                var ETA = $("#txtETD").val();
                var CodigoPaisEmbarque = $("#hddEmbarqueCodigoPais").val();
                var PaisEmbarque = $("#hddEmbarqueNombrePais").val();
                var IdPuertoEmbarque = $("#hddEmbarqueId").val();
                var CodigoPuertoEmbarque = $("#hddEmbarqueCodigoPuerto").val();
                var PuertoEmbarque = $("#txtPuertoEmbarque").val();
                var CodigoPaisDescarga = $("#hddDescargaCodigoPais").val();
                var PaisDescarga = $("#hddDescargaNombrePais").val();
                var IdPuertoDescarga = $("#hddDescargaId").val();
                var CodigoPuertoDescarga = $("#hddDescargaCodigoPuerto").val();
                var PuertoDescarga = $("#txtPuertoDescarga").val();

                var MarcasProducto = $("#txtMarcaProducto").val();
                var PesoCaja = $("#txtPesoCaja").val();
                var CantidadPaletas = $("#txtCantidadPaletas").val();
                var PesoNeto = $("#txtPesoNeto").val(); 
                var ValorFob = $("#txtValorFOB").val();
                var IdIncoterm = 0;
                var CodIncoterm = $("#ddlIncoterm option:selected").val();
                var Incoterm = $("#ddlIncoterm option:selected").text();
                var CantidadTermoregistros = $("#txtCantidadTermoregistros").val();
                var CantidadFiltrosEtileno = $("#txtCantidadFiltrosEtileno").val();
                var Observaciones = $("#txtObservaciones").val();
                var IdOrigenProducto = 0;
                var OrigenProducto = $("#txtLugarOrigen").val();
                //var CitaPacking = formatoFecha($("#txtCitaPacking").val());
                var CitaPacking = $("#txtCitaPacking").val();
                //var HoraInspeccionSenasa = formatoFecha($("#txtFechaSenasa").val());
                var HoraInspeccionSenasa = $("#txtFechaSenasa").val();
                var RazonSocialPacking = $("#txtRazonSocialPacking").val();
                var RucPacking = $("#txtRucPacking").val();
                var DireccionPacking = $("#txtDireccionPacking").val();
                var Drawback = $("#cbo_DrawBack option:selected").val(); 
                var IdDepositoVacio = 0 ;
                var DepositoVacio = $("#txtDepositoVacios").val();

                var CodigoPaisEmbarqueInttra = $("#txtPuertoEmbarqueInttra").val();
                var PaisEmbarqueInttra = $("#txtPuertoEmbarqueInttra").val();
                var IdPuertoEmbarqueInttra = $("#txtPuertoEmbarqueInttra").val();
                var CodigoPuertoEmbarqueInttra = $("#txtPuertoEmbarqueInttra").val();
                var PuertoEmbarqueInttra = $("#txtPuertoEmbarqueInttra").val();
                var IdCotizacionOddo = id;

                var IdCondicionPago = $("#txtCondicionPago option:selected").val();
                var CondicionPago = $("#txtCondicionPago option:selected").text(); 

                //var CanalSini = $("#cboCanalSini option:selected").text();

                //var IdTransporte = $("#txtTransporte option:selected").val();
                //var Transporte = $("#txtTransporte option:selected").text();

                var Nombre = $("#txtNombre").val();
                var IdConsigne = 0;
                var TelefonoConsigne = $("#txtTelefonoConsigne").val();

                var ent = {
                NroBooking                  :    NroBooking              ,
                IdLinea                     :    IdLinea                 ,
                CodLinea                    :    CodLinea                ,
                Linea                       :    Linea                   ,
                NombreNave                  :    NombreNave              ,
                ETA                         :    ETA                     ,
                CodigoPaisEmbarque          :    CodigoPaisEmbarque      ,
                PaisEmbarque                :    PaisEmbarque            ,
                IdPuertoEmbarque            :    IdPuertoEmbarque        ,
                CodigoPuertoEmbarque        :    CodigoPuertoEmbarque    ,
                PuertoEmbarque              :    PuertoEmbarque          ,
                CodigoPaisDescarga          :    CodigoPaisDescarga      ,
                PaisDescarga                :    PaisDescarga            ,
                IdPuertoDescarga            :    IdPuertoDescarga        ,
                CodigoPuertoDescarga        :    CodigoPuertoDescarga    ,
                PuertoDescarga              :    PuertoDescarga          ,
                MarcasProducto              :    MarcasProducto          ,
                PesoCaja                    :    PesoCaja                ,
                CantidadPaletas             :    CantidadPaletas         ,
                CondicionPago               :    CondicionPago           ,
                PesoNeto                    :    PesoNeto                ,
                ValorFob                    :    ValorFob                ,
                IdIncoterm                  :    IdIncoterm              ,
                CodIncoterm                 :    CodIncoterm             ,
                Incoterm                    :    Incoterm                ,
                CantidadTermoregistros      :    CantidadTermoregistros  ,
                CantidadFiltrosEtileno      :    CantidadFiltrosEtileno  ,
                Observaciones               :    Observaciones           ,
                IdOrigenProducto            :    IdOrigenProducto        ,
                OrigenProducto              :    OrigenProducto          ,
                CitaPacking                 :    CitaPacking             ,
                HoraInspeccionSenasa        :    HoraInspeccionSenasa    ,
                RazonSocialPacking          :    RazonSocialPacking      ,
                RucPacking                  :    RucPacking              ,
                DireccionPacking            :    DireccionPacking        ,
                Drawback                    :    Drawback                ,
                IdDepositoVacio             :    IdDepositoVacio         ,
                DepositoVacio               :    DepositoVacio           ,
                CodigoPaisEmbarqueInttra    :   CodigoPaisEmbarqueInttra  ,
                PaisEmbarqueInttra          :   PaisEmbarqueInttra	,
                IdPuertoEmbarqueInttra      :   IdPuertoEmbarqueInttra	,
                CodigoPuertoEmbarqueInttra  :   CodigoPuertoEmbarqueInttra	,
                PuertoEmbarqueInttra        :   PuertoEmbarqueInttra,
                IdCotizacionOddo            :   IdCotizacionOddo,
                //CanalSini                   :   CanalSini,
                //    Transporte: Transporte,
                    IdCondicionPago: IdCondicionPago,
                    //IdTransporte: IdTransporte,
                    Nombre: Nombre,
                    IdConsigne: IdConsigne,
                    TelefonoConsigne: TelefonoConsigne
                }

                const time1 = $("#txtHoraPacking").val(); 
                const time2 = $("#txtHoraSenasa").val(); 

                var valor1 = $("#hddEmbarqueCodigoPuerto").val();
                var valor2 = $("#hddDescargaCodigoPuerto").val();
                var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);
                var tipousuario = $("#tipousuario").val();
                if (val1 == '1') {
                    showSpinner2();

                    $.ajax({
                        url: '../OperacionLogistica2/InsertarOperacionLogistica',
                        type: 'POST',
                        async: false,
                        data: { ent: ent, time1: time1, time2: time2, tipousuario: tipousuario},
                        beforeSend: function () { },
                        success: function (response) {
                            hideSpinner();

                            var dato = response;
                            if (dato != "0") {

                                Swal.fire({
                                    title: 'Registro Correcto.',
                                    text: "Se registro la reserva correctamente.",
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok!'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        if (J_User == "customer") {
                                            url2 = "../OperacionLogistica2/Busqueda";
                                        } else {
                                            url2 = "../OperacionLogistica2/BusquedaCliente";
                                        }
                                        window.open(url2, '_self');
                                    } else {

                                    }
                                })
                            } else {
                                var mensajeError = dato.split('-');
                                const Error = mensajeError[1];
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Error: - ' + Error + ' Contacte al administrador.',
                                });
                            }

                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: 'Error Puertos!',
                    })
                }

            } else {
                Swal.fire({
                    icon: 'info',
                    title: 'Oops...',
                    text: 'No puede generar una reserva sin un ETD o ETA!',
                })
            }
        }
    });

}