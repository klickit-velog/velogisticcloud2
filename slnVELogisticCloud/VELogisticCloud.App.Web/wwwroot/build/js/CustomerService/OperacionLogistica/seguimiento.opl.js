﻿


var id = getParameterByName('id');

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


SeguimientoOpl();

function SeguimientoOpl() {

    showSpinner2();

    $.ajax({
        url: '../OperacionLogistica2/ListaSeguimiento',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
            hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
            ;
        }
    });

}