﻿
var id = getParameterByName('id');

DatosParaEditar();
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



function DatosParaEditar() {
    showSpinner2();

    $.ajax({
        url: '../OperacionLogistica/DatosParaEditar',
        type: 'POST', 
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#txt_nro_contendor").val(dato.numeroContenedor);
            $("#txt_campania").val(dato.commodity);
            $("#txt_cliente").val(dato.cliente);
            $("#txt_booking").val(dato.nroBooking);
            $("#txt_orden_servicio").val(dato.ordenServicio);
            $("#txt_nro_viaje").val(dato.nroViaje);
            $("#txt_canal").val(dato.canal);
            $("#txt_linea").val(dato.linea);
            $("#txt_deposito").val(dato.deposito);
            $("#txt_packing").val(dato.packing);
            $("#txt_operador_logistico").val(dato.operadorLogistico);
            $("#txt_bl").val(dato.nroBL);
            $("#txt_nave").val(dato.nave);
            $("#txt_puerto_destino").val(dato.puertoDestino);
            $("#txt_etd").val(dato.etd);
            $("#txt_eta").val(dato.eta);
            $("#txt_almacen_retiro").val(dato.almacenRetiro);
            $("#txt_almacen_ingreso").val(dato.almacenIngreo);
             document.getElementById("cbo_senasa").value = dato.val_senasa;
             document.getElementById("cbo_aduana").value = dato.val_aduana;
            document.getElementById("cbo_embarque").value = dato.val_embarque; 
            document.getElementById("cbo_Commodity").value = dato.codigoCommodity; 
            document.getElementById("cbo_estado").value = dato.idEstadoOddo; 
            $("#txt_conpectosOp").val(dato.conceptosOPL);
            $("#txt_canal").val(dato.canal);
            
            hideSpinner2();

        }
    });
}


function EditarOPL() {

    var ent = {
        IdOperacionLogistica: id,
        NroContenedor: $("#txt_nro_contendor").val(),
        NroBooking: $("#txt_booking").val(),
        OrdenServicio: $("#txt_orden_servicio").val(),
        CodigoCommodity: $("#cbo_Commodity option:selected").val(),
        Commodity: $("#cbo_Commodity option:selected").text(),
        MovSenasa: $("#cbo_senasa option:selected").val(),
        MovAduana: $("#cbo_aduana option:selected").val(),
        ServicioEmbarque: $("#cbo_embarque option:selected").val(),
        ConceptosOPL: $("#txt_conpectosOp").val(),
        IdEstadoOddo: $("#cbo_estado option:selected").val(),
        EstadoOddo: $("#cbo_estado option:selected").text()
    }

    $.ajax({
        url: '../OperacionLogistica/UpdateOperacionLogistica',
        type: 'POST',
        async: false,
        data: { ent: ent },
        beforeSend: function () { },
        success: function (response) {
            Swal.fire({
                title: 'Registro Correcto.',
                text: "Se registro correctamente.",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.isConfirmed) {
                    url2 = "../OperacionLogistica/Busqueda";
                    window.open(url2, '_self');
                }
            })
        }
    });

}