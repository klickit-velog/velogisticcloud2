﻿$(document).ready(function () {
    ListaActual();
});

function ListaActual() {

    showSpinner2();

    var ent = {
        Comodity: $("#txtcommodity_parametro").val(),
        Naviera: $("#txtNaviera_parametro").val(),
        PuertoDescarga: $("#txtDestino_parametro").val(),
        PuertoEmbarque: $("#txtPuertoSalida_paramtetro").val(),
        Id_Estado_Reserva:  document.getElementById("id_cbEstado").value
    }
    //alert(ent.id_estado);
    $.ajax({
        url: '../Reservas/ListaReservas_CustomerService/',
        type: 'POST',
        async: false,
        data: { ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridLista").html(dato);

            hideSpinner2();

            /* validarRedirect(dato);*/ /*add sysseg*/
            ;
        }
    });

}


function ConfirmarReserva(id) {

    Swal.fire({
        title: '¿Esta Seguro de Confirmar la Reserva.?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            showSpinner2();

            $.ajax({
                url: '../Reservas/ConfirmarReserva/',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    hideSpinner2();

                    Swal.fire({
                        icon: 'success',
                        title: 'Actualizacion Correcta',
                        text: 'Se actualizo correctamente!!',
                    });
                    ListaActual();
                }
            });

        } else {  }
    })


   

}


function RechazarReserva(id) {

    Swal.fire({
        title: '¿Esta Seguro de Rechazar la Reserva ' + id+'.?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            showSpinner2();

            $.ajax({
                url: '../Reservas/RechazarReserva/',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    hideSpinner2();

                    Swal.fire({
                        icon: 'success',
                        title: 'Actualizacion Correcta',
                        text: 'Se actualizo correctamente!!',
                    });
                    ListaActual();
                }
            });

        } else {}
    })

}


function EnvioOdoo(id) {


    Swal.fire({
        title: '¿Esta Seguro de Enviar a Odoo la Reserva con Nro Solicitud : ' + id + ' ?',
        text: "No se puede revertir los cambios.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Confirmar!'
    }).then((result) => {
        if (result.isConfirmed) {
            showSpinner2();

            $.ajax({
                url: '../Reservas/EnvioOdoo/',
                type: 'POST',
                async: false,
                data: { id: id },
                beforeSend: function () { },
                success: function (response) {
                    hideSpinner2();

                    var dato = response;
                    if (dato === '1') {
                        Swal.fire({
                            icon: 'success',
                            title: 'Envio Correcto',
                            text: 'Se envio correctamente!!',
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Contacte con el administrador!',
                        });
                    }
                   
                    ListaActual();
                }
            });

        } else { }
    })
}


function EditReserva(id) {
    $.ajax({
        url: '../ListaReservas/GuardarCodigo_EditarReserva',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            url2 = "../ListaReservas/EditarReserva?id=" + id + '&J_user=customer';
            window.open(url2, '_self');
        }
    });

} 


