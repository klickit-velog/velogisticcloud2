﻿$(function () {
    $("#loaderbody").addClass('hide');

    $(document).bind('ajaxStart', function () {
        //$("#loaderbody").removeClass('hide');
        //showSpinner2();
    }).bind('ajaxStop', function () {
        //$("#loaderbody").addClass('hide');
        //hideSpinner();

    });
    hideSpinner();
});

showInPopup = (url, title) => {
        showSpinner2();

    var naviera = $("#txtLineaNaviera option:selected").val();
    var codigoPuertoCarga = $("#hddEmbarqueCodigoPuerto").val();
    var codigoPuertoDescarga = $("#hddDescargaCodigoPuerto").val();
    $.ajax({
        type: 'GET',
        data: { sacs: naviera, originPort: codigoPuertoCarga, destinationPort: codigoPuertoDescarga },
        url: url,
        success: function (res) {
            $('#form-modal .modal-body').html(res);
            $('#form-modal .modal-title').html(title);
            $('#form-modal').modal('show');
        hideSpinner();

        }
    })
}

getItinerarioSeleccionado = (ETD, ETA, vesselName, voyageNumber,idItinerarioManual) => {

    try {
        const valido_desde = $('#hddValidoDesde').val();
        const valido_hasta = $('#hddValidoHasta').val();
        if (valido_hasta >= ETD && valido_desde <= ETD) {
            $('#txtETD').val(ETD);
            $('#txtETA').val(ETA);
            $('#txtNombreNave').val(vesselName);
            $('#txtNaveViaje').val(voyageNumber);
            $('#IdItinerarioManual').val(idItinerarioManual);
            $('#form-modal .modal-body').html('');
            $('#form-modal .modal-title').html('');
            $('#form-modal').modal('hide');
            //alert("CORRECTO");
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Fecha ETD fuera de rango de cotización valida',
            });
        }
       
        //return false;
    } catch (ex) {
        console.log(ex)
    }
}