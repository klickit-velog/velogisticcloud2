﻿

var id = getParameterByName('id');

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


SeguimientoReserva();

function SeguimientoReserva() {

    showSpinner2();

    $.ajax({
        url: '../ResumenReserva/ListaSeguimiento',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
            hideSpinner2();
           
        }
    });

}


function PlantillaReserva() {
    $.ajax({
        url: '../ListaReservas/GuardarCodigo_EditarReserva',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            url2 = "../ListaReservas/CreateReserva_Plantilla";
            window.open(url2, '_blank');
            ;
        }
    });


}