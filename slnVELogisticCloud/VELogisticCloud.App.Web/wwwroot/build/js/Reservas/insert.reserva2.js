﻿
$(document).ready(function () {
    hideSpinner();
    //hideLoading();

    contenedor = new Array();
    /*
    $("#txtCantidad").val(1);
    $("#txtTipoContenedor").val("");
    $("#txtComodity").val("");
    $("#txtDescrip_Carga").val("");
    $("#txtNaviera").val("");
    $("#txtNroContrato").val("");
    $("#txtExpeditor").val("");
    $("#txtPuertoEmbarque").val("");
    $("#txtConsignatario").val("");
    $("#txtPuertoDescarga").val("");
    $("#txtETD").val("");
    $("#txtETA").val("");
    $("#txtNombreNave").val("");
    $("#txtNaveViaje").val("");
    $("#txtTemperatura").val("");
    $("#txtVentilacion").val("");
    $("#txtHumedad").val("");
    $("#txtPeso").val("");
    $("#txtVolumen").val("");
    $("#txtFlete").val("");
    $("#txtEmision_BL").val("");

    document.getElementById("txtNombreNave").value = "0"; 
    document.getElementById("txtTipoContenedor").value = "0"; 
    document.getElementById("txtNaviera").value = "0"; 
    document.getElementById("txtPuertoEmbarque").value = "0"; 
    document.getElementById("txtPuertoDescarga").value = "0"; 
    document.getElementById("txtExpeditor").value = "0"; 
    document.getElementById("txtConsignatario").value = "0"; 
    document.getElementById("txtComodity").value = "0"; 
    */

    //$("txtNombreNave").prop('disabled', true);
    //$("txtTipoContenedor").prop('disabled', true);
    $("#txtNaviera").prop('disabled', true);
    $("#txtPuertoEmbarque").prop('disabled', true);
    $("#txtPuertoDescarga").prop('disabled', true);
    $("#txtETD").prop('disabled', true);
    $("#txtETA").prop('disabled', true);
    $("#txtNaveViaje").prop('disabled', true);
    $("#txtNombreNave").prop('disabled', true);

    $("#txtExpeditor").prop('disabled', true);
    $("#txtConsignatario").prop('disabled', true);
    
    
    //$("txtExpeditor").prop('disabled', true);
    //$("txtConsignatario").prop('disabled', true);
    //$("txtComodity").prop('disabled', true); 

    //Loading(6).init();

});

function formatoFecha(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
}

function InsertReserva() {
    var etd_valida = $("#txtETD").val()
    var eta_valida = $("#txtETA").val()

    if (etd_valida.trim() != "" && eta_valida.trim() != "") {


        var CodigosNave = $("#txtNombreNave").val();
        var resCodigosNave = CodigosNave.split("-");

        var CodigosEmbarque = $("#txtPuertoEmbarque").val();
        var resCodigosEmbarque = CodigosEmbarque.split("-");

        var CodigosDescarga = $("#txtPuertoDescarga").val();
        var resCodigosDescarga = CodigosDescarga.split("-");

        //var codigo = {
        //    Id_Naviera: document.getElementById("txtNombreNave").value,
        //    Id_Contenedor: document.getElementById("txtTipoContenedor").value,
        //    Codigo_Nave: resCodigosNave[0],
        //    CallSign_Nave: resCodigosNave[1],
        //    Codigo_PuertoEmbarque: resCodigosEmbarque[0],
        //    CodigoPais_PuertoEmbarque: resCodigosEmbarque[1],
        //    Codigo_PuertoDescarga: resCodigosDescarga[0],
        //    CodigoPais_PuertoDescarga: resCodigosDescarga[1],
        //    Codigo_Consignatario: document.getElementById("txtConsignatario").value,
        //    Direccion_Consignatario :"calle Consignatario",
        //    Codigo_Comodity: document.getElementById("txtComodity"),
        //    Codigo_Expeditor: document.getElementById("txtExpeditor").value,
        //    Direccion_Expeditor: "calle Expeditor"
        //}



        var ent = {
            Naviera: $("#txtNaviera option:selected").text(),
            Nro_contrato: $("#txtNroContrato").val(),
            Expeditor: $("#txtExpeditor option:selected").text(),
            Consignatario: $("#txtConsignatario option:selected").text(),
            Puerto_Embarque: $("#txtPuertoEmbarque option:selected").text(),
            Puerto_Descarga: $("#txtPuertoDescarga option:selected").text(),
            ETD: formatoFecha($("#txtETD").val()),
            ETA: formatoFecha($("#txtETA").val()),
            Nombre_Nave: $("#txtNombreNave").val(),
            Nave_Viaje: $("#txtNaveViaje").val(),
            Cantidad: $("#txtCantidad").val(),
            Tipo_Contenedor: $("#txtTipoContenedor option:selected").text(),
            Commodity: $("#txtComodity option:selected").text(),
            Descripcion_Carga: $("#txtDescrip_Carga").val(),
            Temperatura: $("#txtTemperatura").val(),
            Ventilacion: $("#txtVentilacion").val(),
            Humedad: $("#txtHumedad").val(),
            Peso: $("#txtPeso").val(),
            Volumen: $("#txtVolumen").val(),
            Flete: $("#txtFlete").val(),
            Emision_BL: $("#txtEmision_BL").val(),


            Id_Naviera: $("#txtNaviera").val(),
            Id_Contenedor: $("#txtTipoContenedor").val(),
            Codigo_Nave: resCodigosNave[0],
            CallSign_Nave: resCodigosNave[1],
            Codigo_Puerto_Embarque: resCodigosEmbarque[0],
            Codigo_Pais_Embarque: resCodigosEmbarque[1],
            Codigo_Puerto_Descarga: resCodigosDescarga[0],
            Codigo_Pais_Descarga: resCodigosDescarga[1],
            Codigo_Consignatario: $("#txtConsignatario").val(),
            Direccion_Consignatario: "calle Consignatario",
            Codigo_Commodity: $("#txtComodity").val(),
            Codigo_Expeditor: $("#txtExpeditor").val(),
            Direccion_Expeditor: "calle Expeditor",
            Pais_Puerto_Descarga: resCodigosDescarga[2],
            Pais_Puerto_Embarque: resCodigosEmbarque[2]
        }

        var valor1 = $("#txtPuertoDescarga option:selected").text();
        var valor2 = $("#txtPuertoEmbarque option:selected").text();
        var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);

        if (val1 == '1') {
            showSpinner2();

            $.ajax({
                url: '../Itinerario_Solicitar/InsertarDatos',
                type: 'POST',
                async: false,
                data: { ent: ent, contenedor: contenedor },
                beforeSend: function () { },
                success: function (response) {
                    var dato = response;
                    if (dato != "0") {

                        Swal.fire({
                            title: 'Registro Correcto.',
                            text: "Se registro la reserva correctamente.",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                url2 = "../Itinerario_Solicitar/Resumen?id=" + dato;
                                window.open(url2, '_self');
                            } else {
                                url2 = "../Itinerario_Solicitar/Resumen?id=" + dato;
                                window.open(url2, '_self');
                            }
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Contacte con el administrador!',
                        })
                    }

                }
            });
        }
    } else {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'No puede generar una reserva sin un ETD o ETA!',
        })
    }

}




function Inicializar() {
    $.ajax({
        url: '../Itinerario_Solicitar/Inicializar',
        type: 'POST',
        async: false,
        data: { contenedor: contenedor },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            ;
        }
    });
}




function añadirContenedor() {
    //elementos = parseInt(prompt("¿ Cuantos productos quieres añadir ?"));

    var Cantidad = $("#txtCantidad").val();
    var TipoContenedor = $("#txtTipoContenedor option:selected").text();
    
    var Id_Contenedor = document.getElementById("txtTipoContenedor").value;


    var Comodity = $("#txtComodity option:selected").text();
    var Codigo_Comodity = document.getElementById("txtComodity").value;


    var Descrip_Carga = $("#txtDescrip_Carga").val();

    var Temperatura = $("#txtTemperatura").val();
    var Ventilacion = $("#txtVentilacion").val();
    var Humedad = $("#txtHumedad").val();
    var Peso = $("#txtPeso").val();
    var Volumen = $("#txtVolumen").val();

    if (Cantidad != "" && Temperatura != "" && TipoContenedor != "" && Ventilacion != "" && Comodity != "" && Humedad != "" && Descrip_Carga != "" && Peso != "" && Volumen != "") {
        contenedor.push({ Cantidad: Cantidad, TipoContenedor: TipoContenedor, Comodity: Comodity, Descrip_Carga: Descrip_Carga, Temperatura: Temperatura, Ventilacion: Ventilacion, Humedad: Humedad, Peso: Peso, Volumen: Volumen, Id_Contenedor: Id_Contenedor, Codigo_Comodity: Codigo_Comodity});

    } else {
        alert('*Debe agregar los datos correspendientes.')
    }

    return contenedor;
}


function generar_tabla(estado) {
    if (estado == 1) {
        contenedor = añadirContenedor();
    }

    if (contenedor.length >= 0) {
        contador = 0;
        let myTable = "<hr/>";
        myTable += "<div class='row'>";
        myTable += "<div class='col-md-12'>";
        myTable += "<div class='card'>";
        myTable += "<div class='card-body'>";
        myTable += "<table class='table'>";
        myTable += "<tbody>";

        myTable += "<tr>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Cantidad</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Contenedor</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Comodity</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Carga</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Temperatura</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Ventilacion</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Humedad</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Peso</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Volumen</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Quitar</b>  </div></td>";
        myTable += "</tr>";


        for (let i = 0; i < contenedor.length; i++) {
            myTable += "<tr>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Cantidad + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].TipoContenedor + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Comodity + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Descrip_Carga + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Temperatura + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Ventilacion + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Humedad + "  </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Peso + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Volumen + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <a href=\"#\" onclick='QuitarLista(" + contador + "); return false'> <i class=\"fa fa-times \" style=\"color: red\"></i>  </a> </td>";

            myTable += "</tr>";
            contador++;
            //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].prod + "</td>";
            //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].cant + "</td>";
            //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].pr + "</td>";
            //myTable += "</tr>";
        }
        myTable += "</tbody>";
        myTable += "</table>";
        myTable += "</div>";
        myTable += "</div>";
        myTable += "</div>";
        myTable += "</div>";
        $("#gridLista").html(myTable);
    }
   
    //document.getElementById('tablePrint').innerHTML = myTable;
}


function QuitarLista(id) {
    contenedor.splice(id, 1);

    generar_tabla(2);

}


// #region Validadores

function ValidarPuertoEmbarqueYSalida(puerto1,puerto2) {
    if (puerto1 == puerto2) {
        alert("Seleccione diferentes puertos");
        return '0';

    } else {
        return '1';
    }
}


// #endregion