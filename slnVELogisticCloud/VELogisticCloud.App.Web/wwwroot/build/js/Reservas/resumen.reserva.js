﻿
var id = getParameterByName('id');
var J_user = getParameterByName('J_user');

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function () {
    Inicializar();
});


function Inicializar() {
    $.ajax({
        url: '../Itinerario_Solicitar/CargarResumen',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            //document.getElementById('lbl_nroBooking').innerHTML = dato.nro_booking;
            document.getElementById('lbl_Cantidad').innerHTML = dato.cantidadBulto;
            document.getElementById('lbl_etd').innerHTML = dato.etd;
            document.getElementById('lbl_eta').innerHTML = dato.eta; 
            document.getElementById('lbl_tipoContenedor').innerHTML = dato.tipoContenedor; 
            document.getElementById('lbl_CantSolicitados').innerHTML = dato.cantidadBulto; 

            document.getElementById('lbl_TipoCont').innerHTML = dato.tipoContenedor; 
            document.getElementById('lbl_Comodity').innerHTML = dato.commodity; 
            document.getElementById('lbl_temperatura').innerHTML = dato.temperatura; 
            document.getElementById('lbl_ventilacion').innerHTML = dato.ventilacion; 
            document.getElementById('lbl_humedad').innerHTML = dato.humedad; 
            document.getElementById('lbl_peso').innerHTML = dato.pesoBruto; 
            document.getElementById('lbl_emision').innerHTML = dato.emisionBL; 
            //document.getElementById('lbl_flete').innerHTML = dato.flete; 

            document.getElementById('lbl_pais_inicio').innerHTML = dato.paisEmbarque; 
            document.getElementById('lbl_tico_contenedor2').innerHTML = dato.tipoContenedor; 
            document.getElementById('lbl_pais_fin').innerHTML = dato.paisDescarga; 
        }
    });
}


function VerReservas() {
    if (J_user === 'customer') {
        url2 = "../Reservas/Index";
        window.open(url2, '_self');
    } else {
        url2 = "../ListaReservas/Index";
        window.open(url2, '_self');
    }

}