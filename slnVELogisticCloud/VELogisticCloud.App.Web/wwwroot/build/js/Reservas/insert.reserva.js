﻿
$(document).ready(function () {
    document.getElementById("div_dry").style.display = "none";
    document.getElementById("div_reefer").style.display = "none";


    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });


    hideSpinner();
    //hideLoading();

    contenedor = new Array();
    $("#txtCantidad").val(1);
    //$("#txtTipoContenedor").val("");
    //$("#txtComodity").val("");
    $("#txtDescrip_Carga").val("");
    //$("#txtNaviera").val("");
    $("#txtNroContrato").val("");
    $("#txtExpeditor").val("");
    $("#txtPuertoEmbarque").val("");
    $("#txtConsignatario").val("");
    //$("#txtPuertoDescarga").val("");
    $("#txtETD").val("");
    $("#txtETA").val("");
    $("#txtNombreNave").val("");
    $("#txtNaveViaje").val("");
    $("#txtTemperatura").val("");
    $("#txtVentilacion").val("");
    $("#txtHumedad").val("");
    $("#txtPeso").val("");
    $("#txtVolumen").val("");
    $("#txtFlete").val("");
    $("#txtEmision_BL").val("");

    //document.getElementById("txtNombreNave").value = "0"; 
    //document.getElementById("txtTipoContenedor").value = "0"; 
    //document.getElementById("txtNaviera").value = "0"; 
    //document.getElementById("txtPuertoEmbarque").value = "0"; 
    //document.getElementById("txtPuertoDescarga").value = "0"; 
    document.getElementById("txtExpeditor").value = "0"; 
    //document.getElementById("txtConsignatario").value = "0"; 
    //document.getElementById("txtComodity").value = "0"; 
    //Loading(6).init();

 

    
    //document.getElementById("div_name_pago").style.display = "block";
    //document.getElementById("div_direc_pago").style.display = "block";


    CambiarPago();
});



function CambiarContenedor() {
    const tipo = $("#txtTipoContenedor option:selected").text();
    if (tipo.trim().toLocaleLowerCase().includes("reefer")) {
        document.getElementById("div_reefer").style.display = "block";
        document.getElementById("div_dry").style.display = "none";

    } else if (tipo.trim().toLocaleLowerCase().includes("dry")) {
        document.getElementById("div_dry").style.display = "block";
        document.getElementById("div_reefer").style.display = "none";

    } else{
        document.getElementById("div_dry").style.display = "none";
        document.getElementById("div_reefer").style.display = "none";

    }
}

function CambiarPago() {
    const pago = $("#cbo_pago option:selected").val();
    if (pago == "ELSEWHERE") {
        document.getElementById("div_name_pago").style.display = "block";
        document.getElementById("div_direc_pago").style.display = "block";
    } else if (pago == "COLLLECT") {
        document.getElementById("div_name_pago").style.display = "none";
        document.getElementById("div_direc_pago").style.display = "none";
    } else if (pago == "PREPAID") {
        document.getElementById("div_name_pago").style.display = "none";
        document.getElementById("div_direc_pago").style.display = "none";
    }
}

function formatoFecha(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
}

function InsertReserva() {

    var etd_valida = $("#txtETD").val()
    var eta_valida = $("#txtETA").val()

    if (etd_valida.trim() != "" && eta_valida.trim() != "") {


        var CodigoNave = $("#txtNombreNave").val();
        var callsign = "prueba";
        //var callsign = $("#txtNombreNave").val($("select option:selected").attr('callsign'));
        var CodigosEmbarque = $("#txtPuertoEmbarque").val();
        var resCodigosEmbarque = CodigosEmbarque.split("-");
        var Embarquecodpais = $("#txtPuertoEmbarque option:selected").attr('codpais');
        var Embarquepais = $("#txtPuertoEmbarque option:selected").attr('pais');

        var CodigosDescarga = $("#txtPuertoDescarga").val();
        var resCodigosDescarga = CodigosDescarga.split("-");
        var Descargacodpais = $("#txtPuertoDescarga option:selected").attr('codpais');
        var Descargapais = $("#txtPuertoDescarga option:selected").attr('pais');


        const a = $("#txtTipoContenedor option:selected").text();
        let Condicion = "";
        let ColdTreatment = $("#cbo_Cold option:selected").val();
        let TipoTemperatura = $("#cbo_grados option:selected").val();
        let TipoVentilacion = $("#cbo_tipo_ventilacion option:selected").val();
        let TipoHumedad = $("#cbo_tipo_humedad option:selected").val();
        let Atmosfera = $("#cbo_atmosfera option:selected").val();
        let CO2 = $("#txt_co2").val();
        let O2 = $("#txt_o2").val();
        let ID_IMO = '';
        let IMO = '';
        let UN1 = '';
        let UN2 = '';
        let NombrePagador = '';
        let DireccionPagador = '';
        let tipoContenedor = '';

        const b = $("#cbo_pago option:selected").val();
        if (b == "ELSEWHERE") {
            NombrePagador = $("#txt_nombre_pagador").val();
            DireccionPagador = $("#txt_direccion_pagador").val();
        }

        if (a.trim().toLocaleLowerCase().includes("dry")) {
            tipoContenedor = "DRY";
            Condicion = "DRY";
            ColdTreatment = "";
            TipoTemperatura = "";
            TipoHumedad = "";
            Atmosfera = "";
            CO2 = "";
            O2 = "";
            ID_IMO = $("#cbo_imo option:selected").val();
            IMO = $("#cbo_imo option:selected").text();
            UN1 = $("#txt_un1").val();
            UN2 = $("#txt_un2").val();

        } else if (a.trim().toLocaleLowerCase().includes("reefer")) {
            tipoContenedor = "Reefer";
            Condicion = $("#cbo_Condicion option:selected").val();

        } else {

            Condicion = "";
            ColdTreatment = "";
            TipoTemperatura = "";
            TipoHumedad = "";
            Atmosfera = "";
            CO2 = "";
            O2 = "";
            ID_IMO = "";
            IMO = "";
            UN1 = "";
            UN2 = "";
        }

        var ent = {
            Naviera: $("#txtNaviera").val(),
            Nro_contrato: $("#txtNroContrato").val(),
            Expeditor: $("#txtExpeditor option:selected").text(),
            Consignatario: $("#txtConsignatario").val(),
            ETD: formatoFecha($("#txtETD").val()),
            ETA: formatoFecha($("#txtETA").val()),
            Nombre_Nave: $("#txtNombreNave").val(),
            Nave_Viaje: $("#txtNaveViaje").val(),
            Cantidad: $("#txtCantidad").val(),
            Tipo_Contenedor: $("#txtTipoContenedor option:selected").text(),
            Commodity: $("#txtComodity option:selected").text(),
            Descripcion_Carga: $("#txtDescrip_Carga").val(),
            Temperatura: $("#txt_temperatura").val(),
            Ventilacion: $("#txt_ventilacion").val(),
            Humedad: $("#txt_humedad").val(),
            Peso: $("#txtPeso").val(),
            Volumen: ' ',
            Flete: $("#cbo_pago option:selected").text(),
            Emision_BL: $("#cbo_emision option:selected").val(),


            Id_Naviera: $("#txtNaviera").val(),
            Id_Contenedor: $("#txtTipoContenedor").val(),
            Codigo_Nave: CodigoNave,
            CallSign_Nave: callsign,

            Puerto_Embarque: $("#txtPuertoEmbarque").val(),
            Codigo_Puerto_Embarque: $("#hddEmbarqueCodigoPuerto").val(),
            Codigo_Pais_Embarque: $("#hddEmbarqueCodigoPais").val(),
            Pais_Puerto_Embarque: $("#hddEmbarqueNombrePais").val(),

            Puerto_Descarga: $("#txtPuertoDescarga").val(),
            Codigo_Puerto_Descarga: $("#hddDescargaCodigoPuerto").val(),
            Codigo_Pais_Descarga: $("#hddDescargaCodigoPais").val(),
            Pais_Puerto_Descarga: $("#hddDescargaNombrePais").val(),

            Codigo_Consignatario: $("#txtConsignatario").val(),
            Direccion_Consignatario: "calle Consignatario",
            Codigo_Commodity: $("#txtComodity").val(),
            Codigo_Expeditor: $("#txtExpeditor").val(),
            Direccion_Expeditor: "calle Expeditor",

            Tipo: tipoContenedor,
            Condicion: Condicion,
            ColdTreatment: ColdTreatment,
            TipoTemperatura: TipoTemperatura,
            TipoVentilacion: TipoVentilacion,
            TipoHumedad: TipoHumedad,
            Atmosfera: Atmosfera,
            CO2: CO2,
            O2: O2,
            ID_IMO: ID_IMO,
            IMO: IMO,
            UN1: UN1,
            UN2: UN2,
            NombrePagador: NombrePagador,
            DireccionPagador: DireccionPagador


        }

        var valor1 = $("#hddEmbarqueCodigoPuerto").val();
        var valor2 = $("#hddDescargaCodigoPuerto").val();
        var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);

        if (val1 == '1') {
            showSpinner2();

            $.ajax({
                url: '../Itinerario_Solicitar/InsertarDatos',
                type: 'POST',
                async: false,
                data: { ent: ent, contenedor: contenedor },
                beforeSend: function () { },
                success: function (response) {
                    var dato = response;
                    hideSpinner();
                    url2 = "../Itinerario_Solicitar/Resumen?id=" + dato;
                    window.open(url2, '_self');
                    
                }
            });
        }
    } 
    else {
            Swal.fire({
                icon: 'info',
                title: 'Oops...',
                text: 'No puede generar una reserva sin un ETD o ETA!',
            })
        }

   
}

function añadirContenedor() {
    //elementos = parseInt(prompt("¿ Cuantos productos quieres añadir ?"));

    const a = $("#txtTipoContenedor option:selected").text();
    const Condicion = $("#cbo_Condicion option:selected").val();
    const ColdTreatment = $("#cbo_Cold option:selected").val();
    const TipoTemperatura = $("#cbo_grados option:selected").val();
    const TipoVentilacion = $("#cbo_tipo_ventilacion option:selected").val();
    const TipoHumedad = $("#cbo_tipo_humedad option:selected").val();
    const Atmosfera = $("#cbo_atmosfera option:selected").val();
    const CO2 = $("#txt_co2").val();
    const O2 = $("#txt_o2").val();
    const ID_IMO = ''; 
    const IMO = ''; 
    const UN1 = ''; 
    const UN2 = ''; 


    if (a.trim().toLocaleLowerCase().includes("dry")) {
        tipoContenedor = "DRY";
        Condicion = "DRY";
        ColdTreatment = "";
        TipoTemperatura = "";
        TipoHumedad = "";
        Atmosfera = "";
        CO2 = "";
        O2 = "";
        ID_IMO = $("#cbo_imo option:selected").val();
        IMO = $("#cbo_imo option:selected").text();
        UN1 = $("#txt_un1").val();
        UN2 = $("#txt_un2").val();

    } else if (a.trim().toLocaleLowerCase().includes("reefer")) {
        tipoContenedor = "Reefer";
        Condicion = $("#cbo_Condicion option:selected").val();

    } else {

        Condicion = "";
        ColdTreatment = "";
        TipoTemperatura = "";
        TipoHumedad = "";
        Atmosfera = "";
        CO2 = "";
        O2 = "";
        ID_IMO = "";
        IMO = "";
        UN1 = "";
        UN2 = "";
    }


    var Cantidad = $("#txtCantidad").val();
    var TipoContenedor = $("#txtTipoContenedor option:selected").text();
     
    var Id_Contenedor = document.getElementById("txtTipoContenedor").value;


    var Comodity = $("#txtComodity option:selected").text();
    var Codigo_Comodity = document.getElementById("txtComodity").value;
     

    var Descrip_Carga = $("#txtDescrip_Carga").val();

    var Temperatura = $("#txt_temperatura").val() + ' ' + $("#cbo_grados option:selected").val(); 
    var Ventilacion = $("#txt_ventilacion").val() + ' ' + $("#cbo_tipo_ventilacion option:selected").val(); 
    var Humedad = $("#txt_humedad").val() + ' ' + $("#cbo_tipo_humedad option:selected").val(); 
    var Peso = '0';
    var Volumen = ' ' ;

    if (Cantidad != "" && Temperatura != "" && TipoContenedor != "" && Ventilacion != "" && Comodity != "" && Humedad != "" && Descrip_Carga != "" ) {
        contenedor.push({
            Cantidad: Cantidad, TipoContenedor: TipoContenedor, Comodity: Comodity, Descrip_Carga: Descrip_Carga, Temperatura: Temperatura,
            Ventilacion: Ventilacion, Humedad: Humedad, Peso: Peso, Volumen: Volumen, Id_Contenedor: Id_Contenedor, Codigo_Comodity: Codigo_Comodity,
            Condicion: Condicion, ColdTreatment: ColdTreatment, TipoTemperatura: TipoTemperatura, TipoVentilacion: TipoVentilacion, TipoHumedad: TipoHumedad,
            Atmosfera: Atmosfera, CO2: CO2, O2: O2, ID_IMO: ID_IMO, IMO: IMO, UN1: UN1, UN2: UN2
        });

    } else {
        alert('*Debe agregar los datos correspendientes.')
    }

    return contenedor;
}


function generar_tabla(estado) {
    if (estado == 1) {
        contenedor = añadirContenedor();
    }

    if (contenedor.length >= 0) {
        contador = 0;
        let myTable = "<hr/>";
        myTable += "<div class='row'>";
        myTable += "<div class='col-md-12'>";
        myTable += "<div class='card'>";
        myTable += "<div class='card-body'>";
        myTable += "<table class='table'>";
        myTable += "<tbody>";

        myTable += "<tr>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Cantidad</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Contenedor</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Comodity</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Carga</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Temperatura</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Ventilacion</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Humedad</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Peso</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Volumen</b>  </div></td>";
        myTable += "<td style=\"border-top:none; text-align: center;\" class=\"border - right\" ><div class=\"row\"> <b>Quitar</b>  </div></td>";
        myTable += "</tr>";


        for (let i = 0; i < contenedor.length; i++) {
            myTable += "<tr>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Cantidad + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].TipoContenedor + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Comodity + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Descrip_Carga + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Temperatura + "</td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Ventilacion + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Humedad + "  </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Peso + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> " + contenedor[i].Volumen + " </td>";
            myTable += "<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <a href=\"#\" onclick='QuitarLista(" + contador + "); return false'> <i class=\"fa fa-times \" style=\"color: red\"></i>  </a> </td>";

            myTable += "</tr>";
            contador++;
            //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].prod + "</td>";
            //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].cant + "</td>";
            //myTable += "<td style='width: 100px;text-align: right;'>" + productos[i].pr + "</td>";
            //myTable += "</tr>";
        }
        myTable += "</tbody>";
        myTable += "</table>";
        myTable += "</div>";
        myTable += "</div>";
        myTable += "</div>";
        myTable += "</div>";
        $("#gridLista").html(myTable);
    }
   
    //document.getElementById('tablePrint').innerHTML = myTable;
}


function QuitarLista(id) {
    contenedor.splice(id, 1);

    generar_tabla(2);

}


// #region Validadores

function ValidarPuertoEmbarqueYSalida(puerto1,puerto2) {
    if (puerto1 == puerto2) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Seleccione diferentes puertos!',
        });
        return '0';

    } else {
        return '1';
    }
}


// #endregion