﻿
$(document).ready(function () {

    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });

    ListaActual();
});

function ListaActual() {
    showSpinner2();

    let textoEmbarque = $("#txtPuertoEmbarque").val();
    let textoDescarga = $("#txtPuertoDescarga").val();

    
    if (textoEmbarque.trim() == "") {
        $("#hddEmbarqueCodigoPuerto").val("");
    }

    if (textoDescarga.trim() == "") {
        $("#hddDescargaCodigoPuerto").val("");
    }

    var ent = {
        Nro_Booking: $("#txt_nroBooking").val(), 
        Puerto_Descarga: $("#hddDescargaCodigoPuerto").val() ,
        Puerto_Embarque: $("#hddEmbarqueCodigoPuerto").val(),
        Id_Estado_Reserva: $("#id_cbEstado option:selected").val()
    }
  
    $.ajax({
        url: '../ListaReservas/ListaReservas/',
        type: 'POST',
        async: false,
        data: { ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridLista").html(dato);
            /* validarRedirect(dato);*/ /*add sysseg*/
            ;
        }
    });
    hideSpinner();
}


function DeleteReserva(id) {
    if (confirm("¿Estas seguro de eliminar esta reserva?")) {
        $.ajax({
            url: '../ListaReservas/Delete_Reserva',
            type: 'POST',
            async: false,
            data: { id: id },
            beforeSend: function () { },
            success: function (response) {
                var dato = response;
                ListaActual();
                /* validarRedirect(dato);*/ /*add sysseg*/
                ;
            }
        });
    } else {

    }

}


function EditReserva(id) {
    $.ajax({
        url: '../ListaReservas/GuardarCodigo_EditarReserva',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            url2 = "../ListaReservas/EditarReserva?id=" + id + '&J_user=cliente';
            window.open(url2, '_self');
        }
    });

} 


function VerMapa(id) {

    if (id != "0") {
        url2 = "../Alertas/Mapa?imo=" + id;

        //Swal.fire({
        //    url: 'https://localhost:44350/Alertas/Mapa?imo=' + id,
        //    imageHeight: 1500,
        //    imageAlt: 'A tall image'
        //})

        window.open(url2, '_blank');
    } else {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'No se encontro el codigo la Nave!',
        });
    }

} 



function PlantillaReserva(id) {
        $.ajax({
            url: '../ListaReservas/GuardarCodigo_EditarReserva',
            type: 'POST',
            async: false,
            data: { id: id },
            beforeSend: function () { },
            success: function (response) {
                var dato = response;
                url2 = "../ListaReservas/CreateReserva_Plantilla";
                window.open(url2, '_self');
            }
        });
  

}
