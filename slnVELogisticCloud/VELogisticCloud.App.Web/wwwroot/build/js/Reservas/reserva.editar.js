﻿

var id = getParameterByName('id');
var J_user = getParameterByName('J_user');


var IdCotizacionDetalleOdoo = 0;

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function () {



    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });

    CargarDatos();
    CambiarContenedor();
});


function CambiarContenedor() {
    const tipo = $("#txtTipoContenedor option:selected").text();
    if (tipo.trim().toLocaleLowerCase().includes("reefer")) {
        document.getElementById("div_reefer").style.display = "block";
        document.getElementById("div_dry").style.display = "none";

    } else if (tipo.trim().toLocaleLowerCase().includes("dry")) {
        document.getElementById("div_dry").style.display = "block";
        document.getElementById("div_reefer").style.display = "none";

    } else {
        document.getElementById("div_dry").style.display = "none";
        document.getElementById("div_reefer").style.display = "none";

    }
}

function CambiarPago() {
    const pago = $("#cbo_pago option:selected").val();
    if (pago == "ELSEWHERE") {
        document.getElementById("div_name_pago").style.display = "block";
        document.getElementById("div_direc_pago").style.display = "block";
    } else if (pago == "COLLLECT") {
        document.getElementById("div_name_pago").style.display = "none";
        document.getElementById("div_direc_pago").style.display = "none";
    } else if (pago == "PREPAID") {
        document.getElementById("div_name_pago").style.display = "none";
        document.getElementById("div_direc_pago").style.display = "none";
    }
}

function CargarDatos() {
    var count = 0;
    showSpinner2();
    $.ajax({
        url: '../ListaReservas/RecuperarDatosEditar',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            //$("#txtid").val(dato.id_Reserva);
            $("#txtNroContrato").val(dato.nro_contrato);
            //$("#txtETD").val("2021-07-02");
            $("#txtETD").val(dato.etd);
            $("#txtETA").val(dato.eta);
            $("#txtNaveViaje").val(dato.nave_Viaje);
            $("#txtCantidad").val(dato.cantidad);
            $("#txtDescrip_Carga").val(dato.descripcion_Carga);
            $("#txt_temperatura").val(dato.temperatura);
            $("#txt_ventilacion").val(dato.ventilacion);
            $("#txt_humedad").val(dato.humedad);
            $("#txt_co2").val(dato.cO2);
            $("#txt_o2").val(dato.o2);
            $("#txtPeso").val(dato.peso);
            //$("#txt_Volumen").val(dato.volumen);
            $("#txt_nombre_pagador").val(dato.nombrePagador);
            $("#txt_direccion_pagador").val(dato.direccionPagador);

            
            document.getElementById("cbo_pago").value = dato.flete;
            document.getElementById("cbo_emision").value = dato.emision_BL;
            document.getElementById("txtNaviera").value = dato.id_Naviera;
            //$("#txtExpeditor option:selected").text(dato.expeditor);
            //document.getElementById("txtExpeditor").value = dato.expeditor;
            $("#txtExpeditor").val(dato.expeditor);

            document.getElementById("txtConsignatario").value = dato.codigo_Consignatario;

            document.getElementById("txtPuertoEmbarque").value = dato.puerto_Embarque
            $("#hddEmbarqueCodigoPuerto").val(dato.codigo_Puerto_Embarque);
            $("#hddEmbarqueCodigoPais").val(dato.codigo_Pais_Embarque);
            $("#hddEmbarqueNombrePais").val(dato.pais_Puerto_Embarque);
            $("#hddEmbarqueId").val(dato.id_Puerto_Carga);

            document.getElementById("txtPuertoDescarga").value = dato.puerto_Descarga;
            $("#hddDescargaCodigoPuerto").val(dato.codigo_Puerto_Descarga);
            $("#hddDescargaCodigoPais").val(dato.codigo_Pais_Descarga);
            $("#hddDescargaNombrePais").val(dato.pais_Puerto_Descarga);
            $("#hddDescargaId").val(dato.id_Puerto_Descarga);

            document.getElementById("txtNombreNave").value = dato.codigo_Nave;
            document.getElementById("txtTipoContenedor").value = dato.id_Contenedor;
            document.getElementById("txtComodity").value = dato.codigo_Commodity;

            if (dato.tipo.toLocaleLowerCase() == "reefer") {
                document.getElementById("cbo_Condicion").value = dato.condicion;
                document.getElementById("cbo_Cold").value = dato.coldTreatment;
                document.getElementById("cbo_grados").value = dato.tipoTemperatura;
                document.getElementById("cbo_tipo_ventilacion").value = dato.tipoVentilacion;
                document.getElementById("cbo_tipo_humedad").value = dato.tipoHumedad;
                document.getElementById("cbo_atmosfera").value = dato.atmosfera;

            } else {
                document.getElementById("cbo_imo").value = dato.iD_IMO;
                $("#txt_un1").val(dato.uN1);
                $("#txt_un2").val(dato.uN2);
            }
           
            
        }
    });
    CambiarContenedor();
    CambiarPago();
    hideSpinner();

}

function formatoFecha(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
}

function UpdateReserva() {

    var etd_valida = $("#txtETD").val()
    var eta_valida = $("#txtETA").val()
    if (etd_valida.trim() != "" && eta_valida.trim() != "") {


        var CodigoNave = $("#txtNombreNave").val();
        //var resCodigosNave = CodigosNave.split("-");
        var callsign = "prueba";
        //var callsign = $("#txtNombreNave").val($("select option:selected").attr('callsign'));
        var CodigosEmbarque = $("#txtPuertoEmbarque").val();

        var CodigosDescarga = $("#txtPuertoDescarga").val();
        const a = $("#txtTipoContenedor option:selected").text();
        let Condicion = "";
        let ColdTreatment = $("#cbo_Cold option:selected").val();
        let TipoTemperatura = $("#cbo_grados option:selected").val();
        let TipoVentilacion = $("#cbo_tipo_ventilacion option:selected").val();
        let TipoHumedad = $("#cbo_tipo_humedad option:selected").val();
        let Atmosfera = $("#cbo_atmosfera option:selected").val();
        let CO2 = $("#txt_co2").val();
        let O2 = $("#txt_o2").val();
        let ID_IMO = '';
        let IMO = '';
        let UN1 = '';
        let UN2 = '';
        let NombrePagador = '';
        let DireccionPagador = '';
        let tipoContenedor = '';

        const b = $("#cbo_pago option:selected").val();
        if (b == "ELSEWHERE") {
            NombrePagador = $("#txt_nombre_pagador").val();
            DireccionPagador = $("#txt_direccion_pagador").val();
        }

        if (a.trim().toLocaleLowerCase().includes("dry")) {
            tipoContenedor = "DRY";
            Condicion = "DRY";
            ColdTreatment = "";
            TipoTemperatura = "";
            TipoHumedad = "";
            Atmosfera = "";
            CO2 = "";
            O2 = "";
            ID_IMO = $("#cbo_imo option:selected").val();
            IMO = $("#cbo_imo option:selected").text();
            UN1 = $("#txt_un1").val();
            UN2 = $("#txt_un2").val();

        } else if (a.trim().toLocaleLowerCase().includes("reefer")) {
            tipoContenedor = "Reefer";
            Condicion = $("#cbo_Condicion option:selected").val();

        } else {

            Condicion = "";
            ColdTreatment = "";
            TipoTemperatura = "";
            TipoHumedad = "";
            Atmosfera = "";
            CO2 = "";
            O2 = "";
            ID_IMO = "";
            IMO = "";
            UN1 = "";
            UN2 = "";
        }

        var ent = {
            Naviera: $("#txtNaviera option:selected").text(),
            Nro_contrato: $("#txtNroContrato").val(),
            //Expeditor:  $("#txtExpeditor option:selected").text(),
            Expeditor: $("#txtExpeditor").val(),
            Consignatario: $("#txtConsignatario").val(),
            ETD: formatoFecha($("#txtETD").val()),
            ETA: formatoFecha($("#txtETA").val()),
            Nombre_Nave: $("#txtNombreNave option:selected").text(),
            Nave_Viaje: $("#txtNaveViaje").val(),
            Cantidad: $("#txtCantidad").val(),
            Tipo_Contenedor: $("#txtTipoContenedor option:selected").text(),
            Commodity: $("#txtComodity option:selected").text(),
            Descripcion_Carga: $("#txtDescrip_Carga").val(),
            Temperatura: $("#txt_temperatura").val(),
            Ventilacion: $("#txt_ventilacion").val(),
            Humedad: $("#txt_humedad").val(),
            Peso: $("#txtPeso").val(),
            Volumen: ' ',
            Flete: $("#cbo_pago option:selected").text(),
            Emision_BL: $("#cbo_emision option:selected").val(),


            Id_Naviera: $("#txtNaviera").val(),
            Id_Contenedor: $("#txtTipoContenedor").val(),
            Codigo_Nave: CodigoNave,
            CallSign_Nave: callsign,

            Puerto_Embarque: $("#txtPuertoEmbarque").val(),
            Codigo_Puerto_Embarque: $("#hddEmbarqueCodigoPuerto").val(),
            Codigo_Pais_Embarque: $("#hddEmbarqueCodigoPais").val(),
            Pais_Puerto_Embarque: $("#hddEmbarqueNombrePais").val(),

            Puerto_Descarga: $("#txtPuertoDescarga").val(),
            Codigo_Puerto_Descarga: $("#hddDescargaCodigoPuerto").val(),
            Codigo_Pais_Descarga: $("#hddDescargaCodigoPais").val(),
            Pais_Puerto_Descarga: $("#hddDescargaNombrePais").val(),

            Codigo_Consignatario: $("#txtConsignatario").val(),
            Direccion_Consignatario: "calle Consignatario",
            Codigo_Commodity: $("#txtComodity option:selected").val(),
            Codigo_Expeditor: $("#txtExpeditor").val(),
            Direccion_Expeditor: "calle Expeditor",

            Tipo: tipoContenedor,
            Condicion: Condicion,
            ColdTreatment: ColdTreatment,
            TipoTemperatura: TipoTemperatura,
            TipoVentilacion: TipoVentilacion,
            TipoHumedad: TipoHumedad,
            Atmosfera: Atmosfera,
            CO2: CO2,
            O2: O2,
            ID_IMO: ID_IMO,
            IMO: IMO,
            UN1: UN1,
            UN2: UN2,
            NombrePagador: NombrePagador,
            DireccionPagador: DireccionPagador,
            Id_Puerto_Carga: $("#hddEmbarqueId").val(),
            Id_Puerto_Descarga: $("#hddDescargaId").val()
        }

        var valor1 = $("#hddEmbarqueCodigoPuerto").val();
        var valor2 = $("#hddDescargaCodigoPuerto").val();
        var val1 = ValidarPuertoEmbarqueYSalida(valor1, valor2);

        if (val1 == '1') {
            $.ajax({
                url: '../ListaReservas/UpdateDatos',
                type: 'POST',
                async: false,
                data: { ent: ent, id: id },
                beforeSend: function () { },
                success: function (response) {
                    var dato = response;
                    Swal.fire({
                        title: 'Actualizacion Correcta.',
                        text: "Todos los campos de la reserva se actualizacion.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            hideSpinner();
                            url2 = "../Itinerario_Solicitar/Resumen?id=" + dato + '&J_user=' + J_user;
                            window.open(url2, '_self');
                        }
                    })

                    //alert("Se Actualizo Correctamente.");

                }
            });
        }
    } else {
        Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'No puede generar una reserva sin un ETD o ETA!',
        })
    }

}


function ValidarPuertoEmbarqueYSalida(puerto1, puerto2) {
    if (puerto1 == puerto2) {
        alert("Seleccione diferentes puertos");
        return '0';

    } else {
        return '1';
    }
}


function Cancelar() {
    if (J_user === 'customer') {
        url2 = "../Reservas/Index";
        window.open(url2, '_self');
    } else {
        url2 = "../ListaReservas/Index";
        window.open(url2, '_self');
    }

}


$("#txtNroContrato").blur(function () {

    if (J_user === 'customer') {
        showSpinner2();

        var contrato = $("#txtNroContrato").val();
        $.ajax({
            url: '../ListaReservas/ValidarContrato',
            type: 'POST',
            async: false,
            data: { contrato: contrato, idReserva: id },
            beforeSend: function () { },
            success: function (response) {
                var dato = response;
                hideSpinner();

                if (dato === 0) {
                    document.getElementById('span_correcto').innerHTML = "Nro Contrato Erróneo.";
                    document.getElementById('span_correcto').style = "color:red";
                    $('#btn_update').prop('disabled', true);
                } else {
                    IdCotizacionDetalleOdoo = dato;
                    document.getElementById('span_correcto').innerHTML = "Nro Contrato Correcto.";
                    document.getElementById('span_correcto').style = "color:green";
                    $('#btn_update').prop('disabled', false);

                }
            },

        });
    } else {
    }
    
    // tu codigo ajax va dentro de esta function...
});
