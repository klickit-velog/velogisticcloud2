﻿


$(document).ready(function () {
    ListaCliente();
});

function Paginacion() {

    $('#data').after('<div id="nav" class="pagination"></div>');
    var rowsShown = 15;
    var rowsTotal = $('#data tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#nav').append('<a href="#" rel="' + i + '">' + pageNum + '</a> ');
    }
    $('#data tbody tr').hide();
    $('#data tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function () {
    
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#data tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function handleClick(cb, idCliente, Cliente, Correo, idNoti, Notificacion) {

    if ($('#' + cb).prop('checked')) {
        InsertNotificacionUsuario(idCliente, Cliente, Correo, idNoti, Notificacion,1);
    } else {
        InsertNotificacionUsuario(idCliente, Cliente, Correo, idNoti, Notificacion,0);
    }
}

function InsertNotificacionUsuario(idCliente, Cliente, Correo, idNoti, Notificacion,activo) {

    var ent = {
        IdUsuario: idCliente,
        CorreoUsuario: Correo,
        NombreUsuario: Cliente,
        IdTipoNotificacion: idNoti,
        NombreNotificacion: Notificacion,
        Activo: activo
    }
    $.ajax({
        url: '../Notificacion/InsertNotificacionUsuario',
        type: 'POST',
        async: false,
        data: { ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            alert(id);
        }
    });
}

function timeout() {
    setTimeout(function () {
      
    }, 50000, "JavaScript");
}

function ListaCliente(){
    showSpinner2();

    var RazonSocial = $("#txt_razon_social").val();
    var RUC = $("#txt_ruc").val();
    var Correo = $("#txt_correo").val();


    //timeout();
    $.ajax({
        url: '../Notificacion/ListaCliente',
        type: 'POST',
        async: false,
        data: { RazonSocial: RazonSocial, RUC: RUC, Correo: Correo },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
            Paginacion();

            //$('#myTableBody').pageMe({ pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 4 });

            //hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
            ;
        }
    });
    hideSpinner(); 


}



