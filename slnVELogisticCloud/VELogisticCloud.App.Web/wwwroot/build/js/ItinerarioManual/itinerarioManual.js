﻿$(document).ready(function () {

    hideSpinner();

    $("#upload").click(function (evt) {
        var fileUpload = $("#files").get(0);
        var files = fileUpload.files;
        var data = new FormData();

        for (var x = 0; x < files.length; x++) {
            var file = files[x];
            data.append(file.name, file);
        }

        if (files.length > 1) {
            Swal.fire({
                icon: 'info',
                title: 'Oops...!',
                text: 'Solo debe cargar 1 archivo!',
            });
            return;
        }
        showSpinner2();
        $.ajax({
            type: "POST",
            async: false,
            url: "/ItinerarioManual/LoadFile2",
            contentType: false,
            processData: false,
            data: data,
            success: function (dato) {
                hideSpinner();
                if (dato.result === 0) {
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: 'Verifique el archivo !',
                    });
                } else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Ok!',
                        text: 'Se agregaron ' + dato.result +'  registros del excel correctamente!',
                    });
                }
            },
            //error: function (jqXHR, textStatus, errorThrown) {
            //    hideSpinner();
            //    Swal.fire({
            //        icon: 'info',
            //        title: 'Oops...',
            //        text: 'Error!',
            //    });
            //}
        });
    });

    
    document.getElementById("link_ExcelItinerario").style.color = "black";
    document.getElementById("link_ExcelItinerario").style.fontWeight = "bold";
});


