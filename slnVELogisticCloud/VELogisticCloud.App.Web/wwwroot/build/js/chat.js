﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/signalServer").build();
const UsuarioMenu = localStorage.getItem('UsuarioMenu');
const correoUsuario = localStorage.getItem('correoUsuario');
const sumLocal = localStorage.getItem('sum');

document.getElementById('span_UsuarioMenu').innerHTML = UsuarioMenu;
document.getElementById('span_correoUsuario').innerHTML = correoUsuario;
document.getElementById('txt_alertas').innerHTML = sumLocal;
//Disable the send button until connection is established.
//document.getElementById("sendButton").disabled = true;

connection.on("ReceiveMessage", function (user, message) {
    //var li = document.createElement("li");
    //document.getElementById("messagesList").appendChild(li);
    // We can assign user-supplied strings to an element's textContent because it
    // is not interpreted as markup. If you're assigning in any other way, you 
    // should be aware of possible script injection concerns.
    //li.textContent = `${user} says ${message}`;
    const sum = localStorage.getItem('sum');
    document.getElementById('txt_alertas').innerHTML = sum;
    //AumentarContador(user);
});

function AumentarContador(user) {
    const usuarioLogueado = document.getElementById('span_correoUsuario').innerText;
    if (usuarioLogueado === user) {
        const sum = localStorage.getItem('sum');
        let sum2 = parseInt(sum, 10);
        sum2 = sum2 + 1;
        document.getElementById('txt_alertas').innerHTML = sum2;
        localStorage.setItem('sum', sum2);
    }
}

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    AumentarContador(user);
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});