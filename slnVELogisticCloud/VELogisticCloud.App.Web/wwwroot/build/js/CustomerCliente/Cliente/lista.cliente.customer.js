﻿

//document.getElementById("pag_cabecera").style.display = "none"; 
$(document).ready(function () {
    showSpinner();
    (async () => {
        console.log('before start');

        await ReiniciarClaims();
        await ConteoAlertas();
        await hideSpinner();
        console.log('after start');
    })();
    //const myTimeout = setTimeout(, 5000);
});


function PaginaAccesoCliente() {
    url2 = "../CustomerCliente/SeleccionCliente";
    window.open(url2, '_self');
}

async function ConteoAlertas() {
    const result = await
    $.ajax({
        url: '../Alertas/CantidadAlertas',
        type: 'POST',
        async: false,
        //data: { id: id },
        success: function (response) {
            var dato = response;
            if (dato.cantidad === -1) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ocurrió un problema de conexión con Odoo, por favor comuníquese con el administrador.!',
                })
            } else {
                if (dato != undefined) {
                    document.getElementById('txt_alertas').innerHTML = dato.cantidad;
                    document.getElementById('span_UsuarioMenu').innerHTML = dato.nombre;
                    document.getElementById('span_correoUsuario').innerHTML = dato.user;
                    localStorage.setItem('UsuarioMenu', dato.nombre);
                    localStorage.setItem('correoUsuario', dato.user);
                }
            }
            


        }
    });
 return result;
}

async function ReiniciarClaims() {
    const result = await 
    $.ajax({
        url: '../CustomerCliente/ReiniciarClaims',
        async:false,
        type: 'POST',
        //data: { id: id },
        success: function (response) {
        }
    });
    return result;
}


