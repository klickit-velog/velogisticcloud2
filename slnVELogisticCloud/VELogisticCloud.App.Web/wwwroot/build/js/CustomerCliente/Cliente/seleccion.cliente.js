﻿


$(document).ready(function () {
    (async () => {
        console.log('before start');
        await ListaCliente();
        await Paginacion();
        await ReiniciarClaims();
        await ConteoAlertas();
        await hideSpinner()
        console.log('after start');
    })();

    document.getElementById("link_SeleccionCliente").style.color = "black";
    document.getElementById("link_SeleccionCliente").style.fontWeight = "bold";
    //
});

async function Paginacion() {

    $('#data').after('<div id="nav" class="pagination"></div>');
    var rowsShown = 10;
    var rowsTotal = $('#data tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#nav').append('<a href="#" rel="' + i + '">' + pageNum + '</a> ');
    }
    $('#data tbody tr').hide();
    $('#data tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function () {
    
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#data tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}
function ObtenerNuevoUsuario(correo, nombre) {
    if (correo === '') {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'El usuario debe tener un correo!',
        })
    } else {
        showSpinner2();
        $.ajax({
            url: '../CustomerCliente/ObtenerNuevoUsuario',
            type: 'POST',
            data: { correo: correo, nombre: nombre },
            beforeSend: function () { },
            success: function (response) {
                var dato = response;
                url2 = "../Cotizacion/MisSolicitudes";
                window.open(url2, '_self');
                localStorage.setItem('UsuarioMenu', nombre);
                localStorage.setItem('correoUsuario', correo);
                document.getElementById('span_UsuarioMenu').innerHTML = nombre;
                document.getElementById('span_correoUsuario').innerHTML = correo;
                hideSpinner();
            }
        });
    }

   
}

async function ListaCliente(){

    var RazonSocial = $("#txt_razon_social").val();
    var RUC = $("#txt_ruc").val();
    var Correo = $("#txt_correo").val();
    showSpinner2();

    const result =
        await $.ajax({
        url: '../CustomerCliente/ListaCliente',
        type: 'POST',
        //async: false,
        data: { RazonSocial: RazonSocial, RUC: RUC, Correo: Correo },
        success:  function (response) {
            var dato = response;
            if (dato.trim() === '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ocurrió un problema de conexión con Odoo, por favor comuníquese con el administrador.!',
                })
            } else {
                $("#gridhtml").html(dato);
            }
        }
        });
    hideSpinner();

    return result;
}


async function ConteoAlertas() {

    const result = await $.ajax({
        url: '../Alertas/CantidadAlertas',
        type: 'POST',
        //async: false,
        //data: { id: id },
        success: function (response) {
            var dato = response;
            if (dato != undefined) {
                document.getElementById('txt_alertas').innerHTML = dato.cantidad;
                document.getElementById('span_UsuarioMenu').innerHTML = dato.nombre;
                document.getElementById('span_correoUsuario').innerHTML = dato.user;
                localStorage.setItem('UsuarioMenu', dato.nombre);
                localStorage.setItem('correoUsuario', dato.user);
            }
        }
    });
    return result;
}

async function ReiniciarClaims() {
    const result = await $.ajax({
        url: '../CustomerCliente/ReiniciarClaims',
        type: 'POST',
        async: false,
        //data: { id: id },
        beforeSend: function () { },
        success: function (response) {
        }
    });
    return result;
}




