﻿var id = getParameterByName('id');
IniciarDatos();

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function IniciarDatos() {

    //var d = new Date();
    //var dia = d.getDate();
    //var mes = d.getMonth() + 1;
    //var anio = d.getFullYear();
    //var hora = d.getHours();
    //var min = d.getMinutes();
    //var seg = d.getSeconds();

    //var codigo = dia.toString() + mes.toString() + anio.toString() + hora.toString() + min.toString() + seg.toString();

    //$("#txtCodigo").val(codigo);

    $.ajax({
        url: '../CustomerCotizacion/DatosEditar',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#txtCantidad").val(dato.cantidad_Contenedor); 
            $("#txtCodigo").val(dato.ruc);
            $("#txtCliente").val(dato.cliente); 
            $("#txtFechaEnvio").val(dato.fecha_Envio); 
            //document.getElementById('txtFechaEnvio').value = dato.fecha_Envio;
            $("#txtFechaFinal").val(dato.fecha_Final); 
            $("#txtNota").val(dato.nota); 
            $("#txtObservacion").val(dato.observacion);
            document.getElementById("txtTipoContenedor").value = dato.codigo_Tipo_Contenedor;
            document.getElementById("txtCampana").value = dato.campania;
            TablaEditarDetalle(id);
        }
       
    });

}


function TablaEditarDetalle(id) {
    $.ajax({
        url: '../CustomerCotizacion/TablaEditarDetalle',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
        }
    });
}

function añadirDetalle() {
    //elementos = parseInt(prompt("¿ Cuantos productos quieres añadir ?"));
    showSpinner2();

    var CodigosEmbarque = $("#txtPuertoEmbarque").val();
    var resCodigosEmbarque = CodigosEmbarque.split("-");

    var CodigosDescarga = $("#txtPuertoDescarga").val();
    var resCodigosDescarga = CodigosDescarga.split("-");

    var Cod_line_navi = $("#txtNaviera option:selected").val();
    var Linea_naviera = $("#txtNaviera option:selected").text();

    var Cod_puertoemba = resCodigosEmbarque[0];
    var Carga_puerto = $("#txtPuertoEmbarque option:selected").text()

    var Cod_puertodes = resCodigosDescarga[0];
    var desc_puerto = $("#txtPuertoDescarga option:selected").text()

    var Commodity = $("#txtComodity option:selected").text();
    var Cod_comodity = document.getElementById("txtComodity").value;


    var Region = resCodigosDescarga[1];
    var TT = "0";
    var Tamano = "20";

    var det = {
        Id_Cotizacion_Maritimo: id,
        Codigo_Linea_Naviera: Cod_line_navi,
        Linea_Naviera: Linea_naviera,
        Codigo_Puerto_Embarque: Cod_puertoemba,
        Puerto_Embarque: Carga_puerto,
        Codigo_Puerto_Descarga: Cod_puertodes,
        Puerto_Descarga: desc_puerto,
        Commodity: Commodity,
        Codigo_Commodity: Cod_comodity,
        TT: TT,
        Tamanio: Tamano,
        //Valido_Desde: Val_desde,
        //Valido_Hasta: Val_hasta,
        Region: Region
    };

    $.ajax({
        url: '../CustomerCotizacion/InsertDetalleCotizacionMartitimo',
        type: 'POST',
        async: false,
        data: { det: det },
        beforeSend: function () { },
        success: function (response) {
            TablaEditarDetalle(id);
            hideSpinner2();

        }
    });

    //} else {
    //    alert('*Debe agregar los datos correspendientes.')
    //}
}

function EliminarDetalle(id_detalle) {
    if (confirm("¿Estas seguro de eliminar esta reserva?")) {
        showSpinner2();
        $.ajax({
            url: '../CustomerCotizacion/DeleteMaritimoDetalle',
            type: 'POST',
            async: false,
            data: { id: id_detalle },
            beforeSend: function () { },
            success: function (response) {
                TablaEditarDetalle(id);
                hideSpinner2();

            }
        });
    }

}

function UpdateMaritimo() {
    var ent = {
        Cantidad_Contenedor: $("#txtCantidad").val(),
        Codigo_Tipo_Contenedor: $("#txtTipoContenedor option:selected").val(),
        Tipo_Contenedor: $("#txtTipoContenedor option:selected").text(),
        Codigo_Cotizacion: $("#txtCodigo").val(),
        Cliente: $("#txtCliente").val(),
        Fecha_Envio: $("#txtFechaEnvio").val(),
        Fecha_Final: $("#txtFechaFinal").val(),
        Campania: $("#txtCampana option:selected").val(),
        Nota: $("#txtNota").val(),
        Observacion: $("#txtObservacion").val()
    }
    $.ajax({
        url: '../CustomerCotizacion/UpdateCabeceraMaritimo',
        type: 'POST',
        async: false,
        data: { id:id,ent: ent},
        beforeSend: function () { },
        success: function (response) {
            var dato = response;

            Swal.fire({
                title: 'Registro Correcto.',
                text: "Se registro correctamente.",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.isConfirmed) {
                    url2 = "../CustomerCotizacion/MisSolicitudes";
                    window.open(url2, '_self');
                }
            })
        }
    });

}



function Editar(cod) {
    //var url = '@Url.Action("Editar","OperacionLogistica", new {id="' + IdOperacionLogistica+'"})';
    url2 = "../CustomerCotizacion/EditarMaritimo?id=" + cod;
    window.open(url2, '_self');
}

function Cancelar() {
    url2 = "../CustomerCotizacion/MisSolicitudes";
    window.open(url2, '_self');
}




