﻿var id = getParameterByName('id');
IniciarDatos();

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}




function IniciarDatos() {

    $.ajax({
        url: '../CustomerCotizacion/DatosEditarLogistica',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById("cbo_embarque").value = dato.codigo_Puerto_Embarque;
            document.getElementById("cbo_Commodity").value = dato.codigo_Commodity;
            document.getElementById("cbo_AlmacenRetiro").value = dato.codigo_Almacen_Retiro;
            document.getElementById("cbo_Almacen_Packing").value = dato.codigo_Almacen_Packing;
            if (dato.servicio_Embarque ==1)
                $('#chk_embarque').prop('checked', true);
            if (dato.retiro_Contenedor_Vacio == 1)
                $('#chk_retiro').prop('checked', true);
            if (dato.retiro_Transporte == 1)
                $('#chk_transporte').prop('checked', true);
            if (dato.servicio_Agenciamiento_Aduana == 1)
                $('#chk_Servicio_Aduana').prop('checked', true);
            if (dato.certificado_Origen == 1)
                $('#chk_Origen').prop('checked', true);
            if (dato.conexion_Electrica == 1)
                $('#chk_conexion').prop('checked', true);
            if (dato.pago_CPB == 1)
                $('#chk_Pago_cpb').prop('checked', true);
            if (dato.agenciamiento_Maritimo == 1)
                $('#chk_agenciamiento_maritimo').prop('checked', true);
            if (dato.movilizaciones_Contenedor == 1)
                $('#chk_movilizaciones_contenedor').prop('checked', true);
            if (dato.generador == 1)
                $('#chk_generador').prop('checked', true);
            if (dato.aforo_Fisico == 1)
                $('#chk_aforo_fisico').prop('checked', true);
            if (dato.courier == 1)
                $('#chk_courier').prop('checked', true);
            if (dato.senasa == 1)
                $('#chk_senasa').prop('checked', true);
            if (dato.filtro == 1)
                $('#chk_filtro').prop('checked', true);
            if (dato.termografo == 1)
                $('#chk_termografo').prop('checked', true);
            $("#txt_area_termografo").val(dato.tipo_Termografo); 
            $("#txt_fecha").val(dato.fecha_Cita); 

            // $("#cbo_embarque option:selected").val();
            //var Puerto_Embarque = $("#cbo_embarque option:selected").text();
            //var Codigo_Commodity = $("#cbo_Commodity option:selected").val();
            //var Commodity = $("#cbo_Commodity option:selected").text();
            //var Codigo_Almacen_Retiro = $("#cbo_AlmacenRetiro option:selected").val();
            //var Almacen_Retiro = $("#cbo_AlmacenRetiro option:selected").text();
            //$("#txt_fecha").val();
            //var Codigo_Almacen_Packing = $("#cbo_Almacen_Packing option:selected").val();
            //var Almacen_Packing = $("#cbo_Almacen_Packing option:selected").text();

            //$("#txt_area_termografo option:selected").text();

        }

    });

}


function UpdateLogistica() {
    var Servicio_embar = 0;
    var Retiro_cont_vacio = 0;
    var Retiro_trans = 0;
    if ($('#chk_embarque').is(':checked')) {
        Servicio_embar = 1;
    } else {
        Servicio_embar = 0;
    }
    if ($('#chk_retiro').is(':checked')) {
        Retiro_cont_vacio = 1;
    } else {
        Retiro_cont_vacio = 0;
    }
    if ($('#chk_transporte').is(':checked')) {
        Retiro_trans = 1;
    } else {
        Retiro_trans = 0;
    }

    var Ser_Agen_Aduana = 0;
    var Cert_origen = 0;
    var Conex_elect = 0;
    var Pago_cpb = 0;

    if ($('#chk_Servicio_Aduana').is(':checked')) {
        Ser_Agen_Aduana = 1;
    } else {
        Ser_Agen_Aduana = 0;
    }
    if ($('#chk_Origen').is(':checked')) {
        Cert_origen = 1;
    } else {
        Cert_origen = 0;
    }
    if ($('#chk_conexion').is(':checked')) {
        Conex_elect = 1;
    } else {
        Conex_elect = 0;
    }
    if ($('#chk_Pago_cpb').is(':checked')) {
        Pago_cpb = 1;
    } else {
        Pago_cpb = 0;
    }



    var chk_agenciamiento_maritimo = 0;
    var chk_movilizaciones_contenedor = 0;
    var chk_generador = 0;
    var chk_aforo_fisico = 0;

    var chk_courier = 0;
    var chk_senasa = 0;
    var chk_filtro = 0;
    var chk_termografo = 0;

    if ($('#chk_agenciamiento_maritimo').is(':checked')) {
        chk_agenciamiento_maritimo = 1;
    } else {
        chk_agenciamiento_maritimo = 0;
    }
    if ($('#chk_movilizaciones_contenedor').is(':checked')) {
        chk_movilizaciones_contenedor = 1;
    } else {
        chk_movilizaciones_contenedor = 0;
    }
    if ($('#chk_generador').is(':checked')) {
        chk_generador = 1;
    } else {
        chk_generador = 0;
    }
    if ($('#chk_aforo_fisico').is(':checked')) {
        chk_aforo_fisico = 1;
    } else {
        chk_aforo_fisico = 0;
    }

    if ($('#chk_courier').is(':checked')) {
        chk_courier = 1;
    } else {
        chk_courier = 0;
    }
    if ($('#chk_senasa').is(':checked')) {
        chk_senasa = 1;
    } else {
        chk_senasa = 0;
    }
    if ($('#chk_filtro').is(':checked')) {
        chk_filtro = 1;
    } else {
        chk_filtro = 0;
    }
    if ($('#chk_termografo').is(':checked')) {
        chk_termografo = 1;
    } else {
        chk_termografo = 0;
    }

    var Codigo_Puerto_Embarque = $("#cbo_embarque option:selected").val();
    var Puerto_Embarque = $("#cbo_embarque option:selected").text();
    var Codigo_Commodity = $("#cbo_Commodity option:selected").val();
    var Commodity = $("#cbo_Commodity option:selected").text();
    var Codigo_Almacen_Retiro = $("#cbo_AlmacenRetiro option:selected").val();
    var Almacen_Retiro = $("#cbo_AlmacenRetiro option:selected").text();
    var Fecha_Cita = $("#txt_fecha").val();
    var Codigo_Almacen_Packing = $("#cbo_Almacen_Packing option:selected").val();
    var Almacen_Packing = $("#cbo_Almacen_Packing option:selected").text();

    var TipoTermografo = $("#txt_area_termografo").val();


    var ent = {
        Codigo_Puerto_Embarque: Codigo_Puerto_Embarque,
        Puerto_Embarque: Puerto_Embarque,
        Codigo_Commodity: Codigo_Commodity,
        Commodity: Commodity,
        Servicio_Embarque: Servicio_embar,
        Retiro_Contenedor_Vacio: Retiro_cont_vacio,
        Codigo_Almacen_Retiro: Codigo_Almacen_Retiro,
        Almacen_Retiro: Almacen_Retiro,
        Fecha_Cita: Fecha_Cita,
        Retiro_Transporte: Retiro_trans,
        Codigo_Almacen_Packing: Codigo_Almacen_Packing,
        Almacen_Packing: Almacen_Packing,
        Servicio_Agenciamiento_Aduana: Ser_Agen_Aduana,
        Certificado_Origen: Cert_origen,
        Conexion_Electrica: Conex_elect,
        Pago_CPB: Pago_cpb,

        Agenciamiento_Maritimo: chk_agenciamiento_maritimo,
        Movilizaciones_Contenedor: chk_movilizaciones_contenedor,
        Generador: chk_generador,
        Aforo_Fisico: chk_aforo_fisico,
        Courier: chk_courier,
        Senasa: chk_senasa,
        Filtro: chk_filtro,
        Termografo: chk_termografo,
        Tipo_Termografo: TipoTermografo
    }


    $.ajax({
        url: '../CustomerCotizacion/UpdateLogistica',
        type: 'POST',
        async: false,
        data: { id:id,ent: ent },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            url2 = "../CustomerCotizacion/MisSolicitudesLogistica";
            window.open(url2, '_self');
        }
    });

}

function Cancelar() {
    url2 = "../CustomerCotizacion/MisSolicitudesLogistica";
    window.open(url2, '_self');
}

