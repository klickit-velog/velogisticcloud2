﻿
var id = getParameterByName('id');
DatosParaEditar();

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search); 
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function DatosParaEditar() {
    showSpinner2();

    $.ajax({
        url: '../CustomerCotizacion/DetalleLogistica',
        type: 'POST',
        async: false,
        data: { id: id },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;


            document.getElementById('lbl_nroCotizacion').innerHTML = dato.id_Cotizacion_Logistica;
            document.getElementById('lbl_Cantidad').innerHTML = dato.nroContenedor;

            document.getElementById('lbl_embarque').innerHTML = dato.puerto_Embarque;
            document.getElementById('lbl_commodity').innerHTML = dato.commodity;
            document.getElementById('lbl_servicioEmbarque').innerHTML = dato.servicioEmbarque;
            document.getElementById('lbl_almacenRetiro').innerHTML = dato.almacen_Retiro;
            document.getElementById('lbl_fechaCita').innerHTML = dato.fecha_Cita;
            document.getElementById('lbl_almacenPacking').innerHTML = dato.almacen_Packing;


            document.getElementById('lbl_aduana').innerHTML = dato.servicioAduana;
            document.getElementById('lbl_conexElectrica').innerHTML = dato.conexionElectrica;
            document.getElementById('lbl_maritimo').innerHTML = dato.agenciamientoMaritimo;
            document.getElementById('lbl_generador').innerHTML = dato.generador;
            document.getElementById('lbl_courier').innerHTML = dato.courier;
            document.getElementById('lbl_filtro').innerHTML = dato.filtro;
            document.getElementById('lbl_certificado').innerHTML = dato.certificadoOrigen;
            document.getElementById('lbl_pago').innerHTML = dato.pagoCPB;
            document.getElementById('lbl_movili').innerHTML = dato.movilizacionesContenedor;
            document.getElementById('lbl_aforo').innerHTML = dato.aforoFisico;
            document.getElementById('lbl_senasa').innerHTML = dato.senasa;
            document.getElementById('lbl_termografo').innerHTML = dato.termografo;
            //document.getElementById("cbo_Commodity").value = dato.codigoCommodity;
            //document.getElementById("cbo_estado").value = dato.idEstadoOddo;
            //$("#txt_conpectosOp").val(dato.conceptosOPL);
            //$("#txt_canal").val(dato.canal);
            hideSpinner2();
        }
    });
}

