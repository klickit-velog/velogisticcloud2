﻿
$(document).ready(function () {
    MisSolicitudes();
});



function MisSolicitudes() {
    showSpinner2();

    var ValPuertoDestino = $("#txtPuertoDestino option:selected").val();
    var codigoDestino = ValPuertoDestino.split('-');
    var ValPuertoSalida = $("#txtPuertoSalida option:selected").val();
    var codigoOrigen = ValPuertoSalida.split('-');

    var param = {
        cod_coti: $("#txtCodigoCoti").val(),
        Campana: $("#txtCampana option:selected").text(),
        cod_line_navi: $("#txtNaviera option:selected").val(),
        cod_puerto_destino: codigoDestino[0],
        cod_puerto_salida: codigoOrigen[0],
        id_estado: $("#cbo_estado option:selected").val(),
        TipoCotizacion: $("#cboTipoServicio option:selected").val()
    }


    $.ajax({
        url: '../CustomerCotizacion/MisSolicitudesLogistica',
        type: 'POST',
        async: false,
        data: { param: param },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);
            hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
            ;
        }
    });

}


function VerDetalle(Id_Cotizacion_Logistica) {
    //var url = '@Url.Action("Editar","OperacionLogistica", new {id="' + IdOperacionLogistica+'"})';
    url2 = "../CustomerCotizacion/VerDetalleLogistica?id=" + Id_Cotizacion_Logistica;
    window.open(url2, '_self');
}

function Editar(Cod) {
    //var url = '@Url.Action("Editar","OperacionLogistica", new {id="' + IdOperacionLogistica+'"})';
    url2 = "../CustomerCotizacion/EditarLogistica?id=" + Cod;
    window.open(url2, '_self');
}

$("#cboTipoServicio").change(function () {

    var opcion = $("#cboTipoServicio option:selected").val();
    if (opcion == '1') {
        cod = 1;
        $("#cboTipoServicio option:selected").val('1');
        var url = "../Cotizacion/MisSolicitudes";
        window.open(url, '_self');
    } else if (opcion == '2') {
        cod = 2;
        $("#cboTipoServicio option:selected").val('2');
        var url = "../Cotizacion/MisSolicitudesLogistica";
        window.open(url, '_self');
    }
    //url2 = "../Itinerario_Solicitar/Resumen";
    //window.open(url2, '_self');

});

function Confirmar_Rechazar_Logistica(id, estado) {

    $.ajax({
        url: '../CustomerCotizacion/Confirmar_Rechazar_Logistica',
        type: 'POST',
        async: false,
        data: { id: id, estado: estado },
        beforeSend: function () { },
        success: function (response) {
            //var dato = response;
            MisSolicitudes();
            //$("#gridhtml").html(dato);
            //hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
            ;
        }
    });

}