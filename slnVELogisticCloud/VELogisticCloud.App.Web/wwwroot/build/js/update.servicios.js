﻿
ObtenerEstadoInttra();
hideSpinner();
function clickInttra() {
    let estado = 0;
    if (document.getElementById('checkInttra').checked) {
        estado = 1;
    } else {
        estado = 0;
    }
    UpdateInttra(estado);
}

function UpdateInttra(estado) {
    $.ajax({
        url: '../EstadoServicios/UpdateInttra',
        type: 'POST',
        async: false,
        data: { estado: estado },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
        },

    });
}


function ObtenerEstadoInttra() {
    const checkInttra = document.querySelector("#checkInttra");
    $.ajax({
        url: '../EstadoServicios/ObtenerEstadoInttra',
        type: 'POST',
        async: false,
        //data: { estado: estado },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato.length > 0) {
                if (dato[0].activado == 1) {
                    checkInttra.checked = true;
                } else {
                    checkInttra.checked = false;
                }
            }
        },

    });
}