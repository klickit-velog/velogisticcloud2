﻿
ResumenBooking();
hideSpinner();

function VerEstadistica() {
    url2 = "../Estadistica/Index?id=" + 1;
    window.open(url2, '_self');
}

function VerEstadistica2() {
    url2 = "../Estadistica/Index?id=" + 2;
    window.open(url2, '_self');
}

function ResumenBooking(){

    $.ajax({
        url: '../Estadistica/ResumenBooking/',
        type: 'POST',
        async: true,
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            document.getElementById('total_booking').innerHTML = dato[0].total;
            document.getElementById('confirmado_booking').innerHTML = dato[0].aprobados;
            document.getElementById('pendiente_booking').innerHTML = dato[0].pendiente;
            document.getElementById('total_opl').innerHTML = dato[0].total_opl;
            document.getElementById('pendiente_opl').innerHTML = dato[0].pendiente_opl;
            document.getElementById('confirmado_opl').innerHTML = dato[0].aprobados_opl;
        }
    });
}
