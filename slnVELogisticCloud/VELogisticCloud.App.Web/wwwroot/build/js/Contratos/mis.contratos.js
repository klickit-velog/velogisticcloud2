﻿
$(document).ready(function () {

    $("#txtPuertoEmbarque").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddEmbarqueId").val(ui.item.id);
            $("#hddEmbarqueCodigoPuerto").val(ui.item.codigo);
            $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
            $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
            $("#hddEmbarqueNombreRegion").val(ui.item.nombreRegion);
        },
    });

    $("#txtPuertoDescarga").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDescargaId").val(ui.item.id);
            $("#hddDescargaCodigoPuerto").val(ui.item.codigo);
            $("#hddDescargaCodigoPais").val(ui.item.codigoPais);
            $("#hddDescargaNombrePais").val(ui.item.nombrePais);
            $("#hddDescargaNombreRegion").val(ui.item.nombreRegion);
        },
    });


    document.getElementById("link_Contratos").style.color = "black";
    document.getElementById("link_Contratos").style.fontWeight = "bold";

    ListaNaviera();
    hideSpinner2();

});




function ListaNaviera() {

    showSpinner2();
    var tipousuario = $("#tipousuario").val();
    $.ajax({
        url: '../Contratos/ListaNavieras/',
        type: 'POST',
        async: false,
        data: { tipousuario: tipousuario },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            if (dato == "0") {
                document.getElementById("gridCompleto").style.display = "none";
                document.getElementById("gridNoData").style.display = "block";
            } else {
                document.getElementById("gridCompleto").style.display = "block";
                document.getElementById("gridNoData").style.display = "none";
            }
            $("#gridListaNaviera").html(dato);
            hideSpinner();

        }
    });
}


function ListaContratos(idNaviera) {
    showSpinner2();

    $.ajax({
        url: '../Contratos/ListaContratos/',
        type: 'POST',
        async: true,
        data: { idNaviera: idNaviera },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtmldetalle").html(dato);
            hideSpinner();
        }
    });
}


function BusquedaContratos() {
    showSpinner2();

    var idPuertoCarga = $("#hddEmbarqueId").val();
    var idPuertoDescarga = $("#hddDescargaId").val();
    var Estado = $("#cbo_estados option:selected").val();

    var textoOrigen = $("#txtPuertoEmbarque").val(); 
    var textoDestino = $("#txtPuertoDescarga").val();
    if (textoOrigen.trim() == "") {
        idPuertoCarga = 0;
    }
    if (textoDestino.trim() == "") {
        idPuertoDescarga = 0;
    }

    $.ajax({
        url: '../Contratos/BusquedaContratos/',
        type: 'POST',
        async: true,
        data: { idPuertoOrigen: idPuertoCarga, idPuertoDestino: idPuertoDescarga, Estado: Estado },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtmldetalle").html(dato);
            hideSpinner();
        }
    });
}


