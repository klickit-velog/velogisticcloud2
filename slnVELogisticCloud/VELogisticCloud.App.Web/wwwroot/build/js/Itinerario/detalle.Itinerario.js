﻿var _scac = getParameterByName('_scac');
var _item = getParameterByName('_item');
var _diaSalida = getParameterByName('_diaSalida');
var _scheduleType = getParameterByName('_scheduleType');
var _OriginCode = getParameterByName('_OriginCode');
var _DestinationCode = getParameterByName('_DestinationCode');
var _puertoEmbarque = getParameterByName('puertoEmbarque');
var _puertoDescarga = getParameterByName('puertoDescarga');
var _sFecha = getParameterByName('_sFecha');
var _sWeek = getParameterByName('_sWeek');
var _sTipo = getParameterByName('_sTipo');

hideSpinner2();

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



function Volver() {
    url2 = "../Itinerario/Index?back=true" + "&_scac=" + _scac + "&_item=" + _item + "&_diaSalida=" + _diaSalida 
        + "&_scheduleType=" + _scheduleType + "&_OriginCode=" + _OriginCode + "&_DestinationCode=" + _DestinationCode
        + "&_puertoEmbarque=" + _puertoEmbarque + "&_puertoDescarga=" + _puertoDescarga
        + "&_sFecha=" + _sFecha + "&_sWeek=" + _sWeek + "&_sTipo=" + _sTipo;
        window.open(url2, '_self');
}

$(document).ready(function () {
    hideSpinner();
});
