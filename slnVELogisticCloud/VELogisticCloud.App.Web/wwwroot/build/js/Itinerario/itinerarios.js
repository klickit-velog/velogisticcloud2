﻿//var scac = getParameterByName('scac');
//var item = getParameterByName('item');
//var diaSalida = getParameterByName('diaSalida');
//var scheduleType = getParameterByName('scheduleType');
//var OriginCode = getParameterByName('OriginCode');
//var DestinationCode = getParameterByName('DestinationCode');
var _scac = "";
var _item = "";
var _diaSalida = "";
var _scheduleType = "";
var _OriginCode = "";
var _DestinationCode = "";
var _puertoEmbarque = "";
var _puertoDescarga = "";
hideSpinner2();

var back = getParameterByName('back');

if (back == "true") {
    _scac = getParameterByName('_scac');
    _item = getParameterByName('_item');
    _diaSalida = getParameterByName('_diaSalida');
    _scheduleType = getParameterByName('_scheduleType');
    _OriginCode = getParameterByName('_OriginCode');
    _DestinationCode = getParameterByName('_DestinationCode');
    _puertoEmbarque = getParameterByName('_puertoEmbarque');
    _puertoDescarga = getParameterByName('_puertoDescarga');
    _sFecha = getParameterByName('_sFecha');
    _sWeek = getParameterByName('_sWeek');
    _sTipo = getParameterByName('_sTipo');

    VerItinerario(_scac, _item, _diaSalida, _scheduleType, _OriginCode, _DestinationCode);

    $("#txtOrigen").val(_puertoEmbarque);
    $("#hddOrigen").val(_OriginCode);
    $("#txtDestino").val(_puertoDescarga);
    $("#hddDestino").val(_DestinationCode);
    $("#txtSearchDate").val(_sFecha);
    $("#txtBy").val(_sTipo);
    $("#txtWeeksOut").val(_sWeek);

    
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function () {

    //$("#txtOrigen").autocomplete({
    //    source: "../Maestros/AutoCompletePuertos",
    //    minLength: 2,
    //    select: function (event, ui) {
    //        $("#hddOrigen").val(ui.item.id);
    //    },
    //});

     $("#txtOrigen").autocomplete({

         source: "../Maestros/AutoCompletePuertosPais",
         minLength: 2,
         select: function (event, ui) {
             $("#hddEmbarqueId").val(ui.item.id);
             $("#hddOrigen").val(ui.item.codigo);
             $("#hddEmbarqueCodigoPais").val(ui.item.codigoPais);
             $("#hddEmbarqueNombrePais").val(ui.item.nombrePais);
         },
    });

    //$("#txtDestino").autocomplete({
    //    source: "../Maestros/AutoCompletePuertos",
    //    minLength: 2,
    //    select: function (event, ui) {
    //        $("#hddDestino").val(ui.item.id);
    //    },
    //});

    $("#txtDestino").autocomplete({
        source: "../Maestros/AutoCompletePuertosPais",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDestinoId").val(ui.item.id);
            $("#hddDestino").val(ui.item.codigo);
            $("#hddDestinoCodigoPais").val(ui.item.codigoPais);
            $("#hddDestinoNombrePais").val(ui.item.nombrePais);
        },
    });

    //$('#btnBuscar').click(function () {
    //    Lista() ;
    //});
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    //document.getElementById("txtSearchDate").setAttribute('min', today);
    document.getElementById("txtSearchDate").valueAsDate = new Date();

    $('[data-toggle="tooltip"]').tooltip();
    hideSpinner();

    document.getElementById("link_Itinerario").style.color = "black";
    document.getElementById("link_Itinerario").style.fontWeight = "bold";

    var txtSearchDate = document.getElementById("txtSearchDate");
    txtSearchDate.min = new Date().toISOString().split("T")[0];
    //$("#form").valid();
});

function Lista() {

    showSpinner2();

    var model = {
        "origin": $("#txtOrigen").val(),
        "originCode": $("#hddOrigen").val(),
        "destination": $("#txtDestino").val(),
        "destinationCode": $("#hddDestino").val(),
        "searchDate": $("#txtSearchDate").val(),
        "searchDateType": $("#txtBy").val() === '-1' ? '' : $("#txtBy").val(),
        "weeksOut": $("#txtWeeksOut").val() === '-1' ? '' : $("#txtWeeksOut").val(),
        "sacs": $("#txtNaviera").val() === '-1' ? '' : $("#txtNaviera").val(),
        "directOnly": 'false',
        "includeNearbyOriginPorts": $("#chkIncluirOrigenCercano").val(),
        "includeNearbyDestinationPort": $("#chkIncluirDestinoCercano").val(),
    };
    $.ajax({
        url: '../Itinerario/Index',
        type: 'POST',
        data: model,
        dataType: "json"
    });
    hideSpinner();

}


function Lista2() {
    document.getElementById("btnVolver").style.display = "none";

    if ($("#form").valid()) {
        showSpinner2();

        var model = {
            "idOrigin": $("#hddEmbarqueId").val(),
            "origin": $("#txtOrigen").val(),
            "originCode": $("#hddOrigen").val(),
            "codigoPaisOrigen": $("#hddEmbarqueCodigoPais").val(),
            "nombrePaisOrigen": $("#hddEmbarqueNombrePais").val(),


            "idDestination": $("#hddDestinoId").val(),
            "destination": $("#txtDestino").val(),
            "destinationCode": $("#hddDestino").val(),
            "codigoPaisDestino": $("#hddDestinoCodigoPais").val(),
            "nombrePaisDestino": $("#hddDestinoNombrePais").val(),
            

            "searchDate": $("#txtSearchDate").val(),
            "searchDateType": $("#txtBy").val() === '-1' ? '' : $("#txtBy").val(),
            "weeksOut": $("#txtWeeksOut").val() === '-1' ? '' : $("#txtWeeksOut").val(),
            "sacs": $("#txtNaviera").val() === '-1' ? '' : $("#txtNaviera").val(),
            "directOnly": 'false',
            "includeNearbyOriginPorts": $("#chkIncluirOrigenCercano").val(),
            "includeNearbyDestinationPort": $("#chkIncluirDestinoCercano").val(),
        };
        $.ajax({
            url: '../Itinerario/Lista2',
            type: 'POST',
            async: true,
            data: { model: model },
            beforeSend: function () { },
            success: function (response) {
                hideSpinner();

                var dato = response;
                $("#gridhtml").html(dato);
                datoItinerarios = dato;
                //hideSpinner();
                //url2 = "../Itinerario_Solicitar/Resumen";
                //window.open(url2, '_self');
            }
        });
    }
}



function VerItinerario(scac, item, diaSalida, scheduleType, OriginCode, DestinationCode) {

    _scac = scac;
    _item = item;
    _diaSalida = diaSalida;
    _scheduleType = scheduleType;
    _OriginCode = OriginCode;
    _DestinationCode = DestinationCode;
    //if ($("#form").valid()) {
    showSpinner2();
    document.getElementById("btnVolver").style.display = "block";

    //$("#btnVolver").css({ "display": "block" });

    
    var item2 = null;
    if (item > 0) {
        item2 = item;
    }
    const correoUsuario = localStorage.getItem('correoUsuario');

        $.ajax({
            url: '../Itinerario/VerItinerario',
            type: 'POST',
            async: true,
            data: { scac: scac, tt: item2, dia: diaSalida, transbordo: scheduleType, OriginCode: OriginCode, DestinationCode: DestinationCode, correoUsuario: correoUsuario},
            beforeSend: function () { },
            success: function (response) {
                hideSpinner();

                var dato = response;
                if (dato === "0") {
                    hideSpinner();
                    Swal.fire({
                        icon: 'info',
                        title: 'Oops...',
                        text: 'Linea naviera seleccionada no esta registrada en Odoo!',
                    });
                } else {
                $("#gridhtml").html(dato);
                }
                //hideSpinner();
                //url2 = "../Itinerario_Solicitar/Resumen";
                //window.open(url2, '_self');
            },
            error() {
                hideSpinner();
                Swal.fire({
                    icon: 'info',
                    title: 'Oops...',
                    text: 'Comuniquese con el administrador!',
                });
            }
        });
    //}
}

function Volver() {
    document.getElementById("btnVolver").style.display = "none";

    Lista2();
}

function SolicitarBooking(scac, originUnloc, destinationUnloc, etd, eta, vesselName, voyageNumber, Origin, Destination, NroContrato, idOrigin, idDestination
    , CodigoPaisOrigen, NombrePaisOrigen, CodigoPaisDestino, NombrePaisDestino,ImoNumber) {

    var txtOrigen = $("#txtOrigen").val();
    var txtDestino = $("#txtDestino").val();
    //var url = '@Url.Action("Editar","OperacionLogistica", new {id="' + IdOperacionLogistica+'"})';
    url2 = "../Booking/Create?scac=" + scac + "&OriginCode=" + originUnloc + "&DestinationCode=" + destinationUnloc + "&etd=" + etd + "&eta=" + eta
        + "&nombreViaje=" + vesselName + "&naveViaje=" + voyageNumber + "&puertoEmbarque="
        + txtOrigen + "&puertoDescarga=" + txtDestino + "&NroContrato=" + NroContrato + "&Tipo=Itinerario" + "&IdEmbarque=" + idOrigin + "&IdDescarga=" + idDestination
        + "&CodigoPaisOrigen=" + CodigoPaisOrigen + "&NombrePaisOrigen=" + NombrePaisOrigen + "&CodigoPaisDestino=" + CodigoPaisDestino + "&NombrePaisDestino=" + NombrePaisDestino + "&ImoNumber=" + ImoNumber;
    window.open(url2, '_self');
}

function VerDetalle(scac, diaSalida, totalDuration, Origin, Destination) {
    var txtOrigen = $("#txtOrigen").val();
    var txtDestino = $("#txtDestino").val();
    var _sFecha = $("#txtSearchDate").val();
    var _sWeek = $("#txtWeeksOut").val() === '-1' ? '' : $("#txtWeeksOut").val();
    var _sTipo = $("#txtBy").val() === '-1' ? '' : $("#txtBy").val();

    //var url = '@Url.Action("Editar","OperacionLogistica", new {id="' + IdOperacionLogistica+'"})';
    url2 = "../Itinerario/VerDetalle?scac=" + scac + "&tt=" + totalDuration + "&dia=" + diaSalida + "&transbordo=" + "1" + "&puertoEmbarque="
        + txtOrigen + "&puertoDescarga=" + txtDestino
        + "&_scac=" + _scac + "&_item=" + _item + "&_diaSalida=" + _diaSalida + "&_scheduleType=" + _scheduleType
        + "&_OriginCode=" + _OriginCode + "&_DestinationCode=" + _DestinationCode
        + "&_sFecha=" + _sFecha + "&_sWeek=" + _sWeek + "&_sTipo=" + _sTipo;
    window.open(url2, '_self');
}

//function VerItinerario(scac, item, diaSalida, scheduleType, OriginCode, DestinationCode) {
//    url2 = "../Itinerario/VerItinerario?scac=" + scac + "&tt=" + item + "&dia=" + diaSalida + "&transbordo=" + scheduleType + "&OriginCode=" + OriginCode + "&DestinationCode=" + DestinationCode;
//    window.open(url2, '_self');

//}