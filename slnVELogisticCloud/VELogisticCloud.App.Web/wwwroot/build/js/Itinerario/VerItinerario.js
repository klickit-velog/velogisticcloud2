﻿var OriginCode = getParameterByName('OriginCode');
var DestinationCode = getParameterByName('DestinationCode');

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function () {

    $("#txtOrigen").autocomplete({
        source: "../Maestros/AutoCompletePuertos",
        minLength: 2,
        select: function (event, ui) {
            $("#hddOrigen").val(ui.item.id);
        },
    });

    $("#txtDestino").autocomplete({
        source: "../Maestros/AutoCompletePuertos",
        minLength: 2,
        select: function (event, ui) {
            $("#hddDestino").val(ui.item.id);
        },
    });

    //$('#btnBuscar').click(function () {
    //    Lista();
    //});

    document.getElementById("txtOrigen").value = OriginCode;
    document.getElementById("txtDestino").value = DestinationCode;

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("txtSearchDate").setAttribute('min', today);
    document.getElementById("txtSearchDate").valueAsDate = new Date();

    $('[data-toggle="tooltip"]').tooltip();
});



function Lista() {
    showSpinner2();

    var model = {
        "origin": $("#txtOrigen").val(),
        "originCode": $("#hddOrigen").val(),
        "destination": $("#txtDestino").val(),
        "destinationCode": $("#hddDestino").val(),
        "searchDate": $("#txtSearchDate").val(),
        "searchDateType": $("#txtBy").val() === '-1' ? '' : $("#txtBy").val(),
        "weeksOut": $("#txtWeeksOut").val() === '-1' ? '' : $("#txtWeeksOut").val(),
        "sacs": $("#txtNaviera").val() === '-1' ? '' : $("#txtNaviera").val(),
        "directOnly": 'false',
        "includeNearbyOriginPorts": $("#chkIncluirOrigenCercano").val(),
        "includeNearbyDestinationPort": $("#chkIncluirDestinoCercano").val(),
    };
    $.ajax({
        url: '../Itinerario/Index',
        type: 'POST',
        data: model,
        dataType: "json"
    });
    hideSpinner();

}

function Lista2() {
    var model = {
        "origin": $("#txtOrigen").val(),
        "originCode": $("#hddOrigen").val(),
        "destination": $("#txtDestino").val(),
        "destinationCode": $("#hddDestino").val(),
        "searchDate": $("#txtSearchDate").val(),
        "searchDateType": $("#txtBy").val() === '-1' ? '' : $("#txtBy").val(),
        "weeksOut": $("#txtWeeksOut").val() === '-1' ? '' : $("#txtWeeksOut").val(),
        "sacs": $("#txtNaviera").val() === '-1' ? '' : $("#txtNaviera").val(),
        "directOnly": 'false',
        "includeNearbyOriginPorts": $("#chkIncluirOrigenCercano").val(),
        "includeNearbyDestinationPort": $("#chkIncluirDestinoCercano").val(),
    };
    $.ajax({
        url: '../Itinerario/Lista2',
        type: 'POST',
        async: true,
        data: { model: model },
        beforeSend: function () { },
        success: function (response) {
            var dato = response;
            $("#gridhtml").html(dato);

            //hideSpinner();
            //url2 = "../Itinerario_Solicitar/Resumen";
            //window.open(url2, '_self');
        }
    });
}
