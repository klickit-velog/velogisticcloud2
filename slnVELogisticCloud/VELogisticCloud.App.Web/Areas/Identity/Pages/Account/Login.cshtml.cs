﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.CrossCutting.Comun;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public LoginModel(SignInManager<ApplicationUser> signInManager, 
            ILogger<LoginModel> logger,
            UserManager<ApplicationUser> userManager, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _configuration = configuration;
            this._memoryCache = memoryCache;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }
        [TempData]
        public string StatusMessage { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "El usuario es requerido.")]
            [EmailAddress(ErrorMessage = "El correo ingresado no es un correo válido.")]
            [Display(Name = "Correo")]
            public string Email { get; set; }

            [Required(ErrorMessage = "La contraseña es requerida.")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }



        public async Task OnGetAsync(string returnUrl = null)
        {
            StatusMessage = "";
             if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {

            //if (Input.Email.Contains("customer") || Input.Email.ToLower().Contains("jazmin"))
            //{
            //    returnUrl =  Url.Content("~/CustomerCliente/Cliente");

            //}
            //else
            //{
            //returnUrl = returnUrl ?? Url.Content("~/");

            //}


            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");

                    var usuario = await _userManager.FindByNameAsync(Input.Email);

                    if(usuario.IdEstado == 0)
                    {
                        StatusMessage = "Error - El usuario se encuentra inactivo.";
                        return Page();
                    }

                    var roles = await _userManager.GetRolesAsync(usuario);
                    int partner_id = 0;
                    if (roles[0] == "Cliente" || roles[0] == "Customer Service")
                    {
                        try
                        {
                            partner_id = new OdooService(_configuration, _memoryCache).getPartnerId(Input.Email);
                        }
                        catch (Exception ex)
                        {
                            partner_id = 0;
                        }

                        if (partner_id == 0)
                        {
                            //StatusMessage.StartsWith("Error");
                            StatusMessage = "Error - El usuario no tiene registro en el ERP, por favor contactar con el administrador.";
                            return Page();
                        }
                    }


                if (roles.Contains(Rol.CUSTOMER_SERVICE))
                    {
                        returnUrl = Url.Content("~/CustomerCliente/Cliente");
                    }
                    else
                    {
                        returnUrl = returnUrl ?? Url.Content("~/");
                    }

                    return LocalRedirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToPage("./Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Usuario y/o contraseña inválido.");
                    return Page();
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
