﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.Model.Identity;

namespace VELogisticCloud.App.Web.Areas.Identity.Pages.Account.Admin
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public IndexModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager, IConfiguration configuration,  IMemoryCache memoryCache)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            this._memoryCache = memoryCache;
        }

        public string Username { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public List<SelectListItem> ListaRoles { get; set; }
        public List<SelectListItem> ListaActivo { get; set; }
        
        public class InputModel
        {
            //Propiedades personalizadas para VE Cloud
            [Required(ErrorMessage = "El Nombre es obligatorio.")]
            public string Nombre { get; set; }

            [Required(ErrorMessage = "El Razón Social es obligatorio.")]
            [Display(Name = "Razón Social")]
            public string RazonSocial { get; set; }

            [Required(ErrorMessage = "El RUC es obligatorio.")]
            public string RUC { get; set; }
            [Display(Name = "Numero de Contacto")]

            [Required(ErrorMessage = "El Numero de contacto es obligatorio.")]
            public string NumeroContacto { get; set; }

            [Required(ErrorMessage = "El Nombre de contacto es obligatorio.")]
            [Display(Name = "Nombre de Contacto")]
            public string NombreContacto { get; set; }
            public int IdUsuarioRegistro { get; set; }
            public DateTime FechaRegistro { get; set; }
            public int? IdUsuarioActualiza { get; set; }
            public DateTime? FechaActualizacion { get; set; }
            public int IdEstado { get; set; }
            [Required(ErrorMessage = "El Rol del usuario es obligatorio.")]
            [Display(Name = "Rol del usuario")]
            public string Rol { get; set; }
            public string EstadoUsuario { get; set; }
            public string RolAnterior { get; set; }
            public string IdUsuario { get; set; }

        }

        private async Task LoadAsync(ApplicationUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var roles = await _userManager.GetRolesAsync(user);
            
            Username = userName;

            Input = new InputModel
            {
                IdUsuario = user.Id,
                Nombre = user.Nombre,
                RazonSocial = user.RazonSocial,
                RUC = user.RUC,
                NombreContacto = user.NombreContacto,
                NumeroContacto = user.NumeroContacto,
                FechaRegistro = user.FechaRegistro,
                FechaActualizacion = user.FechaActualizacion,
                EstadoUsuario = user.IdEstado == 1 ? EstadoUsuario.ACTIVO : EstadoUsuario.INACTIVO
            };

            if (roles.Any())
            {
                string rolAnterior = roles.First();
                Input.RolAnterior = rolAnterior;
                Input.Rol = rolAnterior;
            }

            ListaRoles = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Seleccione", Value = ""},
                new SelectListItem(){ Text = Rol.ADMINISTRADOR, Value = Rol.ADMINISTRADOR},
                new SelectListItem(){ Text = Rol.CUSTOMER_SERVICE, Value = Rol.CUSTOMER_SERVICE},
                new SelectListItem(){ Text = Rol.CLIENTE, Value = Rol.CLIENTE}
            };

            ListaActivo = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = EstadoUsuario.ACTIVO, Value =  EstadoUsuario.ACTIVO },
                new SelectListItem(){ Text = EstadoUsuario.INACTIVO, Value =  EstadoUsuario.INACTIVO },
            };
        }

        public async Task<IActionResult> OnGetAsync(string Id)
        {
            StatusMessage = string.Empty;

            var user = await _userManager.FindByIdAsync(Id);
            //var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            ViewData["IdUsuarioEdit"] = Id;

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            ViewData["IdUsuarioEdit"] = Input.IdUsuario;

            var user = await _userManager.FindByIdAsync(Input.IdUsuario);
            
            //var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            //var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            //if (Input.PhoneNumber != phoneNumber)
            //{
            //    var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
            //    if (!setPhoneResult.Succeeded)
            //    {
            //        StatusMessage = "Unexpected error when trying to set phone number.";
            //        return RedirectToPage();
            //    }
            //}

            user.Nombre = Input.Nombre;
            user.RazonSocial = Input.RazonSocial;
            user.RUC = Input.RUC;
            user.NumeroContacto = Input.NumeroContacto;
            user.NombreContacto = Input.NombreContacto;
            user.IdUsuarioActualiza = 2; //TODO: ACTUALIZAR ID USUARIO ACTUALIZA
            user.FechaActualizacion = DateTime.Now;
            user.IdEstado = Input.EstadoUsuario == "Activo" ? 1 : 0;
             
            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                //Actualiza rol
                var resultRolRemove = await _userManager.RemoveFromRoleAsync(user, Input.RolAnterior);
                if (resultRolRemove.Succeeded)
                {
                    var resultRolAdd = await _userManager.AddToRoleAsync(user, Input.Rol);
                    //await _signInManager.RefreshSignInAsync(user);
                    StatusMessage = "El perfil fue actualizado.";
                }
            }

            await LoadAsync(user);
            return Page();
            //return RedirectToPage();
        }
    }
}
