﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VELogisticCloud.Model.Identity;

namespace VELogisticCloud.App.Web.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public IndexModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public string Username { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            //Propiedades personalizadas para VE Cloud
            public string Nombre { get; set; }

            public string RazonSocial { get; set; }

            public string RUC { get; set; }

            public string NumeroContacto { get; set; }

            public string NombreContacto { get; set; }
            public int IdUsuarioRegistro { get; set; }
            public DateTime FechaRegistro { get; set; }
            public int? IdUsuarioActualiza { get; set; }
            public DateTime? FechaActualizacion { get; set; }
            public int IdEstado { get; set; }
        }

        private async Task LoadAsync(ApplicationUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);

            Username = userName;

            Input = new InputModel
            {
                Nombre = user.Nombre,
                RazonSocial = user.RazonSocial,
                RUC = user.RUC,
                NombreContacto = user.NombreContacto,
                NumeroContacto = user.NumeroContacto,
                FechaRegistro = user.FechaRegistro,
                FechaActualizacion = user.FechaActualizacion
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            StatusMessage = string.Empty;

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            //var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            //if (Input.PhoneNumber != phoneNumber)
            //{
            //    var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
            //    if (!setPhoneResult.Succeeded)
            //    {
            //        StatusMessage = "Unexpected error when trying to set phone number.";
            //        return RedirectToPage();
            //    }
            //}

            user.Nombre = Input.Nombre;
            user.RazonSocial = Input.RazonSocial;
            user.RUC = Input.RUC;
            user.NumeroContacto = Input.NumeroContacto;
            user.NombreContacto = Input.NombreContacto;
            user.IdUsuarioActualiza = 2; //TODO: ACTUALIZAR ID USUARIO ACTUALIZA
            user.FechaActualizacion = DateTime.Now;

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            { 
                await _signInManager.RefreshSignInAsync(user);
                StatusMessage = "El perfil fue actualizado.";
            }

            return RedirectToPage();
        }
    }
}
