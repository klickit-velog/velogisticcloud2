﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.Model.Identity;

namespace VELogisticCloud.App.Web.Areas.Identity.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage ="El correo es obligatorio.")]
            [EmailAddress]
            [Display(Name = "Correo")]
            public string Email { get; set; }

            [Required(ErrorMessage = "El contraseña es obligatoria.")]
            [StringLength(100, ErrorMessage = "La {0} debe ser de al menos {2} y máximo {1} carácteres de longitud.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Contraseña")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirme contraseña")]
            [Compare("Password", ErrorMessage = "La contraseña y su confirmación no coinciden.")]
            public string ConfirmPassword { get; set; }

            //Propiedades personalizadas para VE Cloud
            [Required(ErrorMessage = "El Nombre es obligatorio.")]
            public string Nombre { get; set; }

            [Required(ErrorMessage = "El Razón Social es obligatorio.")]
            [Display(Name ="Razón Social")]
            public string RazonSocial { get; set; }

            [Required(ErrorMessage = "El RUC es obligatorio.")]
            public string RUC { get; set; }
            [Display(Name = "Numero de Contacto")]

            [Required(ErrorMessage = "El Numero de contacto es obligatorio.")]
            public string NumeroContacto { get; set; }

            [Required(ErrorMessage = "El Nombre de contacto es obligatorio.")]
            [Display(Name = "Nombre de Contacto")]
            public string NombreContacto { get; set; }
            public int IdUsuarioRegistro { get; set; }
            public DateTime FechaRegistro { get; set; }
            public int? IdUsuarioActualiza { get; set; }
            public DateTime? FechaActualizacion { get; set; }
            public int IdEstado { get; set; }

            [Required(ErrorMessage = "El Rol del usuario es obligatorio.")]
            [Display(Name = "Rol del usuario")]
            public string Rol { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            StatusMessage = string.Empty;

            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser {
                    UserName = Input.Email,
                    Email = Input.Email,
                    Nombre = Input.Nombre,
                    RazonSocial = Input.RazonSocial,
                    RUC = Input.RUC,
                    NombreContacto = Input.NombreContacto,
                    NumeroContacto = Input.NumeroContacto,
                    IdUsuarioRegistro = 1, //TODO: CAMBIAR POR EL CODIGO DEL USUARIO LOGUEADO
                    FechaRegistro = DateTime.Now,
                    IdEstado = 1,  //TODO: CAMBIAR POR EL CODIGO DEL USUARIO LOGUEADO
                    EmailConfirmed = true //TODO: CAMBIAR LA VERIFICACION DEL EMAIL
                };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    await _userManager.AddToRoleAsync(user, Input.Rol);
                    StatusMessage = "Usuario creado satisfactoriamente.";
                    //return RedirectToAction("Index", "Usuario");

                    //return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });

                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    //var callbackUrl = Url.Page(
                    //    "/Account/ConfirmEmail",
                    //    pageHandler: null,
                    //    values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                    //    protocol: Request.Scheme);

                    //await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                    //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    //if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    //{
                    //    return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    //}
                    //else
                    //{
                    //    await _signInManager.SignInAsync(user, isPersistent: false);
                    //    return LocalRedirect(returnUrl);
                    //}
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
