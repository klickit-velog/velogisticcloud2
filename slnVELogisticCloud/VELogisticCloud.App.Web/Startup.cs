using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.App.Web.Extensions;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Data.ClasesRepository;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.Servicio.Data.Repository;
using VELogisticCloud.Servicio.Data.Repository.Itinerario_Solicitar;
using Microsoft.AspNetCore.Identity;
using VELogisticCloud.App.Web.Util;
using Microsoft.AspNetCore.Identity.UI.Services;
using VELogisticCloud.CrossCutting.Comun;
using Serilog;

namespace VELogisticCloud.App.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<VECloudDBContext>(options =>
                            options.UseSqlServer(Configuration.GetConnectionString("AppDbContextConnection")));

            services.AddMemoryCache();

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                        .AddRoles<IdentityRole>()
                        .AddEntityFrameworkStores<VECloudDBContext>()
                        .AddDefaultTokenProviders()
                        .AddClaimsPrincipalFactory<VECloudUserClaimsPrincipalFactory>();

            services.AddSingleton<IEmailSender, EmailSender>();

            services.Configure<AppSettings>(Configuration.GetSection("MailSettings"));
            services.AddScoped<IEmailService, EmailService>();

            services.AddScoped<IContenedorTrabajo, ContenedorTrabajo>();
            services.AddScoped<IContenedorNaviera, ContenedorNaviera>();
            services.AddScoped<IContenedorItinearioSolicitud, ContenedorItinerarioSolicitud>();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddRazorPages(options => {
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Register", "RequireAdministratorRole");
                options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Admin", "RequireAdministratorRole");
            });

            services.ConfigureMailSender();

            services.AddCors();

            
            //services.AddSignalR();



            //Session
            //services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(5);//You can set Time   
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdministratorRole", policy => policy.RequireRole("Administrador"));
            });

            //services.AddMvc()
            //        .AddRazorPagesOptions(options =>
            //        {
            //            options.Conventions.AuthorizeAreaPage("Identity", "/Account/Register", "RequireAdministratorRole");
            //            options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Admin", "RequireAdministratorRole");
            //        });

            //
            //services.ConfigureRepository();
            //services.ConfigureEF(Configuration);

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            //Session
            app.UseSession();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<LogPropertyMiddleware>();
            app.UseSerilogRequestLogging();

            //app.UseSignalR(route => {
            //    route.MapHub<SignalServer>("/signalServer");
            //});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute().RequireAuthorization();

                //endpoints.MapControllerRoute(
                //    name: "default",
                //    pattern: "{controller=Usuario}/{action=Autenticar}/{id?}");

                //endpoints.MapControllerRoute(
                //    name: "default",
                //    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });
        }
    }
}
