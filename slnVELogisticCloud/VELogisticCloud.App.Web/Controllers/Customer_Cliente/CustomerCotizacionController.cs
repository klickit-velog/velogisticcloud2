﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Servicio.Business.Cotizacion;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.App.Web.Controllers.Customer_Cliente
{
    public class CustomerCotizacionController : Controller
    {
        private readonly ILogger _logger;
        private IConfiguration _configuration;
        public CustomerCotizacionController(ILogger<CustomerCotizacionController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        //CustomerCotizacion
        #region Vistas Maritimo 

        public IActionResult MisSolicitudes()
        {
            return View();
        }
        public IActionResult VerDetalleMaritimo(string id)
        {
            return View();
        }
        public IActionResult EditarMaritimo(string id)
        {
            return View();
        }
        #endregion
        #region Vistas Logistica 
        public IActionResult MisSolicitudesLogistica()
        {
            return View();
        }
        public IActionResult DetalleLogistica()
        {
            return View();
        }
        public IActionResult VerDetalleLogistica(string id)
        {
            return View();
        }
        public IActionResult EditarLogistica(string id)
        {
            return View();
        }
        #endregion


        #region Metodos Maritimo
        [HttpPost]
        public ActionResult MisSolicitudes(Parametros_Lista_Maritimo param)
        {
            string htmlGrid = "";
            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == "Todas")
            {
                param.Campana = "";
            }

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_MisSolicitudesMaritimo(param);
                htmlGrid = crearGridShowSolicitudes(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/MisSolicitudes");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShowSolicitudes(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<Lista_Maritimo> lista = (List<Lista_Maritimo>)list;

            List<int> id_cotizacion = new List<int>();
            foreach (var lis in lista)
            {
                #region html

                if (id_cotizacion.Contains(lis.Codigo_coti))
                {

                }
                else
                {
                    //List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
                    //Ent_Reserva ent = (Ent_Reserva)lis;
                    //if (campana.Contains(lis.Campania))
                    //{
                    id_cotizacion.Add(lis.Codigo_coti);
                    //}
                    //else
                    //{
                    //campana.Add(lis.Campania);

                    //campana.Where(x => x.)
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-2\" >");
                    htmlShow.Append("<h4> " + lis.Campania + "</h4> Campaña");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.Append("<h4> " + Convert.ToInt32(lis.Codigo_coti) + "</h4>" + "Nro Cotización");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-6\">");
                    htmlShow.Append("<h4> " + Convert.ToInt32(lis.Cantidad_Contenedor) + "</h4>" + "Contenedores");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.Append("Estado <i class=\"fas fa-exclamation-circle\"></i>" + lis.Estado);
                    htmlShow.Append("</div>");

                    htmlShow.Append("</div>");

                    htmlShow.Append("<table class=\"table table-hover\" >");
                    htmlShow.Append("<tbody>");
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<th>Servicio</th>");
                    htmlShow.Append("<th>Origen</th>");
                    htmlShow.Append("<th>Destino</th>");
                    htmlShow.Append("<th>Nro Servicio</th>");

                    htmlShow.Append("<th>");
                    htmlShow.Append("</th>");

                    htmlShow.Append("<th class=\"col-6\"> ");
                    htmlShow.Append("</th>");


                    htmlShow.Append("<th>");
                    htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='Editar({0});'>Editar</a>", lis.Codigo_coti);
                    htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='VerDetalle({0});'>Ver Detalle</a>", lis.Codigo_coti);
                    htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='DescargarPDF({0});'>Descargar PDF</a>", lis.Codigo_coti);
                    htmlShow.Append("</th>");
                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td> Maritimo");
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td>" + lis.POD);
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td>" + lis.POL);
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td> 20210413");
                    htmlShow.Append("</td>");


                    htmlShow.Append("<td > ");
                    htmlShow.Append("</td>");


                    htmlShow.Append("<td > ");
                    htmlShow.Append("</td>");

                    htmlShow.Append("</tr>");
                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</div>");

                    htmlShow.Append(" <div class=\"card-footer\">");
                    if (lis.IdEstado == 2)
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-8\">");
                        htmlShow.Append("</div>");

                        htmlShow.Append("<div class=\"col-2\">");
                        htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:red\" class=\"btn btn-danger float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Maritimo({0},{1}); \">Rechazar </button>", lis.Codigo_coti, 4);
                        htmlShow.Append("</div>");
                        htmlShow.Append("<div class=\"col-2\">");
                        htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:green\" class=\"btn btn-success float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Maritimo({0},{1}); \">Confirmar </button>", lis.Codigo_coti, 3);
                        htmlShow.Append("</div>");

                        htmlShow.Append("</div>");

                    }
                    else if (lis.IdEstado == 3)
                    {
                        htmlShow.AppendFormat(" <button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"RealizarBooking({0}); \"> Solicitar booking</button>", lis.Codigo_coti);
                    }

                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                    //}
                }

            }
            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
        [HttpPost]
        public ActionResult DetalleMaritimo(int id)
        {
            //ParametrosBusqueda param = new ParametrosBusqueda();
            //param.NroContenedor = "";
            //param.CodigoBL = "";
            //param.NroDAM = "";
            //param.NroViaje = "";
            //param.NroBooking = "";
            //param.Commodity = "";
            //param.IdOperacionLogistica = id;

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_DetalleMaritimo(id);
                List<DetalleMaritimo> lsta = (List<DetalleMaritimo>)res;
                DetalleMaritimo detalle = (DetalleMaritimo)lsta[0];
                return Ok(detalle);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/DetalleMaritimo");
                return Ok("");
            }
        }
        [HttpPost]
        public ActionResult TablaDetalleMaritimo(int id)
        {
            string htmlGrid = "";

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_DetalleMaritimo(id);
                htmlGrid = crearGridDetalle(res);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/TablaDetalleMaritimo");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
        public string crearGridDetalle(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<DetalleMaritimo> lista = (List<DetalleMaritimo>)list;

            #region html

            htmlShow.Append("<table class=\"table table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
            htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
            htmlShow.Append("<tr>");
            htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">REGIÓN</th>");
            htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">LINEA</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POL</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POD</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">COMMODITY</th>");
            htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">NETO</th>");
            htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">REBATE</th>");
            htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">FLETE B/L</th>");
            htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">TT</th>");
            htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">Válido Desde</th>");
            htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">Válido Hasta</th>");

            htmlShow.Append("</tr>");

            foreach (var lis1 in lista)
            {
                htmlShow.Append("<tr>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Region + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Linea_Naviera + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Puerto_Embarque + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Puerto_Descarga + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Commodity + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NETO + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.REBATE + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.FLETE + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.TT + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Desde + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Hasta + "</td>");

                htmlShow.Append("</tr>");
            }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            #endregion

            return htmlShow.ToString();

        }
        [HttpPost]
        public ActionResult DatosEditar(int id)
        {
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var dato = BL.DatosEditarMaritimo(id);
                List<DatosEditarMaritimo> lsta = (List<DatosEditarMaritimo>)dato;
                DatosEditarMaritimo ent = (DatosEditarMaritimo)lsta[0];
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
                return Ok(ent);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/DatosEditar");
            }
            return Ok("");

        }
        [HttpPost]
        public ActionResult TablaEditarDetalle(int id)
        {
            string htmlGrid = "";

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var dato = BL.DatosEditarMaritimo(id);
                List<DatosEditarMaritimo> lsta = (List<DatosEditarMaritimo>)dato;
                htmlGrid = crearGridEditDetalle(dato);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/TablaEditarDetalle");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
        public string crearGridEditDetalle(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<DatosEditarMaritimo> lista = (List<DatosEditarMaritimo>)list;

            #region html

            htmlShow.Append("<table class=\"table table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
            htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
            htmlShow.Append("<tr>");
            htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">REGIÓN</th>");
            htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">LINEA</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POL</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POD</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">COMMODITY</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">TAMAÑO</th>");
            htmlShow.Append("<th style=\"color:red\" style=\"border: 1px solid #ccc;\">Eliminar</th>");

            htmlShow.Append("</tr>");

            foreach (var lis1 in lista)
            {
                htmlShow.Append("<tr>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Region + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Linea_Naviera + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Puerto_Embarque + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Puerto_Descarga + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Commodity + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Tamanio + "</td>");
                htmlShow.Append("<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <a href=\"#\" onclick='EliminarDetalle(" + lis1.Id_detalle_cotizacion_maritimo + "); return false'> <i class=\"fa fa-times \" style=\"color: red\"></i>  </a> </td>");
                htmlShow.Append("</tr>");
            }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            #endregion

            return htmlShow.ToString();

        }
        //[HttpPost]
        public ActionResult InsertDetalleCotizacionMartitimo(BE_Det_Maritimo det)
        {
            string htmlGrid = "";
            string dato = "";
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_InsertMaritimoDetalle(det);
                dato = det.Id_Cotizacion_Maritimo.ToString();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/InsertDetalleCotizacionMartitimo");
                htmlGrid = "";
            }
            return Ok(dato);
        }

        public ActionResult DeleteMaritimoDetalle(int id)
        {
            string dato = "";
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.DeleteMaritimoDetalle(id);
                dato = res.ToString();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/DeleteMaritimoDetalle");
            }
            return Ok(dato);
        }
        public ActionResult UpdateCabeceraMaritimo(int id, BE_Maritimo ent)
        {
            string dato = "";
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                string UserUpdate = HttpContext.User.Identity.Name;
                var res = BL.UpdateMaritimoCabecera(id, ent, UserUpdate);
                //dato = det.Id_Cotizacion_Maritimo.ToString();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/UpdateCabeceraMaritimo");
            }
            return Ok(dato);
        }


        #endregion

        #region Metodos Logistica
        [HttpPost]
        public ActionResult MisSolicitudesLogistica(Parametros_Lista_Maritimo param)
        {
            string htmlGrid = "";
            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == "Todas")
            {
                param.Campana = "";
            }

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_MisSolicitudes_Logistica(param);
                htmlGrid = crearGridShowSolicitudesLogistica(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/MisSolicitudesLogistica");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShowSolicitudesLogistica(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<Mis_Solicitudes_Logisticas> lista = (List<Mis_Solicitudes_Logisticas>)list;

            List<string> campana = new List<string>();
            foreach (var lis in lista)
            {
                #region html

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-2\" >");
                htmlShow.Append("<h4> " + lis.Campania + "</h4> Campaña");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"col-2\">");
                htmlShow.Append("<h4> " + Convert.ToInt32(lis.IdCotizacionLogistica) + "</h4>" + "Nro Cotización");
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-6\">");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"col-2\">");
                htmlShow.Append("Estado <i class=\"fas fa-exclamation-circle\"></i>" + lis.Estado);
                htmlShow.Append("</div>");

                htmlShow.Append("</div>");

                htmlShow.Append("<table class=\"table table-hover\" >");
                htmlShow.Append("<tbody>");
                htmlShow.Append("<tr>");
                htmlShow.Append("<th>Servicio</th>");
                htmlShow.Append("<th>Origen</th>");
                htmlShow.Append("<th>Destino</th>");
                htmlShow.Append("<th>Nro Servicio</th>");

                htmlShow.Append("<th>");
                htmlShow.Append("</th>");

                htmlShow.Append("<th class=\"col-6\"> ");
                htmlShow.Append("</th>");


                htmlShow.Append("<th>");
                if (lis.IdEstado == 1)
                {
                    htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='Editar({0});'>Editar</a>", lis.IdCotizacionLogistica);
                }
                htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='VerDetalle({0});'>Ver Detalle</a>", lis.IdCotizacionLogistica);
                htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='DescargarPDF({0});'>Descargar PDF</a>", lis.IdCotizacionLogistica);
                htmlShow.Append("</th>");
                htmlShow.Append("</tr>");

                htmlShow.Append("<tr>");
                htmlShow.Append("<td>Operación Logística");
                htmlShow.Append("</td>");

                htmlShow.Append("<td>" + lis.NombrePuertoCarga);
                htmlShow.Append("</td>");

                htmlShow.Append("<td>" + lis.NombreTerminalRetiroVacio);
                htmlShow.Append("</td>");

                htmlShow.Append("<td>" + lis.NroServicio);
                htmlShow.Append("</td>");


                htmlShow.Append("<td> ");
                htmlShow.Append("</td>");

                htmlShow.Append("</tr>");
                htmlShow.Append("</tbody>");
                htmlShow.Append("</table>");
                htmlShow.Append("</div>");

                htmlShow.Append(" <div class=\"card-footer\">");
                if (lis.IdEstado == 2)
                {
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-8\">");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:red\" class=\"btn btn-danger float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Logistica({0},{1}); \">Rechazar </button>", lis.IdCotizacionLogistica, 4);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:green\" class=\"btn btn-success float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Logistica({0},{1}); \">Confirmar </button>", lis.IdCotizacionLogistica, 3);
                    htmlShow.Append("</div>");

                    htmlShow.Append("</div>");
                }
                else if (lis.IdEstado == 3)
                {
                    htmlShow.Append(" <button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"RealizarBooking(); \"> Solicitar booking</button>");
                }
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

                //}
            }


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult Confirmar_Rechazar_Logistica(int id, int estado)
        {
            string cod = "";
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                //ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
                var res = BL.BL_Confirmar_RechazarCotizacionLogistica(id, estado);
                cod = (string)res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/Confirmar_Rechazar_Logistica");
            }
            return Ok(cod);
        }
        [HttpPost]
        public ActionResult Update(BE_Logistica ent)
        {
            string cod = "";
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
                var res = BL.BL_InsertOpeLogi(ent);
                cod = (string)res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/Update");
            }
            return Ok(cod);
        }
        [HttpPost]
        public ActionResult DetalleLogistica(int id)
        {
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_DetalleLogistica(id);
                List<DetalleLogistica> lsta = (List<DetalleLogistica>)res;
                DetalleLogistica detalle = (DetalleLogistica)lsta[0];
                return Ok(detalle);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/DetalleLogistica");
                return Ok("");
            }
        }
        [HttpPost]
        public ActionResult DatosEditarLogistica(int id)
        {
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.DatosEditarLogistica(id);
                List<DatosEditarLogistica> lsta = (List<DatosEditarLogistica>)res;
                DatosEditarLogistica detalle = (DatosEditarLogistica)lsta[0];
                return Ok(detalle);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/DatosEditarLogistica");
                return Ok("");
            }
        }
        [HttpPost]
        public ActionResult UpdateLogistica(int id, BE_Logistica ent)
        {
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_UpdateCotizacionLogistica(id, ent);
                return Ok(res);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error CustomerCotizacionController/UpdateLogistica");
                return Ok("");
            }
        }

        #endregion
    }
}