﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Model.Identity;
using Microsoft.AspNetCore.Identity;

using VELogisticCloud.Servicio.Middleware;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

namespace VELogisticCloud.App.Web.Controllers.Customer_Cliente
{
    public class CustomerClienteController : Controller
    {

        private readonly ILogger _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        public static List<Cliente> _listarClientes = new List<Cliente>();
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public CustomerClienteController(ILogger<CustomerClienteController> logger, UserManager<ApplicationUser> userManager, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _userManager = userManager;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        //CustomerCliente
        public IActionResult Cliente()
        {
            return View();
        }

        public IActionResult SeleccionCliente()
        {
            return View();
        }

        public IActionResult ObtenerCliente()
        {
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> ObtenerNuevoUsuario(string correo, string nombre)
        {
            string htmlGrid = "";

            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);

                var oldClaims = await _userManager.GetClaimsAsync(user);

                var newClaims = new[]
                {
                new Claim(correo , nombre),
                };

                foreach (var newClaim in newClaims.Where(nc => nc != null))
                {
                    foreach (var oldClaim in oldClaims)
                    {
                        await _userManager.RemoveClaimAsync(user, oldClaim);
                    }
                    await _userManager.AddClaimAsync(user, newClaim);
                }

                var datos = await _userManager.GetClaimsAsync(user);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error CustomerCliente/ListaCliente");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public async Task<ActionResult> ReiniciarClaims(string RazonSocial, string RUC, string Correo)
        {
            string ok = "";

            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);

                var oldClaims = await _userManager.GetClaimsAsync(user);
                foreach (var oldClaim in oldClaims)
                {
                    await _userManager.RemoveClaimAsync(user, oldClaim);
                }
                ok = "1";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error CustomerCliente/ReiniciarClaims");
                ok = "0";
            }
            return Ok(ok);
        }

        [HttpPost]
        public async Task<ActionResult> ListaCliente(string RazonSocial, string RUC, string Correo)
        {
            string htmlGrid = "";

            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);

                var oldClaims = await _userManager.GetClaimsAsync(user);
                foreach (var oldClaim in oldClaims)
                {
                    await _userManager.RemoveClaimAsync(user, oldClaim);
                }
                htmlGrid = crearGridDetalle(RazonSocial, RUC, Correo);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error CustomerCliente/ListaCliente");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
        public string crearGridDetalle(string RazonSocial, string RUC, string Correo)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");
            string User = HttpContext.User.Identity.Name;

            var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(name:RazonSocial, numeroDocumento:RUC, correo:Correo, correoCustomerService:User);
            var listarClientesOpl = new OdooService(_configuration, _memoryCache).ListarClientesPorCustomer(name: RazonSocial, numeroDocumento: RUC, correo: Correo, correoCustomerServiceSecundario: User);
            //var listarClientes = new OdooService(_configuration).ListarClientes(0, 500, "", "admin@mail.com");
            listarClientes.AddRange(listarClientesOpl);
            //listarClientes = listarClientes.GroupBy(x => x).Distinct().ToList();
            var result = (from item in listarClientes
                           group item by new { item.Id, item.Correo, item.Nombre, item.NumeroDocumento, item.TipoDocumento } into g
                           select new Cliente()
                           {
                               Id = g.Key.Id,
                               Correo = g.Key.Correo,
                               Nombre = g.Key.Nombre,
                               NumeroDocumento = g.Key.NumeroDocumento,
                               TipoDocumento = g.Key.TipoDocumento,
                           }).ToList();
            listarClientes = result;


            #region html

            htmlShow.Append("<table class=\"table table-hover\" id=\"data\">");
            htmlShow.Append("<thead>");
            htmlShow.Append("<tr>");
            htmlShow.Append("<th scope=\"col\">#</th>");
            htmlShow.Append("<th scope=\"col\">Cliente</th>");
            htmlShow.Append("<th scope=\"col\">Tipo Doc.</th>");
            htmlShow.Append("<th scope=\"col\">Numero Documento</th>");
            htmlShow.Append("<th scope=\"col\">Correo</th>");
            htmlShow.Append("<th scope=\"col\" class=\"text-right\"></th>");
            htmlShow.Append("</tr>");
            htmlShow.Append("</thead>");
            htmlShow.Append("<tbody id=\"myTableBody\">");
            
            int num=0;
            foreach (var lis1 in listarClientes)
            {
                if(lis1.Correo != null)
                {
                    num++;
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td scope=\"row\"> " + num + " </td>");
                    htmlShow.Append("<td> " + lis1.Nombre + " </td>");
                    htmlShow.Append("<td> " + lis1.TipoDocumento + " </td>");
                    htmlShow.Append("<td> " + lis1.NumeroDocumento + " </td>");
                    htmlShow.Append("<td> " + lis1.Correo + " </td>");
                    htmlShow.Append("<td> <a href = \"#\" onclick = \"ObtenerNuevoUsuario('" + Funciones.QuitarComillasSimples(lis1.Correo) + "','" + Funciones.QuitarComillasSimples(lis1.Nombre) + "'); return false;\"> Ingresar </a>  </td>");

                    htmlShow.Append("</tr>");
                }
              
            }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            #endregion

            return htmlShow.ToString();

        }


    }
}