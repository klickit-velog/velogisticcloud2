﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.Servicio.Business.BL_Proyecciones;
using VELogisticCloud.Servicio.Middleware;
using static VELogisticCloud.Models.Proyecciones;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class ProyeccionesController : Controller
    {
        private readonly ILogger<ProyeccionesController> logger;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        //Proyecciones

        public ProyeccionesController(ILogger<ProyeccionesController> logger, IConfiguration configuration, IMemoryCache memoryCache)
        {
            this.logger = logger;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        public IActionResult Index()
        {
            /*
             * 
             * Ejemplo de uso de Logger
             * 
            
            logger.LogWarning("mensaje de prueba");
            try
            {
                throw new Exception("error de prueba");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "ocurrio un error generado a proposito.");
            }
            */

            return View();
        }
        public IActionResult Create()
        {
            ModelCombos model = new ModelCombos();

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;

            return View(model);
        }

        public IActionResult ViewDatos(int id)
        {

            return View();
        }

        [HttpPost]
        public ActionResult InsertProyeccion(Cabecera_Proyecciones cabecera,Detalle_Proyecciones detalle)
        {

            //Resultado retorno = new Resultado();
            string htmlGrid = "";

            try
            {
                BL_Proyecciones BL = new BL_Proyecciones(_configuration);
                string UserCreacion = HttpContext.User.Identity.Name;

                var res = BL.InsertCabecera(cabecera, UserCreacion);
                string codigo = (string)res;
                detalle.Id_Proyeccion = Convert.ToDecimal(codigo);
                var res2 = BL.InsertDetalle(detalle, UserCreacion);

            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Proyeccion/InsertProyeccion");
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public ActionResult ListaProyeccion(string fecha,string codComodity, string codDestino)
        {

            //Resultado retorno = new Resultado();
            string htmlGrid = "";
            if (fecha == null)
            {
                fecha = "";
            }

            try
            {
                BL_Proyecciones BL = new BL_Proyecciones(_configuration);
                var res = BL.Lista_Proyecciones(fecha, codComodity, codDestino);
                htmlGrid = crearGridShow(res);


            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Proyeccion/ListaProyeccion");
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public ActionResult ViewDatosStore(int id)
        {
            try
            {
                BL_Proyecciones BL = new BL_Proyecciones(_configuration);
                var res = BL.ViewDatos(id);
               List<PROYECCION_DETALLE> view = (List<PROYECCION_DETALLE>)res;
                PROYECCION_DETALLE ent = (PROYECCION_DETALLE)view[0];
                return Ok(ent);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Proyeccion/ViewDatosStore");
            }
            return Ok("");
        }

        public string crearGridShow(object lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            List<Lista_Proyecciones> lista_ = (List<Lista_Proyecciones>)lista;
            #region html

            if (lista_.Count > 0)
            {
                htmlShow.Append("<div class=\"container-fluid\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-12\">");
                htmlShow.Append("<div class=\"card primary\">");
                htmlShow.Append("<div class=\"card-body table-responsive p-0\">");
                htmlShow.Append("<table class=\"table table-hover text-nowrap\">");

                htmlShow.Append("<thead>");
                htmlShow.Append("<th>Campaña</th>");
                htmlShow.Append("<th>Tipo</th>");
                htmlShow.Append("<th>Commodity</th>");
                htmlShow.Append("<th>Cantidad</th>");
                htmlShow.Append("<th>Origen</th>");
                htmlShow.Append("<th>Destino</th>");

                htmlShow.Append("</thead>");
                htmlShow.Append("<tbody>");

                foreach (var lis in lista_)
                {

                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td>" + lis.Campania + "</td>");
                    htmlShow.Append("<td>" + lis.Tipo + "</td>");
                    htmlShow.Append("<td>" + lis.Commodity + "</td>");
                    htmlShow.Append("<td>" + lis.Cantidad + "</td>");
                    htmlShow.Append("<td>" + lis.Origen + "</td>");
                    htmlShow.Append("<td>" + lis.Destino + "</td>");
                    htmlShow.AppendFormat("<td><a href=\"#\" onclick='ViewDatos({0});'>Ver Datos</a></td>", lis.Id_Proyeccion.ToString());
                    htmlShow.Append("</tr>");

                }

                htmlShow.Append("</tbody>");
                htmlShow.Append("</table>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }

            #endregion
            //if (listObras != null && listObras.Count > 0)
            //{
            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult UpdateProyeccion(Detalle_Proyecciones detalle)
        {

            //Resultado retorno = new Resultado();
            string codigo = "";

            try
            {
                BL_Proyecciones BL = new BL_Proyecciones(_configuration);
                string UserUpdate = HttpContext.User.Identity.Name;
                var res2 = BL.UpdateDetalle(detalle, UserUpdate);
                codigo = (string)res2;

            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Proyeccion/UpdateProyeccion");
            }
            return Ok(codigo);
        }
    }
}