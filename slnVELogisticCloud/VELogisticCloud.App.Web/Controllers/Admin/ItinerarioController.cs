﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models;
using VELogisticCloud.Models.General;
using VELogisticCloud.Servicio.Business.General;
using VELogisticCloud.Servicio.Business.Reservas;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class ItinerarioController : Controller
    {
        public static ItinerarioRequestDTO Staticrequest = new ItinerarioRequestDTO();
        public static List<ItinerarioResponseDTO> Staticresponse = new List<ItinerarioResponseDTO>();
        private BL_Variable bl_Variable;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public ItinerarioController(ILogger<ItinerarioController> logger, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _configuration = configuration;
            _memoryCache = memoryCache;
            bl_Variable = new BL_Variable(_configuration);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var modelo = new ItinerarioViewModel();
            var listItems = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listItems.Add(new SelectListItem()
            {
                Value = a.Codigo.ToString(),
                Text = a.Nombre
            }));

            modelo.ListaNavieras = listItems;
            return View(modelo);
        }

        [HttpPost]
        public IActionResult VerItinerario(string scac, int? tt, string dia,string transbordo,string OriginCode, string DestinationCode,string correoUsuario)
        {
            var itinerarioViewModel = HttpContext.Session.GetObject<ItinerarioViewModel>("ListaItinerarios");//TODO: VALIDAR TIEMPO DE EXPIRACION DE SESSION
            //var itinerarioViewModel_temp = TempData["ListaItinerarios"];
            //ItinerarioViewModel itinerarioViewModel = (ItinerarioViewModel)itinerarioViewModel_temp;
            //scac = COSU & dia = domingo & transbordo = unspecified

            if (itinerarioViewModel == null) return View("Index");
            ItinerarioViewModel objecto = new ItinerarioViewModel();
            objecto.ListaItinerarios = new List<ItinerarioResponseDTO>();
            objecto.ListaItinerarios = itinerarioViewModel.ListaItinerarios
            .Where(a => a.diaSalida.ToLower().Trim() == dia.ToLower().Trim())
            .Where(a => a.scac.ToLower().Trim() == scac.ToLower().Trim()).ToList();

            //objecto.ListaItinerarios = objecto.ListaItinerarios;
            //var itinerariosFiltrados2 = itinerarioViewModel.ListaItinerarios.Where(a => a.diaSalida == dia);

            //.ToList();
            if (tt != null)
            {
                objecto.ListaItinerarios = objecto.ListaItinerarios.Where(x => x.totalDuration == tt).ToList();
            }

            if (transbordo == "transshipment")
            {
                objecto.ListaItinerarios = objecto.ListaItinerarios.Where(a => a.scheduleType == "transshipment").ToList();
            }
            else
            {
                objecto.ListaItinerarios = objecto.ListaItinerarios.Where(a => a.scheduleType != "transshipment").ToList();
            }

            //List<ContratoDetalle> lista = new List<ContratoDetalle>();

            string numeroContrato = string.Empty;
            if (objecto.ListaItinerarios.ToList().Count > 0)
            {
                var usuario = HttpContext.User.Identity.Name;
                //correoUsuario
                var listaContratos = new OdooService(_configuration, _memoryCache).ListarContratos(correoUsuario,  obtenerDatosComplementarios: true); 
                var listaNaviera = new OdooService(_configuration, _memoryCache).ListarNavieras();


                foreach (var itinerario in objecto.ListaItinerarios)
                {
                    var existe = listaNaviera.Where(x => x.Codigo == itinerario.scac).FirstOrDefault();
                    if(existe != null)
                    {
                        int idNaviera = listaNaviera.Where(x => x.Codigo == itinerario.scac).FirstOrDefault().Id;

                        var listaContratosNaviera = listaContratos.Where(a => a.IdNaviera == idNaviera && a.CodigoPuertoCarga == OriginCode &&
                                                                                            a.CodigoPuertoDescarga == DestinationCode).ToList();
                        if (listaContratosNaviera.Any())
                            if (listaContratosNaviera.Any())
                            {
                                var ultimoContrato = listaContratosNaviera.OrderBy(a => a.IdContrato).Last();
                                var ultimoContrato2 = listaContratosNaviera.OrderBy(a => a.IdContrato).ToList();

                                itinerario.FechaContratoExpiracion = ultimoContrato.ValidoDesde.HasValue ? ultimoContrato.ValidoDesde.Value.ToString("dd/MM/yyyy") : "";
                                itinerario.IdContrato = ultimoContrato.IdContrato;
                                numeroContrato = ultimoContrato.NumeroContrato;

                                var listaContrataDetallePuertos = ultimoContrato2.Where(c => c.CodigoPuertoCarga == OriginCode &&
                                                                                  c.CodigoPuertoDescarga == DestinationCode).ToList();

                                if (listaContrataDetallePuertos.Any())
                                {
                                    var ultimoContratoDetalle = listaContrataDetallePuertos.OrderBy(a => a.IdCotizacionDetalleOdoo).Last();
                                    itinerario.Flete = ultimoContratoDetalle.FleteBL;
                                    itinerario.Neto = ultimoContratoDetalle.Neto;
                                    itinerario.Rebate = ultimoContratoDetalle.Rebate;
                                }
                            }
                    }else
                    {                                         
                        return Ok("0");
                    }
                    
                }
            }

            itinerarioViewModel.ListaItinerarios = objecto.ListaItinerarios.ToList();
            itinerarioViewModel.Origin = OriginCode;
            itinerarioViewModel.Destination = DestinationCode;
            itinerarioViewModel.NroContrato = numeroContrato;

            string htmlGrid = "";
            htmlGrid =crearGridShowVerItinerario(itinerarioViewModel);

            return Ok(htmlGrid);


        }

        [HttpGet]
        public IActionResult VerDetalle(string scac, int tt, string dia, string transbordo , string puertoEmbarque, string puertoDescarga)
        {
            var itinerarioViewModel = HttpContext.Session.GetObject<ItinerarioViewModel>("ListaItinerarios");

            //var itinerarioViewModel_temp = TempData["ListaItinerarios"];
            //ItinerarioViewModel itinerarioViewModel = (ItinerarioViewModel)itinerarioViewModel_temp;

            if (itinerarioViewModel == null) return View("Index");

            var itinerariosFiltrados = itinerarioViewModel.ListaItinerarios.Where(a => a.scac == scac)
                                                                           .Where(a => a.diaSalida == dia)
                                                                           .Where(a => a.totalDuration == tt);
            //if (transbordo == "transshipment")
            //{
            //    itinerariosFiltrados = itinerariosFiltrados.Where(a => a.scheduleType == "transshipment");
            //}
            //else
            //{
            //    itinerariosFiltrados = itinerariosFiltrados.Where(a => a.scheduleType != "transshipment");
            //}

            itinerarioViewModel.ListaItinerarios = itinerariosFiltrados.ToList();
            itinerarioViewModel.Origin = puertoEmbarque;
            itinerarioViewModel.Destination = puertoDescarga;

            var first = itinerarioViewModel.ListaItinerarios.First();
            first.puertoEmbarque = puertoEmbarque;
            first.puertoDestino = puertoDescarga;

            return View(first);
        }

        [HttpPost]
        public async Task<IActionResult> Lista1(ItinerarioViewModel model)
        {
            ItinerarioRequestDTO request = new ItinerarioRequestDTO();
            request.destinationPort = model.DestinationCode;
            request.directOnly = model.Directo;
            request.includeNearbyDestinationPort = model.includeNearbyDestinationPort;
            request.includeNearbyOriginPorts = model.includeNearbyOriginPorts;
            request.originPort = model.OriginCode;
            request.sacs = model.Sacs;
            request.searchDate = model.SearchDate;
            request.searchDateType = model.SearchDateType;
            request.weeksOut = model.WeeksOut;

            InttraService service = new InttraService();
            var response = await service.ListarItinerarios(request);
            List<ItinerarioResponseDTO> ListaManualDTO = new List<ItinerarioResponseDTO>();
            BL_Reserva BL = new BL_Reserva(_configuration);
            var ItinerarioManual = BL.BL_ListaItinerarioManual();
            List<BE_ItinerarioManual> listaManual = (List<BE_ItinerarioManual>)ItinerarioManual;
            listaManual = listaManual.Where(x => x.originUnloc == request.originPort && x.destinationUnloc == request.destinationPort).ToList();
            foreach (var a in listaManual)
            {
                ItinerarioResponseDTO ent = new ItinerarioResponseDTO();
                ent.scac = a.scac;
                ent.carrierName = a.carrierName;
                ent.vesselName = a.vesselName;
                ent.voyageNumber = a.voyageNumber;
                ent.imoNumber = a.imoNumber;
                ent.originUnloc = a.originUnloc;
                ent.originCountry = a.originCountry;
                ent.originCityName = a.originCityName;
                ent.originSubdivision = a.originSubdivision;
                ent.destinationUnloc = a.destinationUnloc;
                ent.destinationCountry = a.destinationCountry;
                ent.destinationCityName = a.destinationCityName;

                ent.originDepartureDate = Convert.ToDateTime(a.originDepartureDate);
                ent.destinationArrivalDate = Convert.ToDateTime(a.destinationArrivalDate);
                ent.terminalCutoff = Convert.ToDateTime(a.terminalCutoff);
                ListaManualDTO.Add(ent);
            }

            if (ListaManualDTO.Count > 0)
            {
                response.AddRange(ListaManualDTO);
            }
            model.ListaItinerarios = response;
            model.OriginCode = model.OriginCode;
            model.DestinationCode = model.DestinationCode;
            model.Origin = model.OriginCode;
            model.Destination = model.DestinationCode;
            //TempData["ListaItinerarios"] = model;
            HttpContext.Session.SetObject("ListaItinerarios", model);

            return View(model);
        }

        //Primera carga en la vista
        [HttpPost]
        public async Task<IActionResult> Lista2(ItinerarioViewModel model)
        {
            string htmlGrid = "";
            try
            {
                ItinerarioRequestDTO request = new ItinerarioRequestDTO();
                request.destinationPort = model.DestinationCode;
                request.directOnly = model.Directo;
                request.includeNearbyDestinationPort = model.includeNearbyDestinationPort;
                request.includeNearbyOriginPorts = model.includeNearbyOriginPorts;
                request.originPort = model.OriginCode;
                request.sacs = model.Sacs;
                request.searchDate = model.SearchDate;
                request.searchDateType = model.SearchDateType;
                request.weeksOut = model.WeeksOut;

                bool resul = false;
                if(Staticrequest is null)
                {
                    Staticrequest = new ItinerarioRequestDTO();
                }
                if (Staticresponse is null)
                {
                    Staticresponse = new List<ItinerarioResponseDTO>();
                }
                if (request.destinationPort == Staticrequest.destinationPort && 
                    request.directOnly == Staticrequest.directOnly &&
                    request.includeNearbyDestinationPort == Staticrequest.includeNearbyDestinationPort && 
                    request.includeNearbyOriginPorts == Staticrequest.includeNearbyOriginPorts &&
                    request.originPort == Staticrequest.originPort && 
                    request.sacs == Staticrequest.sacs &&
                    request.searchDate == Staticrequest.searchDate && 
                    request.searchDateType == Staticrequest.searchDateType &&
                    request.weeksOut == Staticrequest.weeksOut)
                {
                    resul = true;
                }
              
                //if (resul)
                //{
                //    var response = Staticresponse;

                //    List<ItinerarioResponseDTO> ListaManualDTO = new List<ItinerarioResponseDTO>();
                //    BL_Reserva BL = new BL_Reserva(_configuration);
                //    var ItinerarioManual = BL.BL_ListaItinerarioManual();
                //    List<BE_ItinerarioManual> listaManual = (List<BE_ItinerarioManual>)ItinerarioManual;
                //    listaManual = listaManual.Where(x => x.originUnloc == request.originPort && x.destinationUnloc == request.destinationPort).ToList();
                //    if (request.searchDateType == "ByDepartureDate")
                //    {
                //        listaManual = listaManual.Where(x => x.originDepartureDate == request.searchDate.ToString()).ToList();
                //    }
                //    else
                //    {
                //        listaManual = listaManual.Where(x => x.destinationArrivalDate == request.searchDate.ToString()).ToList();
                //    }

                //    var listaAgrupada = (from item in listaManual
                //                         group item by new { item.scac, item.carrierName,
                //                          item.vesselName,item.voyageNumber,
                //                          item.imoNumber,item.originUnloc,item.originCountry,
                //                          item.originCityName,item.originSubdivision,
                //                          item.destinationUnloc,item.destinationCountry,
                //                          item.destinationCityName,item.legs,
                //                          item.originDepartureDate,item.destinationArrivalDate,
                //                          item.terminalCutoff
                //                         } into g
                //                         select new BE_ItinerarioManual()
                //                         {
                //                             scac = g.Key.scac,
                //                             carrierName = g.Key.carrierName,
                //                             vesselName = g.Key.vesselName,
                //                             voyageNumber = g.Key.voyageNumber,
                //                             imoNumber = g.Key.imoNumber,
                //                             originUnloc = g.Key.originUnloc,
                //                             originCountry = g.Key.originCountry,
                //                             originCityName = g.Key.originCityName,
                //                             originSubdivision = g.Key.originSubdivision,
                //                             destinationUnloc = g.Key.destinationUnloc,
                //                             destinationCityName = g.Key.destinationCityName,
                //                             destinationCountry = g.Key.destinationCountry,
                //                             originDepartureDate = g.Key.originDepartureDate,
                //                             destinationArrivalDate = g.Key.destinationArrivalDate,
                //                             terminalCutoff = g.Key.terminalCutoff,
                //                             legs = g.Key.legs,
                //                         }).ToList();
                //    foreach (var a in listaAgrupada)
                //    {
                //        ItinerarioResponseDTO ent = new ItinerarioResponseDTO();
                //        LegDTO legs = new LegDTO();

                //        ent.scac = a.scac;
                //        ent.carrierName = a.carrierName;
                //        ent.vesselName = a.vesselName;
                //        ent.voyageNumber = a.voyageNumber;
                //        ent.imoNumber = a.imoNumber;
                //        ent.originUnloc = a.originUnloc;
                //        ent.originCountry = a.originCountry;
                //        ent.originCityName = a.originCityName;
                //        ent.originSubdivision = a.originSubdivision;
                //        ent.destinationUnloc = a.destinationUnloc;
                //        ent.destinationCountry = a.destinationCountry;
                //        ent.destinationCityName = a.destinationCityName;

                //        ent.originDepartureDate = Convert.ToDateTime(a.originDepartureDate);
                //        ent.destinationArrivalDate = Convert.ToDateTime(a.destinationArrivalDate);
                //        ent.terminalCutoff = Convert.ToDateTime(a.terminalCutoff);
                //        ent.legs = new List<LegDTO>();

                //        foreach (var detalle in listaManual.Where(x =>
                //             x.scac == a.scac && x.carrierName == a.carrierName &&
                //             x.vesselName == a.vesselName && x.voyageNumber == a.voyageNumber &&
                //             x.imoNumber == a.imoNumber && x.originUnloc == a.originUnloc &&
                //             x.originCountry == a.originCountry && x.originCityName == a.originCityName &&
                //             x.originSubdivision == a.originSubdivision && x.destinationUnloc == a.destinationUnloc &&
                //             x.destinationCountry == a.destinationCountry && x.destinationCityName == a.destinationCityName &&
                //             x.originDepartureDate == a.originDepartureDate && x.destinationArrivalDate == a.destinationArrivalDate &&
                //             x.terminalCutoff == a.terminalCutoff
                //            ))
                //        {
                //            bool IsNull = string.IsNullOrEmpty(detalle.legs);
                //            if (IsNull)
                //            {
                //                detalle.legs = "0";
                //            }
                //            if (detalle.legs == "1")
                //            {
                //                legs.sequence = Convert.ToInt32(detalle.sequence);
                //                legs.serviceName = detalle.serviceName;
                //                legs.transportName = detalle.transportName;
                //                legs.conveyanceNumber = detalle.conveyanceNumber;
                //                legs.departureCityName = detalle.departureCityName;
                //                legs.departureCountry = detalle.departureCountry;
                //                legs.departureDate = Convert.ToDateTime(detalle.departureDate);
                //                legs.arrivalCityName = detalle.arrivalCityName;
                //                legs.arrivalCountry = detalle.arrivalCountry;
                //                legs.arrivalDate = Convert.ToDateTime(detalle.arrivalDate);
                //                legs.transitDuration = Convert.ToInt32(detalle.transitDuration);
                //            }
                //            else
                //            {
                //                legs.sequence = 1;
                //                legs.serviceName = ent.serviceName;
                //                legs.transportName = ent.vesselName;
                //                legs.conveyanceNumber = ent.voyageNumber;
                //                legs.departureCityName = ent.originCityName;
                //                legs.departureCountry = ent.originCountry;
                //                legs.departureDate = Convert.ToDateTime(ent.originDepartureDate);
                //                legs.arrivalCityName = ent.destinationCityName;
                //                legs.arrivalCountry = ent.destinationCountry;
                //                legs.arrivalDate = Convert.ToDateTime(ent.destinationArrivalDate);
                //                legs.transitDuration = Convert.ToInt32(ent.totalDuration);
                //            }
                //            ent.legs.Add(legs);

                //        }

                //        ListaManualDTO.Add(ent);
                //    }

                //    if (ListaManualDTO.Count > 0)
                //    {
                //        response.AddRange(ListaManualDTO);
                //    }

                //    model.ListaItinerarios = response;
                //    model.OriginCode = model.OriginCode;
                //    model.DestinationCode = model.DestinationCode;
                //    model.Origin = model.OriginCode;
                //    model.Destination = model.DestinationCode;
                //    HttpContext.Session.SetObject("ListaItinerarios", model);
                //    htmlGrid = crearGridShow(response, model.OriginCode, model.DestinationCode);
                //}
                //else
                //{
                     Staticrequest = request;
                    InttraService service = new InttraService();
                    List<ItinerarioResponseDTO> response = new List<ItinerarioResponseDTO>();
                    string CodigoVariable = "F0001";
                    var datoVariable = bl_Variable.BL_ListaVariables(CodigoVariable);
                    var Variable = (List<BE_Variables>)datoVariable;
                    if(Variable[0].Activado == 1)
                    {
                    ////  Lista de inttra
                    response = await service.ListarItinerarios(request);
                    }
                    List<ItinerarioResponseDTO> ListaManualDTO = new List<ItinerarioResponseDTO>();
                    BL_Reserva BL = new BL_Reserva(_configuration);
                    var ItinerarioManual = BL.BL_ListaItinerarioManual();
                    List<BE_ItinerarioManual> listaManual = (List<BE_ItinerarioManual>)ItinerarioManual;
                    listaManual = listaManual.Where(x => x.originUnloc == request.originPort && x.destinationUnloc == request.destinationPort).ToList();
                    int dias = request.weeksOut * 7;
                    DateTime fechaFin = request.searchDate.AddDays(dias);

                    if (request.searchDateType == "ByDepartureDate")
                    {
                        listaManual = listaManual.Where(x => Convert.ToDateTime(x.originDepartureDate) >= request.searchDate && Convert.ToDateTime(x.originDepartureDate) <= fechaFin ).ToList();
                    }
                    else{
                        listaManual = listaManual.Where(x => Convert.ToDateTime(x.destinationArrivalDate) >= request.searchDate && Convert.ToDateTime(x.destinationArrivalDate) <= fechaFin ).ToList();
                    }

                    var listaCabecera = new List<BE_ItinerarioManual>();

                    var listaAgrupada = (from item in listaManual
                                         group item by new
                                         {
                                             item.scac,
                                             item.carrierName,
                                             item.vesselName,
                                             item.voyageNumber,
                                             item.imoNumber,
                                             item.originUnloc,
                                             item.originCountry,
                                             item.originCityName,
                                             item.originSubdivision,
                                             item.destinationUnloc,
                                             item.destinationCountry,
                                             item.destinationCityName,
                                             item.legs,
                                             item.originDepartureDate,
                                             item.destinationArrivalDate,
                                             item.terminalCutoff,
                                             item.totalDuration
                                         } into g
                                         select new BE_ItinerarioManual()
                                         {
                                             scac = g.Key.scac,
                                             carrierName = g.Key.carrierName,
                                             vesselName = g.Key.vesselName,
                                             voyageNumber = g.Key.voyageNumber,
                                             imoNumber = g.Key.imoNumber,
                                             originUnloc = g.Key.originUnloc,
                                             originCountry = g.Key.originCountry,
                                             originCityName = g.Key.originCityName,
                                             originSubdivision = g.Key.originSubdivision,
                                             destinationUnloc = g.Key.destinationUnloc,
                                             destinationCityName = g.Key.destinationCityName,
                                             destinationCountry = g.Key.destinationCountry,
                                             originDepartureDate = g.Key.originDepartureDate,
                                             destinationArrivalDate = g.Key.destinationArrivalDate,
                                             terminalCutoff = g.Key.terminalCutoff,
                                             totalDuration = g.Key.totalDuration,
                                             legs = g.Key.legs,
                                         }).ToList();
                    foreach (var a in listaAgrupada)
                    {
                        ItinerarioResponseDTO ent = new ItinerarioResponseDTO();
                        LegDTO legs = new LegDTO();
                        ent.scac = a.scac;
                        ent.carrierName = a.carrierName;
                        ent.vesselName = a.vesselName;
                        ent.voyageNumber = a.voyageNumber;
                        ent.imoNumber = a.imoNumber;
                        ent.originUnloc = a.originUnloc;
                        ent.originCountry = a.originCountry;
                        ent.originCityName = a.originCityName;
                        ent.originSubdivision = a.originSubdivision;
                        ent.destinationUnloc = a.destinationUnloc;
                        ent.destinationCountry = a.destinationCountry;
                        ent.destinationCityName = a.destinationCityName;

                        ent.originDepartureDate = Convert.ToDateTime(a.originDepartureDate);
                        ent.destinationArrivalDate = Convert.ToDateTime(a.destinationArrivalDate);
                        ent.terminalCutoff = Convert.ToDateTime(a.terminalCutoff);
                        ent.totalDuration = Convert.ToInt32(a.totalDuration);
                        ent.legs  = new List<LegDTO>();

                        foreach(var detalle in listaManual.Where(x=> 
                            x.scac == a.scac && x.carrierName==a.carrierName &&
                            x.vesselName == a.vesselName && x.voyageNumber == a.voyageNumber &&
                            x.imoNumber == a.imoNumber && x.originUnloc == a.originUnloc &&
                            x.originCountry == a.originCountry && x.originCityName == a.originCityName &&
                            x.originSubdivision == a.originSubdivision && x.destinationUnloc == a.destinationUnloc &&
                            x.destinationCountry == a.destinationCountry && x.destinationCityName == a.destinationCityName &&
                            x.originDepartureDate == a.originDepartureDate && x.destinationArrivalDate == a.destinationArrivalDate &&
                            x.terminalCutoff == a.terminalCutoff 
                            ) )
                        {
                            legs = new LegDTO();
                            bool IsNull = string.IsNullOrEmpty(detalle.legs);
                            if (IsNull)
                            {
                                detalle.legs = "0";
                            }
                            if (detalle.legs == "1")
                            {
                                ent.transbordo = "SI";
                                ent.scheduleType = "transshipment";
                                legs.sequence = Convert.ToInt32(detalle.sequence);
                                legs.serviceName = detalle.serviceName;
                                legs.transportName = detalle.transportName;
                                legs.conveyanceNumber = detalle.conveyanceNumber;
                                legs.departureCityName = detalle.departureCityName;
                                legs.departureCountry = detalle.departureCountry;
                                legs.departureDate = Convert.ToDateTime(detalle.departureDate);
                                legs.arrivalCityName = detalle.arrivalCityName;
                                legs.arrivalCountry = detalle.arrivalCountry;
                                legs.arrivalDate = Convert.ToDateTime(detalle.arrivalDate);
                                legs.transitDuration = Convert.ToInt32(detalle.transitDuration);
                            }else
                            {
                                legs.sequence = 1;
                                legs.serviceName = ent.serviceName;
                                legs.transportName = ent.vesselName;
                                legs.conveyanceNumber = ent.voyageNumber;
                                legs.departureCityName = ent.originCityName;
                                legs.departureCountry = ent.originCountry;
                                legs.departureDate = Convert.ToDateTime(ent.originDepartureDate);
                                legs.arrivalCityName = ent.destinationCityName;
                                legs.arrivalCountry = ent.destinationCountry;
                                legs.arrivalDate = Convert.ToDateTime(ent.destinationArrivalDate);
                                legs.transitDuration = Convert.ToInt32(ent.totalDuration);
                            }
                            ent.legs.Add(legs);

                        }
                        ListaManualDTO.Add(ent);
                    }

                   if(ListaManualDTO.Count > 0)
                    {
                        response.AddRange(ListaManualDTO);
                    }

                    model.ListaItinerarios = response;
                    model.OriginCode = model.OriginCode;
                    model.DestinationCode = model.DestinationCode;
                    model.Origin = model.OriginCode;
                    model.Destination = model.DestinationCode;
                    //StaticModel = model;
                    Staticresponse = response;
                    HttpContext.Session.SetObject("ListaItinerarios", model);
                    htmlGrid = crearGridShow(response, model.OriginCode, model.DestinationCode);

                //}

                //model.
                //model.ListaItinerarios = response;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Itinerario/Lista2");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShow(List<ItinerarioResponseDTO> lista,string OriginCode,string DestinationCode)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");


            #region html

            //campana.Where(x => x.)
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-md-12\">");
            htmlShow.Append("<div class=\"card\">");
            htmlShow.Append("<div class=\"card-body\">");

            if(lista.Count == 0)
            {
                htmlShow.Append("<p>No se encontraron registros</p>");
            }
            else
            {

                htmlShow.Append("<table class=\"table table-bordered table-hover\" >");

                htmlShow.Append("<thead>");
                htmlShow.Append("<tr>");
                htmlShow.Append("<th class=\"text-center\"> Dia de salida (ETD)");
                htmlShow.Append("<i class=\"fas fa-exclamation-circle float-right\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"La información mostrada es recibida de la Naviera, por favor confirmar con su Customer Service.\"></i> ");
                htmlShow.Append("</th>");
                htmlShow.Append("<th class=\"text-center\">Naviera</th>");
                htmlShow.Append("<th class=\"text-center\">TT</th>");
                htmlShow.Append("<th class=\"text-center\">Transbordo</th>");
                htmlShow.Append("<th class=\"text-center\">Itinerarios</th>");
                htmlShow.Append("</tr>");
                htmlShow.Append("</thead>");

                htmlShow.Append("<tbody>");


                foreach (var lis in lista.GroupBy(a => new { a.diaSalida, a.numeroDiaSemana, a.scac, a.carrierName, a.scheduleType })
                    .OrderBy(a => a.Key.numeroDiaSemana).ThenBy(a => a.Key.carrierName))
                {
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td class=\"text-center\">" + lis.Key.diaSalida  + "</td>");
                    htmlShow.Append("<td class=\"text-center\">" + lis.Key.carrierName + "</td>");
                    htmlShow.Append("<td class=\"text-center\">");
                    //var ListarPuerto = new OdooService(_configuration).ListarPuerto();
                    //var Origin = ListarPuerto.Where(x => x.Codigo == OriginCode).ToList();
                    //var Destination = ListarPuerto.Where(x => x.Codigo == DestinationCode).ToList();
                    int cant = lis.Select(x => x.totalDuration).Distinct().OrderBy(x => x).Count() ;
                    int cant2 = 0;
                    foreach (var item in lis.Select(x => x.totalDuration).Distinct().OrderBy(x => x))
                    {
                        cant2++;
                        //VerItinerario(string scac, int ? tt, string dia, string transbordo, string OriginCode, string DestinationCode)
                        //htmlShow.AppendFormat("<a href=\"../Itinerario/VerItinerario?scac="+ lis.Key.scac + "&tt="+ item + "&dia=" + lis.Key.diaSalida + "&transbordo=" + 
                        //    lis.Key.scheduleType + "&OriginCode=" + Origin[0].Nombre + "&DestinationCode=" + Destination[0].Nombre + "\" '>" + item + "</a>");
                        htmlShow.AppendFormat("<a href=\"#\" onclick=\"VerItinerario(" + "'" + lis.Key.scac + "'" + "," + item + "," + "'" + lis.Key.diaSalida + "'" + "," + "'" + lis.Key.scheduleType + "'" +
                            "," + "'" + OriginCode + "'" + "," + "'" + DestinationCode + "'" + "   );return false;\">"+ item +"</a> ");
                        //Origin[0].Nombre
                        //Destination[0].Nombre
                        if(cant == cant2)
                        {
                            htmlShow.Append("&nbsp;&nbsp; ");
                        }
                        else
                        {
                            htmlShow.Append("&nbsp;&nbsp; / &nbsp;&nbsp;");
                        }

                    }
                    htmlShow.Append("</td>");
                    htmlShow.Append("<td class=\"text-center\">");
                    if (lis.Key.scheduleType == "transshipment")
                    {
                        htmlShow.Append("<span> Si </span>");
                    }
                    else
                    {
                        htmlShow.Append("<span> No </span>");

                    }
                    htmlShow.Append("</td>");
                    htmlShow.Append("<td class=\"text-center\">");
                    //htmlShow.AppendFormat("<a href=\"../Itinerario/VerItinerario?scac= " + lis.Key.scac + "&tt=" + null + "&dia=" + lis.Key.diaSalida + "&transbordo=" +
                    //        lis.Key.scheduleType + "&OriginCode=" + Origin[0].Nombre + "&DestinationCode=" + Destination[0].Nombre + "\" '>Ver Itinerarios</a>");
                    htmlShow.AppendFormat("<a href=\"#\" onclick=\"VerItinerario("+ "'"+ lis.Key.scac + "'" + "," + 0 + "," + "'" + lis.Key.diaSalida + "'" + "," + "'" + lis.Key.scheduleType + "'" +
                        "," + "'" + OriginCode + "'" + "," + "'" + DestinationCode + "'" + "   );return false;\">Ver Itinerarios</a>");


                    //htmlShow.AppendFormat("<a href=\"#\" onclick='VerItinerario('" + lis.Key.scac + "',{1},'{2}','{3}','{4}','{5}');'> Ver Itinerarios</a>",  null, lis.Key.diaSalida, lis.Key.scheduleType, OriginCode, DestinationCode);

                    htmlShow.Append("</td>");
                    htmlShow.Append("<tr>");

                }
                htmlShow.Append("</tbody>");
                htmlShow.Append("</table>");
            }
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");

            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        public string crearGridShowVerItinerario(ItinerarioViewModel itinerarioViewModel)
        {
            StringBuilder htmlShow = new StringBuilder();
            //var listaContratos = new OdooService(_configuration).ListarContratos(User.Identity.Name);

            #region html

            if (itinerarioViewModel.ListaItinerarios.Count == 0)
            {
                htmlShow.Append("<p>No se encontraron registros</p>");
            }
            else
            {

                foreach (var item in itinerarioViewModel.ListaItinerarios)
                {
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");
                    htmlShow.Append("<form>");
                    htmlShow.Append("<table class=\"table table-hover\">");
                    htmlShow.Append("<tbody>");
                    //FILA 1 - INICIO
                    htmlShow.Append("<tr>");
                    //FILA 1 - COLUMNA 1
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\"><h3>"+ item.carrierName +"</h3></div>");
                    htmlShow.Append("<div class=\"row\">carrier</div>");
                    //htmlShow.Append($"<div class=\"row\">Transbordo:{item.transbordo}</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Transbordo:</div>");
                    htmlShow.Append($"<div class=\"col-6\"><b>{(item.scheduleType=="transshipment"?"SI":"NO")}</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("</td>");
                    //FILA 1 - COLUMNA 2
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Contract:</div>");
                    htmlShow.Append($"<div class=\"col-6\"><b>{(item.IdContrato>0?"SI":"NO")}</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Expiration:</div>");
                    htmlShow.Append($"<div class=\"col-6\"><b>{(!string.IsNullOrEmpty(item.FechaContratoExpiracion)?item.FechaContratoExpiracion:"")}</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">TT:</div>");
                    htmlShow.Append("<div class=\"col-6\"><b>"+ item.totalDuration +"/ dias</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");
                    //FILA 1 - COLUMNA 3
                    htmlShow.Append("<td style=\"border-top:none;\" style=\"padding-left:10px\" >");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">");
                    htmlShow.Append("<b> ETD: "+ item.originDepartureDateString+ "</b>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">(" + item.diaSalidaAbreviado +")</div>");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">" + item.originSubdivision +","+item.originCountry + "</div>");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">");
                    htmlShow.Append("</td>");
                    //FILA 1 - COLUMNA 4
                    htmlShow.Append("<td style=\"border-top:none;\"><i class=\"fas fa-chevron-right fa-3x\"></i></td>");
                    //FILA 1 - COLUMNA 5
                    htmlShow.Append("<td style=\"border-top:none;\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<b> ETA: " + item.destinationArrivalDateString + "</b>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">(" + item.diaLlegadaAbreviado + ")</div>");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">" + item.destinationCityName + "," + item.destinationCountry + "</div>");
                    htmlShow.Append("<div class=\"row\" style=\"padding-left:10px\">");
                    htmlShow.Append("</td>");
                    htmlShow.Append("</tr>");
                    //FILA 1 - FIN

                    //FILA 2 - INICIO
                    htmlShow.Append("<tr>");
                    //FILA 2 - COLUMNA 1
                    htmlShow.Append("<td style=\"border-top:none;\">");
                    htmlShow.AppendFormat("<a href=\"#\" onclick=\"VerDetalle('{0}','{1}','{2}','{3}','{4}');return false;\">Ver Detalles</a>",
                                                item.scac, item.diaSalida, item.totalDuration, itinerarioViewModel.Origin, itinerarioViewModel.Destination);
                    htmlShow.Append("</td>");
                    ////FILA 2 - COLUMNA 2
                    //htmlShow.Append("<td style=\"border-top:none;\" colspan=\"3\">");
                    //htmlShow.AppendFormat("<a  style=\"color:white\" class=\"btn btn-primary col-md-3 float-right\" " +
                    //    "onclick=\"Volver();return false;\">Volver</a>");
                    //htmlShow.Append("</td>");

                    //FILA 2 - COLUMNA 3class="col-md-8 col-md-offset-2"
                    htmlShow.Append("<td style=\"border-top:none;\" colspan='4'>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.AppendFormat("<a  style=\"color:white\" class=\"btn btn-primary float-right\" onclick=\"SolicitarBooking('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},'{12}','{13}','{14}','{15}','{16}');return false;\">Solicitar Booking</a>",
                                                item.scac, item.originUnloc, item.destinationUnloc, item.originDepartureDateStringUniversal
                                                , item.destinationArrivalDateStringUniversal, item.vesselName, item.voyageNumber, itinerarioViewModel.Origin,
                                                itinerarioViewModel.Destination, itinerarioViewModel.NroContrato,itinerarioViewModel.idOrigin, itinerarioViewModel.idDestination
                                                ,itinerarioViewModel.CodigoPaisOrigen, itinerarioViewModel.NombrePaisOrigen
                                                , itinerarioViewModel.CodigoPaisDestino, itinerarioViewModel.NombrePaisDestino,item.imoNumber);
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");
                    htmlShow.Append("</tr>");
                    //FILA 2 - FIN
                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</form>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                }

            }

                #endregion

            return htmlShow.ToString();

        }

        public async Task<IActionResult> ListarItinerarioToBooking(string sacs, string originPort, string destinationPort)
        {
            try
            {
                ItinerarioRequestDTO request = new ItinerarioRequestDTO();
                request.originPort = originPort;
                request.destinationPort = destinationPort;
                request.sacs = sacs;
                request.directOnly = false;
                request.includeNearbyDestinationPort = false;
                request.includeNearbyOriginPorts = false;
                request.searchDate = DateTime.Now;
                request.searchDateType = "ByDepartureDate";
                request.weeksOut = 6;

                InttraService service = new InttraService();
                List<ItinerarioResponseDTO> response = new List<ItinerarioResponseDTO>();
                //// Lista Itinerario inttra
                string CodigoVariable = "F0001";
                var datoVariable = bl_Variable.BL_ListaVariables(CodigoVariable);
                var Variable = (List<BE_Variables>)datoVariable;
                if (Variable[0].Activado == 1)
                {
                    ////  Lista de inttra
                    response = await service.ListarItinerarios(request);
                }
                //Itinerarios Manuales
                List<ItinerarioResponseDTO> ListaManualDTO = new List<ItinerarioResponseDTO>();
                BL_Reserva BL = new BL_Reserva(_configuration);
                var ItinerarioManual = BL.BL_ListaItinerarioManual();
                List<BE_ItinerarioManual> listaManual = (List<BE_ItinerarioManual>)ItinerarioManual;
                listaManual = listaManual.Where(x => x.originUnloc.ToUpper() == request.originPort.ToUpper()
                                                  && x.destinationUnloc.ToUpper() == request.destinationPort.ToUpper()
                                                  && x.scac.ToUpper() == request.sacs.ToUpper()).ToList();
                
                string Date = DateTime.Now.ToString("MM-dd-yyyy");
                DateTime FechaHoy = Convert.ToDateTime(Date);

                //listaManual = listaManual.OrderBy(x => x.)

                //listaManual = listaManual.Where(x => Convert.ToDateTime(x.originDepartureDate) >= FechaHoy).OrderBy(x=>x.originDepartureDate).ToList();

                //if (listaManual.Count() > 0)
                //{
                //    var prueba = listaManual[0].originDepartureDate.Substring(0, 10).Trim().Split("/");
                //    string dia = prueba[1].Length == 1 ? '0' + prueba[1] : prueba[1];
                //    string mes = prueba[0].Length == 1 ? '0' + prueba[0] : prueba[0];
                //    string anio = prueba[2];
                //    var fecha = DateTime.Now.Date;

                //    //var fechaActual = Convert.ToDateTime(dia+'/'+mes+'/'+anio);
                //    //listaManual = listaManual.Where(x => ConvertMMDDYYYY_TO_DDMMYYYY(x.originDepartureDate.Substring(0, 10).Trim().Split("/")) > DateTime.Now.Date).ToList();
                //    listaManual = listaManual.Where(x => Convert.ToDateTime(x.originDepartureDate) > DateTime.Now.Date).ToList();
                //}
                foreach (var a in listaManual)
                {
                    ItinerarioResponseDTO ent = new ItinerarioResponseDTO();
                    ent.idItinerarioManual = a.IdItinerarioManual;
                    ent.scac = a.scac;
                    ent.carrierName = a.carrierName;
                    ent.vesselName = a.vesselName;
                    ent.voyageNumber = a.voyageNumber;
                    ent.imoNumber = a.imoNumber;
                    ent.originUnloc = a.originUnloc;
                    ent.originCountry = a.originCountry;
                    ent.originCityName = a.originCityName;
                    ent.originSubdivision = a.originSubdivision;
                    ent.destinationUnloc = a.destinationUnloc;
                    ent.destinationCountry = a.destinationCountry;
                    ent.destinationCityName = a.destinationCityName;
                    ent.totalDuration = Convert.ToInt32(a.totalDuration);

                    ent.originDepartureDate = Convert.ToDateTime(a.originDepartureDate);
                    ent.destinationArrivalDate = Convert.ToDateTime(a.destinationArrivalDate);
                    ent.terminalCutoff = Convert.ToDateTime(a.terminalCutoff);
                    ListaManualDTO.Add(ent);
                }

                //Junta la data de Inttra con la manual
                if (ListaManualDTO.Any())
                {
                    response.AddRange(ListaManualDTO);
                }
                return View(response);

            }
            catch (Exception ex)
            {

                return View();
            }
            //Itinerarios de Inttra

        }
    }
}
