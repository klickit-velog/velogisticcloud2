﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.Models.General;
using VELogisticCloud.Servicio.Business.General;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class EstadoServiciosController : Controller
    {
        private readonly ILogger<EstadoServiciosController> _logger;
        private IConfiguration _configuration;
        private BL_Variable bl_Variable;

        //Proyecciones

        public EstadoServiciosController(ILogger<EstadoServiciosController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            bl_Variable = new BL_Variable(_configuration);
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UpdateInttra(int estado)
        {
            await Task.Yield();
            try
            {
                string Codigo_Usuario = HttpContext.User.Identity.Name;
                string CodigoVariable = "F0001";
                bl_Variable.BL_UpdateVariable(estado, Codigo_Usuario, CodigoVariable);
                return Ok(1);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ListarBooking");
                return Ok(0);
            }
        }

        [HttpPost]
        public async Task<ActionResult> ObtenerEstadoInttra()
        {
            await Task.Yield();
            List<BE_Variables> Variable = new List<BE_Variables>();
            try
            {
                string CodigoVariable = "F0001";
                var datoVariable = bl_Variable.BL_ListaVariables(CodigoVariable);
                Variable = (List<BE_Variables>)datoVariable;
                return Ok(Variable);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ListarBooking");
                return Ok(Variable);
            }
        }
    }
}
