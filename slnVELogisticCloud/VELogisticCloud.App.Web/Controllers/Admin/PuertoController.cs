﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class PuertoController : Controller
    {
        private readonly IContenedorTrabajo _contenedorTrabajo;
        public PuertoController(IContenedorTrabajo contenedorTrabajo)
        {
            _contenedorTrabajo = contenedorTrabajo;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Puerto entidad)
        {
            if (ModelState.IsValid)
            {
                _contenedorTrabajo.Puerto.Add(entidad);
                _contenedorTrabajo.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(entidad);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Puerto puerto = _contenedorTrabajo.Puerto.Get(id);
            if (puerto == null)
            {
                return NotFound();
            }

            return View(puerto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Puerto entidad)
        {
            if (ModelState.IsValid)
            {
                _contenedorTrabajo.Puerto.Update(entidad);
                _contenedorTrabajo.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(entidad);
        }



        #region llamadas API

        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _contenedorTrabajo.Puerto.GetAll() });
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var objFromDb = _contenedorTrabajo.Puerto.Get(id);
            if (objFromDb == null)
            {
                return Json(new { success = false, message = "Error eliminando puerto" });
            }

            _contenedorTrabajo.Puerto.Remove(objFromDb);
            _contenedorTrabajo.Save();
            return Json(new { success = true, message = "Puerto borrado satisfactoriamente." });
        }

        #endregion
    }
}
