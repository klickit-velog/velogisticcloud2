﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class NavieraController : Controller
    {
        private readonly IContenedorNaviera _contenedorNaviera;
        public NavieraController(IContenedorNaviera contenedorNaviera)
        {
            _contenedorNaviera = contenedorNaviera;
        }
        public IActionResult Index()
        {
            return View();
        }    

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Naviera entidad)
        {
            if (ModelState.IsValid)
            {
                _contenedorNaviera.Naviera.Add(entidad);
                _contenedorNaviera.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(entidad);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Naviera naviera = _contenedorNaviera.Naviera.Get(id);
            if (naviera == null)
            {
                return NotFound();
            }

            return View(naviera);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Naviera entidad)
        {
            if (ModelState.IsValid)
            {
                _contenedorNaviera.Naviera.Update(entidad);
                _contenedorNaviera.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(entidad);
        }



        #region llamadas API

        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _contenedorNaviera.Naviera.GetAll() });
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var objFromDb = _contenedorNaviera.Naviera.Get(id);
            if (objFromDb == null)
            {
                return Json(new { success = false, message = "Error eliminando naviera" });
            }

            _contenedorNaviera.Naviera.Remove(objFromDb);
            _contenedorNaviera.Save();
            return Json(new { success = true, message = "Naviera borrado satisfactoriamente." });
        }

        #endregion
    }
}