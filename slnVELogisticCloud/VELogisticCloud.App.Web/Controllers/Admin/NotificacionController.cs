﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Models.Notificacion;
using VELogisticCloud.Servicio.Business.Notificacion;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class NotificacionController : Controller
    {
        private readonly ILogger _logger;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;
        BL_Notificacion BL;
        //public static List<Cliente> _listarClientes = new List<Cliente>();
        public NotificacionController(ILogger<NotificacionController> logger, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _configuration = configuration;
            _memoryCache = memoryCache;
            BL = new BL_Notificacion(configuration);
        }
        public IActionResult AsignarNotificacion()
        {
            return View();
        }


        [HttpPost]
        public ActionResult ListaCliente(string RazonSocial, string RUC, string Correo)
        {
            string htmlGrid = "";

            try
            {
                htmlGrid = crearGridDetalle(RazonSocial, RUC, Correo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error CustomerCliente/ListaCliente");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
        public string crearGridDetalle(string RazonSocial, string RUC, string Correo)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");
            string User = HttpContext.User.Identity.Name;

            //RazonSocial = RazonSocial == null ? 


            var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(name: RazonSocial, numeroDocumento: RUC, correo: Correo);
            //var listarClientes = new OdooService(_configuration).ListarClientes(0, 500, "", "admin@mail.com");

            #region html

            htmlShow.Append("<table class=\"table table-hover\" id=\"data\">");
            htmlShow.Append("<thead>");
            htmlShow.Append("<tr>");
            htmlShow.Append("<th scope=\"col\">#</th>");
            htmlShow.Append("<th scope=\"col\">Cliente</th>");
            htmlShow.Append("<th scope=\"col\">Numero Documento</th>");
            htmlShow.Append("<th scope=\"col\">Correo</th>");
            htmlShow.Append("<th  style=\"text-align:center\" scope=\"col\">Notificación 1</th>");
            htmlShow.Append("<th  style=\"text-align:center\" scope=\"col\">Notificación 2</th>");
            htmlShow.Append("<th  style=\"text-align:center\" scope=\"col\">Notificación 3</th>");
            htmlShow.Append("<th  style=\"text-align:center\" scope=\"col\">Notificación 4</th>");
            htmlShow.Append("<th  style=\"text-align:center\" scope=\"col\">Notificación 5</th>");
            htmlShow.Append("<th  style=\"text-align:center\" scope=\"col\" class=\"text-right\"></th>");
            htmlShow.Append("</tr>");
            htmlShow.Append("</thead>");
            htmlShow.Append("<tbody id=\"myTableBody\">");

            int num = 0;
            List<BE_NotificacionUsuario> Lista = (List<BE_NotificacionUsuario>)BL.ListaNotificacionUsuario();
            List<BE_NotificacionUsuario> lista2 = new List<BE_NotificacionUsuario>();

            foreach (var lis1 in listarClientes)
            {
                num++;
                lista2 = new List<BE_NotificacionUsuario>();
                lista2 = Lista.Where(x => x.IdUsuario == lis1.Id).ToList();
                htmlShow.Append("<tr>");
                htmlShow.Append("<td scope=\"row\"> " + num + " </td>");
                htmlShow.Append("<td> " + lis1.Nombre + " </td>");
                htmlShow.Append("<td> " + lis1.NumeroDocumento + " </td>");
                htmlShow.Append("<td> " + lis1.Correo + " </td>");
                // Formato  idCliente ,NombreCliente, IdNotificacion ,Notificacion

                if(lista2.Where(x=>x.IdTipoNotificacion == 1 && x.Activo == 1).Count() > 0)
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input checked id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',1,'Notificacion_1'" + ");\"  type=\"checkbox\"> </td>");
                }
                else
                {
                    htmlShow.Append("<td  style=\"text-align:center\"> <input id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',1,'Notificacion_1'" + ");\"  type=\"checkbox\"> </td>");
                }
                if (lista2.Where(x => x.IdTipoNotificacion == 2 && x.Activo == 1).Count() > 0)
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input checked id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',2,'Notificacion_2'" + ");\"  type=\"checkbox\"> </td>");
                }
                else
                {
                    htmlShow.Append("<td  style=\"text-align:center\"> <input id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',2,'Notificacion_2'" + ");\"  type=\"checkbox\"> </td>");
                }
                if (lista2.Where(x => x.IdTipoNotificacion == 3 && x.Activo == 1).Count() > 0)
                {
                    htmlShow.Append("<td  style=\"text-align:center\"> <input checked id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',3,'Notificacion_3'" + ");\"  type=\"checkbox\"> </td>");
                }
                else
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',3,'Notificacion_3'" + ");\"  type=\"checkbox\"> </td>");
                }

                if (lista2.Where(x => x.IdTipoNotificacion == 4 && x.Activo == 1).Count() > 0)
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input checked id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',4,'Notificacion_4'" + ");\"  type=\"checkbox\"> </td>");
                }
                else
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',4,'Notificacion_4'" + ");\"  type=\"checkbox\"> </td>");
                }

                if (lista2.Where(x => x.IdTipoNotificacion == 5 && x.Activo == 1).Count() > 0)
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input checked id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',5,'Notificacion_5'" + ");\"  type=\"checkbox\"> </td>");
                }
                else
                {
                    htmlShow.Append("<td style=\"text-align:center\"> <input id=\"chk_" + lis1.Id + "\"  onclick=\"handleClick('chk_" + lis1.Id + "'," + lis1.Id + ",'" + lis1.Nombre + "','" + lis1.Correo + "',5,'Notificacion_5'" + ");\"  type=\"checkbox\"> </td>");
                }
                //htmlShow.Append("<td> <a href = \"#\" onclick = \"ObtenerCliente(" + lis1.Id + "); return false;\"> Ingresar </a>  </td>");

                htmlShow.Append("</tr>");
            }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult InsertNotificacionUsuario(BE_NotificacionUsuario ent)
        {
            string htmlGrid = "";
            int existe = 0;
            try
            {
                List<BE_NotificacionUsuario> Lista = (List<BE_NotificacionUsuario>)BL.ListaNotificacionUsuario();
                existe = Lista.Where(x => x.IdUsuario == ent.IdUsuario && x.IdTipoNotificacion == ent.IdTipoNotificacion).Count();
                ent.UsuarioCreacion = HttpContext.User.Identity.Name;
                ent.UsuarioModificacion = HttpContext.User.Identity.Name;
                if (existe != 0)
                {
                    var update = BL.UpdateNotificacion(ent);
                }else
                {
                    var insert = BL.InsertNotificacion(ent);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error CustomerCliente/ListaCliente");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
    }
}