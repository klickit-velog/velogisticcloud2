﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models.Seguimiento;
using VELogisticCloud.Servicio.Business.Cotizacion;
using VELogisticCloud.Servicio.Business.Seguimiento;
using VELogisticCloud.Servicio.Middleware;
using oddo = VELogisticCloud.CrossCutting.OdooEntity;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.CrossCutting.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Model.Identity;
using Microsoft.AspNetCore.Identity;
using Nancy.Json;
using VELogisticCloud.CrossCutting.Mail;
using System.Text.Json;
using VELogisticCloud.Models.ApiAvanzza;
using static VELogisticCloud.Models.ApiAvanzza.Vehiculos;
using VELogisticCloud.CrossCutting.DTO;
using Microsoft.Extensions.Caching.Memory;
using VELogisticCloud.App.Web.Clases;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    [DisableRequestSizeLimit]
    public class CotizacionController : Controller
    {
        private readonly VECloudDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        private bool esCliente = false;
        private IConfiguration _configuration;
        private readonly INetMailSender netMailSender;
        private readonly IMemoryCache _memoryCache;

        public CotizacionController(VECloudDBContext context, ILogger<CotizacionController> logger,
            UserManager<ApplicationUser> userManager,IConfiguration configuration, INetMailSender _netMailSender, IMemoryCache memoryCache)
        {
            _context = context;
            _logger = logger;
            _userManager = userManager;
            _configuration = configuration;
            netMailSender = _netMailSender;
            this._memoryCache = memoryCache;
        }

        //Cotizacion
        public IActionResult Index()
        {
            ModelCombos model = new ModelCombos();
            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration,_memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre

            }));
            model.ListaNavieras = listaNavieras;

            return View(model);
        }
        public IActionResult CotizacionLogisticaAprobadas()
        {
            ModelCombos model = new ModelCombos();
            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre

            }));
            model.ListaNavieras = listaNavieras;

            return View(model);
        }
        public IActionResult MisSolicitudes()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;

            return View(model);
        }
        public IActionResult MisSolicitudesLogistica()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));
           
            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaNavieras = listaNavieras;
            model.ListaTipoContenedor = listaTipoContenedor;
            return View(model);
        }
        public IActionResult Create()
        {
            ModelCombos model = new ModelCombos();


            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
               
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;
            return View(model);
        }

        public async  Task<IActionResult> CotLogistica()
        {
            ModelCombos model = new ModelCombos();
            string correoUsuario = HttpContext.User.Identity.Name;

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaFiltroEtileno = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).GetTipoServicioOPL(TipoServicioOPL.FILTRO).ForEach(a => listaFiltroEtileno.Add(new SelectListItem()
            {
                Value = a.IdServicio.ToString(),
                Text = a.Nombre
            }));

            var listaTermoRegistro = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).GetTipoServicioOPL(TipoServicioOPL.TERMO).ForEach(a => listaTermoRegistro.Add(new SelectListItem()
            {
                Value = a.IdServicio.ToString(),
                Text = a.Nombre
            }));

            var usuario = "";
            var cliente = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: validar usuario sea customer.
            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                var NomCliente = datos[0].Value;
                cliente = Convert.ToString(idCliente);
                usuario = HttpContext.User.Identity.Name;
            }
            else
            {
                cliente = HttpContext.User.Identity.Name;
                usuario = HttpContext.User.Identity.Name;
            }
            var listaPacking = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarPacking(cliente).ForEach(a => listaPacking.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));
            
            model.ListaTermoRegistro = listaTermoRegistro;
            model.ListaFiltroEtileno = listaFiltroEtileno;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaPacking = listaPacking;

            return View(model);
        }

        public IActionResult CotTransporte()
        {
            return View();
        
        }

        public IActionResult ListaLogistica()
        {
            return View();
        }

        public IActionResult DetalleLogistica()
        {
            return View();
        }

        public IActionResult VerDetalleMaritimo(string id)
        {
             return View(); 
        }

        public IActionResult VerDetalleLogistica(string id)
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> ListaVehiculos()
        {
            string nombre = "";
            try
            {
                RestApiAvanzza api = new RestApiAvanzza(_configuration);

                var response = await api.GetVehiculos();
                var vehiculos = JsonSerializer.Deserialize<Rootobject>(response.message);
                var lista = vehiculos.data;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/ListaVehiculos");
            }
            return Ok(nombre);
        }

        [HttpPost]
        public async Task<ActionResult> IniciarDatos()
        {
            string nombre = "";
            object obj;
            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);

                var datos = await _userManager.GetClaimsAsync(user);
                if(datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correo: idCliente).FirstOrDefault();

                    nombre = Convert.ToString(NomCliente);
                    nombre = nombre + ',' + listarClientes.NumeroDocumento;
                }
                else
                {
                    nombre = HttpContext.User.Identity.Name;
                    var Cantidad = new OdooService(_configuration, _memoryCache).ListarClientes(correo: nombre).Count;

                    if (Cantidad > 0)
                    {
                        var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correo: nombre).FirstOrDefault();
                        nombre = nombre + ',' + listarClientes.NumeroDocumento;
                    }else
                    {
                        nombre = HttpContext.User.Identity.Name;
                        nombre = nombre + ',' + "Customer";
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/IniciarDatos");
            }
            return Ok(nombre);
        }

        #region Maritimo
        [HttpPost]
        public async Task<ActionResult> Confirmar_Rechazar_Maritimo(int id, int estado)
        {
            string cod = "";
            int idCotizacionOdoo=0;
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);                
                var res = BL.BL_Confirmar_RechazarCotizacionMaritimo(id, estado);
                
                cod = (string)res;
                idCotizacionOdoo = Convert.ToInt32(cod);

                if (estado == 3)
                {
                    BL_Cotizacion BL2 = new BL_Cotizacion(_configuration);
                    var res2 = BL2.BL_DetalleMaritimo(id);
                    List<DetalleMaritimo> lsta = (List<DetalleMaritimo>)res2;
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(lsta[0].Cliente);
                    
                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroCotizacion = lsta[0].Id_Cotizacion_Maritimo.ToString()
                    };

                    await netMailSender.sendMailAsync(lsta[0].Cliente, emailTemplate, TipoEmail.COTIZACION_MARITIMA_3_ACEPTADO);
                    var resultado = new OdooService(_configuration, _memoryCache).ConfirmarCotizacionDetalle(idCotizacionOdoo);
                }
                  else if(estado == 4)
                {
                    var resultado = new OdooService(_configuration, _memoryCache).CancelarCotizacionDetalle(idCotizacionOdoo);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/Confirmar_Rechazar_Maritimo idCotizacionOdoo: {idCotizacionOdoo}", idCotizacionOdoo);
            }
            return Ok(cod);
        }
        [HttpPost]
        public ActionResult DetalleMaritimo(int id)
        {
            //ParametrosBusqueda param = new ParametrosBusqueda();
            //param.NroContenedor = "";
            //param.CodigoBL = "";
            //param.NroDAM = "";
            //param.NroViaje = "";
            //param.NroBooking = "";
            //param.Commodity = "";
            //param.IdOperacionLogistica = id;

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_DetalleMaritimo(id);
                List<DetalleMaritimo> lsta = (List<DetalleMaritimo>)res;
                int cantPuertoDescarga = lsta.Select(x => x.Puerto_Descarga).Distinct().Count();
                int cantPuertoCarga = lsta.Select(x => x.Puerto_Embarque).Distinct().Count();
                DetalleMaritimo detalle = (DetalleMaritimo)lsta[0];
                if (cantPuertoCarga > 1)
                {
                    detalle.Puerto_EmbarqueCabecera = "Varios Origenes";
                }
                if (cantPuertoDescarga > 1)
                {
                    detalle.Puerto_DescargaCabecera = "Varios Destinos";
                }
                var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correo: detalle.Codigo_Usuario).FirstOrDefault();
                detalle.Ruc = listarClientes.NumeroDocumento;
                return Ok(detalle);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/DetalleMaritimo");
                return Ok("");
            }
        }

        [HttpPost]
        public ActionResult TablaDetalleMaritimo(int id)
        {
            string htmlGrid = "";

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_DetalleMaritimo(id);
                htmlGrid = crearGridDetalle(res);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/TablaDetalleMaritimo");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
        public string crearGridDetalle(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<DetalleMaritimo> lista = (List<DetalleMaritimo>)list;


            #region html
            htmlShow.Append("<table class=\"table table-bordered table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
                    htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">REGIÓN</th>");
                    htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">LINEA</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POL</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POD</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">COMMODITY</th>");
                    htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">NETO</th>");
                    htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">REBATE</th>");
                    htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">FLETE B/L</th>");
                    htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">TT</th>");
                    htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">Válido Desde</th>");
                    htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">Válido Hasta</th>");

            htmlShow.Append("</tr>");

                    foreach (var lis1 in lista)
                    {
                        htmlShow.Append("<tr>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Region + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Linea_Naviera + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Puerto_Embarque + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Puerto_Descarga + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Commodity + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + String.Format("{0:n0}", lis1.NETO) + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + String.Format("{0:n0}", lis1.REBATE) + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + String.Format("{0:n0}", lis1.FLETE) + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.TT + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Desde + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Hasta + "</td>");

                htmlShow.Append("</tr>");
                    }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public async Task<ActionResult> InsertMaritimo()
        {
            string cod_mar = "";
            ModelCombos model = new ModelCombos();
            var entJson = Request.Form["ent"];
            var contenedorJson = Request.Form["contenedor"];
            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<BE_Det_Maritimo> contenedor = jss.Deserialize<List<BE_Det_Maritimo>>(contenedorJson);
            BE_Maritimo ent = jss.Deserialize<BE_Maritimo>(entJson);
            string NombreCliente = "";
            Resultado resultado = new Resultado();

            try
            {
                //validar contenedor
                var contenedorFiltro = contenedor.Where(x => x.Codigo_Linea_Naviera == "0").ToList();

                if(contenedorFiltro.Count > 0)
                {
                    foreach(var a in contenedorFiltro)
                    {
                       var data = contenedor.Where(x =>   x.Codigo_Linea_Naviera != "0" && 
                                                x.Codigo_Puerto_Descarga == a.Codigo_Puerto_Descarga &&
                                                x.Codigo_Puerto_Embarque == a.Codigo_Puerto_Embarque).ToList();
                       if(data.Count > 0)
                        {
                            resultado.nTipoMensaje = (int)TipoMensaje.Informativo;
                            resultado.sMensaje = "Naviera: " + data[0].Linea_Naviera + "- Puerto Embarque:" + data[0].Puerto_Embarque + "- Puerto Descarga:" + data[0].Puerto_Descarga;
                            return Ok(resultado);

                        }
                    }
                }

                if (contenedor.Count > 0)
                {
                    foreach (var a in contenedor)
                    {
                        var data = contenedor.Where(x => x.Codigo_Linea_Naviera != a.Codigo_Linea_Naviera &&
                                                 x.Codigo_Puerto_Descarga == a.Codigo_Puerto_Descarga &&
                                                 x.Codigo_Puerto_Embarque == a.Codigo_Puerto_Embarque).ToList();
                        if (data.Count > 0)
                        {
                            resultado.nTipoMensaje = (int)TipoMensaje.Informativo;
                            resultado.sMensaje = "Naviera: " + data[0].Linea_Naviera + "- Puerto Embarque:" + data[0].Puerto_Embarque + "- Puerto Descarga:" + data[0].Puerto_Descarga;
                            return Ok(resultado);
                        }
                    }
                }

                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var user = await _userManager.GetUserAsync(HttpContext.User);
                
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    ent.Codigo_Usuario = Convert.ToString(idCliente);
                    ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
                    NombreCliente = NomCliente;
                }
                else
                {
                    ent.Codigo_Usuario = user.Email;
                    ent.IdUsuarioCreacion = user.Email;
                    NombreCliente = user.RazonSocial;
                }

                var res = BL.BL_InsertMaritimoCabecera(ent);
                 cod_mar = (string)res;
                var TodasNavieras = new OdooService(_configuration, _memoryCache).ListarNavieras();

                for (int i = 0; i < contenedor.Count; i++)
                { 
                    if(contenedor[i].Codigo_Linea_Naviera == "0")
                    {
                       
                        contenedor[i].Id_Cotizacion_Maritimo = Convert.ToDecimal(cod_mar);

                        foreach(var lis in TodasNavieras)
                        {
                            contenedor[i].Codigo_Linea_Naviera = lis.Codigo;
                            contenedor[i].Linea_Naviera = lis.Nombre;
                            BL.BL_InsertMaritimoDetalle(contenedor[i]);
                        }
                    }
                    else
                    {
                        contenedor[i].Id_Cotizacion_Maritimo = Convert.ToDecimal(cod_mar);
                        BL.BL_InsertMaritimoDetalle(contenedor[i]);
                    }
                    
                }

                #region Envio Oddo
                BL_Cotizacion BL2 = new BL_Cotizacion(_configuration);
                var res2 = BL2.BL_DetalleMaritimo(Convert.ToInt32(cod_mar));
                List<DetalleMaritimo> lsta = (List<DetalleMaritimo>)res2;


                List<CotizacionDetalleOdoo> ListaCotizacionDetalle = new List<CotizacionDetalleOdoo>();

                foreach(var lis in lsta)
                {
                    CotizacionDetalleOdoo ent2 = new CotizacionDetalleOdoo();
                    ent2.IdPuertoCarga = lis.Id_Puerto_Carga;
                    ent2.IdPuertoDescarga = lis.Id_Puerto_Descarga;
                    var Naviera = TodasNavieras.Where(x => x.Codigo == lis.Codigo_Linea_Naviera).ToList();
                    ent2.IdNaviera = Naviera[0].Id;
                    ent2.IdTerminalRetiroVacio = 1;
                    ent2.IdAlmacenPacking = 1;
                    ent2.IdCotizacionVeCloud = lis.Id_detalle_cotizacion_maritimo;
                    ListaCotizacionDetalle.Add(ent2);
                }



                CotizacionOdoo cotizacionOdoo = new CotizacionOdoo();

                cotizacionOdoo.CorreoCliente = lsta[0].Codigo_Usuario;
                cotizacionOdoo.IdCommodity = Convert.ToInt32(lsta[0].Codigo_Commodity);
                cotizacionOdoo.IdCampania = Convert.ToInt32(lsta[0].CodigoCampania);
                cotizacionOdoo.IdTipoContenedor = Convert.ToInt32(lsta[0].Codigo_Tipo_Contenedor);
                cotizacionOdoo.CantidadContenedor = Convert.ToInt32(lsta[0].Cantidad_Contenedor);
                cotizacionOdoo.TipoCotizacion = TipoCotizacion2.FleteMaritimo;
                cotizacionOdoo.ListaCotizacionDetalle = ListaCotizacionDetalle;
                cotizacionOdoo.IdCotizacionVeCloud = Convert.ToInt32(cod_mar);
                cotizacionOdoo.Notas = lsta[0].Nota;
                var resultOddo = new OdooService(_configuration, _memoryCache).CrearCotizacion(cotizacionOdoo);

                if (resultOddo > 0)
                {
                    BL.BL_InsertIdOddo_Maritimo(Convert.ToInt32(cod_mar), resultOddo);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroCotizacion = cod_mar
                    };

                    resultado.nTipoMensaje = (int)TipoMensaje.Correcto;
                    resultado.sMensaje = CotizacionMaritimaCorrecto.ENVIO_ODOO_CORRECTA;

                    await netMailSender.sendMailAsync(lsta[0].Cliente, emailTemplate, TipoEmail.COTIZACION_MARITIMA_1_RECIBIDO);
                }
                else
                {
                    resultado.nTipoMensaje = (int)TipoMensaje.Error;
                    resultado.sMensaje = CotizacionMaritimaError.ENVIO_ODOO_ERROR;
                }
                #endregion
                return Ok(resultado);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/InsertMaritimo");

                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = CotizacionMaritimaError.ENVIO_ODOO_ERROR;
                return Ok(resultado);
            }
        }
        [HttpPost]
        public async Task<ActionResult> ListaCotizacion(Parametros_Lista_Maritimo param, string tipousuario)
        {
            string htmlGrid = "";

            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: validar usuario sea customer.
            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                param.Codigo_Usuario = Convert.ToString(idCliente);
            }
            else
            {
                if (tipousuario != "customer")
                {
                    param.Codigo_Usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    param.Codigo_Usuario = "";
                }
            }

            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == null)
            {
                param.Campana = "";
            }

            param.cod_puerto_destino = param.cod_puerto_destino == null ? "0" : param.cod_puerto_destino;
            param.cod_puerto_salida = param.cod_puerto_salida == null ? "0" : param.cod_puerto_salida;
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                List<Lista_Maritimo> ListaTemporal = new List<Lista_Maritimo>();
                List<Lista_Maritimo> ListaPrincipal = new List<Lista_Maritimo>();
                param.ListadoUsuarios = new List<string>();
                if (param.Codigo_Usuario == "")
                {
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientesPorCustomer(correoCustomerServiceSecundario: HttpContext.User.Identity.Name);
                    
                    //var listarClientesOpl = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService2: HttpContext.User.Identity.Name);
                    //listarClientes.AddRange(listarClientesOpl);
                    var listaLimpia = listarClientes.Where(x => x.Correo != null && x.Correo != "").ToList().Select(x => x.Correo).ToList();
                    
                    param.ListadoUsuarios.AddRange(listaLimpia);
                    var res1 = BL.BL_Lista_CotizacionesAprobadas(param);
                    ListaTemporal = (List<Lista_Maritimo>)res1;
                    ListaPrincipal.AddRange(ListaTemporal);
                    //foreach (var correo in listaLimpia)
                    //{
                    //    C_Usuario item = new C_Usuario();
                    //    item.Usuario = correo;
                    //    param.ListadoUsuarios.Add(correo);
                    //}

                }
                else
                {
                    param.ListadoUsuarios.Add(param.Codigo_Usuario);
                    var res2 = BL.BL_Lista_CotizacionesAprobadas(param);
                    ListaPrincipal = (List<Lista_Maritimo>)res2;
                }

                //var res = BL.BL_Lista_CotizacionesAprobadas(param);
                htmlGrid = crearGridShow(ListaPrincipal);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/ListaCotizacion");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShow(List<Lista_Maritimo> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            //List<Lista_Maritimo> lista = (List<Lista_Maritimo>)list;

            List<string> campana = new List<string>();
            foreach (var lis in lista)
            {
                #region html
                //List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
                //Ent_Reserva ent = (Ent_Reserva)lis;
                if (campana.Contains(lis.Campania))
                {

                }
                else
                {
                    campana.Add(lis.Campania);

                    //campana.Where(x => x.)
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-2\" >");
                    htmlShow.Append("<h4> " + lis.Campania + "</h4> Campaña");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"col-6\">");
                    //htmlShow.Append("<h4> " + Convert.ToInt32(lis.Cant_contenedor) + "</h4>" + "Contenedores");
                    //htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"col-2\">");
                    //htmlShow.Append("Estado <i class=\"fas fa-exclamation-circle\"></i>" + lis.Estado);
                    //htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<table class=\"table table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
                    //htmlShow.Append("<table>");
                    htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
                    htmlShow.Append("<tr>");
                    //htmlShow.Append("<th></th>");
                    htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">FECHA APROB</th>");
                    htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\"># COTIZACION</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">REGION</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">LINE</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POL</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POD</th>");
                    //if(lis.REBATE > 0)
                    //{
                        htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">NETO</th>");
                        htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">REBATE</th>");
                    //}
                    
                    htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">FLETE B/L</th>");
                    htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">TT</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">VALID FROM</th>");
                    htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">VALID TO</th>");
                    //htmlShow.Append("<th>VALID TO</th>");
                    htmlShow.Append("<th></th>");
                    htmlShow.Append("</tr>");

                    foreach (var lis1 in lista.Where(x => x.Campania == lis.Campania))
                    {
                        //if (lis1.Codigo_coti == lis.Codigo_coti)
                        //{
                        htmlShow.Append("<tr style=\"border-bottom: 1px solid #ccc; border-left: 1px solid #ccc; border-right: 1px solid #ccc;\">");
                        //htmlShow.Append("<td>");
                        //htmlShow.Append("<div class=\"form-check\">");
                        //htmlShow.Append("<input type=\"checkbox\" class=\"form-check-input\">");
                        //htmlShow.Append("</div>");

                        //htmlShow.Append("</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.FECHA_APROBACION + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Codigo_coti + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Region + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Linea_naviera + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.POL + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.POD + "</td>");
                        if(lis1.REBATE > 0)
                        {
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + String.Format("{0:n0}", lis1.NETO) + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + String.Format("{0:n0}", lis1.REBATE) + "</td>");
                        }else
                        {
                            decimal suma = 0;
                            foreach ( var sum in  lista.Where(x => x.Campania == lis.Campania).ToList())
                            {
                                suma = sum.REBATE + suma;
                            }
                            if(suma > 0 )
                            {
                                htmlShow.Append("<td style=\"border: 1px solid #ccc;\"></td>");
                                htmlShow.Append("<td style=\"border: 1px solid #ccc;\"></td>");
                            }
                            else
                            {
                                htmlShow.Append("<td style=\"border: 1px solid #ccc;\"></td>");
                                htmlShow.Append("<td style=\"border: 1px solid #ccc;\"></td>");
                            }
                          
                        }
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\"> <b>" + String.Format("{0:n0}", lis1.FLETE) + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.TT + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Desde + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Hasta + "</td>");
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;\"> <button onclick=\" RealizarBooking(" + lis1.id_detalle + "); return false; \" class=\"btn btn-primary float-right\">Solicitar Booking</button></td>");
                        //htmlShow.Append("<td><a href=\"#\">Descargar en PDF</a></td>");
                        htmlShow.Append("</tr>");
                        //}

                    }
                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</div>");

                    htmlShow.Append(" <div class=\"card-footer\">");
                    //htmlShow.Append(" <button type=\"button\" class=\"btn btn-danger float-right\" onclick=\"RealizarBooking(); \">Rechazar </button>");
                    //htmlShow.Append(" <button type=\"button\" class=\"btn btn-success float-right\" onclick=\"RealizarBooking(); \">Aceptar </button>");
                    //htmlShow.Append(" <button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"RealizarBooking(); \"> Solicitar booking</button>");

                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                }
            }


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public async  Task<ActionResult> MisSolicitudes(Parametros_Lista_Maritimo param,string tipousuario)
        {
            string htmlGrid = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: data de prueba 


            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                param.Codigo_Usuario = Convert.ToString(idCliente);
            }
            else
            {
                if(tipousuario != "customer")
                {
                    param.Codigo_Usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    param.Codigo_Usuario = "";
                }
            }


            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == "Todas")
            {
                param.Campana = "";
            }
            if (param.cod_puerto_destino == null)
            {
                param.cod_puerto_destino = "0";
            }
            if (param.cod_puerto_salida == null)
            {
                param.cod_puerto_salida = "0";
            }
            if (param.Campana == null)
            {
                param.Campana = "";
            }

            try
            {

                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                List<Lista_Maritimo> ListaTemporal = new List<Lista_Maritimo>();
                List<Lista_Maritimo> ListaPrincipal = new List<Lista_Maritimo>();
                param.ListadoUsuarios = new List<string>();

                if (param.Codigo_Usuario == "")
                {
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientesPorCustomer(correoCustomerServiceSecundario: HttpContext.User.Identity.Name);

                    //var listarClientesOpl = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService2: HttpContext.User.Identity.Name);
                    //listarClientes.AddRange(listarClientesOpl);

                    var listaLimpia = listarClientes.Where(x => x.Correo != null && x.Correo != "").ToList().Select(x => x.Correo).ToList();
                    param.ListadoUsuarios.AddRange(listaLimpia);

                    var res1 = BL.BL_Lista_Cotizaciones(param);
                    ListaTemporal = (List<Lista_Maritimo>)res1;
                    ListaPrincipal.AddRange(ListaTemporal);

                    //foreach (var cliente in listarClientes)
                    //{
                    //    if (cliente.Correo != null && cliente.Correo != "")
                    //    {
                    //        param.Codigo_Usuario = cliente.Correo;
                    //        var res1 = BL.BL_Lista_Cotizaciones(param);
                    //        ListaTemporal = (List<Lista_Maritimo>)res1;
                    //        ListaPrincipal.AddRange(ListaTemporal);
                    //    }
                    //}
                }
                else
                {
                    param.ListadoUsuarios.Add(param.Codigo_Usuario);
                    var res2 = BL.BL_Lista_Cotizaciones(param);
                    ListaPrincipal = (List<Lista_Maritimo>)res2;
                }

                htmlGrid = crearGridShowSolicitudes(ListaPrincipal);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/MisSolicitudes");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShowSolicitudes(List<Lista_Maritimo> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<int> id_cotizacion = new List<int>();
            if(lista.Count > 0)
            {
                foreach (var lis in lista)
                {
                    #region html
                    if (id_cotizacion.Contains(lis.Codigo_coti))
                    {

                    }
                    else
                    {
                        //List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
                        //Ent_Reserva ent = (Ent_Reserva)lis;
                        //if (campana.Contains(lis.Campania))
                        //{
                        id_cotizacion.Add(lis.Codigo_coti);
                        //}
                        //else
                        //{
                        //campana.Add(lis.Campania);

                        //campana.Where(x => x.)
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-md-12\">");
                        htmlShow.Append("<div class=\"card\">");
                        htmlShow.Append("<div class=\"card-body\">");

                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-2\" >");
                        htmlShow.Append("<h4> " + lis.Campania + "</h4> Campaña");
                        htmlShow.Append("</div>");

                        htmlShow.Append("<div class=\"col-2\">");
                        htmlShow.Append("<h4> " + Convert.ToInt32(lis.Codigo_coti) + "</h4>" + "Nro Cotización");
                        htmlShow.Append("</div>");

                        htmlShow.Append("<div class=\"col-6\">");
                        htmlShow.Append("<h4> " + Convert.ToInt32(lis.Cantidad_Contenedor) + "</h4>" + "Contenedores");
                        htmlShow.Append("</div>");

                        htmlShow.Append("<div class=\"col-2\">");
                        htmlShow.Append("Estado <i class=\"fas fa-exclamation-circle\"></i>" + lis.Estado);
                        htmlShow.Append("</div>");

                        htmlShow.Append("</div>");

                        htmlShow.Append("<table class=\"table table-hover\" >");
                        htmlShow.Append("<tbody>");
                        htmlShow.Append("<tr>");
                        htmlShow.Append("<th>Servicio</th>");
                        htmlShow.Append("<th>Origen</th>");
                        htmlShow.Append("<th>Destino</th>");
                        htmlShow.Append("<th>Nro Servicio</th>");



                        htmlShow.Append("<th style=\"text-align: right\">");
                        htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='VerDetalle({0});'>Ver Detalle</a>", lis.Codigo_coti);
                        if (lis.IdEstado >= 2)
                        {
                            htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='DescargarPDFCotizacion({0});'>Descargar PDF</a>", lis.IdOddoService);
                        }
                        htmlShow.Append("</th>");
                        htmlShow.Append("</tr>");

                        htmlShow.Append("<tr>");
                        htmlShow.Append("<td> Maritimo");
                        htmlShow.Append("</td>");
                        if (lis.cantPuertoCarga > 1)
                        {
                            htmlShow.Append("<td> Varios Origenes");
                        }
                        else
                        {
                            htmlShow.Append("<td>" + lis.POL);
                        }
                        htmlShow.Append("</td>");

                        if (lis.cantPuertoDescarga > 1)
                        {
                            htmlShow.Append("<td> Varios Destinos");
                        }
                        else
                        {
                            htmlShow.Append("<td>" + lis.POD);
                        }
                        htmlShow.Append("</td>");

                        htmlShow.Append("<td>" + lis.NumeroContrato);
                        htmlShow.Append("</td>");


                        htmlShow.Append("<td > ");
                        htmlShow.Append("</td>");


                        htmlShow.Append("<td > ");
                        htmlShow.Append("</td>");

                        htmlShow.Append("</tr>");
                        htmlShow.Append("</tbody>");
                        htmlShow.Append("</table>");
                        htmlShow.Append("</div>");

                        htmlShow.Append(" <div class=\"card-footer\">");
                        if (lis.IdEstado == 2)
                        {
                            htmlShow.Append("<div class=\"row\">");
                            htmlShow.Append("<div class=\"col-8\">");
                            htmlShow.Append("</div>");

                            htmlShow.Append("<div class=\"col-2\">");
                            htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:green\" class=\"btn btn-success float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Maritimo({0},{1}); \">Confirmar </button>", lis.Codigo_coti, 3);
                            htmlShow.Append("</div>");

                            htmlShow.Append("<div class=\"col-2\">");
                            htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:red\" class=\"btn btn-danger float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Maritimo({0},{1}); \">Rechazar </button>", lis.Codigo_coti, 4);
                            htmlShow.Append("</div>");


                            htmlShow.Append("</div>");

                        }
                        else if (lis.IdEstado == 3)
                        {
                            //htmlShow.AppendFormat(" <button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"RealizarBooking({0}); \"> Solicitar booking</button>", lis.Codigo_coti);
                        }

                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");

                        //}
                    }
                    #endregion
                }
            } else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }
            

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
        #endregion

        [HttpPost]
        public ActionResult PDF_CotizacionMaritimo(int id)
        {
            string base64 = "";
          
            try
            {
                 base64 = new OdooService(_configuration, _memoryCache).GetCotizacionPDF(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/PDF_CotizacionMaritimo");
            }
            return Ok(base64);
        }

        #region Logistica
        [HttpPost]
        public async Task<ActionResult> Confirmar_Rechazar_Logistica(int id, int estado)
        {
            string cod = "";
            int idCotizacionOdoo = 0;
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_Confirmar_RechazarCotizacionLogistica(id, estado);
                cod = (string)res;

                idCotizacionOdoo = Convert.ToInt32(cod);
                if (estado == 3)
                {
                    var resultado = new OdooService(_configuration, _memoryCache).ConfirmarCotizacionDetalle(idCotizacionOdoo);
                    var res2 = BL.BL_DetalleLogistica(id);
                    List<DetalleLogistica> lsta = (List<DetalleLogistica>)res2;
                    DetalleLogistica detalle = (DetalleLogistica)lsta[0];
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(lsta[0].Cliente);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroCotizacion = id.ToString()
                    };

                    await netMailSender.sendMailAsync(detalle.Cliente, emailTemplate, TipoEmail.COTIZACION_LOGISTICA_3_ACEPTADO);


                }
                else if (estado == 4)
                {
                    var resultado = new OdooService(_configuration, _memoryCache).CancelarCotizacionDetalle(idCotizacionOdoo);

                }

                //var resultado = new OdooService(_configuration).ConfirmarCotizacionDetalle(idCotizacionOdoo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/Confirmar_Rechazar_Logistica idCotizacionOdoo:{idCotizacionOdoo}", idCotizacionOdoo);
            }
            return Ok(cod);
        }
        [HttpPost]
        public async Task<ActionResult> InsertLogi(BE_Logistica ent)
        {
            Resultado resultado = new Resultado();

            string cod = "";
            try
            {
                var usuario = "";
                var cliente = "";
                var NomCliente = "";
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                var cotizacionOdoo = new CotizacionOdoo();

                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    NomCliente = datos[0].Value;
                    cliente = Convert.ToString(idCliente);
                    usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    cliente = HttpContext.User.Identity.Name;
                    usuario = HttpContext.User.Identity.Name;
                    NomCliente = user.RazonSocial;
                }

                //string correoUsuario = HttpContext.User.Identity.Name;
                var listaPacking = new List<SelectListItem>();
                new OdooService(_configuration, _memoryCache).ListarPacking(cliente).ForEach(a => listaPacking.Add(new SelectListItem()
                {
                    Value = a.Id.ToString(),
                    Text = a.Nombre
                }));
                List<BE_Logistica> listaPack = new List<BE_Logistica>();
                if (ent.Codigo_Almacen_Packing == "0")
                {
                    foreach (var a in listaPacking)
                    {
                        BE_Logistica ent3 = new BE_Logistica();
                        ent3.Codigo_Almacen_Packing = a.Value;
                        ent3.Almacen_Packing = a.Text;
                        listaPack.Add(ent3);
                    }
                }
                else
                {
                    BE_Logistica ent4 = new BE_Logistica();
                    ent4.Codigo_Almacen_Packing = ent.Codigo_Almacen_Packing;
                    ent4.Almacen_Packing = ent.Almacen_Packing;
                    listaPack.Add(ent4);
                }

               
                    OdooService odooService = new OdooService(_configuration, _memoryCache);
                    var _listaDepositoVaciosPorNaviera = new List<Deposito>();
                    var _tipoContenedor = odooService.ListarTipoContenedor(name: "40 Reefer High Cube").First();
                    var _listaNavieras = odooService.ListarNavieras();
                    var _listaTerminales = odooService.ListarTerminal(id: ent.Id_Puerto_Carga);
                    //var _listaDepositosVacios = odooService.ListarDepositoVacios();
                    var _company = odooService.getCompany();
                    var _listaServiciosExtra = ListarServiciosExtra(ent);
                    
                        foreach(var ent1 in _listaNavieras)
                        {
                            var lis = odooService.ListarDepositoVaciosPorNaviera(idNaviera: ent1.Id, idPuertoOrigen: ent.Id_Puerto_Carga);
                            lis.ForEach(a => a.IdNaviera = ent1.Id);
                            _listaDepositoVaciosPorNaviera.AddRange(lis);
                        }


                    //if (ent.Chk_TermoRegistros == 1) _listaServiciosExtra.Where(a => a.Nombre == "Termo registros").First().Descripcion = ent.TipoTermo;
                    //if (ent.Chk_TermoRegistros == 1) _listaServiciosExtra.Where(a => a.Nombre == "Termo registros").First().Cantidad = ent.CantidadTermo;
                    //if (ent.Chk_FiltrosEtileno == 1) _listaServiciosExtra.Where(a => a.Nombre == "Filtros de Etileno").First().Descripcion = ent.TipoFiltro;
                    //if (ent.Chk_FiltrosEtileno == 1) _listaServiciosExtra.Where(a => a.Nombre == "Filtros de Etileno").First().Cantidad = ent.CantidadFiltro;

                //var usuario = HttpContext.User.Identity.Name;
                    BL_Cotizacion bl = new BL_Cotizacion(_configuration);

                    CotizacionLogistica cotizacion = new CotizacionLogistica();
                    cotizacion.Cliente = cliente;
                    cotizacion.IdCommodity = Convert.ToInt32(ent.Codigo_Commodity);
                    cotizacion.NombreCommodity = ent.Commodity;
                    cotizacion.IdCampania = ent.IdCampania;
                    cotizacion.NombreCampania = ent.Desc_Campania;
                    cotizacion.IdTipoContenedor = _tipoContenedor.Id;
                    cotizacion.NombreTipoContenedor = _tipoContenedor.Nombre;
                    cotizacion.Notas = ent.Comentarios;
                    cotizacion.IdEstado = 1; //PENDIENTE
                    cotizacion.FechaCreacion = DateTime.Now;
                    cotizacion.UsuarioCreacion = usuario;
                    cotizacion.Codigo_Usuario = cliente;
                    _context.CotizacionLogistica.Add(cotizacion);
                    var resultadoCotizacion = _context.SaveChanges(); //TODO: QUITAR Y OPTIMIZAR ESTE SAVECHANGES

                foreach (var lis in listaPack)
                {
                    ent.Codigo_Almacen_Packing = lis.Codigo_Almacen_Packing;
                    ent.Almacen_Packing = lis.Almacen_Packing;

                    List<CotizacionLogisticaDetalle> listaCotizacionDetalle = new List<CotizacionLogisticaDetalle>();
                    foreach (var naviera in _listaNavieras)
                    {
                        //var aa = odooService.ListarDepositoVaciosPorNaviera(idNaviera: naviera.Id, idPuertoOrigen: ent.Id_Puerto_Carga);
                        foreach (var depositoVacio in _listaDepositoVaciosPorNaviera.Where(x => x.IdNaviera == naviera.Id))
                        {
                            foreach (var terminal in _listaTerminales)
                                {
                                    CotizacionLogisticaDetalle detalleDirecto = new CotizacionLogisticaDetalle();
                                    detalleDirecto.IdCotizacionLogistica = cotizacion.IdCotizacionLogistica;
                                    detalleDirecto.IdNaviera = naviera.Id;
                                    detalleDirecto.NombreNaviera = naviera.Nombre;
                                    detalleDirecto.IdPuertoCarga = ent.Id_Puerto_Carga;
                                    detalleDirecto.NombrePuertoCarga = ent.Puerto_Embarque;
                                    detalleDirecto.CodigoPaisCarga = ent.Codigo_Pais_Embarque;
                                    detalleDirecto.NombrePaisCarga = ent.Pais_Puerto_Embarque;
                                    detalleDirecto.IdServicio = _company.IdOperacionLogistica_Directo;
                                    detalleDirecto.NombreServicio = "Directo";
                                    detalleDirecto.IdTerminalRetiroVacio = terminal.Id;
                                    detalleDirecto.NombreTerminalRetiroVacio = terminal.Nombre;
                                    detalleDirecto.IdAlmacenPacking = Convert.ToInt32(ent.Codigo_Almacen_Packing);
                                    detalleDirecto.NombreAlmacenPacking = ent.Almacen_Packing;
                                    detalleDirecto.IdEstado = 1; ///PENDIENTE
                                    detalleDirecto.FechaCreacion = DateTime.Now;
                                    detalleDirecto.UsuarioCreacion = usuario;
                                    detalleDirecto.NombreTerminalDepositoVacio = depositoVacio.Nombre;
                                    detalleDirecto.IdTerminalDepositoVacio = depositoVacio.Id;
                                    listaCotizacionDetalle.Add(detalleDirecto);

                                    if (terminal.EsDepositoTemporal)
                                    {
                                        CotizacionLogisticaDetalle detalleTemporal = new CotizacionLogisticaDetalle();
                                        detalleTemporal.IdCotizacionLogistica = cotizacion.IdCotizacionLogistica;
                                        detalleTemporal.IdNaviera = naviera.Id;
                                        detalleTemporal.NombreNaviera = naviera.Nombre;
                                        detalleTemporal.IdPuertoCarga = ent.Id_Puerto_Carga;
                                        detalleTemporal.NombrePuertoCarga = ent.Puerto_Embarque;
                                        detalleTemporal.CodigoPaisCarga = ent.Codigo_Pais_Embarque;
                                        detalleTemporal.NombrePaisCarga = ent.Pais_Puerto_Embarque;
                                        detalleTemporal.IdServicio = _company.IdOperacionLogistica_DepositoTemporal;
                                        detalleTemporal.NombreServicio = "DepositoTemporal";
                                        detalleTemporal.IdTerminalRetiroVacio = terminal.Id;
                                        detalleTemporal.NombreTerminalRetiroVacio = terminal.Nombre;
                                        detalleTemporal.IdAlmacenPacking = Convert.ToInt32(ent.Codigo_Almacen_Packing);
                                        detalleTemporal.NombreAlmacenPacking = ent.Almacen_Packing;
                                        detalleTemporal.IdEstado = 1; ///PENDIENTE
                                        detalleTemporal.FechaCreacion = DateTime.Now;
                                        detalleTemporal.UsuarioCreacion = usuario;
                                        detalleTemporal.NombreTerminalDepositoVacio = depositoVacio.Nombre;
                                        detalleTemporal.IdTerminalDepositoVacio = depositoVacio.Id;

                                        listaCotizacionDetalle.Add(detalleTemporal);
                                    }
                                }
                        }
                    }

                    foreach (var cotizacionDetalle in listaCotizacionDetalle)
                    {
                        cotizacionDetalle.ServiciosExtra = new List<CotizacionLogisticaServicioExtra>();
                        
                        foreach (var extra in _listaServiciosExtra)
                        {
                            var cotizacionServicioExtra = new CotizacionLogisticaServicioExtra();
                            //cotizacionServicioExtra.IdCotizacionLogisticaDetalle = cotizacionDetalle.IdCotizacionLogisticaDetalle;

                            if (extra.IdServicio == 21 || extra.IdServicio == 22)
                            {
                                cotizacionServicioExtra.IdServicio = extra.IdServicio;
                                cotizacionServicioExtra.NombreServicio = extra.Descripcion;
                                cotizacionServicioExtra.DescripcionServicio = string.Empty;
                                cotizacionServicioExtra.Cantidad = extra.Cantidad;
                                
                            }
                            else
                            {
                                cotizacionServicioExtra.IdServicio = extra.IdServicio;
                                cotizacionServicioExtra.NombreServicio = extra.Nombre;
                                cotizacionServicioExtra.DescripcionServicio = extra.Descripcion;
                                cotizacionServicioExtra.Cantidad = 1;

                            }

                            cotizacionServicioExtra.FechaCreacion = DateTime.Now;
                            cotizacionServicioExtra.UsuarioCreacion = usuario;
                            //_context.CotizacionLogisticaServicioExtra.Add(cotizacionServicioExtra);
                            cotizacionDetalle.ServiciosExtra.Add(cotizacionServicioExtra);
                        }
                    }

                    //ESCRIBE EN ODOO

                    //
                    //listaCotizacionDetalle
                    foreach (var detalle in listaCotizacionDetalle)
                    {
                        var detalleOdoo = new CotizacionDetalleOdoo();
                        detalleOdoo.IdNaviera = detalle.IdNaviera.Value;
                        detalleOdoo.IdPuertoCarga = detalle.IdPuertoCarga.Value;
                        //detalleOdoo.IdCotizacionVeCloud = detalle.IdCotizacionLogisticaDetalle;
                        detalleOdoo.IdProduct = detalle.IdServicio.Value;
                        detalleOdoo.IdTerminalRetiroVacio = detalle.IdTerminalRetiroVacio.Value;
                        detalleOdoo.IdTerminalDepositoVacio = detalle.IdTerminalDepositoVacio;
                        detalleOdoo.IdAlmacenPacking = detalle.IdAlmacenPacking.Value;

                        foreach (var servicio in _listaServiciosExtra)
                        {
                            if(servicio.IdServicio == 21 )
                            {
                                detalleOdoo.ListaServicios.Add(new oddo.Servicio()
                                {
                                    IdServicio = ent.IdTermo,
                                    Descripcion = servicio.Descripcion,
                                    Cantidad = servicio.Cantidad
                                });
                            }else if (servicio.IdServicio == 22)
                            {
                                detalleOdoo.ListaServicios.Add(new oddo.Servicio()
                                {
                                    IdServicio = ent.IdFiltro,
                                    Descripcion = servicio.Descripcion,
                                    Cantidad = servicio.Cantidad
                                });
                            }
                            else
                            {
                                detalleOdoo.ListaServicios.Add(new oddo.Servicio()
                                {
                                    IdServicio = servicio.IdServicio,
                                    Descripcion = servicio.Descripcion,
                                    Cantidad = servicio.Cantidad
                                });
                            }
                            
                        }
                        cotizacionOdoo.ListaCotizacionDetalle.Add(detalleOdoo);
                    }
                    bl.BulkInsertCotizacionDetalle(listaCotizacionDetalle);
                    //_context.AddRange(listaCotizacionDetalle);
                    //_context.SaveChanges();

                }
                cotizacionOdoo.ListaCotizacionDetalle = new List<CotizacionDetalleOdoo>();


                var lista = (List<BE_DetalleLogisticaParaOdoo>)bl.BL_DetalleCotizacionLogistica(cotizacion.IdCotizacionLogistica);
                foreach (var detalle in lista)
                {
                    var detalleOdoo = new CotizacionDetalleOdoo();
                    detalleOdoo.IdNaviera = detalle.IdNaviera;
                    detalleOdoo.IdPuertoCarga = detalle.IdPuertoCarga;
                    detalleOdoo.IdCotizacionVeCloud = detalle.IdCotizacionLogisticaDetalle;
                    detalleOdoo.IdProduct = detalle.IdServicio;
                    detalleOdoo.IdTerminalRetiroVacio = detalle.IdTerminalRetiroVacio;
                    detalleOdoo.IdTerminalDepositoVacio = detalle.IdTerminalDepositoVacio;
                    detalleOdoo.IdAlmacenPacking = detalle.IdAlmacenPacking;

                    foreach (var servicio in _listaServiciosExtra)
                    {
                        if (servicio.IdServicio == 21)
                        {
                            detalleOdoo.ListaServicios.Add(new oddo.Servicio()
                            {
                                IdServicio = ent.IdTermo,
                                //Descripcion = ent.TipoTermo,
                                Cantidad = servicio.Cantidad
                            });
                        }
                        else if (servicio.IdServicio == 22)
                        {
                            detalleOdoo.ListaServicios.Add(new oddo.Servicio()
                            {
                                IdServicio = ent.IdFiltro,
                                //Descripcion = ent.TipoFiltro,
                                Cantidad = servicio.Cantidad
                            });
                        }
                        else
                        {
                            detalleOdoo.ListaServicios.Add(new oddo.Servicio()
                            {
                                IdServicio = servicio.IdServicio,
                                Descripcion = servicio.Descripcion,
                                Cantidad = servicio.Cantidad
                            });
                        }
                    }
                    cotizacionOdoo.ListaCotizacionDetalle.Add(detalleOdoo);
                }

                cotizacionOdoo.CorreoCliente = cliente;
                cotizacionOdoo.IdCommodity = cotizacion.IdCommodity.Value;
                cotizacionOdoo.IdCampania = cotizacion.IdCampania.Value;
                cotizacionOdoo.IdCotizacionVeCloud = cotizacion.IdCotizacionLogistica;
                cotizacionOdoo.TipoCotizacion = TipoCotizacion2.OperacionLogistica;
                cotizacionOdoo.IdTipoContenedor = cotizacion.IdTipoContenedor.Value;
                cotizacionOdoo.Notas = cotizacion.Notas;
                var resultadoExtra = _context.SaveChanges();//TODO: QUITAR Y OPTIMIZAR ESTE SAVECHANGES


                if (cotizacionOdoo.ListaCotizacionDetalle.Count() > 0)
                {
                    var resultOddo = new OdooService(_configuration, _memoryCache).CrearCotizacion(cotizacionOdoo);
                    var cotizacionParaActualizar = _context.CotizacionLogistica.First(a => a.IdCotizacionLogistica == cotizacion.IdCotizacionLogistica);
                    cotizacionParaActualizar.IdCotizacionOdoo = resultOddo;
                    var resultadoCotizacion2 = _context.SaveChanges();

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NomCliente,
                        NroCotizacion = cotizacion.IdCotizacionLogistica.ToString()
                    };

                    resultado.nTipoMensaje = (int)TipoMensaje.Correcto;
                    resultado.sMensaje = CotizacionLogisticaCorrecto.CREACION_CORRECTA;

                    await netMailSender.sendMailAsync(cotizacion.Cliente, emailTemplate, TipoEmail.COTIZACION_LOGISTICA_1_RECIBIDO);
                }
                else
                {
                    resultado.nTipoMensaje = (int)TipoMensaje.Error;
                    resultado.sMensaje = CotizacionLogisticaError.COTIZACION_DETALLE_VACIA;

                    return Ok(resultado);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/InsertLogi");
                string cad = ex.Message;
                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = ex.Message;
                return Ok(resultado);
            }
            return Ok("1");
        }
        private List<oddo.Servicio> ListarServiciosExtra(BE_Logistica ent)
        {
            List<oddo.Servicio> listaNombreServicios = new List<oddo.Servicio>();
            oddo.Servicio servicio = new oddo.Servicio();

            string nameTransporte = ent.Chk_Transporte == 1 ? "Transporte" : "";
            string nameGateOut = ent.Chk_GateOut == 1 ? "Gate Out" : "";
            string nameAgenciamientoAduana = ent.Chk_AgenciamientoAduana == 1 ? "Agenciamiento de Aduanas" : "";
            string nameDerechoEmbarque = ent.Chk_DerechoEmbarque == 1 ? "Derecho de Embarque" : "";
            string nameVistoBueno = ent.Chk_VistoBueno == 1 ? "Vistos buenos o Agenciamiento Maritimo" : "";
            string nameEnvioDocumentos = ent.Chk_EnvioDocumentos == 1 ? "Envio Documentos Courier" : "";
            string nameMovilidad = ent.Chk_Movilidad == 1 ? "Movilizacion senasa" : "";
            //string nameTermoRegistros = ent.Chk_TermoRegistros == 1 ? "Termo registros" : "";
            //string nameFiltrosEtileno = ent.Chk_FiltrosEtileno == 1 ? "Filtros de Etileno" : "";
            string nameCertificado = ent.Chk_Certificado == 1 ? "Certificado Fitosanitario" : "";

            if (nameTransporte != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameTransporte;
                listaNombreServicios.Add(servicio);
            }
            if (nameGateOut != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameGateOut;
                listaNombreServicios.Add(servicio);
            }
            if (nameAgenciamientoAduana != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameAgenciamientoAduana;
                listaNombreServicios.Add(servicio);
            }
            if (nameDerechoEmbarque != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameDerechoEmbarque;
                listaNombreServicios.Add(servicio);
            }
            if (nameVistoBueno != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameVistoBueno;
                listaNombreServicios.Add(servicio);
            }
            if (nameEnvioDocumentos != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameEnvioDocumentos;
                listaNombreServicios.Add(servicio);
            }
            if (nameMovilidad != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameMovilidad;
                listaNombreServicios.Add(servicio);
            }
            //if (nameTermoRegistros != "")
            //{
            //    servicio = new oddo.Servicio();
            //    servicio.Nombre = nameTermoRegistros;
            //    listaNombreServicios.Add(servicio);
            //}
            //if (nameFiltrosEtileno != "")
            //{
            //    servicio = new oddo.Servicio();
            //    servicio.Nombre = nameFiltrosEtileno;
            //    listaNombreServicios.Add(servicio);
            //}
            if (nameCertificado != "")
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = nameCertificado;
                listaNombreServicios.Add(servicio);
            }

            if (ent.Chk_TermoRegistros == 1)
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = ent.TipoTermo;
                listaNombreServicios.Add(servicio);
            }

            if (ent.Chk_FiltrosEtileno == 1)
            {
                servicio = new oddo.Servicio();
                servicio.Nombre = ent.TipoFiltro;
                listaNombreServicios.Add(servicio);
            }

            //    _listaServiciosExtra.Where(a => a.Nombre == "Termo registros").First().Descripcion = ent.TipoTermo;
            //if (ent.Chk_TermoRegistros == 1) _listaServiciosExtra.Where(a => a.Nombre == "Termo registros").First().Cantidad = ent.CantidadTermo;
            //if (ent.Chk_FiltrosEtileno == 1) _listaServiciosExtra.Where(a => a.Nombre == "Filtros de Etileno").First().Descripcion = ent.TipoFiltro;
            //if (ent.Chk_FiltrosEtileno == 1) _listaServiciosExtra.Where(a => a.Nombre == "Filtros de Etileno").First().Cantidad = ent.CantidadFiltro;

            var listaServicios = new OdooService(_configuration, _memoryCache).GetProduct(listaNombreServicios.Select(a => a.Nombre).Distinct().ToList());

            // se asigna las cantidades a los termos y filtros
            if (ent.Chk_TermoRegistros == 1)
            {
                var termo = listaServicios.First(a => a.Nombre == ent.TipoTermo);
                if (termo != null) termo.Cantidad = ent.CantidadTermo;
            }

            if (ent.Chk_FiltrosEtileno == 1)
            {
                var filtro = listaServicios.First(a => a.Nombre == ent.TipoFiltro);
                if (filtro != null) filtro.Cantidad = ent.CantidadFiltro;
            }

            return listaServicios;
        }
        [HttpPost]
        public ActionResult DetalleLogistica(int id)
        {
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_DetalleLogistica(id);
                List<DetalleLogistica> lsta = (List<DetalleLogistica>)res;
                DetalleLogistica detalle = (DetalleLogistica)lsta[0];
                return Ok(detalle);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/DetalleLogistica");
                return Ok("");
            }
        }
        [HttpPost]
        public ActionResult ListaLogistica(Parametros_Lista_Logistica param)
        {
            string htmlGrid = "";
            if (param.numero_contenedor == null)
            {
                param.numero_contenedor = "";
            }
            if (param.codigo_bl == null)
            {
                param.codigo_bl = "";
            }
            if (param.numero_booking == null)
            {
                param.numero_booking = "";
            }
            if (param.numero_dam == null)
            {
                param.numero_dam = "";
            }
            if (param.numero_viaje == null)
            {
                param.numero_viaje = "";
            }
            //if (param.Campana == "Todas")
            //{
            //    param.Campana = "";
            //}

            try
            {

                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_Lista_Logistica(param);
                htmlGrid = crearGridShowLogistica(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/ListaLogistica");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShowLogistica(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<Lista_Logistica> lista = (List<Lista_Logistica>)list;

            List<string> campana = new List<string>();
            foreach (var lis in lista)
            {
                #region html Antiguo
                ////List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
                ////Ent_Reserva ent = (Ent_Reserva)lis;

            //    //campana.Where(x => x.)
            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-md-12\">");
            //    htmlShow.Append("<div class=\"card\">");
            //    htmlShow.Append("<div class=\"card-body\">");

            //    htmlShow.Append("<table  class=\"table\">");
            //    htmlShow.Append("<tbody>");
            //    htmlShow.Append("<th style=\"width:25%\">");
            //    htmlShow.Append("<th style=\"width:60%\">");
            //    htmlShow.Append("<th style=\"width:15%\">");
            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border-top:none; \"><h4>" + lis.Nro_Contenedor + "</h4>Nro de contenedor</td>");
            //    htmlShow.Append("<td style=\"border-top:none; \"><h4>" + lis.Campania + "</h4>Commodity</td>");
            //    htmlShow.Append("<td style=\"border-top:none;\"class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> Deposito de vacios</td>");
            //    htmlShow.Append("</tr>");
            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-6\" >Booking: </div>");
            //    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Booking + "</b></div>");
            //    htmlShow.Append("</div>");

            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-6\">Canal: </div>");
            //    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Canal + "</b></div>");
            //    htmlShow.Append("</div>");

            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-6\">Linea: </div>");
            //    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Linea + "</b></div>");
            //    htmlShow.Append("</div>");

            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-6\">Deposito: </div>");
            //    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Deposito + "</b></div>");
            //    htmlShow.Append("</div>");

            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-6\">Packing: </div>");
            //    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Packing + "</b></div>");
            //    htmlShow.Append("</div>");

            //    htmlShow.Append("<div class=\"row\">");
            //    htmlShow.Append("<div class=\"col-6\">Nro Cotizacion: </div>");
            //    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.IdCotizacionLogistica + "</b></div>");
            //    htmlShow.Append("</div>");
            //    htmlShow.Append("</td>");
            //    htmlShow.Append("<td style=\"border-top:none; \" class=\"border-right\">");

            //    htmlShow.Append("<table  class=\"table\">");
            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<th style=\"border:none;\"></th>");
            //    htmlShow.Append("<th style=\"border:none;\">ANTERIOR</th>");
            //    htmlShow.Append("<th style=\"border:none;\">ACTUAL</th>");
            //    htmlShow.Append("<th style=\"border:none;\">PROXIMO</th>");
            //    htmlShow.Append("</tr>");

            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border:none;\">Estado</td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>En espera</b></td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>Deposito Vacio</b></td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>Transito packing</b></td>");
            //    htmlShow.Append("</tr>");

            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border:none;\">Fec. Programacion</td>");
            //    htmlShow.Append("<td style=\"border:none;\">"+lis.Fecha1+"</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha2 + "</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha3 + "</td>");
            //    htmlShow.Append("</tr>");

            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border:none;\">Fec. Ini.</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha1 + "</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha2 + "</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha3 + "</td>");
            //    htmlShow.Append("</tr>");

            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border:none;\">Fec. Fin.</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha1 + "</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha2 + "</td>");
            //    htmlShow.Append("<td style=\"border:none;\">" + lis.Fecha3 + "</td>");
            //    htmlShow.Append("</tr>");

            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border:none;\">Duración</td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>" + lis.Hora1 + "</b></td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>" + lis.Hora2 + "</b></td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>" + lis.Hora3 + "</b></td>");
            //    htmlShow.Append("</tr>");


            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td style=\"border:none;\">Retraso</td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>" + lis.Hora1 + "</b></td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>" + lis.Hora2 + "</b></td>");
            //    htmlShow.Append("<td style=\"border:none;\"><b>" + lis.Hora3 + "</b></td>");
            //    htmlShow.Append("</tr>");
            //    htmlShow.Append("</table>");
            //    htmlShow.Append("</td>");
            //    htmlShow.Append("<td class=\"text-center\" style=\"border-top:none; \">");
            //    //htmlShow.Append("<a href=\"#\">Ver Detalles</a><br>");
            //    if(lis.IdOperacionLogistica > 0)
            //    {
            //        htmlShow.AppendFormat("<button onclick=\" holamundo(" + lis.IdOperacionLogistica + "); return false; \" class=\"btn btn-primary float-right\">Ver Detalles</button>");
            //    }
            //    else
            //    {
            //        htmlShow.AppendFormat("<button onclick=\" holamundo(" + lis.IdOperacionLogistica + "); return false; \" class=\"btn btn-primary float-right\" disabled >Ver Detalles</button>");
            //    }
                  
            //    //htmlShow.AppendFormat("<button onclick=\" holamundo("+lis + "); return false; \" class=\"btn btn-primary float-right\">Ver Detalles</button>");
            //    htmlShow.Append("</td>");
            //    htmlShow.Append("</tr>");
            //    htmlShow.Append("</tbody>");
            //    htmlShow.Append("</table>");

            //    htmlShow.Append("</div>");
            //    htmlShow.Append("</div>");
            //    htmlShow.Append("</div>");
            //    htmlShow.Append("</div>");



            //}


            #endregion


            #region html
            //List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
            //Ent_Reserva ent = (Ent_Reserva)lis;

            //campana.Where(x => x.)
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-md-12\">");
            htmlShow.Append("<div class=\"card\">");
            htmlShow.Append("<div class=\"card-body\">");

            htmlShow.Append("<table  class=\"table table-hover\">");
            htmlShow.Append("<tbody>");
            htmlShow.Append("<th style=\"width:25%\">");
            htmlShow.Append("<th style=\"width:20%\">");
            htmlShow.Append("<th style=\"width:20%\">");
            htmlShow.Append("<tr>");
            htmlShow.Append("<td style=\"border-top:none; \"><h4>" + lis.Nro_Contenedor + "</h4>Nro de contenedor</td>");
            htmlShow.Append("<td style=\"border-top:none; \"><h4>" + lis.NombreCommodity + "</h4>Commodity</td>");
                htmlShow.Append("<td style=\"border-top:none; \"><h4>" + lis.Cliente + "</h4>Cliente</td>");
            //htmlShow.Append("<td style=\"border-top:none;\"class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> Deposito de vacios</td>");
            htmlShow.Append("</tr>");
            htmlShow.Append("<tr>");

            #region Filas
            htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-6\" >Booking </div>");
            htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Booking + "</b></div>");
            htmlShow.Append("</div>");

            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-6\">Nro Viaje </div>");
            htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Nro_Viaje + "</b></div>");
            htmlShow.Append("</div>");

          

            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-6\">Linea </div>");
            htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Linea + "</b></div>");
            htmlShow.Append("</div>");

            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-6\">Terminal de Retiro de Vacio </div>");
            htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Deposito + "</b></div>");
            htmlShow.Append("</div>");

            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-6\">Packing </div>");
            htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Packing + "</b></div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</td>");


            //segunda fila 
            htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-6\" >Operador Logístico </div>");
            htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + "VE CLOUD " + "</b></div>");
            htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Orden Servicio </div>");
                htmlShow.Append("<div class=\"col-6 text-right \"  ><b> " + lis.OrdenServicio + "</b></div>");
                htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">Nave </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Nave + "</b></div>");
                //htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">Puerto Destino </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.PuertoDestino + "</b></div>");
                //htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">Orden de Servicio </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.OrdenServicio + "</b></div>");
                //htmlShow.Append("</div>");
                //htmlShow.Append("</td>");

                ////tercera fila 
                //htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\" >ETD </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.ETD + "</b></div>");
                //htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">ETA </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b> " + lis.ETA + "</b></div>");
                //htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">Almacen Retiro </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.AlmacenRetiro + "</b></div>");
                //htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">Almacen Ingreso FULL </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.AlmacenIngreo + "</b></div>");
                //htmlShow.Append("</div>");
                //htmlShow.Append("</td>");


                //cuarta fila 
                //htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\" >Servicio </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.NombreServicio + "</b></div>");
                //htmlShow.Append("</div>");

                ////htmlShow.Append("<div class=\"row\">");
                ////htmlShow.Append("<div class=\"col-6\">Mov. Aduana </div>");
                ////htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Aduana + "</b></div>");
                ////htmlShow.Append("</div>");

                ////htmlShow.Append("<div class=\"row\">");
                ////htmlShow.Append("<div class=\"col-6\">Tipo Embarque </div>");
                ////htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Embarque + "</b></div>");
                ////htmlShow.Append("</div>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-6\">Conceptos OP </div>");
                //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.ConceptosOPL + "</b></div>");
                //htmlShow.Append("</div>");
                //htmlShow.Append("</td>");


                //quinta fila 
                htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-12 text-center\" > <b>UBICACIÓN ACTUAL</b> </div>");
            htmlShow.Append("</div>");

            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-12 text-center\">Estado: <i class=\"fas fa-hourglass-half\"></i> " + "Activo" + " </div>");
            htmlShow.Append("</div>");

            //htmlShow.Append("<div class=\"row\">");
            //htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='Editar({0});'>Editar</a>", lis.IdOperacionLogistica);
            //htmlShow.Append("</div>");

            htmlShow.Append("<div class=\"row\">");
            htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='VerMapa({0});'>Ver en mapa</a>", 1);
            htmlShow.Append("</div>");
                if (lis.IdOperacionLogistica > 0)
                {
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='holamundo({0});'>Ver detalles</a>", lis.IdOperacionLogistica);
                    //htmlShow.AppendFormat("<button onclick=\" holamundo(" + lis.IdOperacionLogistica + "); return false; \" class=\"btn btn-primary float-right\">Ver Detalles</button>");
                    htmlShow.Append("</div>");

                }
                else
                {
                    
                }


                htmlShow.Append("</td>");
            #endregion
            htmlShow.Append("</tr>");
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");



        }


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
        [HttpPost]
        public async Task<ActionResult> MisSolicitudesLogistica(Parametros_Lista_Maritimo param,string tipousuario)
        {
            string htmlGrid = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: validar usuario sea customer.
            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                param.Codigo_Usuario = Convert.ToString(idCliente);
            }
            else
            {
                if (tipousuario != "customer")
                {
                    param.Codigo_Usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    param.Codigo_Usuario = "";
                }
            }

            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == "Todas")
            {
                param.Campana = "";
            }
            if (param.cod_puerto_destino == null)
            {
                param.cod_puerto_destino = "0";
            }
            if (param.cod_puerto_salida == null)
            {
                param.cod_puerto_salida = "0";
            }
            if (param.Campana == null)
            {
                param.Campana = "";
            }


            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                List<Mis_Solicitudes_Logisticas> ListaTemporal = new List<Mis_Solicitudes_Logisticas>();
                List<Mis_Solicitudes_Logisticas> ListaPrincipal = new List<Mis_Solicitudes_Logisticas>();
                param.ListadoUsuarios = new List<string>();

                if (param.Codigo_Usuario == "")
                {
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientesPorCustomer(correoCustomerServiceSecundario: HttpContext.User.Identity.Name);

                    var listaLimpia = listarClientes.Where(x => x.Correo != null && x.Correo != "").ToList().Select(x => x.Correo).ToList();

                    param.ListadoUsuarios.AddRange(listaLimpia);
                    var res1 = BL.BL_MisSolicitudes_Logistica(param);
                    ListaTemporal = (List<Mis_Solicitudes_Logisticas>)res1;
                    ListaPrincipal.AddRange(ListaTemporal);

                    //foreach (var cliente in listarClientes)
                    //{
                    //    if (cliente.Correo != null && cliente.Correo != "")
                    //    {
                    //        param.Codigo_Usuario = cliente.Correo;
                    //        var res1 = BL.BL_MisSolicitudes_Logistica(param);
                    //        ListaTemporal = (List<Mis_Solicitudes_Logisticas>)res1;
                    //        ListaPrincipal.AddRange(ListaTemporal);
                    //    }
                    //}
                }
                else
                {
                    param.ListadoUsuarios.Add(param.Codigo_Usuario);
                    var res2 = BL.BL_MisSolicitudes_Logistica(param);
                    ListaPrincipal = (List<Mis_Solicitudes_Logisticas>)res2;
                }
                htmlGrid = crearGridShowSolicitudesLogistica(ListaPrincipal);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/MisSolicitudesLogistica");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShowSolicitudesLogistica(List<Mis_Solicitudes_Logisticas> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            //List<Mis_Solicitudes_Logisticas> lista = (List<Mis_Solicitudes_Logisticas>)list;

            List<string> campana = new List<string>();

            if(lista.Count > 0)
            {
                foreach (var lis in lista)
                {
                    #region html

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-2\" >");
                    htmlShow.Append("<h4> " + lis.Campania + "</h4> Campaña");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.Append("<h4> " + Convert.ToInt32(lis.IdCotizacionLogistica) + "</h4>" + "Nro Cotización");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-6\">");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.Append("Estado <i class=\"fas fa-exclamation-circle\"></i>" + lis.Estado);
                    htmlShow.Append("</div>");

                    htmlShow.Append("</div>");

                    htmlShow.Append("<table class=\"table table-hover\" >");
                    htmlShow.Append("<tbody>");
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<th>Servicio</th>");
                    htmlShow.Append("<th>Origen</th>");
                    //htmlShow.Append("<th>Destino</th>");
                    //htmlShow.Append("<th>Nro Servicio</th>");

                    htmlShow.Append("<th style=\"text-align: right\">");

                    htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick=\"VerDetalle({0},'{1}');\">Ver Detalle</a>", lis.IdCotizacionLogistica, lis.NombreAlmacenPacking);
                    if (lis.IdEstado >= 2)
                    {
                        htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='DescargarPDFCotizacion({0});'>Descargar PDF</a>", lis.IdCotizacionOdoo);
                    }
                    htmlShow.Append("</th>");
                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td>Operación Logística");
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td>" + lis.NombrePuertoCarga);
                    htmlShow.Append("</td>");

                    //htmlShow.Append("<td>" + lis.Almacen_Retiro);
                    //htmlShow.Append("</td>");

                    //htmlShow.Append("<td>" + lis.NroServicio);
                    //htmlShow.Append("</td>");


                    htmlShow.Append("<td> ");
                    htmlShow.Append("</td>");

                    htmlShow.Append("</tr>");
                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</div>");

                    htmlShow.Append(" <div class=\"card-footer\">");
                    if (lis.IdEstado == 2)
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-8\">");
                        htmlShow.Append("</div>");

                        htmlShow.Append("<div class=\"col-2\">");
                        htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:green\" class=\"btn btn-success float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Logistica({0},{1}); \">Confirmar </button>", lis.IdCotizacionLogistica, 3);
                        htmlShow.Append("</div>");

                        htmlShow.Append("<div class=\"col-2\">");
                        htmlShow.AppendFormat(" <button type=\"button\" style=\"background: transparent; color: black; text - transform: uppercase; border - radius: 6px; border - color:red\" class=\"btn btn-danger float-right btn-lg btn-block\" onclick=\"Confirmar_Rechazar_Logistica({0},{1}); \">Rechazar </button>", lis.IdCotizacionLogistica, 4);
                        htmlShow.Append("</div>");


                        htmlShow.Append("</div>");
                    }
                    //else if (lis.IdEstado == 3)
                    //{
                    //    htmlShow.Append(" <button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"RealizarBooking(); \"> Solicitar booking</button>");
                    //}
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    #endregion
                }
            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }
          



            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
        [HttpPost]
        public ActionResult ListaSeguimiento(int id)
        {
            string htmlGrid = "";
            try
            {
                BL_Seguimiento BL = new BL_Seguimiento(_configuration);
                var res = BL.BL_SeguimientoOPL(id);
                htmlGrid = crearGridShowSeguimiento(res);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/ListaSeguimiento");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShowSeguimiento(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<SeguimientoOPL> lista = (List<SeguimientoOPL>)list;


            //Ent_Reserva ent = (Ent_Reserva)lis;

            #region html
            htmlShow.Append("<div class=\"container-fluid\">");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-md-12\">");
            htmlShow.Append("<div class=\"card\">");

            htmlShow.Append("<div class=\"card-header\">");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-12\">");
            htmlShow.Append("<h4> Seguimiento Flete </h4>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");


            htmlShow.Append("<div class=\"card-body\">");
            htmlShow.Append("<table class=\"table table-hover\" style=\"border:none\">");
            htmlShow.Append("<tbody>");

            foreach (var liss in lista)
            {
                htmlShow.Append("<tr>");
                htmlShow.Append("<td style=\"border:none\">");
                htmlShow.Append("<div class=\"row\">");

                //htmlShow.Append("<div class=\"col-1\">" + liss.CodEstado);
                //htmlShow.Append("</div>");
                if (liss.CodEstado.Equals("on-my-way"))
                {
                    htmlShow.Append("<div style=\"padding-left: 6%;\" >  <i class=\"fas fa-truck-moving fa-8x\"></i>");
                    htmlShow.Append("</div>");
                }else if (liss.CodEstado.Equals("arrival-cl")){
                    htmlShow.Append("<div style=\"padding-left: 6%;\"> <i class=\"fas fa-house-user fa-8x\"></i>");
                    htmlShow.Append("</div>");
                }
                else if (liss.CodEstado.Equals("delivered"))
                {
                    htmlShow.Append("<div style=\"padding-left: 6%;\"> <i class=\"fas fa-people-carry fa-8x\"></i>");
                    htmlShow.Append("</div>");
                }
                else{
                    htmlShow.Append("<div style=\"padding-left: 6%;\"> <i class=\"fas fa-user-secret fa-8x\"></i>");
                    htmlShow.Append("</div>");
                }



                htmlShow.Append("<div class=\"col-3\" style=\"padding-left: 10%; padding-top: 3%;\"> <b>Estado: </b> " + liss.DescEstado);
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-4\" style=\"padding-left: 10%; padding-top: 3%;\"> <b>Fecha : </b> " + liss.FechaIni);
                htmlShow.Append("</div>");
                //htmlShow.Append("<div class=\"col-4\"> Fecha Fin: " + liss.FechaFin);
                //htmlShow.Append("</div>");


                htmlShow.Append("</div>");
                htmlShow.Append("</td>");
                htmlShow.Append("</tr>");
            }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");
            htmlShow.Append("</div>");


            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult TablaDetalleLogistica(int id)
        {
            string htmlGrid = "";

            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                var res = BL.BL_TablaDetalleLogistica(id);
                htmlGrid = crearGridDetalleLogistica(res);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/TablaDetalleLogistica");
                htmlGrid = "";
            }
            return Ok(htmlGrid);
        }
        public string crearGridDetalleLogistica(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<TablaDetalleLogistica> lista = (List<TablaDetalleLogistica>)list;


            #region html
            htmlShow.Append("<table class=\"table table-bordered table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
            htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
            htmlShow.Append("<tr>");
            //htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">REGIÓN</th>");
            htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">LINEA</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">PUERTO CARGA</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">SERVICIO</th>");
            htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">TERMINAL</th>");
            htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">PACKING</th>");
            htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">COMMODITY</th>");
            htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">PRECIO OPL</th>");

            htmlShow.Append("</tr>");

            foreach (var lis1 in lista)
            {
                htmlShow.Append("<tr>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreNaviera + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombrePuertoCarga + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreServicio + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreTerminalRetiroVacio + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreAlmacenPacking + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreCommodity + "</td>");
                htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Flete + "</td>");

                htmlShow.Append("</tr>");
            }
            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public async Task<ActionResult> ListaAprobadosLogistica(Parametros_Lista_Maritimo param,string tipousuario)
        {
            string htmlGrid = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: validar usuario sea customer.
            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                param.Codigo_Usuario = Convert.ToString(idCliente);
            }
            else
            {
                if (tipousuario != "customer")
                {
                    param.Codigo_Usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    param.Codigo_Usuario = "";
                }
            }
            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == null)
            {
                param.Campana = "";
            }

          

            param.cod_puerto_destino = param.cod_puerto_destino == null ? "0" : param.cod_puerto_destino;
            param.cod_puerto_salida = param.cod_puerto_salida == null ? "0" : param.cod_puerto_salida;
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                List<ListaCotizacionesLogisticasAprobadas> ListaTemporal = new List<ListaCotizacionesLogisticasAprobadas>();
                List<ListaCotizacionesLogisticasAprobadas> ListaPrincipal = new List<ListaCotizacionesLogisticasAprobadas>();
                param.ListadoUsuarios = new List<string>();

                if (param.Codigo_Usuario == "")
                {
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService2: HttpContext.User.Identity.Name);
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientesPorCustomer(correoCustomerServiceSecundario: HttpContext.User.Identity.Name);


                    var listaLimpia = listarClientes.Where(x => x.Correo != null && x.Correo != "").ToList().Select(x => x.Correo).ToList();

                    param.ListadoUsuarios.AddRange(listaLimpia);
                    var res1 = BL.BL_ListaCotizacionAprobadasLogistica(param);
                    ListaTemporal = (List<ListaCotizacionesLogisticasAprobadas>)res1;
                    ListaPrincipal.AddRange(ListaTemporal);

                    //foreach (var cliente in listarClientes)
                    //{
                    //    if (cliente.Correo != null && cliente.Correo != "")
                    //    {
                    //        param.Codigo_Usuario = cliente.Correo;
                    //        var res1 = BL.BL_ListaCotizacionAprobadasLogistica(param);
                    //        ListaTemporal = (List<ListaCotizacionesLogisticasAprobadas>)res1;
                    //        //if (param.cod_line_navi != "TODAS LAS NAVIERAS")
                    //        //{
                    //        //    ListaTemporal = ListaTemporal.Where(x => x.NombreNaviera.ToLower().Trim() == param.cod_line_navi.ToLower().Trim()).ToList();
                    //        //}
                    //        ListaPrincipal.AddRange(ListaTemporal);
                    //    }
                    //}
                }
                else
                {
                    param.ListadoUsuarios.Add(param.Codigo_Usuario);
                    var res2 = BL.BL_ListaCotizacionAprobadasLogistica(param);
                    ListaPrincipal = (List<ListaCotizacionesLogisticasAprobadas>)res2;
                    //if (param.cod_line_navi != "TODAS LAS NAVIERAS")
                    //{
                    //    ListaPrincipal = ListaPrincipal.Where(x => x.NombreNaviera.ToLower().Trim() == param.cod_line_navi.ToLower().Trim()).ToList();
                    //}
                }

                htmlGrid = crearGridShowLogisticaAprobadas(ListaPrincipal);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/ListaAprobadosLogistica");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShowLogisticaAprobadas(List<ListaCotizacionesLogisticasAprobadas> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");
            OdooService odooService = new OdooService(_configuration, _memoryCache);
            
            List<string> campana = new List<string>();
            if(lista.Count > 0)
            {
                foreach (var lis in lista)
                {
                    #region html
                    //List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
                    //Ent_Reserva ent = (Ent_Reserva)lis;
                    if (campana.Contains(lis.NombreCampania))
                    {

                    }
                    else
                    {
                        campana.Add(lis.NombreCampania);

                        //campana.Where(x => x.)
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-md-12\">");
                        htmlShow.Append("<div class=\"card\">");
                        htmlShow.Append("<div class=\"card-body\">");

                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-2\" >");
                        htmlShow.Append("<h4> " + lis.NombreCampania + "</h4> Campaña");
                        htmlShow.Append("</div>");

                        htmlShow.Append("</div>");

                        htmlShow.Append("<table class=\"table table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
                        htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
                        htmlShow.Append("<tr>");
                        htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">FECHA APROB</th>");
                        htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\"># COTIZACION</th>");
                        htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">LINEA</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">PUERTO CARGA</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">SERVICIO</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">TERMINAL</th>");
                        htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">PACKING</th>");
                        htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">DEPOSITO  VACIO</th>");
                        htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">COSTO OPL</th>");
                        htmlShow.Append("</tr>");

                        foreach (var lis1 in lista.Where(x => x.NombreCampania == lis.NombreCampania))
                        {
                            //var lisss = odooService.ListarDepositoVaciosPorNaviera(lis1.IdNaviera,lis1.IdPuertoCarga);
                            //string NombreDepositoVacio = lisss.Where(x => x.Id == lis1.IdTerminalDepositoVacio).FirstOrDefault().Nombre;
                            htmlShow.Append("<tr style=\"border-bottom: 1px solid #ccc; border-left: 1px solid #ccc; border-right: 1px solid #ccc;\">");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.FechaAprobacion + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.IdCotizacionLogistica + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreNaviera + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombrePuertoCarga + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreServicio + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreTerminalRetiroVacio + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreAlmacenPacking + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NombreTerminalDepositoVacio + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\"> <b>" + lis1.Flete + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\"> <button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"VistaAgregarOP( " + lis1.IdCotizacionDetalleOdoo + "); \"> Solicitar OPL</button> </td>");
                            htmlShow.Append("</tr>");
                        }
                        htmlShow.Append("</tbody>");
                        htmlShow.Append("</table>");
                        htmlShow.Append("</div>");

                        htmlShow.Append(" <div class=\"card-footer\">");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");

                    }
                }
            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }
           


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
        #endregion








    }
}
