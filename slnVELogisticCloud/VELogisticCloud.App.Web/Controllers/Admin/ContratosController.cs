﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class ContratosController : Controller
    {
        //Contratos
        private readonly ILogger _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public ContratosController(ILogger<ContratosController> logger, UserManager<ApplicationUser> userManager, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _userManager = userManager;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        //private static List<Contrato> Static_listaContratos;
        private static List<ContratoDetalle> Static_listaContratosDetalle;
        public static int IdNaviera;
        public IActionResult MisContratos()
        {
            ModelCombos model = new ModelCombos();
            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaCommodity = listaCommodity;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ListaNavieras(string tipousuario)
        {
            string htmlGrid = "";
            try
            {
                var correo = HttpContext.User.Identity.Name;

                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    correo = Convert.ToString(idCliente);
                }
                else
                {
                    if (tipousuario != "customer")
                    {
                        correo = HttpContext.User.Identity.Name;
                    }
                    else
                    {
                        correo = "";
                    }
                }

                List<ContratoDetalle> listaContratos = new OdooService(_configuration, _memoryCache).ListarContratos(correo, 0, 0, 0, 0, 0,10000, true);

                Static_listaContratosDetalle = listaContratos;
                if (listaContratos.Count > 0)
                    htmlGrid = crearGridShow(listaContratos);
                else
                    htmlGrid = "0";
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Contratos/ListaNavieras");
            }
            return Ok(htmlGrid);
        }
        public string crearGridShow(List<ContratoDetalle> listaNaviera)
        {
            StringBuilder htmlShow = new StringBuilder();


            #region html

            //htmlShow.Append("<div class=\"container-fluid\">");
            //htmlShow.Append("<div class=\"row\">");
            //htmlShow.Append("<div class=\"col-3\">");
            //htmlShow.Append("<div class=\"list-group\">");
            var listaDistinct = listaNaviera.Select( x=>x.IdNaviera).Distinct().ToList();

            foreach (var lis in listaDistinct)
            {
                ContratoDetalle contrato = new ContratoDetalle();
                contrato = listaNaviera.Where(x => x.IdNaviera == lis).FirstOrDefault();
                    htmlShow.Append("<a href=\"#\" onclick=\"ListaContratos( " + contrato.IdNaviera + ",0,0 )\"  class=\"list-group-item list-group-item-action\">" + contrato.NombreNaviera + "<i class=\"fas fa-arrow-right float-right\"></i></a>");
            }
            #endregion
            return htmlShow.ToString();
        }

        [HttpPost]
        public ActionResult ListaContratos(int idNaviera)
        {
            string htmlGrid = "";
            try
            {
                IdNaviera = idNaviera;
                var correo = HttpContext.User.Identity.Name;
                List<ContratoDetalle> lista = new List<ContratoDetalle>();
                lista = Static_listaContratosDetalle;
                //lista = TempData["_listaContratos"] as List<Contrato>;
                //TempData["ListaContratos"] = new OdooService(_configuration).ListarContratos(correo, idCampania: 0, idNaviera: 0, idPuertoOrigen: 0, idPuertoDestino: 0, 0, 100000);
                lista = lista.Where(x => x.IdNaviera == idNaviera).ToList();
                //Static_listaContratosDetalle = lista[0].ListaContratoDetalle;
                //lista = new OdooService(_configuration).ListarContratos(correo, idCampania: 0 , idNaviera: idNaviera, idPuertoOrigen: 0, idPuertoDestino: 0, 0, 100000);

                htmlGrid = crearGridShowContratos(lista);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Contratos/ListaContratos");
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public ActionResult BusquedaContratos(int idPuertoOrigen, int idPuertoDestino,string Estado)
        {
            string htmlGrid = "";
            try
            {
                var correo = HttpContext.User.Identity.Name;
                List<ContratoDetalle> lista = new List<ContratoDetalle>();
                lista = Static_listaContratosDetalle;
                //if (!string.IsNullOrEmpty(Estado))
                //{
                //    lista= lista.Where( x=>x.Estado.Equals(Estado)).ToList();

                //}

                var listaDetalle = Static_listaContratosDetalle;

                //lista = Static_listaContratos.Where(x => x.IdNaviera == IdNaviera && x. == idPuertoOrigen && idPuertoDestino == idPuertoDestino).ToList();
                //if (Estado != "" )
                //{
                    lista = lista.Where(x => x.IdNaviera == IdNaviera).ToList();
                    if (idPuertoOrigen == 0 && idPuertoDestino == 0)
                    {
                        listaDetalle = listaDetalle.ToList();
                    }
                    if (idPuertoOrigen > 0 && idPuertoDestino == 0)
                    {
                        listaDetalle = listaDetalle.Where(x=>x.IdPuertoCarga == idPuertoOrigen).ToList();
                    }

                    if (idPuertoOrigen == 0 && idPuertoDestino > 0)
                    {
                        listaDetalle = listaDetalle.Where(x => x.IdPuertoDescarga == idPuertoDestino).ToList();
                    }
                    if (idPuertoOrigen > 0 && idPuertoDestino > 0)
                    {
                        listaDetalle = listaDetalle.Where(x => x.IdPuertoDescarga == idPuertoDestino && x.IdPuertoCarga == idPuertoOrigen).ToList();
                    }
                //}
                //else
                //{
                //    //lista = lista.Where(x => x.IdNaviera == IdNaviera).ToList();
                //    listaDetalle = listaDetalle.Where(x => x.IdPuertoCarga == idPuertoOrigen && x.IdPuertoDescarga == idPuertoDestino).ToList();
                //    //lista = Static_listaContratos.Where(x => x.IdNaviera == IdNaviera && x. == idPuertoOrigen && idPuertoDestino == idPuertoDestino).ToList();
                //}
                //lista = new OdooService(_configuration).ListarContratos(correo, idCampania: 0, idNaviera: IdNaviera, idPuertoOrigen: idPuertoOrigen, idPuertoDestino: idPuertoDestino, Estado, 0, 100000);
                //lista = listaDetalle;
                htmlGrid = crearGridShowContratos(listaDetalle);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Contratos/BusquedaContratos");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShowContratos(List<ContratoDetalle> listaContrato)
        {
            StringBuilder htmlShow = new StringBuilder();


            #region html
            string nroContrato = listaContrato[0].NumeroContrato;
            htmlShow.Append("<div class=\"card\">");
            htmlShow.Append("<div class=\"card-header\">");
            htmlShow.Append("<h3>" + listaContrato[0].NombreNaviera + "</h3>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");

            if(listaContrato.Count > 0)
            {
                foreach (var lis in listaContrato)
                {
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");
                    htmlShow.Append("<div class=\"row\">");

                    htmlShow.Append("<div class=\"col-2\">");
                    htmlShow.Append("<h4>" + lis.NumeroContrato + "</h4> Numero contrato");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"col-8 text-right\">");
                    htmlShow.Append("Commodity/campaña: <b>" + lis.NombreCampania + "</b>");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"col-2 text-center\">");
                    //htmlShow.Append(" Estado: " + lis.Estado);
                    //htmlShow.Append("</div>");

                    htmlShow.Append("</div>");

                    htmlShow.Append("<table class=\"table table-hover\">");
                    htmlShow.Append("<tbody>");

                    foreach (var lis2 in listaContrato)
                    {

                        htmlShow.Append("<tr>");

                        htmlShow.Append("<td>");
                        htmlShow.Append("<div class=\"form-check\" style=\"display:none \" >");
                        htmlShow.Append("<input type=\"checkbox\" class=\"form-check-input\">");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</td>");

                        htmlShow.Append("<td><b>" + lis2.NombrePuertoCarga + "</b></td>");
                        htmlShow.Append("<td><i class=\"fas fa-chevron-right\"></i></td>");
                        htmlShow.Append("<td><b>" + lis2.NombrePuertoDescarga + "</b></td>");
                        htmlShow.Append("<td>TT: <b>" + lis2.TT + " dias</b></td>");
                        htmlShow.Append("<td>Flete BL: <b>$" + lis2.FleteBL + "</b></td>");

                        htmlShow.Append("</tr>");
                    }
                    htmlShow.Append("</table>");
                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"card-footer\" style=\"display:none \">");
                    htmlShow.Append("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"location.href = 'itinerario-solicitar.html'; \">Solicitar booking</button>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                }

            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }
            
            //htmlShow.Append("<div class=\"container-fluid\">");
            //htmlShow.Append("<div class=\"row\">");
            //htmlShow.Append("<div class=\"col-3\">");
            //htmlShow.Append("<div class=\"list-group\">");




            //htmlShow.Append("</div>");
            //htmlShow.Append("</div>");


            #endregion

            return htmlShow.ToString();

        }

        public string crearGridShowBusqueda(List<Contrato> listaContrato, int idPuertoOrigen, int idPuertoDestino)
        {
            StringBuilder htmlShow = new StringBuilder();


            #region html
            string nroContrato = listaContrato[0].NombreContrato;

            foreach (var lis in listaContrato)
            {
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("<div class=\"row\">");

                htmlShow.Append("<div class=\"col-2\">");
                htmlShow.Append("<h4>" + lis.NombreContrato + "</h4> Numero contrato");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"col-8 text-right\">");
                htmlShow.Append("Commodity/campaña: <b>" + lis.NombreCampania + "</b>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"col-2 text-center\">");
                htmlShow.Append(" Estado: Activo");
                htmlShow.Append("</div>");

                htmlShow.Append("</div>");

                htmlShow.Append("<table class=\"table table-hover\">");
                htmlShow.Append("<tbody>");

                foreach (var lis2 in listaContrato[0].ListaContratoDetalle.Where(x => x.IdPuertoCarga == idPuertoOrigen && x.IdPuertoDescarga == idPuertoDestino))
                {

                    htmlShow.Append("<tr>");

                    htmlShow.Append("<td>");
                    htmlShow.Append("<div class=\"form-check\">");
                    htmlShow.Append("<input type=\"checkbox\" class=\"form-check-input\">");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td><b>" + lis2.NombrePuertoCarga + "</b></td>");
                    htmlShow.Append("<td><i class=\"fas fa-chevron-right\"></i></td>");
                    htmlShow.Append("<td><b>" + lis2.NombrePuertoDescarga + "</b></td>");
                    htmlShow.Append("<td>TT: <b>" + lis2.TT + " dias</b></td>");
                    htmlShow.Append("<td>Flete BL: <b>$" + lis2.FleteBL + "</b></td>");

                    htmlShow.Append("</tr>");
                }
                htmlShow.Append("</table>");
                htmlShow.Append("</tbody>");
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"card-footer\">");
                htmlShow.Append("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"location.href = 'itinerario-solicitar.html'; \">Solicitar booking</button>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }

            //htmlShow.Append("<div class=\"container-fluid\">");
            //htmlShow.Append("<div class=\"row\">");
            //htmlShow.Append("<div class=\"col-3\">");
            //htmlShow.Append("<div class=\"list-group\">");




            //htmlShow.Append("</div>");
            //htmlShow.Append("</div>");


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
    }

}