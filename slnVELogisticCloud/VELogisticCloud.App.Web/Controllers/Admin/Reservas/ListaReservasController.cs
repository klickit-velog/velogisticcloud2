﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Servicio.Business.Reservas;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.Admin.Reservas
{
    public class ListaReservasController : Controller
    {
        private static List<ContratoDetalle> Static_listaContratos;

        private static List<Ent_Reserva> listaContenedor = new List<Ent_Reserva>();

        private static decimal idplantilla ;
        private static decimal idplantilla_editar;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public ListaReservasController(ILogger<ListaReservasController> logger, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        public IActionResult Index()
        {
            return View();
        }


        public IActionResult CreateReserva_Plantilla()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));


            var ListaCliente = new List<SelectListItem>();
            var Cliente = new SelectListItem();
            var correo = HttpContext.User.Identity.Name;
            var datos = new OdooService(_configuration, _memoryCache).getCliente(correo);
            Cliente.Value = datos.Id.ToString();
            Cliente.Text = datos.Nombre;
            Cliente.Selected = true;
            ListaCliente.Add(Cliente);

            model.ListaClientes = ListaCliente;
            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;

            return View(model);
        }

        [HttpGet]
        public IActionResult EditarReserva()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            //var ListaCliente = new List<SelectListItem>();
            //var Cliente = new SelectListItem();
            //var correo = HttpContext.User.Identity.Name;
            //var datos = new OdooService(_configuration).getCliente(correo);
            //Cliente.Value = datos.Id.ToString();
            //Cliente.Text = datos.Nombre;
            //Cliente.Selected = true;
            //ListaCliente.Add(Cliente);

            //model.ListaClientes = ListaCliente;
            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;

            return View(model);
        }

        [HttpPost]
        public ActionResult GuardarCodigo(decimal id)
        {
            idplantilla = id;

            return Ok("1");
          
        }

        [HttpPost]
        public ActionResult GuardarCodigo_EditarReserva(decimal id)
        {
            idplantilla_editar = id;

            return Ok("1");

        }

        //[HttpPost]
        //public ActionResult RecuperarDatosEditar(int id)
        //{
        //    try
        //    {
        //        BL_Reserva BL = new BL_Reserva();
        //        var res = BL.ReservexID(id, "", "", "", "", 0, "");
        //        List<Ent_Reserva> lstreserva = (List<Ent_Reserva>)res;
        //        Ent_Reserva reserva = (Ent_Reserva)lstreserva[0];
        //        bool isFechaETA = DateTime.TryParseExact(reserva.ETA, Formato.FechaGeneral, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime FechaETA);
        //        bool isFechaETD = DateTime.TryParseExact(reserva.ETD, Formato.FechaGeneral, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime FechaETD);

        //        //bool isFechaETD = DateTime.TryParse(reserva.ETD, out DateTime FechaETD);
        //        if (isFechaETD)
        //        {
        //            reserva.ETD = DateTime.ParseExact(reserva.ETD, Formato.FechaGeneral, null).ToString(Formato.FechaAnioMesDia);
        //        }else
        //        {
        //            reserva.ETD = string.Empty;
        //        }
        //        //bool isFechaETA = DateTime.TryParse(reserva.ETA, out DateTime FechaETA);
        //        if (isFechaETA)
        //        {
        //            reserva.ETA = DateTime.ParseExact(reserva.ETA, Formato.FechaGeneral, null).ToString(Formato.FechaAnioMesDia);
        //        }
        //        else
        //        {
        //            reserva.ETA = string.Empty;
        //        }

        //        return Ok(reserva);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ListaReservas/RecuperarDatosEditar");
        //        return Ok("0");
        //    }
        //}


        //[HttpPost]
        //public ActionResult ValidarContrato(string contrato,int idReserva)
        //{
        //    try
        //    {

        //        var correo = HttpContext.User.Identity.Name;
        //        int IdCotizacionDetalleOdoo = 0;
        //        //var correo = HttpContext.User.Identity.Name;
        //        if(Static_listaContratos == null || Static_listaContratos.Count < 1)
        //        {
        //            List<ContratoDetalle> listaContratos = new OdooService(_configuration).ListarContratos(correo, 0, 0, 0, 0, 0, 100000, true);
        //            Static_listaContratos = listaContratos;
        //        }


        //        var existe = Static_listaContratos.Where(x => x.NumeroContrato == contrato.Trim()).ToList();

        //        if (existe != null)
        //        {
        //            BL_Reserva BL = new BL_Reserva();
        //            //var data = BL.BookingTOoddo(idReserva);
        //            List<BookingTOoddo> bookingTOoddos = (List<BookingTOoddo>)data;
        //            BookingTOoddo bookingTOoddo = (BookingTOoddo)bookingTOoddos[0];
        //            int Id_Puerto_Carga = bookingTOoddo.Id_Puerto_Carga;
        //            int Id_Puerto_Descarga = bookingTOoddo.Id_Puerto_Descarga;

        //           var b = existe.Where(x => x.IdPuertoCarga == Id_Puerto_Carga && x.IdPuertoDescarga == Id_Puerto_Descarga).FirstOrDefault();
        //            if(b != null)
        //            {
        //                IdCotizacionDetalleOdoo = b.IdCotizacionDetalleOdoo;
        //            }

        //        }

        //        return Ok(IdCotizacionDetalleOdoo);

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError( ex,"Error ListaReservas/ValidarContrato");
        //        return Ok(0);
        //    }
        //}

        //[HttpPost]
        //public ActionResult RecuperarDatosPlantilla()
        //{
        //    try
        //    {
        //        BL_Reserva BL = new BL_Reserva();
        //        var res = BL.ReservexID(idplantilla, "", "", "", "",0,"");
        //        List<Ent_Reserva> lstreserva = (List<Ent_Reserva>)res;
        //        Ent_Reserva reserva = (Ent_Reserva)lstreserva[0];
        //        return Ok(reserva);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ListaReservas/RecuperarDatosPlantilla");
        //        return Ok("");
        //    }
        //}


        [HttpPost]
        public ActionResult ListaReservas(Ent_Reserva ent)
        {

            if (ent.Commodity == null)
            {
                ent.Commodity = "";
            }
            if (ent.Naviera == null)
            {
                ent.Naviera = "";
            }
            if (ent.Puerto_Descarga == null)
            {
                ent.Puerto_Descarga = "";
            }
            if (ent.Puerto_Embarque == null)
            {
                ent.Puerto_Embarque = "";
            }
            if (ent.Nro_Booking == null)
            {
                ent.Nro_Booking = "";
            }
            //Resultado retorno = new Resultado();
            string htmlGrid = "";

            try
            {
                //BL_Reserva BL = new BL_Reserva();
                //var  res = BL.ReservexID(0, ent.Commodity, ent.Naviera, ent.Puerto_Descarga, ent.Puerto_Embarque, ent.Id_Estado_Reserva,ent.Nro_Booking); 
                //htmlGrid = crearGridShow(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error ListaReservas/ListaReservas");
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public ActionResult Delete_Reserva(decimal id)
        {

            //Resultado retorno = new Resultado();
            string htmlGrid = "";

            try
            {
                //BL_Reserva BL = new BL_Reserva();
                //var res = BL.Delete_Reserva(id);
                //htmlGrid = crearGridShow(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error ListaReservas/Delete_Reserva");
            }
            return Ok(htmlGrid);
        }


        //public string crearGridShow(object listObras)
        //{
        //    StringBuilder htmlShow = new StringBuilder();
        //    //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

        //    List<Ent_Reserva> lista =(List<Ent_Reserva>)listObras;
        //    BL_Reserva BL = new BL_Reserva(_configuration);
        //    List<Ent_Navieras_Oddo_VE> lisNavierasImo = (List<Ent_Navieras_Oddo_VE>)BL.BL_ListaNavierasOdooVE();
        //    //var lisNavierasImo =  ;

        //    foreach (var lis in lista)
        //    {
        //        //Ent_Reserva ent = (Ent_Reserva)lis;

        //        #region html



        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-md-12\">");
        //        htmlShow.Append("<div class=\"card\">");
        //        htmlShow.Append("<div class=\"card-body\">");

        //        htmlShow.Append("<table class=\"table table-hover\">");
        //        htmlShow.Append("<tbody>");

        //        htmlShow.Append("<tr>");
        //        string nro_booking = lis.Nro_Booking == null ? " <i>[Sin Nro Booking]</i>" : lis.Nro_Booking; 
        //        string nro_id = lis.Id_Reserva.ToString();
        //        htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\"><h5>"+ "Nro. Booking: " +nro_booking + "</h5><h5>" + "Nro. Solicitud:" + nro_id + "</h5></td>");
        //        htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\" class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> "+lis.Estado_desc + "</td>");
        //        htmlShow.Append("</tr>");

        //        htmlShow.Append("<tr>");

        //        htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Cantidad contenedores:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Cantidad +"</b></div>");
        //        htmlShow.Append("</div>");

        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Origen:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Puerto_Embarque+"</b></div>");
        //        htmlShow.Append("</div>");

        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Nombre de nave:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Nombre_Nave+"</b></div>");
        //        htmlShow.Append("</div>");

        //        //htmlShow.Append("<div class=\"row\">");
        //        //htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
        //        //htmlShow.Append("<div class=\"col-6 text-right\"><b>ABC</b></div>");
        //        //htmlShow.Append("</div>");

        //        htmlShow.Append("</td>");


        //        htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Linea:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Naviera+"</b></div>");
        //        htmlShow.Append("</div>");

        //        //htmlShow.Append("<div class=\"row\">");
        //        //htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
        //        //htmlShow.Append("<div class=\"col-6 text-right\"><b></b></div>");
        //        //htmlShow.Append("</div>");

        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">ETD:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.ETD+"</b></div>");
        //        htmlShow.Append("</div>");

        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">ETA:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETA + "</b></div>");
        //        htmlShow.Append("</div>");

        //        htmlShow.Append("</td>");

        //        htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Fecha de recepcion:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETD + "</b></div>");
        //        htmlShow.Append("</div>");

        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Puerto embarque:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Puerto_Embarque +"</b></div>");
        //        htmlShow.Append("</div>");

        //        htmlShow.Append("<div class=\"row\">");
        //        htmlShow.Append("<div class=\"col-6\">Puerto descarga:</div>");
        //        htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Puerto_Descarga+"</b></div>");
        //        htmlShow.Append("</div>");

        //        //htmlShow.Append("<div class=\"row\">");
        //        //htmlShow.Append("<div class=\"col-6\">Descarga contenedor:</div>");
        //        //htmlShow.Append("<div class=\"col-6 text-right\"><b></b></div>");
        //        //htmlShow.Append("</div>");

        //        htmlShow.Append("</td>");



        //        htmlShow.Append("<td style=\"border-top:none;\">");
        //        //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>"); 
        //        htmlShow.AppendFormat("<a href=\"#\" onclick='PlantillaReserva({0});'>Utilizar como plantilla</a>", lis.Id_Reserva.ToString());
        //        htmlShow.Append("<br>");

        //        htmlShow.AppendFormat("<a href=\"#\" onclick='EditReserva({0});'>Modificar Reserva</a>", lis.Id_Reserva.ToString());
        //        htmlShow.Append("<br>");
        //        if(lis.Nro_Booking != null)
        //        {
        //            string imo = "0";
        //            var a= lisNavierasImo.Where(x => x.NombreNaviera == lis.Nombre_Nave).FirstOrDefault();
        //            if (a != null)
        //            {
        //                imo = a.IMO;
        //            }
                    
        //            htmlShow.AppendFormat("<a href=\"#\" onclick='VerMapa({0});'>Ver en Mapa</a>", imo);
        //            htmlShow.Append("<br>");
        //        }
                

        //        htmlShow.Append("</td>");


        //        htmlShow.Append("</tr>");

        //        htmlShow.Append("<tr>");
        //        htmlShow.Append("<td style=\"border-top:none;\"  colspan=\"4\" class=\"text - right\">");
        //        htmlShow.AppendFormat("<a href=\"#\" onclick='DeleteReserva({0});'>Cancelar booking</a>", lis.Id_Reserva.ToString());
        //        //htmlShow.AppendFormat("<img src='/Imagenes/delete.png'  title='Eliminar Cotización'  onclick='DeleteReserva({0});'  style='cursor: hand' > &nbsp;&nbsp;"

        //        htmlShow.AppendFormat("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../ResumenReserva/Index?id={0}'; \">Ver Detalles</button>", lis.Id_Reserva.ToString());

        //        htmlShow.Append("</tr>");



        //        htmlShow.Append("</tbody>");
        //        htmlShow.Append("</table>");
        //        htmlShow.Append("</div>");
        //        htmlShow.Append("</div>");
        //        htmlShow.Append("</div>");
        //        htmlShow.Append("</div>");

        //        #endregion
        //    }

        //    //if (listObras != null && listObras.Count > 0)
        //    //{

         
        //    return htmlShow.ToString();

        //}



        //[HttpPost]
        //public ActionResult InsertarDatos(Ent_Reserva ent )
        //{
        //    if (ent.Consignatario == null || ent.Consignatario == "undefined")
        //    {
        //        ent.Consignatario = string.Empty;
        //    }
        //    try
        //    {
        //        BL_Reserva BL = new BL_Reserva();

        //        foreach (var lis in listaContenedor)
        //        {
        //            ent.Cantidad = lis.Cantidad;
        //            ent.Commodity = lis.Commodity;
        //            ent.Descripcion_Carga = lis.Descripcion_Carga;
        //            ent.Tipo_Contenedor = lis.Tipo_Contenedor;
        //            ent.Nota = "0";
        //            ent.Codigo_Usuario = HttpContext.User.Identity.Name;
        //            //var res = BL.InsertReserva(ent);

        //        }
        //        return Ok("1");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ListaReservas/InsertarDatos");
        //        return Ok("");
        //    }

        //}

        //[HttpPost]
        //public ActionResult InsertarPlantilla(Ent_Reserva ent)
        //{
        //    ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
        //    string result = "";

        //    try
        //    {
        //        BL_Reserva BL = new BL_Reserva();
        //        var res = BL.InsertReserva(ent);
        //        result = (string)res;
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ListaReservas/InsertarPlantilla");
        //        return Ok("0");
        //    }

        //}

        //[HttpPost]
        //public ActionResult UpdateDatos(Ent_Reserva ent,int id)
        //{
        //    ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
        //    string result = "";
        //    try
        //    {
        //        if(ent.Consignatario == null || ent.Consignatario == "undefined")
        //        {
        //            ent.Consignatario =string.Empty;
        //        }

        //        BL_Reserva BL = new BL_Reserva();
        //        var res = BL.UpdateReserva(id, ent);
        //        result = id.ToString();
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ListaReservas/UpdateDatos");
        //        return Ok("");
        //    }
        //}



        [HttpPost]
        public ActionResult Inicializar()
        {

            try
            {
                listaContenedor = new List<Ent_Reserva>();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error ListaReservas/Inicializar");
            }
            return Ok("");

        }
        [HttpPost]
        public ActionResult ListaContenedores(Ent_Reserva ent)
        {
            if (ent.Cantidad == null)
            {
                ent.Cantidad = "";
            }
            if (ent.Tipo_Contenedor == null)
            {
                ent.Tipo_Contenedor = "";
            }
            if (ent.Commodity == null)
            {
                ent.Commodity = "";
            }
            if (ent.Descripcion_Carga == null)
            {
                ent.Descripcion_Carga = "";
            }
            //Resultado retorno = new Resultado();
            string htmlGrid = "";
            try
            {
                htmlGrid = crearGridShow_ListaContenedores(ent);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {

            }
            return Ok(htmlGrid);
        }


        public string crearGridShow_ListaContenedores(object ent)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            Ent_Reserva ent2 = (Ent_Reserva)ent;
            listaContenedor.Add(ent2);
            #region html

            htmlShow.Append("<hr/>");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-md-12\">");
            htmlShow.Append("<div class=\"card\">");
            htmlShow.Append("<div class=\"card-body\">");

            htmlShow.Append("<table class=\"table table-hover\">");
            htmlShow.Append("<tbody>");

            foreach (var lis in listaContenedor)
            {
                //Ent_Reserva ent = (Ent_Reserva)lis;   

                htmlShow.Append("<tr>");
                htmlShow.Append("<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <b>Cantidad:</b>  " + lis.Cantidad + "</div></td>");

                htmlShow.Append("<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <b>Contenedor:</b>  " + lis.Tipo_Contenedor + "</div></td>");

                htmlShow.Append("<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <b>Comodity:</b>  " + lis.Commodity + "</div></td>");

                htmlShow.Append("<td style=\"border-top:none;\" class=\"border - right\"><div class=\"row\"> <b>Carga:</b>  " + lis.Descripcion_Carga + "</div></td>");
                htmlShow.Append("</tr>");

            }

            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");

            #endregion
            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

    }
}