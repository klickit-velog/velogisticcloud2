﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Seguimiento;
using VELogisticCloud.Servicio.Business.Seguimiento;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.App.Web.Controllers.Admin.Reservas
{
    public class ResumenReservaController : Controller
    {

        private readonly ILogger _logger;
        private IConfiguration _configuration;
        public ResumenReservaController(ILogger<ResumenReservaController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult ListaSeguimiento(int id)
        {
            string htmlGrid = "";
            try
            {
                BL_Seguimiento BL = new BL_Seguimiento(_configuration);
                var res = BL.BL_SeguimientoReserva(id);
                htmlGrid = crearGridShow(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error ResumenReserva/ListaSeguimiento");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShow(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<SeguimientoReserva> lista = (List<SeguimientoReserva>)list;


                //Ent_Reserva ent = (Ent_Reserva)lis;

                #region html
                htmlShow.Append("<div class=\"container-fluid\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");

                htmlShow.Append("<div class=\"card-header\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-12\">");
                htmlShow.Append("<h4> Seguimiento Flete </h4>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");


                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("<table class=\"table table-hover\" style=\"border:none\">");
                htmlShow.Append("<tbody>");

                foreach(var liss in lista)
                {
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border:none\">");
                    htmlShow.Append("<div class=\"row\">");

                    htmlShow.Append("<div class=\"col-1\">" +liss.CodEstado);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-3\"> Estado: " + liss.DescEstado);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-4\"> Fecha Ini.: " + liss.FechaIni);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-4\"> Fecha Fin: " + liss.FechaFin);
                    htmlShow.Append("</div>");


                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");
                    htmlShow.Append("</tr>");
                }
                htmlShow.Append("</tbody>");
                htmlShow.Append("</table>");
                htmlShow.Append("</div>");


                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

    }
}