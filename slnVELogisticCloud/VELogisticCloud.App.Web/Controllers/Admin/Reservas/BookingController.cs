﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Models.Seguimiento;
using VELogisticCloud.Servicio.Business.Booking;
using VELogisticCloud.Servicio.Business.Reservas;
using VELogisticCloud.Servicio.Middleware;
using VELogisticCloud.Model.Identity;
using Microsoft.AspNetCore.Identity;
using VELogisticCloud.Servicio.Business.Inttra;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using VELogisticCloud.Servicio.Business.General;
using VELogisticCloud.Models.General;
using VELogisticCloud.CrossCutting.Mail;
using VELogisticCloud.CrossCutting.DTO;
using Microsoft.Extensions.Caching.Memory;
using VELogisticCloud.App.Web.Clases;

namespace VELogisticCloud.App.Web.Controllers.Admin.Reservas
{
    public class BookingController : Controller
    {
        //Booking
        private readonly ILogger _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private bool esCliente = false;
        private IHostingEnvironment _environment;
        private IConfiguration _configuration;
        private BL_Booking BL;
        private BL_Variable bl_Variable;
        private INetMailSender netMailSender;
        private readonly IMemoryCache _memoryCache;
        public static string StaticTipousuario="";

        public BookingController(ILogger<BookingController> logger, UserManager<ApplicationUser> userManager, IHostingEnvironment environment
            , IConfiguration configuration, INetMailSender _netMailSender, IMemoryCache memoryCache)
        {
            _logger = logger;
            _userManager = userManager;
            _environment = environment;
            _configuration = configuration;
            netMailSender = _netMailSender;
            _memoryCache = memoryCache;
            BL = new BL_Booking(_configuration, _memoryCache);
        }
        public async Task<IActionResult> Create()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();

            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaContractOwner = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarContractOwner().ForEach(a => listaContractOwner.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));


            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            var idCliente = "";
            var NombreCliente = "";

            if (datos.Count > 0)
            {
                idCliente = datos[0].Type;
                NombreCliente = datos[0].Value;
            }
            else
            {
                idCliente = HttpContext.User.Identity.Name;
                NombreCliente = HttpContext.User.Identity.Name;
            }

            var listaClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: "");
            var listaFiltro = listaClientes.Where(x => x.Correo == idCliente).FirstOrDefault();
            var Contacto = new Contacto();

            if (listaFiltro != null)
            {
                if (listaFiltro.Contactos.Count() > 0)
                {
                    Contacto = listaFiltro.Contactos.LastOrDefault();
                }
            }

            if (listaFiltro == null)
            {
                listaFiltro = new Cliente();
            }

            //TODO: validar usuario sea customer.



            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;
            model.ListaContractOwner = listaContractOwner;
            model.Cliente = listaFiltro;
            model.Contacto = Contacto;

            return View(model);
        }

        public IActionResult Editar()
        {
            ModelCombos model = new ModelCombos();



            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var correo = HttpContext.User.Identity.Name;
            var datos = new OdooService(_configuration, _memoryCache).getCliente(correo);

            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;

            return View(model);
        }

        [HttpGet]
        public IActionResult Resumen()
        {
            return View();
        }

        public IActionResult SeguimientoBooking()
        {
            return View();
        }

        public IActionResult ListaBooking()
        {
            return View();
        }

        public IActionResult ListaBookingCustomer()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();

            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;

            return View(model);
        }

        #region Crear Booking
        [HttpPost]
        public ActionResult ObtenerPartidaArancelaria(int id)
        {
            Resultado resultado = new Resultado();

            try
            {
                var a = new OdooService(_configuration, _memoryCache).ListarCommodity().Where(x => x.Id == id).FirstOrDefault();
                if (a != null)
                {
                    resultado.nTipoMensaje = (int)TipoMensaje.Correcto;
                    resultado.nValorString = a.PartidaArancelaria;

                    return Ok(resultado);
                }
                else
                {
                    resultado.nTipoMensaje = (int)TipoMensaje.Informativo;
                    resultado.sMensaje = ErroresBooking.NO_SE_OBTUVO_PARTIDA_ARANCELARIA.ToString();
                    return Ok(resultado);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ObtenerPartidaArancelaria");
                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = ErroresBooking.NO_SE_OBTUVO_PARTIDA_ARANCELARIA.ToString();
                return Ok(resultado);
            }
        }

        [HttpPost]
        public ActionResult CargarResumen(int id)
        {
            Resultado resultado = new Resultado();

            try
            {
                if (id > 0)
                {
                    //BL_Booking BL = new BL_Reserva();
                    var res = BL.BL_ListaBooking(id, "");
                    List<InsertBooking> lsta = (List<InsertBooking>)res;
                    InsertBooking ent = (InsertBooking)lsta[0];
                    ent.Valido_Desde = ent.ETA.ToString("dd/MM/yyyy");
                    ent.Valido_Hasta = ent.ETD.ToString("dd/MM/yyyy");

                    resultado.nTipoMensaje = (int)TipoMensaje.Correcto;
                    resultado.objeto = ent;

                    return Ok(resultado);
                }
                else
                {
                    resultado.nTipoMensaje = (int)TipoMensaje.Error;
                    resultado.sMensaje = ErroresBooking.CODIGO_RESERVA_NO_EXISTE.ToString();
                    return Ok(resultado);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/CargarResumen");

                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_TRAER_RESUMEN.ToString();

                return Ok(resultado);
            }

        }

        [HttpPost]
        public async Task<ActionResult> CargarDatosCoti(int id)
        {
            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                var res = BL.BL_DatosCotizacionMaritima(id);
                List<DatosCotizacionMaritima> lsta = (List<DatosCotizacionMaritima>)res;
                DatosCotizacionMaritima ent = lsta[0];
                return Ok(ent);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/CargarDatosCoti");
                return Ok("0-No se encontraron datos para cargar.");
            }
        }

        public class AutoCompleteList
        {
            public string label;
            public string value;
            public string Id;

        }

        public IActionResult AutoCompleteConsignatario(string term)
        {
            var c = BL.BL_ListaConsignatario();
            List<InsertConsignatario> ListaConsignatario = (List<InsertConsignatario>)c;

            var result = (from U in ListaConsignatario
                          where U.Nombre.ToLower().Trim().Contains(term.ToLower().Trim(), System.StringComparison.CurrentCultureIgnoreCase)
                          select new
                          {
                              value = U.Nombre,
                              label = U.Nombre,
                              Id = U.IdConsignatario,
                              direccion = U.Direccion,
                              contacto = U.PersonaContacto,
                              telefono = U.Telefono,
                              fax = U.Fax
                          });
            //jsonResult = controller.Json(producto, System.Web.Mvc.JsonRequestBehavior.AllowGet);
            return Json(result);

        }

        public IActionResult AutoCompleteNotify(string term)
        {
            var n = BL.BL_ListaNotify();
            List<InsertNotify> ListaNotify = (List<InsertNotify>)n;
            List<AutoCompleteList> listaComplete = new List<AutoCompleteList>();
            ListaNotify.ForEach(x =>
            {
                listaComplete.Add(new AutoCompleteList
                {
                    Id = String.Format("{0}", x.IdNotify),
                    value = String.Format("{0}", x.Nombre),
                    label = String.Format("{0}", x.Nombre)
                });
            });

            var result = (from U in ListaNotify
                          where U.Nombre.ToLower().Trim().Contains(term.ToLower().Trim(), System.StringComparison.CurrentCultureIgnoreCase)
                          select new
                          {
                              value = U.Nombre,
                              label = U.Nombre,
                              Id = U.IdNotify,
                              direccion = U.Direccion,
                              contacto = U.PersonaContacto,
                              telefono = U.Telefono,
                              fax = U.Fax
                          });
            //jsonResult = controller.Json(producto, System.Web.Mvc.JsonRequestBehavior.AllowGet);
            return Json(result);

        }


        [HttpPost]
        public async Task<ActionResult> InsertarDatos(InsertBooking ent, InsertConsignatario consignatario, 
            InsertNotify notify, string tipousuario, string tipoContrato, int IdItinerarioManual)
        {
            Resultado resultado = new Resultado();

            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    ent.Cliente = Convert.ToString(idCliente);
                    ent.UsuarioCreacion = HttpContext.User.Identity.Name;
                }
                else
                {
                    if (tipousuario != "customer")
                    {
                        ent.Cliente = HttpContext.User.Identity.Name;
                        ent.UsuarioCreacion = HttpContext.User.Identity.Name;
                    }
                    else
                    {
                        ent.Cliente = "";
                        resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                        resultado.sMensaje = ErroresBooking.NO_SE_GRABA_CON_USUARIO_CUSTOMER.ToString();
                        return Ok(resultado);
                    }
                }

                if (consignatario.IdConsignatario == 0)
                {
                    ent.IdConsignatario = Convert.ToInt32((string)BL.BL_InsertarConsignatario(consignatario));
                }
                else
                {
                    ent.IdConsignatario = consignatario.IdConsignatario;
                }
                if (notify.IdNotify == 0)
                {
                    ent.IdNotify = Convert.ToInt32((string)BL.BL_InsertarNotify(notify));
                }
                else
                {
                    ent.IdNotify = notify.IdNotify;
                }

                if (ent.ClaseContenedor.ToLower().Trim().Contains("reefer"))
                {
                    ent.IdImo = 0;
                    ent.IMO = null;
                    ent.UN1 = String.Empty;
                    ent.UN2 = String.Empty;
                }
                if (ent.ClaseContenedor.ToLower().Trim().Contains("dry"))
                {
                    ent.AtmosferaControlada = 0;
                    ent.CO2 = 0;
                    ent.O2 = 0;
                    ent.ColdTreatment = 0;
                    ent.TipoTemperatura = null;
                    ent.Temperatura = 0;
                    ent.TipoVentilacion = null;
                    ent.Ventilacion = 0;
                    ent.TipoHumedad = 0;
                    ent.Humedad = 0;
                }
                if (ent.IdCondicionPago != 1)
                {
                    ent.NombrePagador = "";
                    ent.DireccionPagador = "";
                }

                ent.Nota = "";
               
                var userOdoo = new OdooService(_configuration, _memoryCache).getCustomerPorClienteCorreo(ent.Cliente);
                var nombreCustomer = new OdooService(_configuration, _memoryCache).getCustomerNamePorClienteCorreo(ent.Cliente);

                ent.NombreCustomer = userOdoo.Nombre;
                ent.tipoContrato = tipoContrato;
                int codReserva = 0;
                if (ent.IdContactOwner == 0)
                {
                    resultado.nTipoMensaje = ((int)TipoMensaje.Informativo);
                    resultado.sMensaje = InformativoBooking.FALTA_INGRESAR_CONTRACT_OWNER.ToString();
                    return Ok(resultado);
                }
                else
                {
                    if (Convert.ToInt32(ent.CantidadBooking) == 0)
                    {
                        resultado.nTipoMensaje = ((int)TipoMensaje.Informativo);
                        resultado.sMensaje = InformativoBooking.FALTA_INGRESAR_CANTIDAD_BOOKING.ToString();
                        return Ok(resultado);
                    }
                    else
                    {
                        int res = 0;
                        for (int i = 0; i < Convert.ToInt32(ent.CantidadBooking); i++)
                        {
                            res = Convert.ToInt32(BL.BL_InsertarBooking(ent));
                        }
                        codReserva = res;
                    }
                }

                //var result = (string)res;
                if (codReserva > 0)
                {
                    var res2 = BL.BL_InsertBookingItinerarioManual(codReserva, IdItinerarioManual);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = ent.ExportadorNombre,
                        NroSolicitud = codReserva.ToString()
                    };

                    await netMailSender.sendMailAsync(ent.Cliente, emailTemplate, TipoEmail.BOOKING_1_RECIBIDO, userOdoo.Email);

                    resultado.nTipoMensaje = ((int)TipoMensaje.Correcto);
                    resultado.sMensaje = CorrectoBooking.CREACION_CORRECTA.ToString();
                    resultado.nValorEntero = codReserva;
                    return Ok(resultado);
                }
                else
                {
                    resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                    resultado.sMensaje = ErroresBooking.NO_SE_PUDO_GRABAR_BOOKING.ToString();
                    return Ok(resultado);
                    //return Ok("0- Error al grabar datos de la reserva.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/InsertarDatos");
                resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_GRABAR_BOOKING.ToString();
                return Ok(resultado);
            }
        }

        [HttpPost]
        public ActionResult CargarDatosReserva(int id)
        {
            Resultado resultado = new Resultado();

            try
            {
                var res = BL.BL_ListaBooking(id, "");
                List<InsertBooking> lsta = (List<InsertBooking>)res;
                InsertBooking ent = lsta[0];

                resultado.nTipoMensaje = ((int)TipoMensaje.Correcto);
                resultado.objeto = ent;
                return Ok(ent);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/CargarDatosReserva");

                resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_TRAER_DATOS_RESERVA.ToString();
                return Ok(resultado);
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateDatos(int id, InsertBooking ent, InsertConsignatario consignatario,
            InsertNotify notify, string tipoContrato)
        {
            Resultado resultado = new Resultado();

            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    ent.Cliente = Convert.ToString(idCliente);
                    ent.UsuarioModificacion = HttpContext.User.Identity.Name;
                }
                else
                {
                    ent.Cliente = HttpContext.User.Identity.Name;
                    ent.UsuarioModificacion = HttpContext.User.Identity.Name;
                }
                //ent.Cliente = HttpContext.User.Identity.Name;
                //ent.UsuarioModificacion = HttpContext.User.Identity.Name;
                ent.IdBooking = id;
                if (consignatario.IdConsignatario == 0)
                {
                    ent.IdConsignatario = Convert.ToInt32((string)BL.BL_InsertarConsignatario(consignatario));
                }
                else
                {
                    ent.IdConsignatario = consignatario.IdConsignatario;
                }
                if (notify.IdNotify == 0)
                {
                    ent.IdNotify = Convert.ToInt32((string)BL.BL_InsertarNotify(notify));
                }
                else
                {
                    ent.IdNotify = notify.IdNotify;

                }

                if (ent.ClaseContenedor.ToLower().Trim().Contains("reefer"))
                {
                    ent.IdImo = 0;
                    ent.IMO = null;
                    ent.UN1 = String.Empty;
                    ent.UN2 = String.Empty;
                }
                if (ent.ClaseContenedor.ToLower().Trim().Contains("dry"))
                {
                    ent.AtmosferaControlada = 0;
                    ent.CO2 = 0;
                    ent.O2 = 0;
                    ent.ColdTreatment = 0;
                    ent.TipoTemperatura = null;
                    ent.Temperatura = 0;
                    ent.TipoVentilacion = null;
                    ent.Ventilacion = 0;
                    ent.TipoHumedad = 0;
                    ent.Humedad = 0;
                }
                if (ent.IdCondicionPago != 1)
                {
                    ent.NombrePagador = "";
                    ent.DireccionPagador = "";
                }

                ent.Nota = "";
                ent.tipoContrato = tipoContrato;

                var res = BL.BL_UpdateBooking(ent);
                var result = (string)res;
                int codReserva = Convert.ToInt32(result);
                if (codReserva > 0)
                {
                    resultado.nTipoMensaje = ((int)TipoMensaje.Correcto);
                    resultado.sMensaje = CorrectoBooking.ACTUALIZACION_CORRECTA;
                    resultado.nValorEntero = codReserva;
                    return Ok(resultado);
                }
                else
                {
                    resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                    resultado.sMensaje = ErroresBooking.CODIGO_RESERVA_NO_EXISTE;
                    return Ok(resultado);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/UpdateDatos");

                resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_ACTUALIZAR;
                return Ok(resultado);
            }

        }

        [HttpPost]
        public ActionResult UpdateImo(int id, string imo)
        {
            Resultado resultado = new Resultado();

            try
            {
                BL.BL_UpdateImo(id, imo);
                resultado.nTipoMensaje = ((int)TipoMensaje.Correcto);
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_ACTUALIZAR;
                return Ok(resultado);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/UpdateDatos");

                resultado.nTipoMensaje = ((int)TipoMensaje.Error);
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_ACTUALIZAR_IMO_NAVE;

                return Ok(resultado);
            }

        }

        #endregion

        #region Listar Booking Cliente
        [HttpPost]
        public async Task<ActionResult> ListarBooking(InsertBooking ent, string tipousuario)
        {

            //BL_Inttra bl = new BL_Inttra();
            //bl.listFiles();
            StaticTipousuario = tipousuario;
            string Codigo_Usuario = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: validar usuario sea customer.
            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                var NomCliente = datos[0].Value;
                Codigo_Usuario = Convert.ToString(idCliente);
                esCliente = true;
            }
            else
            {
                if (tipousuario != "customer")
                {

                    esCliente = true;
                    Codigo_Usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    esCliente = false;
                    Codigo_Usuario = "";
                }
            }

            //Resultado retorno = new Resultado();
            string htmlGrid = "";

            try
            {
                var res = BL.BL_ListaBooking(0, Codigo_Usuario);
                List<InsertBooking> ListaBooking = new List<InsertBooking>();
                ListaBooking = (List<InsertBooking>)res;
                if (ent.IdCommodity > 0)
                {
                    ListaBooking = ListaBooking.Where(x => x.IdCommodity == ent.IdCommodity).ToList();
                }
                if (ent.IdLinea > 0)
                {
                    ListaBooking = ListaBooking.Where(x => x.IdCommodity == ent.IdCommodity).ToList();
                }
                if (ent.IdPuertoEmbarque > 0)
                {
                    ListaBooking = ListaBooking.Where(x => x.IdPuertoEmbarque == ent.IdPuertoEmbarque).ToList();
                }
                if (ent.IdPuertoDescarga > 0)
                {
                    ListaBooking = ListaBooking.Where(x => x.IdPuertoDescarga == ent.IdPuertoDescarga).ToList();
                }
                if (ent.NroBooking != null && ent.NroBooking.ToLower().Trim() != "")
                {
                    ListaBooking = ListaBooking.Where(x => x.NroBooking.ToLower().Trim().Contains(ent.NroBooking.ToLower().Trim())).ToList();
                }
                if (ent.IdBooking != 0 && ent.IdBooking.ToString().ToLower().Trim() != "")
                {
                    ListaBooking = ListaBooking.Where(x => x.IdBooking.ToString().ToLower().Trim().Contains(ent.IdBooking.ToString().ToLower().Trim())).ToList();
                }
                if (ent.EstadoVE != 0)
                {
                    ListaBooking = ListaBooking.Where(x => x.EstadoVE == ent.EstadoVE).ToList();
                }


                htmlGrid = crearGridShow(ListaBooking);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ListarBooking");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShow(List<InsertBooking> listaBooking)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");
            BL_Reserva bb = new BL_Reserva(_configuration);
            List<Ent_Navieras_Oddo_VE> lisNavierasImo = (List<Ent_Navieras_Oddo_VE>)bb.BL_ListaNavierasOdooVE();
            //var lisNavierasImo =  ;

            if (listaBooking.Count > 0)
            {

                foreach (var lis in listaBooking)
                {
                    //Ent_Reserva ent = (Ent_Reserva)lis;

                    #region html

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<table class=\"table\">");
                    htmlShow.Append("<tbody>");

                    htmlShow.Append("<tr>");
                    string nro_booking = lis.NroBooking == null ? " <i>[Sin Nro Booking]</i>" : lis.NroBooking;
                    string nro_referencia = lis.NumeroReferenciaInttra == null ? " <i>[Sin Nro Referencia]</i>" : lis.NumeroReferenciaInttra;
                    string nro_id = lis.IdBooking.ToString();
                    htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\"><h5>" + "Nro. Booking: " + nro_booking + "</h5>" +
                        "<h5>" + "Nro. Solicitud:" + nro_id + "</h5><h5>" + "Nro. Referencia Inttra:" + nro_referencia + "</h5></td>");

                    htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\" class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> " + lis.EstadoVE_Desc + "</td>");
                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");

                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Cantidad contenedores:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.CantidadBulto + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Origen:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.PuertoEmbarque + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Nombre de nave:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.NombreNave + "</b></div>");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
                    //htmlShow.Append("<div class=\"col-6 text-right\"><b>ABC</b></div>");
                    //htmlShow.Append("</div>");

                    htmlShow.Append("</td>");


                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Linea:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.Linea + "</b></div>");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
                    //htmlShow.Append("<div class=\"col-6 text-right\"><b></b></div>");
                    //htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">ETD (Origen):</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETD.ToString("dd/MM/yyy") + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">ETA (Destino):</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETA.ToString("dd/MM/yyy") + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("</td>");

                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Fecha de recepcion:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETD.ToString("dd/MM/yyy") + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Puerto embarque:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.PuertoEmbarque + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Puerto descarga:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.PuertoDescarga + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("</td>");

                    if (esCliente)
                    {
                        htmlShow.Append("<td style=\"border-top:none;\">");
                        //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>"); 
                        htmlShow.AppendFormat("<a href=\"#\" onclick='PlantillaReserva({0});'>Utilizar como plantilla</a>", lis.IdBooking.ToString());
                        htmlShow.Append("<br>");
                    }
                    else
                    {
                        htmlShow.Append("<td style=\"border-top:none;\">");
                        //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>"); 
                        htmlShow.AppendFormat("<a href=\"#\" onclick='PlantillaReserva({0}); disabled'>Utilizar como plantilla</a>", lis.IdBooking.ToString());
                        htmlShow.Append("<br>");
                    }


                    htmlShow.AppendFormat("<a href=\"#\" onclick='EditReserva({0});'>Modificar Reserva</a>", lis.IdBooking.ToString());
                    htmlShow.Append("<br>");

                    if (lis.ImoInttra != "0")
                    {
                        htmlShow.AppendFormat("<a href=\"#\" onclick='VerMapa({0});'>Ver en Mapa</a>", lis.ImoInttra);
                        htmlShow.Append("<br>");
                    }
                    else
                    {
                        if (lis.NroBooking != null && lis.NroBooking != "Sin Nro Booking")
                        {
                            string imo = "0";
                            var a = lisNavierasImo.Where(x => x.NombreNaviera == lis.NombreNave).FirstOrDefault();
                            if (a != null)
                            {
                                imo = a.IMO;
                                BL.BL_UpdateImo(lis.IdBooking, imo);
                                htmlShow.AppendFormat("<a href=\"#\" onclick='VerMapa({0});'>Ver en Mapa</a>", imo);
                                htmlShow.Append("<br>");
                            }
                        }
                    }

                    htmlShow.AppendFormat("<a href=\"#\" onclick=\"window.location.href='../Booking/SeguimientoBooking?id={0}'; \">Ver Estado de envio </button>", lis.IdBooking.ToString());
                    htmlShow.Append("<br>");

                    htmlShow.Append("</td>");
                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border-top:none;\"  colspan=\"4\" class=\"text - right\">");
                    htmlShow.AppendFormat("<a href=\"#\" onclick='CancelarBooking({0});'>Cancelar booking</a>", lis.IdBooking.ToString());
                    //htmlShow.AppendFormat("<img src='/Imagenes/delete.png'  title='Eliminar Cotización'  onclick='DeleteReserva({0});'  style='cursor: hand' > &nbsp;&nbsp;"
                    htmlShow.AppendFormat("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../Booking/Resumen?id={0}'; \">Ver Detalles</button>", lis.IdBooking.ToString(), StaticTipousuario);
                    //htmlShow.AppendFormat("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../Booking/SeguimientoBooking?id={0}'; \">Ver Detalles </button>", lis.IdBooking.ToString());
                    htmlShow.Append("</td>");

                    //htmlShow.Append("<td style=\"border-top:none;\"  colspan=\"4\" class=\"text - right\">");
                    //htmlShow.AppendFormat("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../Booking/SeguimientoBooking?id={0}'; \">Ver Estado de envio </button>", lis.IdBooking.ToString());
                    //htmlShow.AppendFormat("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../Booking/Resumen?id={0}&J_user={1}'; \">Ver Resumen</button>", lis.IdBooking.ToString(), StaticTipousuario);
                    htmlShow.Append("</td>");

                    htmlShow.Append("</tr>");


                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                    #endregion
                }
            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }
            //if (listObras != null && listObras.Count > 0)
            //{
            return htmlShow.ToString();

        }
        #endregion

        #region Listar Booking Customer
        [HttpPost]
        public async Task<ActionResult> ListarBookingCustomer(ParametrosBusquedaNuevo filtros, string tipousuario)
        {
            //Resultado retorno = new Resultado();
            DataListado dataListado = new DataListado();
            StaticTipousuario = tipousuario;
            string htmlGrid = "";
            string Codigo_Usuario = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            //TODO: validar usuario sea customer.
            if (datos.Count > 0)
            {
                var idCliente = datos[0].Type;
                var NomCliente = datos[0].Value;
                Codigo_Usuario = Convert.ToString(idCliente);
                esCliente = true;
            }
            else
            {
                esCliente = false;
                if (tipousuario != "customer")
                {
                    Codigo_Usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    Codigo_Usuario = "";
                }
            }
            try
            {
                List<NuevaListaBooking> ListaBooking = new List<NuevaListaBooking>();
                var ListaClienteCorreo = new List<string>();

                if (Codigo_Usuario == "")
                {
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    ListaClienteCorreo = listarClientes.Select(a => a.Correo).ToList();
                }
                else
                {
                    ListaClienteCorreo.Add(Codigo_Usuario);
                }

                ListaBooking = BL.ListarBookingDelCustomer(ListaClienteCorreo,
                                                            filtros.IdBooking,
                                                            filtros.NroBooking,
                                                            filtros.IdContactOwner,
                                                            filtros.Linea,
                                                            filtros.NombreNave,
                                                            filtros.NroViaje,
                                                            filtros.IdEmbarque,
                                                            filtros.IdDescarga,
                                                            filtros.IdComodity,
                                                            string.IsNullOrEmpty(filtros.DateFrom) ? null : Convert.ToDateTime(filtros.DateFrom).ToString("yyyy-MM-dd"),
                                                            string.IsNullOrEmpty(filtros.DateTo) ? null : Convert.ToDateTime(filtros.DateTo).ToString("yyyy-MM-dd"),
                                                            filtros.EstadoId
                                                        ).ToList();

                htmlGrid = crearGridNuevoBookingCustomer(ListaBooking.OrderByDescending(x => x.IdBooking).ToList());
                dataListado.data = htmlGrid;
                dataListado.cantidad = ListaBooking.Count();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ListarBooking");
            }
            return Ok(dataListado);
        }

        public string crearGridNuevoBookingCustomer(List<NuevaListaBooking> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            BL_Reserva bb = new BL_Reserva(_configuration);
            List<Ent_Navieras_Oddo_VE> lisNavierasImo = (List<Ent_Navieras_Oddo_VE>)bb.BL_ListaNavierasOdooVE();

            if (lista.Count > 0)
            {
                #region Cabecera
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"table-responsive-sm\">");
                htmlShow.Append("<table class=\"table table-hover table-bordered\" id='data' >");
                htmlShow.Append("<thead style=\"overflow-x: auto;\">");
                htmlShow.Append("<tr>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Acciones</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">N° de Solicitud</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Contrac Owner</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Nombre</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Linea</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nro Contrato</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nro Booking</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Estado</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nombre Nave</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nro Viaje</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Puerto Embarque</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Puerto Descarga</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Commodity</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Cliente</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">ETD (Origen)</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Numero BL</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Fecha Solicitud</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nombre Consignatario</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nombre Notify</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Operador Logístico</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Tiempo Transito</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">ETA</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Tipo Contenedor</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Clase Contenedor</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Cold Treatment</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Cantidad Contenedor</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Atmosfera Controlada</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Pais Descarga</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">CO2</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">O2</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Tipo Temperatura</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Temperatura</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Tipo Ventilacíon</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Ventilacíon</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Humedad</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">UN1</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">UN2</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Domicilio</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Ruc</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Contacto</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Telefono</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Fax</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Exportador Celular</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Direccíon Consignatario</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Fax Consignatario</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Telefono Consignatario</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Persona Contacto Consignatario</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Direccíon Notify</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Fax Notify</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Telefono Notify</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Persona Contacto Notify</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Pais Embarque</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Cantidad Bulto</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Partida Arancelaria</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Tipo Bulto</th>");
                //htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Cantidad Bulto</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Peso Bruto</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Condicíon Pago</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Emisíon BL</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Ruc Operador</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nota</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Usuario Creacíon</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Imo</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Cantidad Booking</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Nombre Customer</th>");
                htmlShow.Append("<th style=\"color:#000000;background-color: #ebebeb\" style=\"border: 1px solid #ccc;\">Tipo Contrato</th>");
                #endregion
                htmlShow.Append("</tr>");
                htmlShow.Append("</thead>");
                htmlShow.Append("<tbody id='developers' style=\"overflow-x: auto;\"  >");

                foreach (var lis in lista)
                {                    
                    if(lis.ImoInttra == "0")
                    {
                        string imo = "0";
                        var a = lisNavierasImo.Where(x => x.NombreNaviera == lis.NombreNave).FirstOrDefault();
                        if (a != null)
                        {
                            imo = a.IMO;
                            BL.BL_UpdateImo(lis.IdBooking, imo);
                        }
                    }
                    #region html
                    //if (lis.IdBooking == 3096)
                    //{
                    //    //var holra = ""
                    //}
                    htmlShow.Append("<tr style=\"border-bottom: 1px solid #ccc; border-left: 1px solid #ccc; border-right: 1px solid #ccc;\">");
                    if(lis.IdEstado == 1)
                    {
                        htmlShow.AppendFormat("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\"> " +
                     "<table class=\"table table-borderless\" style=\"margin:auto\" id=\"Acciones\">" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='EditReserva(" + lis.IdBooking.ToString() + ");'>Editar Reserva</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='PlantillaReserva(" + lis.IdBooking.ToString() + ");'>Usar Plantilla</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='ConfirmarBooking("+lis.IdBooking.ToString()+");' > <i class=\"fas fa-check\"></i> </button></td>" +
                     "</tr>" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='VerMapa(" + lis.ImoInttra + ");'>Ver en mapa</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='CancelarBooking(" + lis.IdBooking.ToString() + ");'>Cancelar booking</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='CancelarBooking(" + lis.IdBooking.ToString() + ");'> X </button></td>" +
                     //if (lis.ImoInttra != "0")
                     //   {
                     //       "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='VerMapa(" + lis.ImoInttra + ");'>Ver en mapa</button></td>" +
                     //   }
                     //   else
                     //   {
                     //       if (lis.NroBooking != null && lis.NroBooking != "Sin Nro Booking")
                     //       {
                     //           string imo = "0";
                     //           var a = lisNavierasImo.Where(x => x.NombreNaviera == lis.NombreNave).FirstOrDefault();
                     //           if (a != null)
                     //           {
                     //               imo = a.IMO;
                     //               BL.BL_UpdateImo(lis.IdBooking, imo);
                     //           }
                     //           if (imo == "0")
                     //           {
                     //               htmlShow.AppendFormat("<a href=\"#\" style=\"color:red\"onclick='UpdateImo({0});'>Ver en Mapa</a>", lis.IdBooking.ToString());
                     //               htmlShow.Append("<br>");
                     //           }
                     //           else
                     //           {
                     //               htmlShow.AppendFormat("<a href=\"#\" onclick='VerMapa({0});'>Ver en Mapa</a>", imo);
                     //               htmlShow.Append("<br>");
                     //           }

                     //       }
                     //   }


                     "</tr>" +

                     "</table>" +

                     "</td>");
                    }
                    if (lis.IdEstado == 2)
                    {
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;padding:0.30rem\"> " +
                     "<table class=\"table table-borderless\" style=\"margin:auto\" id=\"Acciones\">" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> </td>" +
                     "</tr>" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> </td>" +
                     "</tr>" +

                     "</table>" +

                     "</td>");
                    }

                    if (lis.IdEstado == 3)
                    {
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;padding:0.30rem\"> " +
                     "<table class=\"table table-borderless\" style=\"margin:auto\" id=\"Acciones\">" +

                     "<tr style=\"width: auto\">" +
                      "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='EditReserva(" + lis.IdBooking.ToString() + ");'>Editar Reserva</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='PlantillaReserva(" + lis.IdBooking.ToString() + ");'>Usar Plantilla</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem;\" rowspan=\"2\"> <button style=\"width: 125px;height:70px; font-size: smaller;\" type=\"button\" class=\"btn btn-warning\" onclick='EnvioOdoo(" + lis.IdBooking.ToString() + ");'>Enviar a Odoo</button></td>" +
                     "</tr>" +

                     "<tr style=\"width: auto\">" +
                      "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='VerMapa(" + lis.ImoInttra + ");'>Ver en mapa</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='CancelarBooking(" + lis.IdBooking.ToString() + ");'>Cancelar booking</button></td>" +
                     "</tr>" +

                     "</table>" +

                     "</td>");
                    }

                    if (lis.IdEstado == 4)
                    {
                        htmlShow.Append("<td style=\"border: 1px solid #ccc;padding:0.30rem\"> " +
                     "<table class=\"table table-borderless\" style=\"margin:auto\" id=\"Acciones\">" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='PlantillaReserva(" + lis.IdBooking.ToString() + ");'>Usar Plantilla</button></td>" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='CancelarBooking(" + lis.IdBooking.ToString() + ");'>Cancelar booking</button></td>" +
                     "</tr>" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-success\" onclick='VerMapa(" + lis.ImoInttra + ");'>Ver en mapa</button></td>" +
                     "</tr>" +

                     "</table>" +

                     "</td>");
                    }

                    if (lis.IdEstado == 5)
                    {
                      htmlShow.Append("<td style=\"border: 1px solid #ccc;padding:0.30rem\"> " +
                     "<table class=\"table table-borderless\" style=\"margin:auto\" id=\"Acciones\">" +

                     "<tr style=\"width: auto\">" +
                     "<td style=\"padding:0.15rem;padding-bottom:0rem\"> <button style=\"width: 125px;font-size: smaller;\" type=\"button\" class=\"btn btn-secondary\" onclick='PlantillaReserva(" + lis.IdBooking.ToString() + ");'>Usar Plantilla</button></td>" +
                     "</tr>" +
                     "<tr style=\"width: auto\">" +
                     "</tr>" +
                     "</table>" +

                     "</td>");
                    }

                    #region Datos
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.IdBooking + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ContactOwner + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorNombre + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Linea + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NroContrato + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NroBooking + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Estado + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NombreNave + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NroViaje + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PuertoEmbarque + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PuertoDescarga + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Commodity + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Cliente + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ETD + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NumeroBL + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.FechaSolicitud + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NombreConsignatario + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NombreNotify + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.OperadorLogistico + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TiempoTransito + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ETA + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TipoContenedor + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ClaseContenedor + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ColdTreatment + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.CantidadBulto + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.AtmosferaControlada + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PaisDescarga + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.CO2 + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.O2 + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TipoTemperatura + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Temperatura + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TipoVentilacion + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Ventilacion + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Humedad + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.UN1 + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.UN2 + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorDomicilio + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorRuc + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorContacto + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorTelefono + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorFax + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ExportadorCelular + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.DireccionConsignatario + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.FaxConsignatario + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TelefonoConsignatario + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PersonaContactoConsignatario + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.DireccionNotify + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.FaxNotify + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TelefonoNotify + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PersonaContactoNotify + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PaisEmbarque + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Variedad + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PartidaArancelaria + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TipoBulto + "</td>");
                    //htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.CantidadContenedor + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.PesoBruto + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.CondicionPago + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.EmisionBL + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.RucOperador + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.Nota + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.UsuarioCreacion + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.ImoInttra + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.CantidadBooking + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.NombreCustomer + "</td>");
                    htmlShow.Append("<td style=\"border: 1px solid #ccc;padding-top:0.1rem;padding-left:0.1rem;padding-right:0.1rem;padding-bottom:0.1rem\">" + lis.TipoContrato + "</td>");
                    htmlShow.Append("</tr>");
                    #endregion

                    #endregion
                }

                htmlShow.Append("</tbody");
                htmlShow.Append("</table>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }

            return htmlShow.ToString();

        }

        public string crearGridBookingCustomer(List<InsertBooking> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            BL_Reserva bb = new BL_Reserva(_configuration);
            List<Ent_Navieras_Oddo_VE> lisNavierasImo = (List<Ent_Navieras_Oddo_VE>)bb.BL_ListaNavierasOdooVE();

            if (lista.Count > 0)
            {
                foreach (var lis in lista)
                {
                    #region html
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<table class=\"table\">");
                    htmlShow.Append("<tbody>");

                    htmlShow.Append("<tr>");
                    string nro_booking = lis.NroBooking == null ? " <i>[Pendiente]</i>" : lis.NroBooking;
                    string nro_referencia = lis.NumeroReferenciaInttra == null ? " <i>[Sin Nro Referencia]</i>" : lis.NumeroReferenciaInttra;

                    string nro_id = lis.IdBooking.ToString();
                    htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\"><h5>" + "Nro. Booking: " + nro_booking + "</h5>" +
                        "<h5>" + "Nro. Solicitud:" + nro_id + "</h5><h5>" + "Nro. Referencia Inttra:" + nro_referencia + "</h5></td>");
                    htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\" class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> " + lis.EstadoVE_Desc + "</td>");
                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");

                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Cantidad contenedores:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.CantidadBulto + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Origen:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.PuertoEmbarque + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Nombre de nave:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.NombreNave + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>ABC</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("</td>");


                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Linea:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.Linea + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>ABC</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">ETD:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETD.ToString("dd/MM/yyyy") + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">ETA:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETA.ToString("dd/MM/yyyy") + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("</td>");

                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Fecha de recepcion:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.ETD.ToString("dd/MM/yyy") + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Puerto embarque:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.PuertoEmbarque + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Puerto descarga:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.PuertoDescarga + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Descarga contenedor:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b></b></div>");
                    htmlShow.Append("</div>");

                    if (esCliente)
                    {
                        htmlShow.Append("<td style=\"border-top:none;\">");
                        //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>"); 
                        htmlShow.AppendFormat("<a href=\"#\" onclick='PlantillaReserva({0});'>Utilizar como plantilla</a>", lis.IdBooking.ToString());
                        htmlShow.Append("<br>");
                    }
                    else
                    {
                        htmlShow.Append("<td style=\"border-top:none;\">");
                        //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>"); 
                        htmlShow.AppendFormat("<a href=\"#\" onclick='PlantillaReserva({0}); disabled'>Utilizar como plantilla</a>", lis.IdBooking.ToString());
                        htmlShow.Append("<br>");
                    }


                    htmlShow.AppendFormat("<a href=\"#\" onclick='EditReserva({0});'>Modificar Reserva</a>", lis.IdBooking.ToString());
                    htmlShow.Append("<br>");

                    if (lis.ImoInttra != "0")
                    {
                        htmlShow.AppendFormat("<a href=\"#\" onclick='VerMapa({0});'>Ver en Mapa</a>", lis.ImoInttra);
                        htmlShow.Append("<br>");
                    }
                    else
                    {
                        if (lis.NroBooking != null && lis.NroBooking != "Sin Nro Booking")
                        {
                            string imo = "0";
                            var a = lisNavierasImo.Where(x => x.NombreNaviera == lis.NombreNave).FirstOrDefault();
                            if (a != null)
                            {
                                imo = a.IMO;
                                BL.BL_UpdateImo(lis.IdBooking, imo);
                            }
                            if (imo == "0")
                            {
                                htmlShow.AppendFormat("<a href=\"#\" style=\"color:red\"onclick='UpdateImo({0});'>Ver en Mapa</a>", lis.IdBooking.ToString());
                                htmlShow.Append("<br>");
                            }
                            else
                            {
                                htmlShow.AppendFormat("<a href=\"#\" onclick='VerMapa({0});'>Ver en Mapa</a>", imo);
                                htmlShow.Append("<br>");
                            }

                        }
                    }

                    htmlShow.Append("</td>");


                    htmlShow.Append("</td>");
                    htmlShow.Append("<td style=\"border-top:none;\">");
                    if (lis.EstadoVE == 1)
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='ConfirmarBooking({0});'> <i class=\"fa fa-check-circle fa-3x\" style=\"color: green\"></i>  </a><br></div>", lis.IdBooking.ToString());
                        htmlShow.Append("</div>");
                        htmlShow.Append("</br>");
                    }
                    if (lis.EstadoVE == 1)
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='CancelarBooking({0});'> <i class=\"fa fa-times fa-3x\" style=\"color: red\" ></i>  </a><br></div>", lis.IdBooking.ToString());
                        htmlShow.Append("</div>");
                    }
                    if (lis.EstadoVE == 3)
                    {
                        htmlShow.Append("<div style=\"padding-top:20px \" class=\"row col-8 \">");
                        htmlShow.AppendFormat("<button onclick =\"EnvioOdoo({0}); return false\" class=\"col-12 text-left btn btn-primary float-right\">Enviar Booking a Odoo </button>", lis.IdBooking.ToString());
                        htmlShow.Append("</div>");
                    }

                    htmlShow.Append("</tr>");
                    htmlShow.Append("</td>");
                    htmlShow.Append("<tr>");
                    htmlShow.Append("</tr>");


                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"card-footer float-right\" style=\"text-align: right;\" >");
                    htmlShow.AppendFormat("<button onclick =\"CancelarBooking({0}); return false\" class=\"btn btn-primary float-left\">Cancelar Booking</button>", lis.IdBooking.ToString());
                    if (lis.EstadoVE == 1)
                    {
                        htmlShow.AppendFormat("<button onclick =\"EditReserva({0}); return false\" class=\"btn btn-primary\">Editar Reserva</button> ", lis.IdBooking.ToString());
                    }
                    if (esCliente)
                    {
                        htmlShow.AppendFormat("<button onclick =\"PlantillaReserva({0}); return false\" class=\"btn btn-primary \">Usar Plantilla</button>", lis.IdBooking.ToString());
                    }
                    else
                    {
                        htmlShow.AppendFormat("<button onclick =\"PlantillaReserva({0}); return false\" class=\"btn btn-primary \" disabled>Usar Plantilla</button>", lis.IdBooking.ToString());
                    }
                    htmlShow.Append("</div>");

                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                    #endregion
                }

            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }


        #endregion

        #region Seguimiento Booking
        [HttpPost]
        public ActionResult ListaSeguimiento(int id)
        {
            string htmlGrid = "";
            try
            {
                //BL_Seguimiento BL = new BL_Seguimiento();
                var res = BL.BL_SeguimientoBooking(id);
                htmlGrid = crearGridSeguimiento(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ListaSeguimiento");
            }
            return Ok(htmlGrid);
        }

        public string crearGridSeguimiento(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<SeguimientoReserva> lista = (List<SeguimientoReserva>)list;


            //Ent_Reserva ent = (Ent_Reserva)lis;

            #region html
            htmlShow.Append("<div class=\"container-fluid\">");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-md-12\">");
            htmlShow.Append("<div class=\"card\">");

            htmlShow.Append("<div class=\"card-header\">");
            htmlShow.Append("<div class=\"row\">");
            htmlShow.Append("<div class=\"col-12\">");
            htmlShow.Append("<h4> Seguimiento Flete </h4>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");


            htmlShow.Append("<div class=\"card-body\">");
            htmlShow.Append("<table class=\"table table-hover\" style=\"border:none\">");
            htmlShow.Append("<tbody>");

            if (lista.Count > 0)
            {
                foreach (var liss in lista)
                {
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border:none\">");
                    htmlShow.Append("<div class=\"row\">");

                    htmlShow.Append("<div class=\"col-1\">" + liss.CodEstado);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-3\"> Estado: " + liss.DescEstado);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-4\"> Fecha Ini.: " + liss.FechaIni);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-4\"> Fecha Fin: " + liss.FechaFin);
                    htmlShow.Append("</div>");


                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");
                    htmlShow.Append("</tr>");
                }
            }
            else
            {
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("<p>No se encontraron datos.<p>");
                htmlShow.Append("</div>");
            }


            htmlShow.Append("</tbody>");
            htmlShow.Append("</table>");
            htmlShow.Append("</div>");


            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            htmlShow.Append("</div>");
            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult ValidarContrato(string contrato, int idReserva)
        {
            try
            {
                var correo = HttpContext.User.Identity.Name;
                int IdCotizacionDetalleOdoo = 0;
                //var correo = HttpContext.User.Identity.Name;
                //if (Static_listaContratos == null || Static_listaContratos.Count < 1)
                //{
                List<ContratoDetalle> listaContratos = new OdooService(_configuration, _memoryCache).ListarContratos(correo, 0, 0, 0, 0, 0, 100000, true);
                //Static_listaContratos = listaContratos;
                //}


                var existe = listaContratos.Where(x => x.NumeroContrato == contrato.Trim()).ToList();

                if (existe.Count > 0)
                {
                    //BL_Reserva BL = new BL_Reserva();
                    //var data = BL.BookingTOoddo(idReserva);
                    //List<BookingTOoddo> bookingTOoddos = (List<BookingTOoddo>)data;
                    //BookingTOoddo bookingTOoddo = (BookingTOoddo)bookingTOoddos[0];
                    //int Id_Puerto_Carga = bookingTOoddo.Id_Puerto_Carga;
                    //int Id_Puerto_Descarga = bookingTOoddo.Id_Puerto_Descarga;

                    //var b = existe.Where(x => x.IdPuertoCarga == Id_Puerto_Carga && x.IdPuertoDescarga == Id_Puerto_Descarga).FirstOrDefault();
                    //if (b != null)
                    //{
                    //    IdCotizacionDetalleOdoo = b.IdCotizacionDetalleOdoo;
                    //}
                }
                return Ok(IdCotizacionDetalleOdoo);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ValidarContrato");
                return Ok(0);
            }
        }

        #endregion

        #region Cancelar y Confirmar Booking
        [HttpPost]
        public ActionResult CancelarBooking(int id)
        {
            Resultado resultado = new Resultado();

            //Resultado retorno = new Resultado();
            string htmlGrid = "";
            string user = HttpContext.User.Identity.Name;
            try
            {
                var res = BL.BL_CancelarBooking(id, user);

                resultado.nTipoMensaje = (int)TipoMensaje.Correcto;
                resultado.sMensaje = CorrectoBooking.CANCELAR_RESERVA_CORRECTA;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/CancelarBooking");
                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = ErroresBooking.NO_SE_PUDO_CANCELAR_BOOKING;
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmarBookingManual(int id, string booking, string imo)
        {
            Resultado resultado = new Resultado();
            //Resultado retorno = new Resultado();
            string htmlGrid = "1";
            string user = HttpContext.User.Identity.Name;
            try
            {
                var res = BL.ConfirmarBookingManual(id, booking, imo, user);
                var res1 = BL.BL_ListaBooking(id, "");
                List<InsertBooking> ListaBooking = new List<InsertBooking>();
                ListaBooking = (List<InsertBooking>)res1;

                var emailTemplate = new EmailTemplateDTO()
                {
                    NombreCliente = ListaBooking[0].ExportadorNombre,
                    NroSolicitud = ListaBooking[0].IdDetalleCotizacionOdoo.ToString(),
                    NroBooking = ListaBooking[0].NroBooking
                };

                resultado.nTipoMensaje = (int)TipoMensaje.Correcto;

                await netMailSender.sendMailAsync(ListaBooking[0].UsuarioCreacion, emailTemplate, TipoEmail.BOOKING_2_ACEPTADO);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ConfirmarBookingManual");

                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = ErroresBooking.NO_SE_OBTUVO_PARTIDA_ARANCELARIA.ToString();
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public ActionResult ValidarBookingManual(string nroBooking)
        {
            //Resultado retorno = new Resultado();
            string Correcto = "";
            string user = HttpContext.User.Identity.Name;
            try
            {
                if (nroBooking == null)
                {
                    nroBooking = "0";
                }
                var booking = BL.ObtenerByNumeroBooking(nroBooking);
                if (booking!=null)
                {
                    Correcto = "2";
                }
                else
                {
                    Correcto = "1";
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ValidarBookingManual");
            }
            return Ok(Correcto);
        }

        public class Retorno
        {
            public string result { get; set; }
            public string imo { get; set; }
        }

        public class DataListado
        {
            public string data { get; set; }
            public int cantidad { get; set; }
        }


        [HttpPost]
        public async Task<ActionResult> ConfirmarBooking(int id)
        {
            Retorno retorno = new Retorno();
            string Correcto = "";
            string user = HttpContext.User.Identity.Name;
            try
            {
                var Booking = ((List<InsertBooking>)BL.BL_ListaBooking(id, "")).FirstOrDefault();

                var listaNavieras = new OdooService(_configuration, _memoryCache).ListarNavieras();
                var Naviera = listaNavieras.Where(x => x.Codigo == Booking.CodLinea).FirstOrDefault();

                if (Naviera != null)
                {
                    if (Naviera.VaPorInttra)
                    {                      
                        var emailTemplate = new EmailTemplateDTO()
                        {
                            NombreCliente = Booking.ExportadorNombre,
                            NroSolicitud = Booking.IdDetalleCotizacionOdoo.ToString(),
                            NroBooking = Booking.NroBooking
                        };

                        await netMailSender.sendMailAsync(Booking.UsuarioCreacion, emailTemplate, TipoEmail.BOOKING_2_ACEPTADO);

                        var res = BL.BL_ConfirmarBooking(id, user);
                        // envio inttra
                        string CodigoVariable = "F0001";
                        var datoVariable = bl_Variable.BL_ListaVariables(CodigoVariable);
                        var Variable = (List<BE_Variables>)datoVariable;
                        if (Variable[0].Activado == 1)
                        {
                            ////  Lista de inttra
                            var inttra = BL.envioXML(id, user);
                        }
                        //var inttra = BL.envioXML(id, user);
                        Correcto = "1";
                        retorno.result = Correcto;
                    }
                    else
                    {
                        Correcto = "2";
                        retorno.result = Correcto;
                        retorno.imo = Booking.ImoInttra;
                    }
                }
                else
                {
                    Correcto = "2";
                    retorno.result = Correcto;
                    retorno.imo = Booking.ImoInttra;
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/ConfirmarBooking");
            }
            return Ok(retorno);
        }
        #endregion

        #region Envio Odoo
        [HttpPost]
        public async Task<ActionResult> EnvioOdoo(int id, string nroContenedor, string codigoBL)
        {
            Resultado resultado = new Resultado();

            try
            {
                string UserEnvio = HttpContext.User.Identity.Name;
                BL.UpdateNumeroBL(id, codigoBL);
                //BL_Reserva BL = new BL_Reserva();
                var listaTipoContenedor = new OdooService(_configuration, _memoryCache).ListarTipoContenedor();
                var res2 = BL.BL_ListaBooking(id, "");

                List<InsertBooking> bookingTOoddos = (List<InsertBooking>)res2;
                InsertBooking bookingTOoddo = bookingTOoddos[0];

                BookingOdoo bookingOdoo = new BookingOdoo();
                bookingOdoo.CorreoCliente = bookingTOoddo.UsuarioCreacion;
                bookingOdoo.IdCommodity = bookingTOoddo.IdCommodity;
                bookingOdoo.CodigoNaviera = bookingTOoddo.CodLinea;
                bookingOdoo.IdPuertoCarga = bookingTOoddo.IdPuertoEmbarque;
                bookingOdoo.IdPuertoDescarga = bookingTOoddo.IdPuertoDescarga;
                bookingOdoo.CantidadContenedor = Convert.ToInt32(bookingTOoddo.CantidadBulto);
                //bookingOdoo.ETA_Origen =  DateTime.ParseExact(bookingTOoddo.ETA, Formato.FechaGeneral, null);
                bookingOdoo.ETA_Origen = bookingTOoddo.ETA;
                //bookingOdoo.ETD_Origen = DateTime.ParseExact(bookingTOoddo.ETD, Formato.FechaGeneral, null);
                bookingOdoo.ETD_Origen = bookingTOoddo.ETD;
                //bookingOdoo.ETA_Destino = DateTime.ParseExact(bookingTOoddo.ETA_Destino, Formato.FechaGeneral, null);
                bookingOdoo.ETA_Destino = bookingTOoddo.ETA;
                var EntTipo = listaTipoContenedor.Where(x => x.Nombre.Contains(bookingTOoddo.TipoContenedor)).ToList();
                bookingOdoo.IdTipoContenedor = EntTipo[0].Id;
                bookingOdoo.Notas = bookingTOoddo.Nota;
                //bookingOdoo.IdTerminaDepositoVacio = 0;
                bookingOdoo.NombreNave = bookingTOoddo.NombreNave;
                bookingOdoo.NumeroViaje = bookingTOoddo.NroViaje;
                bookingOdoo.Consignatario = bookingTOoddo.ConsignatarioNombre;
                bookingOdoo.IdDetalleCotizacionOdoo = bookingTOoddo.IdDetalleCotizacionOdoo;
                bookingOdoo.Notas = bookingTOoddo.Nota;
                bookingOdoo.Ventilacion = Convert.ToDouble(bookingTOoddo.Ventilacion);
                bookingOdoo.NumeroBooking = bookingTOoddo.NroBooking;
                bookingOdoo.NumeroContenedor = nroContenedor;
                bookingOdoo.CodigoBL = codigoBL;

                if (bookingTOoddo.ClaseContenedor.ToLower().Contains("reefer"))
                {
                    bookingOdoo.Ventilacion = Convert.ToDouble(bookingTOoddo.Ventilacion);
                    bookingOdoo.AtmosferaControlada = Convert.ToBoolean(bookingTOoddo.AtmosferaControlada);
                    bookingOdoo.CO2 = (Double)bookingTOoddo.CO2;
                    bookingOdoo.O2 = (Double)bookingTOoddo.O2;
                    bookingOdoo.ColdTreatment = Convert.ToBoolean(bookingTOoddo.ColdTreatment);

                    if (bookingTOoddo.TipoTemperatura.ToLower().Contains("c"))
                        bookingOdoo.TipoTemperatura = TipoTemperatura.Celsius;
                    else if (bookingTOoddo.TipoTemperatura.ToLower().Contains("f"))
                        bookingOdoo.TipoTemperatura = TipoTemperatura.fahrenheit;

                    bookingOdoo.Temperatura = (Double)bookingTOoddo.Temperatura;

                    if (bookingTOoddo.TipoVentilacion.ToLower().Trim().Contains("cbm"))
                        bookingOdoo.TipoVentilacion = TipoVentilacion.cubic_meters;
                    else if (bookingTOoddo.TipoVentilacion.ToLower().Trim().Contains("othe"))
                        bookingOdoo.TipoVentilacion = TipoVentilacion.other;

                    bookingOdoo.Ventilacion = (Double)bookingTOoddo.Ventilacion;

                    if (bookingTOoddo.TipoHumedad == 0)
                        bookingOdoo.TipoHumedad = TipoHumedad.off;
                    else if (bookingTOoddo.TipoHumedad == 1)
                        bookingOdoo.TipoHumedad = TipoHumedad.on;

                    bookingOdoo.Humedad = (Double)bookingTOoddo.Humedad;

                }
                else
                {
                    if (Enum.TryParse(bookingTOoddo.IdImo.ToString(), out IMO _resultImo))
                    {
                        if(bookingTOoddo.IdImo>0)
                            bookingOdoo.Imo = _resultImo;
                    }
                    bookingOdoo.UN1 = bookingTOoddo.UN1;
                    bookingOdoo.UN2 = bookingTOoddo.UN2;


                }

                bookingOdoo.Peso = Convert.ToDouble(bookingTOoddo.PesoBruto);
                bookingOdoo.NumeroBooking = bookingTOoddo.NroBooking;
                bookingOdoo.ContractOwnerId = Convert.ToString(bookingTOoddo.IdContactOwner);
                bookingOdoo.TipoContrato = bookingTOoddo.tipoContrato;
                bookingOdoo.Tecnologia = bookingTOoddo.Tecnologia;

                var resultOddo = new OdooService(_configuration, _memoryCache).CrearBooking(bookingOdoo);

                if (resultOddo > 0)
                {
                    var res = BL.BL_EstadoEnvioOddobooking(id, UserEnvio);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = bookingTOoddo.ExportadorNombre,
                        NroSolicitud = bookingTOoddo.IdDetalleCotizacionOdoo.ToString(),
                        NroBooking = bookingTOoddo.NroBooking
                    };

                    resultado.nTipoMensaje = (int)TipoMensaje.Correcto;
                    resultado.sMensaje = CorrectoBooking.ENVIO_ODOO_CORRECTO;

                    await netMailSender.sendMailAsync(bookingTOoddo.UsuarioCreacion, emailTemplate, TipoEmail.BOOKING_3_GENERADO);
                }
                else
                {
                    resultado.nTipoMensaje = (int)TipoMensaje.Error;
                    resultado.sMensaje = ErroresBooking.ENVIO_ODOO_ERROR;
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Booking/EnvioOdoo");
                string cad = ex.Message;
                resultado.nTipoMensaje = (int)TipoMensaje.Error;
                resultado.sMensaje = ex.Message;
                return Ok(resultado);
            }
            return Ok(resultado);
        }

        IMO? GetImo(int value)
        {
            IMO? respuesta;
            switch (value)
            {
                case 1:respuesta = IMO.imo1;break;
                case 2:respuesta = IMO.imo2;break;
                case 3:respuesta = IMO.imo3;break;
                case 4:respuesta = IMO.imo4;break;
                case 5:respuesta = IMO.imo5;break;
                case 6:respuesta = IMO.imo6;break;
                case 7:respuesta = IMO.imo7;break;
                case 8:respuesta = IMO.imo8;break;
                case 9:respuesta = IMO.imo9; break;
                default: respuesta = null; break;
            }

            return respuesta;
        }
        #endregion
        
        public ActionResult GetExcel(int id)
        {
            string rutaTemplate = Path.Combine(this._environment.WebRootPath, "files/BookingTemplate.xlsx");

            try
            {
                var data = BL.BL_DatosExcelBooking();
                if (data.Rows.Count > 0)
                {
                    var excelBinary = new Funciones().GetExcelBooking(data, rutaTemplate);
                    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    var fileName = $"Booking{DateTime.Now.ToString("ddMMyyyy")}.xlsx";
                    return File(excelBinary, contentType, fileName);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/PDF_CotizacionMaritimo");
            }

            return Ok("No hay archivo");
        }

    }
}