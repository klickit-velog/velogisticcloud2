﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models.Alertas;
using VELogisticCloud.Servicio.Business.Alertas;
using VELogisticCloud.Servicio.Middleware;
using VELogisticCloud.Model.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class AlertasController : Controller
    {
        private static List<BE_Alertas_Contrato> StaticListaAlertas = new List<BE_Alertas_Contrato>();
        private readonly UserManager<ApplicationUser> _userManager;

        //AlertasController

        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public AlertasController(ILogger<AlertasController> logger, UserManager<ApplicationUser> userManager, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _userManager = userManager;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        public IActionResult Busqueda()
        {
            return View();
        }

        public IActionResult Mapa()
        {
            return View();
        }

        class Result
        {
            public string htmlGrid { get; set; }
            public int gris { get; set; }
            public int rojo { get; set; }
            public int amarillo { get; set; }
            public int verde { get; set; }
        }

        [HttpPost]
        public ActionResult ListaAlertas()
        {
            Result dato = new Result();
            string htmlGrid = "";
            BL_Alertas BL = new BL_Alertas(_configuration);

            try
            {
                var res = BL.BL_Lista_Alertas();
                List<BE_Alertas_Contrato> lista = (List<BE_Alertas_Contrato>)res;
                StaticListaAlertas = lista;
                var correo = HttpContext.User.Identity.Name;
                List<ContratoDetalle> listaContratos = new OdooService(_configuration, _memoryCache).ListarContratos(correo, 0, 0, 0, 0,  0, 100000,true);
                BE_Alertas_Contrato ent = new BE_Alertas_Contrato();


                var listaIdContratos = lista.Select(x => x.IdContrato);

                foreach (var list in listaContratos)
                {
                    if (listaIdContratos.Contains(list.IdContrato))
                    {

                    }
                    else
                    {
                        ent.FechaIni = list.ValidoDesde.HasValue ? list.ValidoDesde.Value : default;
                        ent.FechaFin = list.ValidoHasta.HasValue ? list.ValidoHasta.Value : default;
                        ent.IdContrato = list.IdContrato;
                        ent.Linea = list.NombreNaviera;
                        ent.Tipo = "Contrato";
                        ent.Servicio = "Contrato";
                        ent.bTipo = 1;
                        ent.NroContrato = list.NumeroContrato;
                        ent.DiasVencimiento = 5;
                        BL.InsertarDataAlertas(ent);
                    }
                    
                }

                htmlGrid = crearGridShow(lista);
                dato.htmlGrid = htmlGrid;
                dato.verde = lista.Where(x=>x.IdEstado == 1).Count();
                dato.amarillo = lista.Where(x => x.IdEstado == 2).Count();
                dato.rojo = lista.Where(x => x.IdEstado == 3).Count();
                dato.gris = lista.Where(x => x.IdEstado == 4).Count();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Alertas/ListaAlertas");
            }
            return Ok(dato);
        }

        class Retorno
        {
            public int cantidad { get; set; }
            public string nombre { get; set; }
            public string user { get; set; }
        }

        [HttpPost]
        public async Task<ActionResult> CantidadAlertas()
        {
            var usuario = "";
            var cliente = "";
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var datos = await _userManager.GetClaimsAsync(user);
            var cotizacionOdoo = new CotizacionOdoo();
            Result dato = new Result();
            Retorno retorno = new Retorno();
            BL_Alertas BL = new BL_Alertas(_configuration);
            try
            {
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                var idCliente = datos[0].Type;
                var NomCliente = datos[0].Value;
                cliente = NomCliente;
                //usuario = HttpContext.User.Identity.Name;
                }
                else
                {
                    cliente = HttpContext.User.Identity.Name;
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes().Where(x=>x.Correo == cliente).FirstOrDefault();
                    if(listarClientes == null)
                    {
                        cliente = HttpContext.User.Identity.Name;
                    }
                    else
                    {
                        cliente = listarClientes.Nombre;
                    }
                    usuario = HttpContext.User.Identity.Name;
                }
         
                int cantidad = 0;

                //var res = BL.BL_Lista_Alertas();
                //List<BE_Alertas_Contrato> lsta = (List<BE_Alertas_Contrato>)res;
                //cantidad = lsta.Count;
                retorno.cantidad = cantidad;
                retorno.nombre = cliente;
                retorno.user = user.Email;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Alertas/ListaAlertas");
                retorno.cantidad = -1;
            }
            return Ok(retorno);
        }


        [HttpPost]
        public ActionResult BusquedaAlertas(int idestado,string tipo)
        {
            Result dato = new Result();
            string htmlGrid = "";

            try
            {
                List<BE_Alertas_Contrato> lista = new List<BE_Alertas_Contrato>();
                lista = StaticListaAlertas;
                if (idestado != 0)
                {
                    lista = lista.Where(x => x.IdEstado == idestado).ToList();
                }
                if (tipo != "0")
                {
                    lista = lista.Where(x => x.Tipo == tipo).ToList();
                }

                htmlGrid = crearGridShow(lista);
                dato.htmlGrid = htmlGrid;
                dato.verde = StaticListaAlertas.Where(x => x.IdEstado == 1).Count();
                dato.amarillo = StaticListaAlertas.Where(x => x.IdEstado == 2).Count();
                dato.rojo = StaticListaAlertas.Where(x => x.IdEstado == 3).Count();
                dato.gris = StaticListaAlertas.Where(x => x.IdEstado == 4).Count();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Alertas/BusquedaAlertas");
            }
            return Ok(dato);
        }




        public string crearGridShow(List<BE_Alertas_Contrato> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            //List<BE_Alertas_Contrato> lista = (List<BE_Alertas_Contrato>)list;

            foreach (var lis in lista)
            {
                #region html

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");


                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-8\">");
                htmlShow.Append("<b> " + lis.NroContrato +"  - Doc. Contrato </b>");
                htmlShow.Append("</div>");
                htmlShow.Append("<br/>");
                htmlShow.Append("<div class=\"col-4 icon\">");
                if(lis.IdEstado == 1)
                {
                    htmlShow.Append("<b> Estado:</b> <i class=\"far fa-bell  bg-green fa-1x \"></i> Por Atender");
                }
                else if (lis.IdEstado == 2)
                {
                    htmlShow.Append("<b> Estado:</b> <i class=\"far fa-bell  bg-yellow fa-1x \"></i> Por Vencer");
                }
                else if (lis.IdEstado == 3)
                {
                    htmlShow.Append("<b> Estado:</b> <i class=\"far fa-bell  bg-red fa-1x\"></i> Atencíon Urgente");
                }
                else if (lis.IdEstado == 4)
                {
                    htmlShow.Append("<b> Estado:</b> <i class=\"far fa-bell  bg-grey fa-1x\" style=\"background-color:#adb5bd\"></i> Leído");
                }
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("<br/>");

                //htmlShow.Append("<div class=\"row\">");
                //htmlShow.Append("<div class=\"col-11\">");
                //htmlShow.Append(lis.Descripcion);
                //htmlShow.Append("</div>");
                //htmlShow.Append("</div>");
                //htmlShow.Append("<br/>");

                htmlShow.Append("<div class=\"row\">");

                htmlShow.Append("<div class=\"col-1\">");
                htmlShow.Append("<b> Tipo: </b>");
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-1\">");
                htmlShow.Append(lis.Tipo);
                htmlShow.Append("</div>");
                //htmlShow.Append("<div class=\"col-1\">");
                //htmlShow.Append("<b> Servicio: </b>");
                //htmlShow.Append("</div>");
                //htmlShow.Append("<div class=\"col-1\">");
                //htmlShow.AppendFormat("<a href=\"#\" class=\"col-12 text-center\"  onclick='VerFlete({0});'>"+ lis.Servicio + "</a>", lis.Id_Alerta);
                //htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-1\">");
                htmlShow.Append("<b> Líneas: </b>");
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-3\">");
                htmlShow.Append(lis.Linea);
                htmlShow.Append("</div>");
                htmlShow.Append("<br/>");

                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");

                htmlShow.Append("<div class=\"col-1\">");
                htmlShow.Append("<b> Fecha Fin: </b>");
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-1\">");
                htmlShow.Append(lis.FechaFin.ToString("dd/MM/yyyy"));
                htmlShow.Append("</div>");

                var dias = (lis.FechaFin.Date - DateTime.Now.Date);
                
                htmlShow.Append("<div class=\"col-1\">");
                htmlShow.Append("<b> Vence en: </b>");
                htmlShow.Append("</div>");
                htmlShow.Append("<div class=\"col-2\">");
                htmlShow.Append(dias.Days +" dias");
                htmlShow.Append("</div>");

                htmlShow.Append("</div>");

                if (lis.IdEstado != 4)
                {
                    htmlShow.Append("<div class=\"card-footer\">");
                    htmlShow.Append("<button type=\"submit\" onclick=\"Marcar(" + lis.Id_Alerta + "); return false;\" class=\"btn btn-primary float-right\">Marcar como Leído</button>");
                    htmlShow.Append("</div>");
                }
                   


                //< b > Nombre de Item N45 - doc contrato </ b > esta por vencer
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");


            }


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult MarcarLeido(int id)
        {
            try
            {
                BL_Alertas BL = new BL_Alertas(_configuration);
                var cod = BL.MarcarLeido(id);
                return Ok("1");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Alertas/MarcarLeido");
            }
            return Ok("0");
        }
    }
}