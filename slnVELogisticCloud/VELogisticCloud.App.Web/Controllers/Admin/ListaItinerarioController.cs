﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Servicio.Business.Itinerario;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class ListaItinerarioController : Controller
    {
        private readonly ILogger _logger;
        public ListaItinerarioController(ILogger<ListaItinerarioController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult ObtenerSolicitud()
        {
            
            //Resultado retorno = new Resultado();
            string htmlGrid = "";

            try
            {
                //BL_Itinerario BL = new BL_Itinerario();
                //var res = BL.L_Itinerario();
                //htmlGrid = crearGridShow(res);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error ListaItinerario/ObtenerSolicitud");

            }
            return Ok(htmlGrid);
        }




        public string crearGridShow(object listObras)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");
            decimal num = 0;
            //if (listObras != null && listObras.Count > 0)
            //{

            //    listObras.ForEach(x =>
            //    {
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<table class=\"table table-hover\">");
                    htmlShow.Append("<tbody>");

                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\"><h5>HAMBURG SUD</h5></div>");
                    htmlShow.Append("<div class=\"row\">carrier</div>");
                    htmlShow.Append("<div class=\"row\">Transbordo: Directo</div>");
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Contract:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\">Yes</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Expiration:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>12/11/20</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">TT:</div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><b>3 dias</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td style=\"border-top:none;\">");
                    htmlShow.Append("<div class=\"row\"><b>ETD 04 - Dec - 2020</b></div>");
                    htmlShow.Append("<div class=\"row\">(Fri)</div>");
                    htmlShow.Append("<div class=\"row\">Lima, PERU</div>");
                    htmlShow.Append("<div class=\"row\">Cut-off: 04-Dec-2020 : 14:00hrs</div>");
                    htmlShow.Append("</td>");

                    htmlShow.Append("<td style=\"border-top:none;\"><h2 class=\"display-4\">></h2></td>");

                    htmlShow.Append("<td style=\"border-top:none;\">");
                    htmlShow.Append("<div class=\"row\"><b>ETD 25 - Dec - 2020</b></div>");
                    htmlShow.Append("<div class=\"row\">(Fri)</div>");
                    htmlShow.Append("<div class=\"row\">Antwerpen, BELGIUM</div>");
                    htmlShow.Append("<div class=\"row\">PSA / NOORDZEL TERMINAL / (QUAYS 901-913)</div>");
                    htmlShow.Append("</td>");


                    htmlShow.Append("</tr>");


                    htmlShow.Append("<tr>");

                    htmlShow.Append("<td style=\"border-top:none;\"><a href=\"#\">Ver detalles</a></td>");
                    htmlShow.Append("<td style=\"border-top:none;\" colspan=\"5\">");
                    htmlShow.Append("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"location.href = 'itinerario-solicitar.html';\" > Solicitar Booking</button>");
                    htmlShow.Append("</td>");


                    htmlShow.Append("</tr>");


                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");



            //    });
              


            //}

            //else
            //{
            //    htmlShow.Append("<tr>");
            //    htmlShow.Append("<td colspan='5'>");
            //    htmlShow.Append("<center><b>No se encontraron registros de Productos.</b></center>");
            //    htmlShow.Append("</td>");
            //    htmlShow.Append("</tr>");

            //}
            //htmlShow.Append("</table>");
            return htmlShow.ToString();

        }


    }
}