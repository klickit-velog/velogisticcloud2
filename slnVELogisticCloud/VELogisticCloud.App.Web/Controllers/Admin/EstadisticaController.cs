﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.Servicio.Business.Estadistica;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class EstadisticaController : Controller
    {
        private readonly ILogger<EstadisticaController> logger;
        private IConfiguration _configuration;
        BL_Estadistica bl;
        public EstadisticaController(ILogger<EstadisticaController> logger, IConfiguration configuration)
        {
            this.logger = logger;
            _configuration = configuration;
            bl = new BL_Estadistica(_configuration);
        }
        //Estadistica
       

        public IActionResult Index(int id)
        {
            //logger.LogWarning("mensaje de prueba");
            return View();
        }

        [HttpPost]
        public ActionResult ResumenBooking()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_ResumenBooking(nombre);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/ResumenBooking");
                return Ok("error - " + ex.Message);
            }
        }

        [HttpPost]
        public ActionResult DatosCotizacion()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_DatosEstadisticaCotizacion(nombre); 
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/DatosCotizacion");
                return Ok("error - " + ex.Message);
            }
        }

        [HttpPost]
        public ActionResult DatosReserva()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_DatosEstadisticaReserva(nombre); 
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/DatosReserva");
                return Ok("error - " + ex.Message);

            }
        }

        [HttpPost]
        public ActionResult DatosReservaInttra()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_DatosEstadisticaReservaInttra(nombre);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/DatosReservaInttra");
                return Ok("error - " + ex.Message);
            }
        }

        [HttpPost]
        public ActionResult DatosCotLogistica()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_EstadisticaCotizacionLogistica(nombre);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/DatosCotLogistica");
                return Ok("error - " + ex.Message);
            }
        }

        [HttpPost]
        public ActionResult DatosOperacionLogistica()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_EstadisticaOperacionLogistica(nombre);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/DatosOperacionLogistica");
                return Ok("error - " + ex.Message);
            }
        }

        [HttpPost]
        public ActionResult DatosOperacionLogisticaOddo()
        {
            try
            {
                string nombre = HttpContext.User.Identity.Name;
                var datos = bl.BL_EstadisticaOperacionLogisticaOddo(nombre);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error Estadistica/DatosOperacionLogisticaOddo");
                return Ok("error - " + ex.Message);
            }
        }

    }
}