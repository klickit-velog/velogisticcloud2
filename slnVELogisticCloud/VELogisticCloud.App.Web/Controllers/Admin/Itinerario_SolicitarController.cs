﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.Models;
using VELogisticCloud.Models.ViewModel;
using VELogisticCloud.Servicio.Business.Reservas;
using VELogisticCloud.Servicio.Data.Repository.Itinerario_Solicitar;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.Admin
{
    public class Itinerario_SolicitarController : Controller
    {

        //Itinerario_Solicitar
        //private readonly IContenedorItinearioSolicitud _IContenedorItinearioSolicitud;

        private static List<Ent_Reserva> listaContenedor = new List<Ent_Reserva>();
        private static int CodReserva = 0;

        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public Itinerario_SolicitarController(ILogger<Itinerario_SolicitarController> logger, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _logger = logger;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));



            var ListaCliente = new List<SelectListItem>();
            var Cliente = new SelectListItem();
            var correo = HttpContext.User.Identity.Name;
            var datos = new OdooService(_configuration, _memoryCache).getCliente(correo);
            Cliente.Value = datos.Id.ToString();
            Cliente.Text = datos.Nombre;
            Cliente.Selected = true;
            ListaCliente.Add(Cliente);

            model.ListaClientes = ListaCliente;
            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;

            return View(model);
        }

        [HttpGet]
        public IActionResult CreateFromCoti(int id)
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var ListaCliente = new List<SelectListItem>();
            var Cliente = new SelectListItem();
            var correo = HttpContext.User.Identity.Name;
            var datos = new OdooService(_configuration, _memoryCache).getCliente(correo);
            Cliente.Value = datos.Id.ToString();
            Cliente.Text = datos.Nombre;
            Cliente.Selected = true;
            ListaCliente.Add(Cliente);

            model.ListaClientes = ListaCliente;
            model.ListaNavieras = listaNavieras;
            model.ListaCommodity = listaCommodity;
            model.ListaCampania = listaCampania;
            model.ListaTipoContenedor = listaTipoContenedor;

            return View(model);
        }

        [HttpGet]
        public IActionResult Create2(
            string scac,
            string puertoEmbarque,
            string puertoDescarga,
            string etd,
            string eta,
            string nombreViaje,
            string naveViaje
            )
        {
            var reserva = new ReservaViewModel();
            reserva.CodigoNaviera = scac;
            reserva.CodigoPuertoEmbarque = puertoEmbarque;
            reserva.CodigoPuertoDescarga = puertoDescarga;
            reserva.ETD = etd;
            reserva.ETA = eta;
            reserva.NombreViaje = nombreViaje;
            reserva.NaveViaje = naveViaje;
            ModelState.Clear();

            //var modelo = new ItinerarioViewModel();
            //var listItems = new List<SelectListItem>();
            //new OdooService(_configuration).ListarNavieras().ForEach(a => listItems.Add(new SelectListItem()
            //{
            //    Value = a.Id.ToString(),
            //    Text = a.Nombre
            //}));

            //modelo.ListaNavieras = listItems;

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaCommodity = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCommodity().ForEach(a => listaCommodity.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCampania = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCampanias().ForEach(a => listaCampania.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaTipoContenedor = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTipoContenedor().ForEach(a => listaTipoContenedor.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));


            var ListaCliente = new List<SelectListItem>();
            var Cliente = new SelectListItem();
            var correo = HttpContext.User.Identity.Name;
            var datos = new OdooService(_configuration, _memoryCache).getCliente(correo);
            Cliente.Value = datos.Id.ToString();
            Cliente.Text = datos.Nombre;
            Cliente.Selected = true;
            ListaCliente.Add(Cliente);

            reserva.ListaClientes = ListaCliente;
            reserva.ListaNavieras = listaNavieras;
            reserva.ListaCommodity = listaCommodity;
            reserva.ListaCampania = listaCampania;
            reserva.ListaTipoContenedor = listaTipoContenedor;


            var listaNavierasOdoo = new OdooService(_configuration, _memoryCache).ListarNavieras(codigo: scac);
            var usuario = User.Identity.Name;
            var usuario2 = HttpContext.User.Identity.Name;

            if (listaNavierasOdoo.Any())
            {
                var contratosEncontrados = new OdooService(_configuration, _memoryCache).ListarContratos(usuario,idNaviera:listaNavierasOdoo.First().Id);
                if (contratosEncontrados.Any())
                {
                    reserva.NumeroContrato = contratosEncontrados.OrderByDescending(a => a.IdContrato).First().NumeroContrato;
                }
            }

            return View(reserva);
        }

        [HttpGet]
        public IActionResult Resumen()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult InsertarDatos(Ent_Reserva ent, List<Ent_Reserva> contenedor)
        //{

        //    if (ent.Consignatario == null || ent.Consignatario == "undefined")
        //    {
        //        ent.Consignatario = string.Empty;
        //    }
        //    try
        //    {
        //        object res;
        //        string result = "";
        //        BL_Reserva BL = new BL_Reserva();
        //        ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
        //        ent.Codigo_Usuario = HttpContext.User.Identity.Name;
        //        if (contenedor.Count > 1)
        //        {
        //            foreach (var lis in contenedor)
        //            {
        //                ent.Cantidad = lis.Cantidad;
        //                ent.Commodity = lis.Commodity;
        //                ent.Descripcion_Carga = lis.Descripcion_Carga;
        //                ent.Tipo_Contenedor = lis.Tipo_Contenedor;

        //                ent.Temperatura = lis.Temperatura;
        //                ent.Ventilacion = lis.Ventilacion;
        //                ent.Humedad = lis.Humedad;
        //                ent.Peso = lis.Peso;
        //                ent.Volumen = lis.Volumen;
        //                ent.Id_Contenedor = lis.Id_Contenedor;
        //                ent.Codigo_Commodity = lis.Codigo_Commodity;
        //                var isFechaETA = DateTime.TryParse(ent.ETA,out DateTime FechaETA);
        //                var isFechaETD = DateTime.TryParse(ent.ETD,out DateTime FechaETD);
        //                if (isFechaETA)
        //                {
        //                    ent.ETA = DateTime.ParseExact(ent.ETA, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //                }
        //                else
        //                {
        //                    ent.ETA = null;
        //                }
        //                if (isFechaETD)
        //                {
        //                    ent.ETD = DateTime.ParseExact(ent.ETD, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //                }
        //                else
        //                {
        //                    ent.ETD = null;
        //                }
        //                ent.Nota = "0";
        //                res = BL.InsertReserva(ent);
        //                result = (string)res;

        //            }
        //        }
        //        else
        //        {
        //            ent.Nota = "";
        //            var isFechaETA = DateTime.TryParseExact(ent.ETA, Formato.FechaGeneral, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime FechaETA);
        //            var isFechaETD = DateTime.TryParseExact(ent.ETD, Formato.FechaGeneral, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime FechaETD);
        //            if (isFechaETA)
        //            {
        //                ent.ETA = FechaETA.ToString("dd/MM/yyyy");
        //            }
        //            else
        //            {
        //                ent.ETA = null;
        //            }
        //            if (isFechaETD)
        //            {
        //                ent.ETD = FechaETD.ToString("dd/MM/yyyy");
        //            }
        //            else
        //            {
        //                ent.ETD = null;
        //            }
        //            res = BL.InsertReserva(ent);
        //            result = (string)res;

        //        }
        //        CodReserva = Convert.ToInt32(result);
        //        if (CodReserva > 0)
        //        {
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            return Ok("0");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "Error Itinerario_Solicitar/InsertarDatos");
        //        return Ok("0");

        //    }

        //}

        //[HttpPost]
        //public ActionResult InsertarDatosItinerario(Ent_Reserva ent, List<Ent_Reserva> contenedor)
        //{
        //    if (ent.Consignatario == null || ent.Consignatario == "undefined")
        //    {
        //        ent.Consignatario = string.Empty;
        //    }
        //    try
        //    {
        //        object res;
        //        string result = "";
        //        BL_Reserva BL = new BL_Reserva();
        //        ent.IdUsuarioCreacion = HttpContext.User.Identity.Name;
        //        ent.Codigo_Usuario = HttpContext.User.Identity.Name;
        //        if (contenedor.Count > 1)
        //        {
        //            foreach (var lis in contenedor)
        //            {
        //                ent.Cantidad = lis.Cantidad;
        //                ent.Commodity = lis.Commodity;
        //                ent.Descripcion_Carga = lis.Descripcion_Carga;
        //                ent.Tipo_Contenedor = lis.Tipo_Contenedor;

        //                ent.Temperatura = lis.Temperatura;
        //                ent.Ventilacion = lis.Ventilacion;
        //                ent.Humedad = lis.Humedad;
        //                ent.Peso = lis.Peso;
        //                ent.Volumen = lis.Volumen;
        //                ent.Id_Contenedor = lis.Id_Contenedor;
        //                ent.Codigo_Commodity = lis.Codigo_Commodity;

        //                var isFechaETA = DateTime.TryParse(ent.ETA, out DateTime FechaETA);
        //                var isFechaETD = DateTime.TryParse(ent.ETD, out DateTime FechaETD);
        //                if (isFechaETA)
        //                {
        //                    ent.ETA = DateTime.ParseExact(ent.ETA, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //                }
        //                else
        //                {
        //                    ent.ETA = null;
        //                }
        //                if (isFechaETD)
        //                {
        //                    ent.ETD = DateTime.ParseExact(ent.ETD, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //                }
        //                else
        //                {
        //                    ent.ETD = null;
        //                }

        //                //ent.ETA = DateTime.ParseExact(ent.ETA, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //                //ent.ETD = DateTime.ParseExact(ent.ETD, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //                ent.Nota = "0";
        //                res = BL.InsertReserva(ent);
        //                result = (string)res;

        //            }
        //        }
        //        else
        //        {
        //            var PuertoEmbarque = new OdooService(_configuration).ListarPuerto(0, 1000, "", ent.Codigo_Puerto_Embarque, 0);
        //            var PuertoDescarga = new OdooService(_configuration).ListarPuerto(0, 1000, "", ent.Codigo_Puerto_Descarga, 0);

        //            ent.Codigo_Pais_Embarque = PuertoEmbarque[0].CodigoPais;
        //            ent.Codigo_Pais_Descarga = PuertoDescarga[0].CodigoPais;
        //            ent.Id_Puerto_Carga = PuertoEmbarque[0].Id;
        //            ent.Id_Puerto_Descarga = PuertoDescarga[0].Id;
        //            ent.Pais_Puerto_Embarque = PuertoEmbarque[0].NombrePais;
        //            ent.Pais_Puerto_Descarga = PuertoDescarga[0].NombrePais;

        //            var isFechaETA = DateTime.TryParse(ent.ETA, out DateTime FechaETA);
        //            var isFechaETD = DateTime.TryParse(ent.ETD, out DateTime FechaETD);
        //            if (isFechaETA)
        //            {
        //                ent.ETA = DateTime.ParseExact(ent.ETA, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //            }
        //            else
        //            {
        //                ent.ETA = null;
        //            }
        //            if (isFechaETD)
        //            {
        //                ent.ETD = DateTime.ParseExact(ent.ETD, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //            }
        //            else
        //            {
        //                ent.ETD = null;
        //            }

        //            //ent.ETA = DateTime.ParseExact(ent.ETA, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //            //ent.ETD = DateTime.ParseExact(ent.ETD, Formato.FechaGeneral, null).ToString("dd/MM/yyyy");
        //            ent.Nota = "0";
        //            res = BL.InsertReserva(ent);
        //            result = (string)res;

        //        }
        //        CodReserva = Convert.ToInt32(result);

        //        return Ok(result);

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "Error Itinerario_Solicitar/InsertarDatosItinerario");
        //        return Ok("");
        //    }

        //}


        [HttpPost]
        public ActionResult Inicializar(List<Ent_Reserva> contenedor)
        {

            try
            {
                if (contenedor.Count > 0)
                {
                    listaContenedor = new List<Ent_Reserva>();

                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Itinerario_Solicitar/Inicializar");
            }
            return Ok("");

        }

        //[HttpPost]
        //public ActionResult CargarResumen(int id)
        //{

        //    try
        //    {
        //        if (id > 0)
        //        {
        //            BL_Reserva BL = new BL_Reserva();
        //            var res = BL.Resumen(id);
        //            List<Resumen> lsta = (List<Resumen>)res;
        //            Resumen ent = (Resumen)lsta[0];
        //            return Ok(ent);
        //        }
        //        else
        //        {

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "Error Itinerario_Solicitar/CargarResumen");
        //    }
        //    return Ok("");

        //}

        //[HttpPost]
        //public ActionResult CargarDatosCoti(int id)
        //{
        //    try
        //    {
        //      BL_Reserva BL = new BL_Reserva(_configuration);
        //      var res = BL.BL_DetalleMaritimo(id);
        //        List<Ent_DetalleMaritimo> lsta = (List<Ent_DetalleMaritimo>)res;
        //        Ent_DetalleMaritimo ent = (Ent_DetalleMaritimo)lsta[0];
        //      return Ok(ent);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "Error Itinerario_Solicitar/CargarDatosCoti");
        //    }
        //    return Ok("");

        //}

    }
}