﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VELogisticCloud.Models;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.App.Web.Controllers.Seguridad
{
    public class PerfilController : Controller
    {
        private readonly IContenedorTrabajo _contenedorTrabajo;

        public PerfilController(IContenedorTrabajo contenedorTrabajo)
        {
            _contenedorTrabajo = contenedorTrabajo;
        }

        public IActionResult Index()
        {
            return View("~/Views/Seguridad/Perfil/Index.cshtml");
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("~/Views/Seguridad/Perfil/Create.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Perfil entidad)
        {
            if (ModelState.IsValid)
            {
                entidad.IdUsuarioRegistro = 1;
                entidad.FechaRegistro = DateTime.Now;

                //entidad.IdUsuarioActualiza = 1;
                //entidad.FechaActualizacion = DateTime.Now;

                _contenedorTrabajo.Perfil.Add(entidad);
                _contenedorTrabajo.Save();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Seguridad/Perfil/Create.cshtml",entidad);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Perfil entidad = _contenedorTrabajo.Perfil.Get(id);
            if (entidad == null)
            {
                return NotFound();
            }

            return View("~/Views/Seguridad/Perfil/Edit.cshtml",entidad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Perfil entidad)
        {
            if (ModelState.IsValid)
            {
                _contenedorTrabajo.Perfil.Update(entidad);
                _contenedorTrabajo.Save();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Seguridad/Perfil/Edit.cshtml",entidad);
        }


        #region llamadas API

        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _contenedorTrabajo.Perfil.GetAll() });
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var objFromDb = _contenedorTrabajo.Perfil.Get(id);
            if (objFromDb == null)
            {
                return Json(new { success = false, message = "Error eliminando perfil" });
            }

            _contenedorTrabajo.Perfil.Remove(objFromDb);
            _contenedorTrabajo.Save();
            return Json(new { success = true, message = "Puerto borrado satisfactoriamente." });
        }

        #endregion
    }
}
