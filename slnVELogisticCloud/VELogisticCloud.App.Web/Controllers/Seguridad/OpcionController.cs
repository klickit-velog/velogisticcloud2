﻿using Microsoft.AspNetCore.Mvc;

namespace VELogisticCloud.App.Web.Controllers.Seguridad
{
    public class OpcionController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View("~/Views/Seguridad/Opcion/Index.cshtml");
        }

        [HttpGet]
        public IActionResult Registrar()
        {
            return View("~/Views/Seguridad/Opcion/Registro.cshtml");
        }

        [HttpGet]
        public IActionResult Editar()
        {
            return View("~/Views/Seguridad/Opcion/Registro.cshtml");
        }

    }
}
