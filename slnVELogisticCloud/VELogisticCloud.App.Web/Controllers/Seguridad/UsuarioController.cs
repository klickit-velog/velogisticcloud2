﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.Models.ViewModel;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Data.Repository;

namespace VELogisticCloud.App.Web.Controllers.Seguridad
{
    [Authorize(Roles = Rol.ADMINISTRADOR)]
    public class UsuarioController : Controller
    {
        private readonly IContenedorTrabajo _contenedorTrabajo;

        public UsuarioController(IContenedorTrabajo contenedorTrabajo)
        {
            _contenedorTrabajo = contenedorTrabajo;
        }

        [HttpGet]
        public IActionResult Index()
        {
            UsuarioViewModel model = new UsuarioViewModel();

            model.Estados = new EstadoRepository().GetListaEstado().ToList();

            return View("~/Views/Seguridad/Usuario/Index.cshtml", model);
        }

        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> ListarUsuario(UsuarioDTO usuario)
        {

            EntidadConsultada<UsuarioDTO> response = new EntidadConsultada<UsuarioDTO>();

            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();

            response = await _contenedorTrabajo.Usuario.ListarUsuarios(usuario,
                                                        start != null ? Convert.ToInt32(start) : 0,
                                                        length != null ? Convert.ToInt32(length) : 0);
            response.Lista.ForEach(a => a.NombreEstado = "Activo");

            //foreach (var item in response.Lista)
            //{
            //    if (item.Email == "admin@mail.com")
            //        item.NombreRol = "Administrador";
            //    else if (item.Email == "customer@mail.com")
            //        item.NombreRol = "Customer Service";
            //    else
            //        item.NombreRol = "Usuario";
            //}

            return Json(new
            {
                draw,
                recordsFiltered = response.Lista.Count(),
                recordsTotal = response.TotalRegistros,
                data = response.Lista
            });
        }
    }
}
