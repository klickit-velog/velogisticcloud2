﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VELogisticCloud.Models.Security;

namespace VELogisticCloud.App.Web.Controllers.Seguridad
{
    public class RolController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public RolController(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("~/Views/Seguridad/Perfil/Create.cshtml");
        }

        [HttpPost]
        public async Task<IActionResult> Create(RolViewModel model)
        {

            IdentityRole identityRole = new IdentityRole()
            {
                Name = model.Nombre
            };

            var result = await _roleManager.CreateAsync(identityRole);

            return View("~/Views/Seguridad/Perfil/Create.cshtml", model);
        }
    }
}
