﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using VELogisticCloud.App.Web.Models.Autenticacion;
using VELogisticCloud.App.Web.Models.Seguridad;
using VELogisticCloud.CrossCutting.Comun;
//using VELogisticCloud.CrossCutting.Entidad.Seguridad;
//using VELogisticCloud.Servicio.Business.Seguridad;

namespace VELogisticCloud.App.Web.Controllers.Seguridad
{
    public class UsuarioController_old : Controller
    {
        /*
        private readonly UserManager<IdentityUser<int>> _userManager;
        private readonly SignInManager<IdentityUser<int>> _signInManager;
        private readonly IEstadoService oIEstadoService;
        private readonly IUsuarioService oIUsuarioService;

        public UsuarioController(SignInManager<IdentityUser<int>> signInManager,
                                 UserManager<IdentityUser<int>> userManager,
                                 IEstadoService IEstadoService,
                                 IUsuarioService IUsuarioService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            oIEstadoService = IEstadoService;
            oIUsuarioService = IUsuarioService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Autenticar()
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return View("~/Views/Autenticacion/Login.cshtml");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Autenticar(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return View("~/Views/Home/Index.cshtml");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Email o Password incorrectos. No se puede iniciar sesión.");
                    return View("~/Views/Autenticacion/Login.cshtml");
                }
            }
            return View("~/Views/Autenticacion/Login.cshtml");
        }

        [HttpGet]
        public async Task<IActionResult> Salir()
        {
            await _signInManager.SignOutAsync();

            //_logger.LogInformation("User logged out.");
            return RedirectToAction("Autenticar");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index()
        {
            UsuarioViewModel model = new UsuarioViewModel();

            model.Estados = await oIEstadoService.Listar(Constantes.SEG_Usuario);
            model.Estados.Insert(0, new Estado { IdEstado = -1, Nombre = "Seleccione" });

            return View("~/Views/Seguridad/Usuario/Index.cshtml", model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Consultar(Usuario usuario)
        {

            EntidadConsultada<Usuario> response = new EntidadConsultada<Usuario>();

            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();

            response = await oIUsuarioService.Consultar(usuario,
                                                        start != null ? Convert.ToInt32(start) : 0,
                                                        length != null ? Convert.ToInt32(length) : 0);
            return Json(new
            {
                draw,
                recordsFiltered = response.Lista.Count(),
                recordsTotal = response.TotalRegistros,
                data = response.Lista
            });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Registrar()
        {
            UsuarioRegistroViewModel model = new UsuarioRegistroViewModel();

            model.IdEstado = -1;
            model.Estados = await oIEstadoService.Listar(Constantes.SEG_Usuario);
            model.Estados.Insert(0, new Estado { IdEstado = -1, Nombre = "Seleccione" });
            model.FechaRegistro = DateTime.Now.ToShortDateString();

            return View("~/Views/Seguridad/Usuario/Registro.cshtml", model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Registrar(UsuarioRegistroViewModel usuario)
        {

            bool registro;

            if (ModelState.IsValid)
            {
                var user = new IdentityUser<int> { UserName = usuario.Login, Email = usuario.Email };
                var result = await _userManager.CreateAsync(user, usuario.Password);
                if (result.Succeeded)
                {
                    registro = await oIUsuarioService.Registrar(new Usuario
                    {
                        Login = usuario.Login,
                        Apellidos = usuario.Apellidos,
                        Nombres = usuario.Nombres,
                        TelefonoMovil = usuario.TelefonoMovil,
                        IdEstado = usuario.IdEstado
                    });
                    if (registro)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error al realizar el registro del usuario.");
                        return View("~/Views/Seguridad/Usuario/Registro.cshtml");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error al realizar el registro del usuario.");
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return View("~/Views/Seguridad/Usuario/Registro.cshtml");
                }
            }
            return View("~/Views/Seguridad/Usuario/Registro.cshtml");
        }
        */
    }
}
