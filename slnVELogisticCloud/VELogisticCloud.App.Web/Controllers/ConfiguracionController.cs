﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Comun;

namespace VELogisticCloud.App.Web.Controllers
{
    public class ConfiguracionController : Controller
    {
        private readonly IMemoryCache _memoryCache;

        public ConfiguracionController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CacheRestart()
        {
            foreach (CacheTipo tipoCache in (CacheTipo[]) Enum.GetValues(typeof(CacheTipo)))
            {
                _memoryCache.Remove(tipoCache);
            }
            return Ok();
        }
    }
}
