﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VELogisticCloud.App.Web.Clases;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.CrossCutting.Mail;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.Models;
using VELogisticCloud.Models.ApiAvanzza;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Models.General;
using VELogisticCloud.Models.OperacionLogistica;
using VELogisticCloud.Models.Seguimiento;
using VELogisticCloud.Servicio.Business.Booking;
using VELogisticCloud.Servicio.Business.General;
using VELogisticCloud.Servicio.Business.OperacionLogistica;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.CustomerService
{
    public class OperacionLogistica2Controller : Controller
    {
        //OperacionLogistica
        private readonly ILogger _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private IConfiguration _configuration;
        private IHostingEnvironment _environment;
        private INetMailSender netMailSender;
        private readonly IMemoryCache _memoryCache;
        BL_OperacionLogistica2 BL;
        public OperacionLogistica2Controller(ILogger<OperacionLogistica2Controller> logger, 
            UserManager<ApplicationUser> userManager, IConfiguration configuration, IHostingEnvironment environment, INetMailSender _netMailSender, IMemoryCache memoryCache)
        {
            _logger = logger;
            _userManager = userManager;
            _configuration = configuration;
            _environment = environment;
            netMailSender = _netMailSender;
            _memoryCache = memoryCache;
            BL = new BL_OperacionLogistica2(_configuration);
        }
        public IActionResult Create()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaTransporte = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTransporte().ForEach(a => listaTransporte.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCondicionPago = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCondicionPago().ForEach(a => listaCondicionPago.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            model.ListaNavieras = listaNavieras;
            model.ListaTransporte = listaTransporte;
            model.ListaCondicionPago = listaCondicionPago;
            //model.ListaCommodity = listaCommodity;
            //model.ListaCampania = listaCampania;
            //model.ListaTipoContenedor = listaTipoContenedor;
            //model.Cliente = datos.Nombre;

            return View(model);
        }

        public IActionResult Editar()
        {
            ModelCombos model = new ModelCombos();

            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre
            }));

            var listaTransporte = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarTransporte().ForEach(a => listaTransporte.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));

            var listaCondicionPago = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarCondicionPago().ForEach(a => listaCondicionPago.Add(new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Nombre
            }));
            model.ListaNavieras = listaNavieras;
            model.ListaCondicionPago = listaCondicionPago;
            model.ListaTransporte = listaTransporte;

            return View(model);
        }

        public IActionResult Busqueda()
        {
            return View();
        }
        public IActionResult SeguimientoOpl()
        {
            return View();
        }

        public IActionResult BusquedaCliente()
        {
            return View();
        }

        #region Crear y Actualizar Opl
        [HttpPost]
        public async Task<ActionResult> InsertarOperacionLogistica(BE_OperacionLogistica2 ent,string time1,string time2,string tipousuario)
        {
            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    ent.Codigo_Usuario = Convert.ToString(idCliente);
                    ent.UsuarioCreacion = HttpContext.User.Identity.Name;
                }
                else
                {
                    if (tipousuario != "customer")
                    {
                        ent.Codigo_Usuario = HttpContext.User.Identity.Name;
                        ent.UsuarioCreacion = HttpContext.User.Identity.Name;
                    }
                    else
                    {
                        ent.Codigo_Usuario = "";
                        return Ok("0-No se puede grabar con un usuario diferente a Cliente.");
                    }
                }
                var Esfecha1 = EsFecha(time1);
                var Esfecha2 = EsFecha(time2);
                if (Esfecha1)
                {
                    DateTime hora1 = Convert.ToDateTime(time1);
                    ent.CitaPacking = ent.CitaPacking.AddHours(hora1.Hour).AddMinutes(hora1.Minute).AddSeconds(hora1.Second);
                }
                if (Esfecha2)
                {
                    DateTime hora2 = Convert.ToDateTime(time2);
                    ent.HoraInspeccionSenasa = Convert.ToDateTime(ent.HoraInspeccionSenasa).AddHours(hora2.Hour).AddMinutes(hora2.Minute).AddSeconds(hora2.Second);
                }
                else
                {
                    ent.HoraInspeccionSenasa = null;
                }
                ent.OrdenServicio = "";
                ent.UsuarioCreacion = HttpContext.User.Identity.Name;
                var res = BL.BL_InsertarOperacionLogistica(ent);
                int Codigo = Convert.ToInt32(res);
                if (Codigo > 0)
                {
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(ent.UsuarioCreacion);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroBooking = ent.NroBooking.ToString(),
                        Canal = ent.CanalSini
                    };


                    await netMailSender.sendMailAsync(ent.UsuarioCreacion, emailTemplate, TipoEmail.OPL_1_RECIBIDO);
                    return Ok(Codigo);
                }
                else
                {
                    return Ok("0-No se pudo grabar en la base de datos.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error OperacionLogistica2/InsertarOperacionLogistica");
                return Ok("0-No se puede generar la Opl.");
            }
        }

        public static Boolean EsFecha(String fecha)
        {
            try
            {
                DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        public ActionResult UpdateOperacionLogistica(BE_OperacionLogistica2 ent, string time1, string time2)
        {
            try
            {
                DateTime hora1 = Convert.ToDateTime(time1);
                DateTime hora2 = Convert.ToDateTime(time2);
                ent.CitaPacking = ent.CitaPacking.AddHours(hora1.Hour).AddMinutes(hora1.Minute).AddSeconds(hora1.Second);
                ent.HoraInspeccionSenasa = Convert.ToDateTime( ent.HoraInspeccionSenasa).AddHours(hora2.Hour).AddMinutes(hora2.Minute).AddSeconds(hora2.Second);
                ent.OrdenServicio = ent.OrdenServicio == null ? ent.OrdenServicio = "" : ent.OrdenServicio;
                //ent.Fecha1 = ent.Fecha1 ==null ? "" : DateTime.ParseExact(ent.Fecha1, Formato.FechaGeneral, null).ToString("yyyy/MM/dd");
                //ent.Fecha2 = ent.Fecha2 ==null ? "" :DateTime.ParseExact(ent.Fecha2, Formato.FechaGeneral, null).ToString("yyyy/MM/dd");
                //ent.Fecha3 = ent.Fecha3 ==null ? "":DateTime.ParseExact(ent.Fecha3, Formato.FechaGeneral, null).ToString("yyyy/MM/dd");
                //ent.ETA = ent.ETA ==null ? "" :DateTime.ParseExact(ent.ETA, Formato.FechaGeneral, null).ToString("yyyy/MM/dd");
                //ent.ETD = ent.ETD ==null ? "":DateTime.ParseExact(ent.ETD, Formato.FechaGeneral, null).ToString("yyyy/MM/dd");
                ent.UsuarioModificacion = HttpContext.User.Identity.Name;
                var res = BL.BL_UpdateOperacionLogistica(ent);
                int Codigo = Convert.ToInt32(res);
                if (Codigo > 0)
                {
                    return Ok(Codigo);
                }
                else
                {
                    return Ok("0-No se puede actualizar los datos de la opl.");
                }
                //var resultado = new OdooService(_configuration).RelacionarCotizacionDetalleBooking(ent.IdCotizacionLogistica, ent.NroBooking, ent.NroContenedor);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/InsertarOperacionLogistica");
                return Ok("0-No se puede actualizar los datos de la opl.");
            }
        }

        #endregion

        #region Lista Operacion-Logistica Customer
        public ActionResult DetalleCotizacion(int id)
        {
            object data = BL.BL_DetalleCotizacion(id);
            return Ok(data);
        }

        [HttpPost]
        public async Task<ActionResult> ListaVehiculos()
        {
            string nombre = "";
            try
            {
                RestApiAvanzza api = new RestApiAvanzza(_configuration);

                var response = await api.GetVehiculos();
                var listaCategoriasMagento = JsonSerializer.Deserialize<Vehiculos>(response.message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/IniciarDatos");
            }
            return Ok(nombre);
        }

        [HttpPost]
        public async Task<ActionResult> ListaOperacionLogistica(BE_OperacionLogistica2 param, string tipousuario)
        {
            string htmlGrid = "";
            try
            {
                string Codigo_Usuario = "";
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    Codigo_Usuario = Convert.ToString(idCliente);
                }
                else
                {
                    if (tipousuario != "customer")
                    {
                        Codigo_Usuario = HttpContext.User.Identity.Name;
                    }
                    else
                    {
                        Codigo_Usuario = "";
                    }
                }

                //BL_Cotizacion BL = new BL_Cotizacion();
                List<BE_OperacionLogistica2> ListaTemporal = new List<BE_OperacionLogistica2>();
                List<BE_OperacionLogistica2> lista = new List<BE_OperacionLogistica2>();
                List<string> ListadoUsuarios = new List<string>();

                if (Codigo_Usuario == "")
                {
                    //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                    var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientesPorCustomer(correoCustomerServiceSecundario: HttpContext.User.Identity.Name);

                    var listaLimpia = listarClientes.Where(x => x.Correo != null && x.Correo != "").ToList().Select(x => x.Correo).ToList();
                    ListadoUsuarios.AddRange(listaLimpia);

                    var res1 = BL.BL_ListaOperacionLogistica(0, " ", ListadoUsuarios);
                    ListaTemporal = (List<BE_OperacionLogistica2>)res1;
                    lista.AddRange(ListaTemporal);

                    //foreach (var cliente in listarClientes)
                    //{
                    //    if (cliente.Correo != null && cliente.Correo != "")
                    //    {
                    //        var res1 = BL.BL_ListaOperacionLogistica(0, cliente.Correo);
                    //        ListaTemporal = (List<BE_OperacionLogistica2>)res1;
                    //        lista.AddRange(ListaTemporal);
                    //    }
                    //}
                }
                else
                {
                    ListadoUsuarios.Add(Codigo_Usuario);
                    var res2 = BL.BL_ListaOperacionLogistica(0, Codigo_Usuario, ListadoUsuarios);
                    lista = (List<BE_OperacionLogistica2>)res2;
                }


                if (param.NroBooking !="" && param.NroBooking != null )
                {
                    lista = lista.Where(x => x.NroBooking == param.NroBooking).ToList();
                }
                if (param.IdPuertoEmbarque > 0)
                {
                    lista = lista.Where(x => x.IdPuertoEmbarque == param.IdPuertoEmbarque).ToList();
                }
                if (param.IdPuertoDescarga > 0)
                {
                    lista = lista.Where(x => x.IdPuertoDescarga == param.IdPuertoDescarga).ToList();
                }
                if (param.IdEstadoVE > 0)
                {
                    lista = lista.Where(x => x.IdEstadoVE == param.IdEstadoVE).ToList();
                }

                htmlGrid = crearGridShowLogistica(lista.OrderByDescending(x => x.IdOperacionLogistica).ToList());
                //htmlGrid = crearGridShowLogistica(lista);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogisticaController/ListaOperacionLogistica");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShowLogistica(List<BE_OperacionLogistica2> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            //BL_Booking b2 = new BL_Booking(_configuration, _memoryCache);
            //var res = b2.BL_ListaBooking(0,"");
            //List<InsertBooking> lista2 = (List<InsertBooking>)res;
            //InsertBooking booking = new InsertBooking();
            if(lista.Count > 0)
            {
                foreach (var lis in lista)
                {
                    #region html
                    //var re = b2.ListaBookingXNroBooking(0, lis.NroBooking.ToLower().Trim());
                    //List<InsertBooking> listaa = (List<InsertBooking>)re;
                    //booking = listaa.FirstOrDefault();
                    //if (booking == null)
                    //{
                    //    booking = new InsertBooking();
                    //}
                    ////List<Lista_Maritimo> listadetalle =(List<Lista_Maritimo>)listd;
                    ////Ent_Reserva ent = (Ent_Reserva)lis;

                    ////campana.Where(x => x.)
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<table  class=\"table \">");
                    htmlShow.Append("<tbody>");
                    htmlShow.Append("<th style=\"width:20%\">");
                    htmlShow.Append("<th style=\"width:35%\">");
                    htmlShow.Append("<th style=\"width:20%\">");
                    htmlShow.Append("<th style=\"width:15%\">");
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border-top:none;\" ><h5>" + "Codigo Opl: " + lis.IdOperacionLogistica + "</h5><h5>" + "Tipo Contenedor:" + lis.TipoContenedor + "</h5></td>");

                    //htmlShow.Append("<td style=\"border-top:none; \"><h4>" + lis.IdOperacionLogistica + "</h4>Codigo Opl</td>");
                    //htmlShow.Append("<td style=\"border-top:none; \"><h4>" + booking.TipoContenedor + "</h4>Tipo Contenedor</td>");
                    htmlShow.Append("<td style=\"border-top:none; \"><h5>" + lis.ExportadorNombre + "</h5>Cliente</td>");
                    htmlShow.Append("<td style=\"border-top:none; \"><h5>" + lis.Commodity + "</h5>Commodity</td>");
                    htmlShow.Append("<td style=\"border-top:none; \"><h5>" + lis.Flete + "</h5>Monto</td>");
                    //htmlShow.Append("<td style=\"border-top:none; \"><h5>" + booking.TipoContenedor + "</h5>Tipo Contenedor</td>");
                    //htmlShow.Append("<td style=\"border-top:none;\"class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> Deposito de vacios</td>");
                    htmlShow.Append("</tr>");
                    htmlShow.Append("<tr>");

                    #region Filas
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\" >Booking </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.NroBooking + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Nro Viaje </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.NroViaje + "</b></div>");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-6\">Canal </div>");
                    //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Canal + "</b></div>");
                    //htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Linea </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Linea + "</b></div>");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-6\">Deposito </div>");
                    //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Deposito + "</b></div>");
                    //htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Packing </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.CitaPacking + "</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");


                    //segunda fila 
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\" >Operador Logístico </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.OperadorLogistico + "</b></div>");
                    htmlShow.Append("</div>");

                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-6\">BL </div>");
                    //htmlShow.Append("<div class=\"col-6 text-right \"  ><b> " + lis.NroBL + "</b></div>");
                    //htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Nave </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.NombreNave + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Terminal de retiro de vacio </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.PuertoDescarga + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Orden de Servicio </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.OrdenServicio + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Canal Sini </div>");
                    string canalSini = lis.CanalSini == "green" ? "Verde" : lis.CanalSini == "red" ? "Rojo" : lis.CanalSini;
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + canalSini  + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Transporte </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Transporte + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("</td>");

                    //tercera fila 
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-6\" >ETD </div>");
                    //htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.et + "</b></div>");
                    //htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">ETA </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b> " + lis.ETA.ToString("dd/MM/yyyy") + "</b></div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Estado </div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><i class=\"fas fa-hourglass-half\"></i> " + lis.EstadoVE + " </div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Condicion de pago </div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><i class=\"fas fa-hourglass-half\"></i> " + lis.CondicionPago + " </div>");
                    htmlShow.Append("</div>");

                    if (lis.OrdenServicio != null && lis.OrdenServicio.Trim() !="")
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append($"<a href=\"#\" class=\"col-12 text-center\"  onclick=\"VerMapa('{lis.OrdenServicio.Trim()}');\">Ver en mapa</a>");
                        htmlShow.Append("</div>");
                    }


                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.AppendFormat("<a class=\"col-12 text-center\" href=\" ../OperacionLogistica2/GetAduanasExcel/" + lis.IdOperacionLogistica + " \">Excel Aduanero</a>");
                    htmlShow.Append("</div>");

                    if (lis.IdEstadoVE != 4)
                    {
                        htmlShow.Append("<div class=\"row\" style=\"padding-left: 35%\">");
                        htmlShow.AppendFormat("<button class=\"btn btn-primary col-md-6 float-left form-control\"  onclick='EditarOpl({0});'>Editar Opl</button>", lis.IdOperacionLogistica);
                        htmlShow.Append("</div>");
                    }

                    //quinta fila 
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-12 text-center\" > <b>UBICACIÓN ACTUAL</b> </div>");
                    //htmlShow.Append("</div>");



                    if (lis.IdEstadoVE == 1)
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='ConfirmarOpl({0});'> <i class=\"fa fa-check-circle fa-3x\" style=\"color: green\"></i>  </a><br></div>", lis.IdOperacionLogistica.ToString());
                        htmlShow.Append("</div>");
                        htmlShow.Append("</br>");
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='CancelarOpl({0});'> <i class=\"fa fa-times fa-3x\" style=\"color: red\" ></i>  </a><br></div>", lis.IdOperacionLogistica.ToString());
                        htmlShow.Append("</div>");
                    }
                    if (lis.IdEstadoVE == 3)
                    {
                        htmlShow.Append("<div style=\"padding-top:20px \" class=\"row col-8 \">");
                        htmlShow.AppendFormat("<button onclick =\"EnvioOdoo({0}); return false\" class=\"col-12 text-left btn btn-primary float-right\">Enviar OPL a Odoo </button>", lis.IdOperacionLogistica);
                        htmlShow.Append("</div>");
                    }

                    htmlShow.Append("</td>");
                    #endregion

                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border-top:none;\"  colspan=\"4\" class=\"text - right\">");
                    htmlShow.AppendFormat("<a href=\"#\" onclick='CancelarOpl({0});'>Cancelar Operacion Logistica</a>", lis.IdOperacionLogistica.ToString());
                    htmlShow.Append("</tr>");

                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");


                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");

                    #endregion

                }
            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }
            




            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        [HttpPost]
        public ActionResult ListarTransporte()
        {
            Resultado retorno = new Resultado();
            try
            {
                    var datos = new OdooService(_configuration, _memoryCache).ListarTransporte()
                    .Select(c => new SelectListItem
                    {
                        //Value = Convert.ToString(c.PAR_SUBTYPE),
                        Value = Convert.ToString(c.Id),
                        Text = c.Nombre
                    });
                    retorno.result = 1;
                    retorno.data = Json(datos, System.Web.Mvc.JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

            }
            return Ok(retorno);
            //return Json(retorno, System.Web.Mvc.JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public async Task<ActionResult> GetTransporte(string ordenServicio)
        {
            Resultado retorno = new Resultado();
            retorno.result = 0;

            try
            {
                var nombreTransporte = await new RestApiAvanzza(_configuration).GetTransporte(ordenServicio);
                
                retorno.data = Json(nombreTransporte.message, System.Web.Mvc.JsonRequestBehavior.AllowGet);
                retorno.result = 1;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/GetTransporte");
            }

            return Ok(retorno);
        }
        #endregion

        #region Lista Operacion-Logistica Cliente
        [HttpPost]
        public async Task<ActionResult> ListaOperacionLogisticaCliente(BE_OperacionLogistica2 param,string tipousuario)
        {
            string htmlGrid = "";
            try
            {
                string Codigo_Usuario = "";
                var user = await _userManager.GetUserAsync(HttpContext.User);
                var datos = await _userManager.GetClaimsAsync(user);
                //TODO: validar usuario sea customer.
                if (datos.Count > 0)
                {
                    var idCliente = datos[0].Type;
                    var NomCliente = datos[0].Value;
                    Codigo_Usuario = Convert.ToString(idCliente);
                }
                else
                {
                    if (tipousuario != "customer")
                    {
                        Codigo_Usuario = HttpContext.User.Identity.Name;
                    }
                    else
                    {
                        Codigo_Usuario = "";
                    }
                }


                //BL_Cotizacion BL = new BL_Cotizacion();
                List<string> ListadoUsuarios = new List<string>();
                ListadoUsuarios.Add(Codigo_Usuario);

                var res = BL.BL_ListaOperacionLogistica(0, Codigo_Usuario, ListadoUsuarios);
                List<BE_OperacionLogistica2> lista = (List<BE_OperacionLogistica2>)res;
                if (param.NroBooking != "" && param.NroBooking != null)
                {
                    lista = lista.Where(x => x.NroBooking == param.NroBooking).ToList();
                }
                if (param.IdPuertoEmbarque > 0)
                {
                    lista = lista.Where(x => x.IdPuertoEmbarque == param.IdPuertoEmbarque).ToList();
                }
                if (param.IdPuertoDescarga > 0)
                {
                    lista = lista.Where(x => x.IdPuertoDescarga == param.IdPuertoDescarga).ToList();
                }
                if (param.IdEstadoVE > 0)
                {
                    lista = lista.Where(x => x.IdEstadoVE == param.IdEstadoVE).ToList();
                }

                htmlGrid = crearGridShowLogisticaCliente(lista);
                //htmlGrid = "";
                //var LISTA = BL.ListaSolicitud();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogisticaController/ListaOperacionLogistica");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShowLogisticaCliente(List<BE_OperacionLogistica2> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            BL_Booking b2 = new BL_Booking(_configuration, _memoryCache);
            //var res = b2.BL_ListaBooking(0,"");

            //List<InsertBooking> lista2 = (List<InsertBooking>)res;
            InsertBooking booking = new InsertBooking();

            if (lista.Count > 0)
            {

                var emailusuario = lista[0].Codigo_Usuario;
                var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correo: emailusuario);

                //var listarClientes = new OdooService(_configuration, _memoryCache).ListarClientes(correo: lista[0].UsuarioCreacion);

                foreach (var lis in lista)
                {
                    #region html
                    var re = b2.ListaBookingXNroBooking(0, lis.NroBooking.ToLower().Trim());
                    List<InsertBooking> listaa = (List<InsertBooking>)re;
                    booking = listaa.FirstOrDefault();
                    if (booking == null)
                    {
                        booking = new InsertBooking();
                    }
              
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-md-12\">");
                    htmlShow.Append("<div class=\"card\">");
                    htmlShow.Append("<div class=\"card-body\">");

                    htmlShow.Append("<table  class=\"table \">");
                    htmlShow.Append("<tbody>");
                    htmlShow.Append("<th style=\"width:25%\">");
                    htmlShow.Append("<th style=\"width:20%\">");
                    htmlShow.Append("<th style=\"width:20%\">");
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border-top:none;\"><h5>" + "Codigo Opl: " + lis.IdOperacionLogistica + "</h5><h5>" + "Tipo Contenedor:" + booking.TipoContenedor + "</h5></td>");
                    //htmlShow.Append("<td style=\"border-top:none; \"><h4>" + booking.TipoContenedor + "</h4>Tipo de contenedor</td>");
                    htmlShow.Append("<td style=\"border-top:none; \"><h4>" + booking.Commodity + "</h4>Commodity</td>");
                    htmlShow.Append("<td style=\"border-top:none; \"><h4>" + listarClientes[0].Nombre + "</h4>Cliente</td>");
                    //htmlShow.Append("<td style=\"border-top:none;\"class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> Deposito de vacios</td>");
                    htmlShow.Append("</tr>");
                    htmlShow.Append("<tr>");

                    #region Filas
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\" >Booking </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.NroBooking + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Nro Viaje </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + booking.NroViaje + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Linea </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.Linea + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Terminal de Retiro de Vacio </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.DepositoVacio + "</b></div>");
                    htmlShow.Append("</div>");
              
                    htmlShow.Append("</td>");


                    //segunda fila 
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Packing </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + lis.CitaPacking + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\" >Operador Logístico </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b>" + "VE CLOUD " + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Orden de Servicio </div>");
                    htmlShow.Append("<div class=\"col-6 text-right \"  ><b> " + lis.OrdenServicio + "</b></div>");
                    htmlShow.Append("</div>");

                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.Append("<div class=\"col-6\">Estado </div>");
                    htmlShow.Append("<div class=\"col-6 text-right\"><i class=\"fas fa-hourglass-half\"></i> " + lis.EstadoVE + " </div>");
                    htmlShow.Append("</div>");

                    //quinta fila 
                    htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\" >");
                    //htmlShow.Append("<div class=\"row\">");
                    //htmlShow.Append("<div class=\"col-12 text-center\" > <b>UBICACIÓN ACTUAL</b> </div>");
                    //htmlShow.Append("</div>");

                    if (lis.OrdenServicio != null && lis.OrdenServicio.Trim() != "")
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append($"<a href=\"#\"  onclick=\"VerMapa('{lis.OrdenServicio.Trim()}');\">Ver en mapa</a>");
                        htmlShow.Append("</div>");
                    }
            
                    if(lis.IdEstadoVE == 1)
                    {
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<br>");
                        htmlShow.AppendFormat("<a href=\"#\" onclick='EditReserva({0});'>Modificar Opl</a>", lis.IdOperacionLogistica.ToString());
                        htmlShow.Append("</div>");
                    }
                
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.AppendFormat("<a href=\" ../OperacionLogistica2/GetAduanasExcel/" + lis.IdOperacionLogistica + " \">Excel Aduanero</a>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("<br>");
                    htmlShow.Append("</td>");
                    #endregion
                    htmlShow.Append("</tr>");

                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border-top:none;\"  colspan=\"4\" class=\"text - right\">");
                    htmlShow.AppendFormat("<a href=\"#\" onclick='CancelarOpl({0});'>Cancelar Operacion Logistica</a>", lis.IdOperacionLogistica.ToString());
                    //htmlShow.AppendFormat("<img src='/Imagenes/delete.png'  title='Eliminar Cotización'  onclick='DeleteReserva({0});'  style='cursor: hand' > &nbsp;&nbsp;"

                    htmlShow.AppendFormat("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../OperacionLogistica2/SeguimientoOpl?id={0}'; \">Ver Detalles</button>", lis.IdOperacionLogistica.ToString());

                    htmlShow.Append("</tr>");

                    htmlShow.Append("</tbody>");
                    htmlShow.Append("</table>");

                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                    htmlShow.Append("</div>");
                }
                #endregion

                //if (listObras != null && listObras.Count > 0)
                //{
            }


            return htmlShow.ToString();

        }

        #endregion

        #region Datos Operacion Logistica
        [HttpPost]
        public ActionResult DatosOpl(int id)
        {
            BE_OperacionLogistica2 ent = new BE_OperacionLogistica2();
            try
            {
                List<string> ListadoUsuarios = new List<string>();

                var res = BL.BL_ListaOperacionLogistica(id,"", ListadoUsuarios);
                List<BE_OperacionLogistica2> lista = (List<BE_OperacionLogistica2>)res;
                ent = lista[0];
                return Ok(ent);

            }
            catch (Exception ex)
            {
                _logger.LogError( ex , "Error OperacionLogistica2/DatosOpl");
                return Ok("0");
            }
        }
        #endregion

        #region Cancelar , Confirmar OperacionLogistica y Actualizar datos obligatorios

        [HttpPost]
        public async Task<ActionResult> UpdateDatosObligatorios(string OrdenServicio,string Transporte,string CanalSini, int id)
        {

            //Resultado retorno = new Resultado();
            string result = "0";
            string user = HttpContext.User.Identity.Name;
            try
            {
                var res = BL.BL_UpdateOplDatosObligatorios(OrdenServicio, Transporte, CanalSini, id);
                List<string> ListadoUsuarios = new List<string>();

                var lis = BL.BL_ListaOperacionLogistica(id, "",ListadoUsuarios);
                List<BE_OperacionLogistica2> lsta = (List<BE_OperacionLogistica2>)lis;
                var ent = lsta[0];

                Validadores val = new Validadores();
                var resultValidacion = val.ValidarUpdateDatosObligatorios(ent);

                if (resultValidacion.Any())
                {
                    result = string.Join(", ", resultValidacion);
                    return Ok(result); 
                }

                //TODO: ENVIAR DATOS A ODOO.
                var resultOddo = new OdooService(_configuration, _memoryCache).
                    RelacionarCotizacionDetalleBooking(ent.IdCotizacionOddo, ent.NroBooking, "0", ent.IdCondicionPago, ent.Transporte, ent.CitaPacking, ent.CanalSini);

                if (resultOddo)
                {
                    BL.BL_Confirmar_OPL(id);
                    string canalSini = ent.CanalSini == "green" ? "Verde" : ent.CanalSini == "red" ? "Rojo" : ent.CanalSini;
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(ent.UsuarioCreacion);

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroBooking = ent.NroBooking,
                        Canal = canalSini
                    };

                    await netMailSender.sendMailAsync(ent.UsuarioCreacion, emailTemplate, TipoEmail.OPL_3_CANAL);
                    result = "1";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/UpdateDatosObligatorios");
            }
            return Ok(result);
        }

        [HttpPost]
        public ActionResult CancelarOperacionLogistica(int id)
        {

            //Resultado retorno = new Resultado();
            string htmlGrid = "1";
            string user = HttpContext.User.Identity.Name;
            //string user = HttpContext.User.Claims;
            try
            {
                var res = BL.BL_CancelarOperacionLogistica(id, user);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/CancelarOperacionLogistica");
            }
            return Ok(htmlGrid);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmarOperacionLogistica(int id)
        {

            string htmlGrid = "1";
            string user = HttpContext.User.Identity.Name;
            try
            {
             
                var resultadoOrdenServicio = CrearOrdenServicio(id);
                if (resultadoOrdenServicio)
                {
                    List<string> ListadoUsuarios = new List<string>();
                    var res = BL.BL_ConfirmarOperacionLogistica(id, user);
                    var res2 = BL.BL_ListaOperacionLogistica(id, "", ListadoUsuarios);
                    List<BE_OperacionLogistica2> lista = (List<BE_OperacionLogistica2>)res2;
                    string NombreCliente = new OdooService(_configuration, _memoryCache).getUserNameByEmail(lista[0].UsuarioCreacion);

                    string canalSini = lista[0].CanalSini == "green" ? "Verde" : lista[0].CanalSini == "red" ? "Rojo" : lista[0].CanalSini;

                    var emailTemplate = new EmailTemplateDTO()
                    {
                        NombreCliente = NombreCliente,
                        NroBooking = lista[0].NroBooking,
                        Canal = canalSini
                    };

                    await netMailSender.sendMailAsync(lista[0].UsuarioCreacion, emailTemplate, TipoEmail.OPL_2_ACEPTADO);
                    htmlGrid = "1";
                }
                else
                {
                    htmlGrid = "0";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/ConfirmarOperacionLogistica");
                htmlGrid = "0";
            }
            return Ok(htmlGrid);
        }

        private bool CrearOrdenServicio(int id)
        {
            var lis = BL.BL_DatosEnvioPedido(id);
            List<BE_DatosPedido> lsta = (List<BE_DatosPedido>)lis;
            var ent = lsta[0];

            var pedido = MapearOPLtoPedidoAvanzza(ent);
            bool resultado = false;

            try
            {
                var api = new RestApiAvanzza(_configuration);
                var resultadoPedido = api.SendPedido(pedido).Result;
                resultado = resultadoPedido.success;
                //resultado = true;
                if (!resultado)
                    _logger.LogError(resultadoPedido.message, "No se pudo crear la OrdenServicioAvanza");
                else
                {
                    var res = BL.BL_UpdateOplDatosObligatorios(id.ToString(), string.Empty, string.Empty, id);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/CrearOrdenServicio");

            }

            return resultado;
        }

        private Servicio.Middleware.Models.Pedido MapearOPLtoPedidoAvanzza(BE_DatosPedido opl)
        { 
            var pedido = new Servicio.Middleware.Models.Pedido();
            
            pedido.code = opl.IdOperacionLogistica.ToString();
            //pedido.client_document = opl.ExportadorRuc;
            pedido.client_name = opl.ExportadorNombre;
            pedido.client_address = string.IsNullOrEmpty(opl.DireccionCliente)?"-": opl.DireccionCliente;
            pedido.client_email = opl.CorreoCliente;
            pedido.client_lat =(float)-11.96376697;
            pedido.client_lng = (float)-77.12792673;

            pedido.custom_properties = new Servicio.Middleware.Models.Custom_Properties();
            pedido.custom_properties.custom_29 = opl.Producto; //
            pedido.custom_properties.custom_30 = opl.RazonSocialPacking; //
            pedido.custom_properties.custom_31 = opl.DireccionPacking; //
            pedido.custom_properties.custom_32 = opl.OperadorLogistico; //
            pedido.custom_properties.custom_33 = opl.NroBooking; //
            pedido.custom_properties.custom_34 = opl.TerminalPortuario; //
            pedido.custom_properties.custom_35 = opl.DepositoVacio; //
            pedido.custom_properties.custom_36 = opl.FechaPacking; //
            pedido.custom_properties.custom_37 = opl.HoraPacking; //
            pedido.custom_properties.custom_38 = opl.FechaSenasa; //
            pedido.custom_properties.custom_39 = opl.HoraSenasa; //
            pedido.custom_properties.custom_40 = opl.NombreNave; //
            pedido.custom_properties.custom_41 = opl.NroViaje; //
            pedido.custom_properties.custom_42 = opl.Linea; //
            pedido.custom_properties.custom_43 = opl.CantidadTermoregistros.ToString(); //
            pedido.custom_properties.custom_44 = opl.CantidadFiltrosEtileno.ToString(); //
            pedido.custom_properties.custom_45 = opl.NombreContactoPacking; //
            pedido.custom_properties.custom_46 = opl.TelefonoContacto; //
            pedido.custom_properties.custom_47 = opl.TipoContrato; //
            pedido.custom_properties.custom_48 = opl.CantidadContenedores.ToString(); //
            pedido.custom_properties.custom_49 = opl.PuertoDestino; //
            pedido.custom_properties.custom_50 = opl.ETD; //
            pedido.custom_properties.custom_51 = opl.TipoEmbarque; //
            pedido.custom_properties.custom_52 = opl.PuertoEmbarque; //
            pedido.custom_properties.custom_53 = opl.Observaciones; //
            pedido.custom_properties.custom_54 = opl.ExportadorRuc; //

            return pedido;
        }

        #endregion

        #region Validaciones y Combos
        [HttpPost]
        public ActionResult ListaValores(string grupo)
        {
            try
            {
                BL_General bl = new BL_General(_configuration);
                var res = bl.BL_ListaValores(grupo);
                List<BE_Valores> Lista = (List<BE_Valores>)res;
                return Ok(Lista);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/ListaValores");
                return Ok("0");

            }
        }

        [HttpPost]
        public ActionResult ValidarBooking(string booking)
        {
            if(string.IsNullOrEmpty(booking)) return Ok("0");

            try
             {
                BL_Booking bl = new BL_Booking(_configuration, _memoryCache);
                var res = bl.ListaBookingXNroBooking(0, booking);
                List<InsertBooking> Lista = (List<InsertBooking>)res;
                InsertBooking ent = new InsertBooking();
                //var BookingOdoo = new OdooService(_configuration).GetBooking(booking.ToUpper());
                //if (BookingOdoo != null)
                //{

                    ent = Lista.Where(x => x.NroBooking.ToLower().Trim() == booking.ToLower().Trim()).ToList().FirstOrDefault();
                    if (ent == null)
                    {
                        ent = new InsertBooking();
                        ent.IdBooking = 0;
                    }
                    return Ok(ent);
                //}
                //else
                //{
                //    return Ok("0");
                //}


                 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/ListaValores");
                return Ok("0");

            }
        }
        #endregion

        #region Seguimiento Opl 
        [HttpPost]
        public ActionResult ListaSeguimiento(int id)
        {
            string htmlGrid = "";
            try
            {
                //BL_Seguimiento BL = new BL_Seguimiento();
                var res = BL.BL_SeguimientoOPL(id);
                htmlGrid = crearGridShowSeguimiento(res);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error OperacionLogistica2/ListaSeguimiento");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShowSeguimiento(object list)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<SeguimientoOPL> lista = (List<SeguimientoOPL>)list;


            //Ent_Reserva ent = (Ent_Reserva)lis;
            if(lista.Count > 0)
            {
                #region html
                htmlShow.Append("<div class=\"container-fluid\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");

                htmlShow.Append("<div class=\"card-header\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-12\">");
                htmlShow.Append("<h4> Seguimiento Flete </h4>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");


                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("<table class=\"table table-hover\" style=\"border:none\">");
                htmlShow.Append("<tbody>");

                foreach (var liss in lista)
                {
                    htmlShow.Append("<tr>");
                    htmlShow.Append("<td style=\"border:none\">");
                    htmlShow.Append("<div class=\"row\">");

                    //htmlShow.Append("<div class=\"col-1\">" + liss.CodEstado);
                    //htmlShow.Append("</div>");
                    if (liss.CodEstado.Equals("on-my-way"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\" >  <img src=\"/dist/img/ICONO_1.png\"> </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("LLEGADA_DV"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_2.png\"> </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("IN_DV"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_3.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("Sali"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_4.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("LLEGADA_P"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_5.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("IN_P"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_6.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("OUT_P"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_7.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("LLEGADA_PO"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_8.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("IN_POL"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_9.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("delivered"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_10.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else if (liss.CodEstado.Equals("OUT_POL"))
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <img src=\"/dist/img/ICONO_11.png\">  </i>");
                        htmlShow.Append("</div>");
                    }
                    else
                    {
                        htmlShow.Append("<div style=\"padding-left: 6%;\"> <i class=\"fas fa-user-secret fa-8x\"></i>");
                        htmlShow.Append("</div>");
                    }

                    htmlShow.Append("<div class=\"col-3\" style=\"padding-left: 10%; padding-top: 3%;\"> <b>Estado: </b> " + liss.DescEstado);
                    htmlShow.Append("</div>");
                    htmlShow.Append("<div class=\"col-4\" style=\"padding-left: 10%; padding-top: 3%;\"> <b>Fecha : </b> " + liss.FechaIni);
                    htmlShow.Append("</div>");
                    //htmlShow.Append("<div class=\"col-4\"> Fecha Fin: " + liss.FechaFin);
                    //htmlShow.Append("</div>");


                    htmlShow.Append("</div>");
                    htmlShow.Append("</td>");
                    htmlShow.Append("</tr>");
                }
                htmlShow.Append("</tbody>");
                htmlShow.Append("</table>");
                htmlShow.Append("</div>");


                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                #endregion
            }
            else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

            }
           

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

        public ActionResult GetAduanasExcel(int id)
        {
            string rutaTemplate = Path.Combine(this._environment.WebRootPath, "files/AduanasTemplate.xlsx");

            try
            {
                var data = new BL_OperacionLogistica2(_configuration).BL_ListarExcelAduanero(id);
                if (data.Rows.Count > 0)
                {
                    var excelBinary = new Funciones().GetExcel(data, rutaTemplate);
                    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    var fileName = $"Aduanas{DateTime.Now.ToString("ddMMyyyy")}.xlsx";
                    return File(excelBinary, contentType, fileName);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/PDF_CotizacionMaritimo");
            }

            return Ok("No hay archivo");
        }

        public ActionResult GetExcelOpl(int id)
        {
            string rutaTemplate = Path.Combine(this._environment.WebRootPath, "files\\OplTemplate.xlsx");

            try
            {
                var data = new BL_OperacionLogistica2(_configuration).DatosExcelOpl();
                if (data.Rows.Count > 0)
                {
                    var excelBinary = new Funciones().GetExcelOpl(data, rutaTemplate);
                    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    var fileName = $"OperacionLogistica{DateTime.Now.ToString("ddMMyyyy")}.xlsx";
                    return File(excelBinary, contentType, fileName);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/PDF_CotizacionMaritimo");
            }

            return Ok("No hay archivo");
        }

        #endregion

    }
}