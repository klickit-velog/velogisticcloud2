﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VELogisticCloud.CrossCutting.Comun;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Servicio.Business.Reservas;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.App.Web.Controllers.CustomerService
{
    [Authorize(Roles = Rol.CUSTOMER_SERVICE)]
    public class ReservasController : Controller
    {
        private readonly ILogger _logger;
        public ReservasController(ILogger<ReservasController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult ListaReservas_CustomerService(Ent_Reserva ent)
        //{

        //    if (ent.Commodity == null)
        //    {
        //        ent.Commodity = "";
        //    }
        //    if (ent.Naviera == null)
        //    {
        //        ent.Naviera = "";
        //    }
        //    if (ent.Puerto_Descarga == null)
        //    {
        //        ent.Puerto_Descarga = "";
        //    }
        //    if (ent.Puerto_Embarque == null)
        //    {
        //        ent.Puerto_Embarque = "";
        //    }
            
        //    //Resultado retorno = new Resultado();
        //    string htmlGrid = "";

        //    try
        //    {
        //        BL_Reserva BL = new BL_Reserva(_configuration);
        //        var res = BL.ReservexID(0, ent.Commodity, ent.Naviera, ent.Puerto_Descarga, ent.Puerto_Embarque, ent.Id_Estado_Reserva, "");
        //        htmlGrid = crearGridShow(res);
        //        //htmlGrid = "";
        //        //var LISTA = BL.ListaSolicitud();
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ReservasController/ListaReservas_CustomerService");
        //    }
        //    return Ok(htmlGrid);
        //}

        //[HttpPost]
        //public ActionResult EnvioOdoo(int id)
        //{
        //    try
        //    {
        //        BL_Reserva BL = new BL_Reserva();
        //        var listaTipoContenedor = new OdooService(_configuration).ListarTipoContenedor();
        //        var res2 = BL.BookingTOoddo(id);
        //        List<BookingTOoddo> bookingTOoddos = (List<BookingTOoddo>)res2;
        //        BookingTOoddo bookingTOoddo = (BookingTOoddo)bookingTOoddos[0];
        //        BookingOdoo bookingOdoo = new BookingOdoo();
        //        bookingOdoo.CorreoCliente = bookingTOoddo.Codigo_Usuario;
        //        bookingOdoo.IdCommodity = bookingTOoddo.Codigo_Commodity;
        //        bookingOdoo.CodigoNaviera = bookingTOoddo.CodigoNaviera;
        //        bookingOdoo.IdPuertoCarga = bookingTOoddo.Id_Puerto_Carga;
        //        bookingOdoo.IdPuertoDescarga = bookingTOoddo.Id_Puerto_Descarga;
        //        bookingOdoo.CantidadContenedor = bookingTOoddo.Cantidad;
        //        bookingOdoo.ETA_Origen = DateTime.ParseExact(bookingTOoddo.ETA, Formato.FechaGeneral, null);

        //        bookingOdoo.ETD_Origen = DateTime.ParseExact(bookingTOoddo.ETD,Formato.FechaGeneral,null);
        //        bookingOdoo.ETA_Destino = DateTime.ParseExact(bookingTOoddo.ETA_Destino, Formato.FechaGeneral, null);
        //        var EntTipo = listaTipoContenedor.Where(x => x.Nombre.Contains(bookingTOoddo.Tipo_Contenedor)).ToList();
        //        bookingOdoo.IdTipoContenedor = EntTipo[0].Id;
        //        bookingOdoo.Notas = bookingTOoddo.Nota;
        //        //bookingOdoo.IdTerminaDepositoVacio = 0;
        //        bookingOdoo.NombreNave = bookingTOoddo.Nombre_Nave;
        //        bookingOdoo.NumeroViaje = bookingTOoddo.NumeroViaje;
        //        bookingOdoo.Consignatario = bookingTOoddo.Consignatario;
        //        bookingOdoo.IdDetalleCotizacionOdoo = bookingTOoddo.NumeroCotizacion;
        //        bookingOdoo.Notas = bookingTOoddo.Descripcion_Carga;
        //        bookingOdoo.Ventilacion = Convert.ToDouble(bookingTOoddo.Ventilacion);
        //        bookingOdoo.NumeroBooking = bookingTOoddo.Nro_Booking;




        //        if (!string.IsNullOrEmpty(bookingTOoddo.Condicion))
        //        {
        //            if (bookingTOoddo.Condicion.ToLower().Trim().Contains("fresh"))
        //                bookingOdoo.Condicion = TipoCondicion.fresh;
        //            else if (bookingTOoddo.Condicion.ToLower().Trim().Contains("frozen"))
        //                bookingOdoo.Condicion = TipoCondicion.frozen;
        //        }
        //        else
        //            bookingOdoo.Condicion = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.Ventilacion))
        //            bookingOdoo.Ventilacion = Convert.ToDouble(bookingTOoddo.Ventilacion);
        //        else
        //            bookingOdoo.Ventilacion = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.TipoVentilacion))
        //        {
        //            if (bookingTOoddo.TipoVentilacion.ToLower().Trim().Contains("cbm"))
        //                bookingOdoo.TipoVentilacion = TipoVentilacion.cubic_meters;
        //            else if (bookingTOoddo.TipoVentilacion.ToLower().Trim().Contains("othe"))
        //                bookingOdoo.TipoVentilacion = TipoVentilacion.other;
        //        }
        //        else
        //            bookingOdoo.TipoVentilacion = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.Atmosfera))
        //        {
        //            if (bookingTOoddo.Atmosfera.ToLower().Contains("si"))
        //            {
        //                bookingOdoo.AtmosferaControlada = true;
        //            }
        //            else if (bookingTOoddo.Atmosfera.ToLower().Contains("no"))
        //            {
        //                bookingOdoo.AtmosferaControlada = false;
        //            }
        //        }


        //        if (!string.IsNullOrEmpty(bookingTOoddo.Temperatura))
        //            bookingOdoo.Temperatura = Double.Parse(bookingTOoddo.Temperatura);
        //        else
        //            bookingOdoo.Temperatura = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.TipoTemperatura))
        //        {
        //            if (bookingTOoddo.TipoTemperatura.ToLower().Contains("c"))
        //            {
        //                bookingOdoo.TipoTemperatura = TipoTemperatura.Celsius;
        //            }
        //            else if (bookingTOoddo.TipoTemperatura.ToLower().Contains("f"))
        //            {
        //                bookingOdoo.TipoTemperatura = TipoTemperatura.fahrenheit;
        //            }
        //        }
        //        else
        //        {
        //            bookingOdoo.TipoTemperatura = null;
        //        }

        //        if (!string.IsNullOrEmpty(bookingTOoddo.Humedad))
        //            bookingOdoo.Humedad = Double.Parse(bookingTOoddo.Humedad);
        //        else
        //            bookingOdoo.Humedad = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.TipoHumedad))
        //        {
        //            if (bookingTOoddo.TipoHumedad.ToLower().Contains("off"))
        //            {
        //                bookingOdoo.TipoHumedad = TipoHumedad.off;
        //            }
        //            else if (bookingTOoddo.TipoHumedad.ToLower().Contains("on"))
        //            {
        //                bookingOdoo.TipoHumedad = TipoHumedad.on;
        //            }
        //        }
        //        else
        //        {
        //            bookingOdoo.TipoHumedad = null;
        //        }

        //        //if (bookingOdoo.TipoHumedad.HasValue)
        //        //{
        //        //    bookingOdoo.TieneHumedad = true;
        //        //}
        //        //else
        //        //{
        //        //    bookingOdoo.TieneHumedad = false;
        //        //}

        //        if (!string.IsNullOrEmpty(bookingTOoddo.CO2))
        //            bookingOdoo.CO2 = Double.Parse(bookingTOoddo.CO2);
        //        else
        //            bookingOdoo.CO2 = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.O2))
        //            bookingOdoo.O2 = Double.Parse(bookingTOoddo.O2);
        //        else
        //            bookingOdoo.O2 = null;

        //        if (!string.IsNullOrEmpty(bookingTOoddo.ColdTreatment))
        //        {
        //            if (bookingTOoddo.ColdTreatment.ToLower().Contains("s"))
        //            {
        //                bookingOdoo.ColdTreatment = true;
        //            }
        //            else if (bookingTOoddo.ColdTreatment.ToLower().Contains("n"))
        //            {
        //                bookingOdoo.ColdTreatment = false;
        //            }
        //        }
        //        else
        //        {
        //            bookingOdoo.ColdTreatment = null;
        //        }

        //        bookingOdoo.Peso = Convert.ToDouble(bookingTOoddo.Peso);

        //        if (!string.IsNullOrEmpty(bookingTOoddo.UN1))
        //            bookingOdoo.UN1 = bookingTOoddo.UN1;
        //        else
        //            bookingOdoo.UN1 = null;


        //        if (!string.IsNullOrEmpty(bookingTOoddo.UN2))
        //            bookingOdoo.UN2 = bookingTOoddo.UN2;
        //        else
        //            bookingOdoo.UN2 = null;


        //        if (Enum.TryParse(bookingTOoddo.ID_IMO, out IMO _resultImo))
        //        {
        //            bookingOdoo.Imo = _resultImo;
        //        }
        //        else
        //        {
        //            bookingOdoo.Imo = null;
        //        }

        //        bookingOdoo.NumeroBooking = bookingTOoddo.Nro_Booking;

        //        var resultOddo = new OdooService(_configuration).CrearBooking(bookingOdoo);

        //        if (resultOddo > 0)
        //        {
        //            //var res = BL.BL_Estado_Envio_Oddo_booking(id);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex,"Error ReservasController/EnvioOdoo");
        //        string cad = ex.Message;
        //        return Ok("0");
        //    }
        //    return Ok("1");
        //}


        [HttpPost]
        public ActionResult ConfirmarReserva(int id)
        {
            try
            {
                //BL_Reserva BL = new BL_Reserva();
                //var res = BL.BL_ConfirmarReserva(id);
                // TODO
                //var envioxml = BL.envioXML(id);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error ReservasController/ConfirmarReserva");
                string cad = ex.Message;
                return Ok("0");

            }
            return Ok("1");
        }

        [HttpPost]
        public ActionResult RechazarReserva(decimal id)
        {
            try
            {
                //BL_Reserva BL = new BL_Reserva();
                //var res = BL.BL_RechazarReserva(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error ReservasController/RechazarReserva");
            }
            return Ok("1");
        }

        public string crearGridShow(object listObras)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            List<Ent_Reserva> lista = (List<Ent_Reserva>)listObras;


            foreach (var lis in lista)
            {
                //Ent_Reserva ent = (Ent_Reserva)lis;

                #region html



                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");

                htmlShow.Append("<table class=\"table table-hover\">");
                htmlShow.Append("<tbody>");

                htmlShow.Append("<tr>");
                string nro_booking = lis.Nro_Booking == null ? " <i>[Pendiente]</i>" : lis.Nro_Booking;
                string nro_id = lis.Id_Reserva.ToString();
                htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\"><h5>" + "Nro. Booking: " + nro_booking + "</h5><h5>" + "Nro. Solicitud:" + nro_id + "</h5></td>");
                htmlShow.Append("<td style=\"border-top:none;\" colspan=\"2\" class=\"text-right\">Estado: <i class=\"fas fa-exclamation-circle\"></i> " + lis.Estado_desc + "</td>");
                htmlShow.Append("</tr>");

                htmlShow.Append("<tr>");

                htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Cantidad contenedores:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.Cantidad + "</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Origen:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Puerto_Embarque+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Nombre de nave:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>" + lis.Nombre_Nave + "</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>ABC</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("</td>");


                htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Linea:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Naviera+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Agente maritimo:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>ABC</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">ETD:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.ETD+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">ETA:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.ETA+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("</td>");

                htmlShow.Append("<td style=\"border-top:none;\" class=\"border-right\">");
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Fecha de recepcion:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.ETD+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Puerto embarque:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Puerto_Embarque+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Puerto descarga:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b>"+lis.Puerto_Descarga+"</b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-6\">Descarga contenedor:</div>");
                htmlShow.Append("<div class=\"col-6 text-right\"><b></b></div>");
                htmlShow.Append("</div>");

                htmlShow.Append("</td>");

                //htmlShow.Append("<td style=\"border-top:none;\">");
                //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>");
                //htmlShow.Append("<a href=\"#\">Descargar en PDF</a><br>");
                //htmlShow.Append("</td>");


                htmlShow.Append("<td style=\"border-top:none;\">");
                if (lis.Id_Estado_Reserva == 1)
                {
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='ConfirmarReserva({0});'> <i class=\"fa fa-check-circle fa-3x\" style=\"color: green\"></i>  </a><br></div>", lis.Id_Reserva.ToString());
                    htmlShow.Append("</div>");
                    htmlShow.Append("</br>");
                }
                if (lis.Id_Estado_Reserva == 1)
                {
                    htmlShow.Append("<div class=\"row\">");
                    htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='RechazarReserva({0});'> <i class=\"fa fa-times fa-3x\" style=\"color: red\" ></i>  </a><br></div>", lis.Id_Reserva.ToString());
                    htmlShow.Append("</div>");
                }
                if (lis.Id_Estado_Reserva == 3)
                {
                    htmlShow.Append("<div style=\"padding-top:20px \" class=\"row col-8 \">");
                    htmlShow.AppendFormat("<button onclick =\"EnvioOdoo({0}); return false\" class=\"col-12 text-left btn btn-primary float-right\">Enviar Booking a Odoo </button>", lis.Id_Reserva.ToString());

                    //htmlShow.AppendFormat("<div class=\"col-6 text-right\"><a href=\"#\" onclick='EnvioOdoo({0});'> <i class=\"fas fa-mail-bulk fa-3x\" style=\"color: blue\" ></i>  </a><br></div>", lis.Id_Reserva.ToString());
                    htmlShow.Append("</div>");
                }

           
             

                htmlShow.Append("</tr>");
            
                htmlShow.Append("</td>");
                htmlShow.Append("<tr>");
                
                //htmlShow.Append("<td style=\"border-top:none;\"  colspan=\"4\" class=\"text - right\">");
                //htmlShow.AppendFormat("<a href=\"#\" onclick='DeleteReserva({0});'>Cancelar booking</a>", lis.IdItinerarioSoli.ToString());
                //htmlShow.AppendFormat("<img src='/Imagenes/delete.png'  title='Eliminar Cotización'  onclick='DeleteReserva({0});'  style='cursor: hand' > &nbsp;&nbsp;"

                //htmlShow.Append("<button type=\"button\" class=\"btn btn-primary float-right\" onclick=\"window.location.href='../ResumenReserva/'; \">Ver Detalles</button>");

                htmlShow.Append("</tr>");



                htmlShow.Append("</tbody>");
                htmlShow.Append("</table>");
                htmlShow.Append("</div>");
                if (lis.Id_Estado_Reserva == 1)
                {
                    htmlShow.Append("<div class=\"card-footer\">");
                    htmlShow.AppendFormat("<button onclick =\"EditReserva({0}); return false\" class=\"btn btn-primary float-right\">Editar Reserva</button>", lis.Id_Reserva.ToString());
                    htmlShow.Append("</div>");
                }
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");

                #endregion
            }

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }

    }
}