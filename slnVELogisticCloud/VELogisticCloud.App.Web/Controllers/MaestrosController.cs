﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.Servicio.Middleware;
using VELogisticCloud.CrossCutting.OdooEntity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

namespace VELogisticCloud.App.Web.Controllers
{
    public class MaestrosController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public MaestrosController(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        public IActionResult AutoCompleteNavieras(string term)
        {
            OdooService odooService = new OdooService(_configuration, _memoryCache);
            var lista = odooService.ListarNavieras(name: term);

            var result = (from item in lista
                          where item.Nombre.Contains(term, System.StringComparison.CurrentCultureIgnoreCase)
                          select new { value = item.Nombre, label = item.Nombre, Id = item.Codigo });

            return Json(result);
        }

        public IActionResult AutoCompleteCommodity(string term)
        {
            OdooService odooService = new OdooService(_configuration, _memoryCache);
            var lista = odooService.ListarCommodity(name: term.Trim());

            var result = (from item in lista
                          where item.Nombre.Contains(term, System.StringComparison.CurrentCultureIgnoreCase)
                          select new { value = item.Nombre, label = item.Nombre, Id = item.Id });

            return Json(result);
        }

        public IActionResult AutoCompletePuertos(string term)
        {
            OdooService odooService = new OdooService(_configuration, _memoryCache);
            var lista = odooService.ListarPuerto(name: term);

            var result = (from item in lista
                          where item.Nombre.Contains(term, System.StringComparison.CurrentCultureIgnoreCase)
                          select new { value = item.Nombre, label = item.Nombre, Id = item.Codigo });

            return Json(result);
        }

        public IActionResult AutoCompletePuertosPais(string term)
        {
            OdooService odooService = new OdooService(_configuration, _memoryCache);
            var lista = odooService.ListarPuerto(name: term);

            var result = (from item in lista
                          where item.Nombre.Contains(term, System.StringComparison.CurrentCultureIgnoreCase)
                          //select new { value = item.Nombre, label = item.Nombre, Id = string.Format("{0}-{1}-{2}",item.Codigo, item.CodigoPais, item.NombrePais)});
                          select new { 
                              id = item.Id,
                              value = item.Nombre, 
                              label = item.Nombre,
                              codigo = item.Codigo,
                              codigoPais = item.CodigoPais,
                              nombrePais = item.NombrePais,
                              nombreRegion = item.NombreRegion
                          });

            return Json(result);
        }
        public IActionResult AutoCompleteContactOwner(string term)
        {
            OdooService odooService = new OdooService(_configuration, _memoryCache);
            var lista = odooService.ListarContractOwner(name: term);

            var result = (from item in lista
                          where item.Nombre.Contains(term, System.StringComparison.CurrentCultureIgnoreCase)
                          //select new { value = item.Nombre, label = item.Nombre, Id = string.Format("{0}-{1}-{2}",item.Codigo, item.CodigoPais, item.NombrePais)});
                          select new
                          {
                              id = item.Id,
                              value = item.Nombre,
                              label = item.Nombre,
                              correo = item.Correo
                          });
            return Json(result);
        }
    }
}
