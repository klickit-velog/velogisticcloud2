﻿using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using VELogisticCloud.App.Web.Clases;
using VELogisticCloud.Models.General;
using VELogisticCloud.Servicio.Business.Reservas;

namespace VELogisticCloud.App.Web.Controllers
{
    [DisableRequestSizeLimit]
    public class ItinerarioManualController : Controller
    {
        private IConfiguration _configuration;

        public ItinerarioManualController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoadFile2()
        {
            Resultado result = new Resultado();
            Resultado resultBulk = new Resultado();
            BL_Reserva bl = new BL_Reserva(_configuration);

            int row = -1;
            List<BE_ItinerarioManual> lista = new List<BE_ItinerarioManual>();

            var files = Request.Form.Files;
            int rowError = 0;

            //var tipo = Request.;
            try
            {
                using (var stream = new MemoryStream())
                {
                    await files[0].CopyToAsync(stream);
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        while (reader.Read())
                        {
                            if (reader.GetValue(0) != null)
                            {
                                row++;
                                if (row > 0)
                                {
                                    rowError = row;
                                    BE_ItinerarioManual be = new BE_ItinerarioManual();
                                    be.scac = reader.GetValue(0).ToString();
                                    be.carrierName = reader.GetValue(1).ToString();
                                    be.terminalCutoff = reader.GetValue(2).ToString();
                                    be.vesselName = reader.GetValue(10).ToString();
                                    be.voyageNumber = reader.GetValue(11).ToString();
                                    be.imoNumber = reader.GetValue(12).ToString();
                                    be.originUnloc = reader.GetValue(13).ToString();
                                    be.originCityName = reader.GetValue(14).ToString();
                                    be.originSubdivision = reader.GetValue(15).ToString();
                                    be.originCountry = reader.GetValue(16).ToString();
                                    be.originDepartureDate = reader.GetValue(18).ToString();
                                    be.destinationUnloc = reader.GetValue(19).ToString();
                                    be.destinationCityName = reader.GetValue(20).ToString();
                                    be.destinationCountry = reader.GetValue(22).ToString();
                                    be.destinationArrivalDate = reader.GetValue(24) == null ? "":reader.GetValue(24).ToString();
                                    be.totalDuration = reader.GetValue(25) == null ? "":reader.GetValue(25).ToString();
                                    be.legs = reader.GetValue(27) == null ? "":reader.GetValue(27).ToString();
                                    be.sequence = reader.GetValue(28) == null ? "":reader.GetValue(28).ToString();
                                    be.serviceName = reader.GetValue(29) == null ? "":reader.GetValue(29).ToString();
                                    be.transportName = reader.GetValue(31) == null ? "":reader.GetValue(31).ToString();
                                    be.conveyanceNumber = reader.GetValue(32) == null ? "":reader.GetValue(32).ToString();
                                    be.departureCityName = reader.GetValue(35) == null ? "":reader.GetValue(35).ToString();
                                    be.departureCountry = reader.GetValue(37) == null ? "":reader.GetValue(37).ToString();
                                    be.departureDate = reader.GetValue(39) == null ? "":reader.GetValue(39).ToString();
                                    be.arrivalCityName = reader.GetValue(41) == null ? "":reader.GetValue(41).ToString();
                                    be.arrivalCountry = reader.GetValue(43) == null ? "":reader.GetValue(43).ToString();
                                    be.arrivalDate = reader.GetValue(45) == null ? "":reader.GetValue(45).ToString();
                                    be.transitDuration = reader.GetValue(46) == null ? "":reader.GetValue(46).ToString();
                                    lista.Add(be);
                                }

                            }

                        }
                    }
                }
                if(lista.Count > 0)
                {
                    bl.BulkInsert(lista);
                    result.result = lista.Count;
                }
                //resultBulk = await BulkInsertItinerarioManual2(files[0]);
                //result.result = resultBulk.count;
            }
            catch (Exception ex)
            {
                var error = Convert.ToString(rowError);
                var mensaje = ex.Message;
                result.result = 0;

            }
            return Ok(result);
        }

        //[HttpPost]
        //public IActionResult Index(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        //{
        //    string fileName = $"{hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
        //    using (FileStream fileStream = System.IO.File.Create(fileName))
        //    {
        //        file.CopyTo(fileStream);
        //        fileStream.Flush();
        //    }
        //    var lista = this.GetItinerarioManual(file.FileName);
        //    return View();
        //}

        private List<BE_ItinerarioManual> GetItinerarioManual(string fName)
        {
            List<BE_ItinerarioManual> lista = new List<BE_ItinerarioManual>();
            var fileName = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + fName;
            //System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            int row = -1;
            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    while (reader.Read())
                    {
                        if (reader.GetValue(0) != null)
                        {
                            row++;
                            if(row > 0)
                            {
                                //var Fecha = reader.GetValue(2).ToString();
                                //DateTime DateObject = Convert.ToDateTime(Fecha);
                                //string fechaSeteada = DateObject.Month.ToString() + '/' + DateObject.Day.ToString() + '/' + DateObject.Year.ToString();
                                lista.Add(new BE_ItinerarioManual()
                                {
                                    scac = reader.GetValue(0).ToString(),
                                    carrierName = reader.GetValue(1).ToString(),
                                    //terminalCutoff = fechaSeteada,
                                    terminalCutoff = reader.GetValue(2).ToString(),
                                    vesselName = reader.GetValue(10).ToString(),
                                    voyageNumber = reader.GetValue(11).ToString(),
                                    imoNumber = reader.GetValue(12).ToString(),
                                    originUnloc = reader.GetValue(13).ToString(),
                                    originCityName = reader.GetValue(14).ToString(),
                                    originSubdivision = reader.GetValue(15).ToString(),
                                    originCountry = reader.GetValue(16).ToString(),
                                    originDepartureDate = reader.GetValue(18).ToString(),
                                    destinationUnloc = reader.GetValue(19).ToString(),
                                    destinationCityName = reader.GetValue(20).ToString(),
                                    destinationCountry = reader.GetValue(22).ToString(),
                                    destinationArrivalDate = reader.GetValue(24).ToString(),
                                });
                            }
                            
                        }
                        
                    }
                }
            }

            //lista.RemoveAt(0);
            
            return lista;
        }

        public async Task<Resultado> BulkInsertItinerarioManual2(IFormFile file)
        {
            Resultado result = new Resultado();
            BL_Reserva bl = new BL_Reserva(_configuration);
            var list = new List<BE_ItinerarioManual>();
            string mensaje = "";
            try
            {
                using (var stream = new MemoryStream())
                {
                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage((stream)))
                    {
                        //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                        int cant = package.Workbook.Worksheets.Count;
                        var compatible = package.Compatibility.IsWorksheets1Based;
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                        var rowCount = worksheet.Dimension.Rows;
                        bool error = false;

                        var rowColumns = worksheet.Dimension.Columns;
                        
                        for (int row = 2; row <= rowCount; row++)
                        {
                            BE_ItinerarioManual ent = new BE_ItinerarioManual();
                            //ent.Id = 0;
                            ent.scac = worksheet.Cells[row, 1].Value == null ? "" : worksheet.Cells[row, 1].Value.ToString().Trim();
                            ent.carrierName = worksheet.Cells[row, 8].Value == null ? "" : worksheet.Cells[row, 2].Value.ToString().Trim();
                            ent.terminalCutoff = worksheet.Cells[row, 9].Value == null ? "" : worksheet.Cells[row, 3].Value.ToString().Trim();
                            ent.vesselName = worksheet.Cells[row, 36].Value == null ? "" : worksheet.Cells[row, 11].Value.ToString().Trim();
                            ent.voyageNumber = worksheet.Cells[row, 74].Value == null ? "" : worksheet.Cells[row, 12].Value.ToString().Trim();
                            ent.imoNumber = worksheet.Cells[row, 66].Value == null ? "" : worksheet.Cells[row, 13].Value.ToString().Trim();
                            ent.originUnloc = worksheet.Cells[row, 75].Value == null ? "" : worksheet.Cells[row, 14].Value.ToString().Trim();
                            ent.originCityName = worksheet.Cells[row, 76].Value == null ? "" : worksheet.Cells[row, 15].Value.ToString().Trim();
                            ent.originSubdivision = worksheet.Cells[row, 60].Value == null ? "" : worksheet.Cells[row, 16].Value.ToString().Trim();
                            ent.originCountry = worksheet.Cells[row, 58].Value == null ? "" : worksheet.Cells[row, 17].Value.ToString().Trim();
                            ent.originDepartureDate = worksheet.Cells[row, 92].Value == null ? "" : worksheet.Cells[row, 19].Value.ToString().Trim();
                            ent.destinationUnloc = worksheet.Cells[row, 92].Value == null ? "" : worksheet.Cells[row, 20].Value.ToString().Trim();
                            ent.destinationCityName = worksheet.Cells[row, 92].Value == null ? "" : worksheet.Cells[row, 21].Value.ToString().Trim();
                            ent.destinationCountry = worksheet.Cells[row, 92].Value == null ? "" : worksheet.Cells[row, 23].Value.ToString().Trim();
                            ent.destinationArrivalDate = worksheet.Cells[row, 92].Value == null ? "" : worksheet.Cells[row, 24].Value.ToString().Trim();
                            ent.destinationArrivalDate = worksheet.Cells[row, 92].Value == null ? "" : worksheet.Cells[row, 24].Value.ToString().Trim();
                        }
                    }
                }

                if (list.Count > 0)
                {
                    result.count = list.Count;
                    result.message = mensaje;
                    bl.BulkInsert(list);
                    //for (int row = 0; row <= list.Count; row = row + 500)
                    //{
                    //    var items = list.Skip(row).Take(500).ToList();
                    //    result.result = BL.BL_BulkInsertAtributosPlaneta(items, row);

                    //}
                }
            }
            catch (Exception ex)
            {
               mensaje = ex.Message;
            }
            return result;
        }
    }
}
