﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.App.Web.Util;
using VELogisticCloud.Model.Identity;
using VELogisticCloud.Servicio.Business.Cotizacion;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Middleware;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.App.Web.Controllers
{
    public class ConsigneeController : Controller
    {
        //private readonly VECloudDBContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public ConsigneeController(ILogger<ConsigneeController> logger, UserManager<ApplicationUser> userManager,
            IConfiguration configuration, IMemoryCache memoryCache)
        {
            //_context = context;
            _logger = logger;
            _userManager = userManager;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }
        public IActionResult Index()
        {
            ModelCombos model = new ModelCombos();
            var listaNavieras = new List<SelectListItem>();
            new OdooService(_configuration, _memoryCache).ListarNavieras().ForEach(a => listaNavieras.Add(new SelectListItem()
            {
                Value = a.Codigo,
                Text = a.Nombre

            }));
            model.ListaNavieras = listaNavieras;

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ListaCotizacion(Parametros_Lista_Maritimo param, string tipousuario,string correo)
        {
            string htmlGrid = "";

            //var user = await _userManager.GetUserAsync(HttpContext.User);
            //var datos = await _userManager.GetClaimsAsync(user);
            ////TODO: validar usuario sea customer.
            //if (datos.Count > 0)
            //{
            //    var idCliente = datos[0].Type;
            //    param.Codigo_Usuario = Convert.ToString(idCliente);
            //}
            //else
            //{
            //    if (tipousuario != "customer")
            //    {
            //        param.Codigo_Usuario = HttpContext.User.Identity.Name;
            //    }
            //    else
            //    {
            //        param.Codigo_Usuario = "";
            //    }
            //}

            if(correo == null)
            {
                correo = "";
                param.Codigo_Usuario = "";
            }
            else
            {
                param.Codigo_Usuario = correo;
            }

            if (param.cod_coti == null)
            {
                param.cod_coti = "";
            }
            if (param.Campana == null)
            {
                param.Campana = "";
            }

            param.cod_puerto_destino = param.cod_puerto_destino == null ? "0" : param.cod_puerto_destino;
            param.cod_puerto_salida = param.cod_puerto_salida == null ? "0" : param.cod_puerto_salida;
            try
            {
                BL_Cotizacion BL = new BL_Cotizacion(_configuration);
                List<Lista_Maritimo> ListaTemporal = new List<Lista_Maritimo>();
                List<Lista_Maritimo> ListaPrincipal = new List<Lista_Maritimo>();
                param.ListadoUsuarios = new List<string>();
                //if (param.Codigo_Usuario == "")
                //{
                //    var listarClientes = new OdooService(_configuration).ListarClientes(correoCustomerService: HttpContext.User.Identity.Name);
                //    foreach (var cliente in listarClientes)
                //    {
                //        if (cliente.Correo != null && cliente.Correo != "")
                //        {
                //            param.Codigo_Usuario = cliente.Correo;
                //            param.TipoCotizacion = 1;
                //            var res1 = BL.BL_Lista_CotizacionesAprobadas(param);
                //            ListaTemporal = (List<Lista_Maritimo>)res1;
                //            ListaPrincipal.AddRange(ListaTemporal);
                //        }
                //    }
                //}
                //else
                //{
                    param.TipoCotizacion = 1;
                    param.ListadoUsuarios.Add(param.Codigo_Usuario);
                    var res2 = BL.BL_Lista_CotizacionesAprobadas(param);
                    ListaPrincipal = (List<Lista_Maritimo>)res2;
                //}
                htmlGrid = crearGridShow(ListaPrincipal);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error Cotizacion/ListaCotizacion");
            }
            return Ok(htmlGrid);
        }

        public string crearGridShow(List<Lista_Maritimo> lista)
        {
            StringBuilder htmlShow = new StringBuilder();
            //htmlShow.Append("<table border=0 width='100%;' style='border-collapse: collapse;' class='k-grid k-widget'  >");

            //List<Lista_Maritimo> lista = (List<Lista_Maritimo>)list;

            List<string> campana = new List<string>();
            if(lista.Count > 0)
            {
                foreach (var lis in lista)
                {
                    #region html
                    if (campana.Contains(lis.Campania))
                    {

                    }
                    else
                    {
                        campana.Add(lis.Campania);

                        //campana.Where(x => x.)
                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-md-12\">");
                        htmlShow.Append("<div class=\"card\">");
                        htmlShow.Append("<div class=\"card-body\">");

                        htmlShow.Append("<div class=\"row\">");
                        htmlShow.Append("<div class=\"col-2\" >");
                        htmlShow.Append("<h4> " + lis.Campania + "</h4> Campaña");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");

                        htmlShow.Append("<table class=\"table table-hover\" cellspacing=\"1\" style=\"border: 1px solid #ccc;\">");
                        htmlShow.Append("<tbody style=\"border: 1px solid #ccc;\">");
                        htmlShow.Append("<tr>");
                        htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\">FECHA APROB</th>");
                        htmlShow.Append("<th style=\"color:#306BF3\" style=\"border: 1px solid #ccc;\"># COTIZACION</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">REGION</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">LINE</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POL</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">POD</th>");
                        //if (lis.REBATE > 0)
                        //{
                        //    htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">NETO</th>");
                        //    htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">REBATE</th>");
                        //}

                        //htmlShow.Append("<th style=\"color:#2FE358\" style=\"border: 1px solid #ccc;\">FLETE B/L</th>");
                        htmlShow.Append("<th style=\"color:#5DE3EE\" style=\"border: 1px solid #ccc;\">TT</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">VALID FROM</th>");
                        htmlShow.Append("<th style=\"color:#2651B2\" style=\"border: 1px solid #ccc;\">VALID TO</th>");
                        htmlShow.Append("<th></th>");
                        htmlShow.Append("</tr>");

                        foreach (var lis1 in lista.Where(x => x.Campania == lis.Campania))
                        {
                            //if (lis1.Codigo_coti == lis.Codigo_coti)
                            //{
                            htmlShow.Append("<tr style=\"border-bottom: 1px solid #ccc; border-left: 1px solid #ccc; border-right: 1px solid #ccc;\">");

                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.FECHA_APROBACION + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Codigo_coti + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Region + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Linea_naviera + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.POL + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.POD + "</td>");
                            if (lis1.REBATE > 0)
                            {
                                //htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.NETO + "</td>");
                                //htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.REBATE + "</td>");
                            }
                            else
                            {
                                decimal suma = 0;
                                foreach (var sum in lista.Where(x => x.Campania == lis.Campania).ToList())
                                {
                                    suma = sum.REBATE + suma;
                                }
                                if (suma > 0)
                                {
                                    htmlShow.Append("<td style=\"border: 1px solid #ccc;\"></td>");
                                    htmlShow.Append("<td style=\"border: 1px solid #ccc;\"></td>");
                                }

                            }
                            //htmlShow.Append("<td style=\"border: 1px solid #ccc;\"> <b>" + lis1.FLETE + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.TT + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Desde + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\">" + lis1.Valido_Hasta + "</td>");
                            htmlShow.Append("<td style=\"border: 1px solid #ccc;\"> <button onclick=\" RealizarBooking(" + lis1.id_detalle + "); return false; \" class=\"btn btn-primary float-right\">Solicitar Booking</button></td>");
                            htmlShow.Append("</tr>");
                        }
                        htmlShow.Append("</tbody>");
                        htmlShow.Append("</table>");
                        htmlShow.Append("</div>");

                        htmlShow.Append(" <div class=\"card-footer\">");

                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");
                        htmlShow.Append("</div>");

                    }
                }
            }else
            {
                htmlShow.Append("<div class=\"row\">");
                htmlShow.Append("<div class=\"col-md-12\">");
                htmlShow.Append("<div class=\"card\">");
                htmlShow.Append("<div class=\"card-body\">");
                htmlShow.Append("No se encontraron datos. ");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
                htmlShow.Append("</div>");
            }
          


            #endregion

            //if (listObras != null && listObras.Count > 0)
            //{


            return htmlShow.ToString();

        }
    }
}
