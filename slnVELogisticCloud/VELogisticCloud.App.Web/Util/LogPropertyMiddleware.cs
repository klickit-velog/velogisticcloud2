﻿using Microsoft.AspNetCore.Http;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VELogisticCloud.App.Web.Util
{
    public class LogPropertyMiddleware
    {
        private readonly RequestDelegate next;

        public LogPropertyMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            using (LogContext.PushProperty("UserName", context.User.Identity.Name ?? "anonymous"))
            {
                await next(context);
            }
        }
    }
}
