﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.OdooEntity;

namespace VELogisticCloud.App.Web.Util
{
    public class ModelCombos
    {
        public List<SelectListItem> ListaNavieras { get; set; }
        public List<SelectListItem> ListaExpeditor { get; set; }
        public List<SelectListItem> ListaTipoContenedor { get; set; }
        public List<SelectListItem> ListaCommodity { get; set; }
        public List<SelectListItem> ListaCampania { get; set; }
        public List<SelectListItem> ListaAlmacenDeVacio { get; set; }
        public List<SelectListItem> ListaPacking { get; set; }
        public List<SelectListItem> ListaClientes { get; set; }
        public List<SelectListItem> ListaFiltroEtileno { get; set; }
        public List<SelectListItem> ListaTermoRegistro { get; set; }
        public List<SelectListItem> ListaTransporte { get; set; }
        public List<SelectListItem> ListaCondicionPago { get; set; }
        public List<SelectListItem> ListaContractOwner { get; set; }
        public Cliente Cliente { get; set; }
        public Contacto Contacto { get; set; }
        public string TMP { get; set; }

    }
}
