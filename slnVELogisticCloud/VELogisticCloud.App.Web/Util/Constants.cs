﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VELogisticCloud.App.Web.Util
{
    public class Constants
    {
        public const string msjValidacion_Campo_IdCotizaccionOdoo = "No se encontro la relacion con Odoo.";
        public const string msjValidacion_Campo_NroBooking = "No se encontro el  Id Cotizacion Odoo.";
        public const string msjValidacion_Campo_Transporte= "No se encontro el  Transporte.";
        public const string msjValidacion_Campo_CitaPacking = "No se encontro la Cita Packing.";
        public const string msjValidacion_Campo_IdCondicionPago = "No se encontro la Condicion de pago.";
        public const string msjValidacion_Campo_CanalSini = "No se encontro el Canal sini.";
    }
}
