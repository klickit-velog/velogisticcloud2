﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.Models.OperacionLogistica;

namespace VELogisticCloud.App.Web.Util
{
    public class Validadores
    {
        public List<string> ValidarUpdateDatosObligatorios(BE_OperacionLogistica2 source)
        {
            List<string> resultado = new List<string>();

            if (source.IdCotizacionOddo == 0)
                resultado.Add(Constants.msjValidacion_Campo_IdCotizaccionOdoo);

            if (string.IsNullOrWhiteSpace(source.NroBooking))
                resultado.Add(Constants.msjValidacion_Campo_NroBooking);


            //TODO: CAMBIAR POR LA FECHA DE VENCIMIENTO
            if (string.IsNullOrWhiteSpace(source.Transporte))
                resultado.Add(Constants.msjValidacion_Campo_Transporte);

            if (source.IdCondicionPago == 0 )
                resultado.Add(Constants.msjValidacion_Campo_IdCondicionPago);

            if (!EsFecha(source.CitaPacking.ToShortDateString()))
                resultado.Add(Constants.msjValidacion_Campo_CitaPacking);

            if (string.IsNullOrWhiteSpace(source.CanalSini))
                resultado.Add(Constants.msjValidacion_Campo_CanalSini);

            return resultado;
        }

        public static Boolean EsFecha(String fecha)
        {
            try
            {
                DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
