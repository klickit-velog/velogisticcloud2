﻿using Microsoft.AspNetCore.Identity;
using System;

namespace VELogisticCloud.App.Web.Models
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public int IdEstado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdUsuarioActualiza { get; set; }
        public DateTime FechaActualizacion { get; set; }
        
    }
}
