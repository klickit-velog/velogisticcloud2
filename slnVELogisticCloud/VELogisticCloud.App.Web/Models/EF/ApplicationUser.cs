﻿using Microsoft.AspNetCore.Identity;

namespace VELogisticCloud.App.Web.Models.EF
{
    public class ApplicationUser : IdentityUser
    {

    }
}
