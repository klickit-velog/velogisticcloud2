﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VELogisticCloud.App.Web.Models.Seguridad
{
    public class PerfilViewModel
    {
        [Key]
        public int IdPerfil { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Descripcion { get; set; }

        public int IdEstado { get; set; }

        public int IdUsuarioRegistro { get; set; }

        public DateTime FechaRegistro { get; set; }

        public int IdUsuarioActualiza { get; set; }

        public string FechaActualizacion { get; set; }
    }
}
