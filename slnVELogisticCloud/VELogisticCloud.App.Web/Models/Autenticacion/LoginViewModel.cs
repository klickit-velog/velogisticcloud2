﻿using System.ComponentModel.DataAnnotations;

namespace VELogisticCloud.App.Web.Models.Autenticacion
{
    public class LoginViewModel
    {

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [EmailAddress]
        public string Login { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Recordar?")]
        public bool RememberMe { get; set; }

    }
}
