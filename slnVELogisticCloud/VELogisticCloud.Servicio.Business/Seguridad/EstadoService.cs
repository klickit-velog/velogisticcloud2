﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Entidad.Seguridad;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Data.Seguridad;

namespace VELogisticCloud.Servicio.Business.Seguridad
{
    public class EstadoService : Service, IEstadoService
    {
        public EstadoService(VELogisticsCloudDBContext dbContext) : base(dbContext)
        {

        }

        public async Task<List<Estado>> Listar(int idEstadoPadre)
        {
            List<Estado> response;

            response = await DbContext.EstadosListar(idEstadoPadre);

            return response;
        }
    }
}
