﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.Servicio.Data;
using VELogisticCloud.Servicio.Data.Seguridad;

namespace VELogisticCloud.Servicio.Business.Seguridad
{
    public class UsuarioService : Service, IUsuarioService
    {
        public UsuarioService(VECloudDBContext dbContext) : base(dbContext)
        {

        }

        public async Task<EntidadConsultada<UsuarioDTO>> Consultar(UsuarioDTO usuario, int skip, int pageSize)
        {
            EntidadConsultada<UsuarioDTO> response = await DbContext.UsuariosConsultar(usuario, skip, pageSize);

            return response;
        }

        public Task<UsuarioDTO> ObtenerAsync(int idUsuario)
        {
            throw new NotImplementedException();
        }

        public Task<UsuarioDTO> ObtenerAsync(string login)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Registrar(UsuarioDTO usuario)
        {
            bool TxRealizada = false;

            using (var txAsync = await DbContext.Database.BeginTransactionAsync())
            {
                try
                {
                    Usuario usuarioRegistrado = await DbContext.Usuarios.Where(p => p.Login == usuario.Login).FirstOrDefaultAsync();

                    usuarioRegistrado.Apellidos = usuario.Apellidos;
                    usuarioRegistrado.Nombres = usuario.Nombres;
                    usuarioRegistrado.TelefonoMovil = usuario.TelefonoMovil;
                    usuarioRegistrado.IdEstado = usuario.IdEstado;

                    DbContext.Entry(usuarioRegistrado).Property("Apellidos").IsModified = true;
                    DbContext.Entry(usuarioRegistrado).Property("Nombres").IsModified = true;
                    DbContext.Entry(usuarioRegistrado).Property("TelefonoMovil").IsModified = true;
                    DbContext.Entry(usuarioRegistrado).Property("IdEstado").IsModified = true;

                    await DbContext.SaveChangesAsync();
                    txAsync.Commit();
                }
                catch (Exception)
                {
                    TxRealizada = false; ;
                    txAsync.Rollback();
                }
            }

            return TxRealizada;
        }
    }
}
