﻿using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Entidad.Seguridad;
using VELogisticCloud.Servicio.Business.Responses;

namespace VELogisticCloud.Servicio.Business.Seguridad
{
    public interface IPerfilService : IService
    {
        Task<IListResponse<Perfil>> Consultar(Perfil usuario);
    }
}
