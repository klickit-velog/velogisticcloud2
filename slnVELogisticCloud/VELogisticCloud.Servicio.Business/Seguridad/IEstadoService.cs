﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.Entidad.Seguridad;

namespace VELogisticCloud.Servicio.Business.Seguridad
{
    public interface IEstadoService : IService
    {
        Task<List<Estado>> Listar(int idEstadoPadre);
    }
}
