﻿using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.DTO;

namespace VELogisticCloud.Servicio.Business.Seguridad
{
    public interface IUsuarioService : IService
    {
        Task<UsuarioDTO> ObtenerAsync(int idUsuario);
        Task<UsuarioDTO> ObtenerAsync(string login);
        Task<EntidadConsultada<UsuarioDTO>> Consultar(UsuarioDTO usuario, int skip, int pageSize);
        Task<bool> Registrar(UsuarioDTO usuario);
    }
}
