﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.ConexionADO.Estadistica;

namespace VELogisticCloud.Servicio.Business.Estadistica
{
    public class BL_Estadistica
    {
        Da_Estadistica DA;

        public BL_Estadistica(IConfiguration configuration)
        {
            DA = new Da_Estadistica(configuration);
        }
         
        public object BL_DatosEstadisticaCotizacion(string IdUsuario)
        {
            return DA.DatosEstadisticaCotizacion(IdUsuario).Result;
        }
        
        public object BL_DatosEstadisticaReserva(string IdUsuario)
        {
            return DA.DatosEstadisticaReserva(IdUsuario).Result;
        }

        public object BL_DatosEstadisticaReservaInttra(string IdUsuario)
        {
            return DA.DatosEstadisticaReservaInttra(IdUsuario).Result;
        }

        public object BL_EstadisticaCotizacionLogistica(string IdUsuario)
        {
            return DA.DatosEstadisticaCotizacionLogistica(IdUsuario).Result;
        }

        public object BL_EstadisticaOperacionLogistica(string IdUsuario)
        {
            return DA.DatosEstadisticaOperacionLogistica(IdUsuario).Result;
        }

        public object BL_EstadisticaOperacionLogisticaOddo(string IdUsuario)
        {
            return DA.DatosEstadisticaOperacionLogisticaOddo(IdUsuario).Result;
        }

        public object BL_ResumenBooking(string IdUsuario)
        {
            return DA.ResumenBooking(IdUsuario).Result;
        }

    }
}
