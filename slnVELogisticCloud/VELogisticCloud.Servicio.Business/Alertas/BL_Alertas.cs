﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Models.Alertas;
using VELogisticCloud.Servicio.Data.ConexionADO;
using VELogisticCloud.Servicio.Data.ConexionADO.Alertas;

namespace VELogisticCloud.Servicio.Business.Alertas
{
    public class BL_Alertas 
    {
        DA_Alertas data;
        public BL_Alertas(IConfiguration configuration)
        {
            data = new DA_Alertas(configuration);
        }

        public object BL_Lista_Alertas()
        {
            return data.Lista_Alertas().Result;
        }
        public object MarcarLeido(int id)
        {
            return data.MarcarLeido(id).Result;
        }

        public object InsertarDataAlertas(BE_Alertas_Contrato ent)
        {
            return data.InsertarDataAlertas(ent).Result;

        }
    }
}
