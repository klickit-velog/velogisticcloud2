﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Models.Notificacion;
using VELogisticCloud.Servicio.Data.ConexionADO.Notificacion;

namespace VELogisticCloud.Servicio.Business.Notificacion
{
    public class BL_Notificacion
    {
        DA_Notificacion DA;
        public BL_Notificacion(IConfiguration configuration)
        {
            DA = new DA_Notificacion(configuration);
        }
        public object InsertNotificacion(BE_NotificacionUsuario ent)
        {
           return DA.InsertNotificacion(ent).Result;
        }

        public object UpdateNotificacion(BE_NotificacionUsuario ent)
        {
            return DA.UpdateNotificacion(ent).Result;
        }

        public object ListaNotificacionUsuario()
        {
            return DA.ListaNotificacionUsuario().Result;
        }
    }
}
