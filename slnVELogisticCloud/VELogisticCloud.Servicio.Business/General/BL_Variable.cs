﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.ConexionADO;

namespace VELogisticCloud.Servicio.Business.General
{
    public class BL_Variable
    {
        Da_Variables DA;
        public BL_Variable(IConfiguration configuration)
        {
            DA = new Da_Variables(configuration);
        }
        public object BL_UpdateVariable(int Activado,string Usuario,string Codigo)
        {
            return DA.UpdateVariable(Activado, Usuario, Codigo).Result;
        }
        public object BL_ListaVariables(string Codigo)
        {
            return DA.ListaVariables(Codigo).Result;
        }
    }
}
