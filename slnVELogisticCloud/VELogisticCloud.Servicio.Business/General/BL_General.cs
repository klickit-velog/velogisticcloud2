﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.ConexionADO.General;

namespace VELogisticCloud.Servicio.Business.General
{
    public class BL_General
    {
       DA_General DA;
        public BL_General(IConfiguration configuration)
        {
            DA = new DA_General(configuration);
        }
        public object BL_ListaValores(string grupo)
        {
            return DA.ListaValores(grupo).Result;
        }
    }
}
