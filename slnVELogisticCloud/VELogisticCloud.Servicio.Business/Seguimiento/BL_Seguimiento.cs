﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.ConexionADO.Seguimiento;

namespace VELogisticCloud.Servicio.Business.Seguimiento
{
    public class BL_Seguimiento
    {
        DA_Seguimiento da;
        public BL_Seguimiento(IConfiguration configuration)
        {
            da = new DA_Seguimiento(configuration);
        }
        public object BL_SeguimientoReserva(int id)
        {
            try
            {
                return da.SeguimientoReserva(id).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public object BL_SeguimientoOPL(int id)
        {
            try
            {
                return da.SeguimientoOPL(id).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}
