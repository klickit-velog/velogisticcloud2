﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.Servicio.Data.ConexionADO.Proyecciones;
using static VELogisticCloud.Models.Proyecciones;

namespace VELogisticCloud.Servicio.Business.BL_Proyecciones
{
    public class BL_Proyecciones
    {
        Da_Proyecciones data;
        public BL_Proyecciones(IConfiguration configuration)
        {
            data =  new Da_Proyecciones(configuration);
        }
        public object InsertCabecera(Cabecera_Proyecciones ent,string UserCreacion)
        {
            return data.InsertCabecera(ent, UserCreacion).Result;
        }

        public object InsertDetalle(Detalle_Proyecciones ent, string UserCreacion)
        {
            return data.InsertDetalle(ent, UserCreacion).Result;
        }
        
        public object UpdateDetalle(Detalle_Proyecciones ent, string UserUpdate)
        {
            return data.UpdateDetalle(ent, UserUpdate).Result;
        }
        public object Lista_Proyecciones(string fecha, string Cod_commodity, string cod_destino)
        {
            return data.Lista_Proyecciones(fecha, Cod_commodity, cod_destino).Result;
        }

        public object ViewDatos(decimal id)
        {
            return data.ViewDatos(id).Result;
        }
    }
}
