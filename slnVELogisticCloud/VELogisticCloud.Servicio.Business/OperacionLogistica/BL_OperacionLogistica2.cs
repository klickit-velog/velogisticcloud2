﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using VELogisticCloud.Models.OperacionLogistica;
using VELogisticCloud.Servicio.Data.ConexionADO.OperacionLogistica;

namespace VELogisticCloud.Servicio.Business.OperacionLogistica
{
    public class BL_OperacionLogistica2
    {
        DA_OperacionLogistica2 DA;
        public BL_OperacionLogistica2(IConfiguration configuration)
        {
            DA = new DA_OperacionLogistica2(configuration);
        }
        public object BL_InsertarOperacionLogistica(BE_OperacionLogistica2 ent)
        {
            return DA.DA_InsertarOperacionLogistica(ent).Result;
        }

        public object BL_UpdateOperacionLogistica(BE_OperacionLogistica2 ent)
        {
            return DA.DA_UpdateOperacionLogistica(ent).Result;

        }

        public object BL_ListaOperacionLogistica(int id,string Codigo_Usuario, List<string> ListadoUsuarios)
        {
            return DA.DA_ListaOperacionLogistica(id, Codigo_Usuario, ListadoUsuarios).Result;
        }

        public object BL_DetalleCotizacion(int id)
        {
            return DA.DA_DetalleCotizacion(id).Result;
        }

        public object BL_DatosEnvioPedido(int id)
        {
            return DA.DA_DatosEnvioPedido(id).Result;
        }
        
        public object BL_ConfirmarOperacionLogistica(int id, string user)
        {
            return DA.DA_ConfirmarOperacionLogistica(id, user);
        }
        public object BL_CancelarOperacionLogistica(int id, string user)
        {
            return DA.DA_CancelarOperacionLogistica(id, user);
        }
        public object BL_SeguimientoOPL(int id)
        {
            return DA.SeguimientoOPL(id).Result;

        }

        public DataTable BL_ListarExcelAduanero(int idOperacionLogistica)
        {
            return DA.DA_ListarExcelAduanero(idOperacionLogistica);
        }
        public object BL_ApiUpdateOPL(int id, string codEstado, string descEstado, string fechaIni, string code, string status_id)
        {
            return DA.DA_ApiUpdateOPL(id, codEstado, descEstado, fechaIni, code, status_id).Result;
        }
        public DataTable DatosExcelOpl()
        {
            return DA.DatosExcelOpl();
        }
        public object BL_UpdateOplDatosObligatorios(string OrdenServicio, string Transporte, string CanalSini, int id)
        {
            return DA.DA_UpdateOplDatosObligatorios(OrdenServicio, Transporte, CanalSini, id);

        }

        public int BL_Confirmar_OPL(int idOperacionLogistica)
        {
            return DA.DA_Confirmar_OPL(idOperacionLogistica);
        }
        public object DatosParaCorreoOPL(string code)
        {
            return DA.DatosParaCorreoOPL(code).Result;
        }
    }
}
