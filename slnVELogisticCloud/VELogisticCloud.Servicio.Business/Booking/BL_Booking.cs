﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Servicio.Data.ConexionADO.Booking;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.Servicio.Business.Booking
{
    public class BL_Booking
    {
        DA_Booking DA;
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public BL_Booking(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _memoryCache = memoryCache;
            DA = new DA_Booking(configuration);
        }

        #region Booking
        public  object BL_InsertarBooking(InsertBooking param)
        {
            return  DA.InsertBooking(param).Result;
        }

        public object BL_InsertBookingItinerarioManual(int IdBooking, int IdItinerarioManual)
        {
            return DA.InsertBookingItinerarioManual(IdBooking, IdItinerarioManual).Result;
        }

        
        public object UpdateNumeroBL(int id, string NumeroBL)
        {
            return DA.UpdateNumeroBL(id, NumeroBL).Result;
        }
        
        public object BL_UpdateBooking(InsertBooking param)
        {
            return DA.UpdateBooking(param).Result;
        }
        public object BL_UpdateImo(int id, string imo)
        {
            return DA.UpdateImo(id, imo).Result;
        }


        public object BL_InsertarConsignatario(InsertConsignatario param)
        {
            return DA.InsertConsignatario(param).Result;
        }
        public object BL_InsertarNotify(InsertNotify param)
        {
            return DA.InsertNotify(param).Result;
        }

        public object BL_ListaConsignatario()
        {
            return DA.ListaConsignatario().Result;
        }
        public object BL_ListaNotify()
        {
            return DA.ListaNotify().Result;
        }
        public object BL_ListaBooking(int id,string Codigo_Usuario)
        {
            return DA.ListaBooking(id, Codigo_Usuario).Result;
        }

        public object ListaBookingXNroBooking(int id, string NroBooking)
        {
            return DA.ListaBookingXNroBooking(id, NroBooking).Result;
        }
        public InsertBooking ObtenerByNumeroBooking(string nroBooking)
        {
            return DA.ObtenerByNumeroBooking(nroBooking).Result;
        }
        public DataTable BL_DatosExcelBooking()
        {
            return DA.DatosExcelBooking();
        }

        public object BL_SeguimientoBooking(int id)
        {
            return DA.SeguimientoBooking(id).Result;
        }
        public object BL_CancelarBooking(int id, string user)
        {
            return DA.CancelarBooking(id, user).Result;
        }

        public object BL_ConfirmarBooking(int id, string user)
        {
            return DA.ConfirmarBooking(id, user).Result;
        }
        public object ConfirmarBookingManual(int id, string booking, string imo, string user)
        {
            return DA.ConfirmarBookingManual(id, booking, imo, user).Result;
        }

        public object BL_EstadoEnvioOddobooking(int id, string user)
        {
            return DA.EstadoEnvioOddobooking(id, user).Result;
        }

        public object BL_DatosCotizacionMaritima(int id)
        {
            return DA.DatosCotizacionMaritima(id).Result;
        }

        public object BL_NuevaListaBooking(int id, string Codigo_Usuario)
        {
            return DA.NuevaListaBooking(id, Codigo_Usuario).Result;
        }

        public IEnumerable<NuevaListaBooking> ListarBookingDelCustomer(List<string> listaClienteCorreo, int IdBooking, string NroBooking, int IdContactOwner, string NombreLinea,
            string NombreNave, string NroViaje, int IdEmbarque, int IdDescarga, int IdCommodity, string FechaDesde, string FechaHasta,int EstadoId)
        {
            return DA.ListarBookingDelCustomer(listaClienteCorreo,
                    IdBooking,
                    NroBooking,
                    IdContactOwner,
                    NombreLinea,
                    NombreNave,
                    NroViaje,
                    IdEmbarque,
                    IdDescarga,
                    IdCommodity,
                    FechaDesde,
                    FechaHasta,
                    EstadoId
                );
        }

        #endregion

        #region Envio Inttra 
        public string envioXML(int id,string user)
        {

            //string url = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
            string url = Path.Combine(Environment.CurrentDirectory + "\\xml-inttra");
            string nombre = Guid.NewGuid().ToString() + ".xml";
            string RequestDateTimeStamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss");
            string Booking = "Booking";
            string tipoTemperatuea = "";

            if (!Directory.Exists(url))                         // Revisar si existe el directorio
            {
                DirectoryInfo di = Directory.CreateDirectory(url);
            }
            var res = DA.ListaBooking(id,"");
            List<InsertBooking> lentReserva = (List<InsertBooking>)res.Result;
            InsertBooking entReserva = lentReserva[0];
           
                XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                xmlWriterSettings.Encoding = new UTF8Encoding(false);
                XmlWriter xmlWriter = XmlWriter.Create(url + "\\" + nombre, xmlWriterSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding=\"utf-8\"");
                xmlWriter.WriteStartElement("n1", "Message", @"http://xml.inttra.com/booking/services/01");

                #region Entidades
                List<Cabecera> ListaCabecera = new List<Cabecera>();
                Cabecera ent0 = new Cabecera();
                ent0.title = "SenderId";
                ent0.detalle = "VLOG";
                ListaCabecera.Add(ent0);

                Cabecera ent1 = new Cabecera();
                ent1.title = "ReceiverId";
                ent1.detalle = "INTTRA";
                ListaCabecera.Add(ent1);

                Cabecera ent2 = new Cabecera();
                ent2.title = "RequestDateTimeStamp";
                ent2.detalle = RequestDateTimeStamp;
                ListaCabecera.Add(ent2);

                Cabecera ent3 = new Cabecera();
                ent3.title = "RequestMessageVersion";
                ent3.detalle = "1.0";
                ListaCabecera.Add(ent3);

                Cabecera ent4 = new Cabecera();
                ent4.title = "TransactionType";
                ent4.detalle = Booking;
                ListaCabecera.Add(ent4);

                Cabecera ent5 = new Cabecera();
                ent5.title = "TransactionVersion";
                ent5.detalle = "2.0";
                ListaCabecera.Add(ent5);

                Cabecera ent6 = new Cabecera();
                ent6.title = "DocumentIdentifier";
                ent6.detalle = "0000000" + entReserva.IdBooking;
                ListaCabecera.Add(ent6);

                Cabecera ent7 = new Cabecera();
                ent7.title = "TransactionStatus";
                ent7.detalle = "Original";
                ListaCabecera.Add(ent7);

                #endregion

                #region Body XML
                xmlWriter.WriteStartElement("Header");
                foreach (var lista in ListaCabecera)
                {
                    xmlWriter.WriteStartElement(lista.title);
                    xmlWriter.WriteString(lista.detalle);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("MessageBody");             /*OPEN_MessageBody*/

                xmlWriter.WriteStartElement("MessageProperties");       /*OPEN_MessageProperties*/

                xmlWriter.WriteStartElement("ShipmentID");
                xmlWriter.WriteString("000000" + entReserva.IdBooking);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ContactInformation");      /*OPEN_ContactInformation*/

                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("InformationContact");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString("Usuario logueo");                // TODO: Agregar aqui el usuario logueado
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("CommunicationDetails");    /*OPEN_CommunicationDetails*/

                xmlWriter.WriteStartElement("Phone");
                xmlWriter.WriteString("551125050200");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Email");
                xmlWriter.WriteString(user);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();                            /*CLOSE_CommunicationDetails*/

                xmlWriter.WriteEndElement();                            /*CLOSE_ContactInformation*/

                xmlWriter.WriteStartElement("DateTime");
                xmlWriter.WriteAttributeString("Type", "DateTime");
                xmlWriter.WriteString(RequestDateTimeStamp);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("MovementType");
                xmlWriter.WriteString("PortToPort");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("GeneralInformation");      /*OPEN_GeneralInformation*/
                xmlWriter.WriteStartElement("Text");
                xmlWriter.WriteString("FREIGHT PREPAID ABROAD BY CELLULOSE VLOG AUSTRIA GMBH");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /*CLOSE_GeneralInformation*/

                xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("PlaceOfDelivery");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "UNLOC");
                xmlWriter.WriteString(entReserva.CodigoPuertoDescarga);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(entReserva.PuertoDescarga);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryName");
                xmlWriter.WriteString(entReserva.PaisDescarga );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryCode");
                xmlWriter.WriteString(entReserva.CodigoPaisDescarga );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /*CLOSE_Location*/

                xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("PlaceOfReceipt");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "UNLOC");
                xmlWriter.WriteString(entReserva.CodigoPuertoEmbarque );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(entReserva.PuertoEmbarque );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryName");
                xmlWriter.WriteString(entReserva.PaisEmbarque);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryCode");
                xmlWriter.WriteString(entReserva.CodigoPaisEmbarque);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("DateTime");
                xmlWriter.WriteAttributeString("DateType", "EarliestDeparture");
                xmlWriter.WriteAttributeString("Type", "DateTime");
                xmlWriter.WriteString(RequestDateTimeStamp);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();                            /*CLOSE_Location*/

                xmlWriter.WriteStartElement("ReferenceInformation");            /*ReferenceInformation*/
                xmlWriter.WriteAttributeString("Type", "ContractNumber");
                xmlWriter.WriteStartElement("Value");
                xmlWriter.WriteString(entReserva.NroContrato);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                                    /*CLOSE_ReferenceInformation */

                xmlWriter.WriteStartElement("TransportationDetails");                /*OPEN_TransportationDetails */
                xmlWriter.WriteAttributeString("TransportStage", "Main");
                xmlWriter.WriteAttributeString("TransportMode", "MaritimeTransport");
                xmlWriter.WriteStartElement("ConveyanceInformation");               /*OPEN_ConveyanceInformation*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("ContainerShip");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "VesselName");
                xmlWriter.WriteString(entReserva.NombreNave);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "VoyageNumber");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("OperatorIdentifier");
                xmlWriter.WriteAttributeString("Type", "SCACCode");
                xmlWriter.WriteString(entReserva.CodLinea);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                                            /*CLOSE_ConveyanceInformation*/


                xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("PortOfLoad");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "UNLOC");
                xmlWriter.WriteString(entReserva.CodigoPuertoEmbarque );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(entReserva.PuertoEmbarque);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryName");
                xmlWriter.WriteString(entReserva.PaisEmbarque );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryCode");
                xmlWriter.WriteString(entReserva.CodigoPaisEmbarque );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /*CLOSE_Location*/



                xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("PortOfDischarge");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "UNLOC");
                xmlWriter.WriteString(entReserva.CodigoPuertoDescarga);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(entReserva.PuertoDescarga );
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryName");
                xmlWriter.WriteString(entReserva.PaisDescarga);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CountryCode");
                xmlWriter.WriteString(entReserva.CodigoPaisDescarga);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /*CLOSE_Location*/

                xmlWriter.WriteEndElement();                            /*CLOSE_TransportationDetails */




                xmlWriter.WriteStartElement("Party");                /*OPEN_Party*/
                xmlWriter.WriteStartElement("Role");
                xmlWriter.WriteString("Carrier");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString(entReserva.Linea);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "PartnerAlias");
                xmlWriter.WriteString(entReserva.CodLinea);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                          /*CLOSE_Party*/

                xmlWriter.WriteStartElement("Party");                /*OPEN_Party*/
                xmlWriter.WriteStartElement("Role");
                xmlWriter.WriteString("Booker");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString("VLOG ");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "PartnerAlias");
                xmlWriter.WriteString("VLOG");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Contacts");                /*OPEN_Contacts*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("InformationContact");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString("Patricia Alves");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CommunicationDetails");    /*OPEN_CommunicationDetails*/
                xmlWriter.WriteStartElement("Phone");
                xmlWriter.WriteString("551125050200");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Email");
                xmlWriter.WriteString("shipping@VVLOG.COM");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /*CLOSE_CommunicationDetails*/
                xmlWriter.WriteEndElement();                /*CLOSE_Contacts*/
                xmlWriter.WriteEndElement();                          /*CLOSE_Party*/

                xmlWriter.WriteStartElement("Party");                /*OPEN_Party*/
                xmlWriter.WriteStartElement("Role");
                xmlWriter.WriteString("Shipper");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString("VLOG ");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "PartnerAlias");
                xmlWriter.WriteString("VLOG");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Contacts");                /*OPEN_Contacts*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("InformationContact");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString("JULIANE CAETANO");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CommunicationDetails");    /*OPEN_CommunicationDetails*/
                xmlWriter.WriteStartElement("Phone");
                xmlWriter.WriteString("551125050200");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Email");
                xmlWriter.WriteString("shipping@VVLOG.COM");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /*CLOSE_CommunicationDetails*/
                xmlWriter.WriteEndElement();                /*CLOSE_Contacts*/

                xmlWriter.WriteStartElement("ChargeCategory");                                  /* OPEN_ChargeCategory*/
                xmlWriter.WriteAttributeString("ChargeType", "OceanFreight");
                xmlWriter.WriteStartElement("PrepaidCollector");
                xmlWriter.WriteAttributeString("PrepaidorCollectIndicator", "PrePaid");
                xmlWriter.WriteString("Y");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("ChargeLocation");                                   /* OPEN_ChargeLocation*/
                xmlWriter.WriteStartElement("Type");
                xmlWriter.WriteString("PrepaidChargeLocation");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Identifier");
                xmlWriter.WriteAttributeString("Type", "UNLOC");
                xmlWriter.WriteString("USNYC");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Name");
                xmlWriter.WriteString("NEW YORK");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                                                       /* CLOSE_ChargeLocation*/
                xmlWriter.WriteEndElement();                                                       /* CLOSE_ChargeCategory*/
                xmlWriter.WriteEndElement();                                                        /*CLOSE_Party*/
                xmlWriter.WriteEndElement();                                                        /*CLOSE_MessageProperties*/
                xmlWriter.WriteStartElement("MessageDetails");                                      /* OPEN_MessageDetails*/
                xmlWriter.WriteStartElement("GoodsDetails");                                      /* OPEN_GoodsDetails*/
                xmlWriter.WriteStartElement("LineNumber");
                xmlWriter.WriteString("1");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("PackageDetail");
                xmlWriter.WriteStartElement("OuterPack");
                xmlWriter.WriteStartElement("GoodGrossWeight");
                xmlWriter.WriteAttributeString("UOM", "KGM");
                xmlWriter.WriteString("26000.000");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("CommodityClassification");
                xmlWriter.WriteAttributeString("Type", "USHTS");
                xmlWriter.WriteString(entReserva.CodigoCommodity);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("GoodDescription");
                xmlWriter.WriteString("Celulose Bqda Eucalipto FSC - 2 ara");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                                                           /* CLOSE_GoodsDetails*/


                List<TipoContenedor> listaContenedor = new List<TipoContenedor>();
                listaContenedor = new OdooService(_configuration, _memoryCache).ListarTipoContenedor();
                var Contenedor = listaContenedor.Where(x => x.Id == entReserva.IdTipoContenedor).ToList();
                //listaTipoContenedor.
                //var lis = listaTipoContenedor


                xmlWriter.WriteStartElement("EquipmentDetails");
                xmlWriter.WriteStartElement("EquipmentType");
                xmlWriter.WriteStartElement("EquipmentTypeCode");
                xmlWriter.WriteString(Contenedor[0].CodigoInternacional);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("EquipmentDescription");
                xmlWriter.WriteString(entReserva.TipoContenedor);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("NumberOfEquipment");
                xmlWriter.WriteString("1");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("ImportExportHaulage");             /*OPEN_ImportExportHaulage*/
                xmlWriter.WriteStartElement("CargoMovementType");
                xmlWriter.WriteString("FCL/FCL");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("HaulageArrangements");
                xmlWriter.WriteString("MerchantExportHaulageMerchantImportHaulage");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();                            /* CLOSE_ImportExportHaulage*/


                if (entReserva.ClaseContenedor.ToLower().Trim().Contains("reefer"))
                {
                    xmlWriter.WriteStartElement("EquipmentTemperature");
                if(entReserva.TipoTemperatura == "°F")
                {
                    tipoTemperatuea = "FAH";
                }
                else
                {
                    tipoTemperatuea = "CEL";
                }
                xmlWriter.WriteAttributeString("UOM", tipoTemperatuea);
                    xmlWriter.WriteString(Convert.ToDouble(entReserva.Temperatura).ToString());
                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteStartElement("EquipmentComments");
                xmlWriter.WriteStartElement("Category");
                xmlWriter.WriteString("GeneralEquipmentComments");
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Text");
                xmlWriter.WriteString("Container Limpo, sem mancha e odor. Supertestado");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();                            /*CLOSE_EquipmentComments*/
                xmlWriter.WriteEndElement();                            /*CLOSE_EquipmentDetails*/
                xmlWriter.WriteEndElement();                            /*CLOSE_MessageDetails*/
                xmlWriter.WriteEndElement();                            /*CLOSE_MessageBody*/


                #endregion

                xmlWriter.Close();
            string host = _configuration.GetValue<string>(
               "Inttra:host");
            string username = _configuration.GetValue<string>(
                "Inttra:username");
            string password = _configuration.GetValue<string>(
                "Inttra:password");
            string workingdirectory = _configuration.GetValue<string>(
                "Inttra:workingdirectory");
            int port = _configuration.GetValue<int>(
                "Inttra:port");

            //string host = "ftp.inttraworks.inttra.com";
            //string username = "v0649108";
            //string password = "Q3oh4Z66";
            //string workingdirectory = "/inbound";
            ////@"E:\Trabajo\Klickit\XmlDocument\XmlDocument\bin\Debug\netcoreapp3.1\xmlPrueba.xml";
            //int port = 22;

            //bool validator = false;

            //string xsdFile = Path.Combine(@url, "INTTRABooking2Request.xsd");
            string uploadfile = Path.Combine(@url, nombre);
            var xdoc = XDocument.Load(uploadfile);
                //var schemas = new XmlSchemaSet();
                //using (FileStream stream = File.OpenRead(xsdFile))
                //{
                //    schemas.Add(XmlSchema.Read(stream, (s, e) =>
                //    {
                //        var x = e.Message;
                //    }));
                //}

                bool isvalid = true;
                StringBuilder sb = new StringBuilder();
                //try
                //{
                //    xdoc.Validate(schemas, (s, e) =>
                //    {
                //        isvalid = false;
                //        sb.AppendLine(string.Format("Line : {0}, Message : {1} ",
                //            e.Exception.LineNumber, e.Exception.Message));
                //    });
                //}
                //catch (XmlSchemaValidationException)
                //{
                //    isvalid = false;
                //}

                if (isvalid)
                {
                    using (SftpClient client = new SftpClient(host, port, username, password))
                    {
                        {
                            client.Connect();
                            //Console.WriteLine("Connected to { 0}", host);

                            client.ChangeDirectory(workingdirectory);
                            //Console.WriteLine("Changed directory to { 0}", workingdirectory);

                            //var listDirectory = client.ListDirectory(workingdirectory, null);
                            //Console.WriteLine("Listing directory:");
                            //foreach (var fi in listDirectory)
                            //{
                            //    //Console.WriteLine(fi.Name);
                            //}

                            using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(uploadfile), null);
                            }
                        }
                    }
                }

            return "";
        }
        #endregion





    }

}
