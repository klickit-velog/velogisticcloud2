﻿using System;

namespace VELogisticCloud.Servicio.Business.Responses
{
    public static class ResponseExtensions
    {
        public static void SetError(this IBaseResponse response, Exception ex)
        {
            response.HizoError = true;
            response.MensajeError = ex.ToString();
        }
    }
}
