﻿using System.Collections.Generic;

namespace VELogisticCloud.Servicio.Business.Responses
{
    public interface IListResponse<TModel> : IBaseResponse
    {
        IEnumerable<TModel> Model { get; set; }
    }
}
