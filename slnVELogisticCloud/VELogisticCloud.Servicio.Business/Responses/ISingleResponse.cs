﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Business.Responses
{
    public interface ISingleResponse<TModel> : IBaseResponse
    {
        TModel Model { get; set; }
    }
}
