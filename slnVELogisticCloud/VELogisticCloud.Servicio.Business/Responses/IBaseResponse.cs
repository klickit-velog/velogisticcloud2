﻿namespace VELogisticCloud.Servicio.Business.Responses
{
    public interface IBaseResponse
    {
        string Mensaje { get; set; }
        bool HizoError { get; set; }
        string MensajeError { get; set; }
    }
}
