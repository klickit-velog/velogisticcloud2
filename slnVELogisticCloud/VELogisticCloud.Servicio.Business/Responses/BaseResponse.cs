﻿namespace VELogisticCloud.Servicio.Business.Responses
{
    public class BaseResponse : IBaseResponse
    {
        public string Mensaje { get; set; }
        public bool HizoError { get; set; }
        public string MensajeError { get; set; }
    }
}
