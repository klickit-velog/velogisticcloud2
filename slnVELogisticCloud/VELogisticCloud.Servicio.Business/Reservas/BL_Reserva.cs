﻿using Microsoft.Extensions.Configuration;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.Models;
using VELogisticCloud.Models.Booking;
using VELogisticCloud.Models.General;
using VELogisticCloud.Servicio.Data.ConexionADO.Itinerario;
using VELogisticCloud.Servicio.Data.ConexionADO.Reservas;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.Servicio.Business.Reservas
{


    public class BL_Reserva
    {
        Cls_Datos_Itinerario cls_datos;

        private IConfiguration _configuration;
        DA_Reserva da;
        public BL_Reserva(IConfiguration configuration)
        {
            _configuration = configuration;
            da = new DA_Reserva(configuration);
            cls_datos = new Cls_Datos_Itinerario(configuration);
        }
        public object BL_ListaItinerarioManual()
        {
            try
            {
                return cls_datos.ListaItinerarioManual().Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BulkInsert(List<BE_ItinerarioManual> ent)
        {
            try
            {
                cls_datos.BulkInsert(ent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object UpdateReserva(int id, Ent_Reserva ent)
        {
            try
            {
                return da.UpdateReserva(id,ent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public object BookingTOoddo(int id)
        //{
        //    try
        //    {
        //        return da.BookingTOoddo(id);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        #region CustomerService
       //public object BL_ConfirmarReserva(decimal id)
       // {
       //     try
       //     {
       //         return da.Confirmar_booking(id);
       //     }
       //     catch (Exception ex)
       //     {
       //         throw ex;
       //     }
       // }
        
       //public object BL_Estado_Envio_Oddo_booking(decimal id)
       // {
       //     try
       //     {
       //         return da.Estado_Envio_Oddo_booking(id);
       //     }
       //     catch (Exception ex)
       //     {
       //         throw ex;
       //     }
       // }
       // public object BL_RechazarReserva(decimal id)
       // {
       //     try
       //     {
       //         return da.rechazar_booking(id);
       //     }
       //     catch (Exception ex)
       //     {
       //         throw ex;
       //     }
       // }

        public object BL_ListaNavierasOdooVE()
        {
            try
            {
                return da.ListaNavierasOdooVE().Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        //public string envioXML(decimal id)
        //{

        //    //string url = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
        //    string url = Path.Combine(Environment.CurrentDirectory+ "\\xml-inttra");
        //    string nombre = Guid.NewGuid().ToString() + ".xml";
        //    string RequestDateTimeStamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss");
        //    string Booking = "Booking";

        //    var res = da.ReservexID(id, "", "", "", "",  0,"");
        //    List<Ent_Reserva> lentReserva = (List<Ent_Reserva>)res;
        //    Ent_Reserva entReserva = (Ent_Reserva)lentReserva[0];
        //    try
        //    {
        //        XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
        //        xmlWriterSettings.Encoding = new UTF8Encoding(false);
        //        //WriteProcessingInstruction(“xml”, “version =’1.0′ encoding =’UTF - 8′”);
        //        //xmlWriterSettings.Encoding = Encoding.GetEncoding("ISO-8859-1");
        //        //WriteProcessingInstruction(“xml”, “version =’1.0′ encoding =’UTF - 8′”);
        //        //xmlWriterSettings.Encoding = Encoding.ASCII;
        //        //xmlWriterSettings.CheckCharacters = true;
        //        XmlWriter xmlWriter = XmlWriter.Create(url +"\\"+  nombre, xmlWriterSettings);
        //        //xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding=\"utf-8\" ");
        //        //xmlWriter.WriteProcessingInstruction("xml", " version=\'1.0\'   encoding=\"utf-8\" "); 
        //        xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding=\"utf-8\""); 
        //        //xmlWriter.Settings.
        //        //xmlWriter.WriteStartDocument();

        //        xmlWriter.WriteStartElement("n1", "Message", @"http://xml.inttra.com/booking/services/01");

        //        #region Entidades
        //        List<Cabecera> ListaCabecera = new List<Cabecera>();
        //        Cabecera ent0 = new Cabecera();
        //        ent0.title = "SenderId";
        //        ent0.detalle = "VLOG";
        //        ListaCabecera.Add(ent0);

        //        Cabecera ent1 = new Cabecera();
        //        ent1.title = "ReceiverId";
        //        ent1.detalle = "INTTRA";
        //        ListaCabecera.Add(ent1);

        //        Cabecera ent2 = new Cabecera();
        //        ent2.title = "RequestDateTimeStamp";
        //        ent2.detalle = RequestDateTimeStamp;
        //        ListaCabecera.Add(ent2);

        //        Cabecera ent3 = new Cabecera();
        //        ent3.title = "RequestMessageVersion";
        //        ent3.detalle = "1.0";
        //        ListaCabecera.Add(ent3);

        //        Cabecera ent4 = new Cabecera();
        //        ent4.title = "TransactionType";
        //        ent4.detalle = Booking;
        //        ListaCabecera.Add(ent4);

        //        Cabecera ent5 = new Cabecera();
        //        ent5.title = "TransactionVersion";
        //        ent5.detalle = "2.0";
        //        ListaCabecera.Add(ent5);

        //        Cabecera ent6 = new Cabecera();
        //        ent6.title = "DocumentIdentifier";
        //        ent6.detalle = "000000" + entReserva.Id_Reserva;
        //        ListaCabecera.Add(ent6);

        //        Cabecera ent7 = new Cabecera();
        //        ent7.title = "TransactionStatus";
        //        ent7.detalle = "Original";
        //        ListaCabecera.Add(ent7);

        //        #endregion

        //        #region Body XML
        //        xmlWriter.WriteStartElement("Header");
        //        foreach (var lista in ListaCabecera)
        //        {
        //            xmlWriter.WriteStartElement(lista.title);
        //            xmlWriter.WriteString(lista.detalle);
        //            xmlWriter.WriteEndElement();
        //        }
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("MessageBody");             /*OPEN_MessageBody*/

        //        xmlWriter.WriteStartElement("MessageProperties");       /*OPEN_MessageProperties*/

        //        xmlWriter.WriteStartElement("ShipmentID");
        //        xmlWriter.WriteString("000000" + entReserva.Id_Reserva);
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("ContactInformation");      /*OPEN_ContactInformation*/

        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("InformationContact");
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString("Usuario logueo");                // TODO: Agregar aqui el usuario logueado
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("CommunicationDetails");    /*OPEN_CommunicationDetails*/

        //        xmlWriter.WriteStartElement("Phone");
        //        xmlWriter.WriteString("551125050200");
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("Email");
        //        xmlWriter.WriteString(entReserva.Codigo_Usuario);
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteEndElement();                            /*CLOSE_CommunicationDetails*/

        //        xmlWriter.WriteEndElement();                            /*CLOSE_ContactInformation*/

        //        xmlWriter.WriteStartElement("DateTime");
        //        xmlWriter.WriteAttributeString("Type", "DateTime");
        //        xmlWriter.WriteString(RequestDateTimeStamp);
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("MovementType");
        //        xmlWriter.WriteString("PortToPort");
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("GeneralInformation");      /*OPEN_GeneralInformation*/
        //        xmlWriter.WriteStartElement("Text");
        //        xmlWriter.WriteString("FREIGHT PREPAID ABROAD BY CELLULOSE VLOG AUSTRIA GMBH");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /*CLOSE_GeneralInformation*/

        //        xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("PlaceOfDelivery");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "UNLOC");
        //        xmlWriter.WriteString(entReserva.Codigo_Puerto_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString(entReserva.Puerto_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryName");
        //        xmlWriter.WriteString(entReserva.Pais_Puerto_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryCode");
        //        xmlWriter.WriteString(entReserva.Codigo_Pais_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /*CLOSE_Location*/

        //        xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("PlaceOfReceipt");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "UNLOC");
        //        xmlWriter.WriteString(entReserva.Codigo_Puerto_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString(entReserva.Puerto_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryName");
        //        xmlWriter.WriteString(entReserva.Pais_Puerto_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryCode");
        //        xmlWriter.WriteString(entReserva.Codigo_Pais_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("DateTime");
        //        xmlWriter.WriteAttributeString("DateType", "EarliestDeparture");
        //        xmlWriter.WriteAttributeString("Type", "DateTime");
        //        xmlWriter.WriteString(RequestDateTimeStamp);
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteEndElement();                            /*CLOSE_Location*/

        //        xmlWriter.WriteStartElement("ReferenceInformation");            /*ReferenceInformation*/
        //        xmlWriter.WriteAttributeString("Type", "ContractNumber");
        //        xmlWriter.WriteStartElement("Value");
        //        xmlWriter.WriteString(entReserva.Nro_contrato);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                                    /*CLOSE_ReferenceInformation */

        //        xmlWriter.WriteStartElement("TransportationDetails");                /*OPEN_TransportationDetails */
        //        xmlWriter.WriteAttributeString("TransportStage", "Main");
        //        xmlWriter.WriteAttributeString("TransportMode", "MaritimeTransport");
        //        xmlWriter.WriteStartElement("ConveyanceInformation");               /*OPEN_ConveyanceInformation*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("ContainerShip");
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "VesselName");
        //        xmlWriter.WriteString(entReserva.Nombre_Nave);
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "VoyageNumber");
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteStartElement("OperatorIdentifier");
        //        xmlWriter.WriteAttributeString("Type", "SCACCode");
        //        xmlWriter.WriteString(entReserva.Id_Naviera);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                                            /*CLOSE_ConveyanceInformation*/


        //        xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("PortOfLoad");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "UNLOC");
        //        xmlWriter.WriteString(entReserva.Codigo_Puerto_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString(entReserva.Puerto_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryName");
        //        xmlWriter.WriteString(entReserva.Pais_Puerto_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryCode");
        //        xmlWriter.WriteString(entReserva.Codigo_Pais_Descarga);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /*CLOSE_Location*/



        //        xmlWriter.WriteStartElement("Location");      /*OPEN_Location*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("PortOfDischarge");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "UNLOC");
        //        xmlWriter.WriteString(entReserva.Codigo_Puerto_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString(entReserva.Puerto_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryName");
        //        xmlWriter.WriteString(entReserva.Pais_Puerto_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CountryCode");
        //        xmlWriter.WriteString(entReserva.Codigo_Pais_Embarque);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /*CLOSE_Location*/

        //        xmlWriter.WriteEndElement();                            /*CLOSE_TransportationDetails */
                
     


        //        xmlWriter.WriteStartElement("Party");                /*OPEN_Party*/
        //        xmlWriter.WriteStartElement("Role");
        //        xmlWriter.WriteString("Carrier");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString(entReserva.Naviera);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "PartnerAlias");
        //        xmlWriter.WriteString(entReserva.Id_Naviera);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                          /*CLOSE_Party*/

        //        xmlWriter.WriteStartElement("Party");                /*OPEN_Party*/
        //        xmlWriter.WriteStartElement("Role");
        //        xmlWriter.WriteString("Booker");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString("VLOG ");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "PartnerAlias");
        //        xmlWriter.WriteString("VLOG");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Contacts");                /*OPEN_Contacts*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("InformationContact");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString("Patricia Alves");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CommunicationDetails");    /*OPEN_CommunicationDetails*/
        //        xmlWriter.WriteStartElement("Phone");
        //        xmlWriter.WriteString("551125050200");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Email");
        //        xmlWriter.WriteString("shipping@VVLOG.COM");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /*CLOSE_CommunicationDetails*/
        //        xmlWriter.WriteEndElement();                /*CLOSE_Contacts*/
        //        xmlWriter.WriteEndElement();                          /*CLOSE_Party*/

        //        xmlWriter.WriteStartElement("Party");                /*OPEN_Party*/
        //        xmlWriter.WriteStartElement("Role");
        //        xmlWriter.WriteString("Shipper");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString("VLOG ");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "PartnerAlias");
        //        xmlWriter.WriteString("VLOG");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Contacts");                /*OPEN_Contacts*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("InformationContact");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString("JULIANE CAETANO");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CommunicationDetails");    /*OPEN_CommunicationDetails*/
        //        xmlWriter.WriteStartElement("Phone");
        //        xmlWriter.WriteString("551125050200");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Email");
        //        xmlWriter.WriteString("shipping@VVLOG.COM");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /*CLOSE_CommunicationDetails*/
        //        xmlWriter.WriteEndElement();                /*CLOSE_Contacts*/

        //        xmlWriter.WriteStartElement("ChargeCategory");                                  /* OPEN_ChargeCategory*/
        //        xmlWriter.WriteAttributeString("ChargeType", "OceanFreight");
        //        xmlWriter.WriteStartElement("PrepaidCollector");
        //        xmlWriter.WriteAttributeString("PrepaidorCollectIndicator", "PrePaid");
        //        xmlWriter.WriteString("Y");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("ChargeLocation");                                   /* OPEN_ChargeLocation*/
        //        xmlWriter.WriteStartElement("Type");
        //        xmlWriter.WriteString("PrepaidChargeLocation");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Identifier");
        //        xmlWriter.WriteAttributeString("Type", "UNLOC");
        //        xmlWriter.WriteString("USNYC");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Name");
        //        xmlWriter.WriteString("NEW YORK");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                                                       /* CLOSE_ChargeLocation*/
        //        xmlWriter.WriteEndElement();                                                       /* CLOSE_ChargeCategory*/
        //        xmlWriter.WriteEndElement();                                                        /*CLOSE_Party*/
        //        xmlWriter.WriteEndElement();                                                        /*CLOSE_MessageProperties*/
        //        xmlWriter.WriteStartElement("MessageDetails");                                      /* OPEN_MessageDetails*/
        //        xmlWriter.WriteStartElement("GoodsDetails");                                      /* OPEN_GoodsDetails*/
        //        xmlWriter.WriteStartElement("LineNumber");
        //        xmlWriter.WriteString("1");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("PackageDetail");
        //        xmlWriter.WriteStartElement("OuterPack");
        //        xmlWriter.WriteStartElement("GoodGrossWeight");
        //        xmlWriter.WriteAttributeString("UOM", "KGM");
        //        xmlWriter.WriteString("26000.000");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("CommodityClassification");
        //        xmlWriter.WriteAttributeString("Type", "USHTS");
        //        xmlWriter.WriteString(entReserva.Codigo_Commodity);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("GoodDescription");
        //        xmlWriter.WriteString("Celulose Bqda Eucalipto FSC - 2 ara");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                                                           /* CLOSE_GoodsDetails*/


        //        List<TipoContenedor> listaContenedor = new List<TipoContenedor>();
        //        listaContenedor = new OdooService().ListarTipoContenedor();
        //        var Contenedor = listaContenedor.Where(x => x.Nombre  == entReserva.Tipo_Contenedor).ToList();
        //        //listaTipoContenedor.
        //        //var lis = listaTipoContenedor


        //        xmlWriter.WriteStartElement("EquipmentDetails");
        //        xmlWriter.WriteStartElement("EquipmentType");
        //        xmlWriter.WriteStartElement("EquipmentTypeCode");
        //        xmlWriter.WriteString(Contenedor[0].CodigoInternacional);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("EquipmentDescription");
        //        xmlWriter.WriteString(entReserva.Tipo_Contenedor);
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("NumberOfEquipment");
        //        xmlWriter.WriteString("1");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("ImportExportHaulage");             /*OPEN_ImportExportHaulage*/
        //        xmlWriter.WriteStartElement("CargoMovementType");
        //        xmlWriter.WriteString("FCL/FCL");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("HaulageArrangements");
        //        xmlWriter.WriteString("MerchantExportHaulageMerchantImportHaulage");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteEndElement();                            /* CLOSE_ImportExportHaulage*/


        //        if (entReserva.Tipo.ToLower().Trim().Contains("reefer"))
        //        {
        //            xmlWriter.WriteStartElement("EquipmentTemperature");
        //            xmlWriter.WriteAttributeString("UOM", "CEL");
        //            xmlWriter.WriteString(Convert.ToDouble(entReserva.Temperatura).ToString());
        //            xmlWriter.WriteEndElement();
        //        }

        //        xmlWriter.WriteStartElement("EquipmentComments");
        //        xmlWriter.WriteStartElement("Category");
        //        xmlWriter.WriteString("GeneralEquipmentComments");
        //        xmlWriter.WriteEndElement();
        //        xmlWriter.WriteStartElement("Text");
        //        xmlWriter.WriteString("Container Limpo, sem mancha e odor. Supertestado");
        //        xmlWriter.WriteEndElement();

        //        xmlWriter.WriteEndElement();                            /*CLOSE_EquipmentComments*/
        //        xmlWriter.WriteEndElement();                            /*CLOSE_EquipmentDetails*/
        //        xmlWriter.WriteEndElement();                            /*CLOSE_MessageDetails*/
        //        xmlWriter.WriteEndElement();                            /*CLOSE_MessageBody*/


        //        #endregion

        //        xmlWriter.Close();

        //        string host = "prueba";
        //        //string username = "v0649108";
        //        string username = "prueba";
        //        string password = "Q3oh4Z66";
        //        string workingdirectory = "/inbound";


        //        string uploadfile = Path.Combine(@url, nombre);
        //        //@"E:\Trabajo\Klickit\XmlDocument\XmlDocument\bin\Debug\netcoreapp3.1\xmlPrueba.xml";
        //        int port = 22;

        //        //bool validator = false;

        //        string xsdFile = Path.Combine(Environment.CurrentDirectory, "INTTRABooking2Request.xsd");

        //        var xdoc = XDocument.Load(uploadfile);
        //        var schemas = new XmlSchemaSet();
        //        using (FileStream stream = File.OpenRead(xsdFile))
        //        {
        //            schemas.Add(XmlSchema.Read(stream, (s, e) =>
        //            {
        //                var x = e.Message;
        //            }));
        //        }

        //        bool isvalid = true;
        //        StringBuilder sb = new StringBuilder();
        //        try
        //        {
        //            xdoc.Validate(schemas, (s, e) =>
        //            {
        //                isvalid = false;
        //                sb.AppendLine(string.Format("Line : {0}, Message : {1} ",
        //                    e.Exception.LineNumber, e.Exception.Message));
        //            });
        //        }
        //        catch (XmlSchemaValidationException)
        //        {
        //            isvalid = false;
        //        }

        //        if (isvalid)
        //        {
        //            using (SftpClient client = new SftpClient(host, port, username, password))
        //            {
        //                {
        //                    client.Connect();
        //                    //Console.WriteLine("Connected to { 0}", host);

        //                    client.ChangeDirectory(workingdirectory);
        //                    //Console.WriteLine("Changed directory to { 0}", workingdirectory);

        //                    //var listDirectory = client.ListDirectory(workingdirectory, null);
        //                    //Console.WriteLine("Listing directory:");
        //                    //foreach (var fi in listDirectory)
        //                    //{
        //                    //    //Console.WriteLine(fi.Name);
        //                    //}

        //                    using (var fileStream = new FileStream(uploadfile, FileMode.Open))
        //                    {
        //                        client.BufferSize = 4 * 1024; // bypass Payload error large files
        //                         client.UploadFile(fileStream, Path.GetFileName(uploadfile), null);
        //                    }
        //                }
        //            }
        //        }
                



        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return "";
        //}

        #endregion

    }
}
