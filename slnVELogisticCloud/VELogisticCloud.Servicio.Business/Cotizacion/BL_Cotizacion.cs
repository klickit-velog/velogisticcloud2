﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.CrossCutting.Entities;
using VELogisticCloud.Servicio.Data.ConexionADO.Cotizacion;
using static VELogisticCloud.Models.Cotizacion.BE_Cotizacion;

namespace VELogisticCloud.Servicio.Business.Cotizacion
{
    public class BL_Cotizacion
    {
        private DA_Cotizacion da;

        public BL_Cotizacion(IConfiguration configuration)
        {
            da = new DA_Cotizacion(configuration);
        }

        public object BL_Lista_Cotizaciones(Parametros_Lista_Maritimo param)
        {
            return da.Lista_Cotizaciones(param).Result;
        }
        public object BL_Lista_CotizacionesAprobadas(Parametros_Lista_Maritimo param)
        {
            return da.Lista_CotizacionesAprobadas(param).Result;
        }
        //Lista_CotizacionesAprobadas
        public object BL_ListaCotizacionAprobadasLogistica(Parametros_Lista_Maritimo param)
        {
            return da.ListaCotizacionAprobadasLogistica(param).Result;
        }
        public object BL_MisSolicitudesMaritimo(Parametros_Lista_Maritimo param)
        {
            return da.MisSolicitudesMaritimo(param).Result;

        }

        public object BL_MisSolicitudes_Logistica(Parametros_Lista_Maritimo param)
        {
            return da.MisSolicitudes_Logistica(param).Result;
        }

        public object BL_Lista_Logistica(Parametros_Lista_Logistica param)
        {
            return da.Lista_Logistica(param).Result;
        }
        public object BL_InsertMaritimoCabecera(BE_Maritimo ent)
        {
           return da.InsertMaritimoCabecera(ent).Result;
        }

        public object BL_InsertIdOddo_Maritimo(int idcotizacionmaritimo ,int idoddo)
        {
            return da.InsertIdOddo_Maritimo(idcotizacionmaritimo, idoddo).Result;
        }

        public object BL_InsertIdOddo_Logistica(int idcotizacionmaritimo, int idoddo)
        {
            return da.InsertIdOddo_Logistica(idcotizacionmaritimo, idoddo).Result;
        }


        public object BL_InsertMaritimoDetalle(BE_Det_Maritimo ent)
        {
            return da.InsertMaritimoDetalle(ent).Result;

        }
        public object DeleteMaritimoDetalle(int id)
        {
            return da.DeleteMaritimoDetalle(id).Result;
        }
        public object UpdateMaritimoCabecera(int id, BE_Maritimo ent, string UserUpdate)
        {
            return da.UpdateMaritimoCabecera(id, ent, UserUpdate).Result;

        }
        public object BL_Confirmar_RechazarCotizacionMaritimo(int id, int estado)
        {
            return da.Confirmar_RechazarCotizacionMaritimo(id, estado).Result;
        }

        public object BL_InsertOpeLogi(BE_Logistica ent)
        {
            return da.InsertOpeLogi(ent).Result;
        }

        public object BL_DetalleLogistica(int id)
        {
            return da.DetalleLogistica(id).Result;

        }

        public object BL_DetalleMaritimo(int id)
        {
            return da.DetalleMaritimo(id).Result;

        }

        public object BL_TablaDetalleLogistica(int id)
        {
            return da.TablaDetalleLogistica(id).Result;

        }
        public object DatosEditarMaritimo(int id)
        {
            return da.DatosEditarMaritimo(id).Result;
        }


        public object DatosEditarLogistica(int id)
        {
            return da.DatosEditarLogistica(id).Result;
        }
        public object BL_UpdateCotizacionLogistica(int id, BE_Logistica ent)
        {
            return da.UpdateCotizacionLogistica(id, ent).Result;
        }


       
        public object BL_Confirmar_RechazarCotizacionLogistica(int id, int estado)
        {
            return da.Confirmar_RechazarCotizacionLogistica(id, estado).Result;
        }

        public object BL_InsertDetalleCotizacionAPI(DetalleCotizacionAPI ent)
        {
            return da.InsertDetalleCotizacionAPI(ent).Result;
        }
        public object BL_DetalleCotizacionLogistica(int id)
        {
            return da.DetalleCotizacionLogistica(id).Result;
        }
        public void BulkInsertCotizacionDetalle(List<CotizacionLogisticaDetalle> ent)
        {
            try
            {
                da.BulkInsertCotizacionDetalle(ent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
