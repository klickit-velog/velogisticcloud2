﻿using VELogisticCloud.Servicio.Data;

namespace VELogisticCloud.Servicio.Business
{
    public class Service : IService
    {
        protected bool Disposed;

        public VECloudDBContext DbContext { get; }

        public Service(VECloudDBContext dbContext)
        {
            DbContext = dbContext;
        }

        public void Dispose()
        {
            if (!Disposed)
            {
                DbContext?.Dispose();
                Disposed = true;
            }
        }
    }
}
