﻿using System;
using VELogisticCloud.Servicio.Data;

namespace VELogisticCloud.Servicio.Business
{
    public interface IService : IDisposable
    {
        VECloudDBContext DbContext { get; }
    }
}
