﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace VELogisticCloud.Servicio.Middleware
{
    public class MetodosAsync
    {
        private IConfiguration _configuration;
        private string urlBase = "";

        public MetodosAsync(IConfiguration configuration )
        {
            _configuration = configuration;
            urlBase = _configuration.GetSection("Avanzza:UrlBase").Value;
        }


        public async Task<HttpResponseMessage> getToken(string resource, string body)
        {
            var handler = new WinHttpHandler();
            var tokenClient = new HttpClient(handler);

            tokenClient.BaseAddress = new Uri(urlBase);
            tokenClient.DefaultRequestHeaders.Accept.Clear();
            tokenClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(urlBase + resource),
                Content = new StringContent(body, Encoding.UTF8, "application/json"),
            };

            var tokenResponse = await tokenClient.SendAsync(request);

            return tokenResponse;

        }
        public async Task<HttpResponseMessage> getAsync(string resource, string token, string body)
        {
            var handler = new WinHttpHandler();
            var client = new HttpClient(handler);

            client.BaseAddress = new Uri(urlBase);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            JsonSerializerSettings settings = new JsonSerializerSettings();

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(urlBase + resource),
                Content = new StringContent(body, Encoding.UTF8, "application/json"),
            };

            var response = await client.SendAsync(request);

            return response;

        }

        public async Task<HttpResponseMessage> postAsync(string resource, string token, string body)
        {
            var handler = new WinHttpHandler();
            var client = new HttpClient(handler);


            client.BaseAddress = new Uri(urlBase);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(urlBase + resource),
                Content = new StringContent(body, Encoding.UTF8, "application/json"),
            };

            var response = await client.SendAsync(request);

            return response;
        }
    }
}
