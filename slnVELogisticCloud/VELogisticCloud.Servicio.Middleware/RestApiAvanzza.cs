﻿
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VELogisticCloud.Servicio.Middleware.Models;

namespace VELogisticCloud.Servicio.Middleware
{
    public class RestApiAvanzza
    {
        private IConfiguration _configuration;
        private string urlBase = "";
        private string token = "";
        MetodosAsync metodos;

        public RestApiAvanzza(IConfiguration configuration)
        {
            _configuration = configuration;
            metodos = new MetodosAsync(_configuration);
            token = _configuration.GetSection("Avanzza:Token").Value;
        }
        
        //private string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI0NyIsImp0aSI6IjBjZWVhYjczMDAxYWIzZjMzMTA4MjlhOWZlOTcwYTEzZWZmNmE3NTBkNmEwM2JkN2U4Y2M5NGYxMDQ3ZGI1MTAyNDM5MjFkOWM5NDJmYjI5IiwiaWF0IjoiMTY0Mzk5ODM2Mi4xNTk5NDMiLCJuYmYiOiIxNjQzOTk4MzYyLjE1OTk1NSIsImV4cCI6IjE2NzU1MzQzNjIuMTQ2NjcwIiwic3ViIjoiMyIsInNjb3BlcyI6W119.E_KjKlMfcfwyiY4TYHR_ZKzcUb8ri8RzaxGEQUU7UNo85aaufGiNEj82CNQ_IiXuvSnKkEUxjvA1D3nh3rxmZMdMOF2DYc035yPVNiUcJDWCx9J5_7XJQ78z3uBmDI88dK_7q9Y2ATn097o4duTEV4d8uRc2mw3MJGjjkWHBj1QcBklzeTF9lloodnxon0ww0NLrZNoXBe4wsKpLoDy5KasynY-fnwR8hB5wWHvFIv_0Iagt6k45D0mVDfQkHYwjDEpeEE88gyUvd19GVrc75iynuS0VFgfIWdNpBcPniRUFtn-ShbbemkR2qYsrcipi6WDV6ndrntbhnjofybwBOZaA7K8aZALebyrEql8eg-HwxRzw3KyBgmHQyyetvP3FeF6or6YQxTQHETSMkUStlR8s2neoC5iKMHm6wJXNboVuF9kvSbA5uy25ldUB2Uja1XQ8_eJdMHyyfX-2tqFco2l9wa_oGkqSIVR7wDohu9OiiiTURAfGKoY48SwrkSVZwHe0ckik93yIdDef3Q96sikHL1rzCQYC1SWcPO3OsQM4B35Y9npuVtwl_XNR3hNaENyxeMnRKt5qgSns974gNIwVT1y42tportwOvOKfwayplCPI8LB7X2kc7OmFlr7xlHVZKdr_REjyDN6NcYvvLe6zpzpYjWLFuvKhIXyY6c0";
        private string user = "fredy.ruiz@ve-logistics.com";
        private string pwd = "12345678";
        public async Task<string> getToken()
        {
            var resource = "";
            var body = $"{{\"username\":\"{user}\", \"password\":\"{pwd}\"}}";

            var tokenResponse = await metodos.getToken(resource, body);
            tokenResponse.EnsureSuccessStatusCode();
            string token = await tokenResponse.Content.ReadAsStringAsync();

            token = token.Replace('"', ' ').Trim();
            return token;
        }

        public async Task<ResponseBase> GetVehiculos()
        {
            var resource = "vehicles";

            var body = string.Empty;

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;

            //var token = await getToken();
            var customersResponse = await metodos.getAsync(resource , token, JsonConvert.SerializeObject(body, settings));
            ResponseBase response = new ResponseBase
            {
                success = customersResponse.IsSuccessStatusCode,
                statusCode = (int)customersResponse.StatusCode,
                message = await customersResponse.Content.ReadAsStringAsync()
            };

            return response;
        }

        public async Task<ResponseBase> SendPedido(Pedido pedido)
        {
            var resource = "/orders";

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;

            //var token = await getToken();
            var responseMagento = await metodos.postAsync(resource, token, JsonConvert.SerializeObject(pedido, settings));

            ResponseBase responseBase = new ResponseBase
            {
                success = responseMagento.IsSuccessStatusCode,
                statusCode = (int)responseMagento.StatusCode,
                message = await responseMagento.Content.ReadAsStringAsync()
            };

            return responseBase;
        }

        public async Task<ResponseBase> GetTransporte(string ordenServicio)
        {
            var resource = string.Concat("/requirement-order/",ordenServicio);

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;

            //var token = await getToken();
            var responseMagento = await metodos.getAsync(resource, token, JsonConvert.SerializeObject("", settings));

            string nombreTransporte = string.Empty;
            string jsonResponse = await responseMagento.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ResponseTransporte>(jsonResponse);
            if (response.order!= null && response.order.transports.Length > 0)
            {
                nombreTransporte = response.order.transports[0].vehicle.provider.name;   
            }

            ResponseBase responseBase = new ResponseBase
            {
                success = responseMagento.IsSuccessStatusCode,
                statusCode = (int)responseMagento.StatusCode,
                message = nombreTransporte
            };

            return responseBase;
        }


    }
}
