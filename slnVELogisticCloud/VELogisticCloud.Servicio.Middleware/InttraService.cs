﻿//using Newtonsoft.Json;
//using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using VELogisticCloud.CrossCutting.DTO;
using VELogisticCloud.Servicio.Middleware;

namespace VELogisticCloud.Servicio.Middleware
{
    public class InttraService
    {
        string baseUrl = "https://api.inttra.com";
        string client_id = "919235-96787";
        string client_secret = "63d3a244ecc0d7dfaebf9da325758f082b917ec1";
        string client_credentials = "client_credentials";
        string baseURL_Token = "/auth";
        string baseURL_Query = "/oceanschedules/schedule?";

        public async Task<List<ItinerarioResponseDTO>> ListarItinerarios(ItinerarioRequestDTO request)
        {
            List<ItinerarioResponse> itinerarioResponse = new List<ItinerarioResponse>();

            try
            {
                string token = GetToken();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(baseUrl);
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Add("authorization", $"Bearer {token}");
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "VE Cloud");

                    var requestSerializado = System.Text.Json.JsonSerializer.Serialize(request);
                    var content = new StringContent(requestSerializado, Encoding.UTF8, "application/json");

                    string parameters = ConvertRequestToQueryList(request);
                    var response = await httpClient.GetAsync(string.Concat(baseURL_Query,parameters));

                    if (response.IsSuccessStatusCode) 
                    {
                        var jsonResponse = await response.Content.ReadAsStringAsync();

                        itinerarioResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItinerarioResponse>>(jsonResponse,
                            new Newtonsoft.Json.Converters.IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd hh:mm:ss" });
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return ToItinerarioResponseDTO(itinerarioResponse);
        }


        private string GetToken()
        {
            string token = string.Empty;

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("client_id", client_id);
                httpClient.DefaultRequestHeaders.Add("client_secret", client_secret);
                httpClient.DefaultRequestHeaders.Add("grant_type", client_credentials);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "VE Cloud");

                HttpResponseMessage response = httpClient.GetAsync(baseURL_Token).Result;

                if (response.IsSuccessStatusCode)
                { 
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;

                    var jsonElement = System.Text.Json.JsonSerializer.Deserialize<System.Text.Json.JsonElement>(jsonResponse);
                    token = jsonElement.GetProperty("access_token").GetString();
                }

                return token;
            }
        }

        private string ConvertRequestToQueryList(ItinerarioRequestDTO request)
        {
            var allIputParams = new List<KeyValuePair<string, string>>();

            string requestParams = string.Empty;

            // Converting Request Params to Key Value Pair.  
            allIputParams.Add(new KeyValuePair<string, string>("originPort", request.originPort));
            allIputParams.Add(new KeyValuePair<string, string>("destinationPort", request.destinationPort));
            allIputParams.Add(new KeyValuePair<string, string>("searchDate", request.searchDate.ToString("yyyy-MM-dd")));
            allIputParams.Add(new KeyValuePair<string, string>("searchDateType", request.searchDateType));
            allIputParams.Add(new KeyValuePair<string, string>("weeksOut", request.weeksOut.ToString()));
            allIputParams.Add(new KeyValuePair<string, string>("scacs", request.sacs));
            allIputParams.Add(new KeyValuePair<string, string>("directOnly", request.directOnly.ToString()));
            allIputParams.Add(new KeyValuePair<string, string>("includeNearbyOriginPorts", request.includeNearbyOriginPorts.ToString()));
            allIputParams.Add(new KeyValuePair<string, string>("includeNearbyDestinationPort", request.includeNearbyDestinationPort.ToString()));

            // URL Request Query parameters.  
            requestParams = new FormUrlEncodedContent(allIputParams).ReadAsStringAsync().Result;

            return requestParams;

        }

        private List<ItinerarioResponseDTO> ToItinerarioResponseDTO(List<ItinerarioResponse> listaItinerarioResponse)
        {
            List<ItinerarioResponseDTO> listaResponseDTO = new List<ItinerarioResponseDTO>();

            foreach (var itinerario in listaItinerarioResponse)
            {
                var itinerarioDTO = new ItinerarioResponseDTO();
                itinerarioDTO.scac = itinerario.scac;
                itinerarioDTO.carrierName = itinerario.carrierName;
                itinerarioDTO.serviceName = itinerario.serviceName;
                itinerarioDTO.vesselName = itinerario.vesselName;
                itinerarioDTO.voyageNumber = itinerario.voyageNumber;
                itinerarioDTO.imoNumber = itinerario.imoNumber;
                itinerarioDTO.originUnloc = itinerario.originUnloc;
                itinerarioDTO.originCountry = itinerario.originCountry;
                itinerarioDTO.originCityName = itinerario.originCityName;
                itinerarioDTO.originSubdivision = itinerario.originSubdivision;
                itinerarioDTO.originTerminal = itinerario.originTerminal;
                itinerarioDTO.destinationUnloc = itinerario.destinationUnloc;
                itinerarioDTO.destinationCountry = itinerario.destinationCountry;
                itinerarioDTO.destinationSubdivision = itinerario.destinationSubdivision;
                itinerarioDTO.destinationCityName = itinerario.destinationCityName;
                itinerarioDTO.destinationTerminal = itinerario.destinationTerminal;
                itinerarioDTO.originDepartureDate = itinerario.originDepartureDate;
                itinerarioDTO.destinationArrivalDate = itinerario.destinationArrivalDate;
                itinerarioDTO.estimatedTerminalCutoff = itinerario.estimatedTerminalCutoff;
                itinerarioDTO.terminalCutoff = itinerario.terminalCutoff;
                itinerarioDTO.bkCutoff = itinerario.bkCutoff;
                itinerarioDTO.siCutoff = itinerario.siCutoff;
                itinerarioDTO.hazBkCutoff = itinerario.hazBkCutoff;
                itinerarioDTO.vgmCutoff = itinerario.vgmCutoff;
                itinerarioDTO.reeferCutoff = itinerario.reeferCutoff;
                itinerarioDTO.totalDuration = itinerario.totalDuration;
                itinerarioDTO.scheduleType = itinerario.scheduleType;

                if (itinerario.legs != null && itinerario.legs.Count > 0)
                {
                    foreach (var leg in itinerario.legs)
                    {
                        var legDTO = new LegDTO();
                        legDTO.sequence = leg.sequence;
                        legDTO.transportID = leg.transportID;
                        legDTO.serviceName = leg.serviceName;
                        legDTO.transportType = leg.transportType;
                        legDTO.transportName = leg.transportName;
                        legDTO.conveyanceNumber = leg.conveyanceNumber;
                        legDTO.departureUnloc = leg.departureUnloc;
                        legDTO.departureCityName = leg.departureCityName;
                        legDTO.departureCountry = leg.departureCountry;
                        legDTO.departureSubdivision = leg.departureSubdivision;
                        legDTO.departureTerminal = leg.departureTerminal;
                        legDTO.departureDate = leg.departureDate;
                        legDTO.arrivalUnloc = leg.arrivalUnloc;
                        legDTO.arrivalCityName = leg.arrivalCityName;
                        legDTO.arrivalCountry = leg.arrivalCountry;
                        legDTO.arrivalSubdivision = leg.arrivalSubdivision;
                        legDTO.arrivalTerminal = leg.arrivalTerminal;
                        legDTO.arrivalDate = leg.arrivalDate;
                        legDTO.transshipmentIndicator = leg.transshipmentIndicator;
                        legDTO.transitDuration = leg.transitDuration;
                        itinerarioDTO.legs.Add(legDTO);
                    }
                }
                listaResponseDTO.Add(itinerarioDTO);
            }

            return listaResponseDTO;
        }
    }

    /// <summary>
    /// Se usa 2 tipos de conversores de fecha, se usa porque los 2 son válidos.
    /// https://stackoverflow.com/a/21272518/2477590
    /// https://stackoverflow.com/a/27928572/2477590
    /// </summary>
    [Newtonsoft.Json.JsonObject(Newtonsoft.Json.MemberSerialization.OptOut)]
    internal class ItinerarioResponse
    {
        public string scac { get; set; }
        public string carrierName { get; set; }
        public string serviceName { get; set; }
        public string vesselName { get; set; }
        public string voyageNumber { get; set; }
        public string imoNumber { get; set; }
        public string originUnloc { get; set; }
        public string originCountry { get; set; }
        public string originCityName { get; set; }
        public string originSubdivision { get; set; }
        public string originTerminal { get; set; }
        public string destinationUnloc { get; set; }
        public string destinationCountry { get; set; }
        public string destinationSubdivision { get; set; }
        public string destinationCityName { get; set; }
        public string destinationTerminal { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(InttraDataFormatJsonConverter))]
        public DateTime? originDepartureDate { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? destinationArrivalDate { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? estimatedTerminalCutoff { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? terminalCutoff { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? bkCutoff { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? siCutoff { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? hazBkCutoff { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? vgmCutoff { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? reeferCutoff { get; set; }
        public int totalDuration { get; set; }
        public string scheduleType { get; set; }
        public List<Leg> legs { get; set; }
    }

    internal class Leg
    {
        public int sequence { get; set; }
        public string transportID { get; set; }
        public string serviceName { get; set; }
        public string transportType { get; set; }
        public string transportName { get; set; }
        public string conveyanceNumber { get; set; }
        public string departureUnloc { get; set; }
        public string departureCityName { get; set; }
        public string departureCountry { get; set; }
        public string departureSubdivision { get; set; }
        public string departureTerminal { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? departureDate { get; set; }
        public string arrivalUnloc { get; set; }
        public string arrivalCityName { get; set; }
        public string arrivalCountry { get; set; }
        public string arrivalSubdivision { get; set; }
        public string arrivalTerminal { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.IsoDateTimeConverter))]
        public DateTime? arrivalDate { get; set; }
        public bool transshipmentIndicator { get; set; }
        public int transitDuration { get; set; }
    }
}
