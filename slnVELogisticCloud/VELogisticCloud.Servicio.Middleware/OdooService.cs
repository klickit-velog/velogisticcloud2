﻿using Odoo.XmlRpcAdapter;
using Odoo.XmlRpcAdapter.Domain;
using Odoo.XmlRpcAdapter.Domain.Operators.Mappers;
using System;
using System.Collections.Generic;
using System.Text;
using VELogisticCloud.CrossCutting.OdooEntity;
using VELogisticCloud.CrossCutting.Comun;
using System.Linq;
using CookComputing.XmlRpc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

namespace VELogisticCloud.Servicio.Middleware
{
    public class OdooService
    {
        private IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        //https://ve-logistics-demo-0907-2858096.dev.odoo.com/web
        //PRODUCCION
        //private const string OdooCommonURL = "https://ve-logistics.odoo.com/xmlrpc/2/common";
        //private const string OdooObjectURL = "https://ve-logistics.odoo.com/xmlrpc/2/object";
        //private const string OdooDatabaseName = "samemotion-ve-logistics-main-2070716";
        //private const string OdooUserName = "vecloud";
        //private const string OdooPassword = "%Ve$cl0uD#s@M3.MOt1oN";

        //DESARROLLO
        //private const string OdooCommonURL = "https://ve-logistics-pruebas-1612-3820391.dev.odoo.com/xmlrpc/2/common";
        //private const string OdooObjectURL = "https://ve-logistics-pruebas-1612-3820391.dev.odoo.com/xmlrpc/2/object";
        //private const string OdooDatabaseName = "ve-logistics-pruebas-1612-3820391";
        //private const string OdooUserName = "vecloud";
        //private const string OdooPassword = "$@m3m0ti0n";

        private const string RUC = "20557679392";
        private const string TERMINAL_DEFECTO_OPERACION_LOGISTICA = "40 High Cube";
        private const int HORAS_CACHE = 2;

        //public OdooXmlRpcAdapter odoo;
        public static string OdooCommonURL = "";
        public static string OdooObjectURL = "";
        public static string OdooDatabaseName = "";
        public static string OdooUserName = "";
        public static string OdooPassword = "";
        OdooXmlRpcAdapter odoo;
        public OdooService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            this._memoryCache = memoryCache;
            OdooCommonURL = _configuration.GetSection("Odoo:OdooCommonURL").Value;
            OdooObjectURL = _configuration.GetSection("Odoo:OdooObjectURL").Value;
            OdooDatabaseName = _configuration.GetSection("Odoo:OdooDatabaseName").Value;
            OdooUserName = _configuration.GetSection("Odoo:OdooUserName").Value;
            OdooPassword = _configuration.GetSection("Odoo:OdooPassword").Value;
            odoo = new OdooXmlRpcAdapter(
            OdooCommonURL,
            OdooObjectURL,
            OdooDatabaseName,
            OdooUserName,
            OdooPassword,
            "VE Cloud");
        }

        #region Maestros

        public List<Naviera> ListarNavieras(int paginationOffset = 0, int paginationLimit = 100000, string name = "", string codigo="", int id=0)
        {
            var cacheKey = CacheTipo.NAVIERA;
            var lista = _memoryCache.Get<List<Naviera>>(cacheKey);

            if (lista is null)
            {
                lista = new List<Naviera>();

                var listaCondiciones = new List<OdooDomainExpression>();

                //if (!string.IsNullOrEmpty(name))
                //    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));


                //if (!string.IsNullOrEmpty(codigo))
                //    listaCondiciones.Add(new OdooDomainExpression("ref", OdooComparisonOperator.Equals, codigo));

                //if (id > 0)
                //    listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, id));
                
                
                //Filtro para obtener los registros con ID mayor a 0, ya que sino no trae el listado.
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

                //Filtro para obtener solo navieras
                listaCondiciones.Add(new OdooDomainExpression("x_is_shipping_line", OdooComparisonOperator.Equals, true));

                var odooResponse = odoo.SearchRead(
                                "res.partner",
                                paginationOffset,
                                paginationLimit,
                                new string[] { "name", "ref", "x_goes_for_inttra" },
                                listaCondiciones.ToArray());

                foreach (var item in odooResponse)
                {
                    var entity = new Naviera();
                    entity.Id = (int)item["id"];
                    entity.Nombre = item["name"].ToString();
                    entity.Codigo = item["ref"].ToString();
                    entity.VaPorInttra = (bool)item["x_goes_for_inttra"];
                    lista.Add(entity);
                }

                _memoryCache.Set(cacheKey, lista.OrderBy(a => a.Nombre).ToList(), TimeSpan.FromHours(HORAS_CACHE));
            }

            if (!string.IsNullOrEmpty(name))
                lista = lista.Where(a => a.Nombre.Contains(name)).ToList();

            if (!string.IsNullOrEmpty(codigo))
                lista = lista.Where(a => a.Codigo == codigo).ToList();

            if (id > 0)
                lista = lista.Where(a => a.Id == id).ToList();

            return lista;
        }

        public List<Campania> ListarCampanias(int paginationOffset = 0, int paginationLimit = 100000, DateTime? initialDate = null, DateTime? finalDate = null, string name = "")
        {
            var cacheKey = CacheTipo.CAMPANIA;
            var lista = _memoryCache.Get<List<Campania>>(cacheKey);

            if (lista is null)
            {
                lista = new List<Campania>();
                var listaCondiciones = new List<IOdooDomainMember>();

                //if (initialDate.HasValue)
                //    listaCondiciones.Add(new OdooDomainExpression("x_initial_date", OdooComparisonOperator.Equals, initialDate.Value));

                //if (finalDate.HasValue)
                //    listaCondiciones.Add(new OdooDomainExpression("x_final_date", OdooComparisonOperator.Equals, finalDate.Value));

                //if (!string.IsNullOrEmpty(name))
                //    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

                //if (listaCondiciones.Count == 0)
                //    listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));


                var odooResponse = odoo.SearchRead(
                    "logistic.campaign",
                    paginationOffset,
                    paginationLimit,
                    new string[] { "name", "x_commodity_id", "x_initial_date", "x_final_date", "x_description" },
                    listaCondiciones.ToArray());

                foreach (var item in odooResponse)
                {
                    var entity = new Campania();
                    entity.Id = (int)item["id"];
                    entity.Nombre = item["name"].ToString();
                    entity.FechaInicio = Convert.ToDateTime(item["x_initial_date"]);
                    entity.FechaFin = Convert.ToDateTime(item["x_final_date"]);
                    entity.Descripcion = item["x_description"].ToString();
                    lista.Add(entity);
                }

                _memoryCache.Set(cacheKey, lista.OrderBy(a => a.Nombre).ToList(), TimeSpan.FromHours(HORAS_CACHE));
            }

            return lista;
        }

        public List<Commodity> ListarCommodity(int paginationOffset = 0, int paginationLimit = 100, string name = "")
        {
            //var cacheKey = CacheTipo.COMMODITY;
            //var lista = _memoryCache.Get<List<Commodity>>(cacheKey);

            //if (lista is null)
            //{
                var lista = new List<Commodity>();
                var listaCondiciones = new List<OdooDomainExpression>();

                if (!string.IsNullOrEmpty(name))
                    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

                if (listaCondiciones.Count == 0)
                    listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

                var odooResponse = odoo.SearchRead(
                            "commodity",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "x_arancel_certificate" },
                            listaCondiciones.ToArray());

                foreach (var item in odooResponse)
                {
                    var entity = new Commodity();
                    entity.Id = (int)item["id"];
                    entity.Nombre = (string)item["name"];
                    entity.PartidaArancelaria = (string)item["x_arancel_certificate"];
                    lista.Add(entity);
                }

            //    _memoryCache.Set(cacheKey, lista, TimeSpan.FromHours(HORAS_CACHE));
            //}

            return lista;
        }

        public List<Puerto> ListarPuerto(int paginationOffset = 0, int paginationLimit = 100000, string name = "", string codigo = "", int id=0)
        {
            //var cacheKey = CacheTipo.PUERTO;
            //var lista = _memoryCache.Get<List<Puerto>>(cacheKey);

            //if (lista is null)
            //{
                var lista = new List<Puerto>();
                var listaCondiciones = new List<OdooDomainExpression>();

                if (!string.IsNullOrEmpty(name))
                    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

                if (!string.IsNullOrEmpty(codigo))
                listaCondiciones.Add(new OdooDomainExpression("x_default_code", OdooComparisonOperator.Equals, codigo));

            if(id > 0)
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, id));

            if (listaCondiciones.Count == 0)
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

                var odooResponse = odoo.SearchRead(
                            "port",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "x_default_code", "x_region_id", "x_country_id" },
                            listaCondiciones.ToArray());

                foreach (var item in odooResponse)
                {
                    var entity = new Puerto();
                    entity.Id = (int)item["id"];
                    entity.Nombre = (string)item["name"];
                    entity.Codigo = (string)item["x_default_code"];
                    entity.CodigoPais = entity.Codigo.Substring(0, 2);
                    var itemRegion = (object[])item["x_region_id"];
                    var itemPais = (object[])item["x_country_id"];

                    int cont = 0;
                    foreach (var region in itemRegion)
                    {
                        switch (cont)
                        {
                            case 1: entity.NombreRegion = region.ToString(); break;
                            default: break;
                        }
                        cont++;
                    }

                    cont = 0;
                    foreach (var pais in itemPais)
                    {
                        switch (cont)
                        {
                            case 1: entity.NombrePais = pais.ToString(); break;
                            default: break;
                        }
                        cont++;
                    }

                    lista.Add(entity);
                }

                //_memoryCache.Set(cacheKey, lista.OrderBy(a => a.Nombre).ToList(), TimeSpan.FromHours(HORAS_CACHE));
            //}

            return lista;
        }

        public List<TipoContenedor> ListarTipoContenedor(int paginationOffset = 0, int paginationLimit = 100000, string name = "")
        {
            //var cacheKey = CacheTipo.TIPO_CONTENEDOR;
            //var lista = _memoryCache.Get<List<TipoContenedor>>(cacheKey);

            //if (lista is null)
            //{
                var lista = new List<TipoContenedor>();
                var listaCondiciones = new List<OdooDomainExpression>();

                if (!string.IsNullOrEmpty(name))
                    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

                if (listaCondiciones.Count == 0)
                    listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

                var odooResponse = odoo.SearchRead(
                            "container.type",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "x_international_code", "x_is_principal" },
                            listaCondiciones.ToArray());

                foreach (var item in odooResponse)
                {
                    var entity = new TipoContenedor();
                    entity.Id = (int)item["id"];
                    entity.Nombre = (string)item["name"];
                    entity.CodigoInternacional = item["x_international_code"].ToString();
                    entity.EsPrincipal = (bool)item["x_is_principal"];
                    lista.Add(entity);
                }

            //    _memoryCache.Set(cacheKey, lista.OrderByDescending(a => a.EsPrincipal).ThenBy(a => a.Id).ToList(), TimeSpan.FromHours(HORAS_CACHE));
            //}

            return lista;
        }

        public List<Cliente> ListarClientes(int paginationOffset = 0, int paginationLimit = 100000, string name = "", string numeroDocumento = "", string correo = "", string correoCustomerService = "")
        {
            var lista = new List<Cliente>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

            if (!string.IsNullOrEmpty(numeroDocumento))
                listaCondiciones.Add(new OdooDomainExpression("vat", OdooComparisonOperator.ILike, string.Concat("%", numeroDocumento, "%")));

            if (!string.IsNullOrEmpty(correo))
                listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.ILike, string.Concat("%", correo, "%")));

            if (!string.IsNullOrEmpty(correoCustomerService))
            {
                int customerServiceId = getUserId(correoCustomerService);
                listaCondiciones.Add(new OdooDomainExpression("x_main_service_customer_id", OdooComparisonOperator.Equals, customerServiceId));
            }

            //Filtro para obtener solo clientes
            listaCondiciones.Add(new OdooDomainExpression("customer", OdooComparisonOperator.Equals, true));

            var odooResponse = odoo.SearchRead(
                        "res.partner",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "name", "email", "vat", "l10n_latam_identification_type_id" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Cliente();
                entity.Id = (int)item["id"];
                entity.Nombre = (string)item["name"];

                if (item["email"] is string)
                {
                    entity.Correo = item["email"].ToString();
                }

                if (item["l10n_latam_identification_type_id"] is object[])
                {
                    var tipoDocumento = ((object[])item["l10n_latam_identification_type_id"]).ToList();
                    entity.TipoDocumento = tipoDocumento[1] != null ? tipoDocumento[1].ToString() : "";
                }

                if (item["vat"] is string)
                {
                    entity.NumeroDocumento = item["vat"].ToString();
                }

                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        public List<Cliente> ListarClientesPorCustomer(int paginationOffset = 0, int paginationLimit = 100000, string name = "", string numeroDocumento = "", string correo = "", string correoCustomerServicePrincipal = "", string correoCustomerServiceSecundario = "")
        {
            var lista = new List<Cliente>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

            if (!string.IsNullOrEmpty(numeroDocumento))
                listaCondiciones.Add(new OdooDomainExpression("vat", OdooComparisonOperator.ILike, string.Concat("%", numeroDocumento, "%")));

            if (!string.IsNullOrEmpty(correo))
                listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.ILike, string.Concat("%", correo, "%")));

            if (!string.IsNullOrEmpty(correoCustomerServicePrincipal))
            {
                int customerServiceId = getUserId(correoCustomerServicePrincipal);
                listaCondiciones.Add(new OdooDomainExpression("x_main_service_customer_id", OdooComparisonOperator.Equals, customerServiceId));
            }

            if (!string.IsNullOrEmpty(correoCustomerServiceSecundario))
            {
                int customerServiceId = getUserId(correoCustomerServiceSecundario);
                listaCondiciones.Add(new OdooDomainExpression("x_secondary_service_customer_id", OdooComparisonOperator.Equals, customerServiceId));
            }

            //Filtro para obtener solo clientes
            listaCondiciones.Add(new OdooDomainExpression("customer", OdooComparisonOperator.Equals, true));

            var odooResponse = odoo.SearchRead(
                        "res.partner",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "name", "email", "vat", "l10n_latam_identification_type_id" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Cliente();
                entity.Id = (int)item["id"];
                entity.Nombre = (string)item["name"];

                if (item["email"] is string)
                {
                    entity.Correo = item["email"].ToString();
                }

                if (item["l10n_latam_identification_type_id"] is object[])
                {
                    var tipoDocumento = ((object[])item["l10n_latam_identification_type_id"]).ToList();
                    entity.TipoDocumento = tipoDocumento[1] != null ? tipoDocumento[1].ToString() : "";
                }

                if (item["vat"] is string)
                {
                    entity.NumeroDocumento = item["vat"].ToString();
                }

                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }


        public List<CustomerService> ListarCustomerService(int paginationOffset = 0, int paginationLimit = 100000, string name = "")
        {
            var lista = new List<CustomerService>();
            var listaCondiciones = new List<OdooDomainExpression>();

            //if (!string.IsNullOrEmpty(name))
            //    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

            //customer1@ve-logistics.com
            //Filtro para obtener solo clientes
            listaCondiciones.Add(new OdooDomainExpression("login", OdooComparisonOperator.Equals, name));
            //listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

            var odooResponse = odoo.SearchRead(
                        "res.users",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "login" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new CustomerService();
                entity.Id = (int)item["id"];
                entity.Nombre = (string)item["name"];

                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        public List<Deposito> ListarDepositoVacios(int paginationOffset = 0, int paginationLimit = 100000, string name = "")
        {
            var lista = new List<Deposito>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

            //Filtro para obtener solo clientes
            listaCondiciones.Add(new OdooDomainExpression("x_is_extraportuary", OdooComparisonOperator.Equals, true));

            var odooResponse = odoo.SearchRead(
                        "port.terminal",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "name", "x_is_extraportuary", "x_port_id" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Deposito();
                entity.Id = (int)item["id"];
                entity.Nombre = (string)item["name"];

                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        public List<Deposito> ListarTerminal(int paginationOffset = 0, int paginationLimit = 100000, string name = "", int id = 0, string codigo = "")
        {
            //var cacheKey = CacheTipo.TERMINAL;
            //var lista = _memoryCache.Get<List<Deposito>>(cacheKey);

            //if (lista is null)
            //{
                var lista = new List<Deposito>();
                var listaCondiciones = new List<OdooDomainExpression>();

                if (!string.IsNullOrEmpty(name))
                    listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Like, string.Concat("%", name, "%")));

                if (!string.IsNullOrEmpty(codigo))
                    listaCondiciones.Add(new OdooDomainExpression("x_port_id", OdooComparisonOperator.Equals, codigo));

                if (id > 0)
                    listaCondiciones.Add(new OdooDomainExpression("x_port_id", OdooComparisonOperator.Equals, id));
           
                //Agregado por pedido de SameMotion
                listaCondiciones.Add(new OdooDomainExpression("x_is_port_terminal", OdooComparisonOperator.Equals, true));

                var odooResponse = odoo.SearchRead(
                            "port.terminal",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "x_is_extraportuary", "x_port_id" },
                            listaCondiciones.ToArray());

                foreach (var item in odooResponse)
                {
                    var entity = new Deposito();
                    entity.Id = (int)item["id"];
                    entity.Nombre = (string)item["name"];
                    entity.EsDepositoTemporal = (bool)item["x_is_extraportuary"];

                    lista.Add(entity);
                }

            //    _memoryCache.Set(cacheKey, lista, TimeSpan.FromHours(HORAS_CACHE));
            //}

            return lista;
        }

        public List<Packing> ListarPacking(string correoCliente, string name = "", int paginationOffset = 0, int paginationLimit = 100000)
        {
            //obteniendo el partner_id
            int partner_id = getPartnerId(correoCliente);

            List<int> listaPartnerIdRelacionados = ListarChildIdPorCliente(partner_id);

            var lista = new List<Packing>();
            var listaCondiciones = new List<IOdooDomainMember>();

            for (int i = 0; i < listaPartnerIdRelacionados.Count - 1; i++)
            {
                listaCondiciones.Add(OdooLogicalOperator.Or);
            }

            foreach (var item in listaPartnerIdRelacionados)
            {
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, item));
            }

            if (!listaPartnerIdRelacionados.Any()) return new List<Packing>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Like, string.Concat("%", name, "%")));

            //Filtro para obtener solo para Deposito de packing
            //if (tipoChildCliente == TipoChildCliente.DIRECCION_PACKING)
                listaCondiciones.Add(new OdooDomainExpression("type", OdooComparisonOperator.Equals, "packing_address"));

            //if (tipoChildCliente == TipoChildCliente.CONTACTO)
            //    listaCondiciones.Add(new OdooDomainExpression("type", OdooComparisonOperator.Equals, "contact"));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "ref" },
                            listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Packing();
                entity.Id = (int)item["id"];
                entity.Nombre = item["name"].ToString();
                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        private List<int> ListarChildIdPorCliente(int idCliente, int paginationOffset = 0, int paginationLimit = 100000)
        {
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, idCliente));

            
            //if(tipoChildCliente == TipoChildCliente.DIRECCION_PACKING)
            //    listaCondiciones.Add(new OdooDomainExpression("type", OdooComparisonOperator.Equals, "packing_address"));

            //if (tipoChildCliente == TipoChildCliente.CONTACTO)
            //    listaCondiciones.Add(new OdooDomainExpression("type", OdooComparisonOperator.Equals, "contact"));


            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "child_ids" },
                            listaCondiciones.ToArray());

            List<int> listaIdDetalleCotizacion = new List<int>();
            foreach (var item in odooResponse)
            {
                if (item["child_ids"] is int[])
                {
                    int[] orderLine = ((int[])item["child_ids"]);
                    orderLine.ToList().ForEach(a => listaIdDetalleCotizacion.Add(a));
                }
            }

            return listaIdDetalleCotizacion;
        }

        public Company getCompany()
        {
            var listaCondiciones = new List<IOdooDomainMember>();


            listaCondiciones.Add(new OdooDomainExpression("vat", OdooComparisonOperator.Equals, RUC));

            var odooResponse = odoo.SearchRead(
                            "res.company",
                            0,
                            1,
                            new string[] { "x_shipping_product_id", "x_logistic_dt_product_id", "x_logistic_direct_product_id" },
                            listaCondiciones.ToArray());

            var respuesta = new Company();

            if (odooResponse.Any())
            {
                var item = odooResponse.First();

                respuesta.IdFleteMaritimo = Convert.ToInt32(((object[])item["x_shipping_product_id"]).ToList()[0]);
                respuesta.IdOperacionLogistica_Directo = Convert.ToInt32(((object[])item["x_logistic_direct_product_id"]).ToList()[0]);
                respuesta.IdOperacionLogistica_DepositoTemporal = Convert.ToInt32(((object[])item["x_logistic_dt_product_id"]).ToList()[0]);
            }

            return respuesta;
        }

        public List<CrossCutting.OdooEntity.Servicio> GetProduct(List<string> listaNombre)
        {
            var listaServicios = new List<CrossCutting.OdooEntity.Servicio>();
            var listaCondiciones = new List<IOdooDomainMember>();

            for (int i = 0; i < listaNombre.Count - 1; i++)
            {
                listaCondiciones.Add(OdooLogicalOperator.Or);
            }
            //Envio Documentos Courier
            //Envio Documentos Courier

            foreach (var item in listaNombre)
            {
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Equals, item));
            }

            var odooResponse = odoo.SearchRead(
                            "product.product",
                            0,
                            10000,
                            new string[] { "name" },
                            listaCondiciones.ToArray());
            foreach (var item in odooResponse)
            {
                var servicio = new CrossCutting.OdooEntity.Servicio();
                servicio.IdServicio = Convert.ToInt32(item["id"]);
                servicio.Nombre = item["name"].ToString();

                listaServicios.Add(servicio);
            }

            return listaServicios;
        }

        private CrossCutting.OdooEntity.Servicio GetProduct(string nombre)
        {
            var servicio = new CrossCutting.OdooEntity.Servicio();
            var listaCondiciones = new List<IOdooDomainMember>();


            listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Equals, nombre));

            var odooResponse = odoo.SearchRead(
                            "product.product",
                            0,
                            10000,
                            new string[] { "name" },
                            listaCondiciones.ToArray());

            var respuesta = new CrossCutting.OdooEntity.Servicio();

            if (odooResponse.Any())
            {
                respuesta.IdServicio = Convert.ToInt32(odooResponse.First()["id"]);
                respuesta.Nombre = odooResponse.First()["name"].ToString();
            }

            return respuesta;
        }

        public Cliente getCliente(string correo)
        {
            var cliente = new Cliente();
            var listaCondiciones = new List<OdooDomainExpression>();

            //Filtro para obtener solo clientes
            listaCondiciones.Add(new OdooDomainExpression("customer", OdooComparisonOperator.Equals, true));

            //Filtro por correo
            listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.Equals, correo));

            var odooResponse = odoo.SearchRead(
                        "res.partner",
                        0,
                        1,
                        new string[] { "name", "street","vat", "phone", "mobile"},
                        listaCondiciones.ToArray());

            if(odooResponse.Any())
            {
                cliente.Id = (int)odooResponse[0]["id"];
                cliente.Nombre = (string)odooResponse[0]["name"];

                if (odooResponse[0]["street"] is string) cliente.Direccion = (string)odooResponse[0]["street"];
                if (odooResponse[0]["vat"] is string) cliente.NumeroDocumento = (string)odooResponse[0]["vat"];
                if (odooResponse[0]["phone"] is string) cliente.Telefono = (string)odooResponse[0]["phone"];
                if (odooResponse[0]["mobile"] is string) cliente.Celular = (string)odooResponse[0]["mobile"];

                cliente.Contactos = ListarContactos(correo);
            }

            return cliente;
        }

        public List<Contacto> ListarContactos(string correoCliente, string name = "", int paginationOffset = 0, int paginationLimit = 100000)
        {
            //obteniendo el partner_id
            int partner_id = getPartnerId(correoCliente);

            List<int> listaPartnerIdRelacionados = ListarChildIdPorCliente(partner_id);

            var lista = new List<Contacto>();
            var listaCondiciones = new List<IOdooDomainMember>();

            for (int i = 0; i < listaPartnerIdRelacionados.Count - 1; i++)
            {
                listaCondiciones.Add(OdooLogicalOperator.Or);
            }

            foreach (var item in listaPartnerIdRelacionados)
            {
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, item));
            }

            if (!listaPartnerIdRelacionados.Any()) return new List<Contacto>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Like, string.Concat("%", name, "%")));

            //Filtro para obtener solo para Deposito de packing
            //if (tipoChildCliente == TipoChildCliente.DIRECCION_PACKING)
            //listaCondiciones.Add(new OdooDomainExpression("type", OdooComparisonOperator.Equals, "packing_address"));

            //if (tipoChildCliente == TipoChildCliente.CONTACTO)
               listaCondiciones.Add(new OdooDomainExpression("type", OdooComparisonOperator.Equals, "contact"));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "ref" },
                            listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Contacto();
                entity.Id = (int)item["id"];
                entity.Nombre = item["name"].ToString();
                lista.Add(entity);
            }

            return lista;
        }

        public List<Deposito> ListarDepositoVaciosPorNaviera(int idNaviera, int idPuertoOrigen, string name = "", int paginationOffset = 0, int paginationLimit = 100000)
        {
            var lista = new List<Deposito>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Like, string.Concat("%", name, "%")));

            listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, idNaviera));

            //Filtro para obtener solo navieras
            listaCondiciones.Add(new OdooDomainExpression("x_is_shipping_line", OdooComparisonOperator.Equals, true));

            var odooResponse = odoo.SearchRead(
                        "res.partner",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "name", "x_port_terminal" }, //,"x_is_extraportuary", "x_port_id" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                List<Deposito> listaDepositos = new List<Deposito>();
                if (item["x_port_terminal"] is int[])
                {
                    listaDepositos = ListarDepositoVacios((int[])item["x_port_terminal"], idPuertoOrigen);
                }

                lista.AddRange(listaDepositos);
            }
            return lista;
        }

        private List<Deposito> ListarDepositoVacios(int[] listaIdTerminal, int idPuertoOrigen, int paginationOffset = 0, int paginationLimit = 100000, string name = "")
        {
            var lista = new List<Deposito>();
            var listaCondiciones = new List<IOdooDomainMember>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Like, string.Concat("%", name, "%")));

            for (int i = 0; i < listaIdTerminal.Length - 1; i++)
            {
                listaCondiciones.Add(OdooLogicalOperator.Or);
            }

            foreach (var item in listaIdTerminal)
            {
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, item));
            }

            listaCondiciones.Add(new OdooDomainExpression("x_port_id", OdooComparisonOperator.Equals, idPuertoOrigen));
            //Filtro para obtener solo clientes
            //listaCondiciones.Add(new OdooDomainExpression("x_is_extraportuary", OdooComparisonOperator.Equals, true));

            var odooResponse = odoo.SearchRead(
                        "port.terminal",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "name", "x_is_extraportuary", "x_port_id" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Deposito();
                entity.Id = (int)item["id"];
                entity.Nombre = (string)item["name"];

                lista.Add(entity);
            }

            return lista;
        }

        public List<CrossCutting.OdooEntity.Servicio> GetTipoServicioOPL(TipoServicioOPL tipoServicioOPL)
        {
            var listaServicios = new List<CrossCutting.OdooEntity.Servicio>();
            var listaCondiciones = new List<IOdooDomainMember>();

            switch (tipoServicioOPL)
            {
                case TipoServicioOPL.FILTRO:
                    listaCondiciones.Add(new OdooDomainExpression("x_is_filter_thermo", OdooComparisonOperator.Equals, "filter"));
                    break;
                case TipoServicioOPL.TERMO:
                    listaCondiciones.Add(new OdooDomainExpression("x_is_filter_thermo", OdooComparisonOperator.Equals, "thermo"));
                    break;
            }

            var odooResponse = odoo.SearchRead(
                            "product.product",
                            0,
                            10000,
                            new string[] { "name" },
                            listaCondiciones.ToArray());
            foreach (var item in odooResponse)
            {
                var servicio = new CrossCutting.OdooEntity.Servicio();
                servicio.IdServicio = Convert.ToInt32(item["id"]);
                servicio.Nombre = item["name"].ToString();

                listaServicios.Add(servicio);
            }

            return listaServicios;
        }

        public List<ContractOwner> ListarContractOwner(int paginationOffset = 0, int paginationLimit = 100000,
            string name = "", int id = 0)
        {
            var lista = new List<ContractOwner>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

            if (id > 0)
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, id));

            if (listaCondiciones.Count == 0)
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name", "email" },
                            listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new ContractOwner();
                entity.Id = (int)item["id"];
                entity.Nombre = item["name"].ToString();
                if (item["email"] is string)
                {
                    entity.Correo = item["email"].ToString();
                }
                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        public List<CondicionPago> ListarCondicionPago(int paginationOffset = 0, int paginationLimit = 100000, string name = "")
        {
            var lista = new List<CondicionPago>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.ILike, string.Concat("%", name, "%")));

            if (listaCondiciones.Count == 0)
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.GreaterThan, 0));

            var odooResponse = odoo.SearchRead(
                        "account.payment.term",
                        paginationOffset,
                        paginationLimit,
                        new string[] { "name" },
                        listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new CondicionPago();
                entity.Id = (int)item["id"];
                entity.Nombre = (string)item["name"];
                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        public List<Transporte> ListarTransporte(int paginationOffset = 0, int paginationLimit = 100000, string name = "")
        {
            var lista = new List<Transporte>();
            var listaCondiciones = new List<OdooDomainExpression>();

            if (!string.IsNullOrEmpty(name))
                listaCondiciones.Add(new OdooDomainExpression("name", OdooComparisonOperator.Like, string.Concat("%", name, "%")));

            //Filtro para obtener solo navieras
            listaCondiciones.Add(new OdooDomainExpression("supplier", OdooComparisonOperator.Equals, true));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "name" },
                            listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var entity = new Transporte();
                entity.Id = (int)item["id"];
                entity.Nombre = item["name"].ToString();
                lista.Add(entity);
            }

            return lista.OrderBy(a => a.Nombre).ToList();
        }

        #endregion

        #region GetCodigos

        public int GetTeamId(string teamType)
        {
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("x_team_type", OdooComparisonOperator.Equals, teamType));

            int[] idTeam = odoo.Search(
                "crm.team",
                0,
                1,
                listaCondiciones.ToArray());

            if (idTeam.Length == 0)
            {
                //TODO: DEFINIR SI se devuelve una excepcion
            }

            return idTeam[0];
        }

        public int getUserId(string correo)
        {
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("login", OdooComparisonOperator.Equals, correo));

            int[] IDs = odoo.Search(
                "res.users",
                0,
                1,
                listaCondiciones.ToArray());

            if (IDs.Length == 0)
            {
                throw new Exception($"No se encontró en Odoo un usuario con el correo {correo}");
            }

            return IDs[0];
        }

        public int getPartnerId(string correo)
        {
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.Equals, correo));

            int[] IDs = odoo.Search(
                "res.partner",
                0,
                1,
                listaCondiciones.ToArray());

            if (IDs.Length == 0)
            {
                throw new Exception($"No se encontró un partner en Odoo con el correo {correo}");
            }

            return IDs[0];
        }

        public User getUserById(int userId)
        {
            User userOdoo = new User();
            var listaCondiciones = new List<OdooDomainExpression>();

            //Filtro por correo
            listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, userId));

            var odooResponse = odoo.SearchRead(
                        "res.users",
                        0,
                        1,
                        new string[] { "name","login" },
                        listaCondiciones.ToArray());

            if (odooResponse.Any())
            {
                userOdoo.Id = (int)odooResponse[0]["id"];
                userOdoo.Nombre = (string)odooResponse[0]["name"];
                userOdoo.Email = (string)odooResponse[0]["login"];
            }

            return userOdoo;
        }

        public string getUserNameByEmail(string correo)
        {
            string partnerName = string.Empty;
            var listaCondiciones = new List<OdooDomainExpression>();

            //Filtro para obtener solo clientes
            listaCondiciones.Add(new OdooDomainExpression("customer", OdooComparisonOperator.Equals, true));

            //Filtro por correo
            listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.Equals, correo));

            var odooResponse = odoo.SearchRead(
                        "res.partner",
                        0,
                        1,
                        new string[] { "name" },
                        listaCondiciones.ToArray());

            if (odooResponse.Any())
            {
                partnerName = (string)odooResponse[0]["name"];
            }

            return partnerName;
        }

        public int getCustomerIdPorCliente(int idCliente)
        {
            int id = 0;
            var listaCondiciones = new List<IOdooDomainMember>();

            listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, idCliente));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            0,
                            1,
                            new string[] { "x_main_service_customer_id" },
                            listaCondiciones.ToArray());
            if (odooResponse.Any())
            {
                var customer = (object[])odooResponse.First()["x_main_service_customer_id"];
                if (customer != null)
                {
                    int cont = 1;
                    foreach (var region in customer)
                    {
                        switch (cont)
                        {
                            case 1: id = Convert.ToInt32(region.ToString()); break;
                            default: break;
                        }
                        cont++;
                    }
                    //throw new Exception($"No se encontró customer service principal asociado al cliente con id {idCliente}");
                }
            }

            return id;
        }

        public User getCustomerPorClienteCorreo(string correo)
        {
            int id = 0;
            User user = new User();
            var listaCondiciones = new List<IOdooDomainMember>();

            listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.Equals, correo));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            0,
                            1,
                            new string[] { "x_main_service_customer_id" },
                            listaCondiciones.ToArray());
            if (odooResponse.Any())
            {
                var customer = (object[])odooResponse.First()["x_main_service_customer_id"];
                if (customer != null)
                {
                    int cont = 1;
                    foreach (var region in customer)
                    {
                        switch (cont)
                        {
                            case 1: id = Convert.ToInt32(region.ToString()); break;
                            default: break;
                        }
                        cont++;
                    }
                    if (id > 0)
                    {
                        user = getUserById(id);
                    }
                    //throw new Exception($"No se encontró customer service principal asociado al cliente con id {idCliente}");
                }
            }

            return user;
        }


        public string getCustomerNamePorClienteCorreo(string correo)
        {
            int id = 0;
            string nombreCustomer = string.Empty;
            var listaCondiciones = new List<IOdooDomainMember>();

            listaCondiciones.Add(new OdooDomainExpression("email", OdooComparisonOperator.Equals, correo));

            var odooResponse = odoo.SearchRead(
                            "res.partner",
                            0,
                            1,
                            new string[] { "x_main_service_customer_id" },
                            listaCondiciones.ToArray());
            if (odooResponse.Any())
            {
                var customer = (object[])odooResponse.First()["x_main_service_customer_id"];
                if (customer != null)
                {
                    int cont = 1;
                    foreach (var region in customer)
                    {
                        switch (cont)
                        {
                            case 1: id = Convert.ToInt32(region.ToString()); break;
                            default: break;
                        }
                        cont++;
                    }
                    if (id > 0)
                    {
                        nombreCustomer = getUserNamerById(id);
                    }
                    //throw new Exception($"No se encontró customer service principal asociado al cliente con id {idCliente}");
                }
            }

            return nombreCustomer;
        }

        public string getUserNamerById(int userId)
        {
            string partnerName = string.Empty;
            var listaCondiciones = new List<OdooDomainExpression>();

            //Filtro por correo
            listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, userId));

            var odooResponse = odoo.SearchRead(
                        "res.users",
                        0,
                        1,
                        new string[] { "name" },
                        listaCondiciones.ToArray());

            if (odooResponse.Any())
            {

                partnerName = (string)odooResponse[0]["name"];
            }

            return partnerName;
        }

        public int getNavieraId(string codigoNaviera)
        {
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("ref", OdooComparisonOperator.Equals, codigoNaviera));

            int[] IDs = odoo.Search(
                "res.partner",
                0,
                1,
                listaCondiciones.ToArray());

            if (IDs.Length == 0)
            {
                throw new Exception($"No se encontró una naviera en Odoo con el codigo {codigoNaviera}");
            }

            return IDs[0];
        }

        #endregion


        #region Cotizacion

        public string GetCotizacionPDF(int idCotizacion)
        {
            string respuestaPDF = string.Empty;
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, idCotizacion));

            var odooResponse = odoo.SearchRead(
                            "sale.order",
                            0,
                            1,
                            new string[] { "x_quotation_report" },
                            listaCondiciones.ToArray());

            if (odooResponse.Any())
            {
                if (odooResponse[0]["x_quotation_report"] is string)
                {
                    respuestaPDF = odooResponse[0]["x_quotation_report"].ToString();
                }
            }

            return respuestaPDF;
        }

        public int CrearCotizacion(CotizacionOdoo cotizacionOdoo)
        {
            int resultado = 0;
            if (cotizacionOdoo.TipoCotizacion == TipoCotizacion2.FleteMaritimo)
            {
                resultado = CrearCotizacionMaritima(cotizacionOdoo);
            }
            else if (cotizacionOdoo.TipoCotizacion == TipoCotizacion2.OperacionLogistica)
            {
                resultado = CrearCotizacionLogistica(cotizacionOdoo);
            }

            return resultado;
        }

        private int CrearCotizacionMaritima(CotizacionOdoo cotizacionOdoo)
        {
            //obteniendo el partner_id
            int partner_id = getPartnerId(cotizacionOdoo.CorreoCliente);

            //obteniendo el team_id
            int team_id = GetTeamId("shipping_line");

            var compania = getCompany();

            XmlRpcStruct cotizacion = new XmlRpcStruct();
            cotizacion.Add("partner_id", partner_id);
            cotizacion.Add("x_commodity_id", cotizacionOdoo.IdCommodity);
            cotizacion.Add("x_campaign_id", cotizacionOdoo.IdCampania);
            cotizacion.Add("x_ve_cloud_id", cotizacionOdoo.IdCotizacionVeCloud);
            cotizacion.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());
            cotizacion.Add("team_id", team_id);

            cotizacion.Add("x_container_type_id", cotizacionOdoo.IdTipoContenedor);
            cotizacion.Add("x_container_qty", cotizacionOdoo.CantidadContenedor);
            cotizacion.Add("x_extra_notes", cotizacionOdoo.Notas);
                        
            List<object> listaDetalle = new List<object>();
            foreach (var item in cotizacionOdoo.ListaCotizacionDetalle)
            {
                XmlRpcStruct cotizacionDetalle = new XmlRpcStruct();
                cotizacionDetalle.Add("x_shipment_line_id", item.IdNaviera);
                cotizacionDetalle.Add("x_port_loading_id", item.IdPuertoCarga);
                cotizacionDetalle.Add("product_uom_qty", 0); //AGREGADO EN DURO POR PEDIDO DE SAME MOTION
                cotizacionDetalle.Add("x_ve_cloud_id", item.IdCotizacionVeCloud);
                cotizacionDetalle.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());
                cotizacionDetalle.Add("product_id", compania.IdFleteMaritimo);
                cotizacionDetalle.Add("x_port_discharge_id", item.IdPuertoDescarga);

                object[] detalle = new object[3];
                detalle[0] = 0;
                detalle[1] = 0;
                detalle[2] = cotizacionDetalle;

                listaDetalle.Add(detalle);
            }

            cotizacion.Add("order_line", listaDetalle.ToArray());

            int idCotizacionOdoo = odoo.Create("sale.order", cotizacion);

            return idCotizacionOdoo;
        }

        private int CrearCotizacionLogistica(CotizacionOdoo cotizacionOdoo)
        {
            //obteniendo el partner_id
            int partner_id = getPartnerId(cotizacionOdoo.CorreoCliente);

            //obteniendo el team_id
            int team_id = GetTeamId("logistic");

            XmlRpcStruct cotizacion = new XmlRpcStruct();
            cotizacion.Add("partner_id", partner_id);
            cotizacion.Add("x_commodity_id", cotizacionOdoo.IdCommodity);
            cotizacion.Add("x_campaign_id", cotizacionOdoo.IdCampania);
            cotizacion.Add("x_ve_cloud_id", cotizacionOdoo.IdCotizacionVeCloud);
            cotizacion.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());
            cotizacion.Add("team_id", team_id);
            cotizacion.Add("x_container_type_id", cotizacionOdoo.IdTipoContenedor);
            cotizacion.Add("x_extra_notes", cotizacionOdoo.Notas);

            //ObtenerIdServicios(cotizacionOdoo);

            List<object> listaDetalle = new List<object>();
            foreach (var item in cotizacionOdoo.ListaCotizacionDetalle)
            {
                XmlRpcStruct cotizacionDetalleDirecto = new XmlRpcStruct();
                cotizacionDetalleDirecto.Add("x_shipment_line_id", item.IdNaviera);
                cotizacionDetalleDirecto.Add("x_port_loading_id", item.IdPuertoCarga);
                cotizacionDetalleDirecto.Add("product_uom_qty", 0); //AGREGADO EN DURO POR PEDIDO DE SAME MOTION
                cotizacionDetalleDirecto.Add("x_ve_cloud_id", item.IdCotizacionVeCloud);
                cotizacionDetalleDirecto.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());

                cotizacionDetalleDirecto.Add("product_id", item.IdProduct);
                cotizacionDetalleDirecto.Add("x_terminal_empty_entry_id", item.IdTerminalDepositoVacio);
                cotizacionDetalleDirecto.Add("x_terminal_empty_removal_id", item.IdTerminalRetiroVacio);
                cotizacionDetalleDirecto.Add("x_partner_location_id", item.IdAlmacenPacking);

                List<object> listaDetalleServiciosDirecto = new List<object>();
                foreach (var itemServicio in item.ListaServicios)
                {
                    if (itemServicio.IdServicio > 0)
                    {
                        XmlRpcStruct cotizacionDetalleServicio = new XmlRpcStruct();
                        cotizacionDetalleServicio.Add("x_product_id", itemServicio.IdServicio);
                        cotizacionDetalleServicio.Add("x_include_in_price", true);
                        cotizacionDetalleServicio.Add("x_description", itemServicio.Descripcion);
                        cotizacionDetalleServicio.Add("x_quantity", itemServicio.Cantidad);

                        object[] servicios = new object[3];
                        servicios[0] = 0;
                        servicios[1] = 0;
                        servicios[2] = cotizacionDetalleServicio;
                        listaDetalleServiciosDirecto.Add(servicios);
                    }
                }

                if (listaDetalleServiciosDirecto.Any())
                    cotizacionDetalleDirecto.Add("x_extra_service_ids", listaDetalleServiciosDirecto.ToArray());

                object[] detalleDirecto = new object[3];
                detalleDirecto[0] = 0;
                detalleDirecto[1] = 0;
                detalleDirecto[2] = cotizacionDetalleDirecto;

                listaDetalle.Add(detalleDirecto);
            }

            cotizacion.Add("order_line", listaDetalle.ToArray());

            int idCotizacionOdoo = odoo.Create("sale.order", cotizacion);

            return idCotizacionOdoo;
        }

        private int CrearCotizacionLogistica_old(CotizacionOdoo cotizacionOdoo)
        {
            //obteniendo el partner_id
            int partner_id = getPartnerId(cotizacionOdoo.CorreoCliente);

            //obteniendo el team_id
            int team_id = GetTeamId("logistic");

            var compania = getCompany();

            var listaNaviera = ListarNavieras();

            var tipoContenedor = ListarTipoContenedor(name: TERMINAL_DEFECTO_OPERACION_LOGISTICA);

            XmlRpcStruct cotizacion = new XmlRpcStruct();
            cotizacion.Add("partner_id", partner_id);
            cotizacion.Add("x_commodity_id", cotizacionOdoo.IdCommodity);
            cotizacion.Add("x_campaign_id", cotizacionOdoo.IdCampania);
            cotizacion.Add("x_ve_cloud_id", cotizacionOdoo.IdCotizacionVeCloud);
            cotizacion.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());
            cotizacion.Add("team_id", team_id);
            if(tipoContenedor.Any())
                cotizacion.Add("x_container_type_id", tipoContenedor.First().Id);

            cotizacion.Add("x_extra_notes", cotizacionOdoo.Notas);

            ObtenerIdServicios(cotizacionOdoo);

            List<object> listaDetalle = new List<object>();
            foreach (var item in cotizacionOdoo.ListaCotizacionDetalle)
            {
                var listaTerminal = ListarTerminal(id: item.IdPuertoCarga);

                foreach (var naviera in listaNaviera)
                {
                    foreach (var terminal in listaTerminal)
                    {
                        //TERMINAL DIRECTO
                        XmlRpcStruct cotizacionDetalleDirecto = new XmlRpcStruct();
                        cotizacionDetalleDirecto.Add("x_shipment_line_id", naviera.Id);
                        cotizacionDetalleDirecto.Add("x_port_loading_id", item.IdPuertoCarga);
                        cotizacionDetalleDirecto.Add("product_uom_qty", 0); //AGREGADO EN DURO POR PEDIDO DE SAME MOTION
                        cotizacionDetalleDirecto.Add("x_ve_cloud_id", item.IdCotizacionVeCloud);
                        cotizacionDetalleDirecto.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());

                        cotizacionDetalleDirecto.Add("product_id", compania.IdOperacionLogistica_Directo);
                        cotizacionDetalleDirecto.Add("x_terminal_empty_removal_id", terminal.Id);
                        cotizacionDetalleDirecto.Add("x_partner_location_id", item.IdAlmacenPacking);

                        List<object> listaDetalleServiciosDirecto = new List<object>();
                        foreach (var itemServicio in item.ListaServicios)
                        {
                            if (itemServicio.IdServicio > 0)
                            {
                                XmlRpcStruct cotizacionDetalleServicio = new XmlRpcStruct();
                                cotizacionDetalleServicio.Add("x_product_id", itemServicio.IdServicio);
                                cotizacionDetalleServicio.Add("x_include_in_price", true);
                                cotizacionDetalleServicio.Add("x_description", itemServicio.Descripcion);

                                object[] servicios = new object[3];
                                servicios[0] = 0;
                                servicios[1] = 0;
                                servicios[2] = cotizacionDetalleServicio;
                                listaDetalleServiciosDirecto.Add(servicios);
                            }
                        }

                        if (listaDetalleServiciosDirecto.Any())
                            cotizacionDetalleDirecto.Add("x_extra_service_ids", listaDetalleServiciosDirecto.ToArray());

                        object[] detalleDirecto = new object[3];
                        detalleDirecto[0] = 0;
                        detalleDirecto[1] = 0;
                        detalleDirecto[2] = cotizacionDetalleDirecto;

                        listaDetalle.Add(detalleDirecto);

                        //TERMINAL TEMPORAL
                        if (terminal.EsDepositoTemporal)
                        {
                            XmlRpcStruct cotizacionDetalleTemporal = new XmlRpcStruct();
                            cotizacionDetalleTemporal.Add("x_shipment_line_id", naviera.Id);
                            cotizacionDetalleTemporal.Add("x_port_loading_id", item.IdPuertoCarga);
                            cotizacionDetalleTemporal.Add("product_uom_qty", 0); //AGREGADO EN DURO POR PEDIDO DE SAME MOTION
                            cotizacionDetalleTemporal.Add("x_ve_cloud_id", cotizacionOdoo.IdCotizacionVeCloud);
                            cotizacionDetalleTemporal.Add("x_ve_cloud_quote_type", cotizacionOdoo.TipoCotizacion.ToString());

                            cotizacionDetalleTemporal.Add("product_id", compania.IdOperacionLogistica_DepositoTemporal);
                            cotizacionDetalleTemporal.Add("x_terminal_empty_removal_id", terminal.Id);
                            cotizacionDetalleTemporal.Add("x_partner_location_id", item.IdAlmacenPacking);

                            List<object> listaDetalleServiciosTemporal = new List<object>();
                            foreach (var itemServicio in item.ListaServicios)
                            {
                                if (itemServicio.IdServicio > 0)
                                {
                                    XmlRpcStruct cotizacionDetalleServicio = new XmlRpcStruct();
                                    cotizacionDetalleServicio.Add("x_product_id", itemServicio.IdServicio);
                                    cotizacionDetalleServicio.Add("x_include_in_price", true);
                                    cotizacionDetalleServicio.Add("x_description", itemServicio.Descripcion);

                                    object[] servicios = new object[3];
                                    servicios[0] = 0;
                                    servicios[1] = 0;
                                    servicios[2] = cotizacionDetalleServicio;
                                    listaDetalleServiciosTemporal.Add(servicios);
                                }
                            }

                            if (listaDetalleServiciosTemporal.Any())
                                cotizacionDetalleTemporal.Add("x_extra_service_ids", listaDetalleServiciosTemporal.ToArray());

                            object[] detalleTemporal = new object[3];
                            detalleTemporal[0] = 0;
                            detalleTemporal[1] = 0;
                            detalleTemporal[2] = cotizacionDetalleTemporal;

                            listaDetalle.Add(detalleTemporal);
                        }
                    }
                }
            }

            cotizacion.Add("order_line", listaDetalle.ToArray());

            int idCotizacionOdoo = odoo.Create("sale.order", cotizacion);

            return idCotizacionOdoo;
        }

        public bool ConfirmarCotizacionDetalle(int idCotizacionDetalle)
        {
            var respuesta = odoo.Execute<bool>("sale.order", "action_confirm", new int[] { idCotizacionDetalle });

            return respuesta;
        }

        public bool CancelarCotizacionDetalle(int idCotizacionDetalle)
        {
            var respuesta = odoo.Execute<bool>("sale.order", "action_cancel", new int[] { idCotizacionDetalle });

            return respuesta;
        }

        private void ObtenerIdServicios(CotizacionOdoo cotizacionOdoo)
        {
            List<string> listaNombreServicios = new List<string>();

            cotizacionOdoo.ListaCotizacionDetalle.ForEach(a => a.ListaServicios.ForEach(b => listaNombreServicios.Add(b.Nombre)));

            var listaServicios = GetProduct(listaNombreServicios.Distinct().ToList());

            foreach (var detalle in cotizacionOdoo.ListaCotizacionDetalle)
            {
                foreach (var servicio in detalle.ListaServicios)
                {
                    var servicioOdoo = listaServicios.Where(a => a.Nombre == servicio.Nombre);
                    if (servicioOdoo.Any())
                    {
                        servicio.IdServicio = servicioOdoo.First().IdServicio;
                    }
                }
            }
        }

        #endregion

        #region Booking

        public int CrearBooking(BookingOdoo bookingOdoo)
        {
            //obteniendo el partner_id
            int partner_id = getPartnerId(bookingOdoo.CorreoCliente);
            int idCustomerServicePrincipal = getCustomerIdPorCliente(partner_id);
            int idNaviera = getNavieraId(bookingOdoo.CodigoNaviera);

            XmlRpcStruct booking = new XmlRpcStruct();
            booking.Add("x_partner_id", partner_id);
            booking.Add("x_commodity_id", bookingOdoo.IdCommodity);
            booking.Add("x_shipment_line_id", idNaviera);
            booking.Add("x_port_loading_id", bookingOdoo.IdPuertoCarga);
            booking.Add("x_port_discharge_id", bookingOdoo.IdPuertoDescarga);
            booking.Add("x_booking_container_qty", bookingOdoo.CantidadContenedor);
            booking.Add("x_estimated_time_arrival_origin", bookingOdoo.ETA_Origen.ToString("yyyy-MM-dd"));
            booking.Add("x_estimated_time_departure_origin", bookingOdoo.ETD_Origen.ToString("yyyy-MM-dd"));
            booking.Add("x_estimated_time_arrival_destination", bookingOdoo.ETA_Destino.ToString("yyyy-MM-dd"));
            booking.Add("x_container_type_id", bookingOdoo.IdTipoContenedor);
            booking.Add("x_notes", bookingOdoo.Notas);
            booking.Add("x_state", "pending");
            //booking.Add("x_terminal_empty_entry_id", bookingOdoo.IdTerminaDepositoVacio);
            booking.Add("x_vessel", bookingOdoo.NombreNave);
            booking.Add("x_trip_number", bookingOdoo.NumeroViaje);
            // TODO: Ejecutar 
            if (bookingOdoo.IdDetalleCotizacionOdoo > 0)
            {
                booking.Add("x_sale_order_line_id", bookingOdoo.IdDetalleCotizacionOdoo);
            }
            booking.Add("x_customer_service_id", idCustomerServicePrincipal);
            //booking.Add("x_consignee", bookingOdoo.Consignatario);
            booking.Add("x_weight", bookingOdoo.Peso);

            booking.Add("x_container_number", bookingOdoo.NumeroContenedor);
            booking.Add("x_bl_code", bookingOdoo.CodigoBL);
            //if(bookingOdoo.IdTerminaDepositoVacio > 0)
            //    booking.Add("x_terminal_empty_entry_id", bookingOdoo.IdTerminaDepositoVacio);

            booking.Add("x_booking", bookingOdoo.NumeroBooking);//TODO: QUITAR ESTO PARA EXTRAER LA DATA DE INTTRA

            //TECNOLOGIA
            booking.Add("x_technology", bookingOdoo.Tecnologia);

            //***REEFER***
            //CONDICION
            if (bookingOdoo.Condicion.HasValue)
                booking.Add("x_condition", bookingOdoo.Condicion.Value.ToString());

            //COLD TREATMENT
            if (bookingOdoo.ColdTreatment.HasValue)
                booking.Add("x_cold_treatment", bookingOdoo.ColdTreatment);

            //TEMPERATURA

            if (bookingOdoo.Temperatura.HasValue)
                booking.Add("x_temperature", bookingOdoo.Temperatura.Value);

            if(bookingOdoo.TipoTemperatura.HasValue)
                booking.Add("x_temperature_type", bookingOdoo.TipoTemperatura.Value.ToString());


            //VENTILACION
            if (bookingOdoo.TipoVentilacion.HasValue)
            { 
                booking.Add("x_ventilation_type", bookingOdoo.TipoVentilacion.Value.ToString());
                booking.Add("x_ventilation_hide", true);
                if (bookingOdoo.Ventilacion.HasValue)
                    booking.Add("x_ventilation", bookingOdoo.Ventilacion.Value);
            }

            //HUMEDAD
            if (bookingOdoo.TipoHumedad.HasValue)
            { 
                if (bookingOdoo.TipoHumedad == TipoHumedad.on)
                {
                    booking.Add("x_humidity", bookingOdoo.Humedad.Value);
                    booking.Add("x_humidity_hide", true);
                }
            }

            //ATMOSFERA CONTROLADA
            if (bookingOdoo.AtmosferaControlada.HasValue)
            {
                if (bookingOdoo.AtmosferaControlada.Value)
                { 
                    booking.Add("x_controlled_atmosphere", true);

                    if (bookingOdoo.CO2.HasValue)
                        booking.Add("x_co", bookingOdoo.CO2);

                    if (bookingOdoo.O2.HasValue)
                        booking.Add("x_o", bookingOdoo.O2);
                }
            }


            //***DRY***

            if(bookingOdoo.Imo.HasValue)
                booking.Add("x_imo", bookingOdoo.Imo.ToString());

            if(!string.IsNullOrEmpty(bookingOdoo.UN1))
                booking.Add("x_un1", bookingOdoo.UN1);

            if (!string.IsNullOrEmpty(bookingOdoo.UN2))
                booking.Add("x_un2", bookingOdoo.UN2);

            //CONTRACT OWNER
            if(int.TryParse(bookingOdoo.ContractOwnerId, out int _numero))
            { 
                if (!string.IsNullOrEmpty(bookingOdoo.ContractOwnerId) && bookingOdoo.ContractOwnerId!="0")
                    booking.Add("x_contract_owner_id", int.Parse(bookingOdoo.ContractOwnerId));
            }
            //TIPO CONTRATO
            booking.Add("x_contract_type", bookingOdoo.TipoContrato);

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(booking);
            int idBooking = odoo.Create("booking", booking);

            return idBooking;
        }

        public BookingOL GetBooking(string numeroBooking)
        {
            var listaCondiciones = new List<IOdooDomainMember>();

            listaCondiciones.Add(new OdooDomainExpression("x_booking", OdooComparisonOperator.Equals, numeroBooking));

            var odooResponse = odoo.SearchRead(
                            "booking",
                            0,
                            1,
                            new string[] { "name", "x_booking", "x_partner_id", "x_vessel", "x_trip_number",
                                "x_estimated_time_arrival_origin","x_estimated_time_departure_origin","x_estimated_time_arrival_destination"},
                            listaCondiciones.ToArray());


            if (odooResponse.Any())
            {
                var respuesta = new BookingOL();

                var item = odooResponse.First();
                respuesta.IdBooking = Convert.ToInt32(item["id"]);
                if (item["x_partner_id"] is object[])
                {
                    var cliente = ((object[])item["x_partner_id"]).ToList();
                    respuesta.IdCliente = Convert.ToInt32(cliente[0]);
                    respuesta.NombreCliente = cliente[1].ToString();
                }
                respuesta.Nombre = item["name"].ToString();
                respuesta.NumeroBooking = item["x_booking"].ToString();
                respuesta.NombreNave = item["x_vessel"].ToString();
                respuesta.NumeroViaje = item["x_trip_number"].ToString();
                respuesta.ETA_Origen = item["x_estimated_time_arrival_origin"].ToString();
                respuesta.ETA_Destino = item["x_estimated_time_arrival_destination"].ToString();
                respuesta.ETD_Origen = item["x_estimated_time_departure_origin"].ToString();

                return respuesta;
            }
            else
            {
                return null;
            }

        }

        private int getIdBooking(string numeroBooking)
        {
            var listaCondiciones = new List<OdooDomainExpression>();

            listaCondiciones.Add(new OdooDomainExpression("x_booking", OdooComparisonOperator.Equals, numeroBooking));

            int[] IDs = odoo.Search(
                "booking",
                0,
                1,
                listaCondiciones.ToArray());

            if (IDs.Length == 0)
            {
                return 0;
                //throw new Exception($"No se encontró un partner en Odoo con el correo {numeroBooking}");
            }

            return IDs[0];
        }

        public bool RelacionarCotizacionDetalleBooking(int idCotizacionDetalle, string numeroBooking, string numeroContenedor, int condicionPago, string nombreTransporte, DateTime fechaCarguio, string canalSini)
        {
            int idBooking = getIdBooking(numeroBooking);
            if (idBooking == 0) return false;

            XmlRpcStruct booking = new XmlRpcStruct();
            booking.Add("x_container_number", numeroContenedor);
            booking.Add("x_logistic_sale_order_line_id", idCotizacionDetalle);
            booking.Add("x_opl_payment_term_id", condicionPago);
            booking.Add("x_vecloud_transport_provider", nombreTransporte);
            booking.Add("x_loaded_date", fechaCarguio.ToString("yyyy-MM-dd"));
            booking.Add("x_channel", canalSini);

            var resultado = odoo.Write("booking", idBooking, booking);

            return resultado;
        }

        #endregion

        #region Contratos


        public List<ContratoDetalle> ListarContratos(string correoCliente, int idCampania = 0, int idNaviera = 0, int idPuertoOrigen = 0, int idPuertoDestino = 0, int paginationOffset = 0, int paginationLimit = 100000, bool obtenerDatosComplementarios=false)
        {
            var idCliente = getPartnerId(correoCliente);

            var listaIdDetalleCotizacionPorCliente = ListarContratoDetalle_PorCliente(idCliente, idCampania);

            var listaContratoDetalle = ListarContratoDetalle_PorCotizacionDetalle(listaIdDetalleCotizacionPorCliente,idPuertoOrigen,idPuertoDestino, idNaviera);

            //var listaContrato = ListarContrato_PorIdContrato(listaContratoDetalle, idCampania, idNaviera, estado, paginationOffset, paginationLimit);


            //if (obtenerDatosComplementarios)
            //{
            //    var listaNavieras = ListarNavieras();
            var listaPuertos = ListarPuerto();

            foreach (var cotizacionDetalle in listaContratoDetalle)
            {
                //var naviera = listaNavieras.Where(n => n.Id == contrato.IdNaviera).FirstOrDefault();
                //if (naviera != null)
                //{
                //    contrato.NombreNaviera = naviera.Nombre;
                //    contrato.CodigoNaviera = naviera.Codigo;
                //}
                //foreach (var cotizacionDetalle in contrato.ListaContratoDetalle)
                //{
                    var puertoCarga = listaPuertos.FirstOrDefault(p => p.Id == cotizacionDetalle.IdPuertoCarga);
                    if (puertoCarga != null)
                    {
                        //cotizacionDetalle.NombrePuertoCarga = puertoCarga.Nombre;
                        cotizacionDetalle.CodigoPuertoCarga = puertoCarga.Codigo;
                    }

                    var puertoDescarga = listaPuertos.FirstOrDefault(p => p.Id == cotizacionDetalle.IdPuertoDescarga);
                    if (puertoDescarga != null)
                    {
                        //cotizacionDetalle.NombrePuertoDescarga = puertoDescarga.Nombre;
                        cotizacionDetalle.CodigoPuertoDescarga = puertoDescarga.Codigo;
                    }
                //}
            }
            //}

            return listaContratoDetalle;
        }

        private List<ContratoDetalle> ListarContratoDetalle_PorCliente(int idCliente, int idCampania, int paginationOffset = 0, int paginationLimit = 100000)
        {
            List<ContratoDetalle> listaContratoDetalle = new List<ContratoDetalle>();

            var listaCondiciones = new List<OdooDomainExpression>();

            //Filtro para obtener las cotizaciones por cliente
            listaCondiciones.Add(new OdooDomainExpression("partner_id", OdooComparisonOperator.Equals, idCliente));

            //Filtro por Campaña
            if (idCampania > 0) listaCondiciones.Add(new OdooDomainExpression("x_campaign_id", OdooComparisonOperator.Equals, idCampania));

            var odooResponse = odoo.SearchRead(
                            "sale.order",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "order_line", "x_campaign_id" },
                            listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var listaContratoDetalleTMP = new List<ContratoDetalle>();

                if (item["order_line"] is int[])
                {
                    int[] orderLine = ((int[])item["order_line"]);

                    orderLine.ToList().ForEach(a => listaContratoDetalleTMP.Add(new ContratoDetalle() { IdCotizacionDetalleOdoo = a }));
                }

                if (item["x_campaign_id"] is object[])
                {
                    var campania = ((object[])item["x_campaign_id"]).ToList();

                    foreach (var detalle in listaContratoDetalleTMP)
                    {
                        detalle.IdCampania = Convert.ToInt32(campania[0]);
                        detalle.NombreCampania = campania[1].ToString();
                    }
                }

                listaContratoDetalle.AddRange(listaContratoDetalleTMP);
            }

            return listaContratoDetalle;
        }

        private List<ContratoDetalle> ListarContratoDetalle_PorCotizacionDetalle(List<ContratoDetalle> listaContratoDetalle, int idPuertoOrigen, int idPuertoDestino, int idNaviera, int paginationOffset = 0, int paginationLimit = 100000)
        {
            var listaCondiciones = new List<IOdooDomainMember>();

            for (int i = 0; i < listaContratoDetalle.Count - 1; i++)
            {
                listaCondiciones.Add(OdooLogicalOperator.Or);
            }

            foreach (var item in listaContratoDetalle)
            {
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, item.IdCotizacionDetalleOdoo));
            }

            if(idPuertoOrigen>0)
                listaCondiciones.Add(new OdooDomainExpression("x_port_loading_id", OdooComparisonOperator.Equals, idPuertoOrigen));

            if(idPuertoDestino>0)
                listaCondiciones.Add(new OdooDomainExpression("x_port_discharge_id", OdooComparisonOperator.Equals, idPuertoDestino));

            //Filtro por Naviera
            if (idNaviera > 0) 
                listaCondiciones.Add(new OdooDomainExpression("x_shipment_line_id", OdooComparisonOperator.Equals, idNaviera));

            //TODO: FILTRAR LOS ESTADOS APROBADOS

            var odooResponse = odoo.SearchRead(
                            "sale.order.line",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "x_transport_time", "x_contract_id", "x_freight_bl", "x_rebate", "x_net", "x_port_loading_id", "x_port_discharge_id",
                                           "x_contract_number", "x_shipment_line_id","x_valid_date_from","x_valid_date_to"},
                            listaCondiciones.ToArray());
            foreach (var item in odooResponse)
            {
                var idCotizacionDetalleOdoo = Convert.ToInt32(item["id"]);
                var contratoDetalle = new ContratoDetalle();
                if (listaContratoDetalle.Where(a => a.IdCotizacionDetalleOdoo == idCotizacionDetalleOdoo).Count()> 0)
                {
                    contratoDetalle = listaContratoDetalle.Where(a => a.IdCotizacionDetalleOdoo == idCotizacionDetalleOdoo).First();
                }

                
               
                if(item["x_transport_time"] != null)
                    contratoDetalle.TT = item["x_transport_time"].ToString();

                if (item["x_contract_number"] != null)
                    contratoDetalle.TT = item["x_contract_number"].ToString();

                if (item["x_freight_bl"] is double)
                    contratoDetalle.FleteBL = Convert.ToDouble(item["x_freight_bl"]);

                if(item["x_rebate"] is double)
                    contratoDetalle.Rebate = Convert.ToDouble(item["x_rebate"]);

                if(item["x_net"] is double)
                    contratoDetalle.Neto = Convert.ToDouble(item["x_net"]);

                if (item["x_contract_id"] is object[])
                {
                    var x_contract_id = ((object[])item["x_contract_id"]).ToList();
                    contratoDetalle.IdContrato = Convert.ToInt32(x_contract_id[0]);
                }

                if (item["x_port_loading_id"] is object[])
                {
                    var puertoOrigen = ((object[])item["x_port_loading_id"]).ToList();
                    contratoDetalle.IdPuertoCarga = Convert.ToInt32(puertoOrigen[0]);
                    contratoDetalle.NombrePuertoCarga = puertoOrigen[1].ToString();
                }

                if (item["x_port_discharge_id"] is object[])
                {
                    var puertoDestino = ((object[])item["x_port_discharge_id"]).ToList();
                    contratoDetalle.IdPuertoDescarga = Convert.ToInt32(puertoDestino[0]);
                    contratoDetalle.NombrePuertoDescarga = puertoDestino[1].ToString();
                }

                if (item["x_shipment_line_id"] is object[])
                {
                    var naviera = ((object[])item["x_shipment_line_id"]).ToList();
                    contratoDetalle.IdNaviera = Convert.ToInt32(naviera[0]);
                    contratoDetalle.NombreNaviera = naviera[1].ToString();
                }

                if (DateTime.TryParse(item["x_valid_date_from"].ToString(), out DateTime validoDesde))
                {
                    contratoDetalle.ValidoDesde = validoDesde;
                }

                if (DateTime.TryParse(item["x_valid_date_to"].ToString(), out DateTime validoHasta))
                {
                    contratoDetalle.ValidoHasta = validoHasta;
                }


                listaContratoDetalle.Add(contratoDetalle);
            }

            //SE FILTRA LAS COTIZACIONES QUE TENGAN CONTRATO, Y LA ULTIMA COTIZACION DETALLE POR CONTRATO.
            var listaResultado = listaContratoDetalle.Where(a => a.IdContrato > 0)
                                           //.GroupBy(g => g.IdContrato)
                                           //.Select(b => b.OrderByDescending(d => d.IdCotizacionDetalleOdoo).FirstOrDefault())
                                           .ToList();
            return listaResultado;
        }

        private List<Contrato> ListarContrato_PorIdContrato(List<ContratoDetalle> listaContratoDetalle, int idCampania, int idNaviera, string estado, int paginationOffset = 0, int paginationLimit = 100000)
        {
            List<Contrato> listaResultado = new List<Contrato>();
            var listaCondiciones = new List<IOdooDomainMember>();
            List<int> listaContratoId = listaContratoDetalle.Select(a => a.IdContrato).Distinct().ToList();

            for (int i = 0; i < listaContratoId.Count - 1; i++)
            {
                listaCondiciones.Add(OdooLogicalOperator.Or);
            }

            foreach (var item in listaContratoId)
            {
                listaCondiciones.Add(new OdooDomainExpression("id", OdooComparisonOperator.Equals, item));
            }

            //Filtro por Campaña
            if (idCampania > 0) listaCondiciones.Add(new OdooDomainExpression("x_campaign_id", OdooComparisonOperator.Equals, idCampania));

            //Filtro por Naviera
            if (idNaviera > 0) listaCondiciones.Add(new OdooDomainExpression("x_shipment_line_id", OdooComparisonOperator.Equals, idNaviera));

            //Filtro por estado
            if (!string.IsNullOrEmpty(estado)) listaCondiciones.Add(new OdooDomainExpression("x_state", OdooComparisonOperator.Equals, estado));


            var odooResponse = odoo.SearchRead(
                            "shipping.contract",
                            paginationOffset,
                            paginationLimit,
                            new string[] { "x_campaign_id", "x_shipment_line_id", "x_state", "name", "x_contract", "x_initial_date", "x_final_date" },
                            listaCondiciones.ToArray());

            foreach (var item in odooResponse)
            {
                var idContrato = Convert.ToInt32(item["id"]);
                var contrato = new Contrato();

                contrato.IdContrato = idContrato;

                if (item["x_campaign_id"] is object[])
                {
                    var campania = ((object[])item["x_campaign_id"]).ToList();
                    contrato.IdCampania = Convert.ToInt32(campania[0]);
                    contrato.NombreCampania = campania[1].ToString();
                }

                if (item["x_shipment_line_id"] is object[])
                {
                    var naviera = ((object[])item["x_shipment_line_id"]).ToList();
                    //contrato.IdNaviera = Convert.ToInt32(naviera[0]);
                    //contrato.NombreNaviera = naviera[1].ToString();
                }

                contrato.Estado = item["x_state"].ToString();
                contrato.NombreContrato = item["name"].ToString();
                contrato.NumeroContrato = item["x_contract"].ToString();

                if (DateTime.TryParse(item["x_initial_date"].ToString(), out DateTime fechaInicio))
                {
                    contrato.FechaInicio = fechaInicio;
                }

                if (DateTime.TryParse(item["x_final_date"].ToString(), out DateTime fechaFin))
                {
                    contrato.FechaFin = fechaFin;
                }

                contrato.ListaContratoDetalle = listaContratoDetalle.Where(a => a.IdContrato == idContrato).ToList();

                listaResultado.Add(contrato);
            }

            return listaResultado;
        }

        #endregion

    }
}
