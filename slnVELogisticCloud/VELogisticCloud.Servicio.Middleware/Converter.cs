﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace VELogisticCloud.Servicio.Middleware
{
    /// <summary>
    /// https://stackoverflow.com/questions/27928338/implement-custom-datetime-converter-with-json-net/27928572#27928572
    /// </summary>
    public class InttraDataFormatJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // implement in case you're serializing it back
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            DateTime? date = null;
            var dataString = (string)reader.Value;
            if (!string.IsNullOrEmpty(dataString))
            {
                string format = "yyyy-MM-dd HH:mm:ss";
                date = DateTime.ParseExact(dataString, format, CultureInfo.InvariantCulture);
            }

            return date;
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
}
