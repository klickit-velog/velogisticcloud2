﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Middleware.Models
{

    public class Pedido
    {
        public string code { get; set; }
        //public string description { get; set; }
        //public int client_id { get; set; }
        public string client_name { get; set; }
        public string client_document { get; set; }
        public string client_email { get; set; }
        //public int client_phone { get; set; }
        public string client_address { get; set; }
        public float client_lat { get; set; }
        public float client_lng { get; set; }

        public Custom_Properties custom_properties { get; set; }

        /*

        public object time_window { get; set; }
        public string service_time { get; set; }
        public object notes { get; set; }
        public int price { get; set; }
        public int weight { get; set; }
        public int volume { get; set; }
        //public object location_id { get; set; }
        public string state { get; set; }
        //public int user_responsible_id { get; set; }
        public object location_start_id { get; set; }
        public object start_address { get; set; }
        public object start_lat { get; set; }
        public object start_lng { get; set; }
        public object extra_data { get; set; }
        public int is_overtime_notify { get; set; }
        public Item[] items { get; set; }
        public string[] tags_array { get; set; }
        */
    }

    public class Custom_Properties
    {
        public string custom_29 { get; set; }
        public string custom_30 { get; set; }
        public string custom_31 { get; set; }
        public string custom_32 { get; set; }
        public string custom_33 { get; set; }
        public string custom_34 { get; set; }
        public string custom_35 { get; set; }
        public string custom_36 { get; set; }
        public string custom_37 { get; set; }
        public string custom_38 { get; set; }
        public string custom_39 { get; set; }
        public string custom_40 { get; set; }
        public string custom_41 { get; set; }
        public string custom_42 { get; set; }
        public string custom_43 { get; set; }
        public string custom_44 { get; set; }
        public string custom_45 { get; set; }
        public string custom_46 { get; set; }
        public string custom_47 { get; set; }
        public string custom_48 { get; set; }
        public string custom_49 { get; set; }
        public string custom_50 { get; set; }
        public string custom_51 { get; set; }
        public string custom_52 { get; set; }
        public string custom_53 { get; set; }
        public string custom_54 { get; set; }
    }

    public class Item
    {
        public string code { get; set; }
        public string name { get; set; }
        public string brand { get; set; }
        public string weight { get; set; }
        public string volume { get; set; }
        public int measurement_unit_id { get; set; }
        public string quantity { get; set; }
    }

}
