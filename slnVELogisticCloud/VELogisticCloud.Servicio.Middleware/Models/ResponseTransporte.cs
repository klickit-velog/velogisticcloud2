﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Middleware.Models
{
    public class ResponseTransporte
    {
        [JsonProperty("order")]
        public Order order { get; set; }
    }

    public class Order
    {
        [JsonProperty("transports")]
        public Transport[] transports { get; set; }
    }


    public class Transport
    {
        [JsonProperty("vehicle")]

        public Vehicle vehicle { get; set; }
    }

    public class Vehicle
    {
        [JsonProperty("provider")]
        public Provider provider { get; set; }
    }

    public class Provider
    {
        [JsonProperty("name")]
        public string name { get; set; }
    }
}
