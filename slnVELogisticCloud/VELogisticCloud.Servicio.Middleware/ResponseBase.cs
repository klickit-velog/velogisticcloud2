﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VELogisticCloud.Servicio.Middleware
{
    public class ResponseBase
    {
        public bool success { get; set; }
        public int statusCode { get; set; }
        public string message { get; set; }
        public string request { get; set; }
    }
}
