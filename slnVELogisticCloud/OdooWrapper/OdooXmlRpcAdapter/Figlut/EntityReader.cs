﻿namespace Figlut.Server.Toolkit.Data
{
    #region Using Directives

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Data;

    #endregion //Using Directives

    public class EntityReader<E>
    {
        #region Methods

        public static string GetPropertyName<TProp>(Expression<Func<E, TProp>> expression, bool shapePropertyName)
        {
            var body = expression.Body as MemberExpression;
            if (body == null) throw new ArgumentException("'expression' should be a member expression");
            if (shapePropertyName)
            {
                return DataShaper.ShapeCamelCaseString(body.Member.Name);
            }
            return body.Member.Name;
        }
                
        #endregion //Methods
    }
}