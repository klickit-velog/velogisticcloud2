﻿namespace Odoo.XmlRpcAdapter.XmlRpcProxy
{
    #region Using Directives

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CookComputing.XmlRpc;
    using System.Collections;

    #endregion //Using Directives
    //[XmlRpcUrl("https://ve-logistics-pruebas-0904-2363050.dev.odoo.com/xmlrpc/2/common")]
    [XmlRpcUrl("https://ve-logistics-demo-0907-2858096.dev.odoo.com/xmlrpc/2/common")]
    public interface IOdooXmlRpcCommonProxy : IXmlRpcProxy
    {
        #region Methods

        [XmlRpcMethod("authenticate")]
        //int authenticate(string database, string username, string password, params object[] user_agent_env);
        int authenticate(string database, string username, string password, params object[] user_agent_env);

        #endregion //Methods
    }
}
